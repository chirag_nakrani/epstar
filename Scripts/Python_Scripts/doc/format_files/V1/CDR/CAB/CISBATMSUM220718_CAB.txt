                                                                                                           
CISB ATM TRANSACTION SUMMARY REPORT
BUSINESS DATE : 22-Jul-2018
                                                                                                           
------------------------------------------------------------------------------------------------------------------------------------------
                                                                         UNAPPROVED         APPROVED     APPROVED+DECLINED     APPROVED      
S.NO TERM ID          FIID TERM CITY     TERM LOCATION             NON FIN      FIN    NON FIN     FIN        TOTTRAN       AMT DISPENSED    
------------------------------------------------------------------------------------------------------------------------------------------
   1 02051403         0205 MUMBAI        OVERSEAS MAIN BKC               6       9      61        124            200           392900 
   2 02051402         0205 MUMBAI        SION BRANCH                     0      18      12        111            141           605500 
   3 02051404         0205 THANE         KOLSHET THANE                   4      22      50        114            190           336500 
   4 10821401         1082 AKOLA         AKOLA M G ROAD,AKOLA            5      36      24        206            271           481200 
   5 37901401         3790 BHANDARA      KOTHURNA                        0       0       5          9             14            19500 
   6 02221402         0222 DHULE         DHULE KHOL GALLI                0       5      11         18             34           119000 
   7 15611401         1561 DHULE         MORANE                          3       0      16          0             19                0 
   8 85771401         8577 DHULE         SAKRI ROAD DHULE                0       3       7         37             47           140500 
   9 33121401         3312 GONDIA        GONDIA                          8      27      25        103            163           337500 
  10 85771402         8577 KMR NGR DHULE KUMAR NAGAR DHULE               4      20      29        150            203           392000 
  11 37841401         3784 MANA          MANA                            1       0      21          0             22                0 
  12 20781401         2078 NAGPUR        BHARATH NAGAR                   0      22      12        133            167           258300 
  13 20781402         2078 NAGPUR        VNIT NAGPUR                     7       4       7         40             58            82900 
  14 26321401         2632 NAGPUR        WARDHA                          2      13      11         45             71           138500 
  15 02341401         0234 NANDED        TARA SINGH MARKET               3       3       9         21             36            68500 
  16 37921401         3792 WARDHA        BARBADI                         0       0      10          0             10                0 
  17 37831401         3783 WARDHA        HINGNI                          4      23      16         45             88           180500 
  18 16531402         1653 AHMEDNAGAR    VASANT TEKDI                    1      17       8         57             83           132100 
  19 15791401         1579 APTALE        APTALE                          0       0       0          0              0                0 
  20 25741401         2574 CHIPLUN       CHIPLUN                         1      13      10         47             71           225000 
  21 37611401         3761 JALNA         CHITEGAON                       6      29      50        109            194           273100 
  22 02741401         0274 JUNNAR        JUNNAR                          3      18      22        109            152           307900 
  23 19041401         1904 KALAMBOLI     KALAMBOLI STEEL MKT YARD        0       0       0          0              0                0 
  24 38941401         3894 KOLHAPUR      SANE GURUJI VASAHAT             1       6       4         36             47           133500 
  25 24691402         2469 LATUR         LATUR                           1       3      12         23             39            76500 
  26 85761401         8576 LATUR         LATUR CHANDRANAGAR              4      13      38         91            146           217300 
  27 26761401         2676 PANDHARPUR    PANDHARPUR KORTI CAMPUS         0       5      17         48             70            91100 
  28 25751401         2575 PHALTAN       PHALTAN OFFSITE                 2       3      18         29             52            54500 
  29 37931401         3793 PUNE          AWALWADI PUNE                   0       0       0          0              0                0 
  30 03821402         0382 PUNE          DECCAN GYMKHANA                 2      23      52        175            252           323800 
  31 38931401         3893 PUNE          DHANORI LOHAGAON ROAD           8      14      31         92            145           211700 
  32 46521401         4652 PUNE          KHANDESWAR                      0       4      19        166            189           418700 
  33 37751401         3775 PUNE          MANJARI KHURD                   0      12      22         97            131           202800 
  34 03821401         0382 PUNE          NEAR MANGALA TALKIES            0       0       2          0              2                0 
  35 03821404         0382 PUNE          PIRANGUT                        9      37      55        167            268           305900 
  36 34061401         3406 PUNE          PUNE MICRO FINANCE              0       0       0          0              0                0 
  37 37741401         3774 PUNE          SHIKRAPUR                       5      15      41        134            195           348400 
  38 85241401         8524 PUNE          VIMAN NAGAR PUNE                0       0       0          0              0                0 
  39 02751401         0275 PUNE          WALCHAND NAGAR                  1       9      16         90            116           272000 
  40 85271401         8527 PUNE          WARJE MALWADI                   0       6      23        118            147           318600 
  41 16131401         1613 SANGLI        MAIN ROAD ASHTA                 9      11      12         49             81            86400 
  42 37691401         3769 SATARA        VARNE                          16      25      30         82            153           230100 
  43 55531400         5553 DHARMAPURI    MANJAVADII                      1       6       9         33             49            51400 
