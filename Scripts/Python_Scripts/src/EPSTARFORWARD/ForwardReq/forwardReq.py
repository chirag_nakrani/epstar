from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.http import HttpResponse
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework.authtoken.views import ObtainAuthToken
import sys, json
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import ObtainJSONWebToken
from django_python3_ldap.auth import LDAPBackend
from django_python3_ldap import ldap
import requests as req
from rest_framework_jwt.settings import api_settings
from django.shortcuts import render_to_response, render, redirect
from django.template import RequestContext
import logging
from EPSTARFORWARD.settings import BASE_DIR
import os
from ForwardReq.models import user_auth_info
import uuid
import time, datetime


def login(request):
    usr = request.META['REMOTE_USER']
    extract_usr = str(usr).split('\\')[1];
    log_path = os.path.join(BASE_DIR, 'LogFolder/')
    log_file = os.path.join(log_path, 'syslog.txt');
    logging.basicConfig(filename=log_file, filemode='w', format='%(message)s', level=logging.DEBUG)
    logging.info(extract_usr)
    logging.info(request.META)
    userkey = uuid.uuid4();
    current_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    usr_info = user_auth_info(username=extract_usr, userkey=userkey, timestamp_key=current_time,
                              machine_info=request.META, request_params='')
    usr_info.save();
    url = "http://192.168.75.15:4200/login?v=" + str(userkey)#For server
	#url = "http://localhost:4200/login?v=" + str(userkey)######For local
    logging.info(url)
    return redirect(url)
# with ldap.connection() as connection:
# 	# Look up the user by username.
# 	user = connection.get_user(username=extract_usr)
# 	#print(user)
# 	if user:
# 		jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
# 		jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
# 		payload = jwt_payload_handler(user)
# 		token = jwt_encode_handler(payload)
# 		logging.info(token)

# 		#return HttpResponse({'msg':'Login successful', 'token': token, 'is_login_success': True}, status=status.HTTP_200_OK)
# 		#request.session['access_token'] = token
# 		url = "http://localhost:4200/login?access_token=" + token
# 		logging.info(url)
# 		return redirect(url)
# 	else:
# 		return HttpResponse({'msg': 'Credentials are not valid!'}, status=status.HTTP_400_BAD_REQUEST)


