from django.db import models


class user_auth_info(models.Model):
    username = models.CharField(max_length=2000)
    userkey = models.CharField(max_length=4000, primary_key=True)
    timestamp_key = models.DateTimeField()
    machine_info = models.CharField(max_length=4000)
    request_params = models.CharField(max_length=4000)

    class Meta:
        db_table = 'user_auth_info'