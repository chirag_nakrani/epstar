import ldap3
 
l = ldap3.initialize("nmvikpdcsrv001.epsdc.electronicpay.in")
try:
    l.protocol_version = ldap.VERSION3
    l.set_option(ldap.OPT_REFERRALS, 0)
 
    bind = l.simple_bind_s("EPSDC.electronicpay.in", "Expo#2018")
 
    base = "CN=USERS,DC=EPSDC,DC=electronicpay,DC=in"
    criteria = "(&(objectClass=user)(sAMAccountName=username))"
    attributes = ['displayName', 'company']
    result = l.search_s(base, ldap.SCOPE_SUBTREE, criteria, attributes)
 
    results = [entry for dn, entry in result if isinstance(entry, dict)]
    print(results)
finally:
    l.unbind()