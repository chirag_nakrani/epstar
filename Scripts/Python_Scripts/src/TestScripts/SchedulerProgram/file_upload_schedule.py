import requests
import sys
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def file_upload_schedule():
    pdf_api = requests.post('http://localhost:8000/file_upload_schedule/',headers={'Content-Type': 'application/json'})
    print(pdf_api.content)
    return pdf_api

if __name__ == '__main__':
    file_upload_schedule()