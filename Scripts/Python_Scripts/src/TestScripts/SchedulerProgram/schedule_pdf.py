import requests
import sys
import json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def schedule_pdf(request):

    request_data = json.loads(request)
    pdf_api = requests.post('http://localhost:8000/schedule_indent_pdf/',json=request_data,headers={'Content-Type': 'application/json'})
    print(pdf_api.content)
    message = MIMEMultipart()
    message['From'] = 'epstar@electronicpay.in'
    message['To'] = 'eps@exponentiadata.com'
    message['Subject'] = 'Indent PDF generated'
    body = 'Hello,\n\n Indent Pdf generated successfully through schedule program.'
    message.attach(MIMEText(body))
    try:
        MAILHOST = "smtp.office365.com"
        MAILPORT = 587
        mail_sender = "epstar@electronicpay.in"
        mail_password = "Ruh75373"
        #### smtp configuration for mail ####
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        recipients = []
        recipients.append(message['To'])
        server.sendmail(mail_sender, recipients, message.as_string())
        server.close()
    except Exception as e:
        raise e
    return pdf_api

if __name__ == '__main__':
    request = sys.argv[1]
    schedule_pdf(request=request)
