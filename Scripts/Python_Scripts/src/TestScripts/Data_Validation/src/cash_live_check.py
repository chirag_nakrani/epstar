import pyodbc
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import time
import json
import sys
from json2html import *
import datetime
import logging
import os

def mail_ro_user(recipient, data, ticket_id):
    MAILHOST = "smtp.office365.com"
    MAILPORT = 587
    mail_sender = "epstar@electronicpay.in"
    mail_password = "Ruh75373"
    try:
        #print(" sending mail ")
        message = MIMEMultipart('alternative')
        message['Subject'] = 'Ticket id %s generated for cash not loaded in ATM' % ticket_id
        body = MIMEText(data, 'html')
        message.attach(body)
        # message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        server.sendmail(mail_sender, recipient, message.as_string())
        logger.error('Email sent.')
        server.close()
    except Exception as e:
        raise e

def get_subject_user_data(connection_string,data_list,sub_category):
    category = "Master"
    list_returned = list()
    for data in data_list:
        temp = {}
        temp['Project'] = data[0]
        temp['Bank'] = data[1]
        temp['ATM ID'] = data[2]
        temp['Site Code'] = data[3]
        temp['Location'] = data[4]
        temp['Address'] = data[5]
        temp['Tech Live Date'] = data[6]
        temp['Cash Live Date'] = data[7]
        list_returned.append(temp)
    logger.error('List created for no cash in atm %s' %(list_returned))
    #print(json.dumps(list_returned,default=str))
    emails = """
            select distinct a.email from auth_user a, workflow_master b
            where b.category_id = 
		                (select category_id from ticket_category_master 
		                where category = """ + "'" + category + "'" + """ and sub_category = """  + "'" + sub_category + "'" + """)
            and a.id = b.assigned_to_user
    """
    conn = pyodbc.connect(connection_string)
    crsr = conn.cursor()
    try:
        crsr.execute(emails)
        mails_found = crsr.fetchall()
        recipients = set()
        cash_admin_mails = """
                            select email from auth_user where id in (
                            select user_id from auth_user_groups where group_id =  
                            (select id from auth_group where name = 'cash_admin'))
                            """
        cash_ops_mails = """
                                    select email from auth_user where id in (
                                    select user_id from auth_user_groups where group_id =  
                                    (select id from auth_group where name = 'Cash_Ops_Head'))
                                    """
        crsr.execute(cash_admin_mails)
        cash_admin_mails = crsr.fetchall()
        if cash_admin_mails:
            for mail_tuple in cash_admin_mails:
                recipients.add(mail_tuple[0])
        crsr.execute(cash_ops_mails)
        cash_ops_mails = crsr.fetchall()
        if cash_ops_mails:
            for mail_tuple in cash_ops_mails:
                recipients.add(mail_tuple[0])
        if mails_found:
            for mail in mails_found[0]:
                recipients.add(mail)
            str_data = json.dumps(list_returned,default=str)
            #data = json2html.convert(json = {"data": list_returned})
            data = json2html.convert(json=str_data)
            logger.error('Emails:::%s' %recipients)
            return (recipients,data)
        else:
            pass
    except Exception as e:
        raise e


def check_cash_master(connection_string):
    conn = pyodbc.connect(connection_string)
    crsr = conn.cursor()
    sql_str = "select * from (select project_id,bank_code,atm_id,site_code,location_name,site_address_line_1,TRY_CAST(tech_live_date as datetime) as tech_live_date, cash_live_date,lower(site_status) as site_status,record_status,created_on as created_date, created_by as created_user  from atm_master  where tech_live_date IS NOT NULL and datediff(day,tech_live_date,getdate()) > 3 and cash_live_date IS NULL) t1 where tech_live_date IS NOT NULL and site_status = 'active' and record_status='Active'";
    try:
        crsr.execute(sql_str)
        pending_records_found = crsr.fetchall()
        if pending_records_found:
            (users,data) = get_subject_user_data(connection_string,pending_records_found,"ATM")
            ticket_id = create_ticket(connection_string)
            logger.error('Ticket id: %s'%ticket_id)
            # mail_ro_user(users,data,ticket_id)

        status_dict = {
            "status_code" : "S100",
            "status_text" : "Data fetched and mailed successfully"
        }
        return Response(status=status.HTTP_201_CREATED, data=status_dict)
    except Exception as e:
        status_dict["level"] = masterconf.system_level
        status_dict["status_code"] = 'E500'
        status_dict["status_desc"] = 'Internal server error.'
        return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=status_dict)

def create_ticket(connection_string):
    created_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    conn = pyodbc.connect(connection_string)
    crsr = conn.cursor()
    try:
        admin_user_query = "SELECT [auth_user].[username] FROM [auth_user] where [auth_user].[is_superuser] = 1"
        crsr.execute(admin_user_query)
        admin_user_fetch = crsr.fetchall()
        created_by = admin_user_fetch[0][0]
    except Exception as e:
        created_by = ''
    created_reference_id = generate_ref_id(connection_string)
    category = 'Master'
    sub_category = 'ATM'
    try:
        category_id_query = "SELECT [ticket_category_master].[category_id] FROM [ticket_category_master] WHERE ([ticket_category_master].[category] = 'Master' AND [ticket_category_master].[sub_category] = 'ATM')"
        crsr.execute(category_id_query)
        category_id_fetch = crsr.fetchall()
        category_id = category_id_fetch[0][0]
        is_user_for_cat_id_query = "SELECT * FROM [workflow_master] WHERE [workflow_master].[category_id] = 1"
        crsr.execute(is_user_for_cat_id_query)
        is_user_for_cat_id_fetch = crsr.fetchall()
        is_user_for_cat_id = is_user_for_cat_id_fetch[0]
        if is_user_for_cat_id:
            assigned_to_user_query = "SELECT [workflow_master].[assigned_to_user] FROM [workflow_master] WHERE [workflow_master].[category_id] = 1"
            crsr.execute(assigned_to_user_query)
            assigned_to_user_fetch = crsr.fetchall()
            assigned_to_group_query = "SELECT [workflow_master].[assigned_to_group] FROM [workflow_master] WHERE [workflow_master].[category_id] = 1"
            crsr.execute(assigned_to_group_query)
            assigned_to_group_fetch = crsr.fetchall()
            assigned_to_user = assigned_to_user_fetch[0][0]
            assigned_to_group = assigned_to_group_fetch[0][0]
        else:
            category_id = ''
            admin_user_query = "SELECT * FROM [auth_user] where [auth_user].[is_superuser] = 1"
            crsr.execute(admin_user_query)
            admin_user_fetch = crsr.fetchall()
            assigned_to_user = admin_user_fetch[0]['id']
            assigned_to_group = ''  # Group.objects.get(user = assigned_to_user).id
        data_dict = {
            'subject' : 'Cash not present in ATM',
            'category_id' : category_id,
            'status' : 'open',  #Status Updated
            'assign_to_user' : assigned_to_user,
            'assign_to_group' : assigned_to_group,
            'created_on' : created_on,
            'created_by' : created_by,
            'created_reference_id' : created_reference_id,
            'remarks' : 'Please load cash in the atm for the site_code sent through email.',
            'record_status' : 'Active' #Record Status Added
        }
        tckt_mstr = "insert into ticket_master(subject,category_id,status,assign_to_user,assign_to_group,created_on,created_by,created_reference_id,remarks,record_status) values('%s',%s,'%s',%s,%s,'%s','%s','%s','%s','%s')" %(data_dict['subject'],data_dict['category_id'],data_dict['status'],data_dict['assign_to_user'],data_dict['assign_to_group'],data_dict['created_on'],data_dict['created_by'],data_dict['created_reference_id'],data_dict['remarks'],data_dict['record_status'])
        crsr.execute(tckt_mstr)
        conn.commit()
        ticket_id_query = "SELECT @@Identity"
        crsr.execute(ticket_id_query)
        ticket_id = crsr.fetchone()
        return ticket_id[0]
    except Exception as e:
        logger.setLevel(logging.ERROR)
        logger.error('EXCEPTION IN create_ticket METHOD', exc_info=True)
        raise e

def generate_ref_id(connection_string):
    try:
        conn = pyodbc.connect(connection_string)
        sql_str = "exec generate_ref_id "
        #print("sql_str from generate_ref_id : {}".format(sql_str))
        cur = conn.cursor()
        cur.execute(sql_str)
        ref_id = cur.fetchone()
        #print("Generated Ref Id ::: ", ref_id)
        cur.close()
        return ref_id[0]
    except Exception as e:
        raise e

def check_pending_records():

    if sys.argv[1] != None:
        if sys.argv[1] == 'Sit':
            connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=epstar;UID=sa;PWD=Mysql@123'
        elif sys.argv[1] == 'Uat':
            connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.208.17;DATABASE=epstar;UID=sqladmin;PWD=Mysql@123'
        elif sys.argv[1] == 'Dev':
            connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=127.0.0.1;DATABASE=epstar;UID=sa;PWD=Mysql@123'
        else:
            connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.0.117;DATABASE=epstar_region;UID=sa;PWD=Mysql@123'
    else:
        connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=epstar_uat;UID=sa;PWD=Mysql@123'
    cash_check = check_cash_master(connection_string)
    return cash_check

if __name__ == '__main__':
    d=datetime.datetime.now()
    date = d.strftime('%d_%m_%Y')
    Format = "%(asctime)-15s %(message)s"
    BaseFolder = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    LogFolder = BaseFolder+'\\'+'FileFolder'+'\\'+'LogFolder'+'\\'+'DiversionStatus'
    if not os.path.exists(LogFolder):
        os.makedirs(LogFolder)
    logfile_name = LogFolder+'\\'+'cash_live_error_log_%s' % (date)
    log_file_created = logfile_name+'.log'
    logging.basicConfig(level = logging.ERROR,format = Format,datefmt='%d-%m-%Y %H:%M:%S',filename=log_file_created)
    logger = logging.getLogger('logfile')
    check_pending_records()
