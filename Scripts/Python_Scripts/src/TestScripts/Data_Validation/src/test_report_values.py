import pandas as pd
import pyodbc
import sys

def report_diff(x):
    print (x[0] if x[0] == x[1] else '{} ---> {}'.format(*x))
    return x[0] if x[0] == x[1] else '{} ---> {}'.format(*x)

def has_change(row):
    if "--->" in row.to_string():
        return "Y"
    else:
        return "N"

def test_values_with_excel_sql(sql_data, excel_data):
    sql_data.sort_values(by=['indent_order_number'])
    detail_values = sql_data.sort_index(axis=0)
    excel_data.sort_values(by=['indent_order_number'])
    excel_data = excel_data.sort_index()
    diff_panel = pd.Panel(dict(df1=detail_values, df2=excel_data))
    diff_output = diff_panel.apply(report_diff, axis=0)
    diff_output.to_excel('output_format.xlsx')
    return True

def read_excel(excel_file):
    # python testing_indent_report_values.py sbi kanjurmarg "C:/Users/106/Downloads/BOB(MOF  MS) PLANNING SHEET -31st July-2018.xlsm"

    excel_Data = pd.read_excel(excel_file)
    return excel_Data

def read_sql(bank,feeder_branch):
    sql_conn = pyodbc.connect(
        'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=epstar;UID=sa;PWD=Mysql@123')

    query_sql = "SELECT indent_detail.indent_order_number,indent_detail.atm_id,indent_detail.location,indent_detail.purpose," \
                   "indent_detail.loading_amount_100,indent_detail.loading_amount_200,indent_detail.loading_amount_500,indent_detail.loading_amount_2000," \
                   "indent_detail.total,indent_master.indent_type,indent_master.bank_id,indent_master.project_id,indent_master.cra,indent_master.feeder_branch," \
                   "indent_master.total_atm_loading_amount,indent_master.cra_opening_vault_balance,indent_master.total_bank_withdrawal_amount," \
                   "indent_master.cypher_code,indent_master.indent_order_date,indent_master.collection_date," \
                   "indent_master.replenishment_date,indent_master.total_atm_loading_amount_100,indent_master.total_atm_loading_amount_200," \
                   "indent_master.total_atm_loading_amount_500,indent_master.total_atm_loading_amount_2000,indent_master.cra_opening_vault_balance_100," \
                   "indent_master.cra_opening_vault_balance_200,indent_master.cra_opening_vault_balance_500,indent_master.cra_opening_vault_balance_2000," \
                   "indent_master.total_bank_withdrawal_amount_100,indent_master.total_bank_withdrawal_amount_200,indent_master.total_bank_withdrawal_amount_500," \
                   "indent_master.total_bank_withdrawal_amount_2000,indent_master.amount_in_words," \
                   "indent_master.created_on FROM indent_detail inner join indent_master on indent_detail.indent_order_number = indent_master.indent_order_number " \
                   "where indent_master.bank_id='%s' and indent_master.feeder_branch='%s'" %(bank,feeder_branch)

    df_sql = pd.read_sql(query_sql, sql_conn)
    return df_sql

if __name__ == '__main__':
    bank = sys.argv[1]
    feeder_branch = sys.argv[2]
    excel_file = sys.argv[3]
    excel_data = None
    try:
        sql_data = sql_values = read_sql(bank,feeder_branch)
    except Exception as exc:
        raise exc
    try:
        if excel_file != '':
            excel_data = read_excel(excel_file)
        else:
            print('excel file not provided.')
    except Exception as exc:
        raise exc

    try:
        test_values_with_excel_sql(sql_data,excel_data)
    except Exception as exc:
        raise exc