import glob
from datetime import datetime
import pandas as pd
import xlrd
import sys
import os
import logging

def read_file(f):

    wb = xlrd.open_workbook(f)
    df = pd.read_excel(wb, engine='xlrd', header=[0],skiprows=[1],index_col=None)
    # exit()
    column_list = []
    logger.error('Columns in file %s are ---- %s'%(f,df.columns))
    for col in df.columns:
        if col == 'ATM Machine Error 1':
            col = col.replace('ATM Machine Error 1','Err ')
        if col == 'ATM Machine Error 2':
            col = col.replace('ATM Machine Error 2','Err .1')
        if col == 'ATM Machine Error 3':
            col = col.replace('ATM Machine Error 3','Err .2')
        if col == 'ATM Machine Error 4':
            col = col.replace('ATM Machine Error 4','Err .3')
        if col == 'ATM Machine Error 5':
            col = col.replace('ATM Machine Error 5','Err .4')
        if col == 'ATM Machine Error 6':
            col = col.replace('ATM Machine Error 6','Err .5')
        if col == 'ATM Machine Error 7':
            col = col.replace('ATM Machine Error 7','Err .6')
        if col == 'ATM Machine Error 8':
            col = col.replace('ATM Machine Error 8','Err .7')
        if col == 'Cash Balance':
            col = col.replace('Cash Balance', 'Hop1')
        if col == 'Rupee Note':
            col = col.replace('Rupee Note', 'Hop1.1')
        if col == 'Cash Balance.1':
            col = col.replace('Cash Balance.1', 'Hop2')
        if col == 'Rupee Note.1':
            col = col.replace('Rupee Note.1', 'Hop2.1')
        if col == 'Cash Balance.2':
            col = col.replace('Cash Balance.2', 'Hop3')
        if col == 'Rupee Note.2':
            col = col.replace('Rupee Note.2', 'Hop3.1')
        if col == 'Cash Balance.3':
            col = col.replace('Cash Balance.3', 'Hop4')
        if col == 'Rupee Note.3':
            col = col.replace('Rupee Note.3', 'Hop4.1')
        if col == 'ATM Location Name':
            col = col.replace('ATM Location Name', 'Term')
        if col == 'Total Cash Balance':
            col = col.replace('Total Cash Balance', 'End ')
        if col == 'City':
            col = col.replace('City', 'Term.1')

        column_list.append(col)
    if column_list != []:
        df.columns = column_list
    # df.columns = df.columns.map(lambda h: '{} {}'.format(str(h[0]).strip(), str(h[1]).strip()) if h[1] != '' else '{}'.format(str(h[0]).strip()))
    fields_list = canara_bank_keys
    columns_not_included = list()
    for column in df.columns.values:
        if column not in fields_list:
            del df[column]
    i = 0
    for column in fields_list:
        i+=1
        if column not in df.columns.values:

            df.insert(i-1,column,None)
            # df[str(column)] = None
    logger.error('Columns changed in file %s to %s'%(f,df.columns))
    return df

def get_file():
    new_df = pd.DataFrame()
    for f in glob.glob(InputFolder+'\\'+current_date+'\\*.xlsx'):
        logger.error('Current File %s in directory %s' %(f,current_date))
        single_df = read_file(f)
        new_df = pd.concat([new_df,single_df],ignore_index=True,sort=False)
    return new_df

if __name__ == '__main__':
    ##### canara bank column keys ########
    d=datetime.now()
    date = d.strftime('%d_%m_%Y_%H_%M_%S')
    Format = "%(asctime)-15s %(message)s"
    current_date = datetime.strftime(datetime.now(), '%Y-%m-%d')
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    InputFolder = os.path.join(BASE_DIR,'canara')

    logfile_name = 'error_log-%s'%(date)
    log_file_created = InputFolder+'/'+current_date+'/'+logfile_name+'.log'
    logging.basicConfig(level = logging.ERROR,format = Format,datefmt='%d-%m-%Y %H:%M:%S',filename=log_file_created)
    logger = logging.getLogger('logfile')

    canara_bank_keys = ['ATM ', 'Term', 'Term.1', 'Regn', 'Network Link Status', 'ATM Status','Out ', 'Err ', 'Err .1', 'Err .2', 'Err .3', 'Err .4', 'Err .5','Err .6', 'Err .7', 'End ','Last', 'Last.1', 'Hop1', 'Hop1.1', 'Hop2', 'Hop2.1', 'Hop3','Hop3.1', 'Hop4', 'Hop4.1','Hop5', 'TIMESTAMP']
    canara_bank_keys_first_row = ['ATM','Term','Term','Regn','Network Link Status','ATM Status','Out','Err','Err','Err','Err','Err','Err','Err','Err','End','Last','Last','Hop1','Hop1','Hop2','Hop2','Hop3','Hop3','Hop4','Hop4','Hop5','TIMESTAMP']
    canara_bank_keys_second_row = ['ID','Loc','City','Id','','','Time','1','2','3','4','5','6','7','8','Cash','Tran','Up','Cash','Bill','Cash','Bill','Cash','Bill','Cash','Bill','Cash','']
    current_date_time = datetime.strftime(datetime.now(), '%Y-%m-%d_%H_%M_%S')
    merged = get_file()
    merged.loc[0] = canara_bank_keys_second_row
    merged.index = merged.index + 1
    merged.sort_index(inplace=True)
    merged.loc[0] = canara_bank_keys_first_row
    merged.index = merged.index + 1
    merged.sort_index(inplace=True)

    consolidated_file_name = InputFolder+'\\'+current_date+'\\new_canara_bank_file_'+current_date_time+'.xls'
    excel_file = merged.to_excel(consolidated_file_name,index=False,header=None)
    logger.error('Consolidated file created with name %s'%(consolidated_file_name))

