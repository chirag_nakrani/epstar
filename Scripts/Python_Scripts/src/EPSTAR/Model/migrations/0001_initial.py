# Generated by Django 2.0.7 on 2018-10-26 07:18

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='cash_balance_file_alb',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('group_name', models.CharField(blank=True, max_length=50, null=True)),
                ('termid', models.CharField(blank=True, max_length=50, null=True)),
                ('location', models.CharField(blank=True, max_length=100, null=True)),
                ('currbalcass1', models.IntegerField(blank=True, null=True)),
                ('ccurrbalcass2', models.IntegerField(blank=True, null=True)),
                ('currbalcass3', models.IntegerField(blank=True, null=True)),
                ('currbalcass4', models.IntegerField(blank=True, null=True)),
                ('project_id', models.CharField(blank=True, max_length=50, null=True)),
                ('record_status', models.CharField(blank=True, max_length=50, null=True)),
                ('created_on', models.DateTimeField(blank=True, null=True)),
                ('created_by', models.CharField(blank=True, max_length=50, null=True)),
                ('created_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('datafor_date_time', models.DateTimeField(blank=True, null=True)),
                ('approved_on', models.DateTimeField(blank=True, null=True)),
                ('approved_by', models.CharField(blank=True, max_length=50, null=True)),
                ('approved_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('rejected_on', models.DateTimeField(blank=True, null=True)),
                ('rejected_by', models.CharField(blank=True, max_length=50, null=True)),
                ('reject_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('reject_trigger_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('deleted_on', models.DateTimeField(blank=True, null=True)),
                ('deleted_by', models.CharField(blank=True, max_length=50, null=True)),
                ('deleted_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('modified_on', models.DateTimeField(blank=True, null=True)),
                ('modified_by', models.CharField(blank=True, max_length=50, null=True)),
                ('modified_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('isvalidrecord', models.CharField(blank=True, db_column='isValidRecord', max_length=10, null=True)),
            ],
            options={
                'db_table': 'cash_balance_file_alb',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='cash_balance_register',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('datafor_date_time', models.DateTimeField(blank=True, null=True)),
                ('bank_name', models.CharField(blank=True, max_length=50, null=True)),
                ('atm_id', models.CharField(blank=True, max_length=50, null=True)),
                ('remaining_balance_50', models.BigIntegerField(blank=True, null=True)),
                ('remaining_balance_100', models.BigIntegerField(blank=True, null=True)),
                ('remaining_balance_200', models.BigIntegerField(blank=True, null=True)),
                ('remaining_balance_500', models.BigIntegerField(blank=True, null=True)),
                ('remaining_balance_2000', models.BigIntegerField(blank=True, null=True)),
                ('total_remaining_balance', models.BigIntegerField(blank=True, null=True)),
                ('calculated_remaining_balance_amount', models.BigIntegerField(blank=True, null=True)),
                ('is_total_remaining_matched_with_calculated', models.NullBooleanField()),
                ('exception_trigger_id', models.CharField(blank=True, max_length=255, null=True)),
                ('project_id', models.CharField(blank=True, max_length=50, null=True)),
                ('record_status', models.CharField(blank=True, max_length=20, null=True)),
                ('isvalidrecord', models.CharField(blank=True, db_column='isValidRecord', max_length=20, null=True)),
                ('created_on', models.DateTimeField(blank=True, null=True)),
                ('created_by', models.CharField(blank=True, max_length=50, null=True)),
                ('created_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('approved_on', models.DateTimeField(blank=True, null=True)),
                ('approved_by', models.CharField(blank=True, max_length=50, null=True)),
                ('approved_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('deleted_on', models.DateTimeField(blank=True, null=True)),
                ('deleted_by', models.CharField(blank=True, max_length=50, null=True)),
                ('deleted_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('modified_on', models.DateTimeField(blank=True, null=True)),
                ('modified_by', models.CharField(blank=True, max_length=50, null=True)),
                ('modified_reference_id', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': 'cash_balance_register',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='user_bank_mapping',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('user_name', models.CharField(blank=True, max_length=50, null=True)),
                ('bank_id', models.CharField(blank=True, max_length=50, null=True)),
                ('created_on', models.DateTimeField(blank=True, null=True)),
                ('created_by', models.CharField(blank=True, max_length=50, null=True)),
                ('created_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('approved_on', models.DateField(blank=True, null=True)),
                ('approved_by', models.CharField(blank=True, max_length=50, null=True)),
                ('approved_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('rejected_on', models.DateField(blank=True, null=True)),
                ('rejected_by', models.CharField(blank=True, max_length=50, null=True)),
                ('approve_reject_comment', models.CharField(blank=True, max_length=500, null=True)),
                ('deleted_on', models.DateTimeField(blank=True, null=True)),
                ('deleted_by', models.CharField(blank=True, max_length=50, null=True)),
                ('deleted_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('modified_on', models.DateTimeField(blank=True, null=True)),
                ('modified_by', models.CharField(blank=True, max_length=50, null=True)),
                ('last_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('record_status', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': 'user_bank_mapping',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='user_project_mapping',
            fields=[
                ('id', models.BigAutoField(primary_key=True, serialize=False)),
                ('user_name', models.CharField(blank=True, max_length=50, null=True)),
                ('project_id', models.CharField(blank=True, max_length=50, null=True)),
                ('created_on', models.DateTimeField(blank=True, null=True)),
                ('created_by', models.CharField(blank=True, max_length=50, null=True)),
                ('created_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('approved_on', models.DateField(blank=True, null=True)),
                ('approved_by', models.CharField(blank=True, max_length=50, null=True)),
                ('approved_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('rejected_on', models.DateField(blank=True, null=True)),
                ('rejected_by', models.CharField(blank=True, max_length=50, null=True)),
                ('approve_reject_comment', models.CharField(blank=True, max_length=500, null=True)),
                ('deleted_on', models.DateTimeField(blank=True, null=True)),
                ('deleted_by', models.CharField(blank=True, max_length=50, null=True)),
                ('deleted_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('modified_on', models.DateTimeField(blank=True, null=True)),
                ('modified_by', models.CharField(blank=True, max_length=50, null=True)),
                ('last_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('record_status', models.CharField(blank=True, max_length=50, null=True)),
            ],
            options={
                'db_table': 'user_project_mapping',
                'managed': False,
            },
        ),
        migrations.CreateModel(
            name='cash_balance_file_bomh',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('term_id', models.CharField(max_length=15, null=True)),
                ('acceptorname', models.CharField(max_length=255, null=True)),
                ('location', models.CharField(max_length=100, null=True)),
                ('tot_cash', models.IntegerField(null=True)),
                ('cassette1', models.IntegerField(null=True)),
                ('cassette2', models.IntegerField(null=True)),
                ('cassette3', models.IntegerField(null=True)),
                ('cassette4', models.IntegerField(null=True)),
                ('project_id', models.CharField(max_length=50, null=True)),
                ('record_status', models.CharField(max_length=50, null=True)),
                ('created_on', models.DateTimeField(null=True)),
                ('created_by', models.CharField(max_length=50, null=True)),
                ('created_reference_id', models.CharField(max_length=50, null=True)),
                ('datafor_date_time', models.DateTimeField(null=True)),
                ('approved_on', models.DateTimeField(null=True)),
                ('approved_by', models.CharField(max_length=50, null=True)),
                ('approved_reference_id', models.CharField(max_length=50, null=True)),
                ('rejected_on', models.DateTimeField(null=True)),
                ('rejected_by', models.CharField(max_length=50, null=True)),
                ('reject_reference_id', models.CharField(max_length=50, null=True)),
                ('reject_trigger_reference_id', models.CharField(max_length=50, null=True)),
                ('deleted_on', models.DateTimeField(null=True)),
                ('deleted_by', models.CharField(max_length=50, null=True)),
                ('deleted_reference_id', models.CharField(max_length=50, null=True)),
                ('modified_on', models.DateTimeField(null=True)),
                ('modified_by', models.CharField(max_length=50, null=True)),
                ('modified_reference_id', models.CharField(max_length=50, null=True)),
                ('isValidRecord', models.CharField(max_length=10, null=True)),
            ],
            options={
                'db_table': 'cash_balance_file_bomh',
            },
        ),
        migrations.CreateModel(
            name='user_auth_info',
            fields=[
                ('username', models.CharField(max_length=2000)),
                ('userkey', models.CharField(max_length=4000, primary_key=True, serialize=False)),
                ('timestamp_key', models.DateTimeField()),
                ('machine_info', models.CharField(max_length=4000)),
                ('request_params', models.CharField(max_length=4000)),
            ],
            options={
                'db_table': 'user_auth_info',
            },
        ),
    ]
