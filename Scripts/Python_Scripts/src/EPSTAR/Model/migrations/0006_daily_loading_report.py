# Generated by Django 2.0.7 on 2018-10-30 16:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Model', '0005_vault_cash_balance'),
    ]

    operations = [
        migrations.CreateModel(
            name='Daily_Loading_Report',
            fields=[
                ('id', models.BigAutoField(db_column='Id', primary_key=True, serialize=False)),
                ('date', models.DateTimeField(blank=True, db_column='Date', null=True)),
                ('sr_no', models.IntegerField(blank=True, db_column='Sr_No', null=True)),
                ('atm_id', models.CharField(blank=True, db_column='ATM_ID', max_length=20, null=True)),
                ('location', models.CharField(blank=True, db_column='Location', max_length=500, null=True)),
                ('feeder_branch', models.CharField(blank=True, db_column='Feeder_Branch', max_length=100, null=True)),
                ('cra', models.CharField(blank=True, db_column='CRA', max_length=50, null=True)),
                ('district', models.CharField(blank=True, db_column='District', max_length=50, null=True)),
                ('state', models.CharField(blank=True, db_column='State', max_length=50, null=True)),
                ('bank_name', models.CharField(blank=True, db_column='Bank', max_length=50, null=True)),
                ('planning_100', models.IntegerField(blank=True, db_column='Planning_100', null=True)),
                ('planning_200', models.IntegerField(blank=True, db_column='Planning_200', null=True)),
                ('planning_500', models.IntegerField(blank=True, db_column='Planning_500', null=True)),
                ('planning_2000', models.IntegerField(blank=True, db_column='Planning_2000', null=True)),
                ('planning_total', models.IntegerField(blank=True, db_column='Planning_Total', null=True)),
                ('revised_planning_100', models.IntegerField(blank=True, db_column='Revised_Planning_100', null=True)),
                ('revised_planning_200', models.IntegerField(blank=True, db_column='Revised_Planning_200', null=True)),
                ('revised_planning_500', models.IntegerField(blank=True, db_column='Revised_Planning_500', null=True)),
                ('revised_planning_2000', models.IntegerField(blank=True, db_column='Revised_Planning_2000', null=True)),
                ('revised_planning_total', models.IntegerField(blank=True, db_column='Revised_Planning_Total', null=True)),
                ('actual_loading_100', models.IntegerField(blank=True, db_column='Actual_Loading_100', null=True)),
                ('actual_loading_200', models.IntegerField(blank=True, db_column='Actual_Loading_200', null=True)),
                ('actual_loading_500', models.IntegerField(blank=True, db_column='Actual_Loading_500', null=True)),
                ('actual_loading_2000', models.IntegerField(blank=True, db_column='Actual_Loading_2000', null=True)),
                ('actual_loading_total', models.IntegerField(blank=True, db_column='Actual_Loading_Total', null=True)),
                ('remarks', models.CharField(blank=True, db_column='Remarks', max_length=100, null=True)),
                ('diverted_atm_id', models.IntegerField(blank=True, db_column='Diverted_ATM_ID', null=True)),
                ('project_id', models.CharField(blank=True, max_length=50, null=True)),
                ('record_status', models.CharField(blank=True, max_length=50, null=True)),
                ('created_on', models.DateTimeField(blank=True, null=True)),
                ('created_by', models.CharField(blank=True, max_length=50, null=True)),
                ('created_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('datafor_date_time', models.DateTimeField(blank=True, null=True)),
                ('approved_on', models.DateTimeField(blank=True, null=True)),
                ('approved_by', models.CharField(blank=True, max_length=50, null=True)),
                ('approved_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('deleted_on', models.DateTimeField(blank=True, null=True)),
                ('deleted_by', models.CharField(blank=True, max_length=50, null=True)),
                ('deleted_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('modified_on', models.DateTimeField(blank=True, null=True)),
                ('modified_by', models.CharField(blank=True, max_length=50, null=True)),
                ('modified_reference_id', models.CharField(blank=True, max_length=50, null=True)),
                ('isvalidrecord', models.CharField(blank=True, db_column='IsValidRecord', max_length=20, null=True)),
            ],
            options={
                'db_table': 'Daily_Loading_Report',
                'managed': False,
            },
        ),
    ]
