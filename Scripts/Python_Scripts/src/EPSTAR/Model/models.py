from django.db import models
from django.db import connection
from Common.models import app_config_param
from Common import masterconf
from Common.CommonFunctions import common_util
import logging

def validate_file_frmt(datafor,bankcode,columns):
		logger = logging.getLogger(__name__)
		try:
			common_util_obj = common_util.common_utils()
			logger.info("IN validate_file_frmt METHOD OF Model.models.py:GOING FOR FORMAT VALIDATION.")
			nocount = """ SET NOCOUNT ON; """
			#sql_str = "exec uspFormatValidation " + "'" + datafor + "', " + "'" + bankcode + "', " + "'" +columns + "' ";
			sql_str = "exec uspFormatValidation " + "'" + datafor + "', " + "'" + bankcode + "', " + "'" +columns + "' ";
			logger.info("sql_str from validate_file_frmt : %s ", sql_str)
			cur = connection.cursor()
			cur.execute(nocount+sql_str)
			frmt_seq_no = cur.fetchone()
			status_returned = app_config_param.objects.filter(sequence=int(frmt_seq_no[0])).values('value')
			cur.close()
			logger.info("status_returned from db :::: %s ", status_returned)
			return status_returned[0]['value']
		except Exception as e:
			logger.error('EXCEPTION IN validate_file_frmt METHOD OF Model.models.py', e)
			raise e

def validate_data(strfile_type,createddate,bankcode,recordstatus,referenceid,systemUser,actual_upload_time,Project_id):
		#print ("bank_code from validate_data: {}".format(bankcode))
		actual_upload_time = actual_upload_time
		Project_id = Project_id
		strfile_type_1 = ''
		#print ("strfile_type : {}".format(strfile_type))
		if strfile_type == masterconf.file_type_cbr:
			strfile_type_1 = 'balance'
		elif strfile_type == masterconf.file_type_dispense:
			strfile_type_1 = 'dispense'
		elif strfile_type == masterconf.file_type_vcb_eod:
			strfile_type_1 = 'vcb'
		elif strfile_type == masterconf.file_type_C3R_CMIS:
			strfile_type_1 = 'c3r_cmis'
		elif strfile_type == 'Daily_Loading_Report':
			table_name = 'Daily_Loading_Report'
		else:
			table_name = ''
		if strfile_type_1 == 'balance' or strfile_type_1 == 'dispense':
			table_name = "cash_"+strfile_type_1+"_file_"+bankcode.lower()
		elif strfile_type_1 == 'vcb':
			table_name = 'vault_cash_balance'
		elif strfile_type_1 == 'c3r_cmis':
			table_name = 'c3r_CMIS'

		#print ("tablename generated from validate_data: {}".format(table_name))
		try:
			#print("In validate_data")
			if strfile_type == masterconf.file_type_cbr:
				#print ("strfile_type : {}".format(strfile_type))
				procedure_name1 = 'uspValidateCbrFiles'
			elif strfile_type == masterconf.file_type_dispense:
				#print ("strfile_type : {}".format(strfile_type))
				procedure_name1 = 'uspValidateDispenseFiles'
			elif strfile_type == masterconf.file_type_vcb_eod:
				procedure_name1 = 'uspDataValidationVCB'
			elif strfile_type == masterconf.file_type_C3R_CMIS:
				procedure_name1 = 'uspValidateC3RFiles'
				strfile_type = 'C3R'
			elif strfile_type == masterconf.file_type_Daily_Loading_Report:
				procedure_name1 = 'uspDataValidation_DailyLoadingRprt'

			nocount = """ SET NOCOUNT ON; """
			sql_str = "exec " + procedure_name1 + " " +"'" + actual_upload_time + "', " + "'" + bankcode + "', " + "'" + Project_id + "', " + "'" + recordstatus + "', " + "'" + referenceid + "'" + ", " + "'" + systemUser + "'"
			#print("sql_str for  uspValidateCbrFiles or uspValidateDispenseFiles: {}".format(sql_str))
			cur = connection.cursor()
			#print("Calling data sp")
			cur.execute(nocount + sql_str)
			data_seq_no = cur.fetchone()
			#print ("data_seq_no: {}".format(data_seq_no))
			#print ("procudure1 executed")
			data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
			#print("data_status : {}".format(data_status))
			#print("data_status_value : {}".format(data_status[0]['value']))
			return data_status[0]['value']
			# if (data_status[0]['value'] != 'Data Validation Failed'):
			# 	#print ("Going for uspSetStatus")
			# #sql_str2 = "DECLARE @out varchar(max)EXEC uspSetStatus 'data_update_log','Approval Pending','Approved','2018-10-11 19:25:58.000','Mostaque','C1234567891', ' data_for_datetime = ''2018-10-11 08:00:00.000'' and project_id = ''MOF'' ',@out OUTPUT select @out"
			# #sql_str2 = "DECLARE @out varchar(max)EXEC "+procedure_name3+" 'data_update_log','Approval Pending','Approved','"+createddate+"','"+systemUser+"','"+referenceid+"','data_for_datetime = ''"+actual_upload_time+"'' and data_for_tyoe =''"+dataand project_id = ''"+Project_id+"'' ',@out OUTPUT select @out"
			# 	sql_str2 = "DECLARE @out varchar(max)EXEC "+procedure_name3+" 'data_update_log','Approval Pending','Approved','"+createddate+"','"+systemUser+"','"+referenceid+"','datafor_date_time = ''"+actual_upload_time+"'' and data_for_type = ''"+strfile_type+"'' and project_id = ''"+Project_id+"'' ',@out OUTPUT select @out"
			# 	cur.execute(nocount + sql_str2)
			# 	data_seq_no = cur.fetchone()
			# 	#print ("#printing sql_str2 1: {}".format(sql_str2))
			# 	#print ("data_seq_no: {}".format(data_seq_no))
			# 	#print ("Procudure 3 is running")
			# 	data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
			# 	#print("data_status : {}".format(data_status))
			# 	#print("data_status_value : {}".format(data_status[0]['value']))
			# 	#print ("Again Going for uspSetStatus")
			# 	#sql_str2 = "DECLARE @out varchar(max)EXEC uspSetStatus 'cash_balance_file_alb','Approval Pending','Approved','2018-10-11 19:25:58.000','Mostaque','C1234567891', ' datafor_date_time = ''2018-10-11 08:00:00.000'' and project_id = ''MOF'' ',@out OUTPUT select @out"
			# 	sql_str2 = "DECLARE @out varchar(max)EXEC "+procedure_name3+" '"+table_name+"','Approval Pending','Approved','"+createddate+"','"+systemUser+"','"+referenceid+"',' datafor_date_time = ''"+actual_upload_time+"'' and project_id = ''"+Project_id+"'' ',@out OUTPUT select @out"
			# 	#print ("#printing sql_str2 2: {}".format(sql_str2))
			# 	cur.execute(nocount + sql_str2)
			# 	data_seq_no = cur.fetchone()
			# 	#print ("data_seq_no: {}".format(data_seq_no))
			# 	data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
			# 	#print("data_status : {}".format(data_status))
			# 	#print("data_status_value : {}".format(data_status[0]['value']))
			# 	if strfile_type == 'C3R':
			# 		table_name = 'c3r_VMIS'
			# 		#print ("Now Running procedure 2 for c3r_VMIS")
			# 		sql_str2 = "DECLARE @out varchar(max)EXEC "+procedure_name3+" '"+table_name+"','Approval Pending','Approved','"+createddate+"','"+systemUser+"','"+referenceid+"',' datafor_date_time = ''"+actual_upload_time+"'' and project_id = ''"+Project_id+"'' ',@out OUTPUT select @out"
			# 		#print ("#printing sql_str2 2: {}".format(sql_str2))
			# 		cur.execute(nocount + sql_str2)
			# 		data_seq_no = cur.fetchone()
			# 		#print ("data_seq_no: {}".format(data_seq_no))
			# 		data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
			# 		#print("data_status : {}".format(data_status))
			# 		#print("data_status_value : {}".format(data_status[0]['value']))
			# 	#print ("Going for uspConsolidateCbrFiles")
			# #sql_str3 = "EXEC uspConsolidateCbrFiles '2018-10-11 08:00:00.000','ALB','MOF','sasas','SA'"
			# #sql_str3 = "EXEC "+procedure_name2+" '"+actual_upload_time+"','"+bankcode+"','"+Project_id+"','"+referenceid+"','Mostaque'"
			# 	sql_str3 = "EXEC "+procedure_name2+" '"+actual_upload_time+"','"+bankcode+"','"+Project_id+"','"+referenceid+"','"+systemUser+"'"
			# 	#print(sql_str3)
			# 	cur.execute(nocount + sql_str3)
			# 	data_seq_no = cur.fetchone()
			# 	#print ("data_seq_no: {}".format(data_seq_no))
			# 	#print ("#printing from validate_and_consoldate_data")
			# 	data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
			# 	#print("data_status : {}".format(data_status))
			# 	#print("data_status_value : {}".format(data_status[0]['value']))
			# 	cur.close()
			# 	return data_status[0]['value']
			# else:
		except Exception as e:
			raise e;


def generate_ref_id():
	try:
		sql_str = "exec generate_ref_id "
		#print("sql_str from generate_ref_id : {}".format(sql_str))
		cur = connection.cursor()
		cur.execute(sql_str)
		ref_id = cur.fetchone()
		#print("Generated Ref Id ::: ", ref_id)
		cur.close()
		return ref_id[0]
	except Exception as e:
		raise e


class cash_balance_file_alb(models.Model):
	id = models.BigAutoField(primary_key=True)
	group_name = models.CharField(max_length=50, blank=True, null=True)
	termid = models.CharField(max_length=50, blank=True, null=True)
	location = models.CharField(max_length=100, blank=True, null=True)
	currbalcass1 = models.IntegerField(blank=True, null=True)
	ccurrbalcass2 = models.IntegerField(blank=True, null=True)
	currbalcass3 = models.IntegerField(blank=True, null=True)
	currbalcass4 = models.IntegerField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=50, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_alb'

class cash_balance_file_bob(models.Model):
	id = models.BigAutoField(primary_key=True)
	atmid = models.CharField(max_length=15, blank=True, null=True)
	location = models.CharField(max_length=100, blank=True, null=True)
	state = models.CharField(max_length=10, blank=True, null=True)
	atmstatus = models.CharField(max_length=10, blank=True, null=True)
	msvendor = models.CharField(max_length=15, blank=True, null=True)
	currentstatus = models.CharField(max_length=15, blank=True, null=True)
	regionname = models.CharField(max_length=50, blank=True, null=True)
	zone = models.CharField(max_length=50, blank=True, null=True)
	address = models.TextField(blank=True, null=True)
	hop1bill = models.IntegerField(blank=True, null=True)
	hop2bill = models.IntegerField(blank=True, null=True)
	hop3bill = models.IntegerField(blank=True, null=True)
	hop4bill = models.IntegerField(blank=True, null=True)
	hop1begcash = models.IntegerField(blank=True, null=True)
	hop2begcash = models.IntegerField(blank=True, null=True)
	hop3begcash = models.IntegerField(blank=True, null=True)
	hop4begcash = models.IntegerField(blank=True, null=True)
	hop1endcash = models.IntegerField(blank=True, null=True)
	hop2endcash = models.IntegerField(blank=True, null=True)
	hop3endcash = models.IntegerField(blank=True, null=True)
	hop4endcash = models.IntegerField(blank=True, null=True)
	hop1cashdeposit = models.IntegerField(blank=True, null=True)
	hop2cashdeposit = models.IntegerField(blank=True, null=True)
	hop3cashdeposit = models.IntegerField(blank=True, null=True)
	hop4cashdeposit = models.IntegerField(blank=True, null=True)
	begcash = models.IntegerField(blank=True, null=True)
	endcash = models.IntegerField(blank=True, null=True)
	cashout = models.IntegerField(blank=True, null=True)
	cashincr = models.IntegerField(blank=True, null=True)
	timestamp = models.DateTimeField(blank=True, null=True)
	lastwithdrawaltime = models.DateTimeField(blank=True, null=True)
	lastsupervisorytime = models.DateTimeField(blank=True, null=True)
	lastdeposittranstime = models.CharField(max_length=50, blank=True, null=True)
	lastadmintxntime = models.DateTimeField(blank=True, null=True)
	lastreversaltime = models.DateTimeField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.CharField(max_length=100, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_bob'


class cash_balance_file_boi(models.Model):
	id = models.BigAutoField(primary_key=True)
	institutionid = models.IntegerField(blank=True, null=True)
	termid = models.CharField(max_length=15, blank=True, null=True)
	location = models.CharField(max_length=100, blank=True, null=True)
	cash_start1 = models.IntegerField(blank=True, null=True)
	cash_start2 = models.IntegerField(blank=True, null=True)
	cash_start3 = models.IntegerField(blank=True, null=True)
	cash_start4 = models.IntegerField(blank=True, null=True)
	cash_inc1 = models.IntegerField(blank=True, null=True)
	cash_inc2 = models.IntegerField(blank=True, null=True)
	cash_inc3 = models.IntegerField(blank=True, null=True)
	cash_inc4 = models.IntegerField(blank=True, null=True)
	cash_dec1 = models.IntegerField(blank=True, null=True)
	cash_dec2 = models.IntegerField(blank=True, null=True)
	cash_dec3 = models.IntegerField(blank=True, null=True)
	cash_dec4 = models.IntegerField(blank=True, null=True)
	cash_out1 = models.IntegerField(blank=True, null=True)
	cash_out2 = models.IntegerField(blank=True, null=True)
	cash_out3 = models.IntegerField(blank=True, null=True)
	cash_out4 = models.IntegerField(blank=True, null=True)
	cash_currbal1 = models.IntegerField(blank=True, null=True)
	cash_currbal2 = models.IntegerField(blank=True, null=True)
	cash_currbal3 = models.IntegerField(blank=True, null=True)
	cash_currbal4 = models.IntegerField(blank=True, null=True)
	termid1 = models.CharField(max_length=15, blank=True, null=True)
	txn_date = models.DateTimeField(blank=True, null=True)
	txn_time = models.CharField(max_length=10, blank=True, null=True)
	respcode = models.CharField(max_length=50, blank=True, null=True)
	pcode = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=50, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_boi'


class cash_balance_file_bomh(models.Model):
	id = models.BigAutoField(primary_key=True)
	term_id = models.CharField(max_length=15, blank=True, null=True)
	acceptorname = models.CharField(max_length=255, blank=True, null=True)
	location = models.CharField(max_length=100, blank=True, null=True)
	tot_cash = models.IntegerField(blank=True, null=True)
	cassette1 = models.IntegerField(blank=True, null=True)
	cassette2 = models.IntegerField(blank=True, null=True)
	cassette3 = models.IntegerField(blank=True, null=True)
	cassette4 = models.IntegerField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_bomh'


class cash_balance_file_cab(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	termloc = models.CharField(max_length=100, blank=True, null=True)
	termcity = models.CharField(max_length=50, blank=True, null=True)
	regn_id = models.CharField(max_length=15, blank=True, null=True)
	network_link_status = models.CharField(max_length=10, blank=True, null=True)
	atm_status = models.CharField(max_length=10, blank=True, null=True)
	out_time = models.CharField(max_length=15, blank=True, null=True)
	err_1 = models.TextField(blank=True, null=True)
	err_2 = models.TextField(blank=True, null=True)
	err_3 = models.TextField(blank=True, null=True)
	err_4 = models.TextField(blank=True, null=True)
	err_5 = models.TextField(blank=True, null=True)
	err_6 = models.TextField(blank=True, null=True)
	err_7 = models.TextField(blank=True, null=True)
	err_8 = models.TextField(blank=True, null=True)
	hop1cash = models.IntegerField(blank=True, null=True)
	hop1bill = models.IntegerField(blank=True, null=True)
	hop2cash = models.IntegerField(blank=True, null=True)
	hop2bill = models.IntegerField(blank=True, null=True)
	hop3cash = models.IntegerField(blank=True, null=True)
	hop3bill = models.IntegerField(blank=True, null=True)
	hop4cash = models.IntegerField(blank=True, null=True)
	hop4bill = models.IntegerField(blank=True, null=True)
	end_cash = models.IntegerField(blank=True, null=True)
	timestamp = models.CharField(max_length=255, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=50, blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_cab'


class cash_balance_file_cbi(models.Model):
	id = models.BigAutoField(primary_key=True)
	sst = models.IntegerField(blank=True, null=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	cas_200 = models.IntegerField(blank=True, null=True)
	cas_100 = models.IntegerField(blank=True, null=True)
	cas_500 = models.IntegerField(blank=True, null=True)
	cas_2000 = models.IntegerField(blank=True, null=True)
	remaining = models.IntegerField(blank=True, null=True)
	location = models.CharField(max_length=100, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.CharField(max_length=100, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_cbi'


class cash_balance_file_corp(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	cash_100s = models.IntegerField(blank=True, null=True)
	cash_200s = models.IntegerField(blank=True, null=True)
	cash_500s = models.IntegerField(blank=True, null=True)
	cash_2000s = models.IntegerField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=50, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_corp'


class cash_balance_file_dena(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	stat = models.CharField(max_length=50, blank=True, null=True)
	atm_stat = models.CharField(max_length=50, blank=True, null=True)
	fiid = models.CharField(max_length=100, blank=True, null=True)
	err_2 = models.CharField(max_length=255, blank=True, null=True)
	err_3 = models.CharField(max_length=255, blank=True, null=True)
	err_4 = models.CharField(max_length=255, blank=True, null=True)
	err_5 = models.CharField(max_length=255, blank=True, null=True)
	err_6 = models.CharField(max_length=255, blank=True, null=True)
	err_7 = models.CharField(max_length=255, blank=True, null=True)
	err_8 = models.CharField(max_length=255, blank=True, null=True)
	cashchng = models.IntegerField(blank=True, null=True)
	cashout = models.IntegerField(blank=True, null=True)
	cashrepl = models.CharField(max_length=255, blank=True, null=True)
	cashsply = models.CharField(max_length=255, blank=True, null=True)
	end_cash = models.IntegerField(blank=True, null=True)
	hop4cash = models.IntegerField(blank=True, null=True)
	hop4bill = models.IntegerField(blank=True, null=True)
	hop5cash = models.IntegerField(blank=True, null=True)
	hop5bill = models.IntegerField(blank=True, null=True)
	amt_chk = models.IntegerField(blank=True, null=True)
	num_chk = models.IntegerField(blank=True, null=True)
	amt_dep = models.IntegerField(blank=True, null=True)
	num_dep = models.IntegerField(blank=True, null=True)
	intvdown = models.CharField(max_length=255, blank=True, null=True)
	flt_cnt = models.IntegerField(blank=True, null=True)
	hop1cash = models.IntegerField(blank=True, null=True)
	hop1bill = models.IntegerField(blank=True, null=True)
	hop2cash = models.IntegerField(blank=True, null=True)
	hop2bill = models.IntegerField(blank=True, null=True)
	hop3cash = models.IntegerField(blank=True, null=True)
	hop3bill = models.IntegerField(blank=True, null=True)
	linestat = models.CharField(max_length=255, blank=True, null=True)
	termloc = models.CharField(max_length=255, blank=True, null=True)
	termcity = models.CharField(max_length=255, blank=True, null=True)
	term_st = models.CharField(max_length=255, blank=True, null=True)
	termst_x = models.CharField(max_length=255, blank=True, null=True)
	ownr = models.CharField(max_length=255, blank=True, null=True)
	row_cnt = models.IntegerField(blank=True, null=True)
	src_name = models.CharField(max_length=255, blank=True, null=True)
	spvr_on = models.CharField(max_length=255, blank=True, null=True)
	brch_id = models.CharField(max_length=255, blank=True, null=True)
	regn_id = models.CharField(max_length=255, blank=True, null=True)
	timeofst = models.CharField(max_length=255, blank=True, null=True)
	atm_type = models.CharField(max_length=255, blank=True, null=True)
	vendor_name = models.CharField(max_length=255, blank=True, null=True)
	timestamp_data = models.CharField(max_length=255, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_dena'


class cash_balance_file_idbi(models.Model):
	id = models.BigAutoField(primary_key=True)
	termid = models.CharField(db_column='TERMID', max_length=255, blank=True, null=True)  # Field name made lowercase.
	tiadatetime = models.CharField(db_column='TiaDATETIME', max_length=255, blank=True, null=True)  # Field name made lowercase.
	bill1 = models.BigIntegerField(db_column='Bill1', blank=True, null=True)  # Field name made lowercase.
	bill2 = models.BigIntegerField(db_column='Bill2', blank=True, null=True)  # Field name made lowercase.
	bill3 = models.BigIntegerField(db_column='Bill3', blank=True, null=True)  # Field name made lowercase.
	bill4 = models.BigIntegerField(db_column='Bill4', blank=True, null=True)  # Field name made lowercase.
	startbalcass1 = models.BigIntegerField(db_column='STARTBALCASS1', blank=True, null=True)  # Field name made lowercase.
	startbalcass2 = models.BigIntegerField(db_column='STARTBALCASS2', blank=True, null=True)  # Field name made lowercase.
	startbalcass3 = models.BigIntegerField(db_column='STARTBALCASS3', blank=True, null=True)  # Field name made lowercase.
	startbalcass4 = models.BigIntegerField(db_column='STARTBALCASS4', blank=True, null=True)  # Field name made lowercase.
	cashinccass1 = models.BigIntegerField(db_column='CASHINCCASS1', blank=True, null=True)  # Field name made lowercase.
	cashinccass2 = models.BigIntegerField(db_column='CASHINCCASS2', blank=True, null=True)  # Field name made lowercase.
	cashinccass3 = models.BigIntegerField(db_column='CASHINCCASS3', blank=True, null=True)  # Field name made lowercase.
	cashinccass4 = models.BigIntegerField(db_column='CASHINCCASS4', blank=True, null=True)  # Field name made lowercase.
	cashoutcass1 = models.BigIntegerField(db_column='CASHOUTCASS1', blank=True, null=True)  # Field name made lowercase.
	cashoutcass2 = models.BigIntegerField(db_column='CASHOUTCASS2', blank=True, null=True)  # Field name made lowercase.
	cashoutcass3 = models.BigIntegerField(db_column='CASHOUTCASS3', blank=True, null=True)  # Field name made lowercase.
	cashoutcass4 = models.BigIntegerField(db_column='CASHOUTCASS4', blank=True, null=True)  # Field name made lowercase.
	currbalcass1 = models.BigIntegerField(db_column='CURRBALCASS1', blank=True, null=True)  # Field name made lowercase.
	currbalcass2 = models.BigIntegerField(db_column='CURRBALCASS2', blank=True, null=True)  # Field name made lowercase.
	currbalcass3 = models.BigIntegerField(db_column='CURRBALCASS3', blank=True, null=True)  # Field name made lowercase.
	currbalcass4 = models.BigIntegerField(db_column='CURRBALCASS4', blank=True, null=True)  # Field name made lowercase.
	lastresetdate = models.CharField(db_column='LASTRESETDATE', max_length=255, blank=True, null=True)  # Field name made lowercase.
	lastresettime = models.FloatField(db_column='LASTRESETTIME', blank=True, null=True)  # Field name made lowercase.
	lasttxndate = models.CharField(db_column='LASTTXNDATE', max_length=255, blank=True, null=True)  # Field name made lowercase.
	lasttxntime = models.FloatField(db_column='LASTTXNTIME', blank=True, null=True)  # Field name made lowercase.
	lasttxnserial = models.FloatField(db_column='LASTTXNSERIAL', blank=True, null=True)  # Field name made lowercase.
	lasttxnstatus = models.FloatField(db_column='LASTTXNSTATUS', blank=True, null=True)  # Field name made lowercase.
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_idbi'


class cash_balance_file_iob(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	remaining_1 = models.IntegerField(blank=True, null=True)
	remaining_2 = models.IntegerField(blank=True, null=True)
	remaining_3 = models.IntegerField(blank=True, null=True)
	remaining_4 = models.IntegerField(blank=True, null=True)
	total_of_remaining = models.IntegerField(blank=True, null=True)
	opening_1 = models.IntegerField(blank=True, null=True)
	opening_2 = models.IntegerField(blank=True, null=True)
	opening_3 = models.IntegerField(blank=True, null=True)
	opening_4 = models.IntegerField(blank=True, null=True)
	total_of_opening = models.IntegerField(blank=True, null=True)
	dispense_1 = models.IntegerField(blank=True, null=True)
	dispense_2 = models.IntegerField(blank=True, null=True)
	dispense_3 = models.IntegerField(blank=True, null=True)
	dispense_4 = models.IntegerField(blank=True, null=True)
	total_of_dispense = models.IntegerField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_iob'


# class cash_balance_file_lvb(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     brn = models.CharField(max_length=50, blank=True, null=True)
#     atm_id = models.CharField(max_length=15, blank=True, null=True)
#     atm_location = models.CharField(max_length=100, blank=True, null=True)
#     f_tray = models.IntegerField(blank=True, null=True)
#     f_count = models.IntegerField(blank=True, null=True)
#     g_tray = models.IntegerField(blank=True, null=True)
#     g_count = models.IntegerField(blank=True, null=True)
#     h_tray = models.IntegerField(blank=True, null=True)
#     h_count = models.IntegerField(blank=True, null=True)
#     total_amt = models.IntegerField(blank=True, null=True)
#     file_date_time = models.DateTimeField(blank=True, null=True)
#     project_id = models.CharField(max_length=50, blank=True, null=True)
#     record_status = models.CharField(max_length=50, blank=True, null=True)
#     created_on = models.DateTimeField(blank=True, null=True)
#     created_by = models.CharField(max_length=50, blank=True, null=True)
#     created_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     datafor_date_time = models.DateTimeField(blank=True, null=True)
#     approved_on = models.DateTimeField(blank=True, null=True)
#     approved_by = models.CharField(max_length=50, blank=True, null=True)
#     approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     rejected_on = models.DateTimeField(blank=True, null=True)
#     rejected_by = models.CharField(max_length=50, blank=True, null=True)
#     reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     deleted_on = models.DateTimeField(blank=True, null=True)
#     deleted_by = models.CharField(max_length=50, blank=True, null=True)
#     deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     modified_on = models.DateTimeField(blank=True, null=True)
#     modified_by = models.CharField(max_length=50, blank=True, null=True)
#     modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     is_valid_record = models.CharField(max_length=10, blank=True, null=True)
#     error_code = models.CharField(max_length=50, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'cash_balance_file_lvb'


class cash_balance_file_psb(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	cash_out = models.IntegerField(blank=True, null=True)
	endcash = models.IntegerField(blank=True, null=True)
	hop1cash = models.IntegerField(blank=True, null=True)
	hop1bill = models.IntegerField(blank=True, null=True)
	hop2cash = models.IntegerField(blank=True, null=True)
	hop2bill = models.IntegerField(blank=True, null=True)
	hop3cash = models.IntegerField(blank=True, null=True)
	hop3bill = models.IntegerField(blank=True, null=True)
	hop4cash = models.IntegerField(blank=True, null=True)
	hop4bill = models.IntegerField(blank=True, null=True)
	ownr = models.CharField(max_length=50, blank=True, null=True)
	state = models.CharField(max_length=10, blank=True, null=True)
	timestamp = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_psb'


class cash_balance_file_rsbl(models.Model):
	id = models.BigAutoField(primary_key=True)
	cfb_date_time = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	i_denom = models.IntegerField(blank=True, null=True)
	i_begin_cash = models.IntegerField(blank=True, null=True)
	i_dispense = models.IntegerField(blank=True, null=True)
	i_remining = models.IntegerField(blank=True, null=True)
	ii_denom = models.IntegerField(blank=True, null=True)
	ii_begin_cash = models.IntegerField(blank=True, null=True)
	ii_dispense = models.IntegerField(blank=True, null=True)
	ii_remining = models.IntegerField(blank=True, null=True)
	iii_denom = models.IntegerField(blank=True, null=True)
	iii_begin_cash = models.IntegerField(blank=True, null=True)
	iii_dispense = models.IntegerField(blank=True, null=True)
	iii_remining = models.IntegerField(blank=True, null=True)
	iv_denom = models.IntegerField(blank=True, null=True)
	iv_begin_cash = models.IntegerField(blank=True, null=True)
	iv_dispense = models.IntegerField(blank=True, null=True)
	iv_remining = models.IntegerField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_rsbl'


class cash_balance_file_sbi(models.Model):
	id = models.BigAutoField(primary_key=True)
	terminal_location = models.CharField(max_length=50, blank=True, null=True)
	city = models.CharField(max_length=50, blank=True, null=True)
	cash_dispensed_hopper_1 = models.IntegerField(blank=True, null=True)
	bills_hopr1 = models.IntegerField(blank=True, null=True)
	cash_dispensed_hopper_2 = models.IntegerField(blank=True, null=True)
	bills_hopr2 = models.IntegerField(blank=True, null=True)
	cash_dispensed_hopper_3 = models.IntegerField(blank=True, null=True)
	bills_hopr3 = models.IntegerField(blank=True, null=True)
	cash_dispensed_hopper_4 = models.IntegerField(blank=True, null=True)
	bills_hopr4 = models.IntegerField(blank=True, null=True)
	cash_dispensed_hopper_5 = models.IntegerField(blank=True, null=True)
	bills_hopr5 = models.IntegerField(blank=True, null=True)
	cash_dispensed_hopper_6 = models.IntegerField(blank=True, null=True)
	bills_hopr6 = models.IntegerField(blank=True, null=True)
	atm_id = models.CharField(max_length=20, blank=True, null=True)
	total_remaining_cash_def_curr = models.IntegerField(blank=True, null=True)
	remote_address = models.CharField(max_length=255, blank=True, null=True)
	cash_increment_hopper_1 = models.IntegerField(blank=True, null=True)
	cash_increment_hopper_2 = models.IntegerField(blank=True, null=True)
	cash_increment_hopper_3 = models.IntegerField(blank=True, null=True)
	cash_increment_hopper_4 = models.IntegerField(blank=True, null=True)
	cash_increment_hopper_5 = models.IntegerField(blank=True, null=True)
	cash_increment_hopper_6 = models.IntegerField(blank=True, null=True)
	circle = models.CharField(max_length=100, blank=True, null=True)
	sitetype = models.CharField(max_length=15, blank=True, null=True)
	msvendor = models.CharField(max_length=30, blank=True, null=True)
	lastwithdrawaltime = models.DateTimeField(blank=True, null=True)
	switch = models.CharField(max_length=50, blank=True, null=True)
	district = models.CharField(max_length=50, blank=True, null=True)
	module = models.CharField(max_length=100, blank=True, null=True)
	currentstatus = models.CharField(max_length=10, blank=True, null=True)
	branchmanagerphone = models.TextField(blank=True, null=True)
	channelmanagercontact = models.TextField(blank=True, null=True)
	jointcustodianphone = models.TextField(blank=True, null=True)
	network = models.CharField(max_length=10, blank=True, null=True)
	populationgroup = models.CharField(max_length=20, blank=True, null=True)
	regionname = models.IntegerField(blank=True, null=True)
	statename = models.CharField(max_length=255, blank=True, null=True)
	jointcustodianname1 = models.CharField(max_length=50, blank=True, null=True)
	jointcustodianphone2 = models.CharField(max_length=15, blank=True, null=True)
	last_deposit_txn_time = models.DateTimeField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_sbi'


class cash_balance_file_ubi(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	type_01 = models.IntegerField(blank=True, null=True)
	type_02 = models.IntegerField(blank=True, null=True)
	type_03 = models.IntegerField(blank=True, null=True)
	type_04 = models.IntegerField(blank=True, null=True)
	file_date_time = models.DateTimeField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_ubi'


class cash_balance_file_uco(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	cassete1_start = models.IntegerField(blank=True, null=True)
	cassete1_dispensed = models.IntegerField(blank=True, null=True)
	cassete1_remaining = models.IntegerField(blank=True, null=True)
	cassete2_start = models.IntegerField(blank=True, null=True)
	cassete2_dispensed = models.IntegerField(blank=True, null=True)
	cassete2_remaining = models.IntegerField(blank=True, null=True)
	cassete3_start = models.IntegerField(blank=True, null=True)
	cassete3_dispensed = models.IntegerField(blank=True, null=True)
	cassete3_remaining = models.IntegerField(blank=True, null=True)
	cassete4_start = models.IntegerField(blank=True, null=True)
	cassete4_dispensed = models.IntegerField(blank=True, null=True)
	cassete4_remaining = models.IntegerField(blank=True, null=True)
	total_start = models.IntegerField(blank=True, null=True)
	toatal_dispensed = models.IntegerField(blank=True, null=True)
	total_remaining = models.IntegerField(blank=True, null=True)
	vendor_name = models.CharField(max_length=30, blank=True, null=True)
	region = models.CharField(max_length=100, blank=True, null=True)
	mtloc = models.CharField(max_length=255, blank=True, null=True)
	mtadr = models.CharField(max_length=255, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_uco'


class cash_balance_file_vjb(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	cash_start1 = models.BigIntegerField(blank=True, null=True)
	cash_start2 = models.BigIntegerField(blank=True, null=True)
	cash_start3 = models.BigIntegerField(blank=True, null=True)
	cash_start4 = models.BigIntegerField(blank=True, null=True)
	cash_inc1 = models.BigIntegerField(blank=True, null=True)
	cash_inc2 = models.BigIntegerField(blank=True, null=True)
	cash_inc3 = models.BigIntegerField(blank=True, null=True)
	cash_inc4 = models.BigIntegerField(blank=True, null=True)
	cash_dec1 = models.BigIntegerField(blank=True, null=True)
	cash_dec2 = models.BigIntegerField(blank=True, null=True)
	cash_dec3 = models.BigIntegerField(blank=True, null=True)
	cash_dec4 = models.BigIntegerField(blank=True, null=True)
	cash_out1 = models.BigIntegerField(blank=True, null=True)
	cash_out2 = models.BigIntegerField(blank=True, null=True)
	cash_out3 = models.BigIntegerField(blank=True, null=True)
	cash_out4 = models.BigIntegerField(blank=True, null=True)
	cash_currbal1 = models.BigIntegerField(blank=True, null=True)
	cash_currbal2 = models.BigIntegerField(blank=True, null=True)
	cash_currbal3 = models.BigIntegerField(blank=True, null=True)
	cash_currbal4 = models.BigIntegerField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_file_vjb'


class cash_balance_register(models.Model):
	id = models.BigAutoField(primary_key=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	bank_name = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	remaining_balance_50 = models.BigIntegerField(blank=True, null=True)
	remaining_balance_100 = models.BigIntegerField(blank=True, null=True)
	remaining_balance_200 = models.BigIntegerField(blank=True, null=True)
	remaining_balance_500 = models.BigIntegerField(blank=True, null=True)
	remaining_balance_2000 = models.BigIntegerField(blank=True, null=True)
	total_remaining_balance = models.BigIntegerField(blank=True, null=True)
	calculated_remaining_balance_amount = models.BigIntegerField(blank=True, null=True)
	is_total_remaining_matched_with_calculated = models.NullBooleanField()
	exception_trigger_id = models.CharField(max_length=255, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=20, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_balance_register'




class cash_dispense_file_bomh(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=20)
	location = models.CharField(max_length=50, blank=True, null=True)
	amount_dispensed = models.IntegerField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_on = models.DateTimeField(blank=True, null=True)
	reject_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_dispense_file_bomh'



class cash_dispense_file_cab(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=20)
	fiid = models.CharField(max_length=10, blank=True, null=True)
	term_city = models.CharField(max_length=25, blank=True, null=True)
	term_location = models.CharField(max_length=50, blank=True, null=True)
	unapproved_nonfin = models.IntegerField(blank=True, null=True)
	unapproved_fin = models.IntegerField(blank=True, null=True)
	approved_nonfin = models.IntegerField(blank=True, null=True)
	approved_fin = models.IntegerField(blank=True, null=True)
	approved_declined_tottran = models.IntegerField(blank=True, null=True)
	amount_dispensed = models.IntegerField(blank=True, null=True)
	file_date = models.DateField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_on = models.DateTimeField(blank=True, null=True)
	reject_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_dispense_file_cab'



class cash_dispense_file_rsbl(models.Model):
	id = models.BigAutoField(primary_key=True)
	trl_date_local = models.DateField(db_column='TRL_DATE_LOCAL', blank=True, null=True)  # Field name made lowercase.
	atm_id = models.CharField(max_length=20)
	amount_dispensed = models.IntegerField(blank=True, null=True)
	count_trl_card_acpt_terminal_ident = models.IntegerField(db_column='count_TRL_CARD_ACPT_TERMINAL_IDENT', blank=True, null=True)  # Field name made lowercase.
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_on = models.DateTimeField(blank=True, null=True)
	reject_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_dispense_file_rsbl'



class cash_dispense_file_sbi(models.Model):
	id = models.BigAutoField(primary_key=True)
	atm_id = models.CharField(max_length=20)
	cash_withdrawal = models.IntegerField(blank=True, null=True)
	opening_cash = models.IntegerField(blank=True, null=True)
	total_cash_replenished = models.IntegerField(blank=True, null=True)
	cer = models.FloatField(blank=True, null=True)
	msvendor = models.CharField(max_length=20, blank=True, null=True)
	circle_name = models.CharField(max_length=30, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_on = models.DateTimeField(blank=True, null=True)
	reject_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_dispense_file_sbi'


class cash_dispense_file_ubi(models.Model):
	id = models.BigAutoField(primary_key=True)
	type_01 = models.IntegerField(blank=True, null=True)
	type_02 = models.IntegerField(blank=True, null=True)

	type_03 = models.IntegerField(blank=True, null=True)
	type_04 = models.IntegerField(blank=True, null=True)
	atm_id = models.CharField(max_length=15, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_on = models.DateTimeField(blank=True, null=True)
	reject_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_dispense_file_ubi'




class cash_dispense_register(models.Model):
	id = models.BigAutoField(primary_key=True)
	date = models.DateTimeField(blank=True, null=True)
	atm_id = models.CharField(max_length=20)
	bank_name = models.CharField(max_length=50)
	location = models.CharField(db_column='Location', max_length=50, blank=True, null=True)  # Field name made lowercase.
	total_dispense_amount = models.IntegerField(blank=True, null=True)
	reference_id = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_dispense_register'


class vault_cash_balance(models.Model):
	id = models.BigAutoField(primary_key=True)
	bank_name = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch_name = models.CharField(max_length=50, blank=True, null=True)
	cra_name = models.CharField(max_length=20, blank=True, null=True)
	agent = models.CharField(max_length=50, blank=True, null=True)
	vault_balance_100 = models.IntegerField(blank=True, null=True)
	vault_balance_200 = models.IntegerField(blank=True, null=True)
	vault_balance_500 = models.IntegerField(blank=True, null=True)
	vault_balance_2000 = models.IntegerField(blank=True, null=True)
	total_vault_balance = models.IntegerField(blank=True, null=True)
	dependency = models.CharField(max_length=50, blank=True, null=True)
	remark = models.TextField(blank=True, null=True)
	date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'vault_cash_balance'




class Daily_Loading_Report(models.Model):
	id = models.BigAutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
	date = models.DateTimeField(db_column='Date', blank=True, null=True)  # Field name made lowercase.
	sr_no = models.IntegerField(db_column='Sr_No', blank=True, null=True)  # Field name made lowercase.
	atm_id = models.CharField(db_column='ATM_ID', max_length=20, blank=True, null=True)  # Field name made lowercase.
	location = models.CharField(db_column='Location', max_length=500, blank=True, null=True)  # Field name made lowercase.
	feeder_branch = models.CharField(db_column='Feeder_Branch', max_length=100, blank=True, null=True)  # Field name made lowercase.
	cra = models.CharField(db_column='CRA', max_length=50, blank=True, null=True)  # Field name made lowercase.
	district = models.CharField(db_column='District', max_length=50, blank=True, null=True)  # Field name made lowercase.
	state = models.CharField(db_column='State', max_length=50, blank=True, null=True)  # Field name made lowercase.
	bank_name = models.CharField(max_length=50, blank=True, null=True)
	planning_100 = models.IntegerField(db_column='Planning_100', blank=True, null=True)  # Field name made lowercase.
	planning_200 = models.IntegerField(db_column='Planning_200', blank=True, null=True)  # Field name made lowercase.
	planning_500 = models.IntegerField(db_column='Planning_500', blank=True, null=True)  # Field name made lowercase.
	planning_2000 = models.IntegerField(db_column='Planning_2000', blank=True, null=True)  # Field name made lowercase.
	planning_total = models.IntegerField(db_column='Planning_Total', blank=True, null=True)  # Field name made lowercase.
	revised_planning_100 = models.IntegerField(db_column='Revised_Planning_100', blank=True, null=True)  # Field name made lowercase.
	revised_planning_200 = models.IntegerField(db_column='Revised_Planning_200', blank=True, null=True)  # Field name made lowercase.
	revised_planning_500 = models.IntegerField(db_column='Revised_Planning_500', blank=True, null=True)  # Field name made lowercase.
	revised_planning_2000 = models.IntegerField(db_column='Revised_Planning_2000', blank=True, null=True)  # Field name made lowercase.
	revised_planning_total = models.IntegerField(db_column='Revised_Planning_Total', blank=True, null=True)  # Field name made lowercase.
	actual_loading_100 = models.IntegerField(db_column='Actual_Loading_100', blank=True, null=True)  # Field name made lowercase.
	actual_loading_200 = models.IntegerField(db_column='Actual_Loading_200', blank=True, null=True)  # Field name made lowercase.
	actual_loading_500 = models.IntegerField(db_column='Actual_Loading_500', blank=True, null=True)  # Field name made lowercase.
	actual_loading_2000 = models.IntegerField(db_column='Actual_Loading_2000', blank=True, null=True)  # Field name made lowercase.
	actual_loading_total = models.IntegerField(db_column='Actual_Loading_Total', blank=True, null=True)  # Field name made lowercase.
	remarks = models.CharField(db_column='Remarks', max_length=100, blank=True, null=True)  # Field name made lowercase.
	diverted_atm_id = models.IntegerField(db_column='Diverted_ATM_ID', blank=True, null=True)  # Field name made lowercase.
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'Daily_Loading_Report'


class user_auth_info(models.Model):
	username = models.CharField(max_length=2000)
	userkey = models.CharField(max_length=4000, primary_key=True)
	timestamp_key = models.DateTimeField()
	machine_info = models.CharField(max_length=4000)
	request_params = models.CharField(max_length=4000)

	class Meta:
		db_table = 'user_auth_info'


class user_bank_mapping(models.Model):
	id = models.BigAutoField(primary_key=True)
	user_name = models.CharField(max_length=50, blank=True, null=True)
	bank_id = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=500, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'user_bank_mapping'


class user_project_mapping(models.Model):
	id = models.BigAutoField(primary_key=True)
	user_name = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=500, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'user_project_mapping'

class MetaDataTable(models.Model):
	id = models.BigIntegerField(primary_key=True)
	datafor = models.CharField(max_length=100, blank=True, null=True)
	bankcode = models.CharField(max_length=50, blank=True, null=True)
	columnheaders = models.CharField(db_column='columnHeaders', max_length=100, blank=True, null=True)  # Field name made lowercase.
	isrequired = models.CharField(db_column='IsRequired', max_length=10, blank=True, null=True)  # Field name made lowercase.
	table_column = models.CharField(max_length=300, blank=True, null=True)
	display_column_name = models.CharField(max_length=300, blank=True, null=True)
	table_name = models.CharField(max_length=250, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'MetaDataTable'

class c3r_CMIS(models.Model):
	id = models.BigAutoField(primary_key=True)
	sr_no = models.IntegerField(blank=True, null=True)
	bank = models.CharField(max_length=100, blank=True, null=True)
	feeder_branch = models.CharField(max_length=100, blank=True, null=True)
	atm_id = models.CharField(max_length=100, blank=True, null=True)
	location = models.TextField(blank=True, null=True)
	date = models.DateTimeField(blank=True, null=True)
	eod_loading_time = models.CharField(max_length=255, blank=True, null=True)
	status_of_loading = models.CharField(max_length=50, blank=True, null=True)
	last_transaction_no = models.CharField(max_length=100, blank=True, null=True)
	cra_name = models.CharField(max_length=100, blank=True, null=True)
	indentno = models.IntegerField(blank=True, null=True)
	bankrefno = models.IntegerField(blank=True, null=True)
	atmcount_op_bal_100 = models.IntegerField(db_column='atmcount_op__bal_100', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmcount_op_bal_200 = models.IntegerField(db_column='atmcount_op__bal_200', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmcount_op_bal_500 = models.IntegerField(db_column='atmcount_op__bal_500', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmcount_op_bal_1000 = models.IntegerField(db_column='atmcount_op__bal_1000', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmcount_op_bal_2000 = models.IntegerField(db_column='atmcount_op__bal_2000', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmcount_op_bal_total = models.IntegerField(blank=True, null=True)
	atmcount_disp_100 = models.IntegerField(blank=True, null=True)
	atmcount_disp_200 = models.IntegerField(blank=True, null=True)
	atmcount_disp_500 = models.IntegerField(blank=True, null=True)
	atmcount_disp_1000 = models.CharField(max_length=10, blank=True, null=True)
	atmcount_disp_2000 = models.IntegerField(blank=True, null=True)
	atmcount_disp_total = models.IntegerField(blank=True, null=True)
	atmcount_divert_count_100 = models.IntegerField(blank=True, null=True)
	atmcount_divert_count_200 = models.IntegerField(blank=True, null=True)
	atmcount_divert_count_500 = models.IntegerField(blank=True, null=True)
	atmcount_divert_count_1000 = models.CharField(max_length=10, blank=True, null=True)
	atmcount_divert_count_2000 = models.IntegerField(blank=True, null=True)
	atmcount_divert_count_total = models.IntegerField(blank=True, null=True)
	atmcount_remain_counter_100 = models.IntegerField(blank=True, null=True)
	atmcount_remain_counter_200 = models.IntegerField(blank=True, null=True)
	atmcount_remain_counter_500 = models.IntegerField(blank=True, null=True)
	atmcount_remain_counter_1000 = models.CharField(max_length=10, blank=True, null=True)
	atmcount_remain_counter_2000 = models.IntegerField(blank=True, null=True)
	atmcount_remain_counter_total = models.IntegerField(blank=True, null=True)
	atmphysical_cash_from_cassettes_100 = models.IntegerField(db_column='atmphysical_cash__from_cassettes_100', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmphysical_cash_from_cassettes_200 = models.IntegerField(db_column='atmphysical_cash__from_cassettes_200', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmphysical_cash_from_cassettes_500 = models.IntegerField(db_column='atmphysical_cash__from_cassettes_500', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmphysical_cash_from_cassettes_1000 = models.CharField(db_column='atmphysical_cash__from_cassettes_1000', max_length=10, blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmphysical_cash_from_cassettes_2000 = models.IntegerField(db_column='atmphysical_cash__from_cassettes_2000', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmphysical_cash_from_cassettes_total = models.IntegerField(db_column='atmphysical_cash__from_cassettes_total', blank=True, null=True)  # Field renamed because it contained more than one '_' in a row.
	atmphysical_purge_bin_100 = models.IntegerField(blank=True, null=True)
	atmphysical_purge_bin_200 = models.IntegerField(blank=True, null=True)
	atmphysical_purge_bin_500 = models.IntegerField(blank=True, null=True)
	atmphysical_purge_bin_1000 = models.CharField(max_length=10, blank=True, null=True)
	atmphysical_purge_bin_2000 = models.IntegerField(blank=True, null=True)
	atmphysical_purge_bin_total = models.IntegerField(blank=True, null=True)
	atmphysical_total_remainingcash_100 = models.IntegerField(blank=True, null=True)
	atmphysical_total_remainingcash_200 = models.IntegerField(blank=True, null=True)
	atmphysical_total_remainingcash_500 = models.IntegerField(blank=True, null=True)
	atmphysical_total_remainingcash_1000 = models.CharField(max_length=10, blank=True, null=True)
	atmphysical_total_remainingcash_2000 = models.IntegerField(blank=True, null=True)
	atmphysical_total_remainingcash_total = models.IntegerField(blank=True, null=True)
	atm_cashreturns_100 = models.IntegerField(blank=True, null=True)
	atm_cashreturns_200 = models.IntegerField(blank=True, null=True)
	atm_cashreturns_500 = models.IntegerField(blank=True, null=True)
	atm_cashreturns_1000 = models.CharField(max_length=10, blank=True, null=True)
	atm_cashreturns_2000 = models.IntegerField(blank=True, null=True)
	atm_cashreturns_total = models.IntegerField(blank=True, null=True)
	atm_cashreturns_sealno_100 = models.IntegerField(blank=True, null=True)
	atm_cashreturns_sealno_200 = models.IntegerField(blank=True, null=True)
	atm_cashreturns_sealno_500 = models.IntegerField(blank=True, null=True)
	atm_cashreturns_sealno_1000 = models.CharField(max_length=10, blank=True, null=True)
	atm_cashreturns_sealno_2000 = models.IntegerField(blank=True, null=True)
	atm_return_cashfrompurge_100 = models.IntegerField(blank=True, null=True)
	atm_return_cashfrompurge_200 = models.IntegerField(blank=True, null=True)
	atm_return_cashfrompurge_500 = models.IntegerField(blank=True, null=True)
	atm_return_cashfrompurge_1000 = models.CharField(max_length=10, blank=True, null=True)
	atm_return_cashfrompurge_2000 = models.IntegerField(blank=True, null=True)
	atm_return_cashfrompurge_total = models.IntegerField(blank=True, null=True)
	atm_repl_100 = models.IntegerField(blank=True, null=True)
	atm_repl_200 = models.IntegerField(blank=True, null=True)
	atm_repl_500 = models.IntegerField(blank=True, null=True)
	atm_repl_1000 = models.CharField(max_length=10, blank=True, null=True)
	atm_repl_2000 = models.IntegerField(blank=True, null=True)
	atm_repl_total = models.IntegerField(blank=True, null=True)
	atm_repl_seal_100 = models.IntegerField(blank=True, null=True)
	atm_repl_seal_200 = models.IntegerField(blank=True, null=True)
	atm_repl_seal_500 = models.IntegerField(blank=True, null=True)
	atm_repl_seal_1000 = models.CharField(max_length=10, blank=True, null=True)
	atm_repl_seal_2000 = models.IntegerField(blank=True, null=True)
	atm_closingcashreturn_100 = models.IntegerField(blank=True, null=True)
	atm_closingcashreturn_200 = models.IntegerField(blank=True, null=True)
	atm_closingcashreturn_500 = models.IntegerField(blank=True, null=True)
	atm_closingcashreturn_1000 = models.CharField(max_length=10, blank=True, null=True)
	atm_closingcashreturn_2000 = models.IntegerField(blank=True, null=True)
	atm_closingcashreturn_total = models.IntegerField(blank=True, null=True)
	atm_closingbal_100 = models.IntegerField(blank=True, null=True)
	atm_closingbal_200 = models.IntegerField(blank=True, null=True)
	atm_closingbal_500 = models.IntegerField(blank=True, null=True)
	atm_closingbal_1000 = models.CharField(max_length=10, blank=True, null=True)
	atm_closingbal_2000 = models.IntegerField(blank=True, null=True)
	atm_closingbal_total = models.IntegerField(blank=True, null=True)
	sw_count_openbal_100 = models.IntegerField(blank=True, null=True)
	sw_count_openbal_200 = models.IntegerField(blank=True, null=True)
	sw_count_openbal_500 = models.IntegerField(blank=True, null=True)
	sw_count_openbal_1000 = models.CharField(max_length=10, blank=True, null=True)
	sw_count_openbal_2000 = models.IntegerField(blank=True, null=True)
	sw_count_openbal_total = models.IntegerField(blank=True, null=True)
	sw_disp_100 = models.IntegerField(blank=True, null=True)
	sw_disp_200 = models.IntegerField(blank=True, null=True)
	sw_disp_500 = models.IntegerField(blank=True, null=True)
	sw_disp_1000 = models.CharField(max_length=10, blank=True, null=True)
	sw_disp_2000 = models.IntegerField(blank=True, null=True)
	sw_disp_total = models.IntegerField(blank=True, null=True)
	sw_loading_100 = models.IntegerField(blank=True, null=True)
	sw_loading_200 = models.IntegerField(blank=True, null=True)
	sw_loading_500 = models.IntegerField(blank=True, null=True)
	sw_loading_1000 = models.CharField(max_length=10, blank=True, null=True)
	sw_loading_2000 = models.IntegerField(blank=True, null=True)
	sw_loading_total = models.IntegerField(blank=True, null=True)
	sw_adminincrease_100 = models.IntegerField(blank=True, null=True)
	sw_adminincrease_200 = models.IntegerField(blank=True, null=True)
	sw_adminincrease_500 = models.IntegerField(blank=True, null=True)
	sw_adminincrease_1000 = models.CharField(max_length=10, blank=True, null=True)
	sw_adminincrease_2000 = models.IntegerField(blank=True, null=True)
	sw_adminincrease_total = models.IntegerField(blank=True, null=True)
	sw_admindecrease_100 = models.IntegerField(blank=True, null=True)
	sw_admindecrease_200 = models.IntegerField(blank=True, null=True)
	sw_admindecrease_500 = models.IntegerField(blank=True, null=True)
	sw_admindecrease_1000 = models.CharField(max_length=10, blank=True, null=True)
	sw_admindecrease_2000 = models.IntegerField(blank=True, null=True)
	sw_admindecrease_total = models.IntegerField(blank=True, null=True)
	sw_closing_100 = models.IntegerField(blank=True, null=True)
	sw_closing_200 = models.IntegerField(blank=True, null=True)
	sw_closing_500 = models.IntegerField(blank=True, null=True)
	sw_closing_1000 = models.CharField(max_length=10, blank=True, null=True)
	sw_closing_2000 = models.IntegerField(blank=True, null=True)
	sw_closing_total = models.IntegerField(blank=True, null=True)
	phys_diff_overage_100 = models.IntegerField(blank=True, null=True)
	phys_diff_overage_200 = models.IntegerField(blank=True, null=True)
	phys_diff_overage_500 = models.IntegerField(blank=True, null=True)
	phys_diff_overage_1000 = models.CharField(max_length=10, blank=True, null=True)
	phys_diff_overage_2000 = models.IntegerField(blank=True, null=True)
	phys_diff_overage_total = models.IntegerField(blank=True, null=True)
	phys_diff_shortage_100 = models.IntegerField(blank=True, null=True)
	phys_diff_shortage_200 = models.IntegerField(blank=True, null=True)
	phys_diff_shortage_500 = models.IntegerField(blank=True, null=True)
	phys_diff_shortage_1000 = models.CharField(max_length=10, blank=True, null=True)
	phys_diff_shortage_2000 = models.IntegerField(blank=True, null=True)
	phys_diff_shortage_total = models.IntegerField(blank=True, null=True)
	remarks = models.CharField(max_length=255, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'c3r_CMIS'


def validate_mstr_file_frmt(file_type,columnnames):
	try:
		sql_str = "SET NOCOUNT ON exec uspMasterFormatValidation " + "'" + file_type + "', "  + "'" + columnnames + "' "
		print("sql_str from validate_mstr_file_frmt : {}".format(sql_str))
		cur = connection.cursor()
		cur.execute(sql_str)
		code = cur.fetchone()
		return code[0]
	except Exception as e:
		raise e

class ticket_master(models.Model):
	ticket_id = models.BigAutoField(primary_key=True)
	additional_info = models.CharField(max_length=255, blank=True, null=True)
	category_id = models.CharField(max_length=50, blank=True, null=True)
	status = models.CharField(max_length=50, blank=True, null=True)
	assign_to_user = models.CharField(max_length=100, blank=True, null=True)
	assign_to_group = models.CharField(max_length=100, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	subject = models.CharField(max_length=100, blank=True, null=True)
	remarks = models.TextField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)

	def __str__(self):
		return str(self.ticket_id)

	class Meta:
		managed = False
		db_table = 'ticket_master'


class ticket_reply(models.Model):
	reply_id = models.BigAutoField(primary_key=True)
	ticket_id = models.BigIntegerField()
	replymessage = models.CharField(max_length=255, blank=True, null=True)
	repliedby = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datetimereply = models.DateTimeField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'ticket_reply'

class ticket_category_master(models.Model):
	category_id = models.BigAutoField(db_column='category_id', primary_key=True)
	category = models.CharField(max_length=100, blank=True, null=True)
	sub_category = models.CharField(max_length=100, blank=True, null=True)
	reference_id = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'ticket_category_master'

class workflow_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	category_id = models.CharField(max_length=300, blank=True, null=True)
	sla = models.CharField(max_length=200, blank=True, null=True)
	assigned_to_group = models.IntegerField(blank=True, null=True)
	assigned_to_user = models.IntegerField(blank=True, null=True)
	escalation_level1 = models.CharField(max_length=500, blank=True, null=True)
	escalation_level2 = models.CharField(max_length=500, blank=True, null=True)
	escalation_level3 = models.CharField(max_length=500, blank=True, null=True)
	status = models.CharField(max_length=500, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'workflow_master'


class holiday(models.Model):
	holiday_name = models.CharField(db_column='Holiday Name', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	holiday_type = models.CharField(db_column='Holiday Type', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	holiday_date = models.CharField(db_column='Holiday Date', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	andaman_and_nicobar_islands = models.CharField(db_column='Andaman and Nicobar Islands', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	andhra_pradesh = models.CharField(db_column='Andhra Pradesh', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	arunachal_pradesh = models.CharField(db_column='Arunachal Pradesh', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	assam = models.CharField(db_column='Assam', max_length=255, blank=True, null=True)  # Field name made lowercase.
	bihar = models.CharField(db_column='Bihar', max_length=255, blank=True, null=True)  # Field name made lowercase.
	chandigarh = models.CharField(db_column='Chandigarh', max_length=255, blank=True, null=True)  # Field name made lowercase.
	chhattisgarh = models.CharField(db_column='Chhattisgarh', max_length=255, blank=True, null=True)  # Field name made lowercase.
	daman_and_diu = models.CharField(db_column='Daman and Diu', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	delhi = models.CharField(db_column='Delhi', max_length=255, blank=True, null=True)  # Field name made lowercase.
	goa = models.CharField(db_column='Goa', max_length=255, blank=True, null=True)  # Field name made lowercase.
	gujarat = models.CharField(db_column='Gujarat', max_length=255, blank=True, null=True)  # Field name made lowercase.
	himachal_pradesh = models.CharField(db_column='Himachal Pradesh', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	haryana = models.CharField(db_column='Haryana', max_length=255, blank=True, null=True)  # Field name made lowercase.
	jharkhand = models.CharField(db_column='Jharkhand', max_length=255, blank=True, null=True)  # Field name made lowercase.
	jammu_and_kashmir = models.CharField(db_column='Jammu and Kashmir', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	karnataka = models.CharField(db_column='Karnataka', max_length=255, blank=True, null=True)  # Field name made lowercase.
	kerala = models.CharField(db_column='Kerala', max_length=255, blank=True, null=True)  # Field name made lowercase.
	maharashtra = models.CharField(db_column='Maharashtra', max_length=255, blank=True, null=True)  # Field name made lowercase.
	meghalaya = models.CharField(db_column='Meghalaya', max_length=255, blank=True, null=True)  # Field name made lowercase.
	manipur = models.CharField(db_column='Manipur', max_length=255, blank=True, null=True)  # Field name made lowercase.
	madhya_pradesh = models.CharField(db_column='Madhya Pradesh', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	mizoram = models.CharField(db_column='Mizoram', max_length=255, blank=True, null=True)  # Field name made lowercase.
	nagaland = models.CharField(db_column='Nagaland', max_length=255, blank=True, null=True)  # Field name made lowercase.
	orissa = models.CharField(db_column='Orissa', max_length=255, blank=True, null=True)  # Field name made lowercase.
	punjab = models.CharField(db_column='Punjab', max_length=255, blank=True, null=True)  # Field name made lowercase.
	pondicherry = models.CharField(db_column='Pondicherry', max_length=255, blank=True, null=True)  # Field name made lowercase.
	rajasthan = models.CharField(db_column='Rajasthan', max_length=255, blank=True, null=True)  # Field name made lowercase.
	sikkim = models.CharField(db_column='Sikkim', max_length=255, blank=True, null=True)  # Field name made lowercase.
	tamil_nadu = models.CharField(db_column='Tamil Nadu', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	tripura = models.CharField(db_column='Tripura', max_length=255, blank=True, null=True)  # Field name made lowercase.
	uttar_pradesh = models.CharField(db_column='Uttar Pradesh', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	uttaranchal = models.CharField(db_column='Uttaranchal', max_length=255, blank=True, null=True)  # Field name made lowercase.
	west_bengal = models.CharField(db_column='West Bengal', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	dadra_nagar_haveli = models.CharField(db_column='Dadra & Nagar Haveli', max_length=255, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
	lakshadweep = models.CharField(db_column='Lakshadweep', max_length=255, blank=True, null=True)  # Field name made lowercase.
	telangana = models.CharField(db_column='Telangana', max_length=255, blank=True, null=True)  # Field name made lowercase.
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_on = models.DateTimeField(blank=True, null=True)
	reject_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	class Meta:
		managed = False
		db_table = 'holiday'


class brand_bill_capacity(models.Model):
	id = models.BigAutoField(primary_key=True)
	brand_code = models.CharField(max_length=50, blank=True, null=True)
	description = models.CharField(max_length=50, blank=True, null=True)
	capacity_50 = models.IntegerField(blank=True, null=True)
	capacity_100 = models.IntegerField(blank=True, null=True)
	capacity_200 = models.IntegerField(blank=True, null=True)
	capacity_500 = models.IntegerField(blank=True, null=True)
	capacity_2000 = models.IntegerField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'brand_bill_capacity'

def validate_reference_file_frmt(file_type,columnnames):
	try:
		sql_str = "SET NOCOUNT ON exec uspReferenceFormatValidation " + "'" + file_type + "', "  + "'" + columnnames + "' "
		#print("sql_str from validate_reference_file_frmt : {}".format(sql_str))
		cur = connection.cursor()
		cur.execute(sql_str)
		code = cur.fetchone()
		return code[0]
	except Exception as e:
		raise e

class cra_vault_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	vault_code = models.CharField(max_length=200, blank=True, null=True)
	cra_code = models.CharField(max_length=4000, blank=True, null=True)
	state = models.CharField(max_length=50, blank=True, null=True)
	region = models.CharField(max_length=50, blank=True, null=True)
	location = models.CharField(max_length=1000, blank=True, null=True)
	address = models.CharField(max_length=4000, blank=True, null=True)
	vault_type = models.CharField(max_length=100, blank=True, null=True)
	vault_status = models.CharField(max_length=100, blank=True, null=True)
	vaulting_allowed = models.CharField(max_length=10, blank=True, null=True)
	contact_details = models.CharField(max_length=100, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	rejected_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cra_vault_master'


class Data_update_reasons(models.Model):
	category = models.CharField(max_length=30, blank=True, null=True)
	update_reason = models.CharField(max_length=100, blank=True, null=True)
	status = models.CharField(max_length=10, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'Data_update_reasons'

class Data_overwrite_reasons(models.Model):
	category = models.CharField(max_length=30, blank=True, null=True)
	overwrite_reason = models.CharField(max_length=100, blank=True, null=True)
	status = models.CharField(max_length=10, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'Data_overwrite_reasons'

class Holiday_Master(models.Model):
	id = models.BigAutoField(primary_key=True)
	holiday_code = models.CharField(max_length=50, blank=True, null=True)
	holiday_name = models.CharField(max_length=50, blank=True, null=True)
	holiday_description = models.CharField(max_length=50, blank=True, null=True)
	holiday_type = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	rejected_reference_id = models.CharField(max_length=50, blank=True, null=True)
	last_modified_on = models.DateTimeField(blank=True, null=True)
	last_modified_by = models.CharField(max_length=50, blank=True, null=True)
	last_modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	isvalidrecord = models.CharField(db_column='isValidRecord', max_length=20, blank=True, null=True)  # Field name made lowercase.

	class Meta:
		managed = False
		db_table = 'Holiday_Master'

# class CRA_vault_master(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     vault_code = models.BigIntegerField(blank=True, null=True)
#     cra_code = models.CharField(max_length=50, blank=True, null=True)
#     state = models.CharField(max_length=50, blank=True, null=True)
#     region = models.CharField(max_length=50, blank=True, null=True)
#     location = models.CharField(max_length=100, blank=True, null=True)
#     address = models.CharField(max_length=200, blank=True, null=True)
#     vault_type = models.CharField(max_length=20, blank=True, null=True)
#     vault_status = models.CharField(max_length=20, blank=True, null=True)
#     vaulting_allowed = models.CharField(max_length=10, blank=True, null=True)
#     contact_person = models.CharField(max_length=50, blank=True, null=True)
#     contact_details = models.CharField(max_length=20, blank=True, null=True)
#     project_id = models.CharField(max_length=50, blank=True, null=True)
#     record_status = models.CharField(max_length=50, blank=True, null=True)
#     created_on = models.DateTimeField(blank=True, null=True)
#     created_by = models.CharField(max_length=50, blank=True, null=True)
#     created_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     approved_on = models.DateTimeField(blank=True, null=True)
#     approved_by = models.CharField(max_length=50, blank=True, null=True)
#     approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
#     rejected_on = models.DateTimeField(blank=True, null=True)
#     rejected_by = models.CharField(max_length=50, blank=True, null=True)
#     rejected_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     modified_on = models.DateTimeField(blank=True, null=True)
#     modified_by = models.CharField(max_length=50, blank=True, null=True)
#     modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     is_valid_record = models.CharField(max_length=20, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'CRA_vault_master'



class CRA_Empaneled(models.Model):
	id = models.BigAutoField(primary_key=True)
	cra_code = models.CharField(max_length=50, blank=True, null=True)
	cra_name = models.CharField(max_length=50, blank=True, null=True)
	registered_office = models.CharField(max_length=100, blank=True, null=True)
	registration_number = models.CharField(max_length=50, blank=True, null=True)
	contact_details = models.CharField(max_length=20, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	rejected_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'CRA_Empaneled'


def validate_reference_file_frmt(file_type,columnnames):
	try:
		sql_str = "SET NOCOUNT ON exec uspReferenceFormatValidation " + "'" + file_type + "', "  + "'" + columnnames + "' "
		#print("sql_str from validate_reference_file_frmt : {}".format(sql_str))
		cur = connection.cursor()
		cur.execute(sql_str)
		code = cur.fetchone()
		return code[0]
	except Exception as e:
		raise e

# class CRA_Empaneled(models.Model):
# 	id = models.BigAutoField(primary_key=True)
# 	cra_code = models.CharField(max_length=50, blank=True, null=True)
# 	cra_name = models.CharField(max_length=50, blank=True, null=True)
# 	registered_office = models.CharField(max_length=100, blank=True, null=True)
# 	registration_number = models.CharField(max_length=50, blank=True, null=True)
# 	contact_details = models.CharField(max_length=20, blank=True, null=True)
# 	project_id = models.CharField(max_length=50, blank=True, null=True)
# 	record_status = models.CharField(max_length=50, blank=True, null=True)
# 	created_on = models.DateTimeField(blank=True, null=True)
# 	created_by = models.CharField(max_length=50, blank=True, null=True)
# 	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approved_on = models.DateTimeField(blank=True, null=True)
# 	approved_by = models.CharField(max_length=50, blank=True, null=True)
# 	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
# 	rejected_on = models.DateTimeField(blank=True, null=True)
# 	rejected_by = models.CharField(max_length=50, blank=True, null=True)
# 	rejected_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	modified_on = models.DateTimeField(blank=True, null=True)
# 	modified_by = models.CharField(max_length=50, blank=True, null=True)
# 	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
#
# 	class Meta:
# 		managed = False
# 		db_table = 'CRA_Empaneled'

class Auth_Signatories_Signatures(models.Model):
	id = models.BigAutoField(db_column='Id', primary_key=True)  # Field name made lowercase.
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	signatory_name = models.CharField(max_length=50, blank=True, null=True)
	designation = models.CharField(max_length=50, blank=True, null=True)
	physical_signature = models.BinaryField(blank=True, null=True)
	digital_signature = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'Auth_Signatories_Signatures'

class Data_rejection_reasons(models.Model):
	category = models.CharField(max_length=30)
	rejection_reason = models.CharField(max_length=100, blank=True, null=True)
	status = models.CharField(max_length=10, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'Data_rejection_reasons'

class c3r_VMIS(models.Model):
	id = models.BigAutoField(primary_key=True)
	sr_no = models.IntegerField(blank=True, null=True)
	date = models.DateField(blank=True, null=True)
	bank = models.CharField(max_length=50, blank=True, null=True)
	cra_name = models.CharField(max_length=20, blank=True, null=True)
	feeder_branch_name = models.CharField(max_length=50, blank=True, null=True)
	vault_balance_100 = models.IntegerField(blank=True, null=True)
	vault_balance_200 = models.IntegerField(blank=True, null=True)
	vault_balance_500 = models.IntegerField(blank=True, null=True)
	vault_balance_1000 = models.CharField(max_length=10, blank=True, null=True)
	vault_balance_2000 = models.IntegerField(blank=True, null=True)
	total_vault_balance = models.IntegerField(blank=True, null=True)
	withdrawalfrom_bank_100 = models.IntegerField(blank=True, null=True)
	withdrawalfrom_bank_200 = models.IntegerField(blank=True, null=True)
	withdrawalfrom_bank_500 = models.IntegerField(blank=True, null=True)
	withdrawalfrom_bank_1000 = models.CharField(max_length=10, blank=True, null=True)
	withdrawalfrom_bank_2000 = models.IntegerField(blank=True, null=True)
	total_withdrawalfrom_bank = models.IntegerField(blank=True, null=True)
	replenishto_atm_100 = models.IntegerField(blank=True, null=True)
	replenishto_atm_200 = models.IntegerField(blank=True, null=True)
	replenishto_atm_500 = models.IntegerField(blank=True, null=True)
	replenishto_atm_1000 = models.CharField(max_length=10, blank=True, null=True)
	replenishto_atm_2000 = models.IntegerField(blank=True, null=True)
	total_replenishto_atm = models.IntegerField(blank=True, null=True)
	cashreturn_atm_100 = models.IntegerField(blank=True, null=True)
	cashreturn_atm_200 = models.IntegerField(blank=True, null=True)
	cashreturn_atm_500 = models.IntegerField(blank=True, null=True)
	cashreturn_atm_1000 = models.CharField(max_length=10, blank=True, null=True)
	cashreturn_atm_2000 = models.IntegerField(blank=True, null=True)
	total_cashreturn_atm = models.IntegerField(blank=True, null=True)
	unfit_currency_100 = models.IntegerField(blank=True, null=True)
	unfit_currency_200 = models.IntegerField(blank=True, null=True)
	unfit_currency_500 = models.IntegerField(blank=True, null=True)
	unfit_currency_1000 = models.CharField(max_length=10, blank=True, null=True)
	unfit_currency_2000 = models.IntegerField(blank=True, null=True)
	total_unfit_currency = models.IntegerField(blank=True, null=True)
	atm_unfit_currency_return_100 = models.IntegerField(blank=True, null=True)
	atm_unfit_currency_return_200 = models.IntegerField(blank=True, null=True)
	atm_unfit_currency_return_500 = models.IntegerField(blank=True, null=True)
	atm_unfit_currency_return_1000 = models.CharField(max_length=10, blank=True, null=True)
	atm_unfit_currency_return_2000 = models.IntegerField(blank=True, null=True)
	total_atm_unfit_currency_return = models.IntegerField(blank=True, null=True)
	closing_balance_100 = models.IntegerField(blank=True, null=True)
	closing_balance_200 = models.IntegerField(blank=True, null=True)
	closing_balance_500 = models.IntegerField(blank=True, null=True)
	closing_balance_1000 = models.CharField(max_length=10, blank=True, null=True)
	closing_balance_2000 = models.IntegerField(blank=True, null=True)
	total_closing_balance = models.IntegerField(blank=True, null=True)
	fit_currency_100 = models.IntegerField(blank=True, null=True)
	fit_currency_200 = models.IntegerField(blank=True, null=True)
	fit_currency_500 = models.IntegerField(blank=True, null=True)
	fit_currency_1000 = models.CharField(max_length=10, blank=True, null=True)
	fit_currency_2000 = models.IntegerField(blank=True, null=True)
	total_fit_currency = models.IntegerField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'c3r_VMIS'


class atm_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	site_code = models.CharField(max_length=50, blank=True, null=True)
	old_atm_id = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	atm_band = models.TextField(blank=True, null=True)
	ej_docket_no = models.TextField(blank=True, null=True)
	grouting_status = models.TextField(blank=True, null=True)
	brand = models.TextField(blank=True, null=True)
	vsat_id = models.TextField(blank=True, null=True)
	vendor_name = models.TextField(blank=True, null=True)
	serial_no = models.TextField(blank=True, null=True)
	current_deployment_status = models.TextField(blank=True, null=True)
	tech_live_date = models.TextField(blank=True, null=True)
	cash_live_date = models.TextField(blank=True, null=True)
	insurance_limits = models.TextField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.TextField(blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	circle_zone_region = models.TextField(blank=True, null=True)
	site_code_2015_16 = models.TextField(blank=True, null=True)
	site_code_2016_17 = models.TextField(blank=True, null=True)
	state = models.TextField(blank=True, null=True)
	district = models.TextField(blank=True, null=True)
	city = models.TextField(blank=True, null=True)
	site_address_line_1 = models.TextField(blank=True, null=True)
	site_address_line_2 = models.TextField(blank=True, null=True)
	pincode = models.TextField(blank=True, null=True)
	site_category = models.TextField(blank=True, null=True)
	site_type = models.TextField(blank=True, null=True)
	installation_type = models.TextField(blank=True, null=True)
	site_status = models.TextField(blank=True, null=True)
	channel_manager_name = models.TextField(blank=True, null=True)
	channel_manager_contact_no = models.TextField(blank=True, null=True)
	location_name = models.TextField(blank=True, null=True)
	atm_cash_removal_date = models.TextField(blank=True, null=True)
	atm_ip = models.TextField(blank=True, null=True)
	switch_ip = models.TextField(blank=True, null=True)
	external_camera_installation_status = models.TextField(blank=True, null=True)
	atm_owner = models.TextField(blank=True, null=True)
	stabilizer_status = models.TextField(blank=True, null=True)
	ups_capacity = models.TextField(blank=True, null=True)
	no_of_batteries = models.TextField(blank=True, null=True)
	ups_battery_backup = models.TextField(blank=True, null=True)
	load_shedding_status = models.TextField(blank=True, null=True)
	solar_dg = models.TextField(blank=True, null=True)
	date_created = models.DateTimeField(blank=True, null=True)
	date_updated = models.DateTimeField(blank=True, null=True)
	date_deleted = models.DateTimeField(blank=True, null=True)
	is_deleted = models.CharField(max_length=20, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'atm_master'

class indent_master(models.Model):
    indent_order_number = models.CharField(primary_key=True, max_length=255)
    order_date = models.DateField(blank=True, null=True)
    collection_date = models.DateField(blank=True, null=True)
    replenishment_date = models.DateField(blank=True, null=True)
    cypher_code = models.CharField(max_length=50, blank=True, null=True)
    bank = models.CharField(max_length=50, blank=True, null=True)
    feeder_branch = models.CharField(max_length=50, blank=True, null=True)
    cra = models.CharField(max_length=50, blank=True, null=True)
    total_atm_loading_amount_50 = models.IntegerField(blank=True, null=True)
    total_atm_loading_amount_100 = models.IntegerField(blank=True, null=True)
    total_atm_loading_amount_200 = models.IntegerField(blank=True, null=True)
    total_atm_loading_amount_500 = models.IntegerField(blank=True, null=True)
    total_atm_loading_amount_2000 = models.IntegerField(blank=True, null=True)
    total_atm_loading_amount = models.IntegerField(blank=True, null=True)
    cra_opening_vault_balance_50 = models.IntegerField(blank=True, null=True)
    cra_opening_vault_balance_100 = models.IntegerField(blank=True, null=True)
    cra_opening_vault_balance_200 = models.IntegerField(blank=True, null=True)
    cra_opening_vault_balance_500 = models.IntegerField(blank=True, null=True)
    cra_opening_vault_balance_2000 = models.IntegerField(blank=True, null=True)
    cra_opening_vault_balance = models.IntegerField(blank=True, null=True)
    total_bank_withdrawal_amount_50 = models.IntegerField(blank=True, null=True)
    total_bank_withdrawal_amount_100 = models.IntegerField(blank=True, null=True)
    total_bank_withdrawal_amount_200 = models.IntegerField(blank=True, null=True)
    total_bank_withdrawal_amount_500 = models.IntegerField(blank=True, null=True)
    total_bank_withdrawal_amount_2000 = models.IntegerField(blank=True, null=True)
    total_bank_withdrawal_amount = models.IntegerField(blank=True, null=True)
    authorized_by_1 = models.CharField(max_length=50, blank=True, null=True)
    authorized_by_2 = models.CharField(max_length=50, blank=True, null=True)
    signature_authorized_by_1_auth_signatories_signatures_id = models.BigIntegerField(blank=True, null=True)
    signature_authorized_by_2_auth_signatories_signatures_id = models.BigIntegerField(blank=True, null=True)
    email_indent_mail_master_id = models.BigIntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    amount_in_words = models.CharField(max_length=500, blank=True, null=True)
    indent_type = models.CharField(max_length=50, blank=True, null=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    reject_trigger_reference_id = models.DateTimeField(blank=True, null=True)
    approved_trigger_reference_id = models.DateTimeField(blank=True, null=True)
    indent_counter = models.BigIntegerField(blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    previous_indent_code = models.CharField(max_length=255, blank=True, null=True)
    is_manually_updated = models.CharField(max_length=10, blank=True, null=True)
    indent_short_code = models.CharField(max_length=15, blank=True, null=True)
    total_closing_vault_balance = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_50 = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_100 = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_200 = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_500 = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_2000 = models.BigIntegerField(blank=True, null=True)
    total_atms = models.IntegerField(blank=True, null=True)
    total_cashout_atms = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'indent_master'


class indent_detail(models.Model):
	indent_order_number = models.CharField(primary_key=True, max_length=255)
	sr_no = models.IntegerField(blank=True, null=True)
	atm_id = models.CharField(max_length=50)
	general_ledger_account_number = models.CharField(max_length=50, blank=True, null=True)
	location = models.CharField(max_length=255, blank=True, null=True)
	purpose = models.CharField(max_length=255, blank=True, null=True)
	loading_amount_50 = models.BigIntegerField(blank=True, null=True)
	loading_amount_100 = models.BigIntegerField(blank=True, null=True)
	loading_amount_200 = models.BigIntegerField(blank=True, null=True)
	loading_amount_500 = models.BigIntegerField(blank=True, null=True)
	loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
	total = models.BigIntegerField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	indent_counter = models.BigIntegerField(blank=True, null=True)
	previous_indent_code = models.CharField(max_length=255, blank=True, null=True)
	indent_type = models.CharField(max_length=50, blank=True, null=True)
	is_manually_updated = models.CharField(max_length=10, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'indent_detail'
		unique_together = (('indent_order_number', 'atm_id'),)

class bank_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.CharField(max_length=100, blank=True, null=True)
	description = models.CharField(max_length=255, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=500, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'bank_master'


class project_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	description = models.CharField(max_length=255, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=500, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'project_master'

# class Default_loading(models.Model):
#     project_id = models.CharField(max_length=50, blank=True, null=True)
#     bank_name = models.CharField(max_length=50, blank=True, null=True)
#     bank_code = models.CharField(max_length=10, blank=True, null=True)
#     amount = models.IntegerField(blank=True, null=True)
#     record_status = models.CharField(max_length=50, blank=True, null=True)
#     created_on = models.DateTimeField(blank=True, null=True)
#     created_by = models.CharField(max_length=50, blank=True, null=True)
#     created_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     approved_on = models.DateTimeField(blank=True, null=True)
#     approved_by = models.CharField(max_length=50, blank=True, null=True)
#     approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
#     rejected_on = models.DateTimeField(blank=True, null=True)
#     rejected_by = models.CharField(max_length=50, blank=True, null=True)
#     reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     deleted_on = models.DateTimeField(blank=True, null=True)
#     deleted_by = models.CharField(max_length=50, blank=True, null=True)
#     deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     modified_on = models.DateTimeField(blank=True, null=True)
#     modified_by = models.CharField(max_length=50, blank=True, null=True)
#     modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     is_valid_record = models.CharField(max_length=20, blank=True, null=True)
#     error_code = models.CharField(max_length=50, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'Default_loading'

# class indent_eod_pre_activity(models.Model):
# 	project_id = models.CharField(max_length=50, blank=True, null=True)
# 	bank_code = models.CharField(max_length=10, blank=True, null=True)
# 	region_code = models.CharField(max_length=50, blank=True, null=True)
# 	branch_code = models.CharField(max_length=50, blank=True, null=True)
# 	atm_id = models.CharField(max_length=50, blank=True, null=True)
# 	site_code = models.CharField(max_length=50, blank=True, null=True)
# 	datafor_date_time = models.DateTimeField(blank=True, null=True)
# 	record_status = models.CharField(max_length=50, blank=True, null=True)
# 	created_on = models.DateTimeField(blank=True, null=True)
# 	created_by = models.CharField(max_length=50, blank=True, null=True)
# 	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approved_on = models.DateTimeField(blank=True, null=True)
# 	approved_by = models.CharField(max_length=50, blank=True, null=True)
# 	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
# 	rejected_on = models.DateTimeField(blank=True, null=True)
# 	rejected_by = models.CharField(max_length=50, blank=True, null=True)
# 	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	modified_on = models.DateTimeField(blank=True, null=True)
# 	modified_by = models.CharField(max_length=50, blank=True, null=True)
# 	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
# 	error_code = models.CharField(max_length=50, blank=True, null=True)
#
# 	class Meta:
# 		managed = False
# 		db_table = 'indent_eod_pre_activity'


# class indent_pre_qualify_atm(models.Model):
# 	project_id = models.CharField(max_length=50, blank=True, null=True)
# 	bank_code = models.CharField(max_length=10, blank=True, null=True)
# 	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
# 	site_code = models.CharField(max_length=50, blank=True, null=True)
# 	atm_id = models.CharField(max_length=50, blank=True, null=True)
# 	category = models.CharField(max_length=50, blank=True, null=True)
# 	is_qualified = models.CharField(max_length=20, blank=True, null=True)
# 	from_date = models.DateTimeField(blank=True, null=True)
# 	to_date = models.DateTimeField(blank=True, null=True)
# 	comment = models.CharField(max_length=100, blank=True, null=True)
# 	record_status = models.CharField(max_length=50, blank=True, null=True)
# 	created_on = models.DateTimeField(blank=True, null=True)
# 	created_by = models.CharField(max_length=50, blank=True, null=True)
# 	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approved_on = models.DateTimeField(blank=True, null=True)
# 	approved_by = models.CharField(max_length=50, blank=True, null=True)
# 	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
# 	rejected_on = models.DateTimeField(blank=True, null=True)
# 	rejected_by = models.CharField(max_length=50, blank=True, null=True)
# 	rejected_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	modified_on = models.DateTimeField(blank=True, null=True)
# 	modified_by = models.CharField(max_length=50, blank=True, null=True)
# 	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
# 	error_code = models.CharField(max_length=50, blank=True, null=True)
#
# 	class Meta:
# 		managed = False
# 		db_table = 'indent_pre_qualify_atm'

class menu_access(models.Model):
	id = models.BigAutoField(primary_key=True)
	menu_id = models.CharField(max_length=50)
	group_name = models.CharField(max_length=50)
	group_id = models.IntegerField(blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'menu_access'



class menu_master(models.Model):
	menu_id = models.CharField(primary_key=True, max_length=50)
	display_name = models.CharField(max_length=255)
	menu_description = models.CharField(max_length=255, blank=True, null=True)
	keyword = models.CharField(max_length=255, blank=True, null=True)
	mapped_url = models.CharField(max_length=255, blank=True, null=True)
	link_type = models.CharField(max_length=50, blank=True, null=True)
	parent_menu_id = models.CharField(max_length=50, blank=True, null=True)
	is_active = models.IntegerField(blank=True, null=True)
	menu_level = models.IntegerField(blank=True, null=True)
	seq_no = models.IntegerField(blank=True, null=True)
	icon_name = models.CharField(max_length=255, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'menu_master'


class feeder_branch_master(models.Model):
	bank_code = models.TextField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	region_code = models.CharField(max_length=50, blank=True, null=True)
	sol_id = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch = models.CharField(max_length=50, blank=True, null=True)
	district = models.TextField(blank=True, null=True)
	circle = models.TextField(blank=True, null=True)
	feeder_linked_count = models.TextField(blank=True, null=True)
	contact_details = models.TextField(blank=True, null=True)
	is_vaulting_enabled = models.TextField(blank=True, null=True)
	email_id = models.TextField(blank=True, null=True)
	alternate_cash_balance = models.TextField(blank=True, null=True)
	is_currency_chest = models.TextField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'feeder_branch_master'

class ims_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	ims_sync_master_id = models.BigIntegerField(blank=True, null=True)
	ticketno = models.CharField(max_length=50)
	vendorticketno = models.CharField(max_length=50, blank=True, null=True)
	site_code = models.CharField(max_length=100, blank=True, null=True)
	atmid = models.CharField(max_length=50)
	bankname = models.CharField(max_length=100, blank=True, null=True)
	ticket_generation_type = models.CharField(max_length=255, blank=True, null=True)
	site_details = models.CharField(max_length=255, blank=True, null=True)
	fault = models.CharField(max_length=255, blank=True, null=True)
	fault_description = models.CharField(max_length=255, blank=True, null=True)
	call_status = models.CharField(max_length=255, blank=True, null=True)
	ticket_status = models.CharField(max_length=255, blank=True, null=True)
	message_date_time = models.CharField(max_length=50, blank=True, null=True)
	open_date = models.DateField(blank=True, null=True)
	opentime = models.DateTimeField(blank=True, null=True)
	duration = models.CharField(max_length=50, blank=True, null=True)
	remarks = models.TextField(blank=True, null=True)
	latestremarks = models.TextField(blank=True, null=True)
	follow_up_date = models.TextField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.CharField(max_length=50, blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=100, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'ims_master'

class default_loading(models.Model):
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=10, blank=True, null=True)
	amount = models.IntegerField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'default_loading'

class atm_config_limits(models.Model):
	id = models.BigAutoField(primary_key=True)
	site_code = models.CharField(max_length=50, blank=True, null=True)
	old_atm_id = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=10, blank=True, null=True)
	insurance_limit = models.CharField(max_length=50, blank=True, null=True)
	whether_critical_atm = models.CharField(max_length=5, blank=True, null=True)
	cassette_50_count = models.IntegerField(blank=True, null=True)
	cassette_100_count = models.IntegerField(blank=True, null=True)
	cassette_200_count = models.IntegerField(blank=True, null=True)
	cassette_500_count = models.IntegerField(blank=True, null=True)
	cassette_2000_count = models.IntegerField(blank=True, null=True)
	total_cassette_count = models.IntegerField(blank=True, null=True)
	bank_cash_limit = models.IntegerField(blank=True, null=True)
	base_limit = models.IntegerField(blank=True, null=True)
	s_g_locker_no = models.CharField(max_length=50, blank=True, null=True)
	type_of_switch = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'atm_config_limits'

class cra_feasibility(models.Model):
	id = models.BigAutoField(primary_key=True)
	site_code = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50)
	project_id = models.CharField(max_length=50)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch_code = models.CharField(max_length=200, blank=True, null=True)
	new_cra = models.CharField(max_length=50, blank=True, null=True)
	feasibility_or_loading_frequency = models.CharField(max_length=150, blank=True, null=True)
	distance_from_hub_to_site = models.TextField(blank=True, null=True)
	distance_from_atm_site_to_nodal_branch = models.TextField(blank=True, null=True)
	distance_from_nodal_branch = models.TextField(blank=True, null=True)
	flm_tat = models.TextField(blank=True, null=True)
	cra_spoc = models.CharField(max_length=50, blank=True, null=True)
	cra_spoc_contact_no = models.CharField(max_length=50, blank=True, null=True)
	br_document_status = models.CharField(max_length=50, blank=True, null=True)
	cash_van = models.CharField(max_length=50, blank=True, null=True)
	gunman = models.TextField(blank=True, null=True)
	lc_nearest_hub_or_spoke_or_branch_from_site = models.CharField(max_length=50, blank=True, null=True)
	accessibility = models.CharField(max_length=200, blank=True, null=True)
	first_call_dispatch_time = models.CharField(max_length=50, blank=True, null=True)
	last_call_dispatch_time = models.CharField(max_length=50, blank=True, null=True)
	reason_for_limited_access = models.CharField(max_length=100, blank=True, null=True)
	vaulting = models.CharField(max_length=50, blank=True, null=True)
	feasibility_received_date = models.CharField(max_length=100, blank=True, null=True)
	feasibility_send_date = models.CharField(max_length=100, blank=True, null=True)
	br_request_date = models.CharField(max_length=100, blank=True, null=True)
	br_send_date = models.CharField(max_length=100, blank=True, null=True)
	is_feasible_mon = models.CharField(max_length=50, blank=True, null=True)
	is_feasible_tue = models.CharField(max_length=50, blank=True, null=True)
	is_feasible_wed = models.CharField(max_length=50, blank=True, null=True)
	is_feasible_thu = models.CharField(max_length=50, blank=True, null=True)
	is_feasible_fri = models.CharField(max_length=50, blank=True, null=True)
	is_feasible_sat = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cra_feasibility'

class eps_escalation_emails(models.Model):
	id = models.BigAutoField(primary_key=True)
	eps = models.CharField(max_length=50, blank=True, null=True)
	bank = models.CharField(max_length=100, blank=True, null=True)
	activity = models.CharField(max_length=100, blank=True, null=True)
	primary_email_id = models.CharField(max_length=100, blank=True, null=True)
	primary_contact_no = models.CharField(max_length=100, blank=True, null=True)
	level_1_email_id = models.CharField(max_length=100, blank=True, null=True)
	level_1_contact_no = models.CharField(max_length=100, blank=True, null=True)
	level_2_email_id = models.CharField(max_length=100, blank=True, null=True)
	level_2_contact_no = models.CharField(max_length=100, blank=True, null=True)
	level_3_email_id = models.CharField(max_length=100, blank=True, null=True)
	level_3_contact_no = models.CharField(max_length=100, blank=True, null=True)
	level_4_email_id = models.CharField(max_length=100, blank=True, null=True)
	level_4_contact_no = models.CharField(max_length=100, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'eps_escalation_emails'

class cra_escalation_emails(models.Model):
	id = models.BigAutoField(primary_key=True)
	cra = models.CharField(max_length=50, blank=True, null=True)
	location = models.CharField(max_length=50, blank=True, null=True)
	location_circle = models.CharField(max_length=50, blank=True, null=True)
	activity = models.CharField(max_length=100, blank=True, null=True)
	primary_email_id = models.CharField(max_length=50, blank=True, null=True)
	primary_contact_no = models.CharField(max_length=20, blank=True, null=True)
	level_1_email_id = models.CharField(max_length=150, blank=True, null=True)
	level_1_contact_no = models.CharField(max_length=20, blank=True, null=True)
	level_2_email_id = models.CharField(max_length=50, blank=True, null=True)
	level_2_contact_no = models.CharField(max_length=20, blank=True, null=True)
	level_3_email_id = models.CharField(max_length=50, blank=True, null=True)
	level_3_contact_no = models.CharField(max_length=20, blank=True, null=True)
	level_4_email_id = models.CharField(max_length=50, blank=True, null=True)
	level_4_contact_no = models.CharField(max_length=20, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cra_escalation_emails'

class Bank_Escalation_Emails(models.Model):
	id = models.BigAutoField(primary_key=True)
	bank = models.CharField(max_length=100, blank=True, null=True)
	branch = models.CharField(max_length=100, blank=True, null=True)
	activity = models.CharField(max_length=100, blank=True, null=True)
	primary_email_id = models.TextField(blank=True, null=True)
	primary_contact_no = models.CharField(max_length=100, blank=True, null=True)
	level_1_email_id = models.CharField(max_length=200, blank=True, null=True)
	level_1_contact_no = models.CharField(max_length=50, blank=True, null=True)
	level_2_email_id = models.CharField(max_length=200, blank=True, null=True)
	level_2_contact_no = models.CharField(max_length=50, blank=True, null=True)
	level_3_email_id = models.CharField(max_length=200, blank=True, null=True)
	level_3_contact_no = models.CharField(max_length=50, blank=True, null=True)
	level_4_email_id = models.CharField(max_length=200, blank=True, null=True)
	level_4_contact_no = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)
	sol_id = models.CharField(max_length=10, blank=True, null=True)
	circle = models.CharField(max_length=100, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'Bank_Escalation_Emails'


class Mail_Master(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=10, blank=True, null=True)
	feeder_branch = models.CharField(max_length=50, blank=True, null=True)
	sol_id = models.CharField(max_length=10, blank=True, null=True)
	cra = models.CharField(max_length=50, blank=True, null=True)
	from_email_id = models.CharField(max_length=100, blank=True, null=True)
	to_bank_email_id = models.TextField( blank=True, null=True)
	to_cra_email_id = models.TextField(db_column='to_CRA_email_id', blank=True, null=True)  # Field name made lowercase.
	bcc_email_id = models.TextField(blank=True, null=True)
	subject = models.CharField(max_length=100, blank=True, null=True)
	body_text = models.TextField(blank=True, null=True)
	state = models.CharField(max_length=50, blank=True, null=True)
	importance = models.CharField(max_length=50, blank=True, null=True)
	atm_count = models.CharField(max_length=50, blank=True, null=True)
	status = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	isvalidrecord = models.CharField(db_column='isValidRecord', max_length=20, blank=True, null=True)  # Field name made lowercase.
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'Mail_Master'



class indent_revision_request(models.Model):
    id = models.BigAutoField(primary_key=True)
    indent_revision_order_number = models.CharField(max_length=255)
    indent_order_number = models.CharField(max_length=255, blank=True, null=True)
    total_original_forecast_amount = models.BigIntegerField(blank=True, null=True)
    original_forecast_amount_50 = models.BigIntegerField(blank=True, null=True)
    original_forecast_amount_100 = models.BigIntegerField(blank=True, null=True)
    original_forecast_amount_200 = models.BigIntegerField(blank=True, null=True)
    original_forecast_amount_500 = models.BigIntegerField(blank=True, null=True)
    original_forecast_amount_2000 = models.BigIntegerField(blank=True, null=True)
    total_amount_available = models.BigIntegerField(blank=True, null=True)
    available_50_amount = models.BigIntegerField(blank=True, null=True)
    available_100_amount = models.BigIntegerField(blank=True, null=True)
    available_200_amount = models.BigIntegerField(blank=True, null=True)
    available_500_amount = models.BigIntegerField(blank=True, null=True)
    available_2000_amount = models.BigIntegerField(blank=True, null=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    bank_code = models.CharField(max_length=50, blank=True, null=True)
    feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
    is_withdrawal_done = models.IntegerField(blank=True, null=True)
    reason = models.CharField(max_length=500, blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    indent_short_code = models.CharField(max_length=15, blank=True, null=True)
    order_date = models.DateField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'indent_revision_request'

class modify_cassette_pre_config(models.Model):
	from_date = models.DateTimeField(blank=True, null=True)
	to_date = models.DateTimeField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=10, blank=True, null=True)
	site_code = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	cassette_50_count = models.FloatField(blank=True, null=True)
	cassette_100_count = models.FloatField(blank=True, null=True)
	cassette_200_count = models.FloatField(blank=True, null=True)
	cassette_500_count = models.FloatField(blank=True, null=True)
	cassette_2000_count = models.FloatField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'modify_cassette_pre_config'


class feeder_denomination_pre_availability(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=15, blank=True, null=True)
	bank_code = models.CharField(max_length=20, blank=True, null=True)
	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
	is_50_available = models.CharField(max_length=10, blank=True, null=True)
	is_100_available = models.CharField(max_length=10, blank=True, null=True)
	is_200_available = models.CharField(max_length=10, blank=True, null=True)
	is_500_available = models.CharField(max_length=10, blank=True, null=True)
	is_2000_available = models.CharField(max_length=10, blank=True, null=True)
	from_date = models.DateTimeField(blank=True, null=True)
	to_date = models.DateTimeField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=10, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'feeder_denomination_pre_availability'


# class distribution_planning_detail(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     indent_code = models.CharField(max_length=255, blank=True, null=True)
#     atm_id = models.CharField(max_length=50, blank=True, null=True)
#     site_code = models.CharField(max_length=50, blank=True, null=True)
#     sol_id = models.CharField(max_length=50, blank=True, null=True)
#     bank_code = models.CharField(max_length=50, blank=True, null=True)
#     project_id = models.CharField(max_length=50, blank=True, null=True)
#     indentdate = models.DateTimeField(blank=True, null=True)
#     bank_cash_limit = models.IntegerField(blank=True, null=True)
#     insurance_limit = models.CharField(max_length=50, blank=True, null=True)
#     feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
#     cra = models.CharField(max_length=50, blank=True, null=True)
#     base_limit = models.IntegerField(blank=True, null=True)
#     dispenseformula = models.CharField(max_length=50, blank=True, null=True)
#     decidelimitdays = models.IntegerField(blank=True, null=True)
#     loadinglimitdays = models.IntegerField(blank=True, null=True)
#     avgdispense = models.IntegerField(blank=True, null=True)
#     loading_amount = models.FloatField(blank=True, null=True)
#     morning_balance_50 = models.BigIntegerField(blank=True, null=True)
#     morning_balance_100 = models.BigIntegerField(blank=True, null=True)
#     morning_balance_200 = models.BigIntegerField(blank=True, null=True)
#     morning_balance_500 = models.BigIntegerField(blank=True, null=True)
#     morning_balance_2000 = models.BigIntegerField(blank=True, null=True)
#     total_morning_balance = models.BigIntegerField(blank=True, null=True)
#     cassette_50_brand_capacity = models.IntegerField(blank=True, null=True)
#     cassette_100_brand_capacity = models.IntegerField(blank=True, null=True)
#     cassette_200_brand_capacity = models.IntegerField(blank=True, null=True)
#     cassette_500_brand_capacity = models.IntegerField(blank=True, null=True)
#     cassette_2000_brand_capacity = models.IntegerField(blank=True, null=True)
#     cassette_50_count = models.FloatField(blank=True, null=True)
#     cassette_100_count = models.FloatField(blank=True, null=True)
#     cassette_200_count = models.FloatField(blank=True, null=True)
#     cassette_500_count = models.FloatField(blank=True, null=True)
#     cassette_2000_count = models.FloatField(blank=True, null=True)
#     limit_amount = models.IntegerField(blank=True, null=True)
#     total_capacity_amount_50 = models.FloatField(blank=True, null=True)
#     total_capacity_amount_100 = models.FloatField(blank=True, null=True)
#     total_capacity_amount_200 = models.FloatField(blank=True, null=True)
#     total_capacity_amount_500 = models.FloatField(blank=True, null=True)
#     total_capacity_amount_2000 = models.FloatField(blank=True, null=True)
#     total_cassette_capacity = models.FloatField(blank=True, null=True)
#     avgdecidelimit = models.IntegerField(blank=True, null=True)
#     decidelimit = models.FloatField(db_column='DecideLimit', blank=True, null=True)  # Field name made lowercase.
#     threshold_limit = models.IntegerField(blank=True, null=True)
#     loadinglimit = models.FloatField(blank=True, null=True)
#     applied_vaulting_percentage = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
#     ignore_code = models.IntegerField(blank=True, null=True)
#     ignore_description = models.CharField(max_length=255, blank=True, null=True)
#     dist_purpose = models.CharField(db_column='dist_Purpose', max_length=8, blank=True, null=True)  # Field name made lowercase.
#     defaultamt = models.IntegerField(blank=True, null=True)
#     default_flag = models.IntegerField(blank=True, null=True)
#     curbal_div_avgdisp = models.FloatField(db_column='curbal_div_avgDisp', blank=True, null=True)  # Field name made lowercase.
#     loadinggap_cur_bal_avg_disp = models.BigIntegerField(db_column='loadingGap_cur_bal_avg_disp', blank=True, null=True)  # Field name made lowercase.
#     cashout = models.IntegerField(db_column='CashOut', blank=True, null=True)  # Field name made lowercase.
#     remaining_balance_50 = models.BigIntegerField(blank=True, null=True)
#     remaining_balance_100 = models.BigIntegerField(blank=True, null=True)
#     remaining_balance_200 = models.BigIntegerField(blank=True, null=True)
#     remaining_balance_500 = models.BigIntegerField(blank=True, null=True)
#     remaining_balance_2000 = models.BigIntegerField(blank=True, null=True)
#     cassette_50_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_100_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_200_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_500_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_2000_capacity_amount = models.FloatField(blank=True, null=True)
#     total_cassette_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_capacity_percentage_50 = models.FloatField(blank=True, null=True)
#     cassette_capacity_percentage_100 = models.FloatField(blank=True, null=True)
#     cassette_capacity_percentage_200 = models.FloatField(blank=True, null=True)
#     cassette_capacity_percentage_500 = models.FloatField(blank=True, null=True)
#     cassette_capacity_percentage_2000 = models.FloatField(blank=True, null=True)
#     tentative_loading_amount_50 = models.FloatField(blank=True, null=True)
#     tentative_loading_amount_100 = models.FloatField(blank=True, null=True)
#     tentative_loading_amount_200 = models.FloatField(blank=True, null=True)
#     tentative_loading_amount_500 = models.FloatField(blank=True, null=True)
#     tentative_loading_amount_2000 = models.FloatField(blank=True, null=True)
#     rounded_amount_50 = models.FloatField(blank=True, null=True)
#     rounded_amount_100 = models.IntegerField(blank=True, null=True)
#     rounded_amount_200 = models.IntegerField(blank=True, null=True)
#     rounded_amount_500 = models.IntegerField(blank=True, null=True)
#     rounded_amount_2000 = models.IntegerField(blank=True, null=True)
#     total_expected_balancet1 = models.FloatField(db_column='total_expected_balanceT1', blank=True, null=True)  # Field name made lowercase.
#     expected_balancet1_50 = models.FloatField(db_column='expected_balanceT1_50', blank=True, null=True)  # Field name made lowercase.
#     expected_balancet1_100 = models.FloatField(db_column='expected_balanceT1_100', blank=True, null=True)  # Field name made lowercase.
#     expected_balancet1_200 = models.FloatField(db_column='expected_balanceT1_200', blank=True, null=True)  # Field name made lowercase.
#     expected_balancet1_500 = models.FloatField(db_column='expected_balanceT1_500', blank=True, null=True)  # Field name made lowercase.
#     expected_balancet1_2000 = models.FloatField(db_column='expected_balanceT1_2000', blank=True, null=True)  # Field name made lowercase.
#     total_vault_amount = models.FloatField(blank=True, null=True)
#     vaultingamount_50 = models.FloatField(blank=True, null=True)
#     vaultingamount_100 = models.FloatField(blank=True, null=True)
#     vaultingamount_200 = models.FloatField(blank=True, null=True)
#     vaultingamount_500 = models.FloatField(blank=True, null=True)
#     vaultingamount_2000 = models.FloatField(blank=True, null=True)
#     rounded_vaultingamount_50 = models.FloatField(blank=True, null=True)
#     rounded_vaultingamount_100 = models.IntegerField(blank=True, null=True)
#     rounded_vaultingamount_200 = models.IntegerField(blank=True, null=True)
#     rounded_vaultingamount_500 = models.IntegerField(blank=True, null=True)
#     rounded_vaultingamount_2000 = models.IntegerField(blank=True, null=True)
#     total_rounded_vault_amt = models.FloatField(blank=True, null=True)
#     record_status = models.CharField(max_length=50, blank=True, null=True)
#     created_on = models.DateTimeField(blank=True, null=True)
#     created_by = models.CharField(max_length=50, blank=True, null=True)
#     created_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     approved_on = models.DateTimeField(blank=True, null=True)
#     approved_by = models.CharField(max_length=50, blank=True, null=True)
#     approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
#     rejected_on = models.DateTimeField(blank=True, null=True)
#     rejected_by = models.CharField(max_length=50, blank=True, null=True)
#     reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     deleted_on = models.DateTimeField(blank=True, null=True)
#     deleted_by = models.CharField(max_length=50, blank=True, null=True)
#     deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     modified_on = models.DateTimeField(blank=True, null=True)
#     modified_by = models.CharField(max_length=50, blank=True, null=True)
#     modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     indent_counter = models.BigIntegerField(blank=True, null=True)
#     total_opening_remaining_available_amount = models.BigIntegerField(blank=True, null=True)
#     opening_remaining_available_amount_100 = models.BigIntegerField(blank=True, null=True)
#     opening_remaining_available_amount_200 = models.BigIntegerField(blank=True, null=True)
#     opening_remaining_available_amount_500 = models.BigIntegerField(blank=True, null=True)
#     opening_remaining_available_amount_2000 = models.BigIntegerField(blank=True, null=True)
#     cassette_100_count_original = models.BigIntegerField(blank=True, null=True)
#     cassette_200_count_original = models.BigIntegerField(blank=True, null=True)
#     cassette_500_count_original = models.BigIntegerField(blank=True, null=True)
#     cassette_2000_count_original = models.BigIntegerField(blank=True, null=True)
#     denomination_100_max_capacity_percentage = models.BigIntegerField(blank=True, null=True)
#     denomination_200_max_capacity_percentage = models.BigIntegerField(blank=True, null=True)
#     denomination_500_max_capacity_percentage = models.BigIntegerField(blank=True, null=True)
#     denomination_2000_max_capacity_percentage = models.BigIntegerField(blank=True, null=True)
#     max_amt_allowed_100 = models.BigIntegerField(blank=True, null=True)
#     max_amt_allowed_200 = models.BigIntegerField(blank=True, null=True)
#     max_amt_allowed_500 = models.BigIntegerField(blank=True, null=True)
#     max_amt_allowed_2000 = models.BigIntegerField(blank=True, null=True)
#     denomination_wise_round_off_100 = models.BigIntegerField(blank=True, null=True)
#     denomination_wise_round_off_200 = models.BigIntegerField(blank=True, null=True)
#     denomination_wise_round_off_500 = models.BigIntegerField(blank=True, null=True)
#     denomination_wise_round_off_2000 = models.BigIntegerField(blank=True, null=True)
#     tentative_loading_100 = models.BigIntegerField(blank=True, null=True)
#     tentative_loading_200 = models.BigIntegerField(blank=True, null=True)
#     tentative_loading_500 = models.BigIntegerField(blank=True, null=True)
#     tentative_loading_2000 = models.BigIntegerField(blank=True, null=True)
#     rounded_tentative_loading_100 = models.BigIntegerField(blank=True, null=True)
#     rounded_tentative_loading_200 = models.BigIntegerField(blank=True, null=True)
#     rounded_tentative_loading_500 = models.BigIntegerField(blank=True, null=True)
#     rounded_tentative_loading_2000 = models.BigIntegerField(blank=True, null=True)
#     deno_100_priority = models.IntegerField(blank=True, null=True)
#     deno_200_priority = models.IntegerField(blank=True, null=True)
#     deno_500_priority = models.IntegerField(blank=True, null=True)
#     deno_2000_priority = models.IntegerField(blank=True, null=True)
#     is_deno_wise_cash_available = models.IntegerField(blank=True, null=True)
#     priority_1_is_denomination_100 = models.IntegerField(blank=True, null=True)
#     priority_1_is_denomination_200 = models.IntegerField(blank=True, null=True)
#     priority_1_is_denomination_500 = models.IntegerField(blank=True, null=True)
#     priority_1_is_denomination_2000 = models.IntegerField(blank=True, null=True)
#     priority_1_is_remaining_amount_available_100 = models.IntegerField(blank=True, null=True)
#     priority_1_is_remaining_amount_available_200 = models.IntegerField(blank=True, null=True)
#     priority_1_is_remaining_amount_available_500 = models.IntegerField(blank=True, null=True)
#     priority_1_is_remaining_amount_available_2000 = models.IntegerField(blank=True, null=True)
#     priority_1_is_remaining_capacity_available_100 = models.IntegerField(blank=True, null=True)
#     priority_1_is_remaining_capacity_available_200 = models.IntegerField(blank=True, null=True)
#     priority_1_is_remaining_capacity_available_500 = models.IntegerField(blank=True, null=True)
#     priority_1_is_remaining_capacity_available_2000 = models.IntegerField(blank=True, null=True)
#     priority_1_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
#     priority_1_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
#     priority_1_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
#     priority_1_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
#     priority_2_is_denomination_100 = models.IntegerField(blank=True, null=True)
#     priority_2_is_denomination_200 = models.IntegerField(blank=True, null=True)
#     priority_2_is_denomination_500 = models.IntegerField(blank=True, null=True)
#     priority_2_is_denomination_2000 = models.IntegerField(blank=True, null=True)
#     priority_2_is_remaining_amount_available_100 = models.IntegerField(blank=True, null=True)
#     priority_2_is_remaining_amount_available_200 = models.IntegerField(blank=True, null=True)
#     priority_2_is_remaining_amount_available_500 = models.IntegerField(blank=True, null=True)
#     priority_2_is_remaining_amount_available_2000 = models.IntegerField(blank=True, null=True)
#     priority_2_is_remaining_capacity_available_100 = models.IntegerField(blank=True, null=True)
#     priority_2_is_remaining_capacity_available_200 = models.IntegerField(blank=True, null=True)
#     priority_2_is_remaining_capacity_available_500 = models.IntegerField(blank=True, null=True)
#     priority_2_is_remaining_capacity_available_2000 = models.IntegerField(blank=True, null=True)
#     priority_2_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
#     priority_2_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
#     priority_2_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
#     priority_2_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
#     priority_3_is_denomination_100 = models.IntegerField(blank=True, null=True)
#     priority_3_is_denomination_200 = models.IntegerField(blank=True, null=True)
#     priority_3_is_denomination_500 = models.IntegerField(blank=True, null=True)
#     priority_3_is_denomination_2000 = models.IntegerField(blank=True, null=True)
#     priority_3_is_remaining_amount_available_100 = models.IntegerField(blank=True, null=True)
#     priority_3_is_remaining_amount_available_200 = models.IntegerField(blank=True, null=True)
#     priority_3_is_remaining_amount_available_500 = models.IntegerField(blank=True, null=True)
#     priority_3_is_remaining_amount_available_2000 = models.IntegerField(blank=True, null=True)
#     priority_3_is_remaining_capacity_available_100 = models.IntegerField(blank=True, null=True)
#     priority_3_is_remaining_capacity_available_200 = models.IntegerField(blank=True, null=True)
#     priority_3_is_remaining_capacity_available_500 = models.IntegerField(blank=True, null=True)
#     priority_3_is_remaining_capacity_available_2000 = models.IntegerField(blank=True, null=True)
#     priority_3_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
#     priority_3_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
#     priority_3_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
#     priority_3_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
#     priority_4_is_denomination_100 = models.IntegerField(blank=True, null=True)
#     priority_4_is_denomination_200 = models.IntegerField(blank=True, null=True)
#     priority_4_is_denomination_500 = models.IntegerField(blank=True, null=True)
#     priority_4_is_denomination_2000 = models.IntegerField(blank=True, null=True)
#     priority_4_is_remaining_amount_available_100 = models.IntegerField(blank=True, null=True)
#     priority_4_is_remaining_amount_available_200 = models.IntegerField(blank=True, null=True)
#     priority_4_is_remaining_amount_available_500 = models.IntegerField(blank=True, null=True)
#     priority_4_is_remaining_amount_available_2000 = models.IntegerField(blank=True, null=True)
#     priority_4_is_remaining_capacity_available_100 = models.IntegerField(blank=True, null=True)
#     priority_4_is_remaining_capacity_available_200 = models.IntegerField(blank=True, null=True)
#     priority_4_is_remaining_capacity_available_500 = models.IntegerField(blank=True, null=True)
#     priority_4_is_remaining_capacity_available_2000 = models.IntegerField(blank=True, null=True)
#     priority_4_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
#     priority_4_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
#     priority_4_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
#     priority_4_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
#     loading_amount_100 = models.BigIntegerField(blank=True, null=True)
#     loading_amount_200 = models.BigIntegerField(blank=True, null=True)
#     loading_amount_500 = models.BigIntegerField(blank=True, null=True)
#     loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
#     total_loading_amount = models.BigIntegerField(blank=True, null=True)
#     remaining_capacity_amount_100 = models.BigIntegerField(blank=True, null=True)
#     remaining_capacity_amount_200 = models.BigIntegerField(blank=True, null=True)
#     remaining_capacity_amount_500 = models.BigIntegerField(blank=True, null=True)
#     remaining_capacity_amount_2000 = models.BigIntegerField(blank=True, null=True)
#     closing_remaining_available_amount_100 = models.BigIntegerField(blank=True, null=True)
#     closing_remaining_available_amount_200 = models.BigIntegerField(blank=True, null=True)
#     closing_remaining_available_amount_500 = models.BigIntegerField(blank=True, null=True)
#     closing_remaining_available_amount_2000 = models.BigIntegerField(blank=True, null=True)
#     total_closing_remaining_available_amount = models.BigIntegerField(blank=True, null=True)
#     total_forecasted_remaining_amt = models.BigIntegerField(blank=True, null=True)
#     previous_indent_code = models.CharField(max_length=255, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'distribution_planning_detail'


# class distribution_planning_master(models.Model):
#     id = models.BigAutoField(primary_key=True)
#     indent_code = models.CharField(max_length=255, blank=True, null=True)
#     indentdate = models.DateTimeField(blank=True, null=True)
#     project_id = models.CharField(max_length=50, blank=True, null=True)
#     bank_code = models.CharField(max_length=50, blank=True, null=True)
#     feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
#     cra = models.CharField(max_length=50, blank=True, null=True)
#     max_loading_amount = models.FloatField(blank=True, null=True)
#     max_loading_amount_50 = models.FloatField(blank=True, null=True)
#     max_loading_amount_100 = models.FloatField(blank=True, null=True)
#     max_loading_amount_200 = models.FloatField(blank=True, null=True)
#     max_loading_amount_500 = models.FloatField(blank=True, null=True)
#     max_loading_amount_2000 = models.FloatField(blank=True, null=True)
#     min_loading_amount = models.DecimalField(max_digits=38, decimal_places=1, blank=True, null=True)
#     forecast_loading_amount = models.FloatField(blank=True, null=True)
#     forecast_loading_amount_50 = models.FloatField(blank=True, null=True)
#     forecast_loading_amount_100 = models.IntegerField(blank=True, null=True)
#     forecast_loading_amount_200 = models.IntegerField(blank=True, null=True)
#     forecast_loading_amount_500 = models.IntegerField(blank=True, null=True)
#     forecast_loading_amount_2000 = models.IntegerField(blank=True, null=True)
#     total_cassette_capacity = models.FloatField(blank=True, null=True)
#     cassette_50_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_100_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_200_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_500_capacity_amount = models.FloatField(blank=True, null=True)
#     cassette_2000_capacity_amount = models.FloatField(blank=True, null=True)
#     vaultingamount_50 = models.FloatField(blank=True, null=True)
#     vaultingamount_100 = models.IntegerField(blank=True, null=True)
#     vaultingamount_200 = models.IntegerField(blank=True, null=True)
#     vaultingamount_500 = models.IntegerField(blank=True, null=True)
#     vaultingamount_2000 = models.IntegerField(blank=True, null=True)
#     total_rounded_vault_amt = models.FloatField(blank=True, null=True)
#     vault_balance_100 = models.IntegerField(blank=True, null=True)
#     vault_balance_200 = models.IntegerField(blank=True, null=True)
#     vault_balance_500 = models.IntegerField(blank=True, null=True)
#     vault_balance_2000 = models.IntegerField(blank=True, null=True)
#     total_vault_balance = models.IntegerField(blank=True, null=True)
#     indent_50 = models.FloatField(blank=True, null=True)
#     indent_100 = models.IntegerField(blank=True, null=True)
#     indent_200 = models.IntegerField(blank=True, null=True)
#     indent_500 = models.IntegerField(blank=True, null=True)
#     indent_2000 = models.IntegerField(blank=True, null=True)
#     record_status = models.CharField(max_length=50, blank=True, null=True)
#     created_on = models.DateTimeField(blank=True, null=True)
#     created_by = models.CharField(max_length=50, blank=True, null=True)
#     created_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     approved_on = models.DateTimeField(blank=True, null=True)
#     approved_by = models.CharField(max_length=50, blank=True, null=True)
#     approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
#     rejected_on = models.DateTimeField(blank=True, null=True)
#     rejected_by = models.CharField(max_length=50, blank=True, null=True)
#     reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     deleted_on = models.DateTimeField(blank=True, null=True)
#     deleted_by = models.CharField(max_length=50, blank=True, null=True)
#     deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     modified_on = models.DateTimeField(blank=True, null=True)
#     modified_by = models.CharField(max_length=50, blank=True, null=True)
#     modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
#     indent_counter = models.BigIntegerField(blank=True, null=True)
#     is_deno_wise_cash_available = models.IntegerField(blank=True, null=True)
#     total_remaining_available_amount = models.BigIntegerField(blank=True, null=True)
#     remaining_avail_100 = models.BigIntegerField(blank=True, null=True)
#     remaining_avail_200 = models.BigIntegerField(blank=True, null=True)
#     remaining_avail_500 = models.BigIntegerField(blank=True, null=True)
#     remaining_avail_2000 = models.BigIntegerField(blank=True, null=True)
#     previous_indent_code = models.CharField(max_length=255, blank=True, null=True)
#
#     class Meta:
#         managed = False
#         db_table = 'distribution_planning_master'

class distribution_planning_detail_V2(models.Model):
	id = models.BigAutoField(primary_key=True)
	indent_code = models.CharField(max_length=255, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	site_code = models.CharField(max_length=50, blank=True, null=True)
	sol_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	indentdate = models.DateTimeField(blank=True, null=True)
	bank_cash_limit = models.IntegerField(blank=True, null=True)
	insurance_limit = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
	cra = models.CharField(max_length=50, blank=True, null=True)
	base_limit = models.IntegerField(blank=True, null=True)
	dispenseformula = models.CharField(max_length=50, blank=True, null=True)
	decidelimitdays = models.FloatField(blank=True, null=True)
	loadinglimitdays = models.FloatField(blank=True, null=True)
	avgdispense = models.IntegerField(blank=True, null=True)
	loading_amount = models.FloatField(blank=True, null=True)
	morning_balance_50 = models.BigIntegerField(blank=True, null=True)
	morning_balance_100 = models.BigIntegerField(blank=True, null=True)
	morning_balance_200 = models.BigIntegerField(blank=True, null=True)
	morning_balance_500 = models.BigIntegerField(blank=True, null=True)
	morning_balance_2000 = models.BigIntegerField(blank=True, null=True)
	total_morning_balance = models.BigIntegerField(blank=True, null=True)
	cassette_50_brand_capacity = models.IntegerField(blank=True, null=True)
	cassette_100_brand_capacity = models.IntegerField(blank=True, null=True)
	cassette_200_brand_capacity = models.IntegerField(blank=True, null=True)
	cassette_500_brand_capacity = models.IntegerField(blank=True, null=True)
	cassette_2000_brand_capacity = models.IntegerField(blank=True, null=True)
	cassette_50_count = models.FloatField(blank=True, null=True)
	cassette_100_count = models.FloatField(blank=True, null=True)
	cassette_200_count = models.FloatField(blank=True, null=True)
	cassette_500_count = models.FloatField(blank=True, null=True)
	cassette_2000_count = models.FloatField(blank=True, null=True)
	limit_amount = models.IntegerField(blank=True, null=True)
	total_capacity_amount_50 = models.FloatField(blank=True, null=True)
	total_capacity_amount_100 = models.FloatField(blank=True, null=True)
	total_capacity_amount_200 = models.FloatField(blank=True, null=True)
	total_capacity_amount_500 = models.FloatField(blank=True, null=True)
	total_capacity_amount_2000 = models.FloatField(blank=True, null=True)
	total_cassette_capacity = models.FloatField(blank=True, null=True)
	avgdecidelimit = models.IntegerField(blank=True, null=True)
	decidelimit = models.FloatField(db_column='DecideLimit', blank=True, null=True)  # Field name made lowercase.
	threshold_limit = models.IntegerField(blank=True, null=True)
	loadinglimit = models.FloatField(blank=True, null=True)
	applied_vaulting_percentage = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
	ignore_code = models.IntegerField(blank=True, null=True)
	ignore_description = models.CharField(max_length=255, blank=True, null=True)
	dist_purpose = models.CharField(db_column='dist_Purpose', max_length=8, blank=True, null=True)  # Field name made lowercase.
	defaultamt = models.IntegerField(blank=True, null=True)
	default_flag = models.IntegerField(blank=True, null=True)
	curbal_div_avgdisp = models.FloatField(db_column='curbal_div_avgDisp', blank=True, null=True)  # Field name made lowercase.
	loadinggap_cur_bal_avg_disp = models.BigIntegerField(db_column='loadingGap_cur_bal_avg_disp', blank=True, null=True)  # Field name made lowercase.
	cashout = models.IntegerField(db_column='CashOut', blank=True, null=True)  # Field name made lowercase.
	remaining_balance_50 = models.BigIntegerField(blank=True, null=True)
	remaining_balance_100 = models.BigIntegerField(blank=True, null=True)
	remaining_balance_200 = models.BigIntegerField(blank=True, null=True)
	remaining_balance_500 = models.BigIntegerField(blank=True, null=True)
	remaining_balance_2000 = models.BigIntegerField(blank=True, null=True)
	cassette_capacity_percentage_50 = models.FloatField(blank=True, null=True)
	cassette_capacity_percentage_100 = models.FloatField(blank=True, null=True)
	cassette_capacity_percentage_200 = models.FloatField(blank=True, null=True)
	cassette_capacity_percentage_500 = models.FloatField(blank=True, null=True)
	cassette_capacity_percentage_2000 = models.FloatField(blank=True, null=True)
	max_loading_capacity_amount_100 = models.BigIntegerField(blank=True, null=True)
	max_loading_capacity_amount_200 = models.BigIntegerField(blank=True, null=True)
	max_loading_capacity_amount_500 = models.BigIntegerField(blank=True, null=True)
	max_loading_capacity_amount_2000 = models.BigIntegerField(blank=True, null=True)
	rounded_amount_100 = models.IntegerField(blank=True, null=True)
	rounded_amount_200 = models.IntegerField(blank=True, null=True)
	rounded_amount_500 = models.IntegerField(blank=True, null=True)
	rounded_amount_2000 = models.IntegerField(blank=True, null=True)
	total_expected_balancet1 = models.FloatField(db_column='total_expected_balanceT1', blank=True, null=True)  # Field name made lowercase.
	expected_balancet1_50 = models.FloatField(db_column='expected_balanceT1_50', blank=True, null=True)  # Field name made lowercase.
	expected_balancet1_100 = models.FloatField(db_column='expected_balanceT1_100', blank=True, null=True)  # Field name made lowercase.
	expected_balancet1_200 = models.FloatField(db_column='expected_balanceT1_200', blank=True, null=True)  # Field name made lowercase.
	expected_balancet1_500 = models.FloatField(db_column='expected_balanceT1_500', blank=True, null=True)  # Field name made lowercase.
	expected_balancet1_2000 = models.FloatField(db_column='expected_balanceT1_2000', blank=True, null=True)  # Field name made lowercase.
	total_vault_amount = models.FloatField(blank=True, null=True)
	vaultingamount_50 = models.FloatField(blank=True, null=True)
	vaultingamount_100 = models.FloatField(blank=True, null=True)
	vaultingamount_200 = models.FloatField(blank=True, null=True)
	vaultingamount_500 = models.FloatField(blank=True, null=True)
	vaultingamount_2000 = models.FloatField(blank=True, null=True)
	rounded_vaultingamount_50 = models.FloatField(blank=True, null=True)
	rounded_vaultingamount_100 = models.IntegerField(blank=True, null=True)
	rounded_vaultingamount_200 = models.IntegerField(blank=True, null=True)
	rounded_vaultingamount_500 = models.IntegerField(blank=True, null=True)
	rounded_vaultingamount_2000 = models.IntegerField(blank=True, null=True)
	total_rounded_vault_amt = models.FloatField(blank=True, null=True)
	total_opening_remaining_available_amount = models.BigIntegerField(blank=True, null=True)
	opening_remaining_available_amount_100 = models.BigIntegerField(blank=True, null=True)
	opening_remaining_available_amount_200 = models.BigIntegerField(blank=True, null=True)
	opening_remaining_available_amount_500 = models.BigIntegerField(blank=True, null=True)
	opening_remaining_available_amount_2000 = models.BigIntegerField(blank=True, null=True)
	cassette_100_count_original = models.BigIntegerField(blank=True, null=True)
	cassette_200_count_original = models.BigIntegerField(blank=True, null=True)
	cassette_500_count_original = models.BigIntegerField(blank=True, null=True)
	cassette_2000_count_original = models.BigIntegerField(blank=True, null=True)
	denomination_100_max_capacity_percentage = models.BigIntegerField(blank=True, null=True)
	denomination_200_max_capacity_percentage = models.BigIntegerField(blank=True, null=True)
	denomination_500_max_capacity_percentage = models.BigIntegerField(blank=True, null=True)
	denomination_2000_max_capacity_percentage = models.BigIntegerField(blank=True, null=True)
	max_amt_allowed_100 = models.BigIntegerField(blank=True, null=True)
	max_amt_allowed_200 = models.BigIntegerField(blank=True, null=True)
	max_amt_allowed_500 = models.BigIntegerField(blank=True, null=True)
	max_amt_allowed_2000 = models.BigIntegerField(blank=True, null=True)
	denomination_wise_round_off_100 = models.BigIntegerField(blank=True, null=True)
	denomination_wise_round_off_200 = models.BigIntegerField(blank=True, null=True)
	denomination_wise_round_off_500 = models.BigIntegerField(blank=True, null=True)
	denomination_wise_round_off_2000 = models.BigIntegerField(blank=True, null=True)
	rounded_tentative_loading_100 = models.BigIntegerField(blank=True, null=True)
	rounded_tentative_loading_200 = models.BigIntegerField(blank=True, null=True)
	rounded_tentative_loading_500 = models.BigIntegerField(blank=True, null=True)
	rounded_tentative_loading_2000 = models.BigIntegerField(blank=True, null=True)
	deno_100_priority = models.IntegerField(blank=True, null=True)
	deno_200_priority = models.IntegerField(blank=True, null=True)
	deno_500_priority = models.IntegerField(blank=True, null=True)
	deno_2000_priority = models.IntegerField(blank=True, null=True)
	is_deno_wise_cash_available = models.IntegerField(blank=True, null=True)
	priority_1_is_denomination_100 = models.IntegerField(blank=True, null=True)
	priority_1_is_denomination_200 = models.IntegerField(blank=True, null=True)
	priority_1_is_denomination_500 = models.IntegerField(blank=True, null=True)
	priority_1_is_denomination_2000 = models.IntegerField(blank=True, null=True)
	priority_1_is_remaining_amount_available_100 = models.IntegerField(blank=True, null=True)
	priority_1_is_remaining_amount_available_200 = models.IntegerField(blank=True, null=True)
	priority_1_is_remaining_amount_available_500 = models.IntegerField(blank=True, null=True)
	priority_1_is_remaining_amount_available_2000 = models.IntegerField(blank=True, null=True)
	priority_1_is_remaining_capacity_available_100 = models.IntegerField(blank=True, null=True)
	priority_1_is_remaining_capacity_available_200 = models.IntegerField(blank=True, null=True)
	priority_1_is_remaining_capacity_available_500 = models.IntegerField(blank=True, null=True)
	priority_1_is_remaining_capacity_available_2000 = models.IntegerField(blank=True, null=True)
	priority_1_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
	priority_1_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
	priority_1_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
	priority_1_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
	priority_2_is_denomination_100 = models.IntegerField(blank=True, null=True)
	priority_2_is_denomination_200 = models.IntegerField(blank=True, null=True)
	priority_2_is_denomination_500 = models.IntegerField(blank=True, null=True)
	priority_2_is_denomination_2000 = models.IntegerField(blank=True, null=True)
	priority_2_is_remaining_amount_available_100 = models.IntegerField(blank=True, null=True)
	priority_2_is_remaining_amount_available_200 = models.IntegerField(blank=True, null=True)
	priority_2_is_remaining_amount_available_500 = models.IntegerField(blank=True, null=True)
	priority_2_is_remaining_amount_available_2000 = models.IntegerField(blank=True, null=True)
	priority_2_is_remaining_capacity_available_100 = models.IntegerField(blank=True, null=True)
	priority_2_is_remaining_capacity_available_200 = models.IntegerField(blank=True, null=True)
	priority_2_is_remaining_capacity_available_500 = models.IntegerField(blank=True, null=True)
	priority_2_is_remaining_capacity_available_2000 = models.IntegerField(blank=True, null=True)
	priority_2_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
	priority_2_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
	priority_2_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
	priority_2_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
	priority_3_is_denomination_100 = models.IntegerField(blank=True, null=True)
	priority_3_is_denomination_200 = models.IntegerField(blank=True, null=True)
	priority_3_is_denomination_500 = models.IntegerField(blank=True, null=True)
	priority_3_is_denomination_2000 = models.IntegerField(blank=True, null=True)
	priority_3_is_remaining_amount_available_100 = models.IntegerField(blank=True, null=True)
	priority_3_is_remaining_amount_available_200 = models.IntegerField(blank=True, null=True)
	priority_3_is_remaining_amount_available_500 = models.IntegerField(blank=True, null=True)
	priority_3_is_remaining_amount_available_2000 = models.IntegerField(blank=True, null=True)
	priority_3_is_remaining_capacity_available_100 = models.IntegerField(blank=True, null=True)
	priority_3_is_remaining_capacity_available_200 = models.IntegerField(blank=True, null=True)
	priority_3_is_remaining_capacity_available_500 = models.IntegerField(blank=True, null=True)
	priority_3_is_remaining_capacity_available_2000 = models.IntegerField(blank=True, null=True)
	priority_3_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
	priority_3_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
	priority_3_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
	priority_3_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
	priority_4_is_denomination_100 = models.IntegerField(blank=True, null=True)
	priority_4_is_denomination_200 = models.IntegerField(blank=True, null=True)
	priority_4_is_denomination_500 = models.IntegerField(blank=True, null=True)
	priority_4_is_denomination_2000 = models.IntegerField(blank=True, null=True)
	priority_4_is_remaining_amount_available_100 = models.IntegerField(blank=True, null=True)
	priority_4_is_remaining_amount_available_200 = models.IntegerField(blank=True, null=True)
	priority_4_is_remaining_amount_available_500 = models.IntegerField(blank=True, null=True)
	priority_4_is_remaining_amount_available_2000 = models.IntegerField(blank=True, null=True)
	priority_4_is_remaining_capacity_available_100 = models.IntegerField(blank=True, null=True)
	priority_4_is_remaining_capacity_available_200 = models.IntegerField(blank=True, null=True)
	priority_4_is_remaining_capacity_available_500 = models.IntegerField(blank=True, null=True)
	priority_4_is_remaining_capacity_available_2000 = models.IntegerField(blank=True, null=True)
	priority_4_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
	priority_4_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
	priority_4_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
	priority_4_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
	pre_avail_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
	pre_avail_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
	pre_avail_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
	pre_avail_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
	pre_avail_total_loading_amount = models.BigIntegerField(blank=True, null=True)
	remaining_capacity_amount_100 = models.BigIntegerField(blank=True, null=True)
	remaining_capacity_amount_200 = models.BigIntegerField(blank=True, null=True)
	remaining_capacity_amount_500 = models.BigIntegerField(blank=True, null=True)
	remaining_capacity_amount_2000 = models.BigIntegerField(blank=True, null=True)
	closing_remaining_available_amount_100 = models.BigIntegerField(blank=True, null=True)
	closing_remaining_available_amount_200 = models.BigIntegerField(blank=True, null=True)
	closing_remaining_available_amount_500 = models.BigIntegerField(blank=True, null=True)
	closing_remaining_available_amount_2000 = models.BigIntegerField(blank=True, null=True)
	total_closing_remaining_available_amount = models.BigIntegerField(blank=True, null=True)
	total_forecasted_remaining_amt = models.BigIntegerField(blank=True, null=True)
	final_total_loading_amount = models.BigIntegerField(blank=True, null=True)
	final_total_loading_amount_100 = models.BigIntegerField(blank=True, null=True)
	final_total_loading_amount_200 = models.BigIntegerField(blank=True, null=True)
	final_total_loading_amount_500 = models.BigIntegerField(blank=True, null=True)
	final_total_loading_amount_2000 = models.BigIntegerField(blank=True, null=True)
	total_atm_vaulting_amount_after_pre_availability = models.BigIntegerField(blank=True, null=True)
	atm_vaulting_amount_after_pre_availability_100 = models.BigIntegerField(blank=True, null=True)
	atm_vaulting_amount_after_pre_availability_200 = models.BigIntegerField(blank=True, null=True)
	atm_vaulting_amount_after_pre_availability_500 = models.BigIntegerField(blank=True, null=True)
	atm_vaulting_amount_after_pre_availability_2000 = models.BigIntegerField(blank=True, null=True)
	atm_priority = models.IntegerField(blank=True, null=True)
	is_cash_pre_available = models.IntegerField(blank=True, null=True)
	indent_counter = models.BigIntegerField(blank=True, null=True)
	previous_indent_code = models.CharField(max_length=255, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	feasibility_frequency = models.IntegerField(blank=True, null=True)
	is_feasible_today = models.CharField(max_length=10, blank=True, null=True)
	reason_for_priority = models.CharField(max_length=10, blank=True, null=True)
	bank_decide_limit_days = models.FloatField(blank=True, null=True)
	bank_loading_limit_days = models.FloatField(blank=True, null=True)
	feeder_decide_limit_days = models.FloatField(blank=True, null=True)
	feeder_loading_limit_days = models.FloatField(blank=True, null=True)
	feasibility_decide_limit_days = models.FloatField(blank=True, null=True)
	feasibility_loading_limit_days = models.FloatField(blank=True, null=True)
	indent_type = models.CharField(max_length=50, blank=True, null=True)
	atm_band = models.CharField(max_length=20, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'distribution_planning_detail_V2'

# class distribution_planning_master_V2(models.Model):
# 	id = models.BigAutoField(primary_key=True)
# 	indent_code = models.CharField(max_length=255, blank=True, null=True)
# 	indentdate = models.DateTimeField(blank=True, null=True)
# 	project_id = models.CharField(max_length=50, blank=True, null=True)
# 	bank_code = models.CharField(max_length=50, blank=True, null=True)
# 	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
# 	cra = models.CharField(max_length=50, blank=True, null=True)
# 	max_loading_amount = models.FloatField(blank=True, null=True)
# 	max_loading_amount_100 = models.FloatField(blank=True, null=True)
# 	max_loading_amount_200 = models.FloatField(blank=True, null=True)
# 	max_loading_amount_500 = models.FloatField(blank=True, null=True)
# 	max_loading_amount_2000 = models.FloatField(blank=True, null=True)
# 	min_loading_amount = models.DecimalField(max_digits=38, decimal_places=1, blank=True, null=True)
# 	forecast_loading_amount = models.FloatField(blank=True, null=True)
# 	forecast_loading_amount_100 = models.IntegerField(blank=True, null=True)
# 	forecast_loading_amount_200 = models.IntegerField(blank=True, null=True)
# 	forecast_loading_amount_500 = models.IntegerField(blank=True, null=True)
# 	forecast_loading_amount_2000 = models.IntegerField(blank=True, null=True)
# 	total_cassette_capacity = models.FloatField(blank=True, null=True)
# 	vaultingamount_50 = models.FloatField(blank=True, null=True)
# 	vaultingamount_100 = models.IntegerField(blank=True, null=True)
# 	vaultingamount_200 = models.IntegerField(blank=True, null=True)
# 	vaultingamount_500 = models.IntegerField(blank=True, null=True)
# 	vaultingamount_2000 = models.IntegerField(blank=True, null=True)
# 	total_rounded_vault_amt = models.FloatField(blank=True, null=True)
# 	vault_balance_100 = models.IntegerField(blank=True, null=True)
# 	vault_balance_200 = models.IntegerField(blank=True, null=True)
# 	vault_balance_500 = models.IntegerField(blank=True, null=True)
# 	vault_balance_2000 = models.IntegerField(blank=True, null=True)
# 	total_vault_balance = models.IntegerField(blank=True, null=True)
# 	indent_100 = models.IntegerField(blank=True, null=True)
# 	indent_200 = models.IntegerField(blank=True, null=True)
# 	indent_500 = models.IntegerField(blank=True, null=True)
# 	indent_2000 = models.IntegerField(blank=True, null=True)
# 	is_deno_wise_cash_available = models.IntegerField(blank=True, null=True)
# 	total_amount_available = models.BigIntegerField(db_column='Total_Amount_Available', blank=True, null=True)  # Field name made lowercase.
# 	available_100_amount = models.BigIntegerField(blank=True, null=True)
# 	available_200_amount = models.BigIntegerField(blank=True, null=True)
# 	available_500_amount = models.BigIntegerField(blank=True, null=True)
# 	available_2000_amount = models.BigIntegerField(blank=True, null=True)
# 	total_vaulting_after_pre_availabiliy_allocation = models.BigIntegerField(blank=True, null=True)
# 	vaulting_after_pre_availabiliy_allocation_100 = models.BigIntegerField(blank=True, null=True)
# 	vaulting_after_pre_availabiliy_allocation_200 = models.BigIntegerField(blank=True, null=True)
# 	vaulting_after_pre_availabiliy_allocation_500 = models.BigIntegerField(blank=True, null=True)
# 	vaulting_after_pre_availabiliy_allocation_2000 = models.BigIntegerField(blank=True, null=True)
# 	final_total_loading_amount = models.BigIntegerField(blank=True, null=True)
# 	total_indent_after_pre_availabiliy_allocation = models.BigIntegerField(blank=True, null=True)
# 	indent_after_pre_availabiliy_allocation_100 = models.BigIntegerField(blank=True, null=True)
# 	indent_after_pre_availabiliy_allocation_200 = models.BigIntegerField(blank=True, null=True)
# 	indent_after_pre_availabiliy_allocation_500 = models.BigIntegerField(blank=True, null=True)
# 	indent_after_pre_availabiliy_allocation_2000 = models.BigIntegerField(blank=True, null=True)
# 	cypher_code_sol_id = models.IntegerField(blank=True, null=True)
# 	cypher_code_day = models.IntegerField(blank=True, null=True)
# 	cypher_code_date = models.IntegerField(blank=True, null=True)
# 	cypher_code_month = models.IntegerField(blank=True, null=True)
# 	cypher_code_year = models.IntegerField(blank=True, null=True)
# 	cypher_code_amount = models.IntegerField(blank=True, null=True)
# 	cypher_code = models.IntegerField(blank=True, null=True)
# 	indent_counter = models.BigIntegerField(blank=True, null=True)
# 	previous_indent_code = models.CharField(max_length=255, blank=True, null=True)
# 	record_status = models.CharField(max_length=50, blank=True, null=True)
# 	created_on = models.DateTimeField(blank=True, null=True)
# 	created_by = models.CharField(max_length=50, blank=True, null=True)
# 	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approved_on = models.DateTimeField(blank=True, null=True)
# 	approved_by = models.CharField(max_length=50, blank=True, null=True)
# 	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
# 	rejected_on = models.DateTimeField(blank=True, null=True)
# 	rejected_by = models.CharField(max_length=50, blank=True, null=True)
# 	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	deleted_on = models.DateTimeField(blank=True, null=True)
# 	deleted_by = models.CharField(max_length=50, blank=True, null=True)
# 	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	modified_on = models.DateTimeField(blank=True, null=True)
# 	modified_by = models.CharField(max_length=50, blank=True, null=True)
# 	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	indent_type = models.CharField(max_length=50, blank=True, null=True)
# 	indent_short_code = models.CharField(max_length=15, blank=True, null=True)
#
# 	class Meta:
# 		managed = False
# 		db_table = 'distribution_planning_master_V2'


class distribution_planning_master_V2(models.Model):
    id = models.BigAutoField(primary_key=True)
    indent_code = models.CharField(max_length=255, blank=True, null=True)
    indentdate = models.DateTimeField(blank=True, null=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    bank_code = models.CharField(max_length=50, blank=True, null=True)
    feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
    cra = models.CharField(max_length=50, blank=True, null=True)
    max_loading_amount = models.FloatField(blank=True, null=True)
    max_loading_amount_100 = models.FloatField(blank=True, null=True)
    max_loading_amount_200 = models.FloatField(blank=True, null=True)
    max_loading_amount_500 = models.FloatField(blank=True, null=True)
    max_loading_amount_2000 = models.FloatField(blank=True, null=True)
    min_loading_amount = models.DecimalField(max_digits=38, decimal_places=1, blank=True, null=True)
    forecast_loading_amount = models.FloatField(blank=True, null=True)
    forecast_loading_amount_100 = models.IntegerField(blank=True, null=True)
    forecast_loading_amount_200 = models.IntegerField(blank=True, null=True)
    forecast_loading_amount_500 = models.IntegerField(blank=True, null=True)
    forecast_loading_amount_2000 = models.IntegerField(blank=True, null=True)
    total_cassette_capacity = models.FloatField(blank=True, null=True)
    vaultingamount_50 = models.FloatField(blank=True, null=True)
    vaultingamount_100 = models.IntegerField(blank=True, null=True)
    vaultingamount_200 = models.IntegerField(blank=True, null=True)
    vaultingamount_500 = models.IntegerField(blank=True, null=True)
    vaultingamount_2000 = models.IntegerField(blank=True, null=True)
    total_rounded_vault_amt = models.FloatField(blank=True, null=True)
    vault_balance_100 = models.IntegerField(blank=True, null=True)
    vault_balance_200 = models.IntegerField(blank=True, null=True)
    vault_balance_500 = models.IntegerField(blank=True, null=True)
    vault_balance_2000 = models.IntegerField(blank=True, null=True)
    total_vault_balance = models.IntegerField(blank=True, null=True)
    indent_100 = models.IntegerField(blank=True, null=True)
    indent_200 = models.IntegerField(blank=True, null=True)
    indent_500 = models.IntegerField(blank=True, null=True)
    indent_2000 = models.IntegerField(blank=True, null=True)
    is_deno_wise_cash_available = models.IntegerField(blank=True, null=True)
    total_amount_available = models.BigIntegerField(db_column='Total_Amount_Available', blank=True, null=True)  # Field name made lowercase.
    available_100_amount = models.BigIntegerField(blank=True, null=True)
    available_200_amount = models.BigIntegerField(blank=True, null=True)
    available_500_amount = models.BigIntegerField(blank=True, null=True)
    available_2000_amount = models.BigIntegerField(blank=True, null=True)
    total_vaulting_after_pre_availabiliy_allocation = models.BigIntegerField(blank=True, null=True)
    vaulting_after_pre_availabiliy_allocation_100 = models.BigIntegerField(blank=True, null=True)
    vaulting_after_pre_availabiliy_allocation_200 = models.BigIntegerField(blank=True, null=True)
    vaulting_after_pre_availabiliy_allocation_500 = models.BigIntegerField(blank=True, null=True)
    vaulting_after_pre_availabiliy_allocation_2000 = models.BigIntegerField(blank=True, null=True)
    final_total_loading_amount = models.BigIntegerField(blank=True, null=True)
    total_indent_after_pre_availabiliy_allocation = models.BigIntegerField(blank=True, null=True)
    indent_after_pre_availabiliy_allocation_100 = models.BigIntegerField(blank=True, null=True)
    indent_after_pre_availabiliy_allocation_200 = models.BigIntegerField(blank=True, null=True)
    indent_after_pre_availabiliy_allocation_500 = models.BigIntegerField(blank=True, null=True)
    indent_after_pre_availabiliy_allocation_2000 = models.BigIntegerField(blank=True, null=True)
    cypher_code_sol_id = models.IntegerField(blank=True, null=True)
    cypher_code_day = models.IntegerField(blank=True, null=True)
    cypher_code_date = models.IntegerField(blank=True, null=True)
    cypher_code_month = models.IntegerField(blank=True, null=True)
    cypher_code_year = models.IntegerField(blank=True, null=True)
    cypher_code_amount = models.IntegerField(blank=True, null=True)
    cypher_code = models.IntegerField(blank=True, null=True)
    indent_counter = models.BigIntegerField(blank=True, null=True)
    previous_indent_code = models.CharField(max_length=255, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    indent_type = models.CharField(max_length=50, blank=True, null=True)
    indent_short_code = models.CharField(max_length=15, blank=True, null=True)
    total_closing_vault_balance = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_50 = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_100 = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_200 = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_500 = models.BigIntegerField(blank=True, null=True)
    closing_vault_balance_2000 = models.BigIntegerField(blank=True, null=True)
    is_currency_chest = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'distribution_planning_master_V2'

class indent_mail_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=10, blank=True, null=True)
	feeder_branch = models.CharField(max_length=50, blank=True, null=True)
	cra = models.CharField(max_length=50, blank=True, null=True)
	to_bank_email_id = models.CharField(max_length=320, blank=True, null=True)
	to_cra_email_id = models.CharField(db_column='to_CRA_email_id', max_length=320, blank=True, null=True)  # Field name made lowercase.
	bcc_email_id = models.CharField(max_length=320, blank=True, null=True)
	mail_template_name = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'indent_mail_master'

class indent_mail_status(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch = models.CharField(max_length=50, blank=True, null=True)
	cra = models.CharField(max_length=50, blank=True, null=True)
	indent_order_number = models.CharField(max_length=100, blank=True, null=True)
	file_name = models.CharField(max_length=250, blank=True, null=True)
	email_status = models.CharField(max_length=50, blank=True, null=True)
	indent_date = models.DateTimeField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'indent_mail_status'

class region_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	region_name = models.CharField(max_length=50, blank=True, null=True)
	description = models.CharField(max_length=255, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=500, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	last_modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'region_master'

class CRA_master(models.Model):
	id = models.BigAutoField(primary_key=True)
	cra_id = models.CharField(max_length=50, blank=True, null=True)
	cra_name = models.CharField(max_length=100, blank=True, null=True)
	description = models.CharField(max_length=255, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'CRA_master'
		
# class system_settings(models.Model):
# 	id = models.BigAutoField(primary_key=True)
# 	confidence_factor = models.CharField(max_length=50, blank=True, null=True)
# 	buffer_percentage = models.FloatField(blank=True, null=True)
# 	denomination_wise_round_off_50 = models.IntegerField(blank=True, null=True)
# 	denomination_wise_round_off_100 = models.IntegerField(blank=True, null=True)
# 	denomination_wise_round_off_200 = models.IntegerField(blank=True, null=True)
# 	denomination_wise_round_off_500 = models.IntegerField(blank=True, null=True)
# 	denomination_wise_round_off_2000 = models.IntegerField(blank=True, null=True)
# 	total_rounding_amount = models.IntegerField(blank=True, null=True)
# 	fixed_loading_amount = models.IntegerField(blank=True, null=True)
# 	default_average_dispense = models.IntegerField(blank=True, null=True)
# 	vaulting_for_normal_weekday_percentage = models.FloatField(blank=True, null=True)
# 	vaulting_for_normal_weekend_percentage = models.FloatField(blank=True, null=True)
# 	vaulting_for_extended_weekend_percentage = models.FloatField(blank=True, null=True)
# 	dispenseformula = models.CharField(max_length=50, blank=True, null=True)
# 	deno_100_max_capacity_percentage = models.FloatField(blank=True, null=True)
# 	deno_200_max_capacity_percentage = models.FloatField(blank=True, null=True)
# 	deno_500_max_capacity_percentage = models.FloatField(blank=True, null=True)
# 	deno_2000_max_capacity_percentage = models.FloatField(blank=True, null=True)
# 	deno_100_priority = models.IntegerField(blank=True, null=True)
# 	deno_200_priority = models.IntegerField(blank=True, null=True)
# 	deno_500_priority = models.IntegerField(blank=True, null=True)
# 	deno_2000_priority = models.IntegerField(blank=True, null=True)
# 	deno_100_bill_capacity = models.IntegerField(blank=True, null=True)
# 	deno_200_bill_capacity = models.IntegerField(blank=True, null=True)
# 	deno_500_bill_capacity = models.IntegerField(blank=True, null=True)
# 	deno_2000_bill_capacity = models.IntegerField(blank=True, null=True)
# 	cash_out_logic = models.CharField(max_length=50, blank=True, null=True)
# 	forecasting_algorithm = models.CharField(max_length=50, blank=True, null=True)
# 	record_status = models.CharField(max_length=50, blank=True, null=True)
# 	created_on = models.DateTimeField(blank=True, null=True)
# 	created_by = models.CharField(max_length=50, blank=True, null=True)
# 	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	approved_on = models.DateTimeField(blank=True, null=True)
# 	approved_by = models.CharField(max_length=50, blank=True, null=True)
# 	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	deleted_on = models.DateTimeField(blank=True, null=True)
# 	deleted_by = models.CharField(max_length=50, blank=True, null=True)
# 	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	modified_on = models.DateTimeField(blank=True, null=True)
# 	modified_by = models.CharField(max_length=50, blank=True, null=True)
# 	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
# 	forecasting_coefficient = models.FloatField(blank=True, null=True)
#
# 	class Meta:
# 		managed = False
# 		db_table = 'system_settings'

class system_settings(models.Model):
    id = models.BigAutoField(primary_key=True)
    confidence_factor = models.CharField(max_length=50, blank=True, null=True)
    buffer_percentage = models.FloatField(blank=True, null=True)
    denomination_wise_round_off_50 = models.IntegerField(blank=True, null=True)
    denomination_wise_round_off_100 = models.IntegerField(blank=True, null=True)
    denomination_wise_round_off_200 = models.IntegerField(blank=True, null=True)
    denomination_wise_round_off_500 = models.IntegerField(blank=True, null=True)
    denomination_wise_round_off_2000 = models.IntegerField(blank=True, null=True)
    total_rounding_amount = models.IntegerField(blank=True, null=True)
    fixed_loading_amount = models.IntegerField(blank=True, null=True)
    default_average_dispense = models.IntegerField(blank=True, null=True)
    vaulting_for_normal_weekday_percentage = models.FloatField(blank=True, null=True)
    vaulting_for_normal_weekend_percentage = models.FloatField(blank=True, null=True)
    vaulting_for_extended_weekend_percentage = models.FloatField(blank=True, null=True)
    dispenseformula = models.CharField(max_length=50, blank=True, null=True)
    deno_100_max_capacity_percentage = models.FloatField(blank=True, null=True)
    deno_200_max_capacity_percentage = models.FloatField(blank=True, null=True)
    deno_500_max_capacity_percentage = models.FloatField(blank=True, null=True)
    deno_2000_max_capacity_percentage = models.FloatField(blank=True, null=True)
    deno_100_priority = models.IntegerField(blank=True, null=True)
    deno_200_priority = models.IntegerField(blank=True, null=True)
    deno_500_priority = models.IntegerField(blank=True, null=True)
    deno_2000_priority = models.IntegerField(blank=True, null=True)
    deno_100_bill_capacity = models.IntegerField(blank=True, null=True)
    deno_200_bill_capacity = models.IntegerField(blank=True, null=True)
    deno_500_bill_capacity = models.IntegerField(blank=True, null=True)
    deno_2000_bill_capacity = models.IntegerField(blank=True, null=True)
    cash_out_logic = models.CharField(max_length=50, blank=True, null=True)
    forecasting_algorithm = models.CharField(max_length=50, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    forecasting_coefficient = models.FloatField(blank=True, null=True)
    denomination_wise_round_off_50_currency_chest = models.IntegerField(blank=True, null=True)
    denomination_wise_round_off_100_currency_chest = models.IntegerField(blank=True, null=True)
    denomination_wise_round_off_200_currency_chest = models.IntegerField(blank=True, null=True)
    denomination_wise_round_off_500_currency_chest = models.IntegerField(blank=True, null=True)
    denomination_wise_round_off_2000_currency_chest = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'system_settings'

class indent_pre_qualify_atm(models.Model):
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=10, blank=True, null=True)
	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
	site_code = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	category = models.CharField(max_length=50, blank=True, null=True)
	is_qualified = models.CharField(max_length=20, blank=True, null=True)
	from_date = models.DateTimeField(blank=True, null=True)
	to_date = models.DateTimeField(blank=True, null=True)
	comment = models.CharField(max_length=100, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'indent_pre_qualify_atm'


class indent_eod_pre_activity(models.Model):
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=10, blank=True, null=True)
	region_code = models.CharField(max_length=50, blank=True, null=True)
	branch_code = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	site_code = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'indent_eod_pre_activity'


class limitdays(models.Model):
	id = models.BigAutoField(primary_key=True)
	eps_site_code = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	indent_date = models.DateTimeField(blank=True, null=True)
	next_feasible_date = models.DateTimeField(blank=True, null=True)
	next_working_date = models.DateTimeField(blank=True, null=True)
	default_decide_limit_days = models.FloatField(blank=True, null=True)
	decidelimitdays = models.FloatField(blank=True, null=True)
	default_loading_limit_days = models.FloatField(blank=True, null=True)
	loadinglimitdays = models.FloatField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'limitdays'


class feeder_vaulting_pre_config(models.Model):
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=10, blank=True, null=True)
	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
	is_vaulting_allowed = models.CharField(max_length=10, blank=True, null=True)
	vaulting_percentage = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
	from_date = models.DateTimeField(blank=True, null=True)
	to_date = models.DateTimeField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'feeder_vaulting_pre_config'

class cash_pre_availability(models.Model):
	id = models.BigAutoField(primary_key=True)
	from_date = models.DateField(blank=True, null=True)
	to_date = models.DateField(blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	applied_to_level = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	available_50_amount = models.IntegerField(blank=True, null=True)
	available_100_amount = models.IntegerField(blank=True, null=True)
	available_200_amount = models.IntegerField(blank=True, null=True)
	available_500_amount = models.IntegerField(blank=True, null=True)
	available_2000_amount = models.IntegerField(blank=True, null=True)
	total_amount_available = models.IntegerField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cash_pre_availability'


class cypher_code(models.Model):
	id = models.BigAutoField(primary_key=True)
	category = models.CharField(max_length=20)
	value = models.CharField(max_length=50)
	cypher_code = models.IntegerField()
	is_applicable_to_all_projects = models.CharField(max_length=5, blank=True, null=True)
	project_id = models.CharField(max_length=50)
	is_applicable_to_all_banks = models.CharField(max_length=5, blank=True, null=True)
	bank_code = models.CharField(max_length=50)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'cypher_code'

class feeder_denomination_priority(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
	denomination_100 = models.IntegerField(blank=True, null=True)
	denomination_200 = models.IntegerField(blank=True, null=True)
	denomination_500 = models.IntegerField(blank=True, null=True)
	denomination_2000 = models.IntegerField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=50, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'feeder_denomination_priority'
		
class auth_user_group_metadata(models.Model):
	id = models.IntegerField(primary_key=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'auth_user_group_metadata'

class feeder_cassette_max_capacity_percentage(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
	denomination_100 = models.BigIntegerField(blank=True, null=True)
	denomination_200 = models.BigIntegerField(blank=True, null=True)
	denomination_500 = models.BigIntegerField(blank=True, null=True)
	denomination_2000 = models.BigIntegerField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	reject_trigger_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=50, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'feeder_cassette_max_capacity_percentage'

class indent_pdf_status(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch = models.CharField(max_length=50, blank=True, null=True)
	cra = models.CharField(max_length=50, blank=True, null=True)
	indent_order_number = models.CharField(max_length=100, blank=True, null=True)
	file_name = models.CharField(max_length=250, blank=True, null=True)
	physical_file = models.BinaryField(blank=True, null=True)
	pdf_status = models.CharField(max_length=50, blank=True, null=True)
	indent_date = models.DateTimeField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	indent_type = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'indent_pdf_status'

		
class menu_group_permission_mapping(models.Model):
	id = models.BigAutoField(primary_key=True)
	group_id = models.IntegerField(blank=True, null=True)
	menu_id = models.CharField(max_length=50, blank=True, null=True)
	sub_menu_id = models.CharField(max_length=50, blank=True, null=True)
	permission_id = models.IntegerField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'menu_group_permission_mapping'


class user_bank_project_mapping(models.Model):
	id = models.BigAutoField(primary_key=True)
	user_id = models.IntegerField(blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'user_bank_project_mapping'

class project_bank_region_feeder_mapping(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50)
	bank_code = models.CharField(max_length=50)
	feeder_branch = models.CharField(max_length=50, blank=True, null=True)
	region = models.CharField(max_length=50)
	iscbr = models.CharField(max_length=50, blank=True, null=True)
	isdispense = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'project_bank_region_feeder_mapping'


class cash_balance_file_lvb ( models.Model ) :
	id = models.BigAutoField ( primary_key=True )
	brn = models.CharField ( max_length=50 , blank=True , null=True )
	atm_id = models.CharField ( max_length=15 , blank=True , null=True )
	atm_location = models.CharField ( max_length=100 , blank=True , null=True )
	atm_type = models.CharField ( max_length=50 , blank=True , null=True )
	availability_100 = models.IntegerField ( blank=True , null=True )
	denomination_100 = models.IntegerField ( blank=True , null=True )
	availability_500 = models.IntegerField ( blank=True , null=True )
	denomination_500 = models.IntegerField ( blank=True , null=True )
	availability_2000 = models.IntegerField ( blank=True , null=True )
	denomination_2000 = models.IntegerField ( blank=True , null=True )
	total_amt = models.IntegerField ( blank=True , null=True )
	file_date_time = models.DateTimeField ( blank=True , null=True )
	project_id = models.CharField ( max_length=50 , blank=True , null=True )
	record_status = models.CharField ( max_length=50 , blank=True , null=True )
	created_on = models.DateTimeField ( blank=True , null=True )
	created_by = models.CharField ( max_length=50 , blank=True , null=True )
	created_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	datafor_date_time = models.DateTimeField ( blank=True , null=True )
	region = models.CharField ( max_length=50 , blank=True , null=True )
	approved_on = models.DateTimeField ( blank=True , null=True )
	approved_by = models.CharField ( max_length=50 , blank=True , null=True )
	approved_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	rejected_on = models.DateTimeField ( blank=True , null=True )
	rejected_by = models.CharField ( max_length=50 , blank=True , null=True )
	reject_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	reject_trigger_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	deleted_on = models.DateTimeField ( blank=True , null=True )
	deleted_by = models.CharField ( max_length=50 , blank=True , null=True )
	deleted_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	modified_on = models.DateTimeField ( blank=True , null=True )
	modified_by = models.CharField ( max_length=50 , blank=True , null=True )
	modified_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	is_valid_record = models.CharField ( max_length=10 , blank=True , null=True )
	error_code = models.CharField ( max_length=50 , blank=True , null=True )

	class Meta :
		managed = False
		db_table = 'cash_balance_file_lvb'

class scheduler_config(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	region = models.CharField(max_length=100, blank=True, null=True)
	file_type = models.CharField(max_length=100, blank=True, null=True)
	schedule_frequency = models.IntegerField(blank=True, null=True)
	counter_frequency = models.IntegerField(blank=True, null=True)
	wait_time = models.CharField(max_length=10, blank=True, null=True)
	hourly_frequency = models.TextField(blank=True, null=True)  # This field type is a guess.
	number_1st_escalation = models.TextField(db_column='1st_escalation', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier. This field type is a guess.
	number_2nd_escalation = models.TextField(db_column='2nd_escalation', blank=True, null=True)  # Field renamed because it wasn't a valid Python identifier. This field type is a guess.
	from_email = models.CharField(max_length=100, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'scheduler_config'

class sla(models.Model):
	id = models.BigAutoField(primary_key=True)
	cra = models.CharField(max_length=100, blank=True, null=True)
	file_name = models.TextField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'sla'


class holiday_states(models.Model):
	holiday_code = models.CharField(max_length=50, blank=True, null=True)
	state = models.CharField(max_length=100, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'holiday_states'


class holiday_list(models.Model):
	holiday_code = models.CharField(primary_key=True, max_length=50)
	holiday_name = models.CharField(max_length=100, blank=True, null=True)
	description = models.CharField(max_length=100, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'holiday_list'


class holiday_date(models.Model):
	holiday_code = models.CharField(max_length=50)
	start_date = models.DateField(blank=True, null=True)
	end_date = models.DateField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'holiday_date'
		
		
class ams_api_token_data(models.Model):
	id = models.BigAutoField(primary_key=True)
	datafor_date_time = models.DateTimeField()
	max_response_id = models.IntegerField()
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	deleted_on = models.DateTimeField(blank=True, null=True)
	deleted_by = models.CharField(max_length=50, blank=True, null=True)
	deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
	class Meta:
		managed = False
		db_table = 'ams_api_token_data'

class bank_limit_days ( models.Model ) :
	project_id = models.CharField ( max_length=50 , blank=True , null=True )
	bank_code = models.CharField ( max_length=50 , blank=True , null=True )
	decide_limit_days = models.FloatField ( blank=True , null=True )
	loading_limit_days = models.FloatField ( blank=True , null=True )
	fordate = models.DateTimeField ( blank=True , null=True )
	record_status = models.CharField ( max_length=50 , blank=True , null=True )
	created_on = models.DateTimeField ( blank=True , null=True )
	created_by = models.CharField ( max_length=50 , blank=True , null=True )
	created_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	approved_on = models.DateTimeField ( blank=True , null=True )
	approved_by = models.CharField ( max_length=50 , blank=True , null=True )
	approved_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	approve_reject_comment = models.CharField ( max_length=50 , blank=True , null=True )
	rejected_on = models.DateTimeField ( blank=True , null=True )
	rejected_by = models.CharField ( max_length=50 , blank=True , null=True )
	reject_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	modified_on = models.DateTimeField ( blank=True , null=True )
	modified_by = models.CharField ( max_length=50 , blank=True , null=True )
	modified_reference_id = models.CharField ( max_length=50 , blank=True , null=True )
	is_valid_record = models.CharField ( max_length=20 , blank=True , null=True )
	error_code = models.CharField ( max_length=50 , blank=True , null=True )

	class Meta :
		managed = False
		db_table = 'bank_limit_days'

class feeder_limit_days(models.Model):
	project_id = models.CharField(max_length=50, blank=True, null=True)
	bank_code = models.CharField(max_length=50, blank=True, null=True)
	decide_limit_days = models.FloatField(blank=True, null=True)
	loading_limit_days = models.FloatField(blank=True, null=True)
	fordate = models.DateTimeField(blank=True, null=True)
	feeder = models.CharField(max_length=50, blank=True, null=True)
	cra = models.CharField(max_length=50, blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'feeder_limit_days'

class project_bank_feeder_cra_mapping(models.Model):
	id = models.BigAutoField(primary_key=True)
	bank_id = models.CharField(max_length=50)
	project_id = models.CharField(max_length=50)
	region = models.CharField(max_length=50, blank=True, null=True)
	feeder = models.CharField(db_column='Feeder', max_length=50, blank=True, null=True)  # Field name made lowercase.
	iscbr = models.CharField(max_length=50, blank=True, null=True)
	isdispense = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'project_bank_feeder_cra_mapping'

class zero_cash_dispense(models.Model):
	id = models.BigAutoField(primary_key=True)
	project_id = models.CharField(max_length=50, blank=True, null=True)
	atm_id = models.CharField(max_length=50, blank=True, null=True)
	bank_name = models.CharField(max_length=50, blank=True, null=True)
	site_code = models.CharField(max_length=50, blank=True, null=True)
	datafor_date_time = models.DateTimeField(blank=True, null=True)
	feeder_branch = models.CharField(max_length=50, blank=True, null=True)
	cra = models.CharField(max_length=100, blank=True, null=True)
	total_dispense_amount = models.BigIntegerField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'zero_cash_dispense'

class diversion_status(models.Model):
	diversion_request_no = models.BigAutoField(primary_key=True)
	indent_order_number = models.CharField(max_length=255, blank=True, null=True)
	bank = models.CharField(max_length=50, blank=True, null=True)
	feeder_branch = models.CharField(max_length=100, blank=True, null=True)
	region = models.CharField(max_length=100, blank=True, null=True)
	cra = models.CharField(max_length=100, blank=True, null=True)
	order_date = models.DateTimeField(blank=True, null=True)
	original_atm_id = models.CharField(max_length=50, blank=True, null=True)
	diverted_atm_id = models.CharField(max_length=50, blank=True, null=True)
	total_diversion_amount = models.IntegerField(blank=True, null=True)
	denomination_100 = models.IntegerField(blank=True, null=True)
	denomination_200 = models.IntegerField(blank=True, null=True)
	denomination_500 = models.IntegerField(blank=True, null=True)
	denomination_2000 = models.IntegerField(blank=True, null=True)
	reasons = models.TextField(blank=True, null=True)
	record_status = models.CharField(max_length=50, blank=True, null=True)
	created_on = models.DateTimeField(blank=True, null=True)
	created_by = models.CharField(max_length=50, blank=True, null=True)
	created_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approved_on = models.DateTimeField(blank=True, null=True)
	approved_by = models.CharField(max_length=50, blank=True, null=True)
	approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
	approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
	rejected_on = models.DateTimeField(blank=True, null=True)
	rejected_by = models.CharField(max_length=50, blank=True, null=True)
	reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
	modified_on = models.DateTimeField(blank=True, null=True)
	modified_by = models.CharField(max_length=50, blank=True, null=True)
	modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
	is_valid_record = models.CharField(max_length=20, blank=True, null=True)
	error_code = models.CharField(max_length=50, blank=True, null=True)

	class Meta:
		managed = False
		db_table = 'diversion_status'




class api_call_log(models.Model):
    id = models.BigAutoField(primary_key=True)
    request_api_url = models.CharField(max_length=100, blank=True, null=True)
    request_method = models.CharField(max_length=50, blank=True, null=True)
    request_meta_info = models.TextField(blank=True, null=True)
    request_user_info = models.CharField(max_length=100, blank=True, null=True)
    request_get_info = models.TextField(blank=True, null=True)
    request_post_info = models.TextField(blank=True, null=True)
    reference_id = models.CharField(max_length=50, blank=True, null=True)
    request_date = models.DateTimeField(blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'api_call_log'

class loading_status_confirmation(models.Model):
    indent_order_number = models.CharField(max_length=200, blank=True, null=True)
    site_code = models.CharField(max_length=50, blank=True, null=True)
    atm_id = models.CharField(max_length=50, blank=True, null=True)
    bank_code = models.CharField(max_length=50, blank=True, null=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    feeder_branch_code = models.CharField(max_length=100, blank=True, null=True)
    cra = models.CharField(max_length=50, blank=True, null=True)
    indent_date = models.DateField(blank=True, null=True)
    loading_status = models.CharField(max_length=50, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    is_valid_record = models.CharField(max_length=20, blank=True, null=True)
    error_code = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'loading_status_confirmation'


class withdrawal_status(models.Model):
    indent_short_code = models.CharField(primary_key=True, max_length=15)
    indent_order_number = models.CharField(max_length=255, blank=True, null=True)
    bank_code = models.CharField(max_length=50, blank=True, null=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    feeder_branch = models.CharField(max_length=50, blank=True, null=True)
    cra = models.TextField(blank=True, null=True)
    total_withdrawal_amount = models.IntegerField(blank=True, null=True)
    denomination_100 = models.IntegerField(blank=True, null=True)
    denomination_200 = models.IntegerField(blank=True, null=True)
    denomination_500 = models.IntegerField(blank=True, null=True)
    denomination_2000 = models.IntegerField(blank=True, null=True)
    remarks = models.TextField(blank=True, null=True)
    withdrawal_slip = models.BinaryField(blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'withdrawal_status'

class Reportconfigforui(models.Model):
    bankcode = models.CharField(db_column='BankCode', max_length=50)  # Field name made lowercase.
    reportname = models.CharField(db_column='ReportName', max_length=100)  # Field name made lowercase.
    reporttype = models.CharField(db_column='ReportType', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'ReportConfigForUI'

class historical_data_log(models.Model):
    id = models.BigAutoField(primary_key=True)
    data_for_type = models.CharField(max_length=50, blank=True, null=True)
    data_for_datetime = models.DateTimeField(blank=True, null=True)
    file_name = models.CharField(max_length=100, blank=True, null=True)
    status = models.CharField(max_length=500, blank=True, null=True)
    level = models.CharField(max_length=200, blank=True, null=True)
    file_status = models.CharField(max_length=200, blank=True, null=True)
    bank_code = models.CharField(max_length=100, blank=True, null=True)
    project_id = models.CharField(max_length=100, blank=True, null=True)
    region = models.CharField(max_length=50, blank=True, null=True)
    is_valid_file = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    validation_code = models.CharField(max_length=50, blank=True, null=True)
    cra_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'historical_data_log'

class vaulting_of_balance(models.Model):
    cra = models.CharField(max_length=50, blank=True, null=True)
    indent_order_id = models.CharField(max_length=255, blank=True, null=True)
    bank_code = models.CharField(max_length=50, blank=True, null=True)
    feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
    atm_id = models.CharField(max_length=50, blank=True, null=True)
    vaulting_date = models.DateTimeField(blank=True, null=True)
    amount_100 = models.IntegerField(blank=True, null=True)
    amount_200 = models.IntegerField(blank=True, null=True)
    amount_500 = models.IntegerField(blank=True, null=True)
    amount_2000 = models.IntegerField(blank=True, null=True)
    total_amount = models.IntegerField(blank=True, null=True)
    reason_for_vaulting = models.CharField(max_length=100, blank=True, null=True)
    comments = models.CharField(max_length=50, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    is_valid_record = models.CharField(max_length=20, blank=True, null=True)
    error_code = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'vaulting_of_balance'


class indent_holiday(models.Model):
    holiday_date = models.DateTimeField(blank=True, null=True)
    is_holiday = models.CharField(max_length=10, blank=True, null=True)
    holiday_code = models.CharField(max_length=50, blank=True, null=True)
    holiday_name = models.CharField(max_length=100, blank=True, null=True)
    holiday_type = models.CharField(max_length=50, blank=True, null=True)
    applied_to_level = models.CharField(max_length=50, blank=True, null=True)
    state_code = models.CharField(max_length=50, blank=True, null=True)
    district_code = models.CharField(max_length=50, blank=True, null=True)
    city_code = models.CharField(max_length=50, blank=True, null=True)
    area_code = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    withdrawal_type = models.CharField(max_length=100, blank=True, null=True)
    feeder_branch = models.CharField(db_column='Feeder_branch', max_length=100, blank=True, null=True)  # Field name made lowercase.
    is_indent_required = models.CharField(max_length=100, blank=True, null=True)
    cra = models.CharField(max_length=100, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    project_id = models.CharField(db_column='Project_id', max_length=50, blank=True, null=True)  # Field name made lowercase.
    bank_code = models.CharField(db_column='Bank_Code', max_length=30, blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'indent_holiday'


class atm_list_for_revision(models.Model):
    id = models.BigAutoField(primary_key=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    bank_code = models.CharField(max_length=50, blank=True, null=True)
    feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
    atm_id = models.CharField(max_length=50, blank=True, null=True)
    site_code = models.CharField(max_length=50, blank=True, null=True)
    for_date = models.DateTimeField(blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    is_valid_record = models.CharField(max_length=20, blank=True, null=True)
    error_code = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'atm_list_for_revision'

class atm_revision_cash_availability(models.Model):
    id = models.BigAutoField(primary_key=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    bank_code = models.CharField(max_length=50, blank=True, null=True)
    feeder_branch_code = models.CharField(max_length=50, blank=True, null=True)
    atm_id = models.CharField(max_length=50, blank=True, null=True)
    site_code = models.CharField(max_length=50, blank=True, null=True)
    for_date = models.DateTimeField(blank=True, null=True)
    denomination_100 = models.IntegerField(blank=True, null=True)
    denomination_200 = models.IntegerField(blank=True, null=True)
    denomination_500 = models.IntegerField(blank=True, null=True)
    denomination_2000 = models.IntegerField(blank=True, null=True)
    total_amount = models.IntegerField(blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    is_valid_record = models.CharField(max_length=20, blank=True, null=True)
    error_code = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'atm_revision_cash_availability'

class gl_account_number(models.Model):
    id = models.BigAutoField(primary_key=True)
    atm_id = models.CharField(max_length=50, blank=True, null=True)
    site_code = models.CharField(max_length=50, blank=True, null=True)
    bank_code = models.CharField(max_length=50, blank=True, null=True)
    gl_acc_number = models.CharField(max_length=50, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    created_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approve_reject_comment = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    is_valid_record = models.CharField(max_length=20, blank=True, null=True)
    error_code = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'gl_account_number'
