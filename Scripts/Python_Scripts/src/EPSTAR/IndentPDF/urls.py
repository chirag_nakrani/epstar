from django.conf.urls import url
from . import create_pdf

app_name = "CreatePDF"

urlpatterns = [
    url(r'^create_pdf/$', create_pdf.CreatePDF.as_view(), name='CreatePDF')
]
