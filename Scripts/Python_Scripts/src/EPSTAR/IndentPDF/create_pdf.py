from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from .tasks import create_pdf_task
from celery.result import AsyncResult
from django.http import HttpResponse
from rest_framework.response import Response
import json
from rest_framework import status
from django.views.decorators.csrf import csrf_exempt
import logging
from django.db import connections

class CreatePDF(APIView):

    ## This is the Main API endpoint class for Creating PDF

    authentication_classes = (JSONWebTokenAuthentication, )
    parser_class = (MultiPartParser, FormParser, )
    renderer_classes = (JSONRenderer, )
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        logger = logging.getLogger(__name__)
        logger.info('Start of CreatePDF API of celery task.')
        logger.info("request parameters received in CreatePDF api ::: %s",request.data)
        access_token = request.META['HTTP_AUTHORIZATION'].split(' ')[1]
        username = request.userinfo['username'].username
        email = request.userinfo['username'].email
        reference_id = request.userinfo['reference_id']
        request_obj = {}
        request_obj['data'] =  request.data
        request_obj['access_token'] =  access_token
        request_obj['username'] = username
        request_obj['reference_id'] = reference_id
        logger.info(" request_obj ::: ",request_obj)
        for conn in connections.all():
            conn.close_if_unusable_or_obsolete()
        result = create_pdf_task.delay(request_obj,email)
        logger.info('Task id generated: %s',result.id)
        status_dict = {
            "status_text":"Upload is in progress please check your mailbox",
            "task_id" : result.id
        }
        logger.info('Response from CreatePDF Api: %s',status_dict)
        return Response(status=status.HTTP_200_OK,data=status_dict)

class GetResponse(APIView):
    
    authentication_classes = (JSONWebTokenAuthentication, )
    parser_class = (MultiPartParser, FormParser, )
    renderer_classes = (JSONRenderer, )
    permission_classes = (IsAuthenticated, )
    
    @csrf_exempt
    def post(self,request):
        logger = logging.getLogger(__name__)
        logger.info('Start of GetResponse API of celery task.')
        task_id = request.data['task_id']
        logger.info('Task id received: %s',task_id)
        if task_id is not None:
            task = AsyncResult(task_id)
            data = {
                'state': task.state,
                'result': task.result,
            }
            logger.info('Response Data from Asyncresult: %s',data)
            return HttpResponse(json.dumps(data), content_type='application/json')
        else:
            return HttpResponse('No job id given.')
