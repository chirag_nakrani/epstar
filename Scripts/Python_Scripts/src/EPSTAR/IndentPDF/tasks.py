from Controller.IndentPDF import PDFController
from EPSTAR.celery import app
import logging
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from Common import masterconf
import datetime
import time

@app.task(name='PDF Creation')
def create_pdf_task(request_obj,email):
	logger = logging.getLogger(__name__)
	logger.info(" In create_pdf_task")
	logger.info('Task name is "PDF Creation".')
	logger.info('Request parameters from create_pdf_task: %s',request_obj)
	email_start = send_mail_start(email)
	indent_pdf_controller = PDFController.PDFController()
	process_list = indent_pdf_controller.handle_pdf_create_req(request_obj)
	logger.info('Length of response list from PDFController: %s',len(process_list))
	# chunk_list = gen_chunks(process_list,2)
	indent_pdf_controller.create_pdf(process_list,request_obj['username'],request_obj['reference_id'])
	response = {'current': len(process_list), 'total': len(process_list), 'percent': 100}
	email_end = send_mail_end(email,len(process_list))
	logger.info('Response from create_pdf_task: %s',{'current': len(process_list), 'total': len(process_list), 'percent': 100})
	return response

### send mail function for pdf ####
def send_mail_start(email):
	logger = logging.getLogger(__name__)
	logger.info("INSIDE send_mail_start METHOD OF CreatePDF.")
	start_datetime = datetime.datetime.fromtimestamp(time.time()).strftime(
		'%Y-%m-%d %H:%M:%S')
	response_obj = {}
	try:
		message = MIMEMultipart()
		message['From'] = 'epstar@electronicpay.in'
		message['To'] = email
		message['Subject'] = 'Indent Pdf generation started at ' + start_datetime
		body_text = masterconf.email_body_start
		message.attach(MIMEText(body_text))
		try:
			#### smtp configuration for mail ####
			server = smtplib.SMTP()
			server.connect(masterconf.MAILHOST, masterconf.MAILPORT)
			server.starttls()
			server.login(masterconf.mail_sender, masterconf.mail_password)
			recipients = []
			recipients.append(email)
			server.sendmail(masterconf.mail_sender, recipients, message.as_string())
			response_obj['status_code'] = masterconf.success_mail_pdf
			response_obj['status_text'] = masterconf.success_mail_pdf_text
			logger.info("MAIL SENT.")
			server.close()
		except Exception as e1:
			response_obj['status_code'] = masterconf.failure_mail_pdf
			response_obj['status_text'] = masterconf.failure_maildata_pdf_text
			logger.error(e1)
	except Exception as e:
		response_obj['status_code'] = masterconf.failure_mail_pdf
		response_obj['status_text'] = masterconf.failure_maildata_pdf_text
		logger.error(e)
	logger.info('Returned response from send_mail_start function of CreatePDF: %s', response_obj)
	return response_obj

### send mail function for pdf ####
def send_mail_end(email,total):
	logger = logging.getLogger(__name__)
	logger.info("INSIDE send_mail_end METHOD OF CreatePDF.")
	end_datetime = datetime.datetime.fromtimestamp(time.time()).strftime(
		'%Y-%m-%d %H:%M:%S')
	response_obj = {}
	try:
		message = MIMEMultipart()
		message['From'] = 'epstar@electronicpay.in'
		message['To'] = email
		message['Subject'] = 'Indent Pdf generation Completed at ' + end_datetime
		body_text = masterconf.email_body_end.format(a=total)
		message.attach(MIMEText(body_text))
		try:
			#### smtp configuration for mail ####
			server = smtplib.SMTP()
			server.connect(masterconf.MAILHOST, masterconf.MAILPORT)
			server.starttls()
			server.login(masterconf.mail_sender, masterconf.mail_password)
			recipients = []
			recipients.append(email)
			server.sendmail(masterconf.mail_sender, recipients, message.as_string())
			response_obj['status_code'] = masterconf.success_mail_pdf
			response_obj['status_text'] = masterconf.success_mail_pdf_text
			logger.info("MAIL SENT.")
			server.close()
		except Exception as e1:
			response_obj['status_code'] = masterconf.failure_mail_pdf
			response_obj['status_text'] = masterconf.failure_maildata_pdf_text
			logger.error(e1)
	except Exception as e:
		response_obj['status_code'] = masterconf.failure_mail_pdf
		response_obj['status_text'] = masterconf.failure_maildata_pdf_text
		logger.error(e)
	logger.info('Returned response from send_mail_end function of CreatePDF: %s', response_obj)
	return response_obj

def gen_chunks(process_list,procs):

	if len(process_list) > 2:
		chun_spl = len(process_list)/procs
		chun_spl = int(chun_spl)

		chunks_split = [process_list[x:x+int(chun_spl)] for x in range(0, len(process_list), int(chun_spl))]
	else:
		chun_spl = 1
		chunks_split = [process_list[x:x+int(chun_spl)] for x in range(0, len(process_list), int(chun_spl))]

	return chunks_split