import datetime
from datetime import timedelta
import pyodbc
import re
import pandas as pd
import time
cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.0.169;DATABASE=epstar;UID=sa;PWD=Mysql@123')
cursor = cnxn.cursor()


users_list_dict ={
'cashops1': ['Cash_Operator', datetime.datetime(2018, 9, 2, 12, 39, 37, tzinfo=datetime.timezone.utc), datetime.datetime(2018, 10, 12, 15, 17, 25, tzinfo=datetime.timezone.utc), 'cashops1'],
'cashops2': ['Cash_Operator', datetime.datetime(2018, 9, 3, 12, 31, 49, tzinfo=datetime.timezone.utc), datetime.datetime(2018, 10, 12, 15, 18, 5, tzinfo=datetime.timezone.utc), 'cashops2'],
'cashmgr1': ['Cash_Manager', datetime.datetime(2018, 9, 4, 15, 19, 19, tzinfo=datetime.timezone.utc), datetime.datetime(2018, 10, 12, 15, 19, 19, tzinfo=datetime.timezone.utc), 'cashmgr1'],
'cahmgr2': ['Cash_Manager', datetime.datetime(2018, 9, 5, 15, 20, 3, tzinfo=datetime.timezone.utc), datetime.datetime(2018, 10, 12, 15, 20, 3, tzinfo=datetime.timezone.utc), 'cahmgr2'],
'cashadmin': ['Cash_Admin', datetime.datetime(2018, 9, 6, 13, 12, 50, tzinfo=datetime.timezone.utc), datetime.datetime(2018, 10, 12, 15, 21, 45, tzinfo=datetime.timezone.utc), 'cashadmin'],
'cashadmin1': ['Cash_Admin', datetime.datetime(2018, 9, 12, 13, 12, 50, tzinfo=datetime.timezone.utc), datetime.datetime(2018, 10, 12, 15, 21, 45, tzinfo=datetime.timezone.utc), 'cashadmin1'],
#'cashadmin2': ['Cash_Admin3', datetime.datetime(2018, 9, 14, 13, 12, 50, tzinfo=datetime.timezone.utc), datetime.datetime(2018, 10, 12, 15, 21, 45, tzinfo=datetime.timezone.utc), 'cashadmin2'],
}


user_group_dict =  {'cashops1': ['Cash_Operator'], 'cashops2': ['Cash_Operator'], 'cashmgr1': ['Cash_Manager'], 'cahmgr2': ['Cash_Manager'], 'cashadmin': ['Cash_Admin']}
try:
	sql_query = 'select * from auth_user_dummy'
	cursor.execute(sql_query)
	c = cursor.fetchall()
#when first time data will get uploaded
	if c == []:
		user_group_list = []
		#Pushing data into auth_group_dummy for first time
		for i in user_group_dict:
			if user_group_dict[i][0] not in user_group_list:
				user_group_list.append(user_group_dict[i][0])
		for i in user_group_list:
			sql_query = "insert into auth_group_dummy(name) values(?)"
			values = (i)
			cursor.execute(sql_query,values)
			cnxn.commit()
		#Pushing data into auth_user_dummy for first time
		for i in users_list_dict:
			username = users_list_dict[i][3]
			group_name = users_list_dict[i][0]
			first_name = ' '
			last_name = ' '
			whenChanged = users_list_dict[i][1]
			whenCreated = users_list_dict[i][2]
			password = '1'
			if username == 'cashadmin':
				is_superuser = 1
			else:
				is_superuser = 0
			is_staff = 1
			is_active = 1
			email = ' '
			date_joined = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
			sql_insert_query = "Insert into auth_user_dummy (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined) VALUES (?,?,?,?,?,?,?,?,?)"
			values = (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined)
			cursor.execute(sql_insert_query,values)
			cnxn.commit()
			sql_searchUserID_query = "select id from auth_user_dummy where username = '"+username+"'"
			cursor.execute(sql_searchUserID_query)
			userID = cursor.fetchall()
			userID = userID[0][0]
			##print (userID)
			record_status = 'present'
			sql_insert_query_UserAD = "Insert into auth_userAD (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined,whenChanged,whenCreated,userID,record_status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"
			values = (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined,whenChanged,whenCreated,userID,record_status)
			cursor.execute(sql_insert_query_UserAD,values)
			cnxn.commit()
			sql_query = "select id from auth_group_dummy where name = '"+users_list_dict[i][0]+"'"
			cursor.execute(sql_query)
			groupID = cursor.fetchall()
			groupID = groupID[0][0]
			sql_queryinto_authuser_group = "Insert into auth_user_groups_dummy(user_id,group_id) values(?,?)"
			values = (userID,groupID)
			cursor.execute(sql_queryinto_authuser_group,values)
			cnxn.commit()
	else:
		#when any record in the AD has been updated
		whenChanged_time_list = []
		for i in users_list_dict:
			whenChanged_time_list.append(users_list_dict[i][1])
		#print ("whenChanged_time_list : ",type(whenChanged_time_list[0]))
		sql_query_maxwhenChanged_time = "select max(whenChanged) from auth_userAD"
		cursor.execute(sql_query_maxwhenChanged_time)
		max_whenChanged = cursor.fetchall()
		max_whenChanged = max_whenChanged[0][0]#2018-10-26 12:39:37
		#print ("max_whenChanged : ",max_whenChanged)
		count = 0
		sql_query_to_find_groups = "select name from auth_group_dummy"
		cursor.execute(sql_query_to_find_groups)
		groups = cursor.fetchall()
		user_group_list = []
		for i in range(len(groups)):
			user_group_list.append(groups[i][0])
		#print ("Active user group list : ",user_group_list)
		sql_query_to_find_users = "select distinct(username) from auth_user_dummy where is_active = 1"
		cursor.execute(sql_query_to_find_users)
		users = cursor.fetchall()
		users_list_fromDB = []
		for i in range(len(users)):
			users_list_fromDB.append(users[i][0])
		#print ("users_list from DB : ",users_list_fromDB)
		users_list_AD = []
		for i in users_list_dict.keys():
			users_list_AD.append(i)
#############Adding new user from AD to DB######################################
			##print ("users_list_dict[i][3] :",users_list_dict[i][3])
			if users_list_dict[i][3] not in users_list_fromDB:
				#print ("users_list_dict[i][3] :",users_list_dict[i][3])
				username = users_list_dict[i][3]
				group_name = users_list_dict[i][0]
				first_name = ' '
				last_name = ' '
				whenChanged = users_list_dict[i][1]
				whenCreated = users_list_dict[i][2]
				password = '1'
				if group_name == 'Cash_Admin':
					is_superuser = 1
				else:
					is_superuser = 0
				is_staff = 1
				is_active = 1
				email = ' '
				date_joined = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
				sql_insert_query = "Insert into auth_user_dummy (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined) VALUES (?,?,?,?,?,?,?,?,?)"
				values = (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined)
				cursor.execute(sql_insert_query,values)
				cnxn.commit()
				sql_searchUserID_query = "select id from auth_user_dummy where username = '"+username+"'"
				cursor.execute(sql_searchUserID_query)
				userID = cursor.fetchall()
				userID = userID[0][0]
				record_status = 'present'
				sql_insert_query_UserAD = "Insert into auth_userAD (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined,whenChanged,whenCreated,userID,record_status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"
				values = (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined,whenChanged,whenCreated,userID,record_status)
				cursor.execute(sql_insert_query_UserAD,values)
				cnxn.commit()
				sql_query_to_group = "IF NOT EXISTS (SELECT * from auth_group_dummy where name = '"+group_name+"') insert into auth_group_dummy (name) values (?)"
				values = (group_name)
				cursor.execute(sql_query_to_group,values)
				cnxn.commit()
				sql_query = "select id from auth_group_dummy where name = '"+users_list_dict[i][0]+"'"
				cursor.execute(sql_query)
				groupID = cursor.fetchall()
				groupID = groupID[0][0]
				sql_queryinto_authuser_group = "Insert into auth_user_groups_dummy(user_id,group_id) values(?,?)"
				values = (userID,groupID)
				cursor.execute(sql_queryinto_authuser_group,values)
				cnxn.commit()
##################When record has been updated in the AD#####################################################
			elif time.mktime(max_whenChanged.timetuple()) * 1000 < time.mktime(users_list_dict[i][1].timetuple()) * 1000:
				count = count + 1
				#print ("{} record to be updated".format(count))
				username = users_list_dict[i][3]
				group_name = users_list_dict[i][0]
				first_name = ' '
				last_name = ' '
				whenChanged = users_list_dict[i][1]
				whenCreated = users_list_dict[i][2]
				password = '1'
				if group_name == 'Cash_Admin':
					is_superuser = 1
				else:
					is_superuser = 0
				is_staff = 1
				is_active = 1
				email = ' '
				date_joined = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
				sql_update_record = "update auth_user_dummy set is_active = 0 where username = '"+username+"'"
				cursor.execute(sql_update_record)
				cnxn.commit()
				sql_query_updateuserAD = "update auth_userAD set is_active = 0, record_status = 'history' where username = '"+username+"'"
				cursor.execute(sql_query_updateuserAD)
				cnxn.commit()
				sql_insert_query = "Insert into auth_user_dummy (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined) VALUES (?,?,?,?,?,?,?,?,?)"
				values = (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined)
				cursor.execute(sql_insert_query,values)
				cnxn.commit()
				sql_searchUserID_query = "select id from auth_user_dummy where username = '"+username+"'"
				cursor.execute(sql_searchUserID_query)
				userID = cursor.fetchall()
				userID = userID[0][0]
				record_status = 'present'
				sql_insert_query_UserAD = "Insert into auth_userAD (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined,whenChanged,whenCreated,userID,record_status) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)"
				values = (password,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined,whenChanged,whenCreated,userID,record_status)
				cursor.execute(sql_insert_query_UserAD,values)
				cnxn.commit()
				groupID = ''
				if group_name not in user_group_list:
					sql_query_to_insert_newgroup = "Insert into auth_group_dummy(name) values (?)"
					values = (group_name)
					cursor.execute(sql_query_to_insert_newgroup,values)
					cnxn.commit()
					sql_query = "select id from auth_group_dummy where name = '"+users_list_dict[i][0]+"'"
					cursor.execute(sql_query)
					groupID = cursor.fetchall()
					groupID = groupID[0][0]
					sql_searchUserID_query = "select id from auth_user_dummy where username = '"+username+"' and is_active = 1"
					cursor.execute(sql_searchUserID_query)
					userID = cursor.fetchall()
					userID = userID[0][0]
					sql_queryinto_authuser_group = "Insert into auth_user_groups_dummy(user_id,group_id) values(?,?)"
					values = (userID,groupID)
					cursor.execute(sql_queryinto_authuser_group,values)
					cnxn.commit()
					#print ("auth_group_dummy and auth_user_groups_dummy has been updated")
				else:
					#print ("Group not updated")
					sql_searchUserID_query = "select id from auth_user_dummy where username = '"+username+"' and is_active = 1"
					cursor.execute(sql_searchUserID_query)
					userID = cursor.fetchall()
					userID = userID[0][0]
					sql_query = "select id from auth_group_dummy where name = '"+users_list_dict[i][0]+"'"
					cursor.execute(sql_query)
					groupID = cursor.fetchall()
					groupID = groupID[0][0]
					sql_queryinto_authuser_group = "Insert into auth_user_groups_dummy(user_id,group_id) values(?,?)"
					values = (userID,groupID)
					cursor.execute(sql_queryinto_authuser_group,values)
					cnxn.commit()
			users_list_AD = []
			for i in users_list_dict.keys():
				users_list_AD.append(i)
			if len(users_list_fromDB) > len(users_list_AD):
				for i in users_list_fromDB:
					if i not in users_list_AD:
						sql_query = "Update auth_user_dummy set is_active = 0 where username = '"+i+"'"
						cursor.execute(sql_query)
						cnxn.commit()
						sql_query_authuser_AD = "Update auth_userAD set is_active = 0, record_status = 'deleted' where username = '"+i+"'"
						cursor.execute(sql_query_authuser_AD)
						cnxn.commit()
except Exception as e:
	#print ("Exception Error:",e)
