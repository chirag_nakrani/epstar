"""
WSGI config for EPSTAR project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/howto/deployment/wsgi/
"""

import sys
import os

BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if BASE_PATH not in sys.path:
    sys.path.append(BASE_PATH)
	
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "EPSTAR.settings")

# from django.core.wsgi import get_wsgi_application
from configurations.wsgi import get_wsgi_application

application = get_wsgi_application()

