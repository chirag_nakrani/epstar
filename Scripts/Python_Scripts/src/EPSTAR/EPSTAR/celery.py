from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from django.conf import settings


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'EPSTAR.settings')

import configurations
configurations.setup()



os.environ.setdefault('FORKED_BY_MULTIPROCESSING', '1')
sqlite_database = os.environ.get('SQLITE_DATABASE')
app = Celery('EPSTAR',backend='db+sqlite:///'+sqlite_database, broker='amqp://localhost//',namespace='CELERY')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks(settings.INSTALLED_APPS)

CELERY_BROKER_URL = 'amqp://localhost//'
CELERY_IMPORTS = ('IndentPDF.tasks','HistoricalData.tasks',)
CELERY_RESULT_BACKEND = 'db+sqlite:///'+sqlite_database
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))
