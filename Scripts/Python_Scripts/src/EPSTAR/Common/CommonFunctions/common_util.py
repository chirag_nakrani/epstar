import os
import pandas as pd
import xlrd
import time , datetime
import pyodbc
import csv
from Common import db_queries_properties
from Model import models
from Common.models import data_update_log , data_update_log_master , data_update_log_reference
from Common import masterconf
from django.conf import settings
from django.db import connection
import logging
import pysftp
import paramiko
import paramiko,paramiko.transport as transport
from num2words import num2words

class common_utils ( ) :

    def initiateLogger ( self ) :
        logger = logging.getLogger ( __name__ )
        log_file = masterconf.LogFolder + "systemErr.log"
        handler = logging.FileHandler ( log_file )
        formatter = logging.Formatter ( '%(asctime)s - %(name)s - %(levelname)s - %(message)s' )
        handler.setFormatter ( formatter )
        logger.addHandler ( handler )
        return logger

    def divide_store_file ( self , request ) :

        # ES1-T218 - Folder re-structuring done

        logger = logging.getLogger(__name__)
        basepath = masterconf.InputFolder
        current_date = datetime.datetime.fromtimestamp ( time.time ( ) ).strftime ( '%d-%m-%Y' )
        username = request.userinfo['username'].username
        file_type = ''
        bank_code = ''
        bank_project = ''
        cra = ''
        if 'file_type' in request.POST.keys ( ) :
            file_type = request.POST['file_type']
            if 'bank_code' in request.POST.keys ( ) :
                project_id = ''
                if 'project_id' in request.POST.keys ( ) :
                    project_id = request.POST['project_id']
                bank_code = request.POST['bank_code']
                bank_project = str ( bank_code ) + "-" + str ( project_id )
                input_path = basepath + "\\" + file_type + "\\" + bank_project + "\\" + username + "\\" + current_date + "\\"
            elif 'cra' in request.POST.keys ( ) :
                cra = request.POST['cra']
                input_path = basepath + "\\" + file_type + "\\" + cra + "\\" + username + "\\" + current_date + "\\"
            else:
                input_path = basepath + "\\" + file_type +  username + "\\" + current_date + "\\"
        else :
            input_path = basepath + "\\" + username + "\\" + current_date + "\\"
        input_path = input_path.replace ( "\\" , "/" )
        if not os.path.exists ( input_path ) :
            os.makedirs ( input_path )
        if 'signatory_name' in request.POST.keys():
            filehandle = request.FILES['physical_signature']
            filehandle_name = request.FILES['physical_signature'].name
        else:
            filehandle = request.FILES['file']
            filehandle_name = request.FILES['file'].name
        input_filepath = os.path.join ( input_path , filehandle_name )
        with open ( input_filepath , 'wb+' ) as fout :
            # Iterate through the chunks.
            for chunk in filehandle.chunks ( ) :
                fout.write ( chunk )
            fout.close ( )
        logger.info("Input filepath for incoming file is :: %s", input_filepath)
        return input_filepath

    def read_file_xls ( self , file_path ) :
        logger = logging.getLogger(__name__)
        try :
            logger.info ( "IN read_file_xls METHOD OF common_util" )
            wb = xlrd.open_workbook ( file_path )
            df = pd.read_excel ( wb , engine='xlrd' )
            return df
        except Exception as e :
            logger.error ( 'EXCEPTION OF read_file_xls, FAILED TO OPEN THE FILE %s ' ,e)
            raise e

    ################# reading escalation matrix excel file which has row 1 and row 2 as header ###############
    def read_file_xls_escalation ( self , file_path ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN read_file_xls_escalation METHOD OF common_util" )
        try :
            wb = xlrd.open_workbook ( file_path )
            ##### skipping first row and using first row after skipping as header with index none #######
            df = pd.read_excel ( wb , engine='xlrd' , index_col=None , header=[0] , skiprows=[0] )
            return df
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'IN EXCEPTION OF read_file_xls_escalation, FAILED TO OPEN THE FILE' , exc_info=True )
            raise e

    def add_fields_to_df ( self , df , fortransaction , update_date_time , user , actual_upload_time , Project_id ,
                           reference_id ) :
        try :
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_to_df METHOD OF common_util" )
            if fortransaction == 'Uploaded' :
                df['project_id'] = Project_id
                df['record_status'] = fortransaction
                df['created_on'] = update_date_time
                df['created_by'] = user
                df['created_reference_id'] = reference_id
                df['datafor_date_time'] = actual_upload_time
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_to_df METHOD OF common_util: ALL FILEDS ADDED." )
            return df
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'IN EXCEPTION OF add_fields_to_df METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            raise e

    def write_to_csv ( self , df , filehandle_name , request_post , userinfo ) :

        # ES1-T218 - Folder re-structuring done

        logger = logging.getLogger(__name__)
        current_date = datetime.datetime.fromtimestamp(time.time()).strftime('%d-%m-%Y')
        username = userinfo['username'].username
        file_type = ''
        bank_code = ''
        bank_project = ''
        cra = ''
        try:
            if masterconf.is_sftp:
                basepath = masterconf.sftp_parent_folder
            else:
                basepath = masterconf.OutputFolder
            if 'file_type' in request_post.keys():
                file_type = request_post['file_type']
                if 'bank_code' in request_post.keys():
                    project_id = ''
                    if 'project_id' in request_post.keys():
                        project_id = request_post['project_id']
                    bank_code = request_post['bank_code']
                    bank_project = str(bank_code) + "-" + str(project_id)
                    output_path = basepath + "\\" + file_type + "\\" + bank_project + "\\" + username + "\\" + current_date + "\\"
                    local_app_server_path = file_type + "\\" + bank_project + "\\" + username + "\\" + current_date + "\\"
                elif 'cra' in request_post.keys():
                    cra = request_post['cra']
                    output_path = basepath + "\\" + file_type + "\\" + cra + "\\" + username + "\\" + current_date + "\\"
                    local_app_server_path = file_type + "\\" + cra + "\\" + username + "\\" + current_date + "\\"
                else:
                    output_path = basepath + "\\" + username + "\\" + current_date + "\\"
                    local_app_server_path = username + "\\" + current_date + "\\"
            else:
                output_path = basepath + "\\" + username + "\\" + current_date + "\\"
                local_app_server_path = username + "\\" + current_date + "\\"
            print(" output_path :: ",output_path)
            print(" local_app_server_path :: ", local_app_server_path)
            if masterconf.is_sftp:
                path = masterconf.OutputFolder + "\\" + local_app_server_path
                if not os.path.exists(path):
                    os.makedirs(path)
                csv_file_path = os.path.join(path, filehandle_name.split('.')[0] + ".csv")
                df.to_csv(csv_file_path, quoting=csv.QUOTE_ALL, index=False)
                cnopts = pysftp.CnOpts()
                cnopts.hostkeys = None
                with pysftp.Connection(masterconf.sftp_host, port=masterconf.sftp_host_port,
                                       username=masterconf.sftp_host_username, password=masterconf.sftp_host_password,
                                       cnopts=cnopts) as sftp:
                    if not sftp.exists(output_path):
                        sftp.makedirs(output_path)
                    print("Before File upload ",csv_file_path)
                    new_dir = sftp.chdir(output_path)
                    sftp.put(csv_file_path,remotepath=new_dir)
            else:
                if not os.path.exists(output_path):
                    os.makedirs(output_path)
                csv_file_path = os.path.join(output_path, filehandle_name.split('.')[0] + ".csv")
                df.to_csv(csv_file_path, quoting=csv.QUOTE_ALL, index=False)
            print(" Before return csv_file_path ::: ",csv_file_path)
            if masterconf.is_sftp:
                full_path_server = masterconf.sfpt_file_path + output_path
                csv_file_path = os.path.join(full_path_server, filehandle_name.split('.')[0] + ".csv")
            csv_file_path = csv_file_path.replace ( "\\" , "/" )
            logger.info("csv file path from write_to_csv  %s", csv_file_path)
            return csv_file_path
        except Exception as e :
            print(e)
            raise e

    def insertrecord ( self , sql_string ) :
        conn = pyodbc.connect ( masterconf.connection_string );
        crsr = conn.cursor ( )
        logger = logging.getLogger(__name__)
        try :
            logger.info ( "IN insertrecord METHOD OF common_util" )
            crsr.execute ( sql_string );
            conn.commit ( )
            logger.info("Insert complete")
        except Exception as e :
            logger.error ( 'EXCEPTION IN insertrecord METHOD OF common_util %s' ,e)
            raise e

    def load_data_database ( self , file_path , file_type , bank_code ) :
        logger = logging.getLogger(__name__)
        logger.info ( "IN load_data_database METHOD OF common_util" )
        table_name_key = file_type + "_" + bank_code + "_TABLE"
        format_file_path_key = file_type + "_" + bank_code + "_FORMAT_FILE"
        dynamic_fields_key = file_type + "_" + bank_code
        tablename = db_queries_properties.table_dict[table_name_key]
        delimeter = db_queries_properties.file_delimeter
        format_file_path = db_queries_properties.format_file_path[format_file_path_key]
        logger.info(" format_file_path :::: %s ",format_file_path)
        dynamic_fields = db_queries_properties.fields_dict[dynamic_fields_key]
        dynamic_fields1 = dynamic_fields[1 :len ( dynamic_fields ) - 1]
        try :
            sql_string = """INSERT INTO [dbo].[{tablename}]
                        {fields}
                        SELECT {selectfields}
                        FROM OPENROWSET(
                            BULK '{path}',
                            FIRSTROW=2,
                            FORMATFILE= '{format_file}'
                        ) as T1;""".format ( tablename=tablename , fields=dynamic_fields , path=file_path ,
                                             file_delimeter=delimeter , format_file=format_file_path ,
                                             selectfields=dynamic_fields1 );
            logger.info("sql_string from load_data_database : %s ", sql_string )
            self.insertrecord ( sql_string )
        except Exception as e :
            logger.error ( 'EXCEPTION IN BULK INSERT (load_data_database) METHOD OF common_util. %s ' ,e)
            raise e

    def validate_format_xls ( self , df , file_type , bankcode ) :
        logger = logging.getLogger(__name__)
        logger.info ( "IN validate_format_xls METHOD OF common_util." )
        column_list = []
        logger.info ("df.columns.values: %s ", df.columns.values )
        for column in df.columns.values :
            if ' ' in column :
                column = column.replace ( ' ' , '_' )
            column_list.append ( column )
        logger.info("printing column_list :: %s ", column_list )
        column_name_str = "+".join ( column_list )
        columnnames = column_name_str + "+"
        print ("column_name_str : {}".format (
            column_name_str ))  # TERM_ID+ACCEPTORNAME+LOCATION+Tot_Cash+Cassette1+Cassette2+Cassette3+cassette4
        print ("columnnames : {}".format (
            columnnames ))  # TERM_ID+ACCEPTORNAME+LOCATION+Tot_Cash+Cassette1+Cassette2+Cassette3+cassette4+
        try :
            frmt_status = models.validate_file_frmt ( file_type , bankcode , columnnames )
            logger.info("frmt_status in format validation :::::::: %s " , frmt_status)
            return frmt_status
        except Exception as e :
            logger.error ( 'EXCEPTION IN validate_format_xls METHOD OF common_util %s ' ,e)
            raise e

    def maintainlog ( self , file_type , bank_code , actual_upload_time , record_status , upload_time , Pid , username ,
                      reference_id ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN maintainlog METHOD OF common_util." )
        print("In maintainlog method")
        if file_type == 'C3R_CMIS' or file_type == 'C3R_VMIS' :
            file_type = 'C3R'
        metadata = {
            "data_for_type" : file_type ,
            "bank_code" : bank_code ,
            "datafor_date_time" : actual_upload_time ,
            "record_status" : record_status ,
            "date_created" : upload_time ,
            "project_id" : Pid ,
            "created_reference_id" : reference_id ,
            "created_by" : username
        }
        try :
            print("#printing metadata : {}".format ( metadata ))
            logger.setLevel ( logging.INFO )
            logger.info ( "IN maintainlog METHOD OF common_util, PROCEDING TO SAVE METADATA TO data_update_log." )
            data_update = data_update_log ( **metadata )
            data_update.save ( )
            print("Save Successfully")
            return data_update.id
        except Exception as e :
            print(e)
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN maintainlog METHOD OF common_util.' , exc_info=True )
            print(e)
            raise e

    def validate_data ( self , strfile_type , createddate , bankcode , record_status , referenceid , systemUser ,
                        actual_upload_time , Project_id ) :
        try :
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.INFO )
            logger.info ( "In validate_data" )
            print("In validate_data METHOD OF common_util.")
            status_ret = models.validate_and_consoldate_data ( strfile_type , createddate , bankcode , record_status ,
                                                               referenceid , systemUser , actual_upload_time ,
                                                               Project_id )
            print ("status_ret from common_util validate_data : {}".format ( status_ret ))
            return status_ret
        except Exception as e :
            print ("Exception from validate_data : " , e)
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN validate_data METHOD OF common_util.' , exc_info=True )
            raise e

    def response_generator ( self , flag ) :
        # print("In response_generator method")
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN response_generator METHOD OF common_util." )
        if flag :
            return masterconf.success_response;
        else :
            return masterconf.failure_response;

    def validate_upload_req ( self , request ) :
        logger = logging.getLogger(__name__)
        logger.info ( "IN validate_upload_req METHOD OF common_util." )
        params = masterconf.upload_req_params[request.POST['file_type']]  # ['file_type','bank_code','upload_datatime'];
        logger.info("params from validate_upload_req :: %s ", params)
        status_flag = False
        if 'file' not in request.FILES.keys ( ) :
            status_flag = False
        else :
            for param in params :
                if param not in request.POST.keys ( ) :
                    status_flag = False
                    break
                else :
                    status_flag = True
        return status_flag

    def validate_fileinfo_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_fileinfo_req METHOD OF common_util." )
        params = (masterconf.upload_req_params[request.data['routine_info'].lower ( )])
        status_flag = False
        for param in params :
            if param not in request.data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
            return status_flag

    def validate_filterInfo ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_filterInfo METHOD OF common_util." )
        params = masterconf.upload_req_params[request.data['common_bankfeeder_info'].lower ( )]
        status_flag = False
        for param in params :
            if param not in request.data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def check_uploaded_file ( self , file_type , bank_code , upload_time , status ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_filterInfo METHOD OF common_util." )
        is_present = True;
        query_set = data_update_log.objects.filter ( data_for_type=file_type , bank_code=bank_code ,
                                                     data_for_datetime=upload_time , record_status=status );
        if query_set.count ( ) == 0 :
            is_present = False;
        return is_present;

    def upload_file_sftp ( self , csv_file_path ) :
        # 	# cnopts = pysftp.CnOpts()
        # 	# cnopts.hostkeys = None
        # 	# upload_file_name = csv_file_path.split(".")[0]
        # 	# #print ("m in upload_file_sftp")
        # 	# with pysftp.Connection(host=masterconf.sftp_host, port=masterconf.sftp_host_port,username =masterconf.sftp_host_username,password=masterconf.sftp_host_password,cnopts = cnopts) as sftp:
        # 	# 	#print("Connected To DB Server")
        # 	# 	sftp.put(csv_file_path);
        # 	# 	#print("File Uploaded To DB Server")
        # 	# return os.path.basename(csv_file_path);
        # logger = self.initiateLogger ( )
        # logger.setLevel ( logging.INFO )
        # logger.info ( "IN upload_file_sftp METHOD OF common_util." )
        return csv_file_path

    def validate_view_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_view_req METHOD OF common_util." )
        params = masterconf.view_req_params  # ['file_type','bank_code','upload_datatime'];
        print ("params from validate_view_req : " , params)
        print("request.data from validate_view_req : " , request.data)
        status_flag = False
        for param in params :
            if param not in request.data.keys ( ) :
                status_flag = False;
                break
            else :
                status_flag = True;
        print("status_flag from validate_view_req : " , status_flag)
        return status_flag

    def validate_status_update_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_status_update_req METHOD OF common_util." )
        if (request.data['file_type']) == masterconf.file_type_vcb_api :
            params = masterconf.status_update_param_vcb  # ['file_type','bank_code','datafor_date_time','status']
        else :
            params = masterconf.status_update_param
        status_flag = False
        for param in params :
            if param not in request.data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        print("status_flag from validate_status_update_req : " , status_flag)
        return status_flag

    def trimAllColumns ( self , df ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN trimAllColumns METHOD OF common_util." )
        trimStrings = lambda x : x.strip ( ) if type ( x ) is str else x
        return df.applymap ( trimStrings )

    def fetchColumnName ( self , df_data ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN fetchColumnName METHOD OF common_util." )
        columnname = []
        firstrow = df_data.head ( 1 )
        firstrow = self.trimAllColumns ( firstrow )
        nullcolumns_firstrow = firstrow.columns[firstrow.isnull ( ).any ( )]
        secondrow = df_data.head ( 2 )
        secondrow = self.trimAllColumns ( secondrow )
        nullcolumns_secondrow = secondrow.columns[secondrow.isnull ( ).any ( )]
        if (len ( nullcolumns_firstrow ) > 0) :
            for index in range ( 0 , len ( nullcolumns_firstrow ) ) :
                firstrow.values[0][nullcolumns_firstrow[index]] = ""
        if (len ( nullcolumns_secondrow ) > 0) :
            for index1 in range ( 0 , len ( nullcolumns_secondrow ) ) :
                secondrow.values[1][nullcolumns_secondrow[index1]] = ""
        for index in range ( 0 , len ( secondrow.columns ) ) :
            if (firstrow[index][0] == "") :
                columnname.append ( secondrow[index][0] )
            elif (secondrow[index][1] == "") :
                columnname.append ( firstrow[index][0] )
            else :
                columnname.append ( firstrow[index][0] + "_" + str ( secondrow[index][1] ) )
        df = pd.DataFrame ( columnname )
        df1 = list ( pd.DataFrame ( df , columns=columnname ) )
        for i in range ( len ( df1 ) ) :
            df1[i] = df1[i].lower ( )
        return df1

    def validate_mstr_format_xls ( self , df_data , file_type ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_mstr_format_xls METHOD OF common_util." )
        column_list = []
        for column in df_data.columns.values :
            column = str ( column ).strip ( )
            if ' ' in column :
                column = column.replace ( ' ' , '_' )
            column_list.append ( column )
        print("printing column_list from validate_mstr_format_xls: {}".format ( column_list ))
        column_name_str = "+".join ( column_list )
        columnnames = column_name_str + "+"
        try :
            frmt_status = models.validate_mstr_file_frmt ( file_type.upper ( ) , columnnames )
            return frmt_status
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN validate_mstr_format_xls METHOD OF common_util.' , exc_info=True )
            raise e

    def add_fields_master ( self , df , uploaded_status , upload_time , username , reference_id , file_type ) :
        try :
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_master METHOD OF common_util." )
            type = str ( file_type ).upper ( )
            fields_list = db_queries_properties.master_attr_dict[type]
            columns_not_included = list ( )
            for column in fields_list :
                if column not in df.columns.values :
                    columns_not_included.append ( column )
            for column in columns_not_included :
                df[str ( column )] = None
            if uploaded_status == 'Uploaded' :
                df['record_status'] = uploaded_status
                df['created_on'] = upload_time
                df['created_by'] = username
                df['created_reference_id'] = reference_id
            return df
        except Exception as e :
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN add_fields_master METHOD OF common_util.' , exc_info=True )
            raise e

    def maintainlog_master ( self , file_type , record_status , upload_time , username , reference_id ) :
        print("In maintainlog_master method")
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN maintainlog_master METHOD OF common_util." )
        metadata = {
            "data_for_type" : file_type ,
            "record_status" : record_status ,
            "date_created" : upload_time ,
            "created_by" : username ,
            "created_reference_id" : reference_id
        }
        try :
            logger.setLevel ( logging.INFO )
            logger.info (
                "IN maintainlog_master METHOD OF common_util, PROCEDING TO SAVE METADATA TO data_update_log_master." )
            print("printing metadata : {}".format ( metadata ))
            data_update = data_update_log_master ( **metadata )
            data_update.save ( )
            print("Save Successfully")
            return data_update.id
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN maintainlog_master METHOD OF common_util.' , exc_info=True )
            raise e

    def validate_ams_api_req ( self , request_post ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_ams_api_req METHOD OF common_util." )
        params = masterconf.status_master_param
        status_flag = False
        print("request_post from validate_ams_api_req : " , request_post)
        for param in params :
            print("request_post from validate_ams_api_req : " , request_post)
            if param not in request_post.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        print("status_flag from validate_ams_api_req : " , status_flag)
        return status_flag

    def validate_master_update_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_master_update_req METHOD OF common_util." )
        params = masterconf.status_master_update_param
        status_flag = False
        for param in params :
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_master_data ( self , file_type , apiflag , username , reference_id ) :
        try :
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.INFO )
            logger.info ( "IN validate_master_data METHOD OF common_util." )
            sql_str = "SET NOCOUNT ON exec uspValidateMasterFiles " + "'" + file_type + "', " + "'" + apiflag + "', " + "'" + username + "'," + "'" + reference_id + "'"
            print("sql_str from validate_master_data : {}".format ( sql_str ))
            cur = connection.cursor ( )
            cur.execute ( sql_str )
            code = cur.fetchone ( )
            print("code is ::: " , code[0])
            return code[0]
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN validate_master_data METHOD OF common_util.' , exc_info=True )
            raise e

    def validate_status_req ( self , request_data ) :  # Name Changed as per naming convention

        ## This method is used to validate incoming request for master and Reference data for Approval and Rejection API

        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_status_req METHOD OF common_util." )
        params = masterconf.status_app_rej  # Name Changed as per naming convention
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def change_status ( self , request ) :  # Name Changed as per naming convention

        ## This method is used to change the status of records to Active or Reject for Master, Rejection, Indent Menus

        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN change_status_master_ref METHOD OF common_util." )
        request_status = request.data['status']  # Active, Rejected
        menu = request.data['menu']
        reference_id = request.data['created_reference_id']
        username = request.userinfo['username'].username
        upload_time = datetime.datetime.fromtimestamp ( time.time ( ) ).strftime ( '%Y-%m-%d %H:%M:%S' )
        table_key = str ( menu ).upper ( ) + "_TABLE"
        table = db_queries_properties.all_table_dict[table_key]  # Name Changed as per naming convention
        id_str = request.data["id"]
        # id_str = ','.join(ids_list)
        ## Calling uspSetStatus_masterdata SP to update records to Active or Rejected
        #sql_str = "exec uspSetStatus_master_ref " + "'" + table + "', " + "'" + request_status + "', " + "'" + upload_time + "'," + "'" + username + "','" + id_str + "'" + ",'" + reference_id + "'"
        sql_str = "exec uspSetStatus_master_ref '{}', '{}', '{}', '{}', '{}', '{}'".format(table,request_status,upload_time,username,id_str,reference_id)
        print("sql_str from uspSetStatus_master_ref : {}".format ( sql_str ))
        try :
            nocount = """ SET NOCOUNT ON; """
            cur = connection.cursor ( )
            cur.execute ( nocount + sql_str )
            code = cur.fetchone ( )
            print ("code generated after changing the status from change_status_master_ref: " , code[0])
            return code[0]
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'Exception in change_status_master_ref' , exc_info=True )
            raise e

    def validate_create_ticket_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_create_ticket_req METHOD OF common_util." )
        params = masterconf.create_ticket_fields
        status_flag = False
        for param in params :
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_create_ticket_reply ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_create_ticket_reply METHOD OF common_util." )
        params = masterconf.create_ticket_reply_fields
        status_flag = False
        for param in params :
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_ticket_detail_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_ticket_detail_req METHOD OF common_util." )
        params = masterconf.ticket_detail_req
        status_flag = False
        for param in params :
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def fieldsmapping ( self , incomingdict , mappinglist ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN fieldsmapping METHOD OF common_util." )
        returnlist = list ( );
        for field in mappinglist :
            if field in incomingdict.keys ( ) :
                returnlist.append ( field );
        return returnlist;

    def validate_rejection_req ( self , request_post ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_rejection_req METHOD OF common_util." )
        params = masterconf.rejection_param
        status_flag = False
        for param in params :
            if param not in request_post.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_signature_req ( self , request_post ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_signature_req METHOD OF common_util." )
        params = masterconf.status_signature_param
        status_flag = False
        print ("params : " , params)
        print (request_post.keys ( ))
        for param in params :
            if param not in request_post.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_signature_download_req ( self , request_data ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_signature_download_req METHOD OF common_util." )
        params = masterconf.status_signature_download
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_approve_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_approve_req METHOD OF common_util." )
        params = masterconf.c3rapprovalpending  # ['file_type','bank_code','upload_datatime'];
        print ("params from validate_approve_req : {}".format ( params ))
        status_flag = False
        for param in params :
            if param not in request :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_viewc3r_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_viewc3r_req METHOD OF common_util." )
        params = masterconf.c3rview  # ['file_type','bank_code','upload_datatime'];
        status_flag = False
        for param in params :
            if param not in request :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_indent_detail_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_indent_detail_req METHOD OF common_util." )
        params = masterconf.indent_detail_req
        status_flag = False
        for param in params :
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def add_fields_to_df_c3r ( self , df , fortransaction , update_date_time , user , actual_upload_time , Project_id ,
                               reference_id ) :
        try :
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_to_df_c3r METHOD OF common_util" )
            if fortransaction == 'Uploaded' :
                df['project_id'] = Project_id
                df['record_status'] = fortransaction
                df['created_on'] = update_date_time
                df['created_by'] = user
                df['created_reference_id'] = reference_id
                df['datafor_date_time'] = actual_upload_time
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_to_df_c3r METHOD OF common_util: ALL FIELDS ADDED." )
            return df
        except Exception as e :
            print(e)
            logger.setLevel ( logging.ERROR )
            logger.error ( 'IN EXCEPTION OF add_fields_to_df_c3r METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            raise e

    def validate_amsMaster_req ( self , request_data ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_amsMaster_req METHOD OF common_util." )
        params = masterconf.view_amsMaster_request
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_amsview_req ( self , request_data ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_amsview_req METHOD OF common_util." )
        params = masterconf.amsview_req
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_masterapi_req ( self , request_post ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_masterapi_req METHOD OF common_util." )
        params = masterconf.status_masterapi_param
        status_flag = False
        for param in params :
            if param not in request_post.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_reference_format_xls ( self , df , file_type ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_reference_format_xls METHOD OF common_util." )
        column_list = []
        for column in df.columns.values :
            if ' ' in column :
                column = column.replace ( " - " , '_' )  # replacing ' with _
                column = column.replace ( ' ' , '_' )
                column = column.replace ( '..' , '_' )  # replacing .. with _
                column = column.replace ( '.' , '_' )  # replacing . with _
                column = column.replace ( "'" , '_' )  # replacing ' with _
            column_list.append ( column )
        column_name_str = "+".join ( column_list )
        columnnames = column_name_str + "+"
        try :
            print(columnnames)
            nocount = """ SET NOCOUNT ON; """
            # sql_str = "exec uspFormatValidation " + "'" + datafor + "', " + "'" + bankcode + "', " + "'" +columns + "' ";
            sql_str = "exec uspReferenceFormatValidation " + "'" + file_type + "', " + "'" + columnnames + "' "
            print("sql_str from validate_reference_format_xls : {}".format ( sql_str ))
            cur = connection.cursor ( )
            cur.execute ( nocount + sql_str )
            frmt_seq_no = cur.fetchone ( )
            if frmt_seq_no :
                return frmt_seq_no[0]
            else :
                return 'E100'
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN validate_format_xls METHOD OF common_util.' , exc_info=True )
            raise e

    def add_fields_reference ( self , df , uploaded_status , upload_time , username , reference_id , file_type ) :
        try :
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_reference METHOD OF common_util" )
            type = str ( file_type ).upper ( )
            fields_list = db_queries_properties.reference_attr_dict[type]
            columns_not_included = list ( )
            for column in fields_list :
                if column not in df.columns.values :
                    columns_not_included.append ( column )
            # print(columns_not_included)
            for column in columns_not_included :
                df[str ( column )] = None
            if uploaded_status == 'Uploaded' :
                # df['project_id'] = project_id
                df['record_status'] = uploaded_status
                df['created_on'] = upload_time
                df['created_by'] = username
                df['created_reference_id'] = reference_id
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_reference METHOD OF common_util: ALL FILEDS ADDED." )
            return df
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'IN EXCEPTION OF add_fields_reference METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            raise e

    def maintainlog_reference ( self , file_type , uploaded_status , upload_time , username , reference_id ) :
        print("In maintainlog reference")
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN maintainlog_reference METHOD OF common_util." )
        metadata = {
            "data_for_type" : file_type ,
            "record_status" : uploaded_status ,
            "date_created" : upload_time ,
            # "project_id": project_id,
            "created_by" : username ,
            "created_reference_id" : reference_id
        }
        try :
            logger.setLevel ( logging.INFO )
            logger.info (
                "IN maintainlog_reference METHOD OF common_util, PROCEDING TO SAVE METADATA TO data_update_log_master." )
            data_update = data_update_log_master ( **metadata )
            data_update.save ( )
            print ("Log has been added.")
            if data_update.id :
                return data_update.id
            else :
                return -1
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN maintainlog_reference METHOD OF common_util.' , exc_info=True )
            raise e

    def validate_reference_data ( self , file_type , apiflag , username , reference_id ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_reference_data METHOD OF common_util." )
        try :
            sql_str = "SET NOCOUNT ON exec uspValidateReferenceFiles " + "'" + file_type + "', " + "'" + apiflag + "', " + "'" + username + "'," + "'" + reference_id + "'"
            print("sql_str from validate_reference_data : {}".format ( sql_str ))
            cur = connection.cursor ( )
            cur.execute ( sql_str )
            code = cur.fetchone ( )
            print("code is ::: " , code[0])
            return code[0]
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN validate_reference_data METHOD OF common_util.' , exc_info=True )
            raise e

    def validate_reference_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_reference_req METHOD OF common_util." )
        params = masterconf.status_reference_param
        print ("Params from validate_reference_req : " , params)
        status_flag = False
        for param in params :
            # print(request)
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_user_bank_project_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_indent_detail_req METHOD OF common_util." )
        params = masterconf.user_bank_project_params
        # #print (params)
        status_flag = False
        for param in params :
            # print(request)
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
            # print(status_flag)
        return status_flag

    def validate_menu_perm_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_menu_perm_req METHOD OF common_util." )
        params = masterconf.menu_perm_req
        # #print (params)
        status_flag = False
        for param in params :
            # print(request)
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
            # print(status_flag)
        return status_flag

    def validate_view_req_ims ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_view_req_ims METHOD OF common_util." )
        params = masterconf.view_req_params_ims  # ['file_type','bank_code','upload_datatime'];
        print ("params from validate_view_req_ims : " , params)
        print("request.data from validate_view_req_ims : " , request.data)
        status_flag = False
        for param in params :
            if param not in request.data.keys ( ) :
                status_flag = False;
                break
            else :
                status_flag = True;
        print("status_flag from validate_view_req_ims : " , status_flag)
        return status_flag

    def validate_reference_update_req ( self , request ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_master_update_req METHOD OF common_util." )
        params = masterconf.status_reference_update_param
        status_flag = False
        for param in params :
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def check_null_dataframe ( self , df ) :

        ## This Method is used to check null or empty values in dataframe

        for col in df :
            dt = df[col].dtype
            if dt == int or dt == float :
                df[col].fillna ( 0 , inplace=True )
                df[col] = df[col].astype ( 'int' )
            else :
                df[col].fillna ( "" , inplace=True )
        return df

    #############checking if input request is correct for default uploading###########################
    def validate_defaultLoad_InputReq ( self , request_data ) :
        params = masterconf.defaultLoad_InputReq
        print ("Input params receiving for default loading: " , request_data)
        print("params from validate_defaultLoad_InputReq :" , params)
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        print("Returned status flag from validate_defaultLoad_InputReq : " , status_flag)
        return status_flag

    def validate_indent_status_req ( self , request ) :

        # This method is used to validate incoming request params for Indent approval and rejection API

        params = masterconf.status_indent_param
        status_flag = False
        for param in params :
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_indent_generate_req ( self , request ) :

        # This method is used to validate incoming request params for Indent generation API

        params = masterconf.status_indent_generate_param
        status_flag = False
        for param in params :
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    ############ creating final page data for pdf ##################
    def final_pdf_page_values ( self , df ) :
        logger = logging.getLogger(__name__)
        df_final = {}
        try :
            logger.info ( "IN final_pdf_page_values METHOD OF common_util" )
            final_page_fields = masterconf.final_page

            for i , r in df.iterrows ( ) :
                for fields in final_page_fields :
                    if r[fields]:
                        df_final[fields] = r[fields]
                    else:
                        df_final[fields] = int(0)
            logger.setLevel ( logging.INFO )
            logger.info ( "IN final_pdf_page_values METHOD OF common_util: ALL FIELDS ADDED." )
            return df_final
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'IN EXCEPTION OF final_pdf_page_values METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            return df_final

    ############# creating header data for pdf ###############
    def header_template_fields ( self , df ) :
        logger = logging.getLogger(__name__)
        df_header = {}
        try :
            logger.info ( "IN header_template_fields METHOD OF common_util" )
            header_fields = masterconf.header_fields
            for i , r in df.iterrows ( ) :
                for fields in header_fields :
                    if isinstance(r[fields],datetime.date):
                        df_header[fields] = datetime.datetime.strftime(r[fields],'%d-%b-%Y')
                    else:
                        if r[fields]:
                            df_header[fields] = r[fields]
                        else:
                            df_header[fields] = '-'
                            
            logger.info ( "IN header_template_fields METHOD OF common_util: ALL FIELDS ADDED." )
            return df_header
        except Exception as e :
            print(e)
            logger.setLevel ( logging.ERROR )
            logger.error ( 'IN EXCEPTION OF header_template_fields METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            return df_header

    ########## creating footer data for pdf ###########
    def footer_template_fields ( self , df ) :
        logger = logging.getLogger(__name__)
        df_footer = { }
        try :
            logger.info ( "IN footer_template_fields METHOD OF common_util" )
            footer_fields = masterconf.footer_fields
            for i , r in df.iterrows ( ) :
                for fields in footer_fields :
                    if fields in ['signature_authorized_by_1_auth_signatories_signatures_id' ,
                                  'signature_authorized_by_2_auth_signatories_signatures_id'] :
                        try :
                            signature_data = models.Auth_Signatories_Signatures.objects.get ( id=r[fields] )
                            df_footer[fields] = signature_data.physical_signature.decode ( )
                        except :
                            df_footer[fields] = ''
                    else :
                        df_footer[fields] = r[fields]
            #### get physical signature from Auth signature model ####
            logger.setLevel ( logging.INFO )
            logger.info ( "IN footer_template_fields METHOD OF common_util: ALL FIELDS ADDED." )
            return df_footer
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'IN EXCEPTION OF footer_template_fields METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            return df_footer

    ######### creating detail list for table pdf ############
    def detail_template_fields ( self , df ) :
        logger = logging.getLogger(__name__)
        detail_data = []
        try :
            logger.info ( "IN detail_template_fields METHOD OF common_util" )
            detail_fields = masterconf.detail_fields
            index = 1
            for i , r in df.iterrows ( ) :
                df_detail = { }
                for fields in detail_fields :
                    df_detail['sr_no'] = index
                    if fields == 'location':
                        df_detail[fields] = (r[fields][:40] + '..') if len(r[fields]) > 40 else r[fields]
                    else:
                        df_detail[fields] = r[fields]
                index += 1
                detail_data.append ( df_detail )
            logger.setLevel ( logging.INFO )
            logger.info ( "IN detail_template_fields METHOD OF common_util: ALL FIELDS ADDED." )
            return detail_data
        except Exception as e :
            logger.error ( 'IN EXCEPTION OF detail_template_fields METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            return detail_data

    ########## creating page wise data for indent pdf ##################
    def pagewise_data ( self , df , final_page , header_template_vars , footer_template_vars ) :
        logger = logging.getLogger(__name__)
        indent_data_list = []
        try :
            logger.info ( "IN pagewise_data METHOD OF common_util" )
            # if length of data list greater than 1 then use carry forward to next page requirement else no carry forward, use final page data
            if len ( df ) > 1 :
                for l in range ( 0 , len ( df ) ) :
                    load_100_page_total , load_200_page_total , load_500_page_total , load_2000_page_total , load_total = 0.0 , 0.0 , 0.0 , 0.0 , 0.0
                    for m in df[l] :
                        load_100_page_total += m['loading_amount_100']
                        load_200_page_total += m['loading_amount_200']
                        load_500_page_total += m['loading_amount_500']
                        load_2000_page_total += m['loading_amount_2000']
                        load_total += m['total']
                    if df[l] != df[-1] :
                        detail_page = { }
                        indent_data = { }
                        detail_page['carry_forward'] = 'Carried forward to next page:'
                        detail_page['load_100_page_total'] = int(load_100_page_total)
                        detail_page['load_200_page_total'] = int(load_200_page_total)
                        detail_page['load_500_page_total'] = int(load_500_page_total)
                        detail_page['load_2000_page_total'] = int(load_2000_page_total)
                        detail_page['load_total'] = int(load_total)
                        detail_page['amount_in_words'] = num2words(detail_page['load_total'],lang='en_IN').title()
                        indent_data['indent_data'] = df[l]
                        indent_data['carry_forward'] = detail_page
                        indent_data['header'] = header_template_vars
                        indent_data['footer'] = footer_template_vars
                        indent_data_list.append ( indent_data )
                    else :
                        indent_data = { }
                        indent_data['indent_data'] = df[-1]
                        indent_data['carry_forward'] = final_page
                        indent_data['header'] = header_template_vars
                        indent_data['footer'] = footer_template_vars
                        indent_data_list.append ( indent_data )
            else :
                indent_data = { }
                indent_data['indent_data'] = df[0]
                indent_data['carry_forward'] = final_page
                indent_data['header'] = header_template_vars
                indent_data['footer'] = footer_template_vars
                indent_data_list.append ( indent_data )
            logger.info ( "IN pagewise_data METHOD OF common_util: ALL FIELDS ADDED." )
            return indent_data_list
        except Exception as e :
            logger.error ( 'IN EXCEPTION OF pagewise_data METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            return indent_data_list

    ############validating input request to add record for project or bank or region#####################
    def validate_add_project_bank_region_input_req ( self , request ) :
        params = masterconf.add_project_bank_region_input_operation
        status_flag = False
        # print (request.data)
        # print (request.data.keys)
        for param in params :
            if param not in request.data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    def validate_update_ticket_req ( self , request ) :

        # This method is used to validate ticket update API Params

        try :
            params = masterconf.ticket_update_params
            status_flag = False
            # print (request.data)
            # print (request.data.keys)
            for param in params :
                if param not in request.keys ( ) :
                    status_flag = False
                    break
                else :
                    status_flag = True
            return status_flag
        except Exception as e :
            print(e)
            raise e

    def validate_displaydata_req ( self , request ) :
        ############it is used to check the input params to display the data################
        try :
            params = masterconf.display_data_params
            status_flag = False
            for param in params :
                if param not in request.keys ( ) :
                    status_flag = False
                    break
                else :
                    status_flag = True
            print ("status returned from validate_displaydata_req : " , status_flag)
            return status_flag
        except Exception as e :
            print("Exception from validate_displaydata_req : " , e)
            raise e

    #############checking if input request is correct for System Settings ###########################
    def validate_systemSettings_ParamsReq ( self , request_data ) :
        params = masterconf.system_settings
        print("Input params receiving for System Settings: " , request_data)
        print("params from validate_systemSettings_ParamsReq :" , params)
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        print("Returned status flag from validate_systemSettings_ParamsReq : " , status_flag)
        return status_flag

    ### creating dictionary to create system settings ###
    def validate_systemSettings_InsertReq ( self , request_data ) :
        params = masterconf.system_settings
        print("Input params receiving for System Settings: " , request_data)
        print("params from validate_systemSettings_InsertReq :" , params)
        data_dict = { }
        for param in params :
            data_dict[param] = request_data[param]
        print("Returned data from validate_systemSettings_InsertReq : " , data_dict)
        return data_dict

    #############checking if input request is correct for cypher code insert data###########################
    def validate_cyphercode_InputReq ( self , request_data ) :
        params = masterconf.cyphercode_input_request
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        print("Returned status flag from validate_defaultLoad_InputReq : " , status_flag)
        return status_flag

    ###############Format validation of config menu upload files#######################################
    def validate_config_format_xls ( self , df , file_type ) :
        print ("Inside validate_config_format_xls")
        column_list = []
        for column in df.columns.values :
            if ' ' in column :
                column = column.replace ( " - " , '_' )  # replacing ' with _
                column = column.replace ( ' ' , '_' )
                column = column.replace ( '..' , '_' )  # replacing .. with _
                column = column.replace ( '.' , '_' )  # replacing . with _
                column = column.replace ( "'" , '_' )  # replacing ' with _
            column_list.append ( column )
        column_name_str = "+".join ( column_list )
        columnnames = column_name_str + "+"
        print("Printing column Names : " , columnnames)
        try :
            print(columnnames)
            nocount = """ SET NOCOUNT ON; """
            # sql_str = "exec uspFormatValidation " + "'" + datafor + "', " + "'" + bankcode + "', " + "'" +columns + "' ";
            sql_str = "exec uspConfigFormatValidation " + "'" + file_type + "', " + "'" + columnnames + "' "
            print("sql_str from validate_config_format_xls : {}".format ( sql_str ))
            cur = connection.cursor ( )
            cur.execute ( nocount + sql_str )
            frmt_seq_no = cur.fetchone ( )
            if frmt_seq_no :
                return frmt_seq_no[0]
            else :
                return 'E100'
        except Exception as e :
            print("Exception from validate_config_format_xls : " , e)
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN validate_config_format_xls METHOD OF common_util.' , exc_info=True )
            raise e

    ##############Adding additional fields into config menu upload file ############################################
    def add_fields_config ( self , df , uploaded_status , upload_time , username , reference_id , file_type ) :
        try :
            logger = self.initiateLogger ( )
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_config METHOD OF common_util" )
            type = str ( file_type ).upper ( )
            fields_list = db_queries_properties.config_attr_dict[type]
            columns_not_included = list ( )
            for column in fields_list :
                if column not in df.columns.values :
                    columns_not_included.append ( column )
            for column in columns_not_included :
                df[str ( column )] = None
            if uploaded_status == masterconf.uploaded_state :
                df['record_status'] = uploaded_status
                df['created_on'] = upload_time
                df['created_by'] = username
                df['created_reference_id'] = reference_id
            logger.setLevel ( logging.INFO )
            logger.info ( "IN add_fields_config METHOD OF common_util: ALL FILEDS ADDED." )
            return df
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'IN EXCEPTION OF add_fields_config METHOD OF common_util,UNABLE TO ADD FIELDS.' ,
                           exc_info=True )
            raise e

    #############validating input request for config menu input files###################################
    def validate_config_req ( self , request ) :
        params = masterconf.status_config_param
        print ("Params from validate_reference_req : " , params)
        status_flag = False
        for param in params :
            # print(request)
            if param not in request.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag

    ################Validatiing input records################################################
    def validate_config_data ( self , file_type , apiflag , username , reference_id ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_config_data METHOD OF common_util." )
        try :
            sql_str = "SET NOCOUNT ON exec uspValidateConfigFiles " + "'" + file_type + "', " + "'" + apiflag + "', " + "'" + username + "'," + "'" + reference_id + "'"
            print("sql_str from validate_config_data : {}".format ( sql_str ))
            cur = connection.cursor ( )
            cur.execute ( sql_str )
            code = cur.fetchone ( )
            print("code is ::: " , code[0])
            return code[0]
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'EXCEPTION IN validate_config_data METHOD OF common_util.' , exc_info=True )
            raise e

    ### cash reports ###
    def cash_reports_bomh ( self , df , request ) :
        logger = logging.getLogger(__name__)
        logger.info('Inside cash_reports_bomh method of Common util.')
        report_list = list ( )
        no = 1
        new_df = pd.DataFrame ( ).fillna ( '' )
        export_df = pd.DataFrame ( )
        denomination_100, denomination_200, denomination_500, denomination_2000, total_denomination = 0, 0, 0, 0, 0
        #### grouping on feeder branch level and calculating total amount of each denomination and appending row to the new dateframe####
        for name , group in df.groupby ( 'Feeder Branch' ) :
            total_index = 'Total_%s' % str ( no )
            total_row = pd.DataFrame ( { 'ATMID' : no , 'INDENT NO' : no , 'CODE' : group['CODE'].iloc[-1] ,
                                         'Feeder Branch' : group['Feeder Branch'].iloc[-1] + ' Total' ,
                                         '100' : group['100'].sum ( ) ,
                                         '200' : group['200'].sum ( ) ,
                                         '500' : group['500'].sum ( ) ,
                                         '2000' : group['2000'].sum ( ) ,
                                         'Total' : group['Total'].sum ( ) } ,
                                       index=[total_index] ).fillna ( '' )
            new_df = pd.concat ( [group , total_row] , sort=False ).fillna ( '' )
            denomination_100 += group['100'].sum ( )
            denomination_200 += group['200'].sum()
            denomination_500 += group['500'].sum()
            denomination_2000 += group['2000'].sum()
            total_denomination += group['Total'].sum()
            #### if exporting to excel then concat to new dataframe ####
            if 'export' in request.data :
                export_df = pd.concat ( [export_df , new_df] , sort=False )
            no += 1

            for index , row in new_df.iterrows ( ) :
                feeder_dict = { }
                for key in row.keys ( ) :
                    feeder_dict[key] = row[key]
                report_list.append ( feeder_dict )

        ### adding grand total to dataframe ####
        total_df = pd.DataFrame({'ATMID': 'Grand Total',
                                '100': denomination_100,
                                '200': denomination_200,
                                '500': denomination_500,
                                '2000': denomination_2000,
                                'Total': total_denomination},index=['Grand Total']).fillna('')
        export_df = pd.concat([export_df, total_df],sort=False).fillna('')

        for index, row in total_df.iterrows():
            total_dict = {}
            for key in row.keys():
                total_dict[key] = row[key]
            report_list.append(total_dict)

        ##### if excel download then send dataframe else send report list to view #####
        logger.info('End of cash_reports_bomh function of Common util.')
        if 'export' in request.data :
            return export_df
        else :
            return report_list

    #### convert pandas dataframe to dictionary list ####
    def pandas_to_dict ( self , df ) :
        logger = logging.getLogger(__name__)
        logger.info("INSIDE pandas_to_dict METHOD OF Common Util.")
        report_list = list ( )
        for index , row in df.iterrows ( ) :
            row_dict = { }
            for key in row.keys ( ) :
                row_dict[key] = row[key]
            report_list.append ( row_dict )
        logger.info("End of pandas_to_dict METHOD OF Common Util.")
        return report_list

    #################adding extra fields for CBR files only, adding region in it#############################################
    def add_fields_to_df_cbr ( self , df , fortransaction , update_date_time , user , actual_upload_time , Project_id ,
                               reference_id , region ) :
        logger = logging.getLogger(__name__)
        try :
            logger.info ( "IN add_fields_to_df_cbr METHOD OF common_util" )
            if fortransaction == 'Uploaded' :
                df['project_id'] = Project_id
                df['record_status'] = fortransaction
                df['created_on'] = update_date_time
                df['created_by'] = user
                df['created_reference_id'] = reference_id
                df['datafor_date_time'] = actual_upload_time
                df['region'] = region
            logger.info ( "IN add_fields_to_df_cbr METHOD OF common_util: ALL FILEDS ADDED." )
            return df
        except Exception as e :
            logger.error ( 'IN EXCEPTION OF add_fields_to_df_cbr METHOD OF common_util,UNABLE TO ADD FIELDS %s' , e)
            raise e

    ##############this is for cbr filees only, adding region it################################
    def maintainlog_cbr ( self , file_type , bank_code , actual_upload_time , record_status , upload_time , Pid ,
                          username , reference_id , region ) :
        logger = logging.getLogger(__name__)
        logger.info ( "IN maintainlog_cbr METHOD OF common_util." )
        metadata = {
            "data_for_type" : file_type ,
            "bank_code" : bank_code ,
            "datafor_date_time" : actual_upload_time ,
            "record_status" : record_status ,
            "date_created" : upload_time ,
            "project_id" : Pid ,
            "created_reference_id" : reference_id ,
            "created_by" : username ,
            "region" : region
        }
        try :
            logger.info("printing metadata to add data into data_update_log: %s ", metadata )
            data_update = data_update_log ( **metadata )
            data_update.save ( )
            logger.info("Save Successfully")
            return data_update.id
        except Exception as e :
            logger.error ( 'EXCEPTION IN maintainlog_cbr METHOD OF common_util %s ' ,e)
            raise e

    def validate_holiday_InputReq(self,request_data):
        params = masterconf.holiday_InputReq
        print ("Input params receiving for Holiday Master: ",request_data)
        print("params from validate_holiday_InputReq :",params)
        status_flag = False
        for param in params:
            if param not in request_data.keys():
                status_flag = False
                break
            else:
                status_flag = True
        print("Returned status flag from validate_holiday_InputReq : ",status_flag)
        return status_flag

    def validate_status_req_holiday(self,request_data): #Name Changed as per naming convention

        ## This method is used to validate incoming request for master and Reference data for Approval and Rejection API

        logger = self.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("IN validate_status_req_holiday METHOD OF common_util.")
        print("IN validate_status_req_holiday METHOD OF common_util.")
        params = masterconf.status_app_rej_holiday  #Name Changed as per naming convention
        status_flag = False
        for param in params:
            if param not in request_data.keys():
                status_flag = False
                break
            else:
                status_flag = True
        return status_flag


    def validate_pre_qualify_input_req ( self , request_data ) :  # Name Changed as per naming convention

        ## This method is used to validate incoming request for master and Reference data for Approval and Rejection API
        params = masterconf.pre_qualify_input_param  # Name Changed as per naming convention
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        return status_flag


    def change_status_pre_qualify ( self , request, string_id ) :  # Name Changed as per naming convention

        ## This method is used to change the status of records to Active or Reject for Master, Rejection, Indent Menus
        request_status = request.data['status']  # Active, Rejected
        menu = request.data['menu']
        reference_id = request.data['created_reference_id']
        username = request.userinfo['username'].username
        upload_time = datetime.datetime.fromtimestamp ( time.time ( ) ).strftime ( '%Y-%m-%d %H:%M:%S' )
        table_key = str ( menu ).upper ( ) + "_TABLE"
        table = db_queries_properties.all_table_dict[table_key]  # Name Changed as per naming convention
        id_str = string_id
        # id_str = ','.join(ids_list)
        ## Calling uspSetStatus_masterdata SP to update records to Active or Rejected
        sql_str = "exec uspSetStatus_master_ref " + "'" + table + "', " + "'" + request_status + "', " + "'" + upload_time + "'," + "'" + username + "','" + id_str + "'" + ",'" + reference_id + "'"
        print("sql_str from uspSetStatus_master_ref : {}".format ( sql_str ))
        try :
            nocount = """ SET NOCOUNT ON; """
            cur = connection.cursor ( )
            cur.execute ( nocount + sql_str )
            code = cur.fetchone ( )
            print ("code generated after changing the status from change_status_master_ref: " , code[0])
            return code[0]
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'Exception in change_status_master_ref' , exc_info=True )
            raise e


###############Changing the status of data coming from AMS API##########################################
    def change_status_ams_api ( self , referenceid, username,  table, request_status ) :  # Name Changed as per naming convention

        ## This method is used to change the status of records to Active or Reject for AMS API data only
        upload_time = datetime.datetime.fromtimestamp ( time.time ( ) ).strftime ( '%Y-%m-%d %H:%M:%S' )
        id_str = '0'
        ## Calling uspSetStatus_masterdata SP to update records to Active or Rejected
        sql_str = "exec uspSetStatus_master_ref " + "'" + table + "', " + "'" + request_status + "', " + "'" + upload_time + "'," + "'" + username + "','" + id_str + "'" + ",'" + referenceid + "'"
        print("sql_str from uspSetStatus_master_ref : {}".format ( sql_str ))
        try :
            nocount = """ SET NOCOUNT ON; """
            cur = connection.cursor ( )
            cur.execute ( nocount + sql_str )
            code = cur.fetchone ( )
            print ("code generated after changing the status from change_status_ams_api: " , code[0])
            return code[0]
        except Exception as e :
            logger.setLevel ( logging.ERROR )
            logger.error ( 'Exception in change_status_ams_api' , exc_info=True )
            raise e

    def validate_master_req ( self , request_post ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_master_req METHOD OF common_util." )
        params = masterconf.status_master_param
        status_flag = False
        print("request_post from validate_master_req : " , request_post)
        for param in params :
            print("request_post from validate_mstr_ref_req : " , request_post)
            if param not in request_post.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        print("status_flag from validate_master_req : " , status_flag)
        return status_flag


###################validating input request for bank_limitdays################################

    def validate_bank_or_feeder_limitdays_InputReq ( self , request_data ) :
        logger = self.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "IN validate_bank_limitdays_InputReq METHOD OF common_util." )
        params = masterconf.bank_limitdays_or_feeder__input_param
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        print("status_flag from validate_bank_limitdays_InputReq : " , status_flag)
        return status_flag


###################validating input request for feeder vaulting pre config################################

    def validate_feeder_vaulting_pre_config_InputReq ( self , request ) :
        params = masterconf.feeder_vaulting_pre_config_input_param
        status_flag = False
        if 'file' not in request.FILES.keys ( ) :
            status_flag = False
        else:
            for param in params :
                if param not in request.POST.keys ( ) :
                    status_flag = False
                    break
                else :
                    status_flag = True
        print("status_flag from validate_feeder_vaulting_pre_config_InputReq : " , status_flag)
        return status_flag


    def validate_createUser_InputReq ( self , request_data ) :
        params = masterconf.createUser_input_param
        status_flag = False
        for param in params :
            if param not in request_data.keys ( ) :
                status_flag = False
                break
            else :
                status_flag = True
        print("status_flag from validate_createUser_InputReq : " , status_flag)
        return status_flag

    ### creating dictionary to create diversion_status ###
    def validate_diversion_status_InsertReq(self, request_data):
        logger = logging.getLogger(__name__)
        params = masterconf.diversion_status
        logger.info("Input params receiving for Diversion Status: %s", request_data)
        logger.info("params from validate_diversion_status_InsertReq: %s", params)
        data_dict = {}
        for param in params:
            if param not in request_data:
                data_dict[param] = ''
            else:
                data_dict[param] = request_data[param]
        logger.info("Returned data from validate_diversion_status_InsertReq: %s", data_dict)
        return data_dict

    #############checking if input request is correct for Diversion Status ###########################
    def validate_diversionStatus_ParamsReq(self, request_data):
        logger = logging.getLogger(__name__)
        params = masterconf.diversion_status
        logger.info("Input params receiving for Diversion Status: %s", request_data)
        logger.info("params from validate_diversionStatus_ParamsReq: %s", params)
        status_flag = False
        for param in params:
            if param not in request_data.keys():
                status_flag = False
                break
            else:
                status_flag = True
        logger.info("Returned status flag from validate_diversionStatus_ParamsReq: %s", status_flag)
        return status_flag

    def calculate_morning_balance ( self , indent_date, project_id , bank_code , reference_id, username ) :
        logger = logging.getLogger(__name__)
        logger.info ( "IN calculate_morning_balance METHOD OF common_util." )
        try :
            sql_str = "SET NOCOUNT ON exec usp_morningBalance " + "'" + indent_date + "', " + "'" + project_id + "', " + "'" + bank_code + "'," + "'" + reference_id  + "'," + "'" + username + "'"
            logger.info("sql_str from calculate_morning_balance: %s" % ( sql_str ))
            cur = connection.cursor ( )
            cur.execute ( sql_str )
            code = cur.fetchone ( )
            logger.info("code is ::: %s" , code[0])
            return code[0]
        except Exception as e :
            logger.error ( 'EXCEPTION IN calculate_morning_balance METHOD OF common_util.' , exc_info=True )
            logger.error(e)
            raise e

    def calculate_default_dispense ( self , indent_date, bank_code , reference_id, username ) :
        logger = logging.getLogger(__name__)
        logger.info ( "IN calculate_default_dispense METHOD OF common_util." )
        try :
            sql_str = "SET NOCOUNT ON exec usp_default_dispense_bank_wise " + "'" + indent_date + "', " + "'" + bank_code + "', " + "'" + reference_id + "'," + "'" + username + "'"
            logger.info("sql_str from calculate_default_dispense: %s" % ( sql_str ))
            cur = connection.cursor ( )
            cur.execute ( sql_str )
            code = cur.fetchone ( )
            logger.info("code is ::: %s" , code[0])
            return code[0]
        except Exception as e :
            logger.error ( 'EXCEPTION IN calculate_default_dispense METHOD OF common_util.' , exc_info=True )
            logger.error(e)
            raise e

    def calculate_dispense ( self , datafor_date_time, c3r_date, cbr_t_minus_1, cbr_t_minus_2, bank_code, project_id, username, reference_id ) :
        logger = logging.getLogger(__name__)
        logger.info ( "IN calculate_default_dispense METHOD OF common_util." )
        try :
            sql_str = "SET NOCOUNT ON exec usp_calculate_dispense " + "'" + datafor_date_time + "', " + "'" + cbr_t_minus_1 + "', " + "'" + cbr_t_minus_2 + "'," + "'" + c3r_date + "', " + "'" + reference_id + "', " + "'" + username + "', " + "'" + bank_code + "', " + "'" + project_id + "'"
            logger.info("sql_str from calculate_dispense: %s" % ( sql_str ))
            cur = connection.cursor ( )
            cur.execute ( sql_str )
            code = cur.fetchone ( )
            logger.info("code is ::: %s" , code[0])
            return code[0]
        except Exception as e :
            logger.error ( 'EXCEPTION IN calculate_dispense METHOD OF common_util.' , exc_info=True )
            logger.error(e)
            raise e
			
			
    #############checking if input request is correct for vaulting balance ###########################
    def validate_inputVB_request(self, request_data):
        logger = logging.getLogger(__name__)
        params = masterconf.VB_input_param
        logger.info("Input params receiving for vaulting balance: %s", request_data)
        logger.info("params from validate_inputVB_request: %s", params)
        status_flag = False
        for param in params:
            if param not in request_data.keys():
                status_flag = False
                break
            else:
                status_flag = True
        logger.info("Returned status flag from validate_diversionStatus_ParamsReq: %s", status_flag)
        return status_flag