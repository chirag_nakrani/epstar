import os
from EPSTAR.settings import BASE_DIR,Dev,Local,Uat,Sit,Uat_Sandbox


from Common import masterconf

if eval(os.environ.get('DJANGO_CONFIGURATION')).is_sftp:
    path1 = os.path.join(eval(os.environ.get('DJANGO_CONFIGURATION')).sfpt_file_path, 'FormatFolder/')
else:
    path1 = os.path.join(BASE_DIR, 'FileFolder/FormatFolder/')
path1 = path1.replace ( "\\" , "/" )

###################### Table Names for Different file types and Bank types###############################
table_dict = {
    "CBR_BOMH_TABLE" : "cash_balance_file_bomh" ,
    "CBR_BOB_TABLE" : "cash_balance_file_bob" ,
    "CBR_ALB_TABLE" : "cash_balance_file_alb" ,
    "CBR_BOB_TABLE" : "cash_balance_file_bob" ,
    "CBR_BOI_TABLE" : "cash_balance_file_boi" ,
    "CBR_CAB_TABLE" : "cash_balance_file_cab" ,
    "CBR_CBI_TABLE" : "cash_balance_file_cbi" ,
    "CBR_CORP_TABLE" : "cash_balance_file_corp" ,
    "CBR_DENA_TABLE" : "cash_balance_file_dena" ,
    "CBR_IDBI_TABLE" : "cash_balance_file_idbi" ,
    "CBR_LVB_TABLE" : "cash_balance_file_lvb" ,
    "CBR_PSB_TABLE" : "cash_balance_file_psb" ,
    "CBR_RSBL_TABLE" : "cash_balance_file_rsbl" ,
    "CBR_UCO_TABLE" : "cash_balance_file_uco" ,
    "CBR_VJB_TABLE" : "cash_balance_file_vjb" ,
    "CBR_IOB_TABLE" : "cash_balance_file_iob" ,
    "CBR_UBI_TABLE" : "cash_balance_file_ubi" ,
    "CBR_SBI_TABLE" : "cash_balance_file_sbi_staging" ,
    "DISPENSE_SBI_TABLE" : "cash_dispense_file_sbi_staging" ,
    "DISPENSE_CAB_TABLE" : "cash_dispense_file_cab" ,
    "DISPENSE_RSBL_TABLE" : "cash_dispense_file_rsbl" ,
    "DISPENSE_BOMH_TABLE" : "cash_dispense_file_bomh" ,
    "DISPENSE_UBI_TABLE" : "cash_dispense_file_ubi" ,
    "VCB__TABLE" : "vault_cash_balance" ,
    "C3R_VMIS_ALL_TABLE" : "c3r_VMIS" ,
    "C3R_CMIS_ALL_TABLE" : "c3r_CMIS" ,
    "Daily_Loading_Report_ALL_TABLE" : "Daily_Loading_Report" ,
    "CBR_ALL_TABLE" : "cash_balance_register" ,
    "DISPENSE_ALL_TABLE" : "cash_dispense_register" ,
    "DAILY_LOADING_REPORT_ALL_TABLE" : "Daily_Loading_Report" ,
    "IMS__TABLE" : "ims_master" ,
    "VCB_ALL_TABLE" : "vault_cash_balance" ,
	"CONFIRMLOADING__TABLE" : "loading_status_confirmation" ,
}

##############################File Delimeter#################################################33
file_delimeter = '|';

#############################Format Files Path####################################

format_file_path = {
    "CBR_BOMH_FORMAT_FILE" : path1 + "cash_balance_file_bomh.xml" ,
    "CBR_ALB_FORMAT_FILE" : path1 + "cash_balance_file_alb.xml" ,
    "CBR_BOB_FORMAT_FILE" : path1 + "cash_balance_file_bob.xml" ,
    "CBR_BOI_FORMAT_FILE" : path1 + "cash_balance_file_boi.xml" ,
    "CBR_CAB_FORMAT_FILE" : path1 + "cash_balance_file_cab.xml" ,
    "CBR_CBI_FORMAT_FILE" : path1 + "cash_balance_file_cbi.xml" ,
    "CBR_CORP_FORMAT_FILE" : path1 + "cash_balance_file_corp.xml" ,
    "CBR_DENA_FORMAT_FILE" : path1 + "cash_balance_file_dena.xml" ,
    "CBR_IDBI_FORMAT_FILE" : path1 + "cash_balance_file_idbi.xml" ,
    "CBR_LVB_FORMAT_FILE" : path1 + "cash_balance_file_lvb.xml" ,
    "CBR_PSB_FORMAT_FILE" : path1 + "cash_balance_file_psb.xml" ,
    "CBR_RSBL_FORMAT_FILE" : path1 + "cash_balance_file_rsbl.xml" ,
    "CBR_UCO_FORMAT_FILE" : path1 + "cash_balance_file_uco.xml" ,
    "CBR_VJB_FORMAT_FILE" : path1 + "cash_balance_file_vjb.xml" ,
    "CBR_IOB_FORMAT_FILE" : path1 + "cash_balance_file_iob.xml" ,
    "CBR_UBI_FORMAT_FILE" : path1 + "cash_balance_file_ubi.xml" ,
    "CBR_SBI_FORMAT_FILE" : path1 + "cash_balance_file_sbi.xml" ,
    "DISPENSE_BOMH_FORMAT_FILE" : path1 + 'cash_dispense_file_bomh.xml' ,
    "DISPENSE_CAB_FORMAT_FILE" : path1 + 'cash_dispense_file_cab.xml' ,
    "DISPENSE_SBI_FORMAT_FILE" : path1 + 'cash_dispense_file_sbi.xml' ,
    "DISPENSE_RSBL_FORMAT_FILE" : path1 + 'cash_dispense_file_rsbl.xml' ,
    "DISPENSE_UBI_FORMAT_FILE" : path1 + 'cash_dispense_file_ubi.xml' ,
    "VCB__FORMAT_FILE" : path1 + "vault_cash_balance.xml" ,
    "C3R_VMIS_ALL_FORMAT_FILE" : path1 + "c3r_VMIS.xml" ,
    "C3R_CMIS_ALL_FORMAT_FILE" : path1 + "c3r_CMIS.xml" ,
    "Daily_Loading_Report_ALL_FORMAT_FILE" : path1 + "Daily_Loading_Report.xml" ,
    "IMS__FORMAT_FILE" : path1 + "ims_master.xml" ,
    "VCB_ALL_FORMAT_FILE" : path1 + "vault_cash_balance.xml" ,
}

fields_dict = {
    "CBR_BOMH" : "(term_id,acceptorname,location,tot_cash,cassette1,cassette2,cassette3,cassette4,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_ALB" : "(group_name,termid,location,currbalcass1,ccurrbalcass2,currbalcass3,currbalcass4,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_BOB" : "(atmid,location,state,atmstatus,msvendor,currentstatus,regionname,zone,address,hop1bill,hop2bill,hop3bill,hop4bill,hop1begcash,hop2begcash,hop3begcash,hop4begcash,hop1endcash,hop2endcash,hop3endcash,hop4endcash,hop1cashdeposit,hop2cashdeposit,hop3cashdeposit,hop4cashdeposit,begcash,endcash,cashout,cashincr,timestamp,lastwithdrawaltime,lastsupervisorytime,lastdeposittranstime,lastadmintxntime,lastreversaltime,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_BOI" : "(institutionid,termid,location,cash_start1,cash_start2,cash_start3,cash_start4,cash_inc1,cash_inc2,cash_inc3,cash_inc4,cash_dec1,cash_dec2,cash_dec3,cash_dec4,cash_out1,cash_out2,cash_out3,cash_out4,cash_currbal1,cash_currbal2,cash_currbal3,cash_currbal4,termid1,txn_date,txn_time,respcode,pcode,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_CAB" : "(atm_id,termloc,termcity,regn_id,network_link_status,atm_status,out_time,err_1,err_2,err_3,err_4,err_5,err_6,err_7,err_8, end_cash, last_tran, last_up, hop1cash, hop1bill, hop2cash, hop2bill, hop3cash, hop3bill, hop4cash, hop4bill, hop5cash, timestamp,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_CBI" : "(sst,atm_id,cas_200,cas_100,cas_500,cas_2000,remaining,location,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_CORP" : "(atm_id,cash_100s,cash_200s,cash_500s,cash_2000s,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_DENA" : "(atm_id,stat,atm_stat,fiid,err_2,err_3,err_4,err_5,err_6,err_7,err_8,cashchng,cashout,cashrepl,cashsply,end_cash,hop4cash,hop4bill,hop5cash,hop5bill,amt_chk,num_chk,amt_dep,num_dep,intvdown,flt_cnt,hop1cash,hop1bill,hop2cash,hop2bill,hop3cash,hop3bill,linestat,termloc,termcity,term_st,termst_x,ownr,row_cnt,src_name,spvr_on,brch_id,regn_id,timeofst,atm_type,vendor_name,timestamp_data,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_IDBI" : "(TERMID,TiaDATETIME,Bill1,Bill2,Bill3,Bill4,STARTBALCASS1,STARTBALCASS2,STARTBALCASS3,STARTBALCASS4,CASHINCCASS1,CASHINCCASS2,CASHINCCASS3,CASHINCCASS4,CASHOUTCASS1,CASHOUTCASS2,CASHOUTCASS3,CASHOUTCASS4,CURRBALCASS1,CURRBALCASS2,CURRBALCASS3,CURRBALCASS4,LASTRESETDATE,LASTRESETTIME,LASTTXNDATE,LASTTXNTIME,LASTTXNSERIAL,LASTTXNSTATUS,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_LVB" : "(brn,atm_id,atm_location,atm_type,availability_100,denomination_100,availability_500,denomination_500,availability_2000,denomination_2000,total_amt,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_PSB" : "(atm_id,cash_out,endcash,hop1cash,hop1bill,hop2cash,hop2bill,hop3cash,hop3bill,hop4cash,hop4bill,ownr,state,timestamp,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_RSBL" : "(cfb_date_time,atm_id,i_denom,i_begin_cash,i_dispense,i_remining,ii_denom,ii_begin_cash,ii_dispense,ii_remining,iii_denom,iii_begin_cash,iii_dispense,iii_remining,iv_denom,iv_begin_cash,iv_dispense,iv_remining,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_VJB" : "(atm_id, CASH_START1, CASH_START2, CASH_START3, CASH_START4, CASH_INC1, CASH_INC2, CASH_INC3, CASH_INC4, CASH_DEC1, CASH_DEC2, CASH_DEC3, CASH_DEC4, CASH_OUT1, CASH_OUT2, CASH_OUT3, CASH_OUT4, CASH_CURRBAL1, CASH_CURRBAL2, CASH_CURRBAL3, CASH_CURRBAL4,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_UCO" : "(atm_id, Cassete1_Start, Cassete1_Dispensed, Cassete1_Remaining, Cassete2_Start, Cassete2_Dispensed, Cassete2_Remaining, Cassete3_Start, Cassete3_Dispensed, Cassete3_Remaining, Cassete4_Start, Cassete4_Dispensed, Cassete4_Remaining, Total_Start, Toatal_Dispensed, Total_Remaining, Vendor_Name, region_file, MTLOC, MTADR,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_IOB" : "(atm_id,remaining_1,remaining_2,remaining_3,remaining_4,total_of_remaining,opening_1,opening_2,opening_3,opening_4,total_of_opening,dispense_1,dispense_2,dispense_3,dispense_4,total_of_dispense,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_UBI" : "(atm_id,type_01,type_02,type_03,type_04,file_date_time,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "CBR_SBI" : "(terminal_location,city,cash_dispensed_hopper_1,bills_hopr1,cash_dispensed_hopper_2,bills_hopr2,cash_dispensed_hopper_3,bills_hopr3,cash_dispensed_hopper_4,bills_hopr4,cash_dispensed_hopper_5,bills_hopr5,cash_dispensed_hopper_6,bills_hopr6,atm_id,total_remaining_cash_def_curr,remote_address,cash_increment_hopper_1,cash_increment_hopper_2,cash_increment_hopper_3,cash_increment_hopper_4,cash_increment_hopper_5,cash_increment_hopper_6,circle,sitetype,msvendor,lastwithdrawaltime,switch,district,module,currentstatus,branchmanagerphone,channelmanagercontact,jointcustodianphone,network,populationgroup,regionname,statename,jointcustodianname1,jointcustodianphone2,last_deposit_txn_time,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "DISPENSE_BOMH" : "(atm_id,location,amount_dispensed,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "DISPENSE_SBI" : "(atm_id,cash_withdrawal,opening_cash,total_cash_replenished,cer,msvendor,circle_name,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "DISPENSE_RSBL" : "(TRL_DATE_LOCAL,atm_id,amount_dispensed,count_TRL_CARD_ACPT_TERMINAL_IDENT,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "DISPENSE_CAB" : "(atm_id, fiid, term_city, term_location, unapproved_nonfin, unapproved_fin, approved_nonfin, approved_fin, approved_declined_tottran, amount_dispensed, file_date,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "DISPENSE_UBI" : "(type_01,type_02,type_03,type_04,atm_id,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time, region)" ,
    "VCB_" : "(bank_name,atm_id,feeder_branch_name,cra_name,agent,vault_balance_100,vault_balance_200,vault_balance_500,vault_balance_2000,total_vault_balance,dependency,remark,date,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time)" ,
    "C3R_VMIS_ALL" : "(sr_no,date,bank,cra_name,feeder_branch_name,vault_balance_100,vault_balance_200,vault_balance_500,vault_balance_1000,vault_balance_2000,total_vault_balance,withdrawalfrom_bank_100,withdrawalfrom_bank_200,withdrawalfrom_bank_500,withdrawalfrom_bank_1000,withdrawalfrom_bank_2000,total_withdrawalfrom_bank,replenishto_atm_100,replenishto_atm_200,replenishto_atm_500,replenishto_atm_1000,replenishto_atm_2000,total_replenishto_atm,cashreturn_atm_100,cashreturn_atm_200,cashreturn_atm_500,cashreturn_atm_1000,cashreturn_atm_2000,total_cashreturn_atm,unfit_currency_100,unfit_currency_200,unfit_currency_500,unfit_currency_1000,unfit_currency_2000,total_unfit_currency,atm_unfit_currency_return_100,atm_unfit_currency_return_200,atm_unfit_currency_return_500,atm_unfit_currency_return_1000,atm_unfit_currency_return_2000,total_atm_unfit_currency_return,closing_balance_100,closing_balance_200,closing_balance_500,closing_balance_1000,closing_balance_2000,total_closing_balance,fit_currency_100,fit_currency_200,fit_currency_500,fit_currency_1000,fit_currency_2000,total_fit_currency,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time)" ,
    "C3R_CMIS_ALL" : "(sr_no,bank,feeder_branch,atm_id,location,date,eod_loading_time,status_of_loading,last_transaction_no,cra_name,indentno,bankrefno,atmcount_op__bal_100,atmcount_op__bal_200,atmcount_op__bal_500,atmcount_op__bal_1000,atmcount_op__bal_2000,atmcount_op_bal_total,atmcount_disp_100,atmcount_disp_200,atmcount_disp_500,atmcount_disp_1000,atmcount_disp_2000,atmcount_disp_total,atmcount_divert_count_100,atmcount_divert_count_200,atmcount_divert_count_500,atmcount_divert_count_1000,atmcount_divert_count_2000,atmcount_divert_count_total,atmcount_remain_counter_100,atmcount_remain_counter_200,atmcount_remain_counter_500,atmcount_remain_counter_1000,atmcount_remain_counter_2000,atmcount_remain_counter_total,atmphysical_cash__from_cassettes_100,atmphysical_cash__from_cassettes_200,atmphysical_cash__from_cassettes_500,atmphysical_cash__from_cassettes_1000,atmphysical_cash__from_cassettes_2000,atmphysical_cash__from_cassettes_total,atmphysical_purge_bin_100,atmphysical_purge_bin_200,atmphysical_purge_bin_500,atmphysical_purge_bin_1000,atmphysical_purge_bin_2000,atmphysical_purge_bin_total,atmphysical_total_remainingcash_100,atmphysical_total_remainingcash_200,atmphysical_total_remainingcash_500,atmphysical_total_remainingcash_1000,atmphysical_total_remainingcash_2000,atmphysical_total_remainingcash_total,atm_cashreturns_100,atm_cashreturns_200,atm_cashreturns_500,atm_cashreturns_1000,atm_cashreturns_2000,atm_cashreturns_total,atm_cashreturns_sealno_100,atm_cashreturns_sealno_200,atm_cashreturns_sealno_500,atm_cashreturns_sealno_1000,atm_cashreturns_sealno_2000,atm_return_cashfrompurge_100,atm_return_cashfrompurge_200,atm_return_cashfrompurge_500,atm_return_cashfrompurge_1000,atm_return_cashfrompurge_2000,atm_return_cashfrompurge_total,atm_repl_100,atm_repl_200,atm_repl_500,atm_repl_1000,atm_repl_2000,atm_repl_total,atm_repl_seal_100,atm_repl_seal_200,atm_repl_seal_500,atm_repl_seal_1000,atm_repl_seal_2000,atm_closingcashreturn_100,atm_closingcashreturn_200,atm_closingcashreturn_500,atm_closingcashreturn_1000,atm_closingcashreturn_2000,atm_closingcashreturn_total,atm_closingbal_100,atm_closingbal_200,atm_closingbal_500,atm_closingbal_1000,atm_closingbal_2000,atm_closingbal_total,sw_count_openbal_100,sw_count_openbal_200,sw_count_openbal_500,sw_count_openbal_1000,sw_count_openbal_2000,sw_count_openbal_total,sw_disp_100,sw_disp_200,sw_disp_500,sw_disp_1000,sw_disp_2000,sw_disp_total,sw_loading_100,sw_loading_200,sw_loading_500,sw_loading_1000,sw_loading_2000,sw_loading_total,sw_adminincrease_100,sw_adminincrease_200,sw_adminincrease_500,sw_adminincrease_1000,sw_adminincrease_2000,sw_adminincrease_total,sw_admindecrease_100,sw_admindecrease_200,sw_admindecrease_500,sw_admindecrease_1000,sw_admindecrease_2000,sw_admindecrease_total,sw_closing_100,sw_closing_200,sw_closing_500,sw_closing_1000,sw_closing_2000,sw_closing_total,phys_diff_overage_100,phys_diff_overage_200,phys_diff_overage_500,phys_diff_overage_1000,phys_diff_overage_2000,phys_diff_overage_total,phys_diff_shortage_100,phys_diff_shortage_200,phys_diff_shortage_500,phys_diff_shortage_1000,phys_diff_shortage_2000,phys_diff_shortage_total,remarks,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time)" ,
    "Daily_Loading_Report_ALL" : "(Date,Sr_No,ATM_ID,Location,Feeder_Branch,CRA,District,State,bank_name,Planning_100,Planning_200,Planning_500,Planning_2000,Planning_Total,Revised_Planning_100,Revised_Planning_200,Revised_Planning_500,Revised_Planning_2000,Revised_Planning_Total,Actual_Loading_100,Actual_Loading_200,Actual_Loading_500,Actual_Loading_2000,Actual_Loading_Total,Remarks,Diverted_ATM_ID,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time)" ,
    "IMS_" : "(ticketno,atmid,bankname,ticket_generation_type,site_details,fault,fault_description,call_status,ticket_status,message_date_time,open_date,opentime,duration,remarks,latestremarks,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time)"
}

model_dict_columns = {
    "CBR_BOMH" : ['term_id' , 'acceptorname' , 'location' , 'tot_cash' , 'cassette1' , 'cassette2' , 'cassette3' ,
                  'cassette4' , 'project_id' , 'record_status' , 'created_on' , 'created_by' , 'created_reference_id' ,
                  'datafor_date_time'] ,
    "CBR_ALB" : "group_name,termid,location,currbalcass1,ccurrbalcass2,currbalcass3,currbalcass4,project_id,record_status,created_on,created_by,created_reference_id,datafor_date_time" ,
}

## Table Dictionary for Master and Reference Menus
all_table_dict = {  # Name Changed as per naming convention
    "ATMCNFGLIMIT_TABLE" : "atm_config_limits" ,
    "FEEDER_TABLE" : "feeder_branch_master" ,
    "CYPHERCODE_TABLE" : "Cypher_Code" ,
    "CRAFEASIBILITY_TABLE" : "cra_feasibility" ,
    "AMS_ATM_TABLE" : "atm_master" ,
    "BRANDBILLCAPACITY_TABLE" : "Brand_Bill_Capacity" ,
    "CRAEMPANELED_TABLE" : "cra_empaneled" ,
    "CRAVAULTMASTER_TABLE" : "cra_vault_master" ,
    "DEFAULTLOADING_TABLE" : "default_loading" ,
    "INDENTREVISION_TABLE" : "indent_revision_request" ,
    "CRAESCALATIONMATRIX_TABLE" : "cra_escalation_emails" ,
    "BANKESCALATIONMATRIX_TABLE" : "Bank_Escalation_Emails" ,
    "EPSESCALATIONMATRIX_TABLE" : "eps_escalation_emails" ,
    "MAILMASTER_TABLE" : "Mail_Master" ,
    "SIGNATURE_TABLE" : "Auth_Signatories_Signatures" ,
    "EOD_ACTIVITY_TABLE" : "indent_eod_pre_activity" ,
    "INDENT_PRE_QUALIFY_ATM_TABLE" : "indent_pre_qualify_atm" ,
    "LIMITDAYS_TABLE" : "limitdays" ,
    "FEEDER_VAULTING_PRE_CONFIG_TABLE" : "feeder_vaulting_pre_config" ,
    "CYPHERCODE_TABLE" : "cypher_code" ,
    "CASHPREAVAILABILITY_TABLE" : "cash_pre_availability" ,
    "FEEDER_DENOMINATION_TABLE" : "feeder_denomination_priority" ,
    "FDR_CASSTT_MAX_CAP_PER_TABLE" : "feeder_cassette_max_capacity_percentage" ,
    "FEEDER_DENO_PRE_AVAIL_TABLE" : "feeder_denomination_pre_availability" ,
	"HOLIDAYMASTER_TABLE":["holiday_date","holiday_list","holiday_states"],
	"AMS_API_TABLE" : "atm_master",
    "BLD_TABLE" : "bank_limit_days",
    "FLD_TABLE" : "feeder_limit_days",
    "CASHPREAVAILABILITY_TABLE" : "cash_pre_availability",
	"VB_TABLE" : "vaulting_of_balance",
    "ATM_LIST_TABLE" : "atm_revision_cash_availability",
    "ATM_REVISION_CASH_AVAILABILITY_TABLE" : "atm_revision_cash_availability",
    "GENERAL_LEDGER_ACCOUNT_NUMBER_TABLE" : "gl_account_number"

}
master_field_dict = {
    "ATMCNFGLIMIT" : ['site_code' , 'old_atm_id' , 'current_atm_id' , 'bank_name' , 'whether_critical_atm' ,
                      'cassette_50_count' , 'cassette_100_count' , 'cassette_200_count' , 'cassette_500_count' ,
                      'cassette_2000_count' , 'bank_cash_limit' , 'base_limit' , 's_g_locker_no' , 'type_of_switch' ,
                      'project_id' , 'record_status' , 'created_on' , 'created_by' , 'created_reference_id'] ,
    "FEEDER" : ['bank_code' , 'project_id' , 'region_code' , 'sol_id' , 'feeder_branch' , 'district' , 'circle' ,
                'feeder_linked_count' , 'contact_details' , 'is_vaulting_enabled' , 'email_id' ,
                'alternate_cash_balance' , 'is_currency_chest' , 'record_status' , 'created_on' , 'created_by' ,
                'created_reference_id'] ,
    "CYPHERCODE" : ['category' , 'value' , 'cypher_code' , 'is_applicable_to_all_projects' , 'project_id' ,
                    'is_applicable_to_all_banks' , 'bank_code' , 'record_status' , 'created_on' , 'created_by' ,
                    'created_reference_id'] ,
    "CRAFEASIBILITY" : ['site_code' , 'atm_id' , 'project_id' , 'bank_code' , 'feeder_branch_code' , 'new_cra' ,
                        'feasibility_or_loading_frequency' , 'distance_from_hub_to_site' ,
                        'distance_from_nodal_branch' , 'distance_from_atm_site_to_nodal_branch' , 'flm_tat' ,
                        'cra_spoc' , 'cra_spoc_contact_no' , 'br_document_status' , 'cash_van' , 'gunman' ,
                        'lc_nearest_hub_or_spoke_or_branch_from_site' , 'accessibility' , 'first_call_dispatch_time' ,
                        'last_call_dispatch_time' , 'reason_for_limited_access' , 'vaulting' ,
                        'feasibility_received_date' , 'feasibility_send_date' , 'br_request_date' , 'br_send_date' ,
                        'is_feasible_mon' , 'is_feasible_tue' , 'is_feasible_wed' , 'is_feasible_thu' ,
                        'is_feasible_fri' , 'is_feasible_sat' , 'record_status' , 'created_on' , 'created_by' ,
                        'created_reference_id'] ,
    "AMS_ATM" : ['site_code' , 'old_atm_id' , 'atm_id' , 'atm_band' , 'ej_docket_no' , 'grouting_status' , 'brand' ,
                 'vsat_id' , 'vendor_name' , 'serial_no' , 'current_deployment_status' , 'tech_live_date' ,
                 'cash_live_date' , 'insurance_limits' , 'project_id' , 'bank_name' , 'bank_code' ,
                 'circle_zone_region' , 'site_code_2015_16' , 'site_code_2016_17' , 'state' , 'district' , 'city' ,
                 'site_address_line_1' , 'site_address_line_2' , 'pincode' , 'site_category' , 'site_type' ,
                 'installation_type' , 'site_status' , 'channel_manager_name' , 'channel_manager_contact_no' ,
                 'location_name' , 'atm_cash_removal_date' , 'atm_ip' , 'switch_ip' ,
                 'external_camera_installation_status' , 'atm_owner' , 'stabilizer_status' , 'ups_capacity' ,
                 'no_of_batteries' , 'ups_battery_backup' , 'load_shedding_status' , 'solar_dg' , 'date_created' ,
                 'date_updated' , 'date_deleted' , 'is_deleted' , 'record_status' , 'created_on' , 'created_by' ,
                 'created_reference_id'] ,
}

##############It is required when user inputs incomplete file. At that time, remaining columns are picked from this dictionary.#################
master_attr_dict = {
    "ATMCNFGLIMIT" : ['SITE CODE' , 'OLD ATM ID' , 'CURRENT ATM ID' , 'Project' , 'Bank code' , 'Insurance limit' ,
                      'WHETHER CRITICAL ATM' , 'CASSETTE 50' , 'CASSETTE 100' , 'CASSETTE 200' , 'CASSETTE 500' ,
                      'CASSETTE 2000' , 'Total Cassette Count' , 'BANK LIMIT' , 'BASE LIMIT' , 'S&G LOCK SR. NO.' ,
                      'TYPE OF SWITCH'] ,
    "CRAFEASIBILITY" : ['site_code' , 'atm_id' , 'project_id' , 'bank_code' , 'feeder_branch_code' , 'new_cra' ,
                        'feasibility/loading_frequency' , 'distance_fom_hub_to_site' ,
                        'distance_from_atm_site_to_nodal_branch' , 'distance_from_nodal_branch' , 'flm_tat' ,
                        'cra_spoc' , 'cra_spoc_contact_no' , 'br_document_status' , 'cash_van' , 'gunman' ,
                        'lc_nearest_hub/spoke/branch_from_site' , 'accessibility' , 'first_call_dispatch_time' ,
                        'last_call_dispatch_time' , 'reason_for_limited_access' , 'vaulting' ,
                        'feasibility_received_date' , 'feasibility_send_date' , 'br_request_date' , 'br_send_date' ,
                        'is_feasible_mon' , 'is_feasible_tue' , 'is_feasible_wed' , 'is_feasible_thu' ,
                        'is_feasible_fri' , 'is_feasible_sat'] ,
    'CYPHERCODE' : ['category' , 'value' , 'cypher_code' , 'is_applicable_to_all_projects' , 'project_id' ,
                    'is_applicable_to_all_banks' , 'bank_code'] ,
    'FEEDER' : ['PROJECT' , 'BANK' , 'REGION' , 'SOL ID' , 'CASH FEEDER BRANCH' , 'DISTRICT' , 'CIRCLE' ,
                'No. OF ATMs LINKED TO THIS FEEDER' , 'EMAIL ID' , 'ALTERNATE CASH BRANCH' ,
                'WHETHER CURRENCY CHEST - YES/NO' , 'CONTACT NO.' , 'Whether Currency Chest - Yes/No'] ,
    'AMS_ATM' : ['site_code' , 'old_atm_id' , 'current_atm_id' , 'atm_band' , 'ej_docket_no' , 'grouting_status' ,
                 'brand' , 'vsat_id' , 'vendor_name' , 'serial_no' , 'current_deployment_status' , 'tech_live_date' ,
                 'cash_live_date' , 'insurance_limits' , 'project_id' , 'bank_name' , 'bank_code' ,
                 'circle_zone_region' , 'site_code_2015_16' , 'site_code_2016_17' , 'state' , 'district' , 'city' ,
                 'site_address_line_1' , 'site_address_line_2' , 'pincode' , 'site_category' , 'site_type' ,
                 'installation_type' , 'site_status' , 'channel_manager_name' , 'channel_manager_contact_no' ,
                 'location_name' , 'atm_cash_removal_date' , 'atm_ip' , 'switch_ip' ,
                 'external_camera_installation_status' , 'atm_owner' , 'stabilizer_status' , 'ups_capacity' ,
                 'no_of_batteries' , 'ups_battery_backup' , 'load_shedding_status' , 'solar_dg' , 'date_created' ,
                 'date_updated' , 'date_deleted' , 'is_deleted'] ,
    'ATM_LIST' : ['project id', 'bank code', 'atm id', 'site code', 'feeder branch code', 'for date'],
    'ATM_REVISION_CASH_AVAILABILITY' : ['project id', 'bank code', 'atm id', 'site code', 'feeder branch code', 'for date', 'denomination 100', 'denomination 200', 'denomination 500', 'denomination 2000', 'total'],
    'GENERAL_LEDGER_ACCOUNT_NUMBER' : ['bank code', 'atm id', 'site code', 'general ledger account number'],
    'INDENT_HOLIDAY' : ["project id","bank code" , "feeder branch" , "cra","holiday date","holiday name","is holiday","state","district","city","area","withdrawal type","is indent required"]

}

reference_table_dict = {
    "BRANDBILLCAPACITYTABLE" : "Brand_Bill_Capacity" ,
    "HOLIDAY_MASTER_TABLE" : "Holiday_Master" ,
    "CRAEMPANELED_TABLE" : "CRA_Emapaneled" ,
}
reference_field_dict = {
    "BRANDBILLCAPACITY" : ['brand_code' , 'description' , 'capacity_50' , 'capacity_100' , 'capacity_200' ,
                           'capacity_500' , 'capacity_2000' , 'project_id' , 'record_status' , 'created_on' ,
                           'created_by' , 'created_reference_id'] ,
    "HOLIDAY_MASTER" : ['id' , 'holiday_code' , 'holiday_name' , 'holiday_description' , 'holiday_type' , 'project_id' ,
                        'record_status' , 'created_on' , 'created_by' , 'created_reference_id'] ,
    "CRAVAULTMASTER" : ['vault_code' , 'cra_code' , 'state' , 'region' , 'location' , 'address' , 'vault_type' ,
                        'vault_status' , 'vaulting_allowed' , 'contact_person' , 'contact_details'] ,
    "CRAEMPANELED" : ['cra_code' , 'cra_name' , 'registered_office' , 'registration_number' , 'contact_details']
}

############ reference data attribute dictionary to validate data columns #################
##### dictionary contains list of specific reference type which has column names received in an excel file ################
reference_attr_dict = {
    "BRANDBILLCAPACITY" : ['Brand Code' , 'Description' , 'Capacity 50' , 'Capacity 100' , 'Capacity 200' ,
                           'Capacity 500' , 'Capacity 2000'] ,
    "HOLIDAY_MASTER" : ['id' , 'holiday_code' , 'holiday_name' , 'holiday_description' , 'holiday_type'] ,
    "CRAVAULTMASTER" : ['Vault Code' , 'Name of CRA' , 'State' , 'District' , 'Address of Vault' , 'Type of Vault' ,
                        'Status of Vault - Active / Inactive' , 'Contact Details'] ,
    "CRAEMPANELED" : ['CRA Code' , 'Name of CRA' , 'Registered Office' , 'Registration Number' , 'Contact Details'] ,
    "CRAESCALATIONMATRIX" : ['CRA' , 'Location' , 'Circle' , 'Activity' , 'Email ID' , 'Contact No.' , 'Email ID.1' ,
                             'Contact No..1' , 'Email ID.2' , 'Contact No..2' , 'Email ID.3' , 'Contact No..3' ,
                             'Email ID.4' , 'Contact No..4'] ,
    "EPSESCALATIONMATRIX" : ['CRA' , 'Bank' , 'Activity' , 'Email ID' , 'Contact No.' , 'Email ID.1' , 'Contact No..1' ,
                             'Email ID.2' , 'Contact No..2' , 'Email ID.3' , 'Contact No..3' , 'Email ID.4' ,
                             'Contact No..4'] ,
    "BANKESCALATIONMATRIX" : ['Project Code' , 'Bank' , 'Branch' , 'SOL ID' , 'Circle' , 'Activity' , 'Email ID' ,
                              'Contact No.' , 'Email ID.1' , 'Contact No..1' , 'Email ID.2' , 'Contact No..2' ,
                              'Email ID.3' , 'Contact No..3' , 'Email ID.4' , 'Contact No..4'] ,
    "MAILMASTER" : ['Project' , 'Bank' , 'FEEDER BRANCH' , 'SOL ID' , 'CRA' , "TO - BANK MAIL ID'S" ,
                    "CC - CRA'S MAIL ID" , 'BCC' , 'Subject' , 'Body text' , 'State' , 'Importance' , 'ATM Count' ,
                    'Status','from email id'] ,
    "CYPHERCODE" : ['Category' , 'Value' , 'Cypher Code' , 'Project ID' , 'Bank Code'] ,
    "CASHPREAVAILABILITY" : ['from_date' , 'to_date' , 'project_id' , 'bank_code' , 'feeder_branch_code' ,'applied_to_level',
                             'available_100_amount' , 'available_200_amount' , 'available_500_amount' ,
                             'available_2000_amount' , 'total_amount_available']
}

#####################################Indent#############################################

indent_table_dict = {
    "indent" : "indent_master" ,
    "indent_revision" : "indent_revision_request"
}

##### indent admin ####
admin_query_default = "select f.bank_code,f.project_id,f.feeder_branch,i.indent_short_code,i.cra,i.indent_order_number,i.created_on,i.email_indent_mail_master_id,i.record_status as 'indent_status',p.pdf_status as 'pdf_status',m.email_status as 'mail_status' from Feeder_Branch_Master as f left join indent_master as i on f.feeder_branch = i.feeder_branch and f.bank_code =i.bank and f.project_id = i.project_id and i.record_status = 'Active' AND i.order_date = CAST(GETDATE() as date) left join indent_pdf_status as p on i.indent_order_number = p.indent_order_number left join indent_mail_status as m on i.indent_order_number = m.indent_order_number where f.record_status= 'Active'"
admin_query = "select f.bank_code,f.project_id,f.feeder_branch,i.indent_short_code,i.cra,i.indent_order_number,i.created_on,i.email_indent_mail_master_id,i.record_status as 'indent_status',p.pdf_status as 'pdf_status',m.email_status as 'mail_status' from Feeder_Branch_Master as f left join indent_master as i on f.feeder_branch = i.feeder_branch and f.bank_code =i.bank and f.project_id = i.project_id and i.record_status = 'Active' left join indent_pdf_status as p on i.indent_order_number = p.indent_order_number left join indent_mail_status as m on i.indent_order_number = m.indent_order_number where f.record_status= 'Active'"


config_attr_dict = {
    "FEEDER_DENOMINATION" : ['Project' , 'Bank Code' , 'Feeder Branch Code' , 'Denomination 100' , 'Denomination 200' ,
                             'Denomination 500' , 'Denomination 2000'] ,
    "FDR_CASSTT_MAX_CAP_PER" : ['Project' , 'Bank Code' , 'Feeder Branch Code' , 'Denomination 100' ,
                                'Denomination 200' , 'Denomination 500' , 'Denomination 2000'] ,
    "FEEDER_DENO_PRE_AVAIL" : ['Project' , 'Bank Code' , 'Feeder Branch Code' , 'Atm Id' , '50 Available' ,
                               '100 Available' , '200 Available' , '500 Available' , '2000 Available' , 'From Date' ,
                               'To Date'] ,
    "LIMITDAYS" : ['EPS SITE CODE', 'ATM ID', 'PROJECT ID', 'BANK CODE', 'INDENT DATE', 'NEXT FEASIBLE DATE', 'NEXT WORKING DATE', 'DEFAULT DECIDE DAYS', 'DECIDE DAYS', 'DEFAULT LOADING DAYS', 'LOADING DAYS'],
    "FEEDER_VAULTING_PRE_CONFIG" : ['PROJECT', 'BANK CODE', 'FEEDER_BRANCH_CODE', 'IS_VAULTING_ALLOWED', 'VAULTING_PERCENTAGE', 'FROM_DATE', 'TO_DATE'],
    "EOD_ACTIVITY" : ['atm id', 'site code', 'project id', 'bank code', 'region', 'branch code','for date'],
    "CASHPREAVAILABILITY" : ['project_id', 'bank_code', 'applied_to_level', 'feeder_branch_code', 'available_50_amount', 'available_100_amount', 'available_200_amount', 'available_500_amount', 'available_2000_amount', 'total_amount_available', 'from_date', 'to_date']
}

##### cash reports queries #######
cash_reports_query = {
    "BOMH" : "SELECT id.atm_id as 'ATMID',  replace(convert(varchar, im.order_date, 6),' ','-')	 as 'INDENT DATE','EPS' as VENDOR, '' as 'INDENT NO',fm.sol_id as 'CODE', im.feeder_branch as 'Feeder Branch', id.location as 'Location',im.cra as 'CRA',id.loading_amount_100 as '100',id.loading_amount_200 as '200',id.loading_amount_500 as '500',id.loading_amount_2000 as '2000',id.total as 'Total' FROM indent_master im INNER JOIN indent_detail id ON im.indent_order_number = id.indent_order_number AND id.record_status = im.record_status AND id.record_status = 'Active' LEFT JOIN atm_master am ON id.atm_id = am.atm_id AND im.project_id = am.project_id AND im.bank = am.bank_code AND am.record_status = 'Active' and lower(am.site_status) = 'active' LEFT JOIN feeder_branch_master fm ON fm.feeder_branch = im.feeder_branch AND fm.bank_code = im.bank AND im.project_id = fm.project_id where im.bank = '%s' and fm.record_status = 'Active' and im.order_date = '%s' order by im.feeder_branch" ,
    "DENA" : "SELECT im.order_date as 'Date', ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS 'Sr.no',id.atm_id as 'ATM ID', id.location as 'SITE LOCATION', im.feeder_branch as 'FEEDER BRANCH', id.indent_order_number as 'INDENT NO',im.cra as 'CRA NAME', id.loading_amount_100 as '100',id.loading_amount_200 as '200',id.loading_amount_500 as '500',id.loading_amount_2000 as '2000',id.total as 'Total' FROM indent_master im INNER JOIN indent_detail id ON im.indent_order_number = id.indent_order_number AND id.record_status = im.record_status AND id.record_status = 'Active' LEFT JOIN atm_master am ON id.atm_id = am.atm_id AND im.project_id = am.project_id AND im.bank = am.bank_code AND am.record_status = 'Active' and lower(am.site_status) = 'active'  LEFT JOIN feeder_branch_master fm ON fm.feeder_branch = im.feeder_branch AND fm.bank_code = im.bank AND im.project_id = fm.project_id and fm.record_status = 'Active' where im.bank = '%s' and im.order_date = '%s' order by im.feeder_branch" ,
    "ALB" : "SELECT im.order_date as 'DATE', id.atm_id as 'ATMID', id.location as 'LOCATION',im.feeder_branch as 'FEEDER BRANCH', id.indent_order_number as 'INDENT NO', im.bank as 'Bank', id.loading_amount_100 as '100',id.loading_amount_200 as '200',id.loading_amount_500 as '500',id.loading_amount_2000 as '2000',id.total as 'Total' FROM indent_master im INNER JOIN indent_detail id ON im.indent_order_number = id.indent_order_number AND id.record_status = im.record_status AND id.record_status = 'Active' LEFT JOIN atm_master am ON id.atm_id = am.atm_id AND im.project_id = am.project_id AND im.bank = am.bank_code AND am.record_status = 'Active' and lower(am.site_status) = 'active' LEFT JOIN feeder_branch_master fm ON fm.feeder_branch = im.feeder_branch AND fm.bank_code = im.bank AND im.project_id = fm.project_id and fm.record_status = 'Active' where im.bank = '%s' and im.order_date = '%s' order by im.feeder_branch" ,
    "LVB" : "SELECT ROW_NUMBER() OVER (ORDER BY (SELECT 1)) AS 'Sr. No.',id.atm_id as 'ATM ID',id.location as 'LOCATION',fm.sol_id as 'Br Code',im.feeder_branch AS 'Withdrawal Branch',id.indent_order_number as 'INDENT NO',(id.loading_amount_2000)/2000 AS '2000',(id.loading_amount_500)/500 AS '500',(id.loading_amount_200)/200 AS '200',(id.loading_amount_100)/100 AS '100', id.total as 'Total',im.cra as 'CRA' FROM indent_master im INNER JOIN indent_detail id ON im.indent_order_number = id.indent_order_number AND id.record_status = im.record_status AND id.record_status = 'Active' LEFT JOIN atm_master am ON id.atm_id = am.atm_id AND im.project_id = am.project_id AND im.bank = am.bank_code AND am.record_status = 'Active' and lower(am.site_status) = 'active' LEFT JOIN feeder_branch_master fm ON fm.feeder_branch = im.feeder_branch AND fm.bank_code = im.bank AND im.project_id = fm.project_id and fm.record_status = 'Active' where im.bank = '%s' and im.order_date = '%s' order by im.feeder_branch"
}

loading_recommendations_query = "Select replace(convert(varchar, IM.order_date, 6),' ','-') as 'DATE', ROW_NUMBER() OVER(ORDER BY (SELECT 1)) AS 'SR. NO.', AM.atm_id as 'ATM ID', AM.location_name as 'LOCATION', IM.feeder_branch as 'FEEDER BRANCH', IM.cra as 'CRA', am.state as 'STATE / REGIONS', am.district as 'DISTRICT', IM.bank as 'BANK', IM.total_atm_loading_amount_100 as 'Planning-100', IM.total_atm_loading_amount_200 as 'Planning-200', IM.total_atm_loading_amount_500 as 'Planning-500', IM.total_atm_loading_amount_2000 as 'Planning-2000', IM.total_atm_loading_amount as 'Planning-Total', ' ' as '-', IM_rev.total_atm_loading_amount_100 as 'Revised Planning-100', IM_rev.total_atm_loading_amount_200	as 'Revised Planning-200', IM_rev.total_atm_loading_amount_500 as 'Revised Planning-500', IM_rev.total_atm_loading_amount_2000 as 'Revised Planning-2000', IM_rev.total_atm_loading_amount as 'Revised Planning-Total' from atm_master AM inner join indent_detail ID on ID.atm_id=AM.atm_id and ID.record_status='Active' and AM.record_status='Active' and lower(AM.site_status)= 'active' left join indent_master IM on  IM.indent_order_number=ID.indent_order_number and IM.indent_counter=1 and IM.record_status='Active' left join indent_master IM_rev on ID.indent_order_number=IM_rev.previous_indent_code and IM_rev.indent_counter<>1 and IM_rev.record_status='Active' where IM.bank = '%s' and  IM.order_date = '%s' order by im.feeder_branch"

indent_summary_report_query = "select atm_id,atm_band,site_code,bank_code,feeder_branch_code,indent_code,atm_priority,ignore_code,ignore_description,reason_for_priority,project_id,indentdate,CashOut,bank_decide_limit_days,bank_loading_limit_days,bank_cash_limit,insurance_limit,applied_vaulting_percentage,cra,base_limit,dispenseformula,decidelimitdays,loadinglimitdays,deno_100_priority,deno_200_priority,deno_500_priority,deno_2000_priority,avgdispense,morning_balance_100,morning_balance_200,morning_balance_500,morning_balance_2000,total_morning_balance,avgdecidelimit,DecideLimit,threshold_limit,loadinglimit,loadingGap_cur_bal_avg_disp,loading_amount,0 as tentative_loading_amount_100, 0 as  tentative_loading_amount_200,0 as tentative_loading_amount_500,0 as tentative_loading_amount_2000,rounded_amount_100,rounded_amount_200,rounded_amount_500,rounded_amount_2000,expected_balanceT1_100,expected_balanceT1_200,expected_balanceT1_500,expected_balanceT1_2000,total_expected_balanceT1,final_total_loading_amount_100,final_total_loading_amount_200,final_total_loading_amount_500,final_total_loading_amount_2000,final_total_loading_amount,pre_avail_loading_amount_100,pre_avail_loading_amount_200,pre_avail_loading_amount_500,pre_avail_loading_amount_2000,previous_indent_code,is_zero_dispense,indent_type,indent_counter,curbal_div_avgDisp,default_flag,cassette_100_count,cassette_200_count,cassette_500_count,cassette_2000_count,total_cassette_capacity,created_on,record_status from distribution_planning_detail_V2 where  record_status = ISNULL('%s',record_status)  and indentdate = ISNULL('%s',indentdate) and bank_code = ISNULL('%s',bank_code) order by bank_code,feeder_branch_code, atm_priority"

activity_status_query = "select project_id,region,bank_name,data_for_type,data_status,latest_date_time from ( Select project_id,'All' region,bank_name , 'Morning Balance' as data_for_type,'Uploaded' as data_status ,max(cast(datafor_date_time as date)) latest_date_time  from morningbalance where record_status='Active' and bank_name IN (%s) and cast(datafor_date_time as date) >= dateadd(day,datediff(day,1,GETDATE()),0) group by bank_name,project_id union Select project_id,'All' region,bank_name, 'Balance File' as data_for_type,'Uploaded' as data_status ,max(cast(datafor_date_time as date)) latest_date_time from cash_balance_register where record_status ='Active' and is_valid_record = 'Yes' and bank_name IN (%s) and cast(datafor_date_time as date) >= dateadd(day,datediff(day,1,GETDATE()),0) group by bank_name,project_id union Select project_id,'All' region,bank_name, 'Dispense File' as data_for_type,'Uploaded' as data_status ,max(cast(datafor_date_time as date)) latest_date_time from cash_dispense_register where record_status='Active' and is_valid_record = 'Yes' and bank_name IN (%s) and cast(datafor_date_time as date) >= dateadd(day,datediff(day,1,GETDATE()),0) group by bank_name,project_id union Select project_id,'All' region,bank_code as bank_name, 'Decide Limits' as data_for_type,'Uploaded' as data_status ,max(cast(indent_date as date)) latest_date_time from limitdays  where record_status='Active' and bank_code IN (%s) and cast(indent_date as date) >= dateadd(day,datediff(day,1,GETDATE()),0) group by bank_code,project_id union Select project_id,'All' region,bank , 'Average Dispense Calc' as data_for_type,'Uploaded' as data_status ,max(cast(datafor_date_time as date)) latest_date_time  from calculated_dispense where record_status='Active' and bank IN (%s) and cast(datafor_date_time as date) >= dateadd(day,datediff(day,1,GETDATE()),0) group by bank,project_id union Select project_id,'All' region,bank_name, 'Default Dispense' as data_for_type,'Uploaded' as data_status ,max(cast(datafor_date_time as date)) latest_date_time from cash_dispense_register where record_status='Active' and dispense_type = 'Default' and bank_name IN (%s) and cast(datafor_date_time as date) >= dateadd(day,datediff(day,1,GETDATE()),0) group by bank_name,project_id union Select 'All' as project_id,'All' as region,cra_name as bank_name,'C3R Files' as data_for_type,'Approval Pending' as data_status ,max(cast(datafor_date_time as date)) latest_date_time from c3r_CMIS  where record_status='Approval Pending'  and cast(datafor_date_time as date) >= dateadd(day,datediff(day,1,GETDATE()),0) and cra_name IN (%s) group by cra_name union select 'All' as project_id,'All' as region,'All' as bank_name, case when data_for_type = 'AMS_ATM' then 'ATM Master' when data_for_type = 'ATMCNFGLIMIT' then 'ATM Configs and Limits' when data_for_type = 'BANKESCALATIONMATRIX' then 'Bank Escalation Matrix' when data_for_type = 'BRANDBILLCAPACITY' then 'Brand Bill Capacity' when data_for_type = 'CRAEMPANELED' then 'CRA Empaneled' when data_for_type = 'CRAESCALATIONMATRIX' then 'CRA Escalation Matrix' when data_for_type = 'CRAFEASIBILITY' then 'CRA Feasibility' when data_for_type = 'CRAESCALATIONMATRIX' then 'CRA Escalation Matrix' when data_for_type = 'CYPHERCODE' then 'Cypher Code' when data_for_type = 'EPSESCALATIONMATRIX' then 'EPS Escalation Matrix' when data_for_type = 'MAILMASTER' then 'Mail Master' else data_for_type end data_for_type,'Approval Pending' as data_status ,max(cast(datafor_date_time as date)) latest_date_time  from data_update_log  where record_status='Approval Pending' and cast(datafor_date_time as date) >= dateadd(day,datediff(day,1,GETDATE()),0) and data_for_type in ('MAILMASTER','EPSESCALATIONMATRIX','CYPHERCODE','CRAESCALATIONMATRIX','CRAFEASIBILITY', 'CRAESCALATIONMATRIX','CRAFEASIBILITY','CRAESCALATIONMATRIX','CRAEMPANELED','BRANDBILLCAPACITY','BANKESCALATIONMATRIX','ATMCNFGLIMIT','AMS_ATM') group by data_for_type ) x order by latest_date_time desc"

########### insert project bank feeder cra #######3
project_bank_feeder_cra  = "insert into project_bank_feeder_cra_mapping (bank_id,project_id,feeder,iscbr,region,isdispense )select distinct a.bank_code,a.project_id,case when ab.feeder_branch_code is null then 'na' else ab.feeder_branch_code end feeder_branch_code,'Y' as iscbr,case when a.bank_code in ('ALB','BOMH','CAB','BOB','CBI','CORP','DENA','IDBI','PNB','PSB','SBI','UBI','UCO','VIJAYA','IOB','LVB','KGB','RSBL')then 'All' else case when a.bank_code in ('BOI') and a.project_id in ('MOF_MAH') then 'All' else case when a.bank_code in ('BOI') and a.project_id in ('MOF_BJK') and a.state in ('JHARKHAND','BIHAR') then 'BJ' else case when a.bank_code in ('BOI') and a.project_id in ('MOF_BJK') and a.state in ('KERALA') then 'K' end end end end region,case when a.bank_code  in ('BOMH','SBI','LVB','CAB','RSBL','UBI') then 'Y' else 'N' end iscdr from atm_master a join cra_feasibility ab on a.atm_id = ab.atm_id and a.site_code=ab.site_code and a.site_status = 'Active'and a.record_status = 'Active' and ab.record_status = 'Active' and (ab.feeder_branch_code <> null or ab.feeder_branch_code <> '')where not exists (select pfc.bank_id,pfc.project_id,pfc.Feeder from project_bank_feeder_cra_mapping pfc where pfc.bank_id = a.bank_code and pfc.project_id = a.project_id and pfc.Feeder = ab.feeder_branch_code)"

#### ims columns not needed ###
ims_columns = ['VendorTicketNo','Country','State','City','Location','ATMBrand','PopulationType','Circle','SubCategory','AssignTo','Dispatch Date','Dispatch Time','DispatchedDuration','CloseDate','CloseTime','Updated_By','Updated_Date','UserComment','Special Comment','Follow Up Complete','Ticket Close Flag','Dispatch To','Park To Me','Park By Me','Approval Id','Amount']

menu_permission_query = "select mg.*,m.display_name as 'child_display_name',mm.display_name as'parent_display_name',p.name as 'permission_name',g.name as 'group_name' from menu_group_permission_mapping mg left join menu_master m on mg.menu_id = m.parent_menu_id and mg.sub_menu_id = m.menu_id left join auth_permission p on mg.permission_id = p.id left join auth_group g on mg.group_id = g.id left join menu_master mm on mm.menu_id = mg.menu_id and mm.link_type = 'expand'  where mg.record_status = '%s'"
menu_permission_query_filter = "select mg.*,m.display_name as 'child_display_name',mm.display_name as'parent_display_name',p.name as 'permission_name',g.name as 'group_name' from menu_group_permission_mapping mg left join menu_master m on mg.menu_id = m.parent_menu_id and mg.sub_menu_id = m.menu_id left join auth_permission p on mg.permission_id = p.id left join auth_group g on mg.group_id = g.id left join menu_master mm on mm.menu_id = mg.menu_id and mm.link_type = 'expand' where mg.record_status = '%s'"