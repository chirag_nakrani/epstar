from Common import db_queries_properties
from rest_framework import serializers
from django.apps import apps

def view_serializer_fn(file_type,bank_code):
    model_key = file_type + bank_code + "_TABLE"
    model_name_key = db_queries_properties.table_dict[model_key]
    model_name = apps.get_model(app_label='Model', model_name=model_name_key)
    field_key = file_type + bank_code
    field_name_list = db_queries_properties.table_dict[field_key]
    field_str = "','".join(field_name_list)
    fields_str_final = "'" + field_str + "'"
    print(fields_str_final)
    class view_serializer(serializers.ModelSerializer):
        class Meta:
            model = model_name
            fields = fields_str_final

    return view_serializer()