from django.db import models
from django.db import connection

class data_update_log(models.Model):

    class Meta:
        db_table = 'data_update_log'
    id = models.BigAutoField(primary_key=True)
    bank_code = models.CharField(max_length=15,null=True)
    project_id = models.CharField(max_length=50,null=True)
    operation_type = models.CharField(max_length=50,null=True)
    datafor_date_time = models.DateTimeField(null=True)
    region = models.CharField(max_length=50,null=True)
    data_for_type = models.CharField(max_length=50,null=True)
    source_info = models.CharField(max_length=2000,null=True)
    destination_info = models.CharField(max_length=2000,null=True)
    status_info_json = models.CharField(max_length=4000,null=True)
    start_loading_on = models.DateTimeField(null=True)
    end_loading_on = models.DateTimeField(null=True)
    created_by = models.CharField(max_length=50,null=True)
    created_reference_id = models.CharField(max_length=50,null=True)
    validated_on = models.DateTimeField(null=True)
    approved_on = models.DateTimeField(null=True)
    approved_by = models.CharField(max_length=50,null=True)
    approved_reference_id = models.CharField(max_length=50,null=True)
    approver_comment = models.CharField(max_length=2000,null=True)
    consolidated_on = models.DateTimeField(null=True)
    deleted_on = models.DateTimeField(null=True)
    deleted_by = models.CharField(max_length=50,null=True)
    deleted_reference_id = models.CharField(max_length=50,null=True)
    rejected_on = models.DateTimeField(null=True)
    rejected_by = models.CharField(max_length=50,null=True)
    reject_reference_id = models.CharField(max_length=50,null=True)
    reject_comment = models.CharField(max_length=2000,null=True)
    record_status = models.CharField(max_length=50,null=True)
    is_valid_file = models.IntegerField(blank=True, null=True)
    date_created = models.DateTimeField(null=True)
    date_updated = models.DateTimeField(null=True)
    modified_on = models.DateTimeField(null=True)
    modified_by = models.CharField(max_length=50,null=True)
    modified_reference_id = models.CharField(max_length=50,null=True)
    validation_code = models.CharField(max_length=100, blank=True, null=True)
    cra_name = models.CharField(max_length=50,null=True)

    def __str__(self):
        return str(self.datafor_date_time)

class app_config_param(models.Model):

	class Meta:
		db_table = 'app_config_param'

	category = models.CharField(max_length=50,null=True)
	sub_category = models.CharField(max_length=50,null=True)
	sequence = models.IntegerField(null=True)
	value = models.CharField(max_length=255,null=True)

class data_update_log_master(models.Model):
    id = models.BigAutoField(primary_key=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    operation_type = models.CharField(max_length=50, blank=True, null=True)
    datafor_date_time = models.DateTimeField(blank=True, null=True)
    data_for_type = models.CharField(max_length=50, blank=True, null=True)
    source_info = models.CharField(max_length=2000, blank=True, null=True)
    destination_info = models.CharField(max_length=2000, blank=True, null=True)
    status_info_json = models.TextField(blank=True, null=True)
    start_loading_on = models.DateTimeField(blank=True, null=True)
    end_loading_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    validated_on = models.DateTimeField(blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approver_comment = models.CharField(max_length=2000, blank=True, null=True)
    consolidated_on = models.DateTimeField(blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    reject_comment = models.CharField(max_length=2000, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    is_valid_file = models.CharField(max_length=20, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    validation_code = models.CharField(max_length=100, blank=True, null=True)
    total_count = models.IntegerField(blank=True, null=True)
    approved_count = models.IntegerField(blank=True, null=True)
    pending_count = models.IntegerField(blank=True, null=True)
    reject_count = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.id

    class Meta:
        managed = False
        db_table = 'data_update_log_master'

class data_update_log_reference(models.Model):
    id = models.BigAutoField(primary_key=True)
    project_id = models.CharField(max_length=50, blank=True, null=True)
    operation_type = models.CharField(max_length=50, blank=True, null=True)
    datafor_date_time = models.DateTimeField(blank=True, null=True)
    data_for_type = models.CharField(max_length=50, blank=True, null=True)
    source_info = models.CharField(max_length=2000, blank=True, null=True)
    destination_info = models.CharField(max_length=2000, blank=True, null=True)
    status_info_json = models.TextField(blank=True, null=True)
    start_loading_on = models.DateTimeField(blank=True, null=True)
    end_loading_on = models.DateTimeField(blank=True, null=True)
    created_by = models.CharField(max_length=50, blank=True, null=True)
    created_reference_id = models.CharField(max_length=50, blank=True, null=True)
    validated_on = models.DateTimeField(blank=True, null=True)
    approved_on = models.DateTimeField(blank=True, null=True)
    approved_by = models.CharField(max_length=50, blank=True, null=True)
    approved_reference_id = models.CharField(max_length=50, blank=True, null=True)
    approver_comment = models.CharField(max_length=2000, blank=True, null=True)
    consolidated_on = models.DateTimeField(blank=True, null=True)
    deleted_on = models.DateTimeField(blank=True, null=True)
    deleted_by = models.CharField(max_length=50, blank=True, null=True)
    deleted_reference_id = models.CharField(max_length=50, blank=True, null=True)
    rejected_on = models.DateTimeField(blank=True, null=True)
    rejected_by = models.CharField(max_length=50, blank=True, null=True)
    reject_reference_id = models.CharField(max_length=50, blank=True, null=True)
    reject_comment = models.CharField(max_length=2000, blank=True, null=True)
    record_status = models.CharField(max_length=50, blank=True, null=True)
    is_valid_file = models.CharField(max_length=20, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_updated = models.DateTimeField(blank=True, null=True)
    modified_on = models.DateTimeField(blank=True, null=True)
    modified_by = models.CharField(max_length=50, blank=True, null=True)
    modified_reference_id = models.CharField(max_length=50, blank=True, null=True)
    validation_code = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'data_update_log_reference'
