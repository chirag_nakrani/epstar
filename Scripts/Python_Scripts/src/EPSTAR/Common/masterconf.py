### This File contains properties for general Routine File Upload Configuration ###

###################################File Paths#####################################

import os
import sys
from EPSTAR.settings import BASE_DIR,DATABASES
from configurations import Configuration
from EPSTAR.settings import Dev,Local,Uat,Sit,Uat_Sandbox
InputFolder = os.path.join(BASE_DIR,'FileFolder\InputFolder')
OutputFolder = os.path.join(BASE_DIR, 'FileFolder\OutputFolder')
LogFolder = os.path.join(BASE_DIR, 'FileFolder/LogFolder/')
WithdrawalFolder = os.path.join(BASE_DIR, 'FileFolder/Withdrawal/')
upload_file_path1 = InputFolder
#upload_file_path1 = 'F:/Work/EPS/Source/EPStarEDL2.0/EPSTAR/FileFolder/InputFolder/';
SignatureFolder = os.path.join(BASE_DIR,'FileFolder/SignatureFolder/')
upload_signature_path = SignatureFolder

signature_output_folder = os.path.join(BASE_DIR,'FileFolder/extractfolder/')
withdrawal_output_folder = os.path.join(BASE_DIR,'FileFolder/Withdrawal/download/')
report_output_folder = os.path.join(BASE_DIR,'FileFolder/cash_report/')

#connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.75.15;DATABASE=epstar12Nov;UID=sa;PWD=Mysql@123'
connection_string = eval(os.environ.get('DJANGO_CONFIGURATION')).connection_string
# connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.208.17;DATABASE=epstar;UID=sqladmin;PWD=Mysql@123;'
#connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.43.148;DATABASE=epstarstaging;UID=sa;PWD=Mysql@123;'
#connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.208.17;DATABASE=epstar;UID=sa;PWD=Mysql@123'
upload_file_path2 = OutputFolder

###################################Bank codes#####################################
bankcode_alb = 'ALB'
bankcode_bob = 'BOB'
bankcode_boi = 'BOI'
bankcode_cab = 'CAB'
bankcode_cbi = 'CBI'
bankcode_sbi = 'SBI'
bankcode_ubi = 'UBI'
bankcode_uco = 'UCO'
bankcode_vjb = 'VJB'
bankcode_iob = 'IOB'
bankcode_lvb = 'LVB'
bankcode_psb = 'PSB'
bankcode_rsbl = 'RSBL'
bankcode_bomh = 'BOMH'
bankcode_corp = 'CORP'
bankcode_idbi = 'IDBI'
bankcode_dena = 'DENA'

###################################File Types#####################################
file_type_cbr = 'CBR'
file_type_dispense = 'DISPENSE'
file_type_master = 'MASTER'
file_type_vcb_eod = 'VCB'
file_type_C3R_CMIS = 'C3R_CMIS'
file_type_C3R_VMIS = 'C3R_VMIS'
file_type_C3R = 'C3R'
file_type_Daily_Loading_Report = 'Daily_Loading_Report'
file_type_bank_names_list = 'ALL'
bankinfolist = ['bank_name_list','Project_id_list','CRA_id_list']
file_type_cbr_api = 'CBR'
file_type_dispense_api = 'DISPENSE'
file_type_vcb_api = 'VCB'
file_type_c3r_api = 'C3R'
file_type_dlr_api = 'DLR'
cbr_bomh = 'xls'
file_type_error = 'file_type should be CBR or DISPENSE or VCB or DLR or file_type may not provided'
api_status_Success = 'Success'
api_status_Failure = 'Failure'
file_type_c3r_api = 'C3R'
feederlist = 'feederlist'
file_type_feeder = 'feeder'
file_type_cypher_code = 'CYPHERCODE'
success_response = {
	"status" : "Success"
}
file_type_cra_master = 'cra_master'
file_type_ams_atm = 'ams_atm'

failure_response = {
	"status" : "Failure"
}

upload_req_params = {
	"CBR":['file_type','bank_code','upload_datatime','project_id', 'region'],
	"DISPENSE":['file_type','bank_code','upload_datatime','project_id', 'region'],
	# "VCB":['CRA', 'data_for_date', 'project_id', 'file_type','bank_code'],
	"vcb":['data_for_date','file_type','cra'],
	"C3R_VMIS" : ['file_type','upload_datatime','cra'],
	"Daily_Loading_Report" :['file_type', 'upload_datatime','bank_code', 'project_id','cra'],
	"routine_data_info" : ['routine_info','file_type'],
	"common_bankfeeder_info" : ['file_type','common_bankfeeder_info'],
	"ims" : ['file_type','date']
}
view_req_params = ['file_type','bank_code']
upload_req_params_vcb_eod = ['CRA', 'data_for_date', 'project_id', 'file_type']
status_update_param = ['file_type','datafor_date_time','status','created_reference_id']
status_update_param_vcb = ['file_type','project_id']

#################################SFTP Related properties###############################
is_sftp = eval(os.environ.get('DJANGO_CONFIGURATION')).is_sftp
sftp_host = eval(os.environ.get('DJANGO_CONFIGURATION')).sftp_host
sftp_host_port = eval(os.environ.get('DJANGO_CONFIGURATION')).sftp_host_port
sftp_host_username = eval(os.environ.get('DJANGO_CONFIGURATION')).sftp_host_username
sftp_host_password = eval(os.environ.get('DJANGO_CONFIGURATION')).sftp_host_password
sfpt_file_path = eval(os.environ.get('DJANGO_CONFIGURATION')).sfpt_file_path
sftp_parent_folder = eval(os.environ.get('DJANGO_CONFIGURATION')).sftp_parent_folder

#################################Data Validation in DB properties######################

data_validated_successful = "Data Validation Successful"
#data_validated_successful = "File needs to be in Approved Status before Proceeding";
data_validated_failed = "Data Validation Failed"
format_val_success_status = "Format Validation Successful"
status_update_success = "Status Updated Successfully"
partial_valid_file = "Partial Valid File"
partial_valid_file_vcb = "Partial Valid file. ATM ID not present in system"
partial_valid_file_extra_id	= "Partial Valid file. Extra ATM ID in file"
valid_file_with_new_Atmid ="Valid File. While there is a new atm id in file"
valid_file_with_less_Atmid ="Valid File.  all atm ids not present in file"
format_level = "format"
data_level = "data"
system_level = "system"
system_level_exception = "Internal Server Error"
request_level = "request"
request_level_exception = "Bad Request or Params"
no_bank = "Request for this bank can't be accepted"
Consolidation_Successful = "Consolidation Successful"
data_uploaded_successful = "Data Uploaded Successfully"

################################ Status to be updated #############################
check_for_status = "approved"
updated_status = "Approved"
reject_status = "Rejected"
pending_status = "Approval Pending"


############################Master File Upload Properties##########################
file_type_atm = 'ATM'
file_type_feeder_branch = 'FDRBRNCH'
file_type_atm_cnfg = 'ATMCNFGLIMIT'
file_type_cra_mstr = 'CRAFEASIBILITY'
file_type_cypr_code = 'CYPRCD'
file_type_mail_mstr = 'MAILMSTR'

status_master_param = ['file_type']
status_master_update_param = ['file_type']
status_app_rej = ['menu','id','status','created_reference_id']  #Name Changed as per naming convention
status_app_rej_holiday = ['menu','created_reference_id']


success_codes_desc = {
	"S100" : "Format Validated Successfully",
	"S101" : "Data Validated Successfully",
	"S102" : "Data Updated Successfully",
	"S103" : "Data Authorized Successfully",
	"S104" : "Data Rejected Successfully",
	"S105" : "Data Added Successfully",
	"S106" : "Data Deleted Successfully",
	"S107" : "Data Modified Successfully",
	"S108" : "Indent generated Successfully",
	"11001" : "Indent generated Successfully",
	"11004" : "Indent Request Successfully Submitted",
	"50001" : "Partial Valid File",
	"S109" : "Indent record saved and pdf send successfully"
}

error_codes_desc = {
	"E100" : "Format Validation Failed",
	"E101" : "New ATM ID Found in File",
	"E102" : "Site Code Changed Found",
	"E103" : "Unable to update records",
	"E104" : "Status update failed",
	"E105" : "No records found to update",
	"E106" : "Record Already Exists",
	"E107" : "No record found to delete",
	"E108" : "Data already deleted",
	"E109" : "Fail to generate indent revision",
	"11002" : "Fail to generate indent",
	"11003" : "No Records Found for Indent Generation",
	"E110" : "Unable to Add Records",
	"E111" : "Fail to delete records",
	"E112" : "No Valid record found",
	"10001" : "No Valid record found",
	"E113" : "Something went wrong. Try again",
	"E114" : "Record Approved But failed to generate pdf",
	"E115" : "Record Saved But failed to generate pdf",
}

status_approval_success = "S103"
status_rejection_success = "S104"
format_val_success_mstr_upload = "S100"
data_val_success_mstr_upload = "S101"

format_val_failed_mstr_upload = "E100"

format_val_failed_upload = "E100"
data_failed_atmid_not_found = "E101"
data_failed_site_code_change = "E102"
no_record_found_update = "E105"

system_level_exception_code = "X000"

request_level_exception_code = "R000"


###########################################Ticket Management constants#############################

create_ticket_fields = ['subject','remarks','category','sub_category']
create_ticket_reply_fields = ['ticket_id','replymessage']
ticket_detail_req = ['ticket_id']
assign_category_req = ['category','sub_category','assigned_to_group','assigned_to_user']

##############################################Reference Constants#####################################

rejection_param = ['operation']
rejection_param_add = ['operation','payload']
rejection_param_read = ['operation']
operation_type_add = 'A'
operation_type_delete = 'D'
operation_type_modify = 'M'
operation_type_read = 'R'


######################################Mail Notification Constants###############################

MAILHOST = "smtp.office365.com"
MAILPORT = 587
mail_sender= "epstar@electronicpay.in"
mail_password = "Ruh75373"
mail_recipient = "sudhakar.chavan@exponentiadata.com"


######################################Mail Notification Constants for ALL Banks###############################
ALL_mail_sender = {
	"BOMH": "cashops_bomh@electronicpay.in",
	"SBI" : "cashops.sbi@electronicpay.in",
	"UCO" : "cashops.uco@electronicpay.in",
	"UBI" : "cashops.ubi@electronicpay.in",
	"IOB" : "cashops.iob@electronicpay.in",
	"IDBI": "cashops.idbi@electronicpay.in",
	"DENA": "cashops.dena@electronicpay.in",
	"CORP": "cashops.corp@electronicpay.in",
	"CBI" : "cashops.cbi@electronicpay.in"
}

ALL_mail_password = {
	"BOMH" : "Eps#2019",
	"SBI"  : "110@Sbi@110",
	"UCO"  : "110@Uco@110",
	"UBI"  : "110@Ubi@110",
	"IOB"  : "110@Iob@110",
	"IDBI" : "110@Idbi@110",
	"DENA" : "110@Dena@110",
	"CORP" : "110@Corp@110",
	"CBI"  : "110@Cbi@110"
}

######################################Routine Data related constants (Ticket Management and mail)##############################
category_routine = 'Routine'
subject_format_cbr = "Format Validation Failed in CBR"
subject_data_cbr = "Validation Failed in CBR"
subject_format_disp = "Format Validation Failed in Dispense"
subject_data_disp = "Validation Failed in Dispense"
subject_format_vcb = "Format Validation Failed in VCB"
subject_data_vcb = "Validation Failed in VCB"
subject_format_c3r = "Format Validation Failed in C3R"
subject_data_c3r = "Validation Failed in C3R"
subject_format_daily = "Format Validation Failed in Daily Loading"
subject_data_daily = "Validation Failed in Daily Loading"
subject_data_IMS = "Validation Failed in IMS"
sub_category_cbr = "CBR"
sub_category_disp = "Dispense"
sub_category_vcb = "VCB"
sub_category_c3r = "C3R"
sub_category_daily = "Daily Loading"

#################################Master Data related constants (Ticket Management and mail)########################################

category_master = "Master"
sub_category_atm_cnfg = "ATMCNFGLIMIT"
subject_format_atm_cnfg = "Format Validation Failed in ATM Config Limits"

####################################################################################################################################

upload_fail_subject = "Upload File Failed. System Error Occured"
upload_failed_msg = """ Some System error occured in file upload. Kindly contact system administrator. """
create_ticket_url = "http://192.168.75.15:8080/ticket/create/"
# create_ticket_url = "http://localhost:8080/ticket/create/"
format_validation_error_subject = "Format Validation failed. Ticket Has been raised with ticket ID"
format_validation_error_msg = "Format Validation Failed in File. Ticket has been raised. Please login to EPSTAR Application for further information"
data_validation_error_subject = "Data Validation failed. Ticket Has been raised with ticket ID"
data_validation_error_msg = "Data Validation Failed in File. Ticket has been raised. Please login to EPSTAR Application for further information"

###################################ReferenceData######################################
file_type_RD_brand_BillCapacity = 'BRANDBILLCAPACITY'
status_reference_param = ['file_type'] #it checks for input param of reference file upload
file_type_vault_master = 'CRAVAULTMASTER'
# file_type_vendor_master = 'CRA_vendor_master'
format_val_success_reference_upload = "S100"
format_val_failed_reference_upload = "E100"
status_reference_update_param = ['file_type']
data_val_success_reference_upload = "S101"
file_type_RD_CRAEMPANELED = 'CRAEMPANELED1'
# file_type_vault_master = 'CRA_vault_master'
file_type_Empaneled = 'CRAEMPANELED'
######## excalation matrix reference type list #############
file_type_escalation_matrix = ['CRAESCALATIONMATRIX', 'BANKESCALATIONMATRIX','EPSESCALATIONMATRIX']
######## mail master reference type #############
file_type_mail_master = 'MAILMASTER'
file_type_holiday_master = 'HOLIDAYMASTER'

################################Signature#############################################
status_signature_param = ['signatory_name','designation','bank_code','project_id','file_type']
signature_available = "Signature uploaded successfully"
signature_success_status_code = "S100"
signature_failure_status_code = "E100"
signature_not_available = "Signature Not Available. Try again!!!"
status_signature_download = ['signatory_name','designation']
signature_download = 'yes'
signature_fetched_successfully = 'yes'


c3rapprovalpending = ['data_for_type', 'record_status']
record_status_C3R_AP = 'Approval Pending'
record_status_C3R_APP = 'Approved'
C3RApi_statusText = 'Successful'
c3rview = ['file_type']
Not_Consolidated = 'Status Updated Successfully'
mismatchFileType = 'Input file type is other than C3R'
mismatchparam = 'Input param is not correct'
c3rapi_failure = 'filetype is not C3R'
c3rapi_wrongTablePassed = "Table name other than C3R_CMIS or C3R_VMIS provided"

ams_master = 'ams_atm'
incorrect_input_file = "File is not valid."
data_validated_successful_vcb = 'Data Uploaded Successfully'
no_record = 'No Valid record found'
file_type_signature = 'signature'
success_holiday_master = 'S101'

###############################Indent Related Constants########################################
indent_detail_req = ['indent_order_no']
file_type_ims = 'ims'
improper_file_type = 'Improper File Type'
type_of_file = 'file_type'
view_amsMaster_request = ["menu","record_status"]
no_filter_provided = 'Incomplete Information Provided.'

###############################User Related Constants##########################################
user_bank_req = ['operation','payload']
menu_perm_req = ['operation_type','payload']
success_status_resp = 'Successful'
success_status_desc_resp = 'Data Added Successfully'
failure_status_resp = 'Failed'
failure_status_desc_resp = 'Data Added Failed'
invalid_req = 'Invalid Request'
invalid_req_desc = 'Invalid Request. Please check Request'
success_del_status_resp = 'Successful'
success_del_status_desc_resp = 'Data Removed Successfully'
failure_del_status_resp = 'Failed'
failure__del_status_desc_resp = 'Data Removed Failed'


#################MASTER API CONSTANTS#################################
masterApiSuccess = 'Successful'
masterApiFailed = 'Failed'
amsview_req = ["menu"]
status_masterapi_param = ["file_type", "data_type"]
amsapi_success_code = 200


################IMS VIEW DATA INPUT PARAMETER########################
view_req_params_ims = ['file_type']


##################Reference update API constants######################
status_reference_update_param = ['file_type','project_id']

##################Default Loading related data########################

defaultLoad_InputReq = ['operation', 'payload']
holiday_InputReq = ['operation', 'payload']
add_new_data_for_default_loading = 'ADD'
add_new_data_for_holiday_master = 'ADD'
new_data_record_status = 'Active'
appr_pending_state = 'Approval Pending'
record_not_inserted = "Record Not inserted as same record exists in the memory"
data_updated = "S105"
Record_Already_Exists = "E106"
update_data_for_default_loading = 'UPDATE'
update_data_for_holiday_master = 'UPDATE'
record_status_history = 'History'
data_updated_for_approval = "Data has been updated. In Approval pending state."
data_inserted_successfully = "Data has been inserted successfully, moved to approval Pending state."
data_inserted_successfully_active_state = "Data has been inserted successfully, moved to active state."
file_type_default_uploading = 'DEFAULTLOADING'
file_type_loading_confirm = 'CONFIRMLOADING'
reference_menu_controller = [file_type_RD_brand_BillCapacity.upper(), file_type_Empaneled.upper(), file_type_vault_master.upper(), file_type_default_uploading.upper(),file_type_escalation_matrix[0].upper(),file_type_escalation_matrix[1].upper(),file_type_escalation_matrix[2].upper(),file_type_mail_master.upper(), file_type_signature.upper(),file_type_default_uploading.upper(), file_type_cypher_code.upper(), 'ATM_LIST','ATM_REVISION_CASH_AVAILABILITY','GENERAL_LEDGER_ACCOUNT_NUMBER']
uploaded_state = 'Uploaded'

# data_code = "S"
data_insert_modify_failed = "Data manipulation failed, try again."
wrong_operation_invoked = "Please select correct operation i.e. 'A' for inserting new record or 'M' for modifying the Active record"



############################################Dispense Upload Issue ES1-I44 ##################################################

cash_disp_sbi_column = 'cash_withdrawal_by_cust_'
opening_bal_disp_sbi_col = 'opening cash in the atm_'
total_cash_disp_sbi_col = 'total cash replenished_on_'




###################Indent##########################################
Success_create_record = "S105"
success_create_pending_approval = "S102"
auto_approval_permission = 'auto_approval'
status_indent_param = ['updated_record_status','existing_record']
indent_generation_fail = "E109"
active_status = 'Active'
review_pending_status = 'Review Pending'
history_status = 'History'
rejected_status = 'Rejected'
no_record_updated = 'No Record updated'
status_indent_generate_param = ['feeder_branch','project_id','bank_code']
indent_gen_success_sp = '11001'
indent_gen_fail_sp = '11002'
indent_gen_no_records_sp = '11003'
indent_request_success_submit = '11004'
revision_already_exists = '11005'
revised_indent_type = 'Revised Indent'

###################################### Indent PDF related fields ####################################################

final_page = ['total_atm_loading_amount','total_atm_loading_amount_100','total_atm_loading_amount_200','total_atm_loading_amount_500','total_atm_loading_amount_2000','cra_opening_vault_balance','cra_opening_vault_balance_100','cra_opening_vault_balance_200','cra_opening_vault_balance_500','cra_opening_vault_balance_2000','total_bank_withdrawal_amount','total_bank_withdrawal_amount_100','total_bank_withdrawal_amount_200','total_bank_withdrawal_amount_500','total_bank_withdrawal_amount_2000','amount_in_words','closing_vault_balance_100','closing_vault_balance_200','closing_vault_balance_500','closing_vault_balance_2000','total_closing_vault_balance']
header_fields = ['indent_short_code','indent_order_number','order_date','project_id','collection_date','replenishment_date','cypher_code','bank','feeder_branch','cra','indent_type']
footer_fields = ['signature_authorized_by_1_auth_signatories_signatures_id','signature_authorized_by_2_auth_signatories_signatures_id','authorized_by_1','authorized_by_2','email_indent_mail_master_id']
detail_fields = ['atm_id','general_ledger_account_number','location','purpose','loading_amount_100','loading_amount_200','loading_amount_500','loading_amount_2000','total']
path_to_wkhtmltopdf = r'C:\Program Files\wkhtmltopdf\bin\wkhtmltopdf.exe'
data_not_found = 'E500'
data_not_found_text = 'Data not found.'
success_pdf = 'S100'
success_pdf_text = 'Pdf created Successfully.'
failure_pdf = 'E100'
failure_pdf_text = 'Error in creating Pdf.'
success_mail_pdf = 'S101'
success_mail_pdf_text = 'Mail sent successfully.'
failure_mail_pdf = 'E101'
failure_mail_pdf_text = 'Pdf file not created.'
failure_maildata_pdf = 'E110'
failure_maildata_pdf_text = 'Mail data not found.'
file_not_found = 'E001'
file_not_found_error = 'Pdf File not found.'
recipients_not_found_error = 'Recipients not found to send email.'
email_body_start = 'Hello, \n \n Indent Pdf generation has been started.\n We will let you know the completion of the program thorugh email.\n\n Regards,\nEPSTAR'
email_body_end = 'Hello, \n \n Indent Pdf generation has been completed.\n Total {a} pdf generated successfully.\n\n Regards,\nEPSTAR'


add_project_bank_region_input_operation = ['file_type', 'operation', 'payload']
modify_entity = ['project','bank','region','cra']
insert_record = "Record has been successfully inserted"
insert_record_failed = "Record failed to insert"


ticket_update_params = ["ticket_id","category","sub_category","status","assign_to_user"]
success_updated = 'S102'
failure_code = 'E104'
ticket_status_open = 'open'
ticket_status_closed = 'closed'


display_data_params = ['file_type']
display_data_type = ['project', 'bank','designation', 'cra','region']
Signature_Allowed_Groups_in_auth_group = 'Signature_Allowed_Groups_in_auth_group'
improper_dropdown_datatype = 'improper_dropdown_datatype'
drop_down_data = 'Successfully extracted drop-down data'

################## Indent admin  ###########3
indent_admin_success = 'S100'
indent_admin_success_text = 'Indent Data list successfull'
indent_admin_none = ''


################### system setting parameters ################
system_settings = ['confidence_factor','buffer_percentage','denomination_wise_round_off_100',
				'denomination_wise_round_off_200','denomination_wise_round_off_500','denomination_wise_round_off_2000',
				'fixed_loading_amount','default_average_dispense','vaulting_for_normal_weekday_percentage',
				'vaulting_for_normal_weekend_percentage','vaulting_for_extended_weekend_percentage','dispenseformula',
				'deno_100_max_capacity_percentage','deno_200_max_capacity_percentage','deno_500_max_capacity_percentage',
				'deno_2000_max_capacity_percentage','deno_100_priority','deno_200_priority','deno_500_priority',
				'deno_2000_priority','deno_100_bill_capacity','deno_200_bill_capacity','deno_500_bill_capacity',
				'deno_2000_bill_capacity','cash_out_logic','forecasting_algorithm']

system_settings_success = 'S100'
system_settings_error = 'E100'
system_settings_success_text = 'System Settings saved successfully.'
system_settings_error_text = 'Unable to save System Settings.'
system_settings_approval_success = 'S101'
system_settings_approval_error = 'E101'
system_settings_approval_success_text = 'System settings approved.'
system_settings_approval_error_text = 'Unable to approve system settings.'
system_settings_reject_success = 'S110'
system_settings_reject_error = 'E110'
system_settings_reject_success_text = 'System settings rejected'
system_settings_reject_error_text = 'Unable to reject system settings.'
system_settings_list_success = 'S001'
system_settings_list_error = 'E001'
system_settings_list_success_text = 'System Settings list'
system_settings_list_error_text = 'Unable to show system settings.'
system_settings_approval_pending = 'Approval Pending'
system_settings_active = 'Active'
system_settings_history = 'History'
system_settings_data_error = 'Error'

config_menu_list = ['eod_activity', 'limitdays', 'feeder_vaulting_pre_config','cashpreavailability', 'feeder_denomination', 'fdr_casstt_max_cap_per', 'feeder_deno_pre_avail', 'bld', 'fld']

##### cash availability ###
cash_avail_file_type = "CASHPREAVAILABILITY"
cash_avail_file_type_error = 'File Type Error'
indent_pre_qualify_atm_file_type = 'INDENT_PRE_QUALIFY_ATM'
eod_activity_file_type = 'EOD_ACTIVITY'
success_cash_avail = 'Cash Availability success'
cash_success_code = 'S100'
cash_success = 'Cash Availability Successfull.'
cash_existing_code = 'E110'
uploaded_status = 'Uploaded'
failed_status = 'Failed'
cash_availability_update_status_for_rejected = 'Rejected'
cash_existing = 'Cash Availability Record in pending status not added.'
cash_error_code = 'E100'
cash_error = 'Cash Availability Unsuccessfull.'



consolidate_data = 'consolidate'
sql_consolidate_query_failed = "Query is not generating correct data-sets"

cyphercode_input_request = ['file_type', 'operation', 'payload']
file_upload_flag = 'F'

user_bank_project_params = ['operation_type','payload']
fail_to_add_records = 'E110'
status_deleted = 'Deleted'

feeder_denomination_priority_filetype = 'feeder_denomination'
format_val_success = "S100"
status_config_param = ['file_type']
data_val_success_upload = "S101"
format_val_failed_upload = "E100"
api_execution_successful = 'Successful'
api_execution_failed = 'Failed'

file_type_fdr_casstt_max_cap_per = 'fdr_casstt_max_cap_per' #for sub menu : feeder_cassette_max_capacity_percentage

#### indent pdf view request ####
success_pdf_view = 'S100'
success_pdf_view_text = 'Pdf view successful.'
error_pdf_view = 'E100'
error_pdf_view_text = 'Pdf not generated.'

file_type_feeder_deno_pre_avail = 'feeder_deno_pre_avail'

#### cash reports ###
cash_reports = ['BOMH','LVB','ALB','DENA']
cash_report_type = 'Indent Report'
load_recommendation_type = 'Loading Recommendation'
indent_summary_report_type = 'Indent Summary Report'

delete_record_failed = "E111"
all_region = 'ALL'

#### sla configuration ####
success_sla = 'S101'
failure_sla = 'E101'
file_type_pre_qualify = 'indent_pre_qualify_atm'
pre_qualify_input_param = ['menu', 'modified_records', 'status', 'created_reference_id']


##############AMS API related information######################
token_url = ' http://192.168.85.12:82/token'
params = {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Username' : 'demo@pcsinfinity.in',
				'Password' : 'Demo@1234',
				'grant_Type' : 'password',
				'client_Id' : 'eps-staging'
			}
api_data_url = "http://192.168.85.12:82/api/asset?maxResponseId="
incorrect_file_type = 'Incorrect File Type'



master_category = 'Master'
master_category_dict = {
    'AMS_ATM' : ['ATM', 'Cash Live Master file upload'],
    'ATMCNFGLIMIT' : ['ATM_CNFG_LIMITS', 'Atm Config Limit file upload'],
    'CRAFEASIBILITY' : ['CRA_FEASIBILITY', 'Cra Feasibility file upload'],
    'FEEDER' : ['FEEDER_BRANCH_MASTER','Feeder Branch Master file upload']
}
master_data_upload_subject = "Regarding the "
master_file_uploaded_by = "master_file_uploaded by"
master_data_successful_upload_message = """

Hello,
    
    File has been uploaded successfully. Kindly verify the uploaded data and approve it.
"""

master_data_upload_message_body = """
This is system generated mail.Kindly do not reply on it.

Thank You.

Regards,
EPS
"""

master_data_file_upload_time = "for the date "

file_type_bank_limitdays = 'BLD'
bank_limitdays_or_feeder__input_param = ['file_type', 'records']
file_type_feeder_limitdays = 'FLD'
feeder_vaulting_pre_config_file_type = 'feeder_vaulting_pre_config'
limitdays_file_type = 'limitdays'

feeder_vaulting_pre_config_input_param = ['file_type']
createUser_input_param = ['password', 'is_superuser', 'username', 'first_name', 'last_name', 'email', 'is_staff', 'is_active', 'date_joined']
new_user_success_message = 'New user created successfully'
user_exist_message = 'User already exists'
duplicate_data_entry = 'Duplicate data found. Remove duplicates and try again.'
### diversion status parameters ####
diversion_status = ['order_date','cra','bank','region','original_atm_id','diverted_atm_id','feeder_branch','indent_order_number','total_diversion_amount','denomination_100','denomination_200','denomination_500','denomination_2000','reasons']

success_code = 'S100'
error_code = 'E100'
upload_success_text = 'Data uploaded successfully.'
error_text = 'Unable to upload Data.'
approval_success_code = 'S101'
approval_error_code = 'E101'
approval_success_text = 'Data approved.'
approval_error_text = 'Unable to approve data.'
reject_success_code = 'S110'
reject_error_code = 'E110'
reject_success_text = 'Data rejected.'
reject_error_text = 'Unable to reject data.'
list_success_code = 'S001'
list_error_code = 'E001'
list_success_text = 'Data Loaded.'
no_data_found = 'No data to show.'
list_error_text = 'Unable to show data.'
approval_pending_status = 'Approval Pending'
approved_status = 'Approved'
data_error = 'Error'
dropdown_list_error = 'Indent number not found.'
status_code_for_duplicate_data = 'D101'
success = 'S101'
success_text = 'Data Authorized Successfully'
error = 'E103'
edit_success_code = 'S110'
edit_error_code = 'E110'
edit_success_text = 'Data updated.'
edit_error_text = 'Unable to edit data.'


#calculate morning balance, average dispense and default dispense configuration
morning_balance_type = 'Morning Balance'
average_dispense_type = 'Average Dispense'
default_dispense_type = 'Default Dispense'
dispense_type = 'Dispense'
calculation_success_code = '11001'
calculation_success = ' Calculation Successful'
calculation_error = ' Calculation Error'
calculation_no_records_code = '11003'
calculation_error_code = '11002'
no_records_error_message = '. No records found.'
error_message = ' Calculation Unsuccessfull'

withdrawal_confirmation = ['bank_code','project_id','feeder_branch','cra','indent_short_code','total_withdrawal_amount','denomination_100','denomination_200','denomination_500','denomination_2000','remarks']
historical_data_upload_file_type = 'Historical'
file_type_path_creation = 'folderpath'
history_data_directory_message = "Folder Path has been successfully created. Kindly store the history files in the above created path."
delete_withdrawal_confirmation = ['indent_short_code']
duplicate_data_entry_for_CMIS = 'Duplicate data found in CMIS sheet. Remove duplicates and try again.'
duplicate_data_entry_for_VMIS = 'Duplicate data found in VMIS sheet. Remove duplicates and try again. Check is on (Bank, CRA, Feeder Branch)'
data_issue = "Can not be approved due to some invalid record."
file_type_VB = 'VB'
VB_input_param = ['file_type','payload']
error_inserting_record = 'E112'
internal_server_error = 'E113'
invalid_request_code = 'E114'
operation_menu_list = ['vb']
indent_order_number_dropdown = 'ion'	#indent order number
ion_related_info = 'info'
active_atm_id = "Atm id for respective indent order number doesn't exist."
inactive_indent_order_number ="indent order number doesn't exist or it is not in active state."
improper_data = 'Improper data'
failed_to_generate_pdf = "E114"
revision_via_file_upload_file_type = 'atm_list'

withdrawal_not_available="Withdrawal Slip Not Available. Try again!!!"

#### dispense calculation code ###
morning_t_minus_1_missing_code = '60051'
morning_t_minus_1_missing_text = 'Morning balance of previous day not found.'
morning_t_minus_1_error_code = '60052'
morning_t_minus_1_error_text = 'Error in morning balance of previous day.'
morning_t_minus_2_missing_code = '60053'
morning_t_minus_2_missing_text = 'Morning balance of day before previous day not found.'
morning_t_minus_2_error_code = '60054'
morning_t_minus_2_error_text = 'Error in morning balance of day before previous day.'
C3R_entry_missing_code = '60055'
C3R_entry_missing_text = 'C3R entry not found.'
C3R_error_code = '60056'
C3R_error_text = 'Error in C3R'
dispense_success_code = '60057'
dispense_success_text = 'Dispense Calculated Successfully.'
dispense_error_code = '60058'
dispense_error_text = 'No records inserted.'

failed_to_generate_pdf_save_send = 'E115'
success_edit_save_send = 'S109'
sheet_name_error = 'Sheet name \'Compile Cash Live Master\' is not available, Kindly attach mentioned sheet containing cash live master data.'

atm_revision_cash_availability_file_type = 'Atm_Revision_Cash_Availability'


#### indent holiday code ####
hoiday_success_code = '70052'
hoiday_error_code = '70051'
hoiday_success_text = 'Indent Holiday calculation successful.'
hoiday_error_text = 'Indent Holiday calculation failed.'

incoming_date_issue = 'Date inside file and date entered is not matching. Both date should match to insert the records successfully.'

indent_mstr_args_list = ['indent_order_number','order_date','collection_date','replenishment_date','cypher_code','bank','feeder_branch','cra','total_atm_loading_amount_50','total_atm_loading_amount_100','total_atm_loading_amount_200','total_atm_loading_amount_500','total_atm_loading_amount_2000','total_atm_loading_amount','cra_opening_vault_balance_50','cra_opening_vault_balance_100','cra_opening_vault_balance_200','cra_opening_vault_balance_500','cra_opening_vault_balance_2000','cra_opening_vault_balance','closing_vault_balance_50','closing_vault_balance_100','closing_vault_balance_200','closing_vault_balance_500','closing_vault_balance_2000','total_closing_vault_balance','total_bank_withdrawal_amount_50','total_bank_withdrawal_amount_100','total_bank_withdrawal_amount_200','total_bank_withdrawal_amount_500','total_bank_withdrawal_amount_2000','total_bank_withdrawal_amount','authorized_by_1','authorized_by_2','signature_authorized_by_1_auth_signatories_signatures_id','signature_authorized_by_2_auth_signatories_signatures_id','email_indent_mail_master_id','notes','amount_in_words','indent_type','project_id','record_status','created_on','created_by','created_reference_id','indent_counter','previous_indent_code','is_manually_updated','indent_short_code','total_atms','total_cashout_atms']
indent_detail_args_list = ['sr_no','atm_id','total','loading_amount_50','loading_amount_100','loading_amount_200','loading_amount_500','loading_amount_2000','location','purpose','indent_counter','previous_indent_code','indent_type','is_manually_updated','general_ledger_account_number','created_on','created_by','created_reference_id']
distri_mstr_args_list = ['max_loading_amount','max_loading_amount_100','max_loading_amount_200','max_loading_amount_500','max_loading_amount_2000','min_loading_amount','forecast_loading_amount','forecast_loading_amount_100','forecast_loading_amount_200','forecast_loading_amount_500','forecast_loading_amount_2000','total_cassette_capacity','vaultingamount_50','vaultingamount_100','vaultingamount_200','vaultingamount_500','vaultingamount_2000']
distri_detail_args_list = ['bank_decide_limit_days','bank_loading_limit_days','feeder_decide_limit_days','feeder_loading_limit_days','total_capacity_amount_50','total_capacity_amount_100','total_capacity_amount_200','total_capacity_amount_500','total_capacity_amount_2000','morning_balance_100','morning_balance_200','morning_balance_500','morning_balance_2000','total_morning_balance','avgdispense','insurance_limit','bank_cash_limit']

general_ledger_account_number_file_type = 'General_Ledger_Account_Number'

indent_holiday_type = 'Indent_Holiday'

indent_holiday_success_code = '70055'
indent_holiday_success_text = 'Data validation successful'
indent_holiday_error_code = '70054'
indent_holiday_error_text = 'Data validation error'
indent_holiday_error_name_not_found_code = '70053'
indent_holiday_error_name_not_found_text = 'Holiday not found in holiday list table'

menu_perm_delete_success_code = 'S106'