from Common.CommonFunctions import common_util
import logging
from Common import masterconf, db_queries_properties
import pandas as pd
import pyodbc

class IndentSummaryReportController():

    def handle_indent_summary_report_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_indent_summary_report_req METHOD OF IndentSummaryReportController.")
        # status_dict ={}
        response_list = list()
        try:
            #### query to get report of specific bank on specific date #####
            logger.info('Request Parameters received: %s', request.data)

            record_status = None;
            indent_date = None;
            bank = None;

            if 'record_status' in request.data:
                record_status = request.data['record_status'];
            if 'indent_date' in request.data:
                indent_date = request.data['indent_date'];
            if 'bank' in request.data:
                bank = request.data['bank'];

            sql_query = db_queries_properties.indent_summary_report_query % (record_status,indent_date,bank)
            sql_query = sql_query.replace("'None'","Null");
            sql_query = sql_query.replace('"None"','Null');
            print(sql_query)
            df = pd.read_sql(sql_query, pyodbc.connect(masterconf.connection_string)).fillna('')
            if 'export' in request.data:
                response_list = df
            else:
                response_list = common_util_obj.pandas_to_dict(df)

            print(len(response_list))
            return response_list

        except Exception as e:
            logger.error('EXCEPTION IN handle_indent_summary_report_req METHOD OF IndentSummaryReportController', exc_info=True)
            logger.error(e)
            return response_list
