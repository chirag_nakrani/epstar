from Common.CommonFunctions import common_util
import logging
from Common import masterconf, db_queries_properties
import pandas as pd
import pyodbc

class CashReportsController():

    def handle_cash_reports_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_cash_reports_req METHOD OF CashReportsController.")
        # status_dict ={}
        grouped_list = list()
        try:
            logger.info('Received request parameters: %s',request.data)
            #### query to get report of specific bank on specific date #####
            sql_query = db_queries_properties.cash_reports_query[request.data['bank']] % (request.data['bank'],request.data['date'])
            logger.info('SQL query to fetch data: %s',sql_query)
            df = pd.read_sql(sql_query, pyodbc.connect(masterconf.connection_string)).fillna('')
            if request.data['bank'] == masterconf.cash_reports[0]:
                grouped_list = common_util_obj.cash_reports_bomh(df,request)
            else:
                if 'export' in request.data:
                    grouped_list = df
                else:
                    grouped_list = common_util_obj.pandas_to_dict(df)

            logger.info('Returned response list length: %s',len(grouped_list))
            return grouped_list

        except Exception as e:
            logger.error('EXCEPTION IN handle_cash_reports_req METHOD OF CashReportsController', exc_info=True)
            logger.error(e)
            return grouped_list

    def handle_loading_recommendation_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_loading_recommendation_req METHOD OF CashReportsController.")
        # status_dict ={}
        grouped_list = list()
        try:
            logger.info('Received request parameters: %s', request.data)
            #### query to get loading recommendation of specific bank on specific date #####
            sql_query = db_queries_properties.loading_recommendations_query % (request.data['bank'],request.data['date'])
            logger.info('SQL query to fetch data: %s', sql_query)
            df = pd.read_sql(sql_query, pyodbc.connect(masterconf.connection_string)).fillna('')
            if 'export' in request.data:
                grouped_list = df
            else:
                grouped_list = common_util_obj.pandas_to_dict(df)

            logger.info('Returned response list length: %s', len(grouped_list))
            return grouped_list

        except Exception as e:
            logger.error('EXCEPTION IN handle_loading_recommendation_req METHOD OF CashReportsController', exc_info=True)
            logger.error(e)
            return grouped_list

