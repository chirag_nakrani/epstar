from Common.CommonFunctions import common_util
import logging
from datetime import datetime
import schedule
import time
from datetime import timedelta
from Model.models import workflow_master,ticket_master,ticket_category_master, scheduler_config, generate_ref_id
from Common.models import data_update_log
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from Common import masterconf
from django.contrib.auth.models import User

class FileUploadScheduleController:
    # current_datetime = datetime.strptime('2019-02-13 14:47:00.000', '%Y-%m-%d %H:%M:%S.%f')
    # current_datetime = datetime.strptime(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')
    ### schedule file check request ###
    def handle_schedule_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_schedule_req METHOD OF ScheduleController.")
        try:
            current_datetime = datetime.strptime(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M'), '%Y-%m-%d %H:%M')
            data_list = list()
            # current_time = datetime.strftime(datetime.strptime('12:00:00', '%H:%M:%S'),'%H:%M:%S')
            # current_datetime = datetime.now()
            list_schedule = list(scheduler_config.objects.values())
            # current_time = datetime.strptime(datetime.strftime(datetime.now(), '%H:%M'), '%H:%M').time()
            current_time = datetime.now().time().strftime('%H:%M')
            if list_schedule != []:
                for data in list_schedule:
                    hour_frequency = data['hourly_frequency'].split(',')
                    if current_time in hour_frequency:
                        try:
                            data_update_log_entry = data_update_log.objects.filter(
                                datafor_date_time=current_datetime,bank_code=data['bank_code'],project_id=data['project_id'],region=data['region'],
                                data_for_type=data['file_type']).values()
                            if not data_update_log_entry:
                                data_list.append(data)
                                try:
                                    message = MIMEMultipart()
                                    message['From'] = data['from_email']
                                    message['To'] = data['number_1st_escalation']
                                    message['Subject'] = 'File not received'
                                    body_text = 'Hello,\n File not received for bank %s, project %s, region %s, for file type %s for datetime %s' % \
                                                (data['bank_code'],data['project_id'],data['region'],data['file_type'],current_datetime)
                                    message.attach(MIMEText(body_text))
                                    try:
                                        #### smtp configuration for mail ####
                                        server = smtplib.SMTP()
                                        server.connect(masterconf.MAILHOST, masterconf.MAILPORT)
                                        server.starttls()
                                        server.login(masterconf.mail_sender, masterconf.mail_password)
                                        recipients = []
                                        recipients.append(data['number_1st_escalation'])
                                        server.sendmail(masterconf.mail_sender, recipients, message.as_string())
                                        logger.setLevel(logging.INFO)
                                        logger.info("MAIL SENT.")
                                        server.close()
                                    except Exception as e:
                                        raise e
                                except Exception as e:
                                    raise e
                        except Exception as e:
                            raise e
            else:
                response_obj = {}
                response_obj['message'] = 'Data not found'
                return response_obj

            # print (datetime.strptime(datetime.strftime(current_plus_wait_time,'%Y-%m-%d %H:%M:%S'),'%Y-%m-%d %H:%M:%S'))
            # exit()
            if data_list != []:
                for wait_data in data_list:
                    # wait_time = datetime.strptime(datetime.strftime(wait_data['wait_time'], '%H:%M:%S'), '%H:%M:%S').time()
                    wait_minutes = int(wait_data['wait_time'].split(':')[0])
                    wait_time_convert = current_datetime + timedelta(minutes = wait_minutes)
                    wait_time = datetime.strftime(wait_time_convert, '%H:%M')
                    # schedule.every().day.at(wait_time).do(lambda: self.job(self.data_list, request)).tag(
                    #     'schedule-job')
                    schedule.every().day.at(wait_time).do(lambda: self.job(data_list, request, current_datetime)).tag(
                        'schedule-job')
                print(schedule.jobs)
                while True:
                    if schedule.jobs != []:
                        schedule.run_pending()
                    else:
                        break
                # self.job(self.data_list,request)

                response_list = []
                for d in data_list:
                    response_obj = {}
                    response_obj['project_id'] = d['project_id']
                    response_obj['bank_code'] = d['bank_code']
                    response_obj['region'] = d['region']
                    response_obj['date_time'] = current_datetime
                    response_obj['file_type'] = d['file_type']
                    response_obj['message'] = 'File not received'
                    response_list.append(response_obj)
                if response_list != []:
                    return response_list
                else:
                    response_obj = {}
                    response_obj['message'] = 'All files received.'
                    return response_obj
            else:
                response_obj = {}
                response_obj['message'] = 'All files received no scheduler running.'
                return response_obj
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_schedule_req METHOD OF ScheduleController', exc_info=True)
            raise e

    def job(self, data, request, current_datetime):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        for schedule_data in data:
            data_update_log_entry = data_update_log.objects.filter(
                datafor_date_time=current_datetime, bank_code=schedule_data['bank_code'], project_id=schedule_data['project_id'],
                region=schedule_data['region'],
                data_for_type=schedule_data['file_type']).values()
            if data_update_log_entry:
                data.remove(schedule_data)
            else:
                category = 'Master'
                subcategory = 'ATM'
                admin_user = None
                category_id = ticket_category_master.objects.filter(category=category,
                                                                    sub_category=subcategory).values('category_id')
                cat_id = category_id[0]['category_id']
                is_user_for_cat_id = workflow_master.objects.filter(category_id=cat_id).exists()
                if is_user_for_cat_id:
                    assigned_to_user = workflow_master.objects.filter(category_id=cat_id).values('assigned_to_user')[0][
                        'assigned_to_user']
                    assigned_to_group = \
                    workflow_master.objects.filter(category_id=cat_id).values('assigned_to_group')[0][
                        'assigned_to_group']
                else:
                    cat_id = ''
                    admin_user = User.objects.get(is_superuser=1)
                    assigned_to_user = admin_user.id
                    assigned_to_group = ''  # Group.objects.get(user = assigned_to_user).id

                data_dict = {
                    'subject': 'File not received',
                    'category_id': cat_id,
                    'status': masterconf.ticket_status_open,  # Status Updated
                    'assign_to_user': assigned_to_user,
                    'assign_to_group': assigned_to_group,
                    'created_on': current_datetime,
                    'created_by': admin_user.username,
                    'created_reference_id': generate_ref_id(),
                    'remarks': '%s file not received for %s bank code, %s project id and %s region' % (schedule_data['file_type'], schedule_data['bank_code'], schedule_data['project_id'], schedule_data['region']),
                    'record_status': masterconf.active_status  # Record Status Added
                }
                tckt_mstr = ticket_master(**data_dict)
                tckt_mstr.save()
                try:
                    message = MIMEMultipart()
                    message['From'] = schedule_data['from_email']
                    message['To'] = schedule_data['number_2nd_escalation']
                    message['Subject'] = 'File not received'
                    body_text = 'Hello,\n File not received for bank %s, project %s, region %s, for file type %s for datetime %s.\n Ticket Id: %s' % \
                                (schedule_data['bank_code'], schedule_data['project_id'], schedule_data['region'], schedule_data['file_type'], current_datetime,tckt_mstr.ticket_id)
                    message.attach(MIMEText(body_text))
                    try:
                        #### smtp configuration for mail ####
                        server = smtplib.SMTP()
                        server.connect(masterconf.MAILHOST, masterconf.MAILPORT)
                        server.starttls()
                        server.login(masterconf.mail_sender, masterconf.mail_password)
                        recipients = []
                        recipients.append(schedule_data['number_2nd_escalation'])
                        server.sendmail(masterconf.mail_sender, recipients, message.as_string())
                        logger.setLevel(logging.INFO)
                        logger.info("MAIL SENT.")
                        server.close()
                    except Exception as e:
                        raise e
                except Exception as e:
                    raise e
        schedule.clear('schedule-job')
        return True
