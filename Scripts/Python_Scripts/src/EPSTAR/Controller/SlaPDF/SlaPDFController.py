import logging
from Common import masterconf
from Common.CommonFunctions import common_util
from EPSTAR import settings
import datetime
import os
from Model.models import sla
from datetime import timedelta

class SlaPDFController():

    ### sla pdf save request ###
    def handle_pdf_save_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_pdf_save_req METHOD OF SlaPDFController.")
        ## current date
        date_now = datetime.datetime.now().strftime('%d-%m-%Y')
        response_obj = {}
        username = request.userinfo['username'].username
        uploaded_status = 'Approval Pending'
        file_type = request.POST['file_type'].upper()
        upload_time = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        reference_id = request.userinfo['reference_id']
        try:
            pdf_file_dir = settings.BASE_DIR + '\FileFolder\SlaPDFFolder\%s' % date_now
            if not os.path.exists(pdf_file_dir):
                os.makedirs(pdf_file_dir)
            input_file = request.data['file_name']
            file_name = '%s\%s' % (pdf_file_dir, request.data['file_name'])
            output_file = open(file_name, 'wb')
            input_file.seek(0)
            while 1:
                data = input_file.read()
                if not data:
                    break
                output_file.write(data)
            output_file.close()
            sla_dict = {}
            sla_dict['cra'] = request.data['cra']
            sla_dict['file_name'] = file_name
            sla_dict['record_status'] = uploaded_status
            sla_dict['created_on'] = upload_time
            sla_dict['created_by'] = username
            sla_dict['created_reference_id'] = reference_id
            sla_create = sla(**sla_dict)
            sla_create.save()
            response_obj['status_code'] = masterconf.success_sla
            response_obj['status_text'] = masterconf.api_status_Success
        except Exception as e:
            response_obj['status_code'] = masterconf.failure_sla
            response_obj['status_text'] = masterconf.api_status_Failure

        return response_obj

    ### sla pdf delete request ###
    def handle_pdf_delete_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_pdf_delete_req METHOD OF SlaPDFController.")
        ## current date
        deleted_time = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        response_obj = {}
        username = request.userinfo['username'].username
        record_status = 'History'
        reference_id = request.userinfo['reference_id']
        try:
            sla_delete = sla.objects.get(id=request.data['id'])
            delete_file = sla_delete.file_name
            os.system('rm %s' % delete_file)
            sla_delete.record_status = record_status
            sla_delete.deleted_on = deleted_time
            sla_delete.deleted_by = username
            sla_delete.deleted_reference_id = reference_id
            sla_delete.save()
            response_obj['status_code'] = masterconf.success_sla
            response_obj['status_text'] = masterconf.api_status_Success
        except Exception as e:
            response_obj['status_code'] = masterconf.failure_sla
            response_obj['status_text'] = masterconf.api_status_Failure

        return response_obj

    ### sla pdf approve or reject request ###
    def handle_pdf_approve_reject_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_pdf_approve_reject_req METHOD OF SlaPDFController.")
        ## current date
        approved_time = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        response_obj = {}
        username = request.userinfo['username'].username
        record_status = request.data['record_status']
        reference_id = request.userinfo['reference_id']
        try:
            sla_delete = sla.objects.get(id=request.data['id'])
            sla_delete.record_status = record_status
            sla_delete.deleted_on = approved_time
            sla_delete.deleted_by = username
            sla_delete.deleted_reference_id = reference_id
            sla_delete.save()
            response_obj['status_code'] = masterconf.success_sla
            response_obj['status_text'] = masterconf.api_status_Success
        except Exception as e:
            response_obj['status_code'] = masterconf.failure_sla
            response_obj['status_text'] = masterconf.api_status_Failure

        return response_obj

    ### sla pdf list request ###
    def handle_pdf_list_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_pdf_list_req METHOD OF SlaPDFController.")
        ## current date
        response_obj = {}
        try:
            response_list = sla.objects.filter(record_status=request.data['record_status']).values()
            return response_list

        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_pdf_list_req METHOD OF SlaPDFController', exc_info=True)
            raise e