from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
import json

class CsrfExemptSessionAuthentication(SessionAuthentication):
	def enforce_csrf(self, request):
		return  # To not perform the csrf check previously happening

class common_bankfeeder_filterinfo():
	def handle_bankname_request(self,request_data,username):
		common_util_obj = common_util.common_utils()
		data_info = {}
		try:
			if request_data['file_type'].lower() == (masterconf.file_type_cbr_api).lower():
				sql_cbr_query = "select distinct(user_bank_mapping.bank_id) from user_bank_mapping left join project_bank_feeder_cra_mapping on user_bank_mapping.bank_id = project_bank_feeder_cra_mapping.bank_id where user_bank_mapping.user_name = '" +username+"' and iscbr = 'Y'"
				cursor = connection.cursor()
				cursor.execute(sql_cbr_query)
				c = cursor.fetchall()
				cbr_bank_list = []
				for i in range(len(c)):
					for j in range(len(c[i])):
						cbr_bank_list.append(c[i][j])
				sql_query_feeder_list = "select distinct(project_bank_feeder_cra_mapping.Feeder) from project_bank_feeder_cra_mapping right join user_bank_mapping on project_bank_feeder_cra_mapping.bank_id = user_bank_mapping.bank_id where user_bank_mapping.user_name = '" +username+"' and iscbr = 'Y'"
				cursor = connection.cursor()
				cursor.execute(sql_query_feeder_list)
				c1 = cursor.fetchall()
				cbr_feeder_list = []
				for i in range(len(c1)):
					for j in range(len(c1[i])):
						cbr_feeder_list.append(c1[i][j])
				#print (cbr_feeder_list)
				data_info['bank'] = cbr_bank_list
				data_info[masterconf.feederlist] = cbr_feeder_list
				data_info['api_status'] = masterconf.api_status_Success
				return data_info
			elif request_data['file_type'].lower() == (masterconf.file_type_dispense_api).lower():
				sql_dispense_query = "select distinct(user_bank_mapping.bank_id) from user_bank_mapping left join project_bank_feeder_cra_mapping on user_bank_mapping.bank_id = project_bank_feeder_cra_mapping.bank_id where user_bank_mapping.user_name = '" +username+"' and isdispense = 'Y'"
				cursor = connection.cursor()
				cursor.execute(sql_dispense_query)
				c = cursor.fetchall()
				disp_bank_list = []
				for i in range(len(c)):
					for j in range(len(c[i])):
						disp_bank_list.append(c[i][j])
				sql_query_feeder_list = "select distinct(project_bank_feeder_cra_mapping.Feeder) from project_bank_feeder_cra_mapping right join user_bank_mapping on project_bank_feeder_cra_mapping.bank_id = user_bank_mapping.bank_id where user_bank_mapping.user_name = '" +username+"' and isdispense = 'Y'"
				cursor = connection.cursor()
				cursor.execute(sql_query_feeder_list)
				c1 = cursor.fetchall()
				disp_feeder_list = []
				for i in range(len(c1)):
					for j in range(len(c1[i])):
						disp_feeder_list.append(c1[i][j])
				data_info['bank'] = disp_bank_list
				data_info[masterconf.feederlist] = disp_feeder_list
				data_info['api_status'] = masterconf.api_status_Success
				return data_info
			elif request_data['file_type'].lower() == (masterconf.file_type_vcb_api).lower():
				sql_vcb_query = "select distinct(CRA) from project_bank_feeder_cra_mapping"
				cursor = connection.cursor()
				cursor.execute(sql_vcb_query)
				v = cursor.fetchall()
				VCBproject_id_list = []
				VCBproject_id_dict = dict()
				for i in range(len(v)):
					for j in range(len(v[i])):
						k = v[i][j]
						VCBproject_id_list.append(k)
				data_info['bank'] = VCBproject_id_list
				data_info['api_status'] = masterconf.api_status_Success
				return data_info
			elif request_data['file_type'].lower() == (masterconf.file_type_dlr_api).lower():
				sql_dlr_query = "select distinct(project_id) from project_bank_feeder_cra_mapping"
				cursor = connection.cursor()
				cursor.execute(sql_dlr_query)
				v = cursor.fetchall()
				dlrproject_id__list = []
				dlrproject_id_dict = dict()
				for i in range(len(v)):
					for j in range(len(v[i])):
						dlrproject_id__list.append(v[i][j])
				# for i in dlrproject_id__list:
				# 	dlrproject_id_dict[i] = i
				data_info['bank'] = dlrproject_id__list
				data_info['api_status'] = masterconf.api_status_Success
				return data_info
			elif request_data['file_type'].lower() == (masterconf.file_type_c3r_api).lower():
				sql_c3r_query = "select distinct(CRA) from project_bank_feeder_cra_mapping "
				cursor = connection.cursor()
				cursor.execute(sql_c3r_query)
				v = cursor.fetchall()
				c3r_cra_list = []
				for i in range(len(v)):
					for j in range(len(v[i])):
						c3r_cra_list.append(v[i][j])
				#c3r_cra_dict = dict()
				# for i in c3r_cra_list:
				# 	c3r_cra_dict[i] = i
				data_info['bank'] = c3r_cra_list
				data_info['api_status'] = masterconf.api_status_Success
				return data_info
			else:
				data_info['filetype'] = masterconf.file_type_error
				data_info['api_status'] = masterconf.api_status_Failure
				return data_info
		except Exception as e:
			#print ("Exception from BanknameController : {}".format(e))
			raise e
