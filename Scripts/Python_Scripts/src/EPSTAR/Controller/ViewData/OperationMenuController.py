
from django.http import HttpResponse
import os
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import time, datetime
from Common import masterconf
import json

from Common.models import data_update_log_master

class CsrfExemptSessionAuthentication(SessionAuthentication):
	def enforce_csrf(self, request):
		return  # To not perform the csrf check previously happening

class OperationMenuController():
	def handle_view_request(self,request):
		print ("Inside in OperationMenuController")
		menu_type = str(request.data['menu']).upper()
#########################Extracting uploaded files from data_update_log_master, according to record status from atm_master table ###########################
		model_name = data_update_log_master
		filters_param = request.data['record_status']
		data = []
		status_dict = {}
		try:
			for i in request.data['record_status']:#to find the no of files uplaoded into tables
				data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
				for i in data_list:
					data.append(i)#to collect all the files uploaded in their respective tables
			if len(data) >= 0:
				status_dict["data"] = data
				status_dict["status_text"] = masterconf.masterApiSuccess
				return status_dict
			else:
				status_dict["data"] = []
				status_dict["status_text"] = masterconf.masterApiFailed
				return status_dict
		except Exception as e:
			print ("Exception from OperationMenuController : {}".format(e))
			raise e
