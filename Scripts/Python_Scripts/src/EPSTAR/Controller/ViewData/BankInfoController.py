from rest_framework.authentication import SessionAuthentication
from Common import masterconf
from django.db import connection
import json
from Model.models import CRA_master
import pandas as pd

class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


class BankInfoController ( ) :
    def handle_bankname_request ( self , request_data , username ) :
        data_info = { }
        print ("Inside handle_bankname_request :")
        print ("request_data['file_type'].lower ( ) :",request_data['file_type'].lower ( ))
        print ("(masterconf.file_type_cbr_api).lower ( ) : ", (masterconf.file_type_cbr_api).lower ( ))
        try :
            if request_data['file_type'].lower ( ) == (masterconf.file_type_cbr_api).lower ( ) :
                sql_cbr_query = "select  project.project_id,bank.bank_code as bank_id,feeder.region, feeder.feeder from user_bank_project_mapping bank JOIN user_bank_project_mapping project   on bank.user_id = project.user_id and bank.bank_code = project.bank_code and bank.project_id = project.project_id inner join project_bank_feeder_cra_mapping feeder on feeder.bank_id  = bank.bank_code and feeder.project_id = project.project_id where bank.user_id = (select id from auth_user where username = '" + username + "' AND is_active = 1) and iscbr = 'Y' order by project_id"
                data = pd.read_sql(sql_cbr_query, connection)
                data = data.fillna('')
                data_list = self.getBankList(data)
                if data_list != []:
                    data_info['data'] = data_list
                    data_info['api_status'] = masterconf.api_status_Success
                return data_info
            elif request_data['file_type'].lower ( ) == (masterconf.file_type_dispense_api).lower ( ) :
                sql_dispense_query = "select project.project_id,bank.bank_code as bank_id,feeder.region, feeder.feeder from user_bank_project_mapping bank JOIN user_bank_project_mapping project on bank.user_id = project.user_id and bank.bank_code = project.bank_code and bank.project_id = project.project_id inner join project_bank_feeder_cra_mapping feeder on feeder.bank_id  = bank.bank_code and feeder.project_id = project.project_id where bank.user_id = (select id from auth_user where username = '" + username + "' ) and isdispense = 'Y' order by project_id"
                data = pd.read_sql(sql_dispense_query, connection)
                data = data.fillna('')
                data_list = self.getBankList(data)
                print(data_list)
                if data_list != []:
                    data_info['data'] = data_list
                    data_info['api_status'] = masterconf.api_status_Success
                return data_info
            elif request_data['file_type'].lower ( ) == (masterconf.file_type_vcb_api).lower ( ) :
                data_list = CRA_master.objects.filter ( record_status=masterconf.active_status ).values ( 'cra_id' )
                data_info['data'] = data_list
                data_info['api_status'] = masterconf.api_status_Success
                return data_info
            elif request_data['file_type'].lower ( ) == (masterconf.file_type_dlr_api).lower ( ) :
                data_list = CRA_master.objects.filter ( record_status=masterconf.active_status ).values ( 'cra_id' )
                data_info['data'] = data_list
                data_info['api_status'] = masterconf.api_status_Success
                return data_info
            elif request_data['file_type'].lower ( ) == (masterconf.file_type_c3r_api).lower ( ) :
                data_list = CRA_master.objects.filter ( record_status=masterconf.active_status ).values ( 'cra_id' )
                data_info['data'] = data_list
                data_info['api_status'] = masterconf.api_status_Success
                return data_info
            else :
                data_info['filetype'] = masterconf.data_info
                data_info['api_status'] = masterconf.api_status_Success
                return data_info
        except Exception as e :
            data_info['filetype'] = []
            data_info['api_status'] = masterconf.api_status_Success
            print(data_info)
            return data_info

    def getBankList(self, data):
        try:
            data_list = []
            for project in pd.unique(data['project_id'].values):
                project_dataframe = pd.DataFrame()
                project_dataframe = data.loc[data['project_id'] == project]
                project_dict = {}
                project_dict['project_id'] = project
                bank_list = []
                for bank in pd.unique(project_dataframe['bank_id'].values):
                    bank_dict = {}
                    bank_dict['bank_id'] = bank
                    bank_list.append(bank_dict)
                    bank_dataframe = pd.DataFrame()
                    bank_dataframe = project_dataframe.loc[project_dataframe['bank_id'].values == bank]
                    feeder_list = []
                    region_list = []
                    for i,r in bank_dataframe.iterrows():
                        feeder_record = {}
                        feeder_record['feeder'] = r['feeder']
                        feeder_list.append(feeder_record)
                    for region in pd.unique(bank_dataframe['region'].values):
                        region_record = {}
                        region_record['region'] = region
                        region_list.append(region_record)
                    bank_dict['feeder'] = feeder_list
                    bank_dict['region'] = region_list
                project_dict['bank'] = bank_list
                data_list.append(project_dict)
            return data_list
        except Exception as e:
            print(e)