from Common.CommonFunctions import common_util
from Common import masterconf
from Common import db_queries_properties
from django.apps import apps
import logging
from django.db import connection
import pandas as pd


####################This API is used for Viewing the data from table of Routine files#########################
class ViewController ( ) :
    def handle_view_request ( self , request ) :
        print ("Inside ViewController")
        common_util_obj = common_util.common_utils ( )
        logger = common_util_obj.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "In handle_view_request of ViewController." )
        strfile_type = str ( request.data['file_type'] ).upper ( )
        print ("strfile_type :" , strfile_type)
        if str ( request.data['file_type'] ).upper ( ) == masterconf.file_type_ims.upper ( ) or str ( request.data['file_type'] ).upper ( ) == masterconf.file_type_loading_confirm.upper ( ):
            strbank_code = ''
        else :
            strbank_code = str ( request.data['bank_code'] ).upper ( )
        print ("strbank_code :" , strbank_code)
        empty_list = list ( )
        list_returned = list ( )
        table_name_key = strfile_type + "_" + strbank_code + "_TABLE"
        print ("table_name_key :" , table_name_key)
        column_names_key = strfile_type + "_" + strbank_code
        print ("column_names_key :" , column_names_key)
        model_name_str = db_queries_properties.table_dict[table_name_key]
        print ("model_name_str : " , model_name_str)
        model_name = apps.get_model ( app_label='Model' , model_name=model_name_str )
        print ("model_name : " , model_name)
        if strbank_code.lower ( ) == 'all' :
            bank_list = request.userinfo['bank_list']
            project_list = request.userinfo['project_list']
            if 'filters' in request.data :
                filters_param = request.data['filters']
                if 'date_range' in request.data:
                    list_db = model_name.objects.filter(datafor_date_time__range=[request.data['date_range'][0],
                                                                                  request.data['date_range'][
                                                                                      1]]).filter(**filters_param).values()
                else:
                    list_db = model_name.objects.filter ( project_id__in=project_list ).filter (
                        bank_name__in=bank_list ).filter ( **filters_param ).values ( )
            else :
                list_db = model_name.objects.filter ( project_id__in=project_list ).filter (
                    bank_name__in=bank_list ).values ( )
        else :
            if 'filters' in request.data :
                filters_param = request.data['filters']
                if str(request.data['file_type']).upper() == masterconf.file_type_ims.upper() and 'date_range' in request.data:
                    list_db = model_name.objects.filter(
                        message_date_time__range=[request.data['date_range'][0], request.data['date_range'][1]]).filter(
                        **filters_param).values()
                else:
                    list_db = model_name.objects.filter ( **filters_param ).values ( )
            else :
                list_db = model_name.objects.all ( ).values ( )
        if 'required_fields' in request.data.keys ( ) :
            print("In required_fields")
            required_fields_arr = request.data['required_fields']
            for i in range ( 0 , len ( list_db ) ) :
                inner_obj = { key : list_db[i][key] for key in required_fields_arr }
                list_returned.append ( inner_obj )
            return list_returned
        else :
            return list_db


#####################This API is used for viewing the data from master menu###########################################
class ViewControllerMaster ( ) :
    def handle_view_request ( self , request ) :
        common_util_obj = common_util.common_utils ( )
        logger = common_util_obj.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "In handle_view_request of ViewControllerMaster" )
        menu_type = str ( request.data['menu'] ).upper ( )
        table_name_key = menu_type + "_TABLE"
        model_name_str = db_queries_properties.all_table_dict[table_name_key]
        if type(model_name_str) == list and menu_type:
            model_name_list = list()
            for modelname in model_name_str:
                model_name_list.append(apps.get_model(app_label='Model', model_name=modelname))
        else:
            model_name = apps.get_model(app_label='Model', model_name=model_name_str)
        print ("Inside ViewControllerMaster")
        list_db = list()
        menu_list = [masterconf.ams_master.lower ( ) , masterconf.file_type_feeder.lower ( ) ,
                     masterconf.file_type_RD_brand_BillCapacity.lower ( ) , masterconf.file_type_Empaneled.lower ( ) ,
                     masterconf.file_type_vault_master.lower ( ) , masterconf.file_type_default_uploading.lower ( ) ,
                     masterconf.file_type_mail_master.lower ( ) , masterconf.file_type_signature.lower ( ),
                      masterconf.file_type_cypher_code.lower ( )]
        #############this below comparison is made separately because in the mentioned menu's, there is no data validation so is_valid_record column is NULL there
        if menu_type.lower ( ) in menu_list or menu_type.upper() in masterconf.file_type_escalation_matrix:

            if 'filter_data' in request.data :
                filters_param = request.data["filter_data"]
                if 'date_range' in request.data:
                    list_db = model_name.objects.filter(created_on__range=[request.data['date_range'][0],request.data['date_range'][1]]).filter ( **filters_param ).values ( )
                else:
                    list_db = model_name.objects.filter ( **filters_param ).values ( )
        elif menu_type.lower ( ) in masterconf.config_menu_list :
            if 'filter_data' in request.data :
                filters_param = request.data["filter_data"]
                list_db = model_name.objects.filter ( **filters_param ).values ( )
            # else:
            # 	list_db = model_name.objects.filter(created_reference_id = created_reference_id).values()
        elif menu_type.lower ( ) in masterconf.operation_menu_list :
            if 'filter_data' in request.data :
                filters_param = request.data["filter_data"]
                list_db = model_name.objects.filter ( **filters_param ).values ( )
        ##############this is done because data validation is done on them, so is_valid_record column is maintained to handle them accordingly#########################
        elif menu_type.lower ( ) == masterconf.file_type_holiday_master.lower ( ) :
            print("inside holidaymaster")
            if 'filter_data' in request.data :
                filters_param = request.data["filter_data"]
                record_status = filters_param["record_status"]
                if record_status == masterconf.pending_status :
                    created_reference_id = filters_param["created_reference_id"]
                    sql_query = """
                    select l.holiday_code,l.holiday_name,s.State,d.start_date,d.end_date,
                    CASE WHEN
                            (l.record_status <>  d.record_status)
							THEN d.record_status
                         WHEN  (l.record_status <> s.record_status)
                            THEN s.record_status
						ELSE
							l.record_status
                    END as record_status,
                    CASE WHEN
                            l.created_reference_id <> d.created_reference_id
							THEN d.created_reference_id
                         WHEN  l.created_reference_id <> s.created_reference_id
                            THEN s.created_reference_id
                            ELSE
                            l.created_reference_id
                            END AS created_reference_id,
                    CASE WHEN
                            l.created_on <> d.created_on
                          THEN
							d.created_on
						WHEN l.created_on <> s.created_on
                            THEN s.created_on
                        ELSE l.created_on
                           END as created_on,
                    CASE WHEN
                        l.created_by <> d.created_by
						THEN d.created_by
                        WHEN l.created_by <> s.created_by
                        THEN s.created_by
                        ELSE l.created_by
                        END as created_by,
                    case when
                        l.approved_on <> ISNULL(d.approved_on,0)
						THEN d.approved_on
                        WHEN l.approved_on <> ISNULL(s.approved_on,0)
                        THEN s.approved_on
                        ELSE l.approved_on
                        END as approved_on,
                    case when
                        l.approved_by <> ISNULL(d.approved_by,0)
						THEN d.approved_by
                        WHEN l.approved_by <> ISNULL(s.approved_by,0)
                        THEN s.approved_by
                        ELSE l.approved_by
                        END as approved_by,
                    case when
                        l.approved_reference_id <> ISNULL(d.approved_reference_id,0)
						THEN d.approved_reference_id
                        WHEN l.approved_reference_id <> ISNULL(s.approved_reference_id,0)
                        THEN s.approved_reference_id
                        ELSE l.approved_reference_id
                        END as approved_reference_id,
                    case when
                        l.rejected_on <> ISNULL(d.rejected_on,0)
						THEN d.rejected_on
                        WHEN l.rejected_on <> ISNULL(s.rejected_on,0)
                        THEN s.rejected_on
                        ELSE l.rejected_on
                        END as rejected_on,
                    case when
                        l.rejected_by <> ISNULL(d.rejected_by,0)
						THEN d.rejected_by
                        WHEN l.rejected_by <> ISNULL(s.rejected_by,0)
                        THEN s.rejected_by
                        ELSE l.rejected_by
                        END as rejected_by,
                    case when
                        l.reject_reference_id <> ISNULL(d.reject_reference_id,0)
						THEN d.reject_reference_id
                        WHEN l.reject_reference_id <> ISNULL(s.reject_reference_id,0)
                        THEN s.reject_reference_id
                        ELSE l.reject_reference_id
                        END as reject_reference_id,
                    case when
                        l.modified_on <> ISNULL(d.modified_on,0)
						THEN d.modified_on
                        WHEN l.modified_on <>ISNULL(s.modified_on,0)
                        THEN s.modified_on
                        ELSE l.modified_on
                        END as modified_on,
                    case when
                        l.modified_by <> ISNULL(d.modified_by,0)
						THEN d.modified_by
                        WHEN l.modified_by <>ISNULL(s.modified_by,0)
                        THEN s.modified_by
                        ELSE l.modified_by
                        END as modified_by,
                    case when
                        l.modified_reference_id <> ISNULL(d.modified_reference_id,0)
						THEN d.modified_reference_id
                        WHEN l.modified_reference_id <> ISNULL(s.modified_reference_id,0)
                        THEN s.modified_reference_id
                        ELSE l.modified_reference_id
                        END as modified_reference_id
                        from holiday_list l
                    join holiday_date d
                    on l.holiday_code = d.holiday_code
                    join holiday_states s
                    on s.holiday_code = l.holiday_code
					where (l.record_status =  '""" + record_status + "' OR s.record_status =  '""" + record_status + "' OR d.record_status =  '""" + record_status + "') and  (l.created_reference_id='""" + created_reference_id + "' OR s.created_reference_id =  '""" + created_reference_id + "' OR d.created_reference_id = '""" + created_reference_id + "')"


                    print(sql_query)
                    cursor = connection.cursor ( )
                    cursor.execute ( sql_query )
                    list_db = cursor.fetchall ( )
                    data_list = []
                    for record in list_db :
                        temp_dict = { }
                        temp_dict['holiday_code'] = record[0]
                        temp_dict['holiday_name'] = record[1]
                        temp_dict['state'] = record[2]
                        temp_dict['start_date'] = record[3]
                        temp_dict['end_date'] = record[4]
                        temp_dict['record_status'] = record[5]
                        temp_dict['created_reference_id'] = record[6]
                        temp_dict['created_on'] = record[7]
                        temp_dict['created_by'] = record[8]
                        temp_dict['approved_on'] = record[9]
                        temp_dict['approved_by'] = record[10]
                        temp_dict['approved_reference_id'] = record[11]
                        temp_dict['rejected_on'] = record[12]
                        temp_dict['rejected_by'] = record[13]
                        temp_dict['reject_reference_id'] = record[14]
                        temp_dict['modified_on'] = record[15]
                        temp_dict['modified_by'] = record[16]
                        temp_dict['modified_reference_id'] = record[17]
                        data_list.append ( temp_dict )
                    # print (data_list)
                    status_dict = { }
                    status_dict['data'] = data_list
                    # print(data_list)
                    return data_list
                else :
                    sql_query = """
                        select l.holiday_code,l.holiday_name,s.State,d.start_date,d.end_date,
                    CASE WHEN
                            (l.record_status <>  d.record_status)
							THEN d.record_status
                         WHEN  (l.record_status <> s.record_status)
                            THEN s.record_status
						ELSE
							l.record_status
                    END as record_status,
                    CASE WHEN
                            l.created_reference_id <> d.created_reference_id
							THEN d.created_reference_id
                         WHEN  l.created_reference_id <> s.created_reference_id
                            THEN s.created_reference_id
                            ELSE
                            l.created_reference_id
                            END AS created_reference_id,
                    CASE WHEN
                            l.created_on <> d.created_on
                          THEN
							d.created_on
						WHEN l.created_on <> s.created_on
                            THEN s.created_on
                        ELSE l.created_on
                           END as created_on,
                    CASE WHEN
                        l.created_by <> d.created_by
						THEN d.created_by
                        WHEN l.created_by <> s.created_by
                        THEN s.created_by
                        ELSE l.created_by
                        END as created_by,
                    case when
                        l.approved_on <> ISNULL(d.approved_on,0)
						THEN d.approved_on
                        WHEN l.approved_on <> ISNULL(s.approved_on,0)
                        THEN s.approved_on
                        ELSE l.approved_on
                        END as approved_on,
                    case when
                        l.approved_by <> ISNULL(d.approved_by,0)
						THEN d.approved_by
                        WHEN l.approved_by <> ISNULL(s.approved_by,0)
                        THEN s.approved_by
                        ELSE l.approved_by
                        END as approved_by,
                    case when
                        l.approved_reference_id <> ISNULL(d.approved_reference_id,0)
						THEN d.approved_reference_id
                        WHEN l.approved_reference_id <> ISNULL(s.approved_reference_id,0)
                        THEN s.approved_reference_id
                        ELSE l.approved_reference_id
                        END as approved_reference_id,
                    case when
                        l.rejected_on <> ISNULL(d.rejected_on,0)
						THEN d.rejected_on
                        WHEN l.rejected_on <> ISNULL(s.rejected_on,0)
                        THEN s.rejected_on
                        ELSE l.rejected_on
                        END as rejected_on,
                    case when
                        l.rejected_by <> ISNULL(d.rejected_by,0)
						THEN d.rejected_by
                        WHEN l.rejected_by <> ISNULL(s.rejected_by,0)
                        THEN s.rejected_by
                        ELSE l.rejected_by
                        END as rejected_by,
                    case when
                        l.reject_reference_id <> ISNULL(d.reject_reference_id,0)
						THEN d.reject_reference_id
                        WHEN l.reject_reference_id <> ISNULL(s.reject_reference_id,0)
                        THEN s.reject_reference_id
                        ELSE l.reject_reference_id
                        END as reject_reference_id,
                    case when
                        l.modified_on <> ISNULL(d.modified_on,0)
						THEN d.modified_on
                        WHEN l.modified_on <>ISNULL(s.modified_on,0)
                        THEN s.modified_on
                        ELSE l.modified_on
                        END as modified_on,
                    case when
                        l.modified_by <> ISNULL(d.modified_by,0)
						THEN d.modified_by
                        WHEN l.modified_by <>ISNULL(s.modified_by,0)
                        THEN s.modified_by
                        ELSE l.modified_by
                        END as modified_by,
                    case when
                        l.modified_reference_id <> ISNULL(d.modified_reference_id,0)
						THEN d.modified_reference_id
                        WHEN l.modified_reference_id <> ISNULL(s.modified_reference_id,0)
                        THEN s.modified_reference_id
                        ELSE l.modified_reference_id
                        END as modified_reference_id
                        from holiday_list l
						join holiday_date d
                        on l.holiday_code = d.holiday_code
                        AND l.record_status = d.record_status
                        join holiday_states s
                        on s.holiday_code = l.holiday_code
                        and s.record_status = l.record_status
                        where (l.record_status =  '""" + record_status + "' OR s.record_status =  '""" + record_status + "' OR d.record_status =  '""" + record_status + "')"
                    print(sql_query)
                    cursor = connection.cursor ( )
                    cursor.execute ( sql_query )
                    list_db = cursor.fetchall ( )
                    data_list = []
                    for record in list_db :
                        temp_dict = { }
                        temp_dict['holiday_code'] = record[0]
                        temp_dict['holiday_name'] = record[1]
                        temp_dict['state'] = record[2]
                        temp_dict['start_date'] = record[3]
                        temp_dict['end_date'] = record[4]
                        temp_dict['record_status'] = record[5]
                        temp_dict['created_reference_id'] = record[6]
                        temp_dict['created_on'] = record[7]
                        temp_dict['created_by'] = record[8]
                        temp_dict['approved_on'] = record[9]
                        temp_dict['approved_by'] = record[10]
                        temp_dict['approved_reference_id'] = record[11]
                        temp_dict['rejected_on'] = record[12]
                        temp_dict['rejected_by'] = record[13]
                        temp_dict['reject_reference_id'] = record[14]
                        temp_dict['modified_on'] = record[15]
                        temp_dict['modified_by'] = record[16]
                        temp_dict['modified_reference_id'] = record[17]

                        data_list.append ( temp_dict )
                    status_dict = { }
                    status_dict['data'] = data_list
                    print(data_list)
                    return data_list
        else :
            if 'filter_data' in request.data :
                filters_param = request.data["filter_data"]
                if 'for_date' in filters_param:
                    for_date_filter = filters_param['for_date']
                    del filters_param['for_date']
                    list_db = model_name.objects.filter ( is_valid_record='Yes' ).filter ( **filters_param ).filter(for_date__date=for_date_filter).values ( )
                else:
                    list_db = model_name.objects.filter ( is_valid_record='Yes' ).filter ( **filters_param ).values ( )
            # else:
            # 	list_db = model_name.objects.filter(created_reference_id = created_reference_id, is_valid_record = 'Yes').values()
        return list_db


class ViewControllerConsolidateMaster ( ) :
    def handle_view_request ( self , request ) :
        common_util_obj = common_util.common_utils ( )
        data_info = { }
        try :
            sql_consolidate_query = "select a.site_code,a.atm_id,a.atm_band,a.brand,a.project_id,a.bank_code,a.state,a.district,a.city,a.site_status,acl.insurance_limit,acl.whether_critical_atm,acl.cassette_50_count,acl.cassette_100_count,acl.cassette_200_count,acl.cassette_500_count,acl.cassette_2000_count,acl.total_cassette_count,acl.bank_cash_limit,acl.base_limit,cra.feeder_branch_code,cra.new_cra,cra.feasibility_or_loading_frequency,cra.br_document_status,cra.vaulting,cra.is_feasible_mon,cra.is_feasible_tue,cra.is_feasible_wed,cra.is_feasible_thu,cra.is_feasible_fri,cra.is_feasible_sat,fbm.region_code,fbm.sol_id,fbm.feeder_branch,fbm.feeder_linked_count,fbm.is_vaulting_enabled,fbm.alternate_cash_balance,fbm.is_currency_chest from atm_master as a left join ATM_Config_limits  as acl on a.atm_id = acl.atm_id left join cra_feasibility as cra on a.atm_id = cra.atm_id left join feeder_branch_master as fbm on cra.feeder_branch_code = fbm.feeder_branch where a.record_status = 'Active'"
            data = pd.read_sql ( sql_consolidate_query , connection )
            data = data.fillna ( '' )
            data = data.to_dict ( 'records' )
            if data is not None :
                data_info['data'] = data
                data_info['api_status'] = masterconf.api_status_Success
                return data_info
            else :
                data_info['data'] = []
                data_info['api_status'] = masterconf.api_status_Failure
                return data_info
        except Exception as e :
            data_info["data"] = []
            data_info["api_status"] = masterconf.sql_consolidate_query_failed
            return data_info
