from Common import masterconf
from Model.models import indent_master, indent_detail

class IonDropDownController ( ) :

    def handle_ion_dropdown_request (self, request_data) :
        ion_data_list = []
        status_dict = {}
        atm_id_list = []
        try :
            #extracting data related to indent order number
            data_set = list(indent_master.objects.filter(record_status = masterconf.active_status, indent_order_number = request_data['indent_order_number']).values('bank', 'feeder_branch', 'cra'))
            if data_set:
                #extracting atm_id related to that particular indent order number
                atm_id = list(indent_detail.objects.filter(record_status = masterconf.active_status, indent_order_number = request_data['indent_order_number']).values('atm_id'))
                
                if atm_id:
                    for single_atm_id in atm_id:
                        atm_id_list.append(single_atm_id['atm_id'])
                    data_set = data_set[0]
                    data_set['atm_id'] = atm_id_list
                    status_dict['data'] = data_set
                    status_dict['status_text'] = masterconf.api_status_Success
                    return status_dict
                else:
                    status_dict['status_text'] = masterconf.active_atm_id
                    return status_dict
            else:
                status_dict['status_text'] = masterconf.inactive_indent_order_number
                return status_dict
        except Exception as e :
            print ("Exception : ",e)
            status_dict['status_text'] = masterconf.improper_data
            status_dict['data'] = []
            return status_dict
