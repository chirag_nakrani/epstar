from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common import db_queries_properties
from django.apps import apps
import json
from django.db import models
import logging

class C3RViewController():
	def handle_view_request(self,request):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In GetDataC3R.")
		strfile_type = str(request.data['file_type']).upper()
		empty_list = list()
		list_returned = list()
		table_name_key = strfile_type+"_ALL_TABLE"
		print(table_name_key)
		model_name_str = db_queries_properties.table_dict[table_name_key]
		print(model_name_str)
		model_name = apps.get_model(app_label='Model',model_name=model_name_str)
		print(model_name)
		if 'filters' in  request.data:
			filters_param = request.data['filters']
			if 'date_range' in request.data:
				list_db = model_name.objects.filter(date__range=[request.data['date_range'][0],
																			  request.data['date_range'][
																				  1]]).filter(**filters_param).values()
				print(list_db.query)
			else:
				list_db = model_name.objects.filter(**filters_param).values()
			return list_db
		else:
			list_db = model_name.objects.all().values()
			return list_db
