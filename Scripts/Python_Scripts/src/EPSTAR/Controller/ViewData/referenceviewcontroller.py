from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import pyexcel
import xlrd
import time, datetime
#from Controller.Routine import BaseCBRController, BaseCDRController, BaseVCBControllerEOD, BaseC3RMISController, DailyLoad_RepCntrlr
from Controller.Master import ATMMasterController
from Controller.ViewData import BankInfoController
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Model.models import Data_rejection_reasons
from Model.models import project_master
from Model.models import bank_master
from django.contrib.auth.models import User,Group,Permission
import json
from json import JSONEncoder
from django.db import connection

class referenceviewcontroller():
	def show_project_list(self,request):
		try:
			data_info = {}
			project_list1 = []
			bank_list1 = []
			user_list1 = []
			group_list1 = []
			status_dict = {}
			project_data = project_master.objects.values('project_id').all()
			#print (project_data.query)
			for i in project_data:
				#print (i["project_id"])
				project_list1.append(i["project_id"])
			status_dict["projectList"] = project_list1
			#print (status_dict)
			bank_data = bank_master.objects.values('bank_code').all()
			for i in bank_data:
				#print (i["bank_code"])
				bank_list1.append(i["bank_code"])
			status_dict["BankList"] = bank_list1
			#print (status_dict)
			user_data = User.objects.values('username').all()
			for i in user_data:
				#print (i["username"])
				user_list1.append(i["username"])
			status_dict["UserList"] = user_list1
			#print (status_dict)
			group_data = Group.objects.values('name').all()
			for i in group_data:
				#print (i["name"])
				group_list1.append(i["name"])
			status_dict["UserGroupList"] = group_list1
			# data_info.append(bank_data)
			# status_dict = {}
			#status_dict['Info list'] = data_info
			return status_dict
		except Exception as e:
			#print (e)
			raise e

	# def show_bank_list(self,request):
	# 	try:
	# 		bank_data = list(bank_list.objects.values('bank_code','bank_name'))
	# 		#print(bank_data)
	# 		status_dict = {}
	# 		status_dict['bank list'] = bank_data
	# 		return status_dict
	# 	except Exception as e:
	# 		#print (e)
	# 		raise e

# #print(project_data)
# exit()
# data_info.append(project_data)
# exit()
