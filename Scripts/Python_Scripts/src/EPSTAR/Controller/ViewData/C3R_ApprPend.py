from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param, data_update_log
import json
import logging

class CsrfExemptSessionAuthentication(SessionAuthentication):
	def enforce_csrf(self, request):
		return  # To not perform the csrf check previously happening

class C3R_ApprPend():
	def handle_ApprPendAPI(self,request_data):
		print ("Inside C3R_ApprPend")
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In C3R_ApprPend.py.")
		data_info = {}
		record_status_data = {}
		data = []
		try:
			for i in request_data['record_status']:
				pending_list = data_update_log.objects.filter(data_for_type = request_data['data_for_type'], record_status = i).values('created_by','created_reference_id','datafor_date_time','record_status').all()
				for i in pending_list:
					data.append(i)
			if len(data) >= 0:
				data_info["data"] = data
				data_info["status_text"] = masterconf.C3RApi_statusText
				return data_info
			else:
				data_info["status_text"] = "Failed"
				return data_info
		except Exception as e:
			print ("Exception from C3R_ApprPend : {}".format(e))
			logger = common_util_obj.initiateLogger()
			logger.setLevel(logging.ERROR)
			logger.error('Exception occured in C3R_ApprPend', exc_info=True)
			raise e
