from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from Common.CommonFunctions import common_util
from Common import masterconf
from Common import db_queries_properties
from django.apps import apps
from Common.models import  data_update_log_master

class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening

class ConfigMenuController():
    def handle_view_request(self,request):
        print ("Inside in ConfigMenuController")
        common_util_obj = common_util.common_utils()
        # menu_type = str(request.data['menu']).upper()
#########################Extracting uploaded data from tables of respective menu###########################


        menu_type = str ( request.data['menu'] ).upper()
        table_name_key = menu_type + "_TABLE"
        model_name_str = db_queries_properties.all_table_dict[table_name_key]
        model_name = apps.get_model ( app_label='Model' , model_name=model_name_str )
        filters_param = request.data['record_status']
        data = []
        status_dict = {}
        try:
            if menu_type == masterconf.cash_avail_file_type or menu_type == masterconf.indent_pre_qualify_atm_file_type or menu_type == 'EOD_ACTIVITY' or menu_type == masterconf.file_type_bank_limitdays.upper() or menu_type == masterconf.file_type_feeder_limitdays.upper() or menu_type == masterconf.feeder_vaulting_pre_config_file_type.upper() or menu_type == masterconf.feeder_denomination_priority_filetype.upper() or menu_type == masterconf.file_type_feeder_deno_pre_avail.upper() or menu_type == masterconf.file_type_fdr_casstt_max_cap_per.upper():
                model_name = data_update_log_master
            for i in request.data['record_status']:#to find the no of files uplaoded into tables
                if menu_type == masterconf.eod_activity_file_type.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                elif menu_type == masterconf.indent_pre_qualify_atm_file_type.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                elif menu_type == masterconf.limitdays_file_type.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('eps_site_code','atm_id','project_id','bank_code','indent_date','next_feasible_date','next_working_date','decidelimitdays','loadinglimitdays','record_status', 'created_on', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                elif menu_type == masterconf.feeder_vaulting_pre_config_file_type.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                elif menu_type == masterconf.cash_avail_file_type.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('date_created', 'record_status', 'created_by','created_reference_id')  # from table only filtered records are needed to further operation
                elif menu_type == masterconf.feeder_denomination_priority_filetype.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                elif menu_type == masterconf.file_type_fdr_casstt_max_cap_per.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                elif menu_type == masterconf.file_type_feeder_deno_pre_avail.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                elif menu_type == masterconf.file_type_bank_limitdays.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                elif menu_type == masterconf.file_type_feeder_limitdays.upper():
                    data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
                else:
                    data_list = []
                for i in data_list:
                    data.append(i)#to collect all the files uploaded in their respective tables
            if len(data) >= 0:
                status_dict["data"] = data
                status_dict["status_text"] = masterconf.api_execution_successful
                return status_dict#If API is successfully executed
            else:
                status_dict["data"] = []
                status_dict["status_text"] = masterconf.api_execution_failed
                return status_dict#If API is execution failed
        except Exception as e:
            print ("Exception from ConfigMenuController : {}".format(e))
            status_dict["data"] = []
            status_dict["status_text"] = masterconf.api_execution_failed
            return status_dict
