from rest_framework.authentication import SessionAuthentication , BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from Common.CommonFunctions import common_util
from Common import masterconf
from Model.models import project_master , bank_master , CRA_master , region_master
from Common.models import app_config_param


class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


class DisplayDataController ( ) :
    def handle_displaydata_request ( self , request_data ) :
        print ("Inside handle_displaydata_request from DisplayDataController")
        common_util_obj = common_util.common_utils ( )
        data_list = []
        status_dict = { }
        table_objects = []
        try :
            if request_data['file_type'].upper ( ) == masterconf.display_data_type[
                0].upper ( ) :  # to extract the active project_id from project_master for drop down menu in ui
                table_objects = project_master.objects.filter ( record_status=masterconf.active_status ).values (
                    'project_id' )
            elif request_data['file_type'].upper ( ) == masterconf.display_data_type[
                1].upper ( ) :  # to extract the active bank_code and bank_name from bank_master for drop down menu in ui
                table_objects = bank_master.objects.filter ( record_status=masterconf.active_status ).values (
                    'bank_code' , 'bank_name' )
            elif request_data['file_type'].upper ( ) == masterconf.display_data_type[
                2].upper ( ) :  # to extract the designation from app_config_param for drop down menu in ui
                table_objects = app_config_param.objects.filter (
                    category=masterconf.Signature_Allowed_Groups_in_auth_group ).values ( 'value' )
            elif request_data['file_type'].upper ( ) == masterconf.display_data_type[
                3].upper ( ) :  # to extract the cra_id from cra_master for drop down menu in ui
                table_objects = CRA_master.objects.filter ( record_status=masterconf.active_status ).values ( 'cra_id' )
            elif request_data['file_type'].upper ( ) == masterconf.display_data_type[
                4].upper ( ) :  # to extract the region name from region master for drop down menu in ui
                table_objects = region_master.objects.filter ( record_status=masterconf.active_status ).values (
                    'region_name' )
            else :
                status_dict["data"] = data_list
                status_dict["status_desc"] = masterconf.improper_dropdown_datatype
                return status_dict
            if table_objects :
                status_dict["data"] = table_objects
                status_dict["status_desc"] = masterconf.drop_down_data
                return status_dict
            else :
                status_dict["data"] = data_list
                status_dict["status_desc"] = masterconf.improper_dropdown_datatype
                return status_dict
        except Exception as e :
            print ("Exception from DisplayDataController : {}".format ( e ))
            status_dict["level"] = masterconf.system_level
            status_dict["status_code"] = masterconf.system_level_exception_code
            status_dict["status_desc"] = masterconf.system_level_exception
            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
