from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common import db_queries_properties
from django.apps import apps
import json
from django.db import models
import logging
from Common.models import data_update_log_master

class CsrfExemptSessionAuthentication(SessionAuthentication):
	def enforce_csrf(self, request):
		return  # To not perform the csrf check previously happening

class FeederMasterController():
	def handle_view_request(self,request):
		print ("m here in FeederMasterController")
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In AmsAtmController.")
		menu_type = str(request.data['menu']).upper()
######################Extracting uploaded files from data_update_log_master, according to record status from feeder_branch_master table ###########################
		model_name = data_update_log_master
		filters_param = request.data['record_status']
		data = []
		status_dict = {}
		try:
			for i in request.data['record_status']:#to find the no of files uplaoded into tables
				data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
				for i in data_list:
					data.append(i)#to collect all the files uploaded in their respective tables
			if len(data) >= 0:
				status_dict["data"] = data
				status_dict["status_text"] = masterconf.masterApiSuccess
				return status_dict
			else:
				status_dict["data"] = []
				status_dict["status_text"] = masterconf.masterApiFailed
				return status_dict
		except Exception as e:
			print ("Exception from FeederMasterController : {}".format(e))
			logger.setLevel(logging.ERROR)
			logger.error('Exception from FeederMasterController', exc_info=True)
			raise e
