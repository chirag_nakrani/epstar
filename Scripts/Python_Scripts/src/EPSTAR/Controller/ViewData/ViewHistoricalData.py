from rest_framework.authentication import SessionAuthentication
from Common import masterconf
from django.db import connection
from Model.models import historical_data_log

class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


class ViewHistoricalData ( ) :
    def handle_view_data ( self , request_data) :
        data_info = { }
        try :
            if 'filter' in request_data:
                historical_data_list = historical_data_log.objects.filter(**request_data['filter']).values().order_by('-id')
            else:
                historical_data_list = historical_data_log.objects.values().order_by('-id')
            return historical_data_list
        except Exception as e :
            print ("exception : ",e)
            data_info['data'] = []
            data_info['api_status'] = e
            return data_info

