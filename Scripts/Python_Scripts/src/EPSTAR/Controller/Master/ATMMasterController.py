import pandas as pd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from Model.models import atm_config_limits
import logging


class ATMMasterController():
	maintain_log_id = 0
	def handle_master_upload(self,request,input_filepath):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		username = request.userinfo['username'].username
		uploaded_status = 'Uploaded'
		file_type = request.POST['file_type']
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		status_dict = {}
		logger.setLevel(logging.INFO)
		logger.info("In handle_master_upload method of ATMMasterController")
		try:
			df_data = common_util_obj.read_file_xls(input_filepath)
			format_status = common_util_obj.validate_mstr_format_xls(df_data, file_type)
###############Going for format validation#######################################################
			if format_status == masterconf.format_val_success_mstr_upload:
				df_data.columns = df_data.columns.str.lower ( )
				if 'insurance limit' not in df_data.columns:
					df_data['insurance limit'] = None
				if 'old atm id' not in df_data.columns:
					df_data['old atm id'] = None
				if 'cassette 50' not in df_data.columns:
					df_data['cassette 50'] = None
				print ("df_data.columns.values : ", df_data.columns.values)
				duplicated_data = df_data[df_data.duplicated ( subset=['current atm id' , 'site code'] , keep='first' )]
				if duplicated_data.empty != True:
					status_dict["level"] = masterconf.data_level
					status_dict["status_desc"] = masterconf.duplicate_data_entry
					status_dict["status_code"] = masterconf.status_code_for_duplicate_data
					return status_dict
				df_enhanced = common_util_obj.add_fields_master(df_data, uploaded_status, upload_time,username,reference_id,file_type)
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type,uploaded_status, upload_time,username,reference_id)
###################GOinf to upload the data to the DB table###########################################
				self.load_ATM_master_data(df_enhanced)
#####################Going for data validation of the uploaded data ####################################
				data_code = common_util_obj.validate_master_data(file_type, apiflag, username, reference_id)
				status_dict["level"] = masterconf.data_level
				status_dict["status_code"] = data_code
				return status_dict
			else:
				status_dict["level"] = masterconf.format_level
				status_dict["status_code"] = masterconf.format_val_failed_mstr_upload
				status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.format_val_failed_mstr_upload]
				return status_dict
		except Exception as e:
			print("Exception occured in ATMMasterController ::: ",e)
			data_update_log_master.objects.filter(created_reference_id = reference_id).update(record_status = 'Failed')
			raise e

	def load_ATM_master_data(self, df_enhanced):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In load_ATM_master_data method of ATMMasterController")
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				data.append(
					atm_config_limits(
						site_code=str(row['site code']).strip(),
						old_atm_id=row['old atm id'],
						atm_id=str(row['current atm id']).strip(),
						project_id=row['project'],
						bank_code=row['bank code'],
						insurance_limit = row['insurance limit'],
						whether_critical_atm=row['whether critical atm'],
						cassette_50_count=row['cassette 50'],
						cassette_100_count=row['cassette 100'],
						cassette_200_count=row['cassette 200'],
						cassette_500_count=row['cassette 500'],
						cassette_2000_count=row['cassette 2000'],
						total_cassette_count = row['total cassette count'],
						bank_cash_limit=row['bank limit'],
						base_limit=row['base limit'],
						s_g_locker_no=row['s&g lock sr. no.'],
						type_of_switch=row['type of switch'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)
			atm_config_limits.objects.bulk_create(data)#performing bulk insertion into respective tables
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('Exception in bulk insert in load_ATM_master_data method of ATMMasterController', exc_info=True)
			raise e
