import pandas as pd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from datetime import timedelta
from Model.models import cra_feasibility
import logging


class CRA_master_controller():
	maintain_log_id = 0
	def handle_master_upload(self,request,input_filepath):
		print ("Inside ", request.POST['file_type'].upper())
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In handle_master_upload method of CRA_master_controller")
		username = request.userinfo['username'].username
		uploaded_status = 'Uploaded'
		file_type = request.POST['file_type'].upper()
		df_data = common_util_obj.read_file_xls(input_filepath)
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		maintain_log_id = 0
		# print("reference_id from CRA_master_controller:::: ",reference_id)
		status_dict = {}
		try:
###############Going for format validation#######################################################
			format_status = common_util_obj.validate_mstr_format_xls(df_data, file_type)
			print("format_status :::: ", format_status)
			if format_status == masterconf.format_val_success_mstr_upload:
				df_data.columns = df_data.columns.str.lower ( )
				# print ("df_data.columns : ", df_data.columns)
				duplicated_data = df_data[df_data.duplicated ( subset=['site_code' , 'atm_id' , 'project_id' , 'bank_code', 'feeder_branch_code'] , keep='first' )]
				# print ("duplicated_data : ", duplicated_data)
				if duplicated_data.empty != True :
					status_dict["level"] = masterconf.data_level
					status_dict["status_desc"] = masterconf.duplicate_data_entry
					status_dict["status_code"] = masterconf.status_code_for_duplicate_data
					return status_dict
				df_enhanced = common_util_obj.add_fields_master(df_data, uploaded_status, upload_time,username,reference_id,file_type)
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type,uploaded_status, upload_time,username,reference_id)
###################GOinf to upload the data to the DB table###########################################
				# print("After masterlog")
				self.load_CRAFEASIBILITY_master_data(df_enhanced,file_type)
#####################Going for data validation of the uploaded data ####################################
				data_code = common_util_obj.validate_master_data(file_type, apiflag, username, reference_id)
				status_dict["level"] = masterconf.data_level
				status_dict["status_code"] = data_code
				return status_dict
			else:
				status_dict["level"] = masterconf.format_level
				status_dict["status_code"] = masterconf.format_val_failed_mstr_upload
				status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.format_val_failed_mstr_upload]
				return status_dict
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('Exception in handle_master_upload method of CRA_master_controller', exc_info=True)
			print("Exception occured in CRA_master_controller ::: ",e)
			print ("maintain_log_id : ",maintain_log_id)
			data_update_log_master.objects.filter ( created_reference_id=reference_id ).update (
				record_status='Failed' )
			raise e
##########################Dumping the data from file to DB#################################################
	def load_CRAFEASIBILITY_master_data(self, df_enhanced, file_type):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In load_CRAFEASIBILITY_master_data method of CRA_master_controller")
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				data.append(
					cra_feasibility(
						site_code = str(row['site_code']).strip(),
						atm_id = str(row['atm_id']).strip(),
						project_id = row['project_id'],
						bank_code=row['bank_code'],
						feeder_branch_code = row['feeder_branch_code'],
						new_cra = row['new_cra'],
						feasibility_or_loading_frequency = row['feasibility/loading_frequency'],
						distance_from_hub_to_site = row['distance_fom_hub_to_site'],
						distance_from_atm_site_to_nodal_branch	= row['distance_from_atm_site_to_nodal_branch'],
						distance_from_nodal_branch = row['distance_from_nodal_branch'],
						flm_tat = row['flm_tat'],
						cra_spoc = row['cra_spoc'],
						cra_spoc_contact_no = row['cra_spoc_contact_no'],
						br_document_status = row['br_document_status'],
						cash_van = row['cash_van'],
						gunman = row['gunman'],
						lc_nearest_hub_or_spoke_or_branch_from_site = row['lc_nearest_hub/spoke/branch_from_site'],
						accessibility = row['accessibility'],
						first_call_dispatch_time = row['first_call_dispatch_time'],
						last_call_dispatch_time = row['last_call_dispatch_time'],
						reason_for_limited_access = row['reason_for_limited_access'],
						vaulting = row['vaulting'],
						feasibility_received_date = row['feasibility_received_date'],
						feasibility_send_date = row['feasibility_send_date'],
						br_request_date = row['br_request_date'],
						br_send_date = row['br_send_date'],
						is_feasible_mon = row['is_feasible_mon'],
						is_feasible_tue = row['is_feasible_tue'],
						is_feasible_wed = row['is_feasible_wed'],
						is_feasible_thu = row['is_feasible_thu'],
						is_feasible_fri = row['is_feasible_fri'],
						is_feasible_sat = row['is_feasible_sat'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)
			#print(df_enhanced.columns)
			cra_feasibility.objects.bulk_create(data)#performing bulk insertion into respective tables
		except Exception as e:
			print (e)
			logger.setLevel(logging.ERROR)
			logger.error('Exception in bulk insert in load_CRAFEASIBILITY_master_data method of CRA_master_controller', exc_info=True)
			raise e
