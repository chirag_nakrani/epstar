import pandas as pd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from Model.models import ams_api_token_data
import logging
import requests
import json
from django.apps import apps
from datetime import timedelta
from Common import db_queries_properties
from Model.models import atm_master


class AMSAPI_Controller():
	maintain_log_id = 0
	def handle_ams_api_data_upload(self,request):
		status_dict = {}
		common_util_obj = common_util.common_utils()

		reference_id = request.userinfo['reference_id']
		username = request.userinfo['username'].username
		datafor_date_time_input = request.data['datafor_date_time']
		data_to_be_deleted_list = list()
		file_type = request.data['file_type'].upper()
		try:
			print ("Inside handle_ams_api_data_upload of AMSAPI_Controller")
			response = requests.post(masterconf.token_url, data=masterconf.params)
			text_response = response.text
			received_json = json.loads(text_response)
			print ("received_json : ",received_json)
			if received_json["access_token"]:
				access_token_extracted = received_json["access_token"]
				tokenid_extracted = received_json["TokenId"]
				maxResponseId = list(ams_api_token_data.objects.filter(record_status = 'Active').values('max_response_id'))
				if not maxResponseId:
					maxResponseId = 0
				else:
					maxResponseId = maxResponseId[0]['max_response_id']
				lastUpdatedDate = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
				api_data_url = masterconf.api_data_url+str(maxResponseId)+"&@lastUpdatedDate="+str(lastUpdatedDate)
				print ("api_data_url : ",api_data_url)
				# exit()
				params = {
					'Content-Type' : 'Application/JSON',
					'Authorization' : 'bearer '+access_token_extracted,
					'TokenId' : str(tokenid_extracted)
				}
				data_response = requests.get(api_data_url, headers=params,allow_redirects=False)
				data_text_response = data_response.text
				received_data_json = json.loads(data_text_response)
				# print ("received_data_json : ", received_data_json)
				extracted_maxReturnResponseId_from_api = received_data_json["maxReturnResponseId"]
				if maxResponseId != 0:
					###########marking previous record of the maxResponseId as history and making new entry###################### 
					maxResponseId_from_db = ams_api_token_data.objects.get(record_status = 'Active')
					print ("maxResponseId_from_db : ",maxResponseId_from_db)
					# entry = data_update_log.objects.get ( id=self.maintain_log_id )
					maxResponseId_from_db.record_status = 'History'
					maxResponseId_from_db.deleted_on = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
					maxResponseId_from_db.deleted_by = username
					maxResponseId_from_db.deleted_reference_id = reference_id
					maxResponseId_from_db.save()
					metadata = {
						"datafor_date_time" : datafor_date_time_input,
						"max_response_id" : extracted_maxReturnResponseId_from_api,
						"record_status" : masterconf.new_data_record_status,
						"created_on" : lastUpdatedDate,
						"created_by" : username,
						"created_reference_id" : reference_id
					}
					maxResponseId_insert_obj = ams_api_token_data(**metadata)
					maxResponseId_insert_obj.save()
				else:
					metadata = {
						"datafor_date_time" : datafor_date_time_input,
						"max_response_id" : extracted_maxReturnResponseId_from_api,
						"record_status" : masterconf.new_data_record_status,
						"created_on" : lastUpdatedDate,
						"created_by" : username,
						"created_reference_id" : reference_id
					}
					maxResponseId_insert_obj = ams_api_token_data(**metadata)
					maxResponseId_insert_obj.save()

				if received_data_json["data"]:
					record_to_be_inserted = received_data_json["data"]["inserted"] #storing newly data fetching from ams api into local variable  
					record_to_be_updated = received_data_json["data"]["updated"] #stroing data that is to be updated in the master table
					record_to_be_deleted = received_data_json["data"]["deleted"] #stroing data that is to be deleted from master data
					if record_to_be_inserted:
						for i in record_to_be_inserted:						
							if i['SiteCode'] is not None and i['CurrentATMID'] is not  None:
								metadata = {
									"site_code" : i['SiteCode'],
									"old_atm_id" : i['OldATMID'],
									"atm_id" : i['CurrentATMID'],
									"atm_band" : i['ATMBand'],
									"ej_docket_no" : i['EJDocketNo'],
									"grouting_status" : i['GroutingStatus'],
									"brand" : i['Brand'],
									"vsat_id" : i['VSATID'],
									"vendor_name" : i['VendorName'],
									"serial_no" : i['SerialNo'],
									"tech_live_date" : i['TechLiveDate'],
									"cash_live_date" : i['CashLiveDate'],
									"insurance_limits" : i['InsuranceLimits'],
									"project_id" : i['ProjectCode'],
									"bank_name" : i['BankName'],
									"circle_zone_region" : i['CircleZoneRegion'],
									"site_code_2015_16" : i['SiteCode201516'],
									"site_code_2016_17" : i['SiteCode201617'],
									"state" : i['State'],
									"district" : i['District'],
									"city" : i['City'],
									"site_address_line_1" : i['SiteAddress'],
									"site_address_line_2" : '',
									"pincode" : i['Pincode'],
									"site_category" : i['SiteCategory'],
									"site_type" : i['SiteType'],
									"installation_type" : i['InstallationType'],
									"site_status" : i['SiteStatus'],
									"channel_manager_name" : i['ChannelManagerName'],
									"channel_manager_contact_no" : i['ChannelManagerContactNo'],
									"location_name" : i['LocationName'],
									"atm_cash_removal_date" : i['ATMCashRemovalDate'],
									"atm_ip" : i['ATMIP'],
									"switch_ip" : i['SwitchIP'],
									"external_camera_installation_status" : i['ExternalCameraInstallationStatus'],
									"atm_owner" : i['ATMOwner'],
									"stabilizer_status" : i['StabilizerStatus'],
									"ups_capacity" : i['UPSCapacity'],
									"no_of_batteries" : i['NoofBatteries'],
									"ups_battery_backup" : i['UPSBatterybackupinHrs'],
									"load_shedding_status" : i['LoadSheddingStatusinhrs'],
									"solar_dg" : i['SolarDG'],
									"created_on" : lastUpdatedDate,
									"created_by" : username,
									"created_reference_id" : reference_id,
									"record_status" : masterconf.pending_status
								}
								Atm_master = atm_master(**metadata)#inserting record sequence wise into respective table and saving the same
								Atm_master.save()

							# else:
							# 	pass
						self.maintain_log_id = common_util_obj.maintainlog_master(file_type, masterconf.pending_status, lastUpdatedDate, username, reference_id)
						table_name = file_type.upper()+"_TABLE"
						table_name = db_queries_properties.all_table_dict[table_name]
						request_status = masterconf.new_data_record_status
						code = common_util_obj.change_status_ams_api(reference_id, username, table_name, request_status)
	###############when the record is to be updated, mark the previous record as history and add the current data#########################
					if record_to_be_updated:
						for i in record_to_be_updated:
							if i['SiteCode'] is not None and i['CurrentATMID'] is not  None:
								metadata = {
									"site_code" : i['SiteCode'],
									"old_atm_id" : i['OldATMID'],
									"atm_id" : i['CurrentATMID'],
									"atm_band" : i['ATMBand'],
									"ej_docket_no" : i['EJDocketNo'],
									"grouting_status" : i['GroutingStatus'],
									"brand" : i['Brand'],
									"vsat_id" : i['VSATID'],
									"vendor_name" : i['VendorName'],
									"serial_no" : i['SerialNo'],
									"tech_live_date" : i['TechLiveDate'],
									"cash_live_date" : i['CashLiveDate'],
									"insurance_limits" : i['InsuranceLimits'],
									"project_id" : i['ProjectCode'],
									"bank_name" : i['BankName'],
									"circle_zone_region" : i['CircleZoneRegion'],
									"site_code_2015_16" : i['SiteCode201516'],
									"site_code_2016_17" : i['SiteCode201617'],
									"state" : i['State'],
									"district" : i['District'],
									"city" : i['City'],
									"site_address_line_1" : i['SiteAddress'],
									"site_address_line_2" : '',
									"pincode" : i['Pincode'],
									"site_category" : i['SiteCategory'],
									"site_type" : i['SiteType'],
									"installation_type" : i['InstallationType'],
									"site_status" : i['SiteStatus'],
									"channel_manager_name" : i['ChannelManagerName'],
									"channel_manager_contact_no" : i['ChannelManagerContactNo'],
									"location_name" : i['LocationName'],
									"atm_cash_removal_date" : i['ATMCashRemovalDate'],
									"atm_ip" : i['ATMIP'],
									"switch_ip" : i['SwitchIP'],
									"external_camera_installation_status" : i['ExternalCameraInstallationStatus'],
									"atm_owner" : i['ATMOwner'],
									"stabilizer_status" : i['StabilizerStatus'],
									"ups_capacity" : i['UPSCapacity'],
									"no_of_batteries" : i['NoofBatteries'],
									"ups_battery_backup" : i['UPSBatterybackupinHrs'],
									"load_shedding_status" : i['LoadSheddingStatusinhrs'],
									"solar_dg" : i['SolarDG'],
									"created_on" : lastUpdatedDate,
									"created_by" : username,
									"created_reference_id" : reference_id,
									"record_status" : masterconf.pending_status
								}
								Atm_master = atm_master(**metadata)#inserting record sequence wise into respective table and saving the same
								Atm_master.save()

							# else:
							# 	pass
						self.maintain_log_id = common_util_obj.maintainlog_master(file_type, masterconf.pending_status, lastUpdatedDate, username, reference_id)
						table_name = file_type.upper()+"_TABLE"
						table_name = db_queries_properties.all_table_dict[table_name]
						request_status = masterconf.new_data_record_status
						code = common_util_obj.change_status_ams_api(reference_id, username, table_name, request_status)
	###############when the record is to be deleted, mark the site_status = history, no hardcore deleting#########################
					if record_to_be_deleted:
						for i in record_to_be_deleted:
							#Entry.objects.filter(pub_date__year=2010).update(comments_on=False)
							if i['SiteCode'] is not None and i['CurrentATMID'] is not  None:
								record_exist_to_be_deleted = list(atm_master.objects.filter(atm_id = i['CurrentATMID'], site_code = i['SiteCode'], record_status = masterconf.new_data_record_status).values('atm_id'))
								if record_exist_to_be_deleted:
									Atm_master = atm_master.objects.filter(atm_id = i['CurrentATMID'], site_code = i['SiteCode'], record_status = masterconf.new_data_record_status).update(record_status = masterconf.record_status_history, deleted_on = lastUpdatedDate,deleted_by = username, deleted_reference_id = reference_id, modified_on = lastUpdatedDate, modified_by = username, modified_reference_id = reference_id)
								else:
									pass
							else:
								pass	
					status_dict['status_desc'] = received_data_json["message"]
					status_dict['status_text'] = masterconf.success_codes_desc["S107"]
					print ("\nstatus_dict after modification of data:",status_dict)
					return status_dict
	################When no record is found to insert or update ###################################################################
				else:
					status_dict['status_desc'] = received_json['error']
					status_dict['status_text'] = masterconf.failure_status_resp
					print ("\nstatus_dict from else:",status_dict)
					return status_dict
		except Exception as e:
			print ("Exception returned from ams_api controller : ",e)
			token_entry = list(ams_api_token_data.objects.filter(datafor_date_time = datafor_date_time_input, created_reference_id = reference_id).values('id'))
			if token_entry:
				token_entry_id = token_entry[0]['id']
				token_entry_update = ams_api_token_data.objects.filter(datafor_date_time = datafor_date_time_input, created_reference_id = reference_id).update(record_status = masterconf.failure_status_resp)
			# if token_entry:
			# 	token_entry_update = ams_api_token_data.objects.filter(datafor_date_time = datafor_date_time_input, created_reference_id = reference_id).update(record_status = masterconf.failure_status_resp)
			data_update_log_entry = list(data_update_log_master.objects.filter(data_for_type = file_type, record_status = masterconf.pending_status, created_reference_id = reference_id).values('id'))
			if data_update_log_entry:
				data_update_log_entry_id = data_update_log_entry[0]['id']
				entry_update = data_update_log_master.objects.filter(id = data_update_log_entry, data_for_type = file_type, record_status = masterconf.pending_status, created_reference_id = reference_id).update(record_status = masterconf.failure_status_resp)
			# if data_update_log_entry:
			# 	entry_update = data_update_log_master.objects.filter(id = data_update_log_entry, data_for_type = file_type, record_status = masterconf.pending_status, created_reference_id = reference_id).update(record_status = masterconf.failure_status_resp)
			print ("Exception returned from ams_api controller : ",e)
			status_dict['status_desc'] = str(e)
			status_dict['status_text'] = masterconf.failure_status_resp
			print ("status_dict returned from exception of AMSAPI_Controller : ", status_dict)
			return status_dict