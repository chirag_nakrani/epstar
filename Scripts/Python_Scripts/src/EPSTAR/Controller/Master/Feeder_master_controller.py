import pandas as pd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from datetime import timedelta
from Model.models import feeder_branch_master
import logging

class Feeder_master_controller():
	maintain_log_id = 0
	def handle_master_upload(self,request,input_filepath):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In handle_master_upload method of Feeder_master_controller")
		username = request.userinfo['username'].username
		uploaded_status = 'Uploaded'
		file_type = request.POST['file_type'].upper()
		df_data = common_util_obj.read_file_xls(input_filepath)
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		status_dict = {}
		try:
###############Going for format validation#######################################################
			format_status = common_util_obj.validate_mstr_format_xls(df_data, file_type)
			if format_status == masterconf.format_val_success_mstr_upload:
				df_data.columns = df_data.columns.str.lower ( )
				duplicated_data = df_data[df_data.duplicated ( subset=['project' , 'bank', 'region', 'cash feeder branch'] , keep='first' )]
				if duplicated_data.empty != True :
					status_dict["level"] = masterconf.data_level
					status_dict["status_desc"] = masterconf.duplicate_data_entry
					status_dict["status_code"] = masterconf.status_code_for_duplicate_data
					return status_dict
				df_enhanced = common_util_obj.add_fields_master(df_data, uploaded_status, upload_time,username, reference_id,file_type)
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type.upper(),uploaded_status, upload_time,username,reference_id)
###################GOinf to upload the data to the DB table###########################################
				self.load_FEEDER_master_data(df_enhanced)
#####################Going for data validation of the uploaded data ####################################
				data_code = common_util_obj.validate_master_data(file_type, apiflag, username, reference_id)
				status_dict["level"] = masterconf.data_level
				status_dict["status_code"] = data_code
				return status_dict
			else:
				status_dict["level"] = masterconf.format_level
				status_dict["status_code"] = masterconf.format_val_failed_mstr_upload
				status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.format_val_failed_mstr_upload]
				return status_dict
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('Exception in handle_master_upload method of of Feeder_master_controller', exc_info=True)
			print("Exception occured in Feeder_Branch_Master Controller ::: ",e)
			data_update_log_master.objects.filter ( created_reference_id=reference_id ).update (
				record_status='Failed' )
			raise e
##########################Dumping the data from file to DB#################################################
	def load_FEEDER_master_data(self, df_enhanced):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In load_ATM_master_data method of Feeder_master_controller")
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				data.append(
					feeder_branch_master(
						project_id=row['project'],
						bank_code=row['bank'],
						region_code=row['region'],
						sol_id=row['sol id'],
						feeder_branch=row['cash feeder branch'],
						district = row['district'],
						circle = row['circle'],
						feeder_linked_count = row['no. of atms linked to this feeder'],
						email_id = row['email id'],
						alternate_cash_balance = row['alternate cash branch'],
						is_currency_chest = row['whether currency chest - yes/no'],
						contact_details=row['contact no.'],
						is_vaulting_enabled=row['vaulting (yes/no)'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)
			feeder_branch_master.objects.bulk_create(data)#performing bulk insertion into respective tables
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('Exception in bulk insert in load_FEEDER_master_data method of Feeder_master_controller', exc_info=True)
			raise e
