from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
from Model import models
import csv
from Common import db_queries_properties
from Common.models import data_update_log
from django.apps import apps
# from Model.models import ATM_Config_limits
from Model.models import atm_config_limits, feeder_branch_master, Cypher_Code, cra_feasibility, atm_master

class ATMMasterUpdateController():
	def handle_master_update(self,request):
		print ("inside ATMMasterUpdateController")
		common_util_obj = common_util.common_utils()
		update_list = request.data['update']
		file_type = request.data['file_type'].upper()
		# project_id = request.data['project_id']
		apiflag = 'S'
		username = request.userinfo['username'].username
		reference_id = request.userinfo['reference_id']
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		status_dict = {}
		maintain_log_id = 0
		try:
			if file_type.lower() == masterconf.file_type_atm_cnfg.lower():
				print ("inside ATMMasterUpdateController")
				for update_obj in update_list:
					row_obj = {}
					# row_obj['id'] = int(update_obj['id'])
					row_obj['site_code'] = update_obj['site_code']
					row_obj['old_atm_id'] = update_obj['old_atm_id']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['insurance_limit'] = update_obj['insurance_limit']
					row_obj['whether_critical_atm'] = update_obj['whether_critical_atm']
					row_obj['cassette_50_count'] = int(update_obj['cassette_50_count'])
					row_obj['cassette_100_count'] = int(update_obj['cassette_100_count'])
					row_obj['cassette_200_count'] = int(update_obj['cassette_200_count'])
					row_obj['cassette_500_count'] = int(update_obj['cassette_500_count'])
					row_obj['cassette_2000_count'] = int(update_obj['cassette_2000_count'])
					row_obj['total_cassette_count'] = int(update_obj['total_cassette_count'])
					row_obj['bank_cash_limit'] = int(update_obj['bank_cash_limit'])
					row_obj['base_limit'] = int(update_obj['base_limit'])
					row_obj['s_g_locker_no'] = update_obj['s_g_locker_no']
					row_obj['type_of_switch'] = update_obj['type_of_switch']
					row_obj['record_status'] = 'Uploaded'
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Atm_config_limits = atm_config_limits(**row_obj)
					Atm_config_limits.save()
					self.maintain_log_id = common_util_obj.maintainlog_master(file_type, uploaded_status, upload_time, username, reference_id)

			elif file_type.lower() == (masterconf.file_type_cra_mstr).lower():
				print ("Updating CRAFEASIBILITY")
				for update_obj in update_list:
					row_obj = {}
					row_obj['site_code'] = update_obj['site_code']
					# row_obj['old_atm_id'] = update_obj['old_atm_id']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['new_cra'] = update_obj['new_cra']
					row_obj['feasibility_or_loading_frequency'] = update_obj['feasibility_or_loading_frequency']
					row_obj['distance_from_hub_to_site'] = update_obj['distance_from_hub_to_site']
					row_obj['distance_from_atm_site_to_nodal_branch'] = update_obj['distance_from_atm_site_to_nodal_branch']
					row_obj['distance_from_nodal_branch'] = update_obj['distance_from_nodal_branch']
					row_obj['flm_tat'] = update_obj['flm_tat']
					row_obj['cra_spoc'] = update_obj['cra_spoc']
					row_obj['cra_spoc_contact_no'] = update_obj['cra_spoc_contact_no']
					row_obj['br_document_status'] = update_obj['br_document_status']
					row_obj['cash_van'] = update_obj['cash_van']
					row_obj['gunman'] = update_obj['gunman']
					row_obj['lc_nearest_hub_or_spoke_or_branch_from_site'] = update_obj['lc_nearest_hub_or_spoke_or_branch_from_site']
					row_obj['accessibility'] = update_obj['accessibility']
					row_obj['first_call_dispatch_time'] = update_obj['first_call_dispatch_time']
					row_obj['last_call_dispatch_time'] = update_obj['last_call_dispatch_time']
					row_obj['reason_for_limited_access'] = update_obj['reason_for_limited_access']
					row_obj['vaulting'] = update_obj['vaulting']
					row_obj['feasibility_received_date'] = update_obj['feasibility_received_date']
					row_obj['feasibility_send_date'] = update_obj['feasibility_send_date']
					row_obj['feasibility_received_date'] = update_obj['feasibility_received_date']
					row_obj['br_request_date'] = update_obj['br_request_date']
					row_obj['br_send_date'] = update_obj['br_send_date']
					row_obj['is_feasible_mon'] = update_obj['is_feasible_mon']
					row_obj['is_feasible_tue'] = update_obj['is_feasible_tue']
					row_obj['is_feasible_wed'] = update_obj['is_feasible_wed']
					row_obj['is_feasible_thu'] = update_obj['is_feasible_thu']
					row_obj['is_feasible_fri'] = update_obj['is_feasible_fri']
					row_obj['is_feasible_sat'] = update_obj['is_feasible_sat']
					row_obj['record_status'] = 'Uploaded'
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					cRA_Feasibility = cra_feasibility(**row_obj)
					cRA_Feasibility.save()
					# self.maintain_log_id = common_util_obj.maintainlog_master(file_type, 'Uploaded', upload_time, username, reference_id)
			# for update_obj in update_list:
			# 	# update_obj['project_id'] = project_id
			# 	update_obj['record_status'] = 'Uploaded'
			# 	update_obj['created_on'] = upload_time
			# 	update_obj['created_by'] = username
			# 	update_obj['created_reference_id'] = reference_id
			# 	if file_type.lower() == masterconf.file_type_atm_cnfg.lower():
			# 		atm_config_limit = atm_config_limits(**update_obj)
			# 		atm_config_limit.save()
			# 	elif file_type.lower() == (masterconf.file_type_feeder).lower():
			# 		feeder_Branch_Master = feeder_branch_master(**update_obj)
			# 		feeder_Branch_Master.save()
			# 	elif file_type.lower() == (masterconf.file_type_cra_mstr).lower():
			# 		cRA_Feasibility = CRA_Feasibility(**update_obj)
			# 		cRA_Feasibility.save()
			# 	elif file_type.lower() == (masterconf.file_type_ams_atm).lower():
			# 		Atm_master = atm_master(**update_obj)
			# 		Atm_master.save()
				data_code = common_util_obj.validate_master_data(file_type, apiflag, username, reference_id)
				status_dict["level"] = masterconf.data_level
				status_dict["status_code"] = data_code
				return status_dict
		except Exception as e:
			print("Exception in ATMMasterUpdateController ",e)
			print ("maintain_log_id : ", maintain_log_id)
			entry = data_update_log.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e
