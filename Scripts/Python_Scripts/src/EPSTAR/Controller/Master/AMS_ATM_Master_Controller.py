import os
import pandas as pd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from datetime import timedelta
from Model.models import atm_master
import logging
import sys

class AMS_ATM_Master_Controller():
	maintain_log_id = 0
	def handle_master_upload(self,request,input_filepath):
		status_dict = {}
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		username = request.userinfo['username'].username
		uploaded_status = 'Uploaded'
		file_type = request.POST['file_type'].upper()
		try:
			df_data = pd.read_excel(input_filepath, sheet_name = 'Compile Cash Live Master')
		except Exception as e:
			status_dict["level"] = masterconf.data_level
			status_dict["status_desc"] = masterconf.sheet_name_error
			status_dict["status_code"] = masterconf.status_code_for_duplicate_data
			return status_dict
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		
		try:
###############Going for format validation#######################################################
			format_status = common_util_obj.validate_mstr_format_xls(df_data, file_type)
			if format_status == masterconf.format_val_success_mstr_upload:
				df_data.columns = df_data.columns.str.lower ( )
				duplicated_data = df_data[df_data.duplicated (
					subset=['site_code' , 'current_atm_id' , 'site_status'] ,
					keep='first' )]
				#duplicated_data.to_csv("E:\\duplicated_data.csv")
				if duplicated_data.empty != True :
					status_dict["level"] = masterconf.data_level
					status_dict["status_desc"] = masterconf.duplicate_data_entry
					status_dict["status_code"] = masterconf.status_code_for_duplicate_data
					return status_dict
				df_enhanced = common_util_obj.add_fields_master(df_data, uploaded_status, upload_time, username, reference_id,file_type)
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type,uploaded_status, upload_time, username,reference_id)
###################GOinf to upload the data to the DB table###########################################
				self.load_AMSATM_master_data(df_enhanced,file_type)
#####################Going for data validation of the uploaded data ####################################
				data_code = common_util_obj.validate_master_data(file_type, apiflag, username, reference_id)
				status_dict["level"] = masterconf.data_level
				status_dict["status_code"] = data_code
				print ("status_dict from AMS_ATM_Master_Controller: ",status_dict)
				return status_dict
			else:
				status_dict["level"] = masterconf.format_level
				status_dict["status_code"] = masterconf.format_val_failed_mstr_upload
				status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.format_val_failed_mstr_upload]
				return status_dict
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('Exception from AMS_ATM_Master_Controller', exc_info=True)
			print("Exception occured in AMS_ATM_Master_Controller ::: ",e)
			data_update_log_master.objects.filter ( created_reference_id=reference_id ).update (
				record_status='Failed' )
			raise e
##########################Dumping the data from file to DB#################################################
	def load_AMSATM_master_data(self, df_enhanced, file_type):
		try:
			common_util_obj = common_util.common_utils()
			logger = common_util_obj.initiateLogger()
			logger.setLevel(logging.INFO)
			logger.info("In load_AMSATM_master_data of AMS_ATM_Master_Controller.")
			df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
			data = list()
			for i, row in df_enhanced_new.iterrows():
				data.append(
					atm_master(
						site_code = row['site_code'],
						old_atm_id = row['old_atm_id'],
						atm_id = row['current_atm_id'],
						atm_band = row['atm_band'],
						ej_docket_no = row['ej_docket_no'],
						grouting_status = row['grouting_status'],
						brand = row['brand'],
						vsat_id = row['vsat_id'],
						vendor_name = row['vendor_name'],
						serial_no = row['serial_no'],
						current_deployment_status = row['current_deployment_status'],
						tech_live_date = row['tech_live_date'],
						cash_live_date = row['cash_live_date'],
						insurance_limits = row['insurance_limits'],
						project_id = row['project_id'],
						bank_code = row['bank_code'],
						bank_name = row['bank_name'],
						circle_zone_region = row['circle_zone_region'],
						site_code_2015_16 = row['site_code_2015_16'],
						site_code_2016_17 = row['site_code_2016_17'],
						state = row['state'],
						district = row['district'],
						city = row['city'],
						site_address_line_1 = row['site_address_line_1'],
						site_address_line_2 = row['site_address_line_2'],
						pincode = row['pincode'],
						site_category = row['site_category'],
						site_type = row['site_type'],
						installation_type = row['installation_type'],
						site_status = row['site_status'],
						channel_manager_name = row['channel_manager_name'],
						channel_manager_contact_no = row['channel_manager_contact_no'],
						location_name = row['location_name'],
						atm_cash_removal_date = row['atm_cash_removal_date'],
						atm_ip = row['atm_ip'],
						switch_ip = row['switch_ip'],
						external_camera_installation_status = row['external_camera_installation_status'],
						atm_owner = row['atm_owner'],
						stabilizer_status = row['stabilizer_status'],
						ups_capacity = row['ups_capacity'],
						no_of_batteries = row['no_of_batteries'],
						ups_battery_backup = row['ups_battery_backup'],
						load_shedding_status = row['load_shedding_status'],
						solar_dg = row['solar_dg'],
						record_status = row['record_status'],
						created_on = row['created_on'],
						created_by = row['created_by'],
						created_reference_id = row['created_reference_id'],)
				)
			print("load_AMSATM_master_data bulk create pending")
			atm_master.objects.bulk_create(data)#performing bulk insertion into respective tables
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('In Exception of load_AMSATM_master_data of AMS_ATM_Master_Controller.', exc_info=True)
			exc_type, exc_obj, exc_tb = sys.exc_info()
			fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
			print(exc_type, " ", fname, " ", exc_tb.tb_lineno,
					" : Exception in  -------AMS_ATM_Master_Controller.load_AMSATM_master_data----- ->", e)
			print ("In Exception of load_AMSATM_master_data of AMS_ATM_Master_Controller. : ",e)
			raise e
