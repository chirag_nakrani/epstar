from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
from Model import models
import csv
from Common import db_queries_properties
from Common.models import data_update_log_master
from django.apps import apps
from Model.models import atm_config_limits, feeder_branch_master, cra_feasibility, atm_master

class MasterMenuUpdateController():
	maintain_log_id = 0
	def handle_master_update(self,request):
		print ("Inside MasterMenuUpdateController")
		menu_dict = {#It is just made to use it in print stattements to know for which menu we are working
			"ATMCNFGLIMIT" : "ATM CONFIG LIMITS",
			"CRAFEASIBILITY" : "CRA FEASIBILITY",
			"ams_atm" : "ATM MASTER",
			"FEEDER" : "FEEDER BRANCH MASTER"
		}
		print ("Updating master menu initiated for menu {}".format(menu_dict[request.data['file_type']]))
		common_util_obj = common_util.common_utils()
		update_list = request.data['update']
		file_type = request.data['file_type'].upper()
		apiflag = 'S'
		username = request.userinfo['username'].username
		reference_id = request.userinfo['reference_id']
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		status_dict = {}
		# maintain_log_id = 0
		try:
			if file_type.lower() == masterconf.file_type_atm_cnfg.lower():
				print ("Going for ATM CONFIG LIMITS")
				for update_obj in update_list:#extracting data which is to be modified coming from
					row_obj = {}
					row_obj['site_code'] = update_obj['site_code']
					row_obj['old_atm_id'] = update_obj['old_atm_id']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_name'] = update_obj['bank_name']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['insurance_limit'] = update_obj['insurance_limit']
					row_obj['whether_critical_atm'] = update_obj['whether_critical_atm']
					row_obj['cassette_50_count'] = update_obj['cassette_50_count']
					row_obj['cassette_100_count'] = update_obj['cassette_100_count']
					row_obj['cassette_200_count'] = update_obj['cassette_200_count']
					row_obj['cassette_500_count'] = update_obj['cassette_500_count']
					row_obj['cassette_2000_count'] = update_obj['cassette_2000_count']
					row_obj['total_cassette_count'] = update_obj['total_cassette_count']
					row_obj['bank_cash_limit'] = update_obj['bank_cash_limit']
					row_obj['base_limit'] = update_obj['base_limit']
					row_obj['s_g_locker_no'] = update_obj['s_g_locker_no']
					row_obj['type_of_switch'] = update_obj['type_of_switch']
					row_obj['record_status'] = masterconf.uploaded_state
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Atm_config_limits = atm_config_limits(**row_obj)#inserting record sequence wise into respective table and saving the same
					Atm_config_limits.save()
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type, masterconf.uploaded_state, upload_time, username, reference_id)#adding the inserted data in to data update log to track back

			elif file_type.lower() == (masterconf.file_type_cra_mstr).lower():
				print ("Going for Updating CRAFEASIBILITY")
				for update_obj in update_list:
					row_obj = {}
					row_obj['site_code'] = update_obj['site_code']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['new_cra'] = update_obj['new_cra']
					row_obj['feasibility_or_loading_frequency'] = update_obj['feasibility_or_loading_frequency']
					row_obj['distance_from_hub_to_site'] = update_obj['distance_from_hub_to_site']
					row_obj['distance_from_atm_site_to_nodal_branch'] = update_obj['distance_from_atm_site_to_nodal_branch']
					row_obj['distance_from_nodal_branch'] = update_obj['distance_from_nodal_branch']
					row_obj['flm_tat'] = update_obj['flm_tat']
					row_obj['cra_spoc'] = update_obj['cra_spoc']
					row_obj['cra_spoc_contact_no'] = update_obj['cra_spoc_contact_no']
					row_obj['br_document_status'] = update_obj['br_document_status']
					row_obj['cash_van'] = update_obj['cash_van']
					row_obj['gunman'] = update_obj['gunman']
					row_obj['lc_nearest_hub_or_spoke_or_branch_from_site'] = update_obj['lc_nearest_hub_or_spoke_or_branch_from_site']
					row_obj['accessibility'] = update_obj['accessibility']
					row_obj['first_call_dispatch_time'] = update_obj['first_call_dispatch_time']
					row_obj['last_call_dispatch_time'] = update_obj['last_call_dispatch_time']
					row_obj['reason_for_limited_access'] = update_obj['reason_for_limited_access']
					row_obj['vaulting'] = update_obj['vaulting']
					row_obj['feasibility_received_date'] = update_obj['feasibility_received_date']
					row_obj['feasibility_send_date'] = update_obj['feasibility_send_date']
					row_obj['br_request_date'] = update_obj['br_request_date']
					row_obj['br_send_date'] = update_obj['br_send_date']
					row_obj['is_feasible_mon'] = update_obj['is_feasible_mon']
					row_obj['is_feasible_tue'] = update_obj['is_feasible_tue']
					row_obj['is_feasible_wed'] = update_obj['is_feasible_wed']
					row_obj['is_feasible_thu'] = update_obj['is_feasible_thu']
					row_obj['is_feasible_fri'] = update_obj['is_feasible_fri']
					row_obj['is_feasible_sat'] = update_obj['is_feasible_sat']
					row_obj['record_status'] = masterconf.uploaded_state
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					cRA_Feasibility = cra_feasibility(**row_obj)#inserting record sequence wise into respective table and saving the same
					cRA_Feasibility.save()
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type, masterconf.uploaded_state, upload_time, username, reference_id)#adding the inserted data in to data update log to track back

			elif file_type.lower() == (masterconf.file_type_feeder).lower():
				print ("Going for Updating Feeder Branch Master through screen")
				for update_obj in update_list:
					row_obj = {}
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['region_code'] = update_obj['region_code']
					row_obj['sol_id'] = update_obj['sol_id']
					row_obj['feeder_branch'] = update_obj['feeder_branch']
					row_obj['district'] = update_obj['district']
					row_obj['circle'] = update_obj['circle']
					row_obj['feeder_linked_count'] = update_obj['feeder_linked_count']
					row_obj['contact_details'] = update_obj['contact_details']
					row_obj['is_vaulting_enabled'] = update_obj['is_vaulting_enabled']
					row_obj['email_id'] = update_obj['email_id']
					row_obj['alternate_cash_balance'] = update_obj['alternate_cash_balance']
					row_obj['is_currency_chest'] = update_obj['is_currency_chest']
					row_obj['record_status'] = masterconf.uploaded_state
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Feeder_branch_master = feeder_branch_master(**row_obj)#inserting record sequence wise into respective table and saving the same
					Feeder_branch_master.save()
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type, masterconf.uploaded_state, upload_time, username, reference_id)#adding the inserted data in to data update log to track back

			elif file_type.lower() == (masterconf.file_type_ams_atm).lower():
				print ("Going for Updating ATM Master through screen")
				for update_obj in update_list:
					row_obj = {}
					row_obj['site_code'] = update_obj['site_code']
					row_obj['old_atm_id'] = update_obj['old_atm_id']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['atm_band'] = update_obj['atm_band']
					row_obj['ej_docket_no'] = update_obj['ej_docket_no']
					row_obj['grouting_status'] = update_obj['grouting_status']
					row_obj['brand'] = update_obj['brand']
					row_obj['vsat_id'] = update_obj['vsat_id']
					row_obj['vendor_name'] = update_obj['vendor_name']
					row_obj['serial_no'] = update_obj['serial_no']
					row_obj['current_deployment_status'] = update_obj['current_deployment_status']
					row_obj['tech_live_date'] = update_obj['tech_live_date']
					row_obj['cash_live_date'] = update_obj['cash_live_date']
					row_obj['insurance_limits'] = update_obj['insurance_limits']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_name'] = update_obj['bank_name']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['circle_zone_region'] = update_obj['circle_zone_region']
					row_obj['site_code_2015_16'] = update_obj['site_code_2015_16']
					row_obj['site_code_2016_17'] = update_obj['site_code_2016_17']
					row_obj['state'] = update_obj['state']
					row_obj['district'] = update_obj['district']
					row_obj['city'] = update_obj['city']
					row_obj['site_address_line_1'] = update_obj['site_address_line_1']
					row_obj['site_address_line_2'] = update_obj['site_address_line_2']
					row_obj['pincode'] = update_obj['pincode']
					row_obj['site_category'] = update_obj['site_category']
					row_obj['site_type'] = update_obj['site_type']
					row_obj['installation_type'] = update_obj['installation_type']
					row_obj['site_status'] = update_obj['site_status']
					row_obj['channel_manager_name'] = update_obj['channel_manager_name']
					row_obj['channel_manager_contact_no'] = update_obj['channel_manager_contact_no']
					row_obj['location_name'] = update_obj['location_name']
					row_obj['atm_cash_removal_date'] = update_obj['atm_cash_removal_date']
					row_obj['atm_ip'] = update_obj['atm_ip']
					row_obj['switch_ip'] = update_obj['switch_ip']
					row_obj['external_camera_installation_status'] = update_obj['external_camera_installation_status']
					row_obj['atm_owner'] = update_obj['atm_owner']
					row_obj['stabilizer_status'] = update_obj['stabilizer_status']
					row_obj['ups_capacity'] = update_obj['ups_capacity']
					row_obj['no_of_batteries'] = update_obj['no_of_batteries']
					row_obj['ups_battery_backup'] = update_obj['ups_battery_backup']
					row_obj['load_shedding_status'] = update_obj['load_shedding_status']
					row_obj['solar_dg'] = update_obj['solar_dg']
					row_obj['record_status'] = masterconf.uploaded_state
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Atm_master = atm_master(**row_obj)#inserting record sequence wise into respective table and saving the same
					Atm_master.save()
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type, masterconf.uploaded_state, upload_time, username, reference_id)#adding the inserted data in to data update log to track back
			print ("Updated record has been inserted, moved for Approval for data of {}.".format(file_type))
			apiflag = 'S'
			data_code = common_util_obj.validate_master_data(file_type, apiflag, username, reference_id)
			print ("data_code from MasterMenuUpdateController is : ",data_code)
			# data_code = common_util_obj.validate_master_data(file_type, apiflag, username, reference_id)
			status_dict["data"] = masterconf.data_inserted_successfully
			status_dict["status_code"] = data_code
			print ("status_dict from MasterMenuUpdateController is : ",status_dict)
			return status_dict
		except Exception as e:
			print("Exception in MasterMenuUpdateController ",e)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e
