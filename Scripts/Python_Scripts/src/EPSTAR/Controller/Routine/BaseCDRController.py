import os
import pandas as pd
from rest_framework.authentication import SessionAuthentication , BasicAuthentication
from rest_framework.parsers import MultiPartParser , FormParser
from rest_framework.renderers import JSONRenderer
import xlrd
import time , datetime
from Common.CommonFunctions import common_util
from Common import masterconf
import datetime
from django.db import connection
from Common.models import app_config_param
from Common.models import data_update_log
import logging


class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


class BaseCDRController ( ) :
    authentication_classes = []
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    data_update_log_id = 0
    maintain_log_id = 0

    def handle_upload_request ( self , request_post , input_filepath , filehandle_name , str_file_extention ,
                                userinfo ) :
        common_util_obj = common_util.common_utils ( )
        strfile_type = request_post['file_type']
        strbank_code = request_post['bank_code']
        upload_time = datetime.datetime.fromtimestamp ( time.time ( ) ).strftime ( '%Y-%m-%d %H:%M:%S' )
        actual_upload_time = request_post['upload_datatime']
        username = userinfo['username'].username
        reference_id = userinfo['reference_id']
        uploaded_status = masterconf.uploaded_state
        failed_status = masterconf.failed_status
        Project_id = request_post['project_id']
        region = request_post['region']
        if region == '' or region == 'undefined' or region == 'none':
            region = masterconf.all_region
        # print ("region : ", region)
        # print ("request_post['region'] : ", request_post['region'])
        status_flag = 0
        status_dict = { }
        format_status = masterconf.no_bank
        logger = logging.getLogger(__name__)
        logger.info ( "In handle_upload_request method of BaseCDRController" )
        try :
            if strbank_code == masterconf.bankcode_bomh :
                logger.info ( "Inside CDR:BOMH, reading input files" )
                try :
                    wb = xlrd.open_workbook ( input_filepath )
                    df_data = pd.read_excel ( wb , skiprows=1 , engine='xlrd' )
                    column_names = list ( df_data.columns.values )
                    mod_column_names = [column_names[0] , column_names[1] , 'A' , column_names[2] , ]
                    df_data = pd.read_excel ( wb , skiprows=1 , engine='xlrd' ,
                                              names=[mod_column_names[0] , mod_column_names[1] , mod_column_names[2] ,
                                                     mod_column_names[3]] )
                    df_data = df_data.drop ( columns=['A'] )
                    df_data = df_data.drop ( df_data.index[len ( df_data ) - 1] )
                    df_data['DISPENSED AMT'] = df_data['DISPENSED AMT'].astype ( int )
                except Exception as e :
                    logger.error('Exception while reading BOMH Dispense Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                ###Validate format
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CDR:BOMH Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv(df_enhanced, filehandle_name, request_post,
                                                                 userinfo)  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code , actual_upload_time ,
                                                           uploaded_status , upload_time , Project_id , username ,
                                                           df_enhanced['created_reference_id'][0] , region )
                    # data_status = common_util_obj.validate_data(strfile_type,upload_time,strbank_code,uploaded_status,df_enhanced['created_reference_id'][0],username,actual_upload_time,Project_id)
                    data_status = self.validate_data_cdr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cdr for BOMH :: %s", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]  # ES1-I44 Issue Change
                    if data_status["status_text"] == masterconf.no_record :  # ES1-I44 Issue Change
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]  # ES1-I44 Issue Change
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :  # ES1-I44 Issue Change
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for BOMH CDR ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :  # ES1-I44 Issue Change
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict

            elif strbank_code == masterconf.bankcode_sbi :
                logger.info ( "Inside CDR:SBI, reading input files" )
                try :
                    digits = ['0' , '1' , '2' , '3' , '4' , '5' , '6' , '7' , '8' , '9']
                    df_data = pd.read_excel ( input_filepath )
                    df_data.dropna ( subset=['ATM ID'] ,
                                     inplace=True )  # This will drop rows with empty ATM ID - issue - ES1-I54
                    column_names = list ( df_data.columns.values )
                    if column_names[0] == 'ATM ID' :
                        column_names[0] = 'atm_id'
                    ############################### ES1-I44 Issue Change starts#####################################
                    ##### Translating column names for format validation ES1-I44 ####################################
                    cash_withdrawal = str ( column_names[1] ).translate (
                        { ord ( k ) : None for k in digits } ).lower ( )
                    opening_cash = str ( column_names[2] ).translate ( { ord ( k ) : None for k in digits } ).lower ( )
                    total_cash = str ( column_names[3] ).translate ( { ord ( k ) : None for k in digits } ).lower ( )
                    ##################################################################################################
                    if cash_withdrawal == masterconf.cash_disp_sbi_column :
                        column_names[1] = 'cash_withdrawal_by_cust'
                    if opening_cash == masterconf.opening_bal_disp_sbi_col :
                        column_names[2] = 'opening_cash_in_the_atm'
                    if total_cash == masterconf.total_cash_disp_sbi_col :
                        column_names[3] = 'total_cash_replenished_on'
                    df_data.columns = column_names
                    df_data = common_util_obj.check_null_dataframe ( df_data )
                # print(" df_data ::::: ", df_data)
                ############################### ES1-I44 Issue Change ends#####################################
                # print(df_data.columns.values)
                except Exception as e :
                    logger.error('Exception while reading SBI Dispense Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                ##print ("column_names : {}".format(df_data))
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                # print ("format_status from SBI bank: {}".format(format_status))
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CDR:SBI Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv(df_enhanced, filehandle_name, request_post,
                                                                 userinfo)  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name );
                    # print("#printing Uploaded path: {}".format(uploaded_path))
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code , actual_upload_time ,
                                                           uploaded_status , upload_time , Project_id , username ,
                                                           df_enhanced['created_reference_id'][0] , region )
                    # data_status = common_util_obj.validate_data(strfile_type,upload_time,strbank_code,uploaded_status,df_enhanced['created_reference_id'][0],username,actual_upload_time,Project_id)
                    data_status = self.validate_data_cdr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cdr for SBI :: %s", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]  # ES1-I44 Issue Change
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]  # ES1-I44 Issue Change
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :  # ES1-I44 Issue Change
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for SBI CDR ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :  # ES1-I44 Issue Change
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict

            # elif strbank_code == 'RSBL':
            # 	logger.setLevel(logging.INFO)
            # 	logger.info("Inside CDR:RSBL, reading input files")
            # 	try:
            # 		df_data = pd.read_csv(input_filepath)
            # 		column_names = list(df_data.columns.values)
            # 		column_names[0] = (''.join([i for i in column_names[0] if not i.isdigit()])).lower()
            # 		if column_names[0] == 'substr(trl_datetime_local_txn,,)':
            # 			column_names[0] = 'substr_trl_datetime_local_txn'
            # 		if column_names[1] == 'TRL_CARD_ACPT_TERMINAL_IDENT':
            # 			column_names[1] = column_names[1].lower()
            # 		if column_names[2] == 'SUM(TRL_AMT_TXN)':
            # 			column_names[2] = 'sum_trl_amt_txn'
            # 		if column_names[3] == 'COUNT(TRL_CARD_ACPT_TERMINAL_IDENT)':
            # 			column_names[3] = 'count_trl_card_acpt_terminal_ident'
            # 		#print ("Inside RSBL :")
            # 		#print ("column_names : {}".format(column_names))
            # 		df_data = pd.read_csv(input_filepath,skiprows=[0],names=[column_names[0],column_names[1],column_names[2],column_names[3]])
            # 	except Exception as e:
            # 		common_util_obj = common_util.common_utils()
            # 		logger = common_util_obj.initiateLogger()
            # 		logger.setLevel(logging.ERROR)
            # 		logger.error('Exception in handle_upload_request method of BaseCDRController', exc_info=True)
            # 		status_dict["level"] = masterconf.format_level
            # 		status_dict["status_text"] = masterconf.incorrect_input_file
            # 		return status_dict
            # 	format_status = common_util_obj.validate_format_xls(df_data, strfile_type, strbank_code)
            # 	if format_status.lower() == masterconf.format_val_success_status.lower():
            # 		df_enhanced = common_util_obj.add_fields_to_df(df_data,uploaded_status,upload_time,username,actual_upload_time,Project_id,reference_id)
            # 		csv_file_path = common_util_obj.write_to_csv(df_enhanced,filehandle_name)
            # 		uploaded_file_name = common_util_obj.upload_file_sftp(csv_file_path)
            # 		uploaded_path = os.path.join(masterconf.sfpt_file_path,uploaded_file_name)
            # 		common_util_obj.load_data_database(uploaded_path,strfile_type,strbank_code)
            # 		id = common_util_obj.maintainlog(strfile_type,strbank_code,actual_upload_time,uploaded_status,upload_time,Project_id,username,df_enhanced['created_reference_id'][0])
            # 		#data_status = common_util_obj.validate_data(strfile_type,upload_time,strbank_code,uploaded_status,df_enhanced['created_reference_id'][0],username,actual_upload_time,Project_id)
            # 		data_status = self.validate_data_cdr(strfile_type,upload_time,strbank_code,uploaded_status,df_enhanced['created_reference_id'][0],username,actual_upload_time,Project_id)
            # 		status_dict["level"] = masterconf.data_level
            # 		status_dict["status_text"] = data_status["status_text"]  #ES1-I44 Issue Change
            # 		if data_status["status_text"] == masterconf.no_record:
            # 			status_dict["level"] = masterconf.data_level
            # 			status_dict["status_text"] = data_status["status_text"]  #ES1-I44 Issue Change
            # 			status_dict["file_status"] = "complete file failed"
            # 			return status_dict
            # 		elif data_status["status_text"] == masterconf.data_validated_successful or data_status["status_text"] == masterconf.partial_valid_file:  #ES1-I44 Issue Change
            # 			result = self.status_update_and_consolidate(strfile_type,upload_time,strbank_code,'Approved',df_enhanced['created_reference_id'][0],username,actual_upload_time,Project_id);
            # 			#print ("\n\nresult : ",result)
            # 			#print ("status_dict from here : ",status_dict)
            # 			if result == masterconf.Consolidation_Successful and status_dict["status_text"] == masterconf.data_validated_successful:
            # 				status_dict["level"] = masterconf.data_level
            # 				status_dict["status_text"] = masterconf.data_uploaded_successful
            # 				status_dict["file_status"] = masterconf.data_validated_successful
            # 				return status_dict
            # 			else:
            # 				status_dict["level"] = masterconf.data_level
            # 				status_dict["status_text"] = data_status["status_text"]+data_status["data"]
            # 				status_dict["file_status"] = data_status["status_text"]
            # 				return status_dict
            # 		else:  #ES1-I44 Issue Change
            # 			status_dict["level"] = masterconf.data_level
            # 			status_dict["status_text"] = data_status["status_text"]
            # 			status_dict["file_status"] = data_status["status_text"]
            # 			return status_dict
            #
            elif strbank_code == masterconf.bankcode_cab :
                logger.info ( "Inside CDR:CAB, reading input files" )
                try :
                    df_data = pd.read_fwf ( input_filepath , skiprows=[0 , 1] )
                    column_names = list ( df_data.columns.values )
                    a = column_names[0].split ( ':' )
                    file_date = a[1]
                    df_data = pd.read_fwf ( input_filepath , skiprows=[0 , 1 , 2 , 3 , 4 , 5 , 6 , 7] )
                    df_data = df_data.drop ( df_data.columns[0] , axis=1 )
                    fd = file_date.split ( '-' )
                    months = {
                        'jan' : '01' ,
                        'feb' : '02' ,
                        'mar' : '03' ,
                        'apr' : '04' ,
                        'may' : '05' ,
                        'jun' : '06' ,
                        'jul' : '07' ,
                        'aug' : '08' ,
                        'sep' : '09' ,
                        'oct' : '10' ,
                        'nov' : '11' ,
                        'dec' : '12' ,
                    }
                    yy = fd[2]
                    mm = months[fd[1].lower ( )]
                    dd = int ( fd[0] )
                    if dd in [1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9] :
                        dd = "0" + str ( dd )
                    else :
                        dd = str ( dd )
                    string = yy + mm + dd
                    new_date = datetime.datetime.strptime ( string , '%Y%m%d' ).date ( )
                    df_data = pd.read_fwf ( input_filepath , skiprows=[0 , 1 , 2 , 3 , 4 , 7] )
                    column_names = list ( df_data.columns.values )
                    mod_column_names = []
                    for column_name in column_names :
                        if column_name == 'UNAPPROVED' or column_name == 'APPROVED' or column_name == 'APPROVED+DECLINED' or column_name == 'APPROVED.1' :
                            mod_column_names.append ( column_name.lower ( ) )
                    for i in range ( len ( mod_column_names ) ) :
                        if mod_column_names[i] == 'approved+declined' :
                            mod_column_names[i] = 'approved_declined'
                        elif mod_column_names[i] == 'approved.1' :
                            mod_column_names[i] = 'approved'
                        else :
                            mod_column_names[i] = mod_column_names[i]
                    df_data = pd.read_fwf ( input_filepath , skiprows=[0 , 1 , 2 , 3 , 4 , 5 , 7] )
                    df_data = df_data.drop ( df_data.columns[0] , axis=1 )
                    column_names_headers = list ( df_data.columns.values )

                    if len ( column_names_headers ) == 12 :
                        if column_names_headers[4] == 'NON' and column_names_headers[5] == 'FIN' :
                            column_names_headers[4] = column_names_headers[4].lower ( ) + "_" + column_names_headers[
                                5].lower ( )
                            del column_names_headers[5]
                        if column_names_headers[5] == 'FIN.1' :
                            column_names_headers[5] = 'fin'
                        if column_names_headers[6] == 'NON.1' and column_names_headers[7] == 'FIN.2' :
                            column_names_headers[6] = 'non'
                            column_names_headers[7] = 'fin'
                            column_names_headers[6] = column_names_headers[6].lower ( ) + "_" + column_names_headers[
                                7].lower ( )
                            del column_names_headers[7]
                        if column_names_headers[7] == 'FIN.3' :
                            column_names_headers[7] = 'fin'
                        if column_names_headers[9] == 'AMT DISPENSED' :
                            column_names_headers[9] = 'amt_dispensed'
                        if column_names_headers[0] == 'TERM ID' :
                            column_names_headers[0] = 'term_id'
                        if column_names_headers[2] == 'TERM CITY' :
                            column_names_headers[2] = 'term_city'
                        if column_names_headers[3] == 'TERM LOCATION' :
                            column_names_headers[3] = 'term_location'
                        # print ("Before : {}".format(column_names_headers))
                        for i in range ( len ( mod_column_names ) - 1 ) :
                            if mod_column_names[i] == 'unapproved' :
                                column_names_headers[4] = mod_column_names[i] + "_" + column_names_headers[4]
                                column_names_headers[5] = mod_column_names[i] + "_" + column_names_headers[5]
                            if mod_column_names[i] == 'approved' :
                                column_names_headers[6] = mod_column_names[i] + "_" + column_names_headers[6]
                                column_names_headers[7] = mod_column_names[i] + "_" + column_names_headers[7]
                                column_names_headers[9] = mod_column_names[i] + "_" + column_names_headers[9]
                            if mod_column_names[i] == 'approved_declined' :
                                column_names_headers[8] = mod_column_names[i] + "_" + column_names_headers[8]
                        ##print (column_names_headers)
                        df_data = pd.read_fwf ( input_filepath , skiprows=[0 , 1 , 2 , 3 , 4 , 5 , 6 , 7] ,
                                                names=[column_names_headers[0].lower ( ) ,
                                                       column_names_headers[1].lower ( ) ,
                                                       column_names_headers[2].lower ( ) ,
                                                       column_names_headers[3].lower ( ) ,
                                                       column_names_headers[4].lower ( ) ,
                                                       column_names_headers[5].lower ( ) ,
                                                       column_names_headers[6].lower ( ) ,
                                                       column_names_headers[7].lower ( ) ,
                                                       column_names_headers[8].lower ( ) ,
                                                       column_names_headers[9].lower ( )] )
                        df_data['file_date'] = new_date
                except Exception as e :
                    logger.error('Exception while reading CAB Dispense Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CDR:CAB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv(df_enhanced, filehandle_name, request_post,
                                                                 userinfo)  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name );
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code , actual_upload_time ,
                                                           uploaded_status , upload_time , Project_id , username ,
                                                           reference_id , region )
                    data_status = self.validate_data_cdr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           reference_id , username , actual_upload_time , Project_id ,
                                                           region )
                    logger.info("data_status from validate_data_cdr for CAB :: %s", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]  # ES1-I44 Issue Change
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]  # ES1-I44 Issue Change
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :  # ES1-I44 Issue Change
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' , reference_id , username ,
                                                                      actual_upload_time , Project_id , region )
                        logger.info("result from status_update_and_consolidate for CAB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :  # ES1-I44 Issue Change
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict

            elif strbank_code == masterconf.bankcode_ubi :
                logger.info ( "Inside CDR:UBI, reading input files" )
                try :
                    df_data = pd.read_csv ( input_filepath , delimiter=',' ,
                                            names=['1' , '2' , '3' , '4' , '5' , '6' , 'type_01' , 'type_02' ,
                                                   'type_03' , 'type_04' , '11' , '12' , '13' , '14' , '15' , '16' ,
                                                   '17' , '18' , '19' , '20' , '21' , '22' , '23' , '24' , '25' ,
                                                   'atm_id'] )
                    cols = [0 , 1 , 2 , 3 , 4 , 5 , 10 , 11 , 12 , 13 , 14 , 15 , 16 , 17 , 18 , 19 , 20 , 21 , 22 ,
                            23 , 24]
                    df_data.drop ( df_data.columns[cols] , axis=1 , inplace=True )
                except Exception as e :
                    logger.error('Exception while reading UBI Dispense Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CDR:UBI Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv(df_enhanced, filehandle_name, request_post,
                                                                 userinfo)  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code , actual_upload_time ,
                                                           uploaded_status , upload_time , Project_id , username ,
                                                           df_enhanced['created_reference_id'][0] , region )
                    data_status = self.validate_data_cdr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cdr for UBI :: %s", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]  # ES1-I44 Issue Change
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]  # ES1-I44 Issue Change
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :  # ES1-I44 Issue Change
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for UBI ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :  # ES1-I44 Issue Change
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict

        except Exception as e :
            entry = data_update_log.objects.get ( id=self.maintain_log_id )
            entry.record_status = 'Failed'
            entry.save ( )
            logger.error ( 'Exception in handle_upload_request method of BaseCDRController ::: %s' , e)
            raise e

    def validate_data_cdr ( self , strfile_type , createddate , bankcode , recordstatus , referenceid , systemUser ,
                            actual_upload_time , Project_id , region ) :
        logger = logging.getLogger(__name__)
        logger.info("Inside validate_data_cdr")
        try :
            data_status_return = { }
            nocount = """ SET NOCOUNT ON; """
            sql_str = "exec uspValidateDispenseFiles " + "'" + actual_upload_time + "', " + "'" + bankcode + "', " + "'" + Project_id + "', " + "'" + region + "', " + "'" + recordstatus + "', " + "'" + referenceid + "'" + ", " + "'" + systemUser + "'"
            logger.info("sql_str for  uspValidateCbrFiles or uspValidateDispenseFiles: %s ", sql_str )
            cur = connection.cursor ( )
            cur.execute ( nocount + sql_str )
            data_seq_no = cur.fetchone ( )
            logger.info("data_seq_no: %s ", data_seq_no )
            data_status = app_config_param.objects.filter ( sequence=int ( data_seq_no[0] ) ).values ( 'value' );
            if data_seq_no[0] == '50001' :
                all_codes = data_update_log.objects.filter ( created_reference_id=referenceid ).values (
                    'validation_code' )
                all_codes_list = list ( all_codes[0]['validation_code'].split ( "," ) )
                error_code_str = ''
                for i in all_codes_list :
                    error_code = app_config_param.objects.filter ( sequence=i ).values ( 'value' )
                    error_code_str = error_code_str + ", " + error_code[0]['value']
                data_status_return["status_text"] = data_status[0]['value']
                data_status_return["data"] = error_code_str
                return data_status_return
            data_status_return["status_text"] = data_status[0]['value']
            data_status_return["data"] = data_status[0]['value']
            return data_status_return

        except Exception as e :
            logger.error ( 'Exception in validate_data_cdr method of BaseCDRController ::: %s' , e)
            raise e

    def status_update_and_consolidate ( self , strfile_type , createddate , bankcode , recordstatus , referenceid ,
                                        systemUser , actual_upload_time , Project_id , region ) :
        logger = logging.getLogger(__name__)
        logger.info( "Inside status_update_and_consolidate of BaseCDRController" )
        nocount = """ SET NOCOUNT ON; """
        try :
            procedure_name2 = 'uspConsolidateCDRFiles'
            cur = connection.cursor ( )
            sql_str3 = "EXEC " + procedure_name2 + " '" + actual_upload_time + "','" + bankcode + "','" + Project_id + "','" + region + "','" + referenceid + "','" + systemUser + "'"
            logger.info("Calling SP from status_update_and_consolidate : %s " , sql_str3)
            cur.execute ( nocount + sql_str3 )
            data_seq_no = cur.fetchone ( )
            data_status = app_config_param.objects.filter ( sequence=int ( data_seq_no[0] ) ).values ( 'value' );
            cur.close ( )
            logger.info("data_status returned from app_config_param : %s ", data_status)
            return data_status[0]['value']
        except Exception as e :
            logger.error ( 'Exception in status_update_and_consolidate method of BaseCDRController ::: %s ' , e)
            raise e
