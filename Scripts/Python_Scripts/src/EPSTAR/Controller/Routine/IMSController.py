import datetime
import logging
import os
import time

import pandas as pd
from Common import db_queries_properties
from Common import masterconf
from Common.CommonFunctions import common_util
from Common.models import app_config_param
from Common.models import data_update_log
from django.db import connection
from rest_framework.authentication import SessionAuthentication
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class IMSController():
    authentication_classes = [];
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    data_update_log_id = 0
    maintain_log_id = 0

    def handle_upload_request(self, request_post, input_filepath, filehandle_name, str_file_extention, userinfo):
        print("Inside handle_upload_request of IMSController")
        common_util_obj = common_util.common_utils()
        strfile_type = request_post['file_type']
        actual_upload_time = datetime.datetime.strftime(datetime.datetime.strptime(request_post['date'],'%Y-%m-%d'),'%Y-%m-%d %H:%M:%S')
        # actual_upload_time = request_post['date']
        upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        username = userinfo['username'].username
        reference_id = userinfo['reference_id']
        uploaded_status = "Uploaded";
        failed_status = "Failed";
        Project_id = ' '
        status_flag = 0
        status_dict = {}
        strbank_code = ''
        format_status = masterconf.no_bank
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("In handle_upload_request method of IMSController")
        try:
            if strfile_type.upper() == masterconf.file_type_ims.upper():
                logger.setLevel(logging.INFO)
                logger.info("In IMSController")
                try:
                    ######################Reading IMS file ####################################
                    df_data = pd.read_excel(input_filepath)
                    # print('df_data',df_data)
                    for idx in df_data.index:
                        df_data['Remarks'].loc[idx] = str(df_data['Remarks'].loc[idx]).replace('\W', ' ')
                        df_data['Remarks'].loc[idx] = str(df_data['Remarks'].loc[idx])[:3000]
                    for column in db_queries_properties.ims_columns:
                        if column in df_data:
                            del df_data[column]
                except Exception as e:
                    common_util_obj = common_util.common_utils()
                    logger = common_util_obj.initiateLogger()
                    logger.setLevel(logging.ERROR)
                    logger.error('Exception in handle_upload_request method of IMSController', exc_info=True)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                logger.setLevel(logging.INFO)
                logger.info("In IMSController, going for format validation")
                strbank_code = ''
                ######################Going for format validation################################
                format_status = common_util_obj.validate_format_xls(df_data, strfile_type.upper(), strbank_code)
                if format_status.lower() == masterconf.format_val_success_status.lower():
                    print("Format Status : ", )
                    strbank_code = ''
                    logger.setLevel(logging.INFO)
                    logger.info("In IMSController, format validation successful")
                    df_enhanced = common_util_obj.add_fields_to_df(df_data, uploaded_status, upload_time, username,
                                                                   actual_upload_time, Project_id, reference_id)
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    ######################Loading fetched data to database##########################################################
                    common_util_obj.load_data_database(uploaded_path, strfile_type.upper(), strbank_code)
                    id = common_util_obj.maintainlog(strfile_type.upper(), strbank_code, actual_upload_time,
                                                     uploaded_status, upload_time, Project_id, username,
                                                     df_enhanced['created_reference_id'][0])
                    data_status = self.validate_data_ims(strfile_type, upload_time, strbank_code, uploaded_status,
                                                         df_enhanced['created_reference_id'][0], username,
                                                         actual_upload_time, Project_id)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status
                    ##########################When No data is found ############################################
                    if data_status == masterconf.no_record:
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    ################when file is complete successful or partial ###############################################
                    elif data_status == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file:
                        recordstatus = 'Active'
                        result = self.setstatus(strfile_type.upper(), upload_time, strbank_code, recordstatus,
                                                df_enhanced['created_reference_id'][0], username, actual_upload_time,
                                                Project_id)
                        if result == masterconf.status_update_success and status_dict[
                            "status_text"] == masterconf.data_validated_successful:
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else:
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                else:
                    common_util_obj = common_util.common_utils()
                    logger = common_util_obj.initiateLogger()
                    logger.setLevel(logging.ERROR)
                    logger.error('Exception in handle_upload_request method of BaseVCBControllerEOD', exc_info=True)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict
        except Exception as e:
            print("In Controller Exception", e)
            entry = data_update_log.objects.get(id=self.maintain_log_id)
            entry.record_status = 'Failed'
            entry.save()
            common_util_obj = common_util.common_utils()
            logger = common_util_obj.initiateLogger()
            logger.setLevel(logging.ERROR)
            logger.error('Exception in handle_upload_request method of BaseVCBControllerEOD', exc_info=True)
            raise e

    ##########################validating data in DB#################################################################
    def validate_data_ims(self, strfile_type, createddate, bankcode, recordstatus, referenceid, systemUser,
                          actual_upload_time, Project_id):
        try:
            common_util_obj = common_util.common_utils()
            logger = common_util_obj.initiateLogger()
            logger.setLevel(logging.INFO)
            logger.info("In validate_data_ims method of iMSController")
            nocount = """ SET NOCOUNT ON; """
            sql_str = "exec uspDataValidationIMS " + "'" + actual_upload_time + "', " + "'" + recordstatus + "', " + "'" + referenceid + "'" + ", " + "'" + systemUser + "'"
            cur = connection.cursor()
            print("Calling data sp : ", sql_str)
            cur.execute(nocount + sql_str)
            data_seq_no = cur.fetchone()
            print("data_seq_no: {}".format(data_seq_no))
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            print("data_status : {}".format(data_status))
            print("data_status_value : {}".format(data_status[0]['value']))

            if data_seq_no[0] == '50001':
                all_codes = data_update_log.objects.filter(created_reference_id=referenceid).values('validation_code')
                print(all_codes[0]['validation_code'])
                print(type(all_codes[0]['validation_code']))
                all_codes_list = list(all_codes[0]['validation_code'].split(","))
                print("all_codes_list : ", all_codes_list)
                error_code_str = ''
                data_status_return = {}
                for i in all_codes_list:
                    error_code = app_config_param.objects.filter(sequence=i).values('value')
                    error_code_str = error_code_str + ", " + error_code[0]['value']
                data_status_return["status_text"] = data_status[0]['value']
                data_status_return["data"] = error_code_str
                print("data_status_return : ", data_status_return)
                return data_status_return
            return data_status[0]['value']

        except Exception as e:
            logger = common_util_obj.initiateLogger()
            logger.setLevel(logging.ERROR)
            logger.error('Exception in validate_data_VCB method of BaseVCBControllerEOD', exc_info=True)
            raise e

    ############################Setting the status as per the record ##############################################
    def setstatus(self, strfile_type, createddate, bankcode, recordstatus, referenceid, systemUser, actual_upload_time,
                  Project_id):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("In setstatus method of iMSController")
        strfile_type_1 = ''
        nocount = """ SET NOCOUNT ON; """
        # print ("strfile_type : {}".format(strfile_type))
        table_name_key = strfile_type + "_" + bankcode + "_TABLE"
        table_name = db_queries_properties.table_dict[table_name_key]
        try:
            # print("In setstatus")
            procedure_name1 = 'uspSetStatus_pycaller'
            cur = connection.cursor()
            table_name1 = 'data_update_log'
            activerecordstatus = 'Active'
            sql_str3 = "DECLARE @out varchar(max) EXEC " + procedure_name1 + " '" + table_name1 + "','Approved','" + activerecordstatus + "','" + createddate + "','" + systemUser + "','" + referenceid + "',' datafor_date_time = ''" + actual_upload_time + "'' and data_for_type = ''" + strfile_type + "'' ',@out OUTPUT select @out"
            cur.execute(nocount + sql_str3)
            sql_str2 = "DECLARE @out varchar(max) EXEC " + procedure_name1 + " '" + table_name + "','Approved','" + recordstatus + "','" + createddate + "','" + systemUser + "','" + referenceid + "',' datafor_date_time = ''" + actual_upload_time + "'' and project_id = ''" + Project_id + "'' ',@out OUTPUT select @out"
            cur.execute(nocount + sql_str2)
            data_seq_no = cur.fetchone()
            # print ("data_seq_no: {}".format(data_seq_no))
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # print("data_status : {}".format(data_status))
            # print("data_status_value : {}".format(data_status[0]['value']))
            # print ("data_seq_no: {}".format(data_seq_no))
            # print ("#printing from validate_and_consoldate_data")
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # print("data_status : {}".format(data_status))
            # print("data_status_value : {}".format(data_status[0]['value']))
            cur.close()
            return data_status[0]['value']
        except Exception as e:
            logger = common_util_obj.initiateLogger()
            logger.setLevel(logging.ERROR)
            logger.error('Exception in status_update_and_consolidate method of BaseVCBControllerEOD', exc_info=True)
            raise e
