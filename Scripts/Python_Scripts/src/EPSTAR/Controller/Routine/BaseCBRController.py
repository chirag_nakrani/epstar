import os
import pandas as pd
from rest_framework.authentication import SessionAuthentication
import xlrd
import datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
from Common.models import data_update_log
import logging
from datetime import timedelta


class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


class BaseCBRController ( ) :
    data_update_log_id = 0
    maintain_log_id = 0

    def handle_upload_request ( self , request_post , input_filepath , filehandle_name , str_file_extention ,
                                userinfo ) :
        common_util_obj = common_util.common_utils ( )
        logger = logging.getLogger(__name__)
        logger.info ( "In handle_upload_request method of BaseCBRController" )
        strfile_type = request_post['file_type']
        strbank_code = request_post['bank_code']
        actual_upload_time = request_post['upload_datatime']
        upload_time = datetime.datetime.utcnow ( ) + timedelta ( hours=5 , seconds=1800 )
        username = userinfo['username'].username
        reference_id = userinfo['reference_id']
        uploaded_status = masterconf.uploaded_state
        failed_status = masterconf.failed_status
        Project_id = request_post['project_id']
        region = request_post['region']
        if region == '' or region == 'undefined' or region == 'none':
            region = masterconf.all_region
        status_flag = 0
        status_dict = { }
        format_status = masterconf.no_bank
        try :
            if strbank_code == masterconf.bankcode_bomh :  # for CBR of BOMH
                logger.info ( "CBR:BOMH file process gets started." )
                try :
                    df_data = common_util_obj.read_file_xls ( input_filepath )
                except Exception as e :
                    logger.error ( 'Exception while reading BOMH Bank file %s ' , e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:BOMH Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for BOMH :: %s", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for BOMH :: %s", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_alb :
                logger.info ( "CBR:ALB file process gets started." )
                try :
                    df_data = pd.read_csv ( input_filepath )
                    df_data.drop ( df_data.columns[len ( df_data.columns ) - 1] , axis=1 , inplace=True )
                except Exception as e :
                    logger.error ( 'Exception while reading ALB Bank file %s ' , e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:ALB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for ALB ::: %s ",data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    print("data_status returned from SP : " , data_status)
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for ALB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_boi :
                logger.info ( "CBR:BOI file process gets started." )
                try :
                    df_data = pd.read_csv ( input_filepath )
                    df_data.drop ( df_data.columns[len ( df_data.columns ) - 1] , axis=1 , inplace=True )
                except Exception as e :
                    logger.error('Exception while reading BOI Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:BOI Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    # print("#printing Uploaded path: {}".format(uploaded_path))
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for BOI ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for BOI ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_cab :
                try :
                    df_data = pd.read_excel ( input_filepath)
                    if 'ATM .1' in df_data.columns:
                        df_data.rename({'ATM .1': 'ATM_Status'}, axis='columns', inplace=True)
                    if 'Line'in df_data.columns:
                        df_data.rename({'Line' : 'Network_Link_Status'}, axis='columns', inplace=True)
                    if 'Out ' in df_data.columns:
                        df_data.rename({'Out ':'out_time'}, axis='columns', inplace=True)
                    if 'Last' in df_data.columns:
                        df_data.rename({'Last' : 'last_tran'}, axis='columns', inplace=True)
                    if 'Last.1' in df_data.columns:
                        df_data.rename({'Last.1': 'last_up'}, axis='columns', inplace=True)
                    if 'Hop5' in df_data.columns:
                        df_data.rename({'Hop5': 'hop5_cash'}, axis='columns', inplace=True)
                    if 'End ' in df_data.columns:
                        df_data.rename({'End ':'end_cash'}, axis='columns', inplace=True)
                    if 'hop5_cash' not in df_data.columns and 'Circle' not in df_data.columns:
                        df_data.rename({'end_cash':'End Cash'}, axis='columns', inplace=True)
                        df_data.insert(15, 'end_cash', df_data['End Cash'].values)
                        del df_data['End Cash']
                        df_data.insert(16,'last_tran',0)
                        df_data.insert(17,'last_up',0)
                        df_data.insert(26,'hop5_cash',0)
                    if 'Circle' in df_data.columns:
                        df_data.insert(6, 'out_time', 0)
                        df_data.insert(15, 'end_cash', df_data['Total Cash Balance'].values)
                        del df_data['Total Cash Balance']
                        df_data.insert(16,'last_tran',0)
                        df_data.insert(17,'last_up',0)
                        df_data.rename({'ATM Location Name': 'Term', 'City' : 'Term.1', 'Circle' : 'Regn', 'ATM Machine Error 1': 'Err_', 'ATM Machine Error 2': 'Err .1', 'ATM Machine Error 3':'Err .2', 'ATM Machine Error 4':'Err .3', 'ATM Machine Error 5':'Err .4', 'ATM Machine Error 6':'Err .5', 'ATM Machine Error 7':'Err .6', 'ATM Machine Error 8':'Err .7', 'Cash Balance': 'Hop1', 'Rupee Note': 'Hop1.1', 'Cash Balance.1': 'Hop2', 'Rupee Note.1':'Hop2.1', 'Cash Balance.2': 'Hop3', 'Rupee Note.2':'Hop3.1', 'Cash Balance.3': 'Hop4', 'Rupee Note.3': 'Hop4.1'}, axis='columns', inplace=True)
                        df_data.insert(26,'hop5_cash',0)
                    df_data.drop(df_data.index[[0]], inplace=True)
                    df_data['end_cash'] = df_data['end_cash'].astype(int)
                    df_data['Hop1'] = df_data['Hop1'].astype(int)
                    df_data['Hop1.1'] = df_data['Hop1.1'].astype(int)
                    df_data['Hop2'] = df_data['Hop2'].astype(int)
                    df_data['Hop2.1'] = df_data['Hop2.1'].astype(int)
                    df_data['Hop3'] = df_data['Hop3'].astype(int)
                    df_data['Hop3.1'] = df_data['Hop3.1'].astype(int)
                    df_data['Hop4'] = df_data['Hop4'].astype(int)
                    df_data['Hop4.1'] = df_data['Hop4.1'].astype(int)
                    df_data['hop5_cash'] = df_data['hop5_cash'].astype(int)
                except Exception as e :
                    print ("Exception from CBR:CAB : ",e)
                    logger.error('Exception while reading CAB Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:CAB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             reference_id ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           reference_id , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for CAB ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      reference_id ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for CAB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_cbi :
                logger.info ( "CBR:CBI file process gets started." )
                try :
                    df_data = pd.read_csv ( input_filepath ,usecols = [0,1,2,3,4,5,6,7], skiprows=[1])
                    column_names = list ( df_data.columns.values )
                    for i in range ( len ( column_names ) ) :
                        column_names[i] = column_names[i].strip ( )
                    # print ("column_names : {}".format(column_names))
                    df_data = pd.read_csv ( input_filepath , skiprows=[0,1] ,
                                            names=[str ( column_names[0] ) , str ( column_names[1] ) ,
                                                   str ( column_names[2] ) , str ( column_names[3] ) ,
                                                   str ( column_names[4] ) , str ( column_names[5] ) ,
                                                   str ( column_names[6] ) , str ( column_names[7] )] ,
                                            index_col=False )
                    # print('df_data',df_data)
                except Exception as e :
                    logger.error('Exception while reading CBI Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:CBI Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for CBI ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for CBI ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_corp :
                logger.info ( "CBR:CORP file process gets started." )
                try :
                    wb = xlrd.open_workbook ( input_filepath )
                    df_data = pd.read_excel ( wb , engine='xlrd' , usecols="A:E" ).fillna(0)
                    df_data['100s'] = df_data['100s'].astype(int)
                    df_data['200s'] = df_data['200s'].astype(int)
                    df_data['500s'] = df_data['500s'].astype(int)
                    df_data['2000s'] = df_data['2000s'].astype(int)
                    print('df_data',df_data)
                except Exception as e :
                    logger.error('Exception while reading CORP Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:CORP Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    logger.info("CORP Data frame columns ::: %s " , df_enhanced.columns)
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for CORP ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for CORP ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_dena :
                logger.info ( "CBR:DEB file process gets started." )
                try :
                    wb = xlrd.open_workbook ( input_filepath )
                    df_data = pd.read_excel ( wb , skiprows=2 , engine='xlrd' )
                    df_data1 = pd.read_excel ( input_filepath , header=None )
                    df1 = common_util_obj.fetchColumnName ( df_data1 )
                    df_data = pd.read_excel ( wb , skiprows=[0] , engine='xlrd' ,
                                              names=[df1[0] , df1[1] , df1[2] , df1[3] , df1[4] , df1[5] , df1[6] ,
                                                     df1[7] , df1[8] , df1[9] , df1[10] , df1[11] , df1[12] , df1[13] ,
                                                     df1[14] , df1[15] , df1[16] , df1[17] , df1[18] , df1[19] ,
                                                     df1[20] , df1[21] , df1[22] , df1[23] , df1[24] , df1[25] ,
                                                     df1[26] , df1[27] , df1[28] , df1[29] , df1[30] , df1[31] ,
                                                     df1[32] , df1[33] , df1[34] , df1[35] , df1[36] , df1[37] ,
                                                     df1[38] , df1[39] , df1[40] , df1[41] , df1[42] , df1[43] ,
                                                     df1[44] , df1[45] , df1[46]] )
                except Exception as e :
                    logger.error('Exception while reading DEB Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:DEB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for DEB ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for DEB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_idbi :
                logger.info ( "CBR:IDBI file process gets started." )
                try :
                    df_data = pd.read_csv ( input_filepath )
                except Exception as e :
                    logger.error('Exception while reading IDBI Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:IDBI Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for IDBI ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for IDBI ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_lvb :
                logger.info ( "CBR:LVB file process gets started." )
                try :
                    wb = xlrd.open_workbook ( input_filepath )
                    # df_data = pd.read_excel(wb, skiprows = [0], engine = 'xlrd')
                    # c = list(df_data.columns.values)
                    # print("c  ::::: ",c)
                    df_data = pd.read_excel ( wb , skiprows=[0 , 1] , engine='xlrd' )
                    df_data = df_data.filter ( regex='^((?!Unnamed).)*$' )
                    d = list ( df_data.columns.values )
                    df_data = common_util_obj.check_null_dataframe ( df_data )
                    df_data = df_data.drop ( df_data.index[len ( df_data ) - 1] )
                except Exception as e :
                    logger.error('Exception while reading LVB Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:LVB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    # uploaded_path = csv_file_path
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    # data_status = common_util_obj.validate_data_cbr(strfile_type,upload_time,strbank_code,uploaded_status,df_enhanced['created_reference_id'][0],username,actual_upload_time,Project_id)
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for LVB ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for LVB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_psb :
                logger.info ( "CBR:PSB file process gets started." )
                # print ("#printing from PSB")
                try :
                    wb = xlrd.open_workbook ( input_filepath )
                    df_data1 = pd.read_excel ( input_filepath , header=None )
                    df1 = common_util_obj.fetchColumnName ( df_data1 )
                    df_data = pd.read_excel ( wb , skiprows=[0 , 1] , engine='xlrd' ,
                                              names=[df1[0] , df1[1] , df1[2] , df1[3] , df1[4] , df1[5] , df1[6] ,
                                                     df1[7] , df1[8] , df1[9] , df1[10] , df1[11] , df1[12] , df1[13]] )
                except Exception as e :
                    logger.error('Exception while reading PSB Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:PSB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name );
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for PSB ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for PSB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]['value']
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_sbi :
                logger.info ( "CBR:SBI file process gets started." )
                # print ("Inside SBI")
                try :
                    df_data = pd.read_excel ( input_filepath )
                except Exception as e :
                    logger.error('Exception while reading SBI Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:SBI Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name );
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name );
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for SBI ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for SBI ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_uco :
                logger.info ( "CBR:UCO file process gets started." )
                try :
                    df_data = pd.read_excel ( input_filepath )
                except Exception as e :
                    logger.error('Exception while reading UCO Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:UCO Format validation successful .")
                    df_data.rename ( columns={ 'Region' : 'region_file' } , inplace=True )
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for UCO ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for UCO ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_vjb :
                logger.info ( "CBR:VJB file process gets started." )
                try :
                    df_data = pd.read_csv ( input_filepath )
                    df_data.drop ( df_data.columns[len ( df_data.columns ) - 1] , axis=1 , inplace=True )
                except Exception as e :
                    logger.error('Exception while reading VJB Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:VJB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for VJB ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for VJB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_bob :
                logger.info ( "CBR:BOB file process gets started." )
                try :
                    df_data = pd.read_excel ( input_filepath, sheet_name = 'Data')
                    if 'ConnectivityProvider' in df_data:
                        del df_data['ConnectivityProvider']
                    df_data.insert(2, 'state', '-')
                    df_data.insert(3, 'atmstatus', '-')
                    df_data.insert(34, 'lastreversaltime', '')
                except Exception as e :
                    logger.error('Exception while reading BOB Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:BOB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name );
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    ref_id = df_enhanced['created_reference_id'][0]
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             reference_id,
                                                                             region )
                    
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           reference_id , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for BOB ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      reference_id ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for BOB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = result
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_iob :
                logger.info ( "CBR:IOB file process gets started." )
                try :
                    df_data = pd.read_csv ( input_filepath , sep="|" ,
                                            names=['atm_id' , 'remaining_1' , 'remaining_2' , 'remaining_3' ,
                                                   'remaining_4' , 'total_of_remaining' , 'opening_1' , 'opening_2' ,
                                                   'opening_3' , 'opening_4' , 'total_of_opening' , 'dispense_1' ,
                                                   'dispense_2' , 'dispense_3' , 'dispense_4' , 'total_of_dispense' ,
                                                   'A'] )
                    del df_data['A']
                except Exception as e :
                    logger.error('Exception while reading IOB Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    logger.info("CBR:IOB Format validation successful .")
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for IOB ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for IOB ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict

            elif strbank_code == masterconf.bankcode_ubi :
                logger.info ( "CBR:UBI file process gets started." )
                # print ("Inside UBI")
                try :
                    df_data = pd.read_fwf ( input_filepath )
                    column_names = list ( df_data.columns.values )
                    logger.info("column_names for UBI :::: %s " , column_names)
                    date1 = column_names[0]
                    list1 = date1.split ( ' ' )
                    new_list = []
                    for i in list1 :
                        if i != '' :
                            new_list.append ( i )
                    hh = ''
                    if new_list[3] == 'PM' :
                        new_list[2] = str ( int ( new_list[2].split ( ':' )[0] ) + 12 ) + ':' + \
                                      new_list[2].split ( ':' )[1]
                        hh = new_list[2]
                    else :
                        hh = new_list[2]
                    months = {
                        'JANUARY' : '01' ,
                        'FEBRUARY' : '02' ,
                        'MARCH' : '03' ,
                        'APRIL' : '04' ,
                        'MAY' : '05' ,
                        'JUNE' : '06' ,
                        'JULY' : '07' ,
                        'AUGUST' : '08' ,
                        'SEPTEMBER' : '09' ,
                        'OCTOBER' : '10' ,
                        'NOVEMBER' : '11' ,
                        'DECEMBER' : '12'
                    }
                    mm = months.get ( new_list[0] )
                    date_year = new_list[1].split ( ',' )
                    dd = int ( date_year[0] )
                    yy = date_year[1]
                    if dd in [1 , 2 , 3 , 4 , 5 , 6 , 7 , 8 , 9] :
                        dd = "0" + str ( dd )
                    else :
                        dd = str ( dd )
                    extracted_date_time = yy + "-" + mm + "-" + dd + " " + hh + ":00"
                    df_data = pd.read_fwf ( input_filepath , error_bad_lines=False , skiprows=[0 , 1] )
                    column_names = list ( df_data.columns.values )
                    print("column_names after 2 rows   :::: " , column_names)
                    column_names_list = column_names[0].split ( ' ' )
                    cnl = []
                    for i in column_names_list :
                        if i != '' :
                            cnl.append ( i )
                    for i in range ( len ( cnl ) ) :
                        cnl[i] = cnl[i].replace ( '-' , '_' )
                    df_data = pd.read_fwf ( input_filepath , delimiter=' ,' , skiprows=[0 , 1 , 2 , 3 , 4] ,
                                            names=[cnl[0] , cnl[1] , cnl[2] , cnl[3] , cnl[4]] ,
                                            delim_whitespace=',' )  # header=1
                    df_data['file_date_time'] = extracted_date_time
                except Exception as e :
                    logger.error('Exception while reading UBI Bank file %s ', e)
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = masterconf.incorrect_input_file
                    return status_dict
                format_status = common_util_obj.validate_format_xls ( df_data , strfile_type , strbank_code )
                logger.info("CBR:UBI Format validation successful .")
                if format_status.lower ( ) == masterconf.format_val_success_status.lower ( ) :
                    df_enhanced = common_util_obj.add_fields_to_df_cbr ( df_data , uploaded_status , upload_time ,
                                                                         username , actual_upload_time , Project_id ,
                                                                         reference_id , region )
                    print("Dataframe after metadata add ::::: " , df_enhanced)
                    csv_file_path = common_util_obj.write_to_csv ( df_enhanced , filehandle_name , request_post ,
                                                                   userinfo )  # ES1-T218 - Folder re-structuring done
                    uploaded_file_name = common_util_obj.upload_file_sftp ( csv_file_path )
                    uploaded_path = os.path.join ( masterconf.sfpt_file_path , uploaded_file_name )
                    common_util_obj.load_data_database ( uploaded_path , strfile_type , strbank_code )
                    self.maintain_log_id = common_util_obj.maintainlog_cbr ( strfile_type , strbank_code ,
                                                                             actual_upload_time , uploaded_status ,
                                                                             upload_time , Project_id , username ,
                                                                             df_enhanced['created_reference_id'][0] ,
                                                                             region )
                    data_status = self.validate_data_cbr ( strfile_type , upload_time , strbank_code , uploaded_status ,
                                                           df_enhanced['created_reference_id'][0] , username ,
                                                           actual_upload_time , Project_id , region )
                    logger.info("data_status from validate_data_cbr for UBI ::: %s ", data_status)
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = data_status["status_text"]
                    if data_status["status_text"] == masterconf.no_record :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = "complete file failed"
                        return status_dict
                    elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
                        "status_text"] == masterconf.partial_valid_file :
                        result = self.status_update_and_consolidate ( strfile_type , upload_time , strbank_code ,
                                                                      'Approved' ,
                                                                      df_enhanced['created_reference_id'][0] ,
                                                                      username , actual_upload_time , Project_id ,
                                                                      region )
                        logger.info("result from status_update_and_consolidate for UBI ::: %s ", result)
                        if result == masterconf.Consolidation_Successful and status_dict[
                            "status_text"] == masterconf.data_validated_successful :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = masterconf.data_uploaded_successful
                            status_dict["file_status"] = masterconf.data_validated_successful
                            return status_dict
                        else :
                            status_dict["level"] = masterconf.data_level
                            status_dict["status_text"] = data_status["status_text"] + data_status["data"]
                            status_dict["file_status"] = data_status["status_text"]
                            return status_dict
                    else :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status["status_text"]
                        status_dict["file_status"] = data_status["status_text"]
                        return status_dict
                else :
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
                    return status_dict
            else :
                status_dict["level"] = masterconf.format_level;
                status_dict["status_text"] = masterconf.no_bank
                return status_dict
        except Exception as e :
            entry = data_update_log.objects.get ( id=self.maintain_log_id )
            entry.record_status = 'Failed'
            entry.save ( )
            logger.error ( 'EXCEPTION IN handle_upload_request method OF BaseCBRController %s' , e)
            raise e

    def validate_data_cbr ( self , strfile_type , createddate , bankcode , recordstatus , referenceid , systemUser ,
                            actual_upload_time , Project_id , region ) :
        logger = logging.getLogger(__name__)
        try :
            data_status_return = { }
            common_util_obj = common_util.common_utils ( )
            logger.info ( "INSIDE validate_data_cbr method OF BaseCBRController" )
            nocount = """ SET NOCOUNT ON; """
            sql_str = "exec uspValidateCbrFiles " + "'" + actual_upload_time + "', " + "'" + bankcode + "', " + "'" + Project_id + "', " + "'" + region + "', " + "'" + recordstatus + "', " + "'" + referenceid + "'" + ", " + "'" + systemUser + "'"
            logger.info("sql_str for  uspValidateCbrFiles or uspValidateDispenseFiles: %s ", sql_str )
            cur = connection.cursor ( )
            cur.execute ( nocount + sql_str )
            data_seq_no = cur.fetchone ( )
            data_status = app_config_param.objects.filter ( sequence=int ( data_seq_no[0] ) ).values ( 'value' )
            logger.info ("data_seq_no from validate_data_cbr data_seq_no[0]: %s " , data_seq_no[0])
            if data_seq_no[0] == '50001' :
                all_codes = data_update_log.objects.filter ( created_reference_id=referenceid ).values (
                    'validation_code' )
                all_codes_list = list ( all_codes[0]['validation_code'].split ( "," ) )
                error_code_str = ''
                for i in all_codes_list :
                    error_code = app_config_param.objects.filter ( sequence=i ).values ( 'value' )
                    error_code_str = error_code_str + ", " + error_code[0]['value']
                #print ("data_status_return : " , data_status_return)
                data_status_return["status_text"] = data_status[0]['value']
                data_status_return["data"] = error_code_str
                return data_status_return
            data_status_return["status_text"] = data_status[0]['value']
            data_status_return["data"] = data_status[0]['value']  # ES1-I44 Issue Fix
            return data_status_return
        except Exception as e :
            logger.error ( 'EXCEPTION IN validate_data_cbr method OF BaseCBRController. %s ' , e)
            raise e

    def status_update_and_consolidate ( self , strfile_type , createddate , bankcode , recordstatus , referenceid ,
                                        systemUser , actual_upload_time , Project_id , region ) :
        common_util_obj = common_util.common_utils ( )
        logger = logging.getLogger(__name__)
        logger.info ( "INSIDE status_update_and_consolidate method of BaseCBRController" )
        nocount = """ SET NOCOUNT ON; """
        table_name_key = strfile_type + "_" + bankcode + "_TABLE"
        try :
            procedure_name2 = 'uspConsolidateCbrFiles'
            cur = connection.cursor ( )
            logger.info (
                "Inside status_update_and_consolidate method of BaseCBRController, Going for uspConsolidateCbrFiles" )
            sql_str3 = "EXEC " + procedure_name2 + " '" + actual_upload_time + "','" + bankcode + "','" + Project_id + "','" + region + "','" + referenceid + "','" + systemUser + "'"
            logger.info("Dynamic uspConsolidateCbrFiles SP is : %s " , sql_str3)
            cur.execute ( nocount + sql_str3 )
            data_seq_no = cur.fetchone ( )
            logger.info("data_seq_no from db ::::: %s " , data_seq_no)
            #print("type of data_seq_no from db ::::: " , type ( data_seq_no ))
            data_status = app_config_param.objects.filter ( sequence=int ( data_seq_no[0] ) ).values ( 'value' )
            cur.close ( )
            return data_status[0]['value']
        except Exception as e :
            logger.error ( 'EXCEPTION IN validate_data_cbr METHOD OF BaseCBRController %s ' ,e)
            raise e
