import datetime
import logging
import os
import sys
import time
import pandas as pd
from Common import db_queries_properties
from Common import masterconf
from Common.CommonFunctions import common_util
from Common.models import app_config_param
from Common.models import data_update_log
from django.db import connection
from rest_framework.authentication import SessionAuthentication
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from Model.models import c3r_VMIS
from Common.models import data_update_log

class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class BaseC3RMISController():
    authentication_classes = [];
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    data_update_log_id = 0
    maintain_log_id = 0

    def handle_upload_request(self, request_post, input_filepath, filehandle_name, str_file_extention, userinfo):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("In handle_upload_request method of BaseC3RMISController")
        strfile_type = request_post['file_type']
        strbank_code = 'ALL'
        actual_upload_time = request_post['upload_datatime']
        upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        username = userinfo['username'].username
        uploaded_status = "Uploaded";
        failed_status = "Failed";
        Project_id = 'MOF'
        status_flag = 0
        status_dict = {}
        strfile_type = 'C3R'
        reference_id = userinfo['reference_id']  # ES1-I44 Issue Change
        format_status = masterconf.no_bank
        try:
            ####################Reading C3R VMIS file first, it is mendatory to read VMIS file and store it into DB before the same for CMIS########################
            if strfile_type == masterconf.file_type_C3R:
                logger.setLevel(logging.INFO)
                logger.info("Going for file validation of C3R:C3R_VMIS")
                final_column_names = []
                ############Extracting essential data from file from 0th row################################
                df_data1 = pd.read_excel(input_filepath, sheet_name='V MIS', usecols = 52)
                file_date = df_data1.loc[6,'Date']
                if str(file_date) != str(actual_upload_time):
                    status_dict["level"] = masterconf.data_level
                    status_dict["status_text"] = masterconf.incoming_date_issue
                    status_dict["status_code"] = masterconf.error_inserting_record
                    return status_dict

                
                df_data = common_util_obj.check_null_dataframe(
                    df_data1)  ## Method to check null or empty values  ES1-I44 Issue Change
                h1 = list(df_data.columns.values)
                final_column_names = h1[:6]
                final_column_names.append(h1[11])
                final_column_names.append(h1[17])
                final_column_names.append(h1[23])
                final_column_names.append(h1[29])
                final_column_names.append(h1[35])
                final_column_names.append(h1[41])
                final_column_names.append(h1[47])
                ############Extracting essential data from file from 1th row################################
                df_data1 = pd.read_excel(input_filepath, skiprows=[0], sheet_name='V MIS', usecols = 52)
                if 'Unnamed: 4' in list(df_data1.columns.values):
                    df_data1.rename({'Unnamed: 4': 'Name'}, axis='columns', inplace=True)
                    if pd.isnull(df_data1.loc[0,'Name']) == True:
                        df_data1 = df_data1.drop([0], axis=0)
                df_data = common_util_obj.check_null_dataframe(
                    df_data1)  ## Method to check null or empty values  ES1-I44 Issue Change
                h2 = list(df_data.columns.values)
                final_column_names[13:] = h2[4:]
                for i in range(len(final_column_names)):
                    final_column_names[i] = str(final_column_names[i])
                final_column_names_mod = pd.DataFrame(final_column_names)
                df1 = pd.DataFrame(final_column_names_mod, columns=final_column_names)
                strfile_type_vmis = masterconf.file_type_C3R_VMIS
                logger.setLevel(logging.INFO)
                logger.info("Going for format validation of C3R:C3R_VMIS")
                ###############Going for format validation for C3R VMIS##############################################
                format_status = common_util_obj.validate_format_xls(df1, strfile_type_vmis, strbank_code)
                if format_status.lower() == masterconf.format_val_success_status.lower():
#####################Duplicate check in cmis sheet of the file###############################################
                    a = list(df_data.columns[1:52])
                    df_data.drop_duplicates(subset=a, keep='first', inplace=True)
                    # duplicated_data = df_data[df_data.duplicated ( subset=["Unnamed: 2", "Unnamed: 3", "Name"] , keep='first' )]
                    # duplicated_data.to_excel("E:\\a.xlsx")
                    # if duplicated_data.empty != True :
                    #     status_dict["level"] = masterconf.data_level
                    #     status_dict["status_text"] = masterconf.duplicate_data_entry_for_VMIS
                    #     status_dict["status_code"] = masterconf.status_code_for_duplicate_data
                    #     return status_dict
######################################################################################## 
                    logger.setLevel(logging.INFO)
                    logger.info("format validation of C3R:C3R_VMIS completed")
                    ########################Adding Extra fields to VMIS file to store it into DB#############################
                    df_enhanced = common_util_obj.add_fields_to_df_c3r(df_data, uploaded_status, upload_time, username,
                                                                       actual_upload_time, Project_id, reference_id)
                    csv_file_path = common_util_obj.write_to_csv(df_enhanced, filehandle_name, request_post,
                                                                 userinfo)  # ES1-T218 - Folder re-structuring done
                    # uploaded_path = csv_file_path
                    uploaded_file_name = common_util_obj.upload_file_sftp(csv_file_path)
                    uploaded_path = os.path.join(masterconf.sfpt_file_path, uploaded_file_name)
                    #####################Loading file data into DB##################################################
                    common_util_obj.load_data_database(uploaded_path, strfile_type_vmis, strbank_code)
                    id = common_util_obj.maintainlog(strfile_type_vmis, strbank_code, actual_upload_time,
                                                     uploaded_status, upload_time, Project_id, username, reference_id)
                    data_update_log.objects.filter(created_reference_id = reference_id).update(cra_name= request_post['cra'])
                    # strfile_type = masterconf.
                    ######################GOing for read operation on CMIS file #######################################
                    final_column_names = []
                    logger.setLevel(logging.INFO)
                    logger.info("Going for file validation of C3R:C3R_CMIS")
                    ######################Extracting essential information from 0th row##############################
                    df_data1 = pd.read_excel(input_filepath, sheet_name='C MIS',usecols = 143)
                    df_data = common_util_obj.check_null_dataframe(
                        df_data1)  ## Method to check null or empty values  ES1-I44 Issue Change
                    h1 = list(df_data.columns.values)
                    final_column_names.append(h1[0].strip())

                    ######################Extracting essential information from 1st row###################################
                    df_data1 = pd.read_excel(input_filepath, skiprows=[0], sheet_name='C MIS',usecols = 143)
                    
                    # if pd.isnull(df_data1.loc[2, 'Feeder Branch']) == True:
                    #     df_data1 = df_data1.drop([2],axis=0)
                    df_data = common_util_obj.check_null_dataframe(
                        df_data1)  ## Method to check null or empty values  ES1-I44 Issue Change
                    h2 = list(df_data.columns.values)
                    final_column_names[1:] = h2[:13]
                    final_column_names.append(h2[36])
                    final_column_names.append(h2[54])
                    final_column_names.append(h2[71])
                    final_column_names.append(h2[82])
                    final_column_names.append(h2[94])
                    final_column_names.append(h2[130])
                    final_column_names.append(h2[142])
                    ###################Extracting essential information from 2nd row of the file ################################
                    df_data1 = pd.read_excel(input_filepath, skiprows=[0, 1], sheet_name='C MIS',usecols = 143)
                    df_data = common_util_obj.check_null_dataframe(
                        df_data1)  ## Method to check null or empty values ES1-I44 Issue Change
                    h3 = list(df_data.columns.values)
                    final_column_names.append(h3[12])
                    final_column_names.append(h3[18])
                    final_column_names.append(h3[24])
                    final_column_names.append(h3[30])
                    final_column_names.append(h3[36])
                    final_column_names.append(h3[42])
                    final_column_names.append(h3[48])
                    final_column_names.append(h3[54])
                    final_column_names.append(h3[60])
                    final_column_names.append(h3[65])
                    final_column_names.append(h3[71])
                    final_column_names.append(h3[77])
                    final_column_names.append(h3[82])
                    final_column_names.append(h3[88])
                    final_column_names.append(h3[94])
                    final_column_names.append(h3[100])
                    final_column_names.append(h3[106])
                    final_column_names.append(h3[112])
                    final_column_names.append(h3[118])
                    final_column_names.append(h3[124])
                    final_column_names.append(h3[136])
                    final_column_names.append(h3[142])
                    df_data1 = pd.read_excel(input_filepath, skiprows=[0, 1, 2], sheet_name='C MIS',usecols = 143)
                    if pd.isnull(df_data1.loc[0,'Total']) == True:
                        df_data1 = df_data1.drop([0], axis=0)
                    df_data = common_util_obj.check_null_dataframe(
                        df_data1)  ## Method to check null or empty values  ES1-I44 Issue Change
                    df_data.drop(df_data.columns[len(df_data.columns) - 1], axis=1, inplace=True)
                    h4 = list(df_data.columns.values)
                    final_column_names[43:] = h4[12:]
                    for i in range(len(final_column_names)):
                        final_column_names[i] = str(final_column_names[i])
                    final_column_names_mod = pd.DataFrame(final_column_names)
                    df1 = pd.DataFrame(final_column_names_mod, columns=final_column_names)
                    # sys.setrecursionlimit(1500)
                    strfile_type_cmis = masterconf.file_type_C3R_CMIS
                    # strfile_type
                    logger.setLevel(logging.INFO)
                    logger.info("Going for format validation of C3R:C3R_CMIS")
                    ###################Going for format validation of CMIS file ############################
                    format_status = common_util_obj.validate_format_xls(df1, strfile_type_cmis, strbank_code)
                    if format_status.lower() == masterconf.format_val_success_status.lower():
                        logger.setLevel(logging.INFO)
                        logger.info("format validation of C3R:C3R_CMIS completed")
#####################Duplicate check in cmis sheet of the file###############################################
                        a = list(df_data.columns[1:143])
                        df_data.drop_duplicates(subset=a, keep='first', inplace=True)
                        # duplicated_data = df_data[df_data.duplicated ( subset=["Unnamed: 1", "Unnamed: 2", "Unnamed: 3", "Unnamed: 6", "Unnamed: 7"] , keep='first' )]
                        # if duplicated_data.empty != True :
                        #     status_dict["level"] = masterconf.data_level
                        #     status_dict["status_text"] = masterconf.duplicate_data_entry_for_CMIS
                        #     status_dict["status_code"] = masterconf.status_code_for_duplicate_data
                        #     return status_dict
######################################################################################## 



                        df_enhanced = common_util_obj.add_fields_to_df_c3r(df_data, uploaded_status, upload_time,
                                                                           username, actual_upload_time, Project_id,
                                                                           reference_id)
                        csv_file_path = common_util_obj.write_to_csv(df_enhanced, filehandle_name, request_post,
                                                                     userinfo)  # ES1-T218 - Folder re-structuring done
                        uploaded_file_name = common_util_obj.upload_file_sftp(csv_file_path)
                        uploaded_path = os.path.join(masterconf.sfpt_file_path, uploaded_file_name);
                        common_util_obj.load_data_database(uploaded_path, strfile_type_cmis, strbank_code)
                        data_status = self.validate_data_C3R(strfile_type, upload_time, strbank_code, uploaded_status,
                                                             reference_id, username,
                                                             actual_upload_time, Project_id)
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_text"] = data_status
                        return status_dict
                    else:
                        status_dict["level"] = masterconf.format_level
                        status_dict["status_text"] = format_status
                        return status_dict
                else:
                    status_dict["level"] = masterconf.format_level
                    status_dict["status_text"] = format_status
                    return status_dict
        except Exception as e:
            print("In Controller Exception of C3R", e)
            entry = data_update_log.objects.get(id=self.maintain_log_id)
            entry.record_status = 'Failed'
            entry.save()
            logger.setLevel(logging.ERROR)
            logger.error('Exception in handle_upload_request method of BaseVCBControllerEOD', exc_info=True)
            raise e

    def validate_data_C3R(self, strfile_type, createddate, bankcode, recordstatus, referenceid, systemUser,
                          actual_upload_time, Project_id):

        try:
            common_util_obj = common_util.common_utils()
            logger = common_util_obj.initiateLogger()
            logger.setLevel(logging.INFO)
            logger.info("In validate_data_C3R method of C3R")
            nocount = """ SET NOCOUNT ON; """
            sql_str = "exec uspValidateC3RFiles " + "'" + actual_upload_time + "', " + "'" + bankcode + "', " + "'" + Project_id + "', " + "'" + recordstatus + "', " + "'" + referenceid + "'" + ", " + "'" + systemUser + "'"
            cur = connection.cursor()
            cur.execute(nocount + sql_str)
            data_seq_no = cur.fetchone()
            # print("data_seq_no: {}".format(data_seq_no))
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # print("data_status : {}".format(data_status))
            # print("data_status_value : {}".format(data_status[0]['value']))
            return data_status[0]['value']

        except Exception as e:
            common_util_obj = common_util.common_utils()
            logger = common_util_obj.initiateLogger()
            logger.setLevel(logging.ERROR)
            logger.error('Exception in validate_data_C3R method of C3R', exc_info=True)
            raise e

    def status_update_and_consolidate_C3R(self, strfile_type, createddate, bankcode, recordstatus, referenceid,
                                          systemUser, actual_upload_time, Project_id):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("In status_update_and_consolidate_C3R method of C3R")
        strfile_type_1 = ''
        nocount = """ SET NOCOUNT ON; """
        table_name_key = strfile_type + "_VMIS_" + bankcode + "_TABLE"
        table_name = db_queries_properties.table_dict[table_name_key]
        table_name1_key = strfile_type + "_CMIS_" + bankcode + "_TABLE"
        table_name1 = db_queries_properties.table_dict[table_name1_key]
        cra_name = data_update_log.objects.filter(created_reference_id = referenceid, record_status = masterconf.pending_status).values('cra_name')
        cra_name = cra_name[0]['cra_name']
        try:
            procedure_name1 = 'uspSetStatus_pycaller'
            procedure_name2 = 'uspConsolidateC3RFiles'
            sql_str2 = "DECLARE @out varchar(max) EXEC " + procedure_name1 + " 'dbo.data_update_log','Approval Pending','" + recordstatus + "','" + createddate + "','" + systemUser + "','" + referenceid + "',' bank_code = ''" + bankcode + "'' and datafor_date_time = ''" + actual_upload_time + "'' and data_for_type = ''" + strfile_type + "'' and project_id = ''" + Project_id + "'' and created_reference_id = ''" + referenceid + "''',@out OUTPUT select @out"
            cur = connection.cursor()
            cur.execute(nocount + sql_str2)
            data_seq_no = cur.fetchone()
            # print("printing sql_str2 1: {}".format(sql_str2))
            # print("data_seq_no: {}".format(data_seq_no))
            # print("Procudure 3 is running")
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # print("data_status : {}".format(data_status))
            # print("data_status_value : {}".format(data_status[0]['value']))
            # print("Again Going for uspSetStatus")
            sql_str2 = "DECLARE @out varchar(max) EXEC " + procedure_name1 + " '" + table_name + "','Approval Pending','" + recordstatus + "','" + createddate + "','" + systemUser + "','" + referenceid + "',' datafor_date_time = ''" + actual_upload_time + "'' and project_id = ''" + Project_id + "'' and created_reference_id = ''" + referenceid + "''',@out OUTPUT select @out"
            cur.execute(nocount + sql_str2)
            data_seq_no = cur.fetchone()
            # print("data_seq_no: {}".format(data_seq_no))
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # print("data_status : {}".format(data_status))
            # print("data_status_value : {}".format(data_status[0]['value']))
            # print("Again Going for uspSetStatus")
            sql_str3 = "DECLARE @out varchar(max) EXEC " + procedure_name1 + " '" + table_name1 + "','Approval Pending','" + recordstatus + "','" + createddate + "','" + systemUser + "','" + referenceid + "',' datafor_date_time = ''" + actual_upload_time + "'' and project_id = ''" + Project_id + "''  and created_reference_id = ''" + referenceid + "''',@out OUTPUT select @out"
            # print(sql_str3)
            cur.execute(nocount + sql_str3)
            data_seq_no = cur.fetchone()
            # print("data_seq_no: {}".format(data_seq_no))
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value')
            # print ("data_status is file is updated or not :", data_status)
            if data_status[0]['value'] == masterconf.status_update_success and recordstatus == 'Approved':
                sql_str4 = "EXEC " + procedure_name2 + " '" + actual_upload_time + "','" + cra_name + "','" + Project_id + "','" + referenceid + "','" + systemUser + "'"
                cur.execute(nocount + sql_str4)
                data_seq_no = cur.fetchone()
                data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
                # print("data_status : {}".format(data_status))
                # print("data_status_value : {}".format(data_status[0]['value']))
                cur.close()
                return data_status[0]['value']
            else:
                return data_status[0]['value']
        except Exception as e:
            print ("exception : ",e)
            logger = common_util_obj.initiateLogger()
            logger.setLevel(logging.ERROR)
            logger.error('Exception in status_update_and_consolidate_C3R method of BaseC3RMISController', exc_info=True)
            raise e
