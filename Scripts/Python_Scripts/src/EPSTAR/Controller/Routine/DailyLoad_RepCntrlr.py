from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
from Common import db_queries_properties
from Common.models import data_update_log


class CsrfExemptSessionAuthentication(SessionAuthentication):
    def enforce_csrf(self, request):
        return  # To not perform the csrf check previously happening


class DailyLoad_RepCntrlr():
    authentication_classes = [];
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    data_update_log_id = 0
    maintain_log_id = 0

    def handle_upload_request(self, request_post, input_filepath, filehandle_name, str_file_extention, userinfo):
        common_util_obj = common_util.common_utils();
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("In handle_upload_request method of DailyLoad_RepCntrlr")
        strfile_type = request_post['file_type']
        actual_upload_time = request_post['upload_datatime']
        # print ("actual_upload_time : {}".format(actual_upload_time))
        upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        username = userinfo['username'].username
        reference_id = userinfo['reference_id']
        uploaded_status = "Uploaded";
        failed_status = "Failed";
        status_flag = 0
        status_dict = {}
        format_status = masterconf.no_bank
        strbank_code = request_post['bank_code']
        Project_id = request_post['project_id']
        try:
            col = []
            df_data = pd.read_excel(input_filepath, 'Sheet1')
            for column in df_data.columns.values:
                column = column.replace(' ', '_')
                column = column.replace('\n', '')
                column = column.replace('\'', '')
                col.append(column)
            # print(col)
            df = pd.DataFrame(col)
            df1 = pd.DataFrame(df, columns=col)
            # print(df1.columns.values)
            logger.setLevel(logging.INFO)
            logger.info("Going for Format validation of DailyLoad_RepCntrlr")
            format_status = common_util_obj.validate_format_xls(df1, strfile_type, strbank_code)
            df1mod = list(df1)
            df_data = pd.read_excel(input_filepath, 'Sheet1', skiprows=[0],
                                    names=[df1mod[0], df1mod[1], df1mod[2], df1mod[3], df1mod[4], df1mod[5], df1mod[6],
                                           df1mod[7], df1mod[8], df1mod[9], df1mod[10], df1mod[11], df1mod[12],
                                           df1mod[13], df1mod[14], df1mod[15], df1mod[16], df1mod[17], df1mod[18],
                                           df1mod[19], df1mod[20], df1mod[21], df1mod[22], df1mod[23], df1mod[24],
                                           df1mod[25]])
            if format_status.lower() == masterconf.format_val_success_status.lower():
                logger.setLevel(logging.INFO)
                logger.info("Format validation of DailyLoad_RepCntrlr is successfull")
                df_enhanced = common_util_obj.add_fields_to_df(df_data, uploaded_status, upload_time, username,
                                                               actual_upload_time, Project_id, reference_id)
                # print(type(df_data['datafor_date_time']))
                # print(df_data['datafor_date_time'])
                # Write CSV (Common)
                csv_file_path = common_util_obj.write_to_csv(df_enhanced, filehandle_name, request_post,
                                                             userinfo)  # ES1-T218 - Folder re-structuring done
                uploaded_file_name = common_util_obj.upload_file_sftp(csv_file_path)
                uploaded_path = os.path.join(masterconf.sfpt_file_path, uploaded_file_name)
                # uploaded_path = csv_file_path
                common_util_obj.load_data_database(uploaded_path, strfile_type, strbank_code)
                id = common_util_obj.maintainlog(strfile_type_vmis, strbank_code, actual_upload_time, uploaded_status,
                                                 upload_time, Project_id, username,
                                                 df_enhanced['created_reference_id'][0])
                # data_status = common_util_obj.validate_data(strfile_type,upload_time,strbank_code,uploaded_status,df_enhanced['created_reference_id'][0],username,actual_upload_time,Project_id)
                data_status = self.validate_data_DL(strfile_type, upload_time, strbank_code, uploaded_status,
                                                    df_enhanced['created_reference_id'][1], username,
                                                    actual_upload_time, Project_id)
                status_dict["level"] = masterconf.data_level;
                status_dict["status_text"] = data_status
                return status_dict;
            else:
                status_dict["level"] = masterconf.format_level;
                status_dict["status_text"] = format_status
                return status_dict;
        except Exception as e:
            # print("In Controller Exception",e)
            entry = data_update_log.objects.get(id=self.maintain_log_id)
            entry.record_status = 'Failed'
            entry.save()
            logger.setLevel(logging.ERROR)
            logger.error('Exception in handle_upload_request method of DailyLoad_RepCntrlr', exc_info=True)
            raise e

    # def validate_and_consoldate_data_DLR(self,strfile_type,createddate,bankcode,recordstatus,referenceid,systemUser,actual_upload_time,Project_id):
    # 		#print ("ank_code from validate_and_consoldate_data: {}".format(bankcode))
    # 		actual_upload_time = actual_upload_time
    # 		Project_id = Project_id
    # 		#print ("strfile_type : {}".format(strfile_type))
    # 		if strfile_type == 'Daily_Loading_Report':
    # 			table_name = 'Daily_Loading_Report'
    # 		#print ("Tablename generated from validate_and_consoldate_data for Daily_Loading_Report: {}".format(table_name))
    # 		try:
    # 			#print("In validate_and_consoldate_data")
    # 			procedure_name1 = 'uspDataValidation_DailyLoadingRprt'
    # 			procedure_name2 = 'uspConsolidate_DailyLoadingRprt'
    # 			procedure_name3 = 'uspSetStatus'

    # 			nocount = """ SET NOCOUNT ON; """
    # 			sql_str = "exec " + procedure_name1 + " " +"'" + actual_upload_time + "', " + "'" + bankcode + "', " + "'" + Project_id + "', " + "'" + recordstatus + "', " + "'" + referenceid + "'" + ", " + "'" + systemUser + "'"
    # 			#print("sql_str for  uspValidateCbrFiles or uspValidateDispenseFiles: {}".format(sql_str))
    # 			cur = connection.cursor()
    # 			#print("Calling data sp")
    # 			cur.execute(nocount + sql_str)
    # 			data_seq_no = cur.fetchone()
    # 			#print ("data_seq_no: {}".format(data_seq_no))
    # 			#print ("procudure1 executed")
    # 			data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
    # 			#print("data_status : {}".format(data_status))
    # 			#print("data_status_value : {}".format(data_status[0]['value']))
    # 			if (data_status[0]['value'] != 'Data Validation Failed'):
    # 				#print ("Going for uspSetStatus")
    # 				sql_str2 = "DECLARE @out varchar(max)EXEC "+procedure_name3+" 'data_update_log','Approval Pending','Approved','"+createddate+"','"+systemUser+"','"+referenceid+"','datafor_date_time = ''"+actual_upload_time+"'' and data_for_type = ''"+strfile_type+"'' and project_id = ''"+Project_id+"'' ',@out OUTPUT select @out"
    # 				cur.execute(nocount + sql_str2)
    # 				data_seq_no = cur.fetchone()
    # 				#print ("#printing sql_str2 1: {}".format(sql_str2))
    # 				#print ("data_seq_no: {}".format(data_seq_no))
    # 				#print ("Procudure 3 is running")
    # 				data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
    # 				#print("data_status : {}".format(data_status))
    # 				#print("data_status_value : {}".format(data_status[0]['value']))
    # 				#print ("Again Going for uspSetStatus")
    # 				sql_str2 = "DECLARE @out varchar(max)EXEC "+procedure_name3+" '"+table_name+"','Approval Pending','Approved','"+createddate+"','"+systemUser+"','"+referenceid+"',' datafor_date_time = ''"+actual_upload_time+"'' and project_id = ''"+Project_id+"'' ',@out OUTPUT select @out"
    # 				#print ("#printing sql_str2 2: {}".format(sql_str2))
    # 				cur.execute(nocount + sql_str2)
    # 				data_seq_no = cur.fetchone()
    # 				#print ("data_seq_no: {}".format(data_seq_no))
    # 				data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
    # 				#print("data_status : {}".format(data_status))
    # 				#print("data_status_value : {}".format(data_status[0]['value']))
    # 				#print ("Going for uspConsolidateCbrFiles")
    # 				sql_str3 = "EXEC "+procedure_name2+" '"+actual_upload_time+"','"+bankcode+"','"+Project_id+"','"+referenceid+"','"+systemUser+"'"
    # 				#print(sql_str3)
    # 				cur.execute(nocount + sql_str3)
    # 				data_seq_no = cur.fetchone()
    # 				#print ("data_seq_no: {}".format(data_seq_no))
    # 				#print ("#printing from validate_and_consoldate_data")
    # 				data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
    # 				#print("data_status : {}".format(data_status))
    # 				#print("data_status_value : {}".format(data_status[0]['value']))
    # 				cur.close()
    # 				return data_status[0]['value']
    # 			else:
    # 				return data_status[0]['value']
    # 		except Exception as e:
    # 			raise e;

    def validate_data_DL(self, strfile_type, createddate, bankcode, recordstatus, referenceid, systemUser,
                         actual_upload_time, Project_id):

        try:
            logger = common_util_obj.initiateLogger()
            logger.setLevel(logging.INFO)
            logger.info("In validate_data_DL method of DailyLoad_RepCntrlr")
            nocount = """ SET NOCOUNT ON; """
            sql_str = "exec uspDataValidation_DailyLoadingRprt " + "'" + actual_upload_time + "', " + "'" + bankcode + "', " + "'" + Project_id + "', " + "'" + recordstatus + "', " + "'" + referenceid + "'" + ", " + "'" + systemUser + "'"
            ##print("sql_str for  uspValidateCbrFiles or uspValidateDispenseFiles: {}".format(sql_str))
            cur = connection.cursor()
            # print("Calling data sp")
            cur.execute(nocount + sql_str)
            data_seq_no = cur.fetchone()
            # print ("data_seq_no: {}".format(data_seq_no))
            # print ("procudure1 executed")
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # print("data_status : {}".format(data_status))
            # print("data_status_value : {}".format(data_status[0]['value']))
            return data_status[0]['value']

        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('Exception in validate_data_DL method of DailyLoad_RepCntrlr', exc_info=True)
            raise e

    def status_update_and_consolidate_DL(self, strfile_type, createddate, bankcode, recordstatus, referenceid,
                                         systemUser, actual_upload_time, Project_id):
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("In status_update_and_consolidate method of DailyLoad_RepCntrlr")
        # print ("status update and consolidate for : {}".format(bankcode))
        strfile_type_1 = ''
        nocount = """ SET NOCOUNT ON; """
        # print ("strfile_type : {}".format(strfile_type))
        table_name_key = strfile_type + "_" + bankcode + "_TABLE"
        table_name = db_queries_properties.table_dict[table_name_key]
        # print("table name : ",table_name)
        try:
            # print("In status_update_and_consolidate")
            # if strfile_type == masterconf.file_type_cbr:
            # 	#print ("strfile_type : {}".format(strfile_type))
            procedure_name1 = 'uspSetStatus_pycaller'
            procedure_name2 = 'uspConsolidate_DailyLoadingRprt'
            # print ("Going for uspSetStatus_pycaller")
            sql_str2 = "DECLARE @out varchar(max) EXEC " + procedure_name1 + " 'data_update_log','Approval Pending','" + recordstatus + "','" + createddate + "','" + systemUser + "','" + referenceid + "',' bank_code = ''" + bankcode + "'' and datafor_date_time = ''" + actual_upload_time + "'' and data_for_type = ''" + strfile_type + "'' and project_id = ''" + Project_id + "'' ',@out OUTPUT select @out"
            # print(sql_str2)
            cur = connection.cursor()
            cur.execute(nocount + sql_str2)
            data_seq_no = cur.fetchone()
            # print ("#printing sql_str2 1: {}".format(sql_str2))
            # print ("data_seq_no: {}".format(data_seq_no))
            # print ("Procudure 3 is running")
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # print("data_status : {}".format(data_status))
            # print("data_status_value : {}".format(data_status[0]['value']))
            # print ("Again Going for uspSetStatus")
            # sql_str2 = "DECLARE @out varchar(max)EXEC uspSetStatus 'cash_balance_file_alb','Approval Pending','Approved','2018-10-11 19:25:58.000','Mostaque','C1234567891', ' datafor_date_time = ''2018-10-11 08:00:00.000'' and project_id = ''MOF'' ',@out OUTPUT select @out"
            sql_str2 = "DECLARE @out varchar(max) EXEC " + procedure_name1 + " '" + table_name + "','Approval Pending','" + recordstatus + "','" + createddate + "','" + systemUser + "','" + referenceid + "',' datafor_date_time = ''" + actual_upload_time + "'' and project_id = ''" + Project_id + "'' ',@out OUTPUT select @out"
            # print(sql_str2)
            # print ("#printing sql_str2 2: {}".format(sql_str2))
            cur.execute(nocount + sql_str2)
            data_seq_no = cur.fetchone()
            # print ("data_seq_no: {}".format(data_seq_no))
            data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # print("data_status : {}".format(data_status))
            # print("data_status_value : {}".format(data_status[0]['value']))
            # if strfile_type == 'C3R':
            # 	table_name = 'c3r_VMIS'
            # 	#print ("Now Running procedure 2 for c3r_VMIS")
            # 	sql_str2 = "DECLARE @out varchar(max)EXEC "+procedure_name1+" '"+table_name+"','Approval Pending','Approved','"+createddate+"','"+systemUser+"','"+referenceid+"',' datafor_date_time = ''"+actual_upload_time+"'' ',@out OUTPUT select @out"
            # 	#print ("#printing sql_str2 2: {}".format(sql_str2))
            # 	cur.execute(nocount + sql_str2)
            # 	data_seq_no = cur.fetchone()
            # 	#print ("data_seq_no: {}".format(data_seq_no))
            # 	data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
            # 	#print("data_status : {}".format(data_status))
            # 	#print("data_status_value : {}".format(data_status[0]['value']))

            if data_status[0]['value'] == masterconf.status_update_success:
                # print ("Going for uspConsolidateCbrFiles")
                # sql_str3 = "EXEC uspConsolidateCbrFiles '2018-10-11 08:00:00.000','ALB','MOF','sasas','SA'"
                # sql_str3 = "EXEC "+procedure_name2+" '"+actual_upload_time+"','"+bankcode+"','"+Project_id+"','"+referenceid+"','Mostaque'"
                sql_str3 = "EXEC " + procedure_name2 + " '" + actual_upload_time + "','" + bankcode + "','" + Project_id + "','" + referenceid + "','" + systemUser + "'"
                # print(sql_str3)
                cur.execute(nocount + sql_str3)
                data_seq_no = cur.fetchone()
                # print ("data_seq_no: {}".format(data_seq_no))
                # print ("#printing from validate_and_consoldate_data")
                data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value');
                # print("data_status : {}".format(data_status))
                # print("data_status_value : {}".format(data_status[0]['value']))
                cur.close()
                return data_status[0]['value']
            else:
                return data_status[0]['value']
        # return data_status[0]['value']
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('Exception in status_update_and_consolidate method of DailyLoad_RepCntrlr', exc_info=True)
            raise e
