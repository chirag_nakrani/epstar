from Common.CommonFunctions import common_util
from Common import masterconf
from datetime import timedelta
from Model.models import vaulting_of_balance
import datetime

class VaultingBalanceController():
    def handle_vaultingBalance_req(self,request):
        try:
            common_util_obj = common_util.common_utils()
            created_by = request.userinfo['username'].username
            created_on = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
            created_reference_id = request.userinfo['reference_id']
            status_dict = {}
            file_type = request.data['file_type'].upper()
            payload = request.data["payload"]
            for single_record in payload:
                add_dict = {
                    "cra" : single_record["cra"],
                    "indent_order_id" : single_record["indent_order_number"],
                    "bank_code" : single_record["bank_code"],
                    "feeder_branch_code" : single_record["feeder_branch_code"],
                    "atm_id" : single_record["atm_id"],
                    "vaulting_date": single_record["vaulting_date"],
                    "amount_100": single_record["amount_100"],
                    "amount_200" : single_record["amount_200"],
                    "amount_500" : single_record["amount_500"],
                    "amount_2000" : single_record["amount_2000"],
                    "total_amount" : single_record["total_amount"],
                    "reason_for_vaulting" : single_record["reason_for_vaulting"],
                    "comments" : single_record["comments"],
                    "record_status" : masterconf.appr_pending_state,
                    "created_on" : created_on,
                    "created_by" : created_by,
                    "created_reference_id" : created_reference_id,
                }
                load_data = vaulting_of_balance(**add_dict)#inserting all the records together in the vaulting balance table and saving the same
                load_data.save()
            common_util_obj.maintainlog_master(file_type,masterconf.appr_pending_state, created_on,created_by,created_reference_id)
            status_dict['status_code'] = masterconf.success
            status_dict["data"] = masterconf.success_codes_desc['S101']
            return status_dict
        except Exception as e:
            print("Exception from handle_vaultingBalance_req of VaultingBalanceController : ",e)
            status_dict = {}
            status_dict['status_code'] = masterconf.internal_server_error
            status_dict["data"] = masterconf.error_codes_desc['E113']
            return status_dict
