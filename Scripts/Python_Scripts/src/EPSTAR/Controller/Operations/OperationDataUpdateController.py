
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from Model.models import vaulting_of_balance
from datetime import timedelta


class OperationDataUpdateController():

	# This is the main class for handling reference data update through Screen

	maintain_log_id = 0

	def handle_operation_update(self,request):

		#This method is for updating the existing records through screen for Reference Menus -- Brand Bill, CRA Empaneled and Vault Master

		common_util_obj = common_util.common_utils()
		update_list = request.data['update']
		file_type = request.data['file_type'].upper()
		username = request.userinfo['username'].username
		reference_id = request.userinfo['reference_id']
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		uploaded_status = masterconf.uploaded_state
		status_dict = {}
		try:

			if file_type.upper() == masterconf.file_type_VB.upper():
				## Code block for vaulting of balance
				for update_obj in update_list:
					row_obj = {}
					row_obj['cra'] = update_obj['cra']
					row_obj['indent_order_id'] = update_obj['indent_order_id']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['vaulting_date'] = update_obj['vaulting_date']
					row_obj['amount_100'] = update_obj['amount_100']
					row_obj['amount_200'] = update_obj['amount_200']
					row_obj['amount_500'] = update_obj['amount_500']
					row_obj['amount_2000'] = update_obj['amount_2000']
					row_obj['total_amount'] = update_obj['total_amount']
					row_obj['reason_for_vaulting'] = update_obj['reason_for_vaulting']
					row_obj['comments'] = update_obj['comments']
					row_obj['record_status'] = masterconf.pending_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					bbc = vaulting_of_balance(**row_obj) #limitdays Model object
					bbc.save()

			file_type = file_type.upper()
			# Inserting entry in data_update_log_master table
			self.maintain_log_id = common_util_obj.maintainlog_master(file_type,masterconf.appr_pending_state, upload_time,username,reference_id)
			status_dict["data"] = masterconf.data_inserted_successfully
			status_dict["status_code"] = masterconf.success
			return status_dict
		except Exception as e:
			print("Exception in OperationDataUpdateController ",e)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e
