import datetime
import logging
import time
from Common.CommonFunctions import common_util
from Model.models import ticket_category_master, workflow_master
from Common import masterconf
from django.contrib.auth.models import User, Group


class WorkflowController():

    # This class handles workflow related activities

    def create_or_delete_workflow(self, request):

        #This method is used to create new entry in workflow_master Model

        common_util_obj = common_util.common_utils()
        payload_obj_list = request.data['payload']
        changedate = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        username = request.userinfo['username'].username
        reference_id = request.userinfo['reference_id']
        status_dict = {}
        try:
            for payload_obj in payload_obj_list:
                category = payload_obj['category']
                subcategory = payload_obj['sub_category']
                sla = payload_obj['sla']
                escalation_level1 = payload_obj['escalation_level1']
                escalation_level2 = payload_obj['escalation_level2']
                escalation_level3 = payload_obj['escalation_level3']
                assigned_to_group = payload_obj['assigned_to_group']
                assigned_to_user = payload_obj['assigned_to_user']
                status = payload_obj['status']
                # user_id = User.objects.filter(username=assigned_to_user).values('id')
                # print(" user_id :: ",user_id)
                # group_id = Group.objects.filter(name=assigned_to_group).values('id')
                # print(" group_id :: ", group_id)
                category_id = ticket_category_master.objects.filter(category=category, sub_category=subcategory).values(
                    'category_id')
                cat_id = category_id[0]['category_id']
                if request.data['operation_type'] == masterconf.operation_type_add:
                    data_dict = {
                        'category_id': cat_id,
                        'sla': sla,
                        'escalation_level1': escalation_level1,
                        'escalation_level2': escalation_level2,
                        'escalation_level3': escalation_level3,
                        'assigned_to_group': assigned_to_group,
                        'assigned_to_user': assigned_to_user,
                        'status': status,
                        'created_on': changedate,
                        'created_by': username,
                        'created_reference_id': reference_id,
                        'record_status':masterconf.active_status
                    }
                    wkflw_mstr = workflow_master(**data_dict)
                    wkflw_mstr.save()
                else:
                    entry_to_delete = workflow_master.objects.filter(category_id=cat_id,
                                                                     sla=sla,
                                                                     assigned_to_group=assigned_to_group,
                                                                     assigned_to_user=assigned_to_user)
                    entry_to_delete.update(deleted_by=username, deleted_on=changedate,
                                           deleted_reference_id=reference_id,
                                           record_status=masterconf.status_deleted)
                status_dict['status_code'] = masterconf.success_updated
                return status_dict
        except Exception as e:
            print(e)
            raise e

    def read_workflow(self, request):

        # This method is used to read Workflow information

        common_util_obj = common_util.common_utils()
        request_obj = request.data['payload']
        list_returned = list()
        inner_obj = {}
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                filters_param['record_status'] = masterconf.active_status
                list_db = workflow_master.objects.filter(**filters_param).values()
            else:
                list_db = workflow_master.objects.filter(record_status=masterconf.active_status).values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            print(e)
            raise e
