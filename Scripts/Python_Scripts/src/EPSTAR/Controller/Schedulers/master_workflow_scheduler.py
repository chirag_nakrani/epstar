import schedule
import pyodbc
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import time
import json
import sys
from json2html import *

def mail_ro_user(recipient, subject, data):
    MAILHOST = "smtp.office365.com"
    MAILPORT = 587
    mail_sender = "epstar@electronicpay.in"
    mail_password = "Ruh75373"
    try:
        #print(" sending mail ")
        message = MIMEMultipart('alternative')
        message['Subject'] = subject
        body = MIMEText(data, 'html')
        message.attach(body)
        # message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        server.sendmail(mail_sender, recipient, message.as_string())
        server.close()
    except Exception as e:
        raise e

def get_subject_user_data(connection_string,data_list,sub_category):
    category = "Master"
    list_returned = list()
    for data in data_list:
        temp = {}
        temp['Uploaded Date'] = data[0]
        temp['User'] = data[1]
        list_returned.append(temp)
    #print(json.dumps(list_returned,default=str))
    subject = "Data uploaded for " + category + " " + sub_category + " is in pending state"
    emails = """
            select distinct a.email from auth_user a, workflow_master b
            where b.category_id = 
		                (select category_id from ticket_category_master 
		                where category = """ + "'" + category + "'" + """ and sub_category = """  + "'" + sub_category + "'" + """)
            and a.id = b.assigned_to_user
    """
    print(emails)
    conn = pyodbc.connect(connection_string)
    crsr = conn.cursor()
    try:
        crsr.execute(emails)
        mails_found = crsr.fetchall()
        print(" emails :::: ",mails_found)
        recipients = set()
        cash_admin_mails = """
                            select email from auth_user where id in (
                            select user_id from auth_user_groups where group_id =  
                            (select id from auth_group where name = 'cash_admin'))
                            """
        cash_ops_mails = """
                                    select email from auth_user where id in (
                                    select user_id from auth_user_groups where group_id =  
                                    (select id from auth_group where name = 'Cash_Ops_Head'))
                                    """
        crsr.execute(cash_admin_mails)
        cash_admin_mails = crsr.fetchall()
        if cash_admin_mails:
            for mail_tuple in cash_admin_mails:
                recipients.add(mail_tuple[0])
        crsr.execute(cash_ops_mails)
        cash_ops_mails = crsr.fetchall()
        if cash_ops_mails:
            for mail_tuple in cash_ops_mails:
                recipients.add(mail_tuple[0])
        print(cash_admin_mails)
        if mails_found:
            for mail in mails_found[0]:
                recipients.add(mail)
            str_data = json.dumps(list_returned,default=str)
            #data = json2html.convert(json = {"data": list_returned})
            data = json2html.convert(json=str_data)
            print("Total recipients :::: ",recipients)
            return (recipients,subject,data)
        else:
            pass
    except Exception as e:
        raise e


def check_atm_master(connection_string):
    sql_str = "select a.created_on as created_date, a.created_by as created_user from atm_master a, workflow_master b where b.sla <= (DATEDIFF(DAY, a.created_on,CURRENT_TIMESTAMP)) and a.record_status = 'Approval Pending' group by a.created_on, a.created_by";
    print(sql_str)
    conn = pyodbc.connect(connection_string)
    crsr = conn.cursor()
    try:
        crsr.execute(sql_str)
        pending_records_found = crsr.fetchall()
        if pending_records_found:
            (users,subject,data) = get_subject_user_data(connection_string,pending_records_found,"ATM")
            print("users mails ::: ",users)
            print("subject ::: ", subject)
            print("data ::: ", data)
            mail_ro_user(users,subject,data)
    except Exception as e:
        raise e

def check_cra_feasibility(connection_string):
    sql_str = "select a.created_on as created_date, a.created_by as created_user from cra_feasibility a, workflow_master b where b.sla <= (DATEDIFF(DAY, a.created_on,CURRENT_TIMESTAMP)) and a.record_status = 'Approval Pending' group by a.created_on, a.created_by";
    print(sql_str)
    conn = pyodbc.connect(connection_string)
    crsr = conn.cursor()
    try:
        crsr.execute(sql_str)
        pending_records_found = crsr.fetchall()
        if pending_records_found:
            (users, subject, data) = get_subject_user_data(connection_string, pending_records_found, "CRA_FEASIBILITY")
            print("users mails ::: ", users)
            print("subject ::: ", subject)
            print("data ::: ", data)
            mail_ro_user(users, subject, data)
    except Exception as e:
        raise e

def check_feeder_branch_mstr(connection_string):
    sql_str = "select a.created_on as created_date, a.created_by as created_user from feeder_branch_master a, workflow_master b where b.sla <= (DATEDIFF(DAY, a.created_on,CURRENT_TIMESTAMP)) and a.record_status = 'Approval Pending' group by a.created_on, a.created_by";
    print(sql_str)
    conn = pyodbc.connect(connection_string)
    crsr = conn.cursor()
    try:
        crsr.execute(sql_str)
        pending_records_found = crsr.fetchall()
        if pending_records_found:
            (users, subject, data) = get_subject_user_data(connection_string, pending_records_found, "FEEDER_BRANCH_MASTER")
            print("users mails ::: ", users)
            print("subject ::: ", subject)
            print("data ::: ", data)
            mail_ro_user(users, subject, data)
    except Exception as e:
        raise e

def check_atm_cnfig_lmts(connection_string):
    sql_str = "select a.created_on as created_date, a.created_by as created_user from ATM_Config_limits a, workflow_master b where b.sla <= (DATEDIFF(DAY, a.created_on,CURRENT_TIMESTAMP)) and a.record_status = 'Approval Pending' group by a.created_on, a.created_by";
    print(sql_str)
    conn = pyodbc.connect(connection_string)
    crsr = conn.cursor()
    try:
        crsr.execute(sql_str)
        pending_records_found = crsr.fetchall()
        if pending_records_found:
            (users, subject, data) = get_subject_user_data(connection_string, pending_records_found, "ATM_CNFG_LIMITS")
            print("users mails ::: ", users)
            print("subject ::: ", subject)
            print("data ::: ", data)
            mail_ro_user(users, subject, data)
    except Exception as e:
        raise e

def check_pending_records():

    if sys.argv[1] != None:
        if sys.argv[1] == 'Sit':
            connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=epstar;UID=sa;PWD=Mysql@123'
        elif sys.argv[1] == 'Uat':
            connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.208.17;DATABASE=epstar;UID=sqladmin;PWD=Mysql@123'
        elif sys.argv[1] == 'Dev':
            connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=127.0.0.1;DATABASE=epstar;UID=sa;PWD=Mysql@123'
        else:
            connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.0.117;DATABASE=epstar_region;UID=sa;PWD=Mysql@123'
    else:
        connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=epstar;UID=sa;PWD=Mysql@123'
    ams_atm_check = check_atm_master(connection_string)
    cra_feasibility_check = check_cra_feasibility(connection_string)
    feedr_brnch_check = check_feeder_branch_mstr(connection_string)
    atm_cnfig_lmts = check_atm_cnfig_lmts(connection_string)


if __name__ == '__main__':
    print(sys.argv[1])

    check_pending_records()

