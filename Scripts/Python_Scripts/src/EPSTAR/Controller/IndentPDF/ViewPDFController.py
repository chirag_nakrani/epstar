from Common.CommonFunctions import common_util
import logging
from Common import masterconf
from Model.models import indent_pdf_status
from rest_framework.response import Response
from rest_framework import status
import base64

class ViewPDFController():

    def handle_indent_pdf_view_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_indent_pdf_view_req METHOD OF ViewPDFController.")
        status_dict ={}
        try:
            get_pdf = indent_pdf_status.objects.get(indent_order_number=request.data['indent_order_number'])
            return Response(status=status.HTTP_201_CREATED, data=get_pdf.physical_file.decode('utf-8').rstrip('\n'))
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_indent_pdf_view_req METHOD OF ViewPDFController',
                         exc_info=True)
            status_dict['status_code'] = masterconf.error_pdf_view
            status_dict['status_text'] = masterconf.error_pdf_view_text
            return status_dict

