import datetime
import logging
import smtplib
import time
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import pandas as pd
from django.conf import settings

from Common import masterconf
from Common.CommonFunctions import common_util
from Model.models import Mail_Master, indent_mail_status, indent_pdf_status
import pyodbc
import os
import jinja2
from subprocess import Popen, PIPE
import base64
from celery import current_task

class PDFController():
    cnxn = pyodbc.connect(masterconf.connection_string)
    cursor = cnxn.cursor()

    def handle_pdf_create_req(self, request):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_pdf_create_req METHOD OF PDFController.")
        try:
            # reading indent_master details and storing it in a pandas dataframe
            if isinstance(request, dict):
                request_data = request['data']
            else:
                request_data = request.data
            df_master = pd.DataFrame()
            if 'indent_order_number' in request_data:
                sql_master = "SELECT a.indent_order_number as indent_order_number,order_date,collection_date,replenishment_date,cypher_code,bank,feeder_branch,cra,total_atm_loading_amount_50,total_atm_loading_amount_100,total_atm_loading_amount_200,total_atm_loading_amount_500,total_atm_loading_amount_2000,total_atm_loading_amount,cra_opening_vault_balance_50,cra_opening_vault_balance_100,cra_opening_vault_balance_200,cra_opening_vault_balance_500,cra_opening_vault_balance_2000,cra_opening_vault_balance,closing_vault_balance_100,closing_vault_balance_200,closing_vault_balance_500,closing_vault_balance_2000,total_closing_vault_balance,total_bank_withdrawal_amount_50,total_bank_withdrawal_amount_100,total_bank_withdrawal_amount_200,total_bank_withdrawal_amount_500,total_bank_withdrawal_amount_2000,total_bank_withdrawal_amount,authorized_by_1,authorized_by_2,signature_authorized_by_1_auth_signatories_signatures_id,signature_authorized_by_2_auth_signatories_signatures_id,indent_short_code,email_indent_mail_master_id,notes,amount_in_words,a.indent_type,project_id,atm_id,general_ledger_account_number,location,purpose,loading_amount_100,loading_amount_200,loading_amount_500,loading_amount_2000,total FROM indent_master a join indent_detail b on a.indent_order_number = b.indent_order_number and a.indent_order_number = '%s' and a.record_status = 'Active'" %(request_data['indent_order_number'])
                df_master = pd.read_sql(sql_master, self.cnxn)
            # elif 'bank_code' in request_data and 'indent_date' in request_data and 'project_id' in request_data:
            #     sql_master = "SELECT a.indent_order_number as indent_order_number,order_date,collection_date,replenishment_date,cypher_code,bank,feeder_branch,cra,total_atm_loading_amount_50,total_atm_loading_amount_100,total_atm_loading_amount_200,total_atm_loading_amount_500,total_atm_loading_amount_2000,total_atm_loading_amount,cra_opening_vault_balance_50,cra_opening_vault_balance_100,cra_opening_vault_balance_200,cra_opening_vault_balance_500,cra_opening_vault_balance_2000,cra_opening_vault_balance,total_bank_withdrawal_amount_50,total_bank_withdrawal_amount_100,total_bank_withdrawal_amount_200,total_bank_withdrawal_amount_500,total_bank_withdrawal_amount_2000,total_bank_withdrawal_amount,authorized_by_1,authorized_by_2,signature_authorized_by_1_auth_signatories_signatures_id,signature_authorized_by_2_auth_signatories_signatures_id,indent_short_code,email_indent_mail_master_id,notes,amount_in_words,project_id,atm_id,location,purpose,loading_amount_100,loading_amount_200,loading_amount_500,loading_amount_2000,total FROM indent_master a join indent_detail b on a.indent_order_number = b.indent_order_number and a.bank = '%s' and a.order_date = '%s' and a.project_id = '%s' and a.record_status = 'Active'" % (request_data['bank_code'], request_data['indent_date'], request_data['project_id'])
            #     df_master = pd.read_sql(sql_master, self.cnxn)
            elif 'bank_code' in request_data and 'indent_date' in request_data:
                bank_code = ", ".join(repr(e) for e in request_data['bank_code'])
                sql_master = "SELECT a.indent_order_number as indent_order_number,order_date,collection_date,replenishment_date,cypher_code,bank,feeder_branch,cra,total_atm_loading_amount_50,total_atm_loading_amount_100,total_atm_loading_amount_200,total_atm_loading_amount_500,total_atm_loading_amount_2000,total_atm_loading_amount,cra_opening_vault_balance_50,cra_opening_vault_balance_100,cra_opening_vault_balance_200,cra_opening_vault_balance_500,cra_opening_vault_balance_2000,cra_opening_vault_balance,closing_vault_balance_100,closing_vault_balance_200,closing_vault_balance_500,closing_vault_balance_2000,total_closing_vault_balance,total_bank_withdrawal_amount_50,total_bank_withdrawal_amount_100,total_bank_withdrawal_amount_200,total_bank_withdrawal_amount_500,total_bank_withdrawal_amount_2000,total_bank_withdrawal_amount,authorized_by_1,authorized_by_2,signature_authorized_by_1_auth_signatories_signatures_id,signature_authorized_by_2_auth_signatories_signatures_id,indent_short_code,email_indent_mail_master_id,notes,amount_in_words,a.indent_type,project_id,atm_id,general_ledger_account_number,location,purpose,loading_amount_100,loading_amount_200,loading_amount_500,loading_amount_2000,total FROM indent_master a join indent_detail b on a.indent_order_number = b.indent_order_number and a.bank IN (%s) and a.order_date = '%s' and a.record_status = 'Active'" % (bank_code, request_data['indent_date'])
                df_master = pd.read_sql(sql_master, self.cnxn)
            else:
                ### getting indent master with record_status Active ####
                sql_master =  "SELECT a.indent_order_number as indent_order_number,order_date,collection_date,replenishment_date,cypher_code,bank,feeder_branch,cra,total_atm_loading_amount_50,total_atm_loading_amount_100,total_atm_loading_amount_200,total_atm_loading_amount_500,total_atm_loading_amount_2000,total_atm_loading_amount,cra_opening_vault_balance_50,cra_opening_vault_balance_100,cra_opening_vault_balance_200,cra_opening_vault_balance_500,cra_opening_vault_balance_2000,cra_opening_vault_balance,closing_vault_balance_100,closing_vault_balance_200,closing_vault_balance_500,closing_vault_balance_2000,total_closing_vault_balance,total_bank_withdrawal_amount_50,total_bank_withdrawal_amount_100,total_bank_withdrawal_amount_200,total_bank_withdrawal_amount_500,total_bank_withdrawal_amount_2000,total_bank_withdrawal_amount,authorized_by_1,authorized_by_2,signature_authorized_by_1_auth_signatories_signatures_id,signature_authorized_by_2_auth_signatories_signatures_id,indent_short_code,email_indent_mail_master_id,notes,amount_in_words,a.indent_type,project_id,atm_id,general_ledger_account_number,location,purpose,loading_amount_100,loading_amount_200,loading_amount_500,loading_amount_2000,total FROM indent_master a join indent_detail b on a.indent_order_number = b.indent_order_number and a.record_status = 'Active' and a.order_date = CAST(GETDATE() as date)"
                df_master = pd.read_sql(sql_master, self.cnxn)
            full_data_list = self.process_data(df_master)

            return full_data_list

        except Exception as e:
            response_obj = {}
            response_obj['status_code'] = masterconf.error_code
            response_obj['status_text'] = masterconf.error_text
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_pdf_create_req METHOD OF PDFController', exc_info=True)
            return  response_obj

    def process_data(self,df_master):
        common_util_obj = common_util.common_utils()
        logger = logging.getLogger(__name__)
        full_data_list = []
        try:
            base_dir = settings.BASE_DIR
            current_milli_time = int(round(time.time() * 1000))
            df_master_indent = pd.DataFrame()
            df_master_indent = df_master[['indent_order_number']].drop_duplicates()
            df_master_final_page = df_master[
                ['indent_order_number', 'total_atm_loading_amount', 'total_atm_loading_amount_100',
                 'total_atm_loading_amount_200', 'total_atm_loading_amount_500', 'total_atm_loading_amount_2000',
                 'cra_opening_vault_balance', 'cra_opening_vault_balance_100', 'cra_opening_vault_balance_200',
                 'cra_opening_vault_balance_500', 'cra_opening_vault_balance_2000', 'total_bank_withdrawal_amount',
                 'total_bank_withdrawal_amount_100', 'total_bank_withdrawal_amount_200', 'total_bank_withdrawal_amount_500',
                 'total_bank_withdrawal_amount_2000', 'amount_in_words','closing_vault_balance_100','closing_vault_balance_200','closing_vault_balance_500',
                 'closing_vault_balance_2000','total_closing_vault_balance']].drop_duplicates()
            # creating header data
            df_master_header_template_vars = df_master[
                ['indent_short_code','indent_order_number', 'order_date', 'project_id', 'collection_date',
                 'replenishment_date', 'cypher_code', 'bank', 'feeder_branch',
                 'cra','indent_type']].drop_duplicates()
            df_master_header_template_vars['base_dir'] = base_dir
            df_master_header_template_vars['title'] = 'Indent Report'
            # creating footer data
            df_master_footer_template_vars = df_master[['indent_order_number',
                                                        'signature_authorized_by_1_auth_signatories_signatures_id',
                                                        'signature_authorized_by_2_auth_signatories_signatures_id',
                                                        'authorized_by_1', 'authorized_by_2',
                                                        'email_indent_mail_master_id']].drop_duplicates()

            for index, df_master_row in df_master_indent.iterrows():
                indent_order_number = df_master_row['indent_order_number']
                df_detail = df_master[df_master['indent_order_number'] == indent_order_number]
                df_detail = df_detail[
                    ['atm_id', 'general_ledger_account_number', 'location', 'purpose', 'loading_amount_100', 'loading_amount_200', 'loading_amount_500',
                     'loading_amount_2000', 'total']]

                # filtering out indent detail of each indent_order_number from the indent master table and storing it in a pandas dataframe
                final_page = df_master_final_page[df_master_final_page['indent_order_number'] == indent_order_number]
                final_page = common_util_obj.final_pdf_page_values(final_page)

                # creating header data
                header_template_vars = df_master_header_template_vars[
                    df_master_header_template_vars['indent_order_number'] == indent_order_number]
                header_template_vars = common_util_obj.header_template_fields(header_template_vars)
                header_template_vars['base_dir'] = base_dir
                header_template_vars['title'] = 'Indent Report'

                # creating footer data
                footer_template_vars = df_master_footer_template_vars[
                    df_master_footer_template_vars['indent_order_number'] == indent_order_number]
                footer_template_vars = common_util_obj.footer_template_fields(footer_template_vars)

                # creating table detail data
                detail_data = common_util_obj.detail_template_fields(df_detail)
                detail_list = []

                # diving the table data into chunks upto 25 rows
                for k in range(0, len(detail_data), 25):
                    detail_list.append(detail_data[k:k + 25])

                # creating pagewise output for pdf
                full_data_list.append(common_util_obj.pagewise_data(detail_list, final_page, header_template_vars,
                                                                    footer_template_vars))
            finished = int(round(time.time() * 1000))
        except Exception as e:
            logger.error(e)
        logger.info('List created for creating pdf pagewise.')
        return full_data_list

    def create_pdf(self,response_list,username,reference_id):
        logger = logging.getLogger(__name__)
        logger.info('Inside create_pdf function.')
        path_wkthmltopdf = masterconf.path_to_wkhtmltopdf
        template = jinja2.Template(open(os.path.join(settings.BASE_DIR, 'templates') + '\\indent_pdf.html').read())
        body_template = jinja2.Template(open(os.path.join(settings.BASE_DIR, 'templates') + '\\page_indent.html').read())
        current_datetime = datetime.datetime.fromtimestamp(time.time()).strftime(
            '%Y-%m-%d %H:%M:%S')
        response_data = []
        try:
            status_obj = {}
            file_name = ''
            ## current date
            date_now = datetime.datetime.now().strftime('%d-%m-%Y')
            ### creating date wise folder
            count = 0
            for data in response_list:
                html_final = ''
                html = ''
                pdf_status_obj = {}
                header_vars = None
                footer_vars = None
                for d in data:
                    body_vars = d['indent_data']
                    carry_forward = d['carry_forward']
                    footer_vars = d['footer']
                    header_vars = d['header']
                    try:
                        ### appending each pdf page to html ###
                        html = html + body_template.render(
                            {'body_vars': body_vars, 'carry_forward': carry_forward,
                             'header_vars': header_vars, 'footer_vars': footer_vars})
                        ###rendering final html layout to pdf html page layout ####
                        html_final = template.render({'body': html})

                        ### creating file name ###
                        # generate_name = '%s-%s-%s-%s' % (header_vars['bank'], header_vars['project_id'],
                        #                                  header_vars['feeder_branch'],
                        #                                  header_vars['order_date'])

                        generate_name = header_vars['indent_order_number'].replace(' ', '_').replace('/', '_')
                        pdf_file_dir = settings.BASE_DIR + '\FileFolder\IndentPDFFolder\%s\%s\%s' % (header_vars['project_id'],header_vars['bank'],date_now)
                        if not os.path.exists(pdf_file_dir):
                            os.makedirs(pdf_file_dir)
                        file_name = '%s\%s.pdf' % (pdf_file_dir, generate_name)

                        #### options to create pdf layout ####
                        p = Popen([path_wkthmltopdf, '--page-size', 'A4',
                                   '--margin-top', '0.45in',
                                   '--margin-right', '0.45in',
                                   '--margin-bottom', '0.45in',
                                   '--margin-left', '0.45in',
                                   '--encoding', "UTF-8",
                                   '--footer-html', os.path.join(settings.BASE_DIR,'templates')+'\\footer_indent.html',
                                   '-', '-'], stdin=PIPE, stdout=PIPE)

                        ### creating pdf file ###
                        output, _ = p.communicate(input=html_final.encode('utf-8'))
                        f = open(file_name, 'w+b')
                        f.write(output)
                        f.close()

                        status_obj['bank'] = header_vars['bank']
                        status_obj['project_id'] = header_vars['project_id']
                        status_obj['feeder_branch'] = header_vars['feeder_branch']
                        status_obj['cra'] = header_vars['cra']
                        status_obj['indent_order_number'] = header_vars['indent_order_number']
                        status_obj['email_id'] = footer_vars['email_indent_mail_master_id']
                        status_obj['file_name'] = file_name
                        status_obj['mail_file_name'] = generate_name
                        status_obj['indent_type'] = header_vars['indent_type']
                        status_obj['username'] = username
                        status_obj['reference_id'] = reference_id
                        status_obj['current_datetime'] = datetime.datetime.fromtimestamp(time.time()).strftime(
                                                        '%Y-%m-%d %H:%M:%S')
                    except Exception as e:
                        logger.error('Exception occured while creating pdf', exc_info=True)
                        logger.error(e)
                response_data.append(self.pdf_status_insert(status_obj))
                count += 1
                if len(response_list) > 1:
                    current_task.update_state(state='PROGRESS',
                                              meta={'current': count, 'total': len(response_list),
                                                    'percent': int((float(count) / len(response_list)) * 100)})
        except Exception as e:
            response_obj = {}
            response_obj['status_code'] = masterconf.failure_pdf
            response_obj['status_text'] = masterconf.failure_pdf_text
            response_obj['exception'] = e
            response_data.append(response_obj)
            logger.error('Exception occured while creating pdf', exc_info=True)
            logger.error(e)

        logger.info('Returned response from create_pdf: %s',response_data)
        return response_data

    #### insert pdf status ###
    def pdf_status_insert(self,status_obj):
        logger = logging.getLogger(__name__)
        logger.info('Inside pdf_status_insert function.')
        logger.info('Request parameters received: %s',status_obj)
        response_data = []
        response_obj = {}
        update_pdf_record, insert_pdf_record = None, None
        try:
            update_pdf_record, insert_pdf_record = indent_pdf_status.objects.get_or_create(
                bank_code=status_obj['bank'],
                project_id=status_obj['project_id'],
                feeder_branch=status_obj['feeder_branch'],
                cra=status_obj['cra'],
                indent_order_number=status_obj['indent_order_number'])
            #### sending email to respective feeder branch and bank ####

            mail_obj = {}
            mail_obj['file_name'] = status_obj['file_name']
            mail_obj['mail_file_name'] = '%s.pdf' % status_obj['mail_file_name']
            mail_obj['id'] = status_obj['email_id']
            mail_obj['indent_order_number'] = status_obj['indent_order_number']

            response_obj['project_id'] = status_obj['project_id']
            response_obj['bank_code'] = status_obj['bank']
            response_obj['feeder_branch'] = status_obj['feeder_branch']
            response_obj['cra'] = status_obj['cra']
            response_obj['indent_order_number'] = status_obj['indent_order_number']
            if insert_pdf_record == True:
                update_pdf_record.file_name = status_obj['file_name']
                with open(status_obj['file_name'], 'rb') as f:
                    pdf = f.read()
                update_pdf_record.physical_file = base64.b64encode(pdf)
                update_pdf_record.indent_type = status_obj['indent_type']
                update_pdf_record.pdf_status = masterconf.success_pdf_text
                update_pdf_record.indent_date = status_obj['current_datetime']
                update_pdf_record.record_status = masterconf.api_status_Success
                update_pdf_record.created_on = status_obj['current_datetime']
                update_pdf_record.created_by = status_obj['username']
                update_pdf_record.created_reference_id = status_obj['reference_id']
                update_pdf_record.save()
            else:
                update_pdf_record.file_name = status_obj['file_name']
                with open(status_obj['file_name'], 'rb') as f:
                    pdf = f.read()
                update_pdf_record.indent_type = status_obj['indent_type']
                update_pdf_record.physical_file = base64.b64encode(pdf)
                update_pdf_record.pdf_status = masterconf.success_pdf_text
                update_pdf_record.indent_date = status_obj['current_datetime']
                update_pdf_record.modified_on = status_obj['current_datetime']
                update_pdf_record.modified_by = status_obj['username']
                update_pdf_record.modified_reference_id = status_obj['reference_id']
                update_pdf_record.record_status = masterconf.api_status_Success
                update_pdf_record.save()
            response_obj['email_status'] = self.notify_mail_operation_status(
                mail_obj,status_obj)
            response_obj['status_code'] = masterconf.success_pdf
            response_obj['status_text'] = masterconf.success_pdf_text

            response_data.append(response_obj)
        except Exception as e:
            if insert_pdf_record == True:
                update_pdf_record.indent_type = status_obj['indent_type']
                update_pdf_record.pdf_status = masterconf.failure_pdf_text
                update_pdf_record.indent_date = status_obj['current_datetime']
                update_pdf_record.record_status = masterconf.api_status_Failure
                update_pdf_record.created_on = status_obj['current_datetime']
                update_pdf_record.created_by = status_obj['username']
                update_pdf_record.created_reference_id = status_obj['reference_id']
                update_pdf_record.save()
            else:
                update_pdf_record.indent_type = status_obj['indent_type']
                update_pdf_record.pdf_status = masterconf.failure_pdf_text
                update_pdf_record.indent_date = status_obj['current_datetime']
                update_pdf_record.record_status = masterconf.api_status_Failure
                update_pdf_record.modified_on = status_obj['current_datetime']
                update_pdf_record.modified_by = status_obj['username']
                update_pdf_record.modified_reference_id = status_obj['reference_id']
                update_pdf_record.save()
        return response_obj

    #### send pdf mail to the respective feeder branch and bank #####
    def notify_mail_operation_status(self, mail_obj,status_obj):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE notify_mail_operation_status METHOD OF PDFController.")
        logger.info('Request parameters received for sending email: %s, %s',mail_obj,status_obj)
        response_obj = {}
        try:
            ### Get mail data for indent_mail_status model using email_id from indent_master table ####
            mail_data = pd.DataFrame(list(Mail_Master.objects.filter(id=mail_obj['id']).values()))
            #### find pdf file in indent_pdf_status table ####
            try:
                indent_order_number = indent_pdf_status.objects.get(
                    indent_order_number=mail_obj['indent_order_number'])
                file_name = indent_order_number.file_name
            except Exception as e:
                response_obj['status_code'] = masterconf.file_not_found
                response_obj['status_text'] = masterconf.file_not_found_error
                logger.error('PDF file not created for sending email.')
                logger.error(e)
                logger.error("Returned response: %s",response_obj)
                return response_obj

            mail_file_name = mail_obj['mail_file_name']
            created_on = status_obj['current_datetime']
            created_by = status_obj['username']
            indent_type = status_obj['indent_type']
            created_reference_id = status_obj['reference_id']

            update_mail_record, insert_mail_record = indent_mail_status.objects.get_or_create(
                bank_code=mail_data.loc[0, 'bank_code'],
                project_id=mail_data.loc[0, 'project_id'],
                feeder_branch=mail_data.loc[0, 'feeder_branch'],
                cra=mail_data.loc[0, 'cra'],
                indent_order_number=mail_obj['indent_order_number'])

            #### if new mail record is created save file_name in mail_status table for future use ####
            if insert_mail_record == True:
                update_mail_record.file_name = indent_order_number.file_name
                update_mail_record.save()

            response_obj = self.send_mail(update_mail_record, insert_mail_record, file_name, mail_file_name, mail_data, created_on,
                                          created_by, created_reference_id, indent_type)
            return response_obj

        except Exception as e2:
            response_obj['status_code'] = masterconf.failure_maildata_pdf
            response_obj['status_text'] = masterconf.failure_maildata_pdf_text
            update_mail_record, insert_mail_record = indent_mail_status.objects.get_or_create(
                bank_code=status_obj['bank'],
                project_id=status_obj['project_id'],
                feeder_branch=status_obj['feeder_branch'],
                cra=status_obj['cra'],
                indent_order_number=mail_obj['indent_order_number'])
            if insert_mail_record == True:
                update_mail_record.record_status = masterconf.api_status_Failure
                update_mail_record.email_status = masterconf.failure_maildata_pdf_text
                update_mail_record.indent_date = status_obj['current_datetime']
                update_mail_record.created_on = status_obj['current_datetime']
                update_mail_record.created_by = status_obj['username']
                update_mail_record.created_reference_id = status_obj['reference_id']
                update_mail_record.file_name = status_obj['file_name']
                update_mail_record.save()
            else:
                update_mail_record.record_status = masterconf.api_status_Failure
                update_mail_record.email_status = masterconf.failure_maildata_pdf_text
                update_mail_record.indent_date = status_obj['current_datetime']
                update_mail_record.modified_on = status_obj['current_datetime']
                update_mail_record.modified_by = status_obj['username']
                update_mail_record.modified_reference_id = status_obj['reference_id']
                update_mail_record.file_name = status_obj['file_name']
                update_mail_record.save()

            logger.error('Mail not sent.')
            logger.error(e2)
            logger.error("Returned response: %s", response_obj)
        return response_obj

    ### send mail function for pdf ####
    def send_mail(self,mail_record, insert_mail_record, file_name,mail_file_name,mail_data, created_on,created_by,created_reference_id, indent_type):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE send_mail METHOD OF PDFController.")
        response_obj = {}
        try:
            message = MIMEMultipart()
            # message['From'] = 'epstar@electronicpay.in'
            message['From'] = mail_data.loc[0, 'from_email_id']
            message['To'] = mail_data.loc[0, 'to_bank_email_id']
            message['CC'] = mail_data.loc[0, 'to_cra_email_id']
            message['Bcc'] = mail_data.loc[0, 'bcc_email_id']
            bank_code = mail_data.loc[0, 'bank_code']
            if indent_type == masterconf.revised_indent_type:
                message['Subject'] = mail_data.loc[0, 'subject'] + '-' + datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d') + '-' + indent_type
            else:
                message['Subject'] = mail_data.loc[0, 'subject'] + '-' + datetime.datetime.strftime(
                    datetime.datetime.now(), '%Y-%m-%d')
            message.attach(MIMEText(mail_data.loc[0, 'body_text']))
            with open(file_name, "rb") as fil:
                part = MIMEApplication(
                    fil.read(),
                    Name=mail_file_name
                )
            #### After the file is closed after read operation ####
            part['Content-Disposition'] = 'attachment; filename="%s"' % mail_file_name
            message.attach(part)
            #### update record if exists else insert record ####
            try:
                #### smtp configuration for mail ####
                server = smtplib.SMTP()
                server.connect(masterconf.MAILHOST, masterconf.MAILPORT)
                server.starttls()
                server.login(masterconf.ALL_mail_sender[bank_code], masterconf.ALL_mail_password[bank_code])
                recipients = []
                cra_list = []
                to_email_list = []
                bcc_email_list = []
                if mail_data.loc[0, 'to_bank_email_id'] != '':
                    to_bank_data = str(mail_data.loc[0, 'to_bank_email_id'])
                    if ',' in to_bank_data:
                        to_bank_data = to_bank_data.replace(',',';')
                    if ' ' in to_bank_data:
                        to_bank_data = to_bank_data.replace(' ', '')
                    to_email_list = to_bank_data.split(';')
                    for to_bank in to_email_list:
                        recipients.append(to_bank)
                if mail_data.loc[0, 'to_cra_email_id'] != '':
                    cra_data_str = str(mail_data.loc[0, 'to_cra_email_id'])
                    if ',' in cra_data_str:
                        cra_data_str = cra_data_str.replace(',',';')
                    if ' ' in cra_data_str:
                        cra_data_str = cra_data_str.replace(' ', '')
                    cra_list = cra_data_str.split(';')
                    for cra in cra_list:
                        recipients.append(cra)
                if mail_data.loc[0, 'bcc_email_id'] != '':
                    bcc_data = str(mail_data.loc[0, 'bcc_email_id'])
                    if ',' in bcc_data:
                        bcc_data = bcc_data.replace(',',';')
                    if ' ' in bcc_data:
                        bcc_data = bcc_data.replace(' ', '')
                    bcc_email_list = bcc_data.split(';')
                    for bcc in bcc_email_list:
                        recipients.append(bcc)
                    print(" recipients are :::: ",recipients)
                if recipients != []:
                    server.sendmail(masterconf.ALL_mail_sender[bank_code], recipients, message.as_string())
                    logger.info("MAIL SENT.")
                    response_obj['status_code'] = masterconf.success_mail_pdf
                    response_obj['status_text'] = masterconf.success_mail_pdf_text
                    mail_record.email_status = masterconf.success_mail_pdf_text
                    mail_record.record_status = masterconf.api_status_Success
                else:
                    logger.info("MAIL NOT SENT.")
                    response_obj['status_code'] = masterconf.failure_mail_pdf
                    response_obj['status_text'] = masterconf.recipients_not_found_error
                    mail_record.email_status = masterconf.recipients_not_found_error
                    mail_record.record_status = masterconf.api_status_Failure
                server.close()
                #### if mail record found then update email_status and indent generation date ####
                mail_record.file_name = file_name
                mail_record.indent_date = datetime.datetime.fromtimestamp(time.time()).strftime(
                    '%Y-%m-%d %H:%M:%S')
                if insert_mail_record == True:
                    mail_record.created_on = datetime.datetime.fromtimestamp(time.time()).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    mail_record.created_by = created_by
                    mail_record.created_reference_id = created_reference_id
                else:
                    mail_record.modified_on = datetime.datetime.fromtimestamp(time.time()).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    mail_record.modified_by = created_by
                    mail_record.modified_reference_id = created_reference_id
                mail_record.save()
            except Exception as e1:
                response_obj['status_code'] = masterconf.failure_mail_pdf
                response_obj['status_text'] = masterconf.failure_mail_pdf_text
                mail_record.file_name = file_name
                mail_record.email_status = masterconf.failure_maildata_pdf_text
                mail_record.record_status = masterconf.api_status_Failure
                mail_record.indent_date = datetime.datetime.fromtimestamp(time.time()).strftime(
                    '%Y-%m-%d %H:%M:%S')
                if insert_mail_record == True:
                    mail_record.created_on = datetime.datetime.fromtimestamp(time.time()).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    mail_record.created_by = created_by
                    mail_record.created_reference_id = created_reference_id
                else:
                    mail_record.modified_on = datetime.datetime.fromtimestamp(time.time()).strftime(
                        '%Y-%m-%d %H:%M:%S')
                    mail_record.modified_by = created_by
                    mail_record.modified_reference_id = created_reference_id
                mail_record.save()
                logger.error(e1)
        except Exception as e:
            response_obj['status_code'] = masterconf.failure_mail_pdf
            response_obj['status_text'] = masterconf.failure_mail_pdf_text
            logger.error(e)
        logger.info('Returned response from send_mail function of PDFController: %s',response_obj)
        return response_obj
