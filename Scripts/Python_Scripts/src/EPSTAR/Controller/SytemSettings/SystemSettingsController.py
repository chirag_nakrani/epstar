from Common.CommonFunctions import common_util
import logging
from Model.models import system_settings
import datetime
from datetime import timedelta
from Common import masterconf

class SystemSettingsController():

    ### creating system settings ####
    def handle_system_settings_create_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_system_settings_create_req METHOD OF SystemSettingsController.")
        request_data = request.data
        created_by = request.userinfo['username'].username
        created_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        created_reference_id = request.userinfo['reference_id']
        status_dict ={}
        try:
            ##### creating system settings ###
            data = common_util_obj.validate_systemSettings_InsertReq(request_data)
            data['created_by'] = created_by
            data['created_on'] = created_on
            data['created_reference_id'] = created_reference_id
            data['record_status'] = masterconf.system_settings_approval_pending
            try:
                system_data = system_settings(**data)
                system_data.save()
                if system_data:
                    status_dict['status_code'] = masterconf.system_settings_success
                    status_dict['status_text'] = masterconf.system_settings_success_text
                    status_dict["data"] = data
                else:
                    status_dict['status_code'] = masterconf.system_settings_error
                    status_dict['status_text'] = masterconf.system_settings_error_text
                    status_dict["data"] = data
            except Exception as e:
                print(e)


            return status_dict

        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_system_settings_create_req METHOD OF SystemSettingsController', exc_info=True)
            raise e

    ### approve system settings ###
    def handle_system_settings_approve_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_system_settings_approve_req METHOD OF SystemSettingsController.")
        approved_by = request.userinfo['username'].username
        approved_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        approved_reference_id = request.userinfo['reference_id']
        status_dict ={}
        try:
            #### update if any active record is present to History ###
            system_settings.objects.filter(record_status='Active').update(record_status='History')
            ### updating system settings to active status ###
            system_data = system_settings.objects.get(id=request.data['id'])
            system_data.approved_by = approved_by
            system_data.approved_on = approved_on
            system_data.approved_reference_id = approved_reference_id
            system_data.modified_by = approved_by
            system_data.modified_on = approved_on
            system_data.modified_reference_id = approved_reference_id
            system_data.record_status = masterconf.system_settings_active
            system_data.save()
            if system_data:
                status_dict['status_code'] = masterconf.system_settings_approval_success
                status_dict['status_text'] = masterconf.system_settings_approval_success_text
                status_dict['data'] = system_data.id
            else:
                status_dict['status_code'] = masterconf.system_settings_approval_error
                status_dict['status_text'] = masterconf.system_settings_approval_error_text
                status_dict['data'] = system_data.id
            return status_dict

        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_system_settings_approve_req METHOD OF SystemSettingsController', exc_info=True)
            raise e

    #### reject system settings ###
    def handle_system_settings_reject_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_system_settings_reject_req METHOD OF SystemSettingsController.")
        rejected_by = request.userinfo['username'].username
        rejected_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        rejected_reference_id = request.userinfo['reference_id']
        status_dict ={}
        try:
            ### updating system settings to History status ###
            system_data = system_settings.objects.get(id=request.data['id'])
            system_data.modified_by = rejected_by
            system_data.modified_on = rejected_on
            system_data.modified_reference_id = rejected_reference_id
            system_data.record_status = masterconf.system_settings_history
            system_data.save()
            if system_data:
                status_dict['status_code'] = masterconf.system_settings_reject_success
                status_dict['status_text'] = masterconf.system_settings_reject_success_text
                status_dict['data'] = system_data.id
            else:
                status_dict['status_code'] = masterconf.system_settings_reject_error
                status_dict['status_text'] = masterconf.system_settings_reject_error_text
                status_dict['data'] = system_data.id
            return status_dict

        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_system_settings_reject_req METHOD OF SystemSettingsController', exc_info=True)
            raise e

    #### list system settings ####
    def handle_system_settings_list_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_system_settings_list_req METHOD OF SystemSettingsController.")
        status_dict ={}
        try:
            #### list system settings based on filters ####
            if request.data != '':
                system_data = system_settings.objects.filter(**request.data).values()
            else:
                system_data = system_settings.objects.all().values()

            if system_data:
                status_dict['status_code'] = masterconf.system_settings_list_success
                status_dict['status_text'] = masterconf.system_settings_list_success_text
                status_dict['data'] = system_data
            else:
                status_dict['status_code'] = masterconf.system_settings_list_error
                status_dict['status_text'] = masterconf.system_settings_list_error_text
                status_dict['data'] = []
            return status_dict

        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_system_settings_list_req METHOD OF SystemSettingsController', exc_info=True)
            raise e
