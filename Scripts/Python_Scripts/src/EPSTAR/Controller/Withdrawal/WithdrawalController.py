import time, datetime
from datetime import timedelta
import logging
from Common import masterconf, db_queries_properties
from Model.models import withdrawal_status
import base64
import os
import io
from PIL import Image
import base64
import uuid
from django.http import HttpResponse
from Model.models import indent_master
import logging


class WithdrawalController:

    def handle_withdrawal_conf_req(self, return_file_path, request):
        logger = logging.getLogger(__name__)
        request_data = request.POST
        created_by = request.userinfo['username'].username
        created_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        created_reference_id = request.userinfo['reference_id']
        status_dict = {}
        temp_dict = {}
        successfully_inserted_or_updated = False
        try:
            encodestring = None
            logger.info("In handle_withdrawal_conf_req of WithdrawalController")
            temp_dict['bank_code'] = request_data['bank_code']
            temp_dict['project_id'] = request_data['project_id']
            temp_dict['feeder_branch'] = request_data['feeder_branch']
            temp_dict['cra'] = request_data['cra']
            temp_dict['indent_short_code'] = request_data['indent_short_code']
            temp_dict['total_withdrawal_amount'] = request_data['total_withdrawal_amount']
            temp_dict['denomination_100'] = request_data['denomination_100']
            temp_dict['denomination_200'] = request_data['denomination_200']
            temp_dict['denomination_500'] = request_data['denomination_500']
            temp_dict['denomination_2000'] = request_data['denomination_2000']
            temp_dict['remarks'] = request_data['remarks']
            temp_dict['created_by'] = created_by
            temp_dict['created_on'] = created_on
            temp_dict['created_reference_id'] = created_reference_id
            temp_dict['record_status'] = masterconf.new_data_record_status
            if 'file' in request.FILES:
                with open(return_file_path, 'rb') as f:
                    photo = f.read()
                encodestring = base64.b64encode(photo)
            temp_dict['withdrawal_slip'] = encodestring
            if withdrawal_status.objects.filter(indent_short_code=request_data['indent_short_code'],record_status=masterconf.new_data_record_status).exists():
                update_record = withdrawal_status.objects.filter(indent_short_code=request_data['indent_short_code'])
                update_record.update(bank_code=temp_dict['bank_code'], project_id=temp_dict['project_id'],
                                     feeder_branch=temp_dict['feeder_branch'], cra=temp_dict['cra'],
                                     indent_short_code=temp_dict['indent_short_code'],
                                     total_withdrawal_amount=temp_dict['total_withdrawal_amount'],
                                     denomination_100=temp_dict['denomination_100'],
                                     denomination_200=temp_dict['denomination_200'],
                                     denomination_500=temp_dict['denomination_500'],
                                     denomination_2000=temp_dict['denomination_2000'],
                                     remarks=temp_dict['remarks'],modified_by=created_by,
                                     modified_on=created_on,modified_reference_id=created_reference_id,
                                     withdrawal_slip=encodestring)
                successfully_inserted_or_updated = True
            else:
                withdrawal_status_data = withdrawal_status(**temp_dict)
                withdrawal_status_data.save()
                successfully_inserted_or_updated = True
            logger.info("data successfully saved in withdrawal_status table")
            if successfully_inserted_or_updated:
                status_dict['status_code'] = masterconf.success_code
                status_dict['status_text'] = masterconf.upload_success_text
                status_dict["data"] = request_data
            else:
                status_dict['status_code'] = masterconf.error_code
                status_dict['status_text'] = masterconf.error_text
                status_dict["data"] = request_data
            logger.info("status_dict returned from WithdrawalController :::: %s", status_dict)
            return status_dict
        except Exception as e:
            logger.error('Exception in handle_withdrawal_conf_req of WithdrawalController %s ', e)
            raise e

    def handle_withdrawal_conf_view_req(self, request):
        status_dict = {}
        print(" request.GET ::: ",request.GET)
        indent_short_code = dict(request.GET)['indent_short_code'][0]
        record_status = 'Active'
        logger = logging.getLogger(__name__)
        try:
            is_record_exists = withdrawal_status.objects.filter(indent_short_code=indent_short_code,
                                                                record_status=record_status).exists()
            if is_record_exists:
                logger.info("Record exists::: %s", is_record_exists)
                withdrawal_slip = withdrawal_status.objects.filter(indent_short_code=indent_short_code,
                                                                   record_status=record_status).values(
                    'withdrawal_slip')[0]['withdrawal_slip']
                print(" withdrawal_slip ",withdrawal_slip)
                if withdrawal_slip != None:
                    data1 = base64.b64decode(withdrawal_slip)
                    unique_id = uuid.uuid4()
                    file_like = io.BytesIO(data1)
                    img = Image.open(file_like)
                    m = masterconf.withdrawal_output_folder + str(unique_id) + "_extracted.png"
                    img.save(m)
                    file_name = str(unique_id) + "_extracted.png"
                    path_to_file = masterconf.withdrawal_output_folder
                    if os.path.exists(path_to_file + file_name):
                        with open(path_to_file + file_name, 'rb') as fh:
                            # response = HttpResponse(fh, content_type="application/force-download")
                            response = HttpResponse(fh, content_type='image/png')
                            response['Content-Disposition'] = "'attachment; filename=%s' % " + file_name
                            return response
                else:
                    status_dict["status_text"] = masterconf.withdrawal_not_available
                    status_dict["status_code"] = masterconf.signature_failure_status_code
                    return status_dict
            else:
                status_dict["status_text"] = masterconf.withdrawal_not_available
                status_dict["status_code"] = masterconf.signature_failure_status_code
                return status_dict
        except Exception as e:
            logger.error('Exception in handle_withdrawal_conf_view_req of  WithdrawalController :: %s ', e)
            raise e

    def get_all_withdrawal_records(self, request):
        logger = logging.getLogger(__name__)
        records = []
        try:
            records = list(
                withdrawal_status.objects.filter(record_status='Active').values('indent_order_number','indent_short_code', 'bank_code',
                                                                                'project_id', 'feeder_branch', 'cra',
                                                                                'total_withdrawal_amount',
                                                                                'denomination_100', 'denomination_200',
                                                                                'denomination_500', 'denomination_2000',
                                                                                'remarks', 'record_status',
                                                                                'created_on', 'created_by',
                                                                                'created_reference_id', 'deleted_on',
                                                                                'deleted_by', 'deleted_reference_id',
                                                                                'modified_on', 'modified_by',
                                                                                'modified_reference_id'))
            return records
        except Exception as e:
            logger.error('Exception in get_indents of  WithdrawalController :: %s ', e)
            raise e

    def delete_withdrawal_records(self, request):
        logger = logging.getLogger(__name__)
        status_dict = {}
        try:
            deleted_by = request.userinfo['username'].username
            deleted_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
            deleted_reference_id = request.userinfo['reference_id']
            indent_short_code = request.data['indent_short_code']
            is_already_deleted = withdrawal_status.objects.filter(indent_short_code=indent_short_code,
                                                                  record_status=masterconf.status_deleted).exists()
            print(" is_already_deleted ::: ", is_already_deleted)
            if is_already_deleted:
                status_dict['status_code'] = 'E108'
            else:
                with_record = withdrawal_status.objects.filter(indent_short_code=indent_short_code)
                print(" with_record ::: ", with_record)
                if len(with_record) > 0:
                    with_record.update(deleted_by=deleted_by, deleted_on=deleted_on,
                                       deleted_reference_id=deleted_reference_id,
                                       record_status=masterconf.status_deleted)
                    status_dict['status_code'] = 'S106'
                else:
                    status_dict['status_code'] = 'E107'
            return status_dict
        except Exception as e:
            logger.error('Exception in delete_withdrawal_records of  WithdrawalController :: %s ', e)
            raise e
