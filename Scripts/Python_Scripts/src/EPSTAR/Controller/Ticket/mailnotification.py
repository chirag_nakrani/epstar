import datetime
import pyodbc
import smtplib
import time

from Common import masterconf, db_queries_properties
from Common.CommonFunctions import common_util
from Model.models import ticket_category_master, ticket_master, workflow_master
from django.contrib.auth.models import User,Group,Permission
from json2html import *
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from Common.models import data_update_log_master

class MailNotification():

    def notify_mail_operation_status(self, recipient, subject, json_error_codes):

        # This method is used to send mail

        common_util_obj = common_util.common_utils()
        try:
            message = MIMEMultipart('alternative')
            message['Subject'] = subject
            body = MIMEText(json_error_codes, 'html')
            message.attach(body)
            #message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
            server = smtplib.SMTP()
            server.connect(masterconf.MAILHOST, masterconf.MAILPORT)
            server.starttls()
            server.login(masterconf.mail_sender, masterconf.mail_password)
            server.sendmail(masterconf.mail_sender, recipient, message.as_string())
            server.close()
        except Exception as e:
            raise e

    def get_atmid_column_name(self, raw_tablename):

        # This method is used to fetch the relevant atmid column name in various raw tables

        conn = pyodbc.connect(masterconf.connection_string);
        crsr = conn.cursor()
        try:
            sql_str = """select COLUMN_NAME,TABLE_NAME from INFORMATION_SCHEMA.columns 
                                        where COLUMN_NAME IN ('termid','atm_id','atmid','term_id', 'Terminal_ID')
                                        AND TABLE_NAME = """ + "'" + raw_tablename + "'"

            print(sql_str)
            crsr.execute(sql_str)
            atmid_column_name = crsr.fetchone()
            return atmid_column_name[0]
        except Exception as e:
            raise e

    def get_error_codes_files(self, request, raw_tablename):

        # This method is used to get error codes from various raw tables of banks

        conn = pyodbc.connect(masterconf.connection_string)
        crsr = conn.cursor()
        created_reference_id = request.userinfo['reference_id']
        try:
            atm_id = self.get_atmid_column_name(raw_tablename)
            nocount = """ SET NOCOUNT ON; """
            sql_str = """DECLARE @output nvarchar(max) = ( SELECT atmid,code,description FROM
                        ( 
                        select top 100 r.""" + atm_id + """ as atmid ,
                        s.value as code, 
                        e.value as description
                        from """ + raw_tablename + " " + """ r
                        outer apply STRING_SPLIT(r.error_code,',') s
                        join app_config_param e on e.sequence = s.value
                        and r.created_reference_id = """ + "'" + created_reference_id + "'" + """
                        and r.error_code IS NOT NULL
                        order by datafor_date_time desc
                        ) a  FOR JSON Path
                        )
                        select @output
                        """
            print(sql_str)
            crsr.execute(nocount + sql_str)
            json_error_codes = crsr.fetchone()
            print(json_error_codes[0])
            return json_error_codes[0]
        except Exception as e:
            raise e

    def notify_and_raise_ticket_routine(self, request):

        # This method is used to generate ticket for routine files

        common_util_obj = common_util.common_utils()
        created_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        created_by = request.userinfo['username'].username
        created_reference_id = request.userinfo['reference_id']
        category = masterconf.category_routine
        request_post = request.POST
        file_type = request_post['file_type']
        try:
            metadata_dict = self.get_meta_info_routine(request)
            json_error_codes = self.get_error_codes_files(request, metadata_dict['raw_table'])
            if json_error_codes is not None:
                is_cat_id_exists = ticket_category_master.objects.filter(category=metadata_dict['category'],
                                                                    sub_category=metadata_dict['sub_category']).exists()
                if is_cat_id_exists:
                    category_id = ticket_category_master.objects.filter(category=metadata_dict['category'],
                                                                    sub_category=metadata_dict['sub_category']).values('category_id')
                    print(" category_id ::: ", category_id)
                    cat_id = category_id[0]['category_id']
                    is_user_for_cat_id = workflow_master.objects.filter(category_id=cat_id).exists()
                    if is_user_for_cat_id:
                        assigned_to_user = workflow_master.objects.filter(category_id=cat_id).values('assigned_to_user')[0][
                            'assigned_to_user']
                        assigned_to_group = workflow_master.objects.filter(category_id=cat_id).values('assigned_to_group')[0][
                            'assigned_to_group']
                    else:
                        assigned_to_user = request.userinfo['username'].id
                        assigned_to_group = '' #Group.objects.get(user = assigned_to_user).id
                else:
                    cat_id = ''
                    assigned_to_user = request.userinfo['username'].id
                    assigned_to_group = '' #Group.objects.get(user=assigned_to_user).id
                data_dict = {
                    'category_id': cat_id,
                    'status': masterconf.ticket_status_open,
                    'assign_to_user': assigned_to_user,
                    'assign_to_group': assigned_to_group,
                    'created_on': created_on,
                    'created_by': created_by,
                    'created_reference_id': created_reference_id,
                    'record_status': masterconf.active_status,
                    'remarks': json_error_codes
                }
                tckt_mstr = ticket_master(**data_dict)
                tckt_mstr.save()
                generated_ticket_id = str(tckt_mstr.ticket_id)
                print(" generated_ticket_id :: ", generated_ticket_id)
                ticket_subject = "Ticket id " + " " + generated_ticket_id + " : " + metadata_dict['subject'] + " for " + self.get_bank_date(request)
                update_ticket_mstr = ticket_master.objects.filter(ticket_id=generated_ticket_id).update(
                    subject=ticket_subject)
                table_of_error_codes = json2html.convert(json=json_error_codes)
                return (generated_ticket_id, ticket_subject, table_of_error_codes)
            else:
                return (0, 'No Subject', 0)
        except Exception as e:
            raise e

    def get_bank_date(self,request):
        request_post = request.POST
        first_param = ''
        second_param = ''
        third_param = ''
        fourth_param = ''
        fifth_param = ''
        if 'project_id' in request_post:
            first_param =  request_post['project_id']
        elif 'cra' in request_post:
            second_param = request_post['cra']
        if 'bank_code' in request_post:
            third_param = request_post['bank_code']
        if 'region' in request_post:
            fourth_param = request_post['region']
        if 'upload_datatime' in request_post:
            fifth_param = request_post['upload_datatime']
        if first_param != '' and second_param == '':
            return first_param + " / " +  third_param + " / "  + fourth_param + " / Date - " + fifth_param
        else:
            return second_param + " / " + third_param + " / " + fourth_param + " / Date - " + fifth_param

    def get_meta_info_routine(self,request):

        metadata_dict = {}
        category = masterconf.category_routine
        request_post = request.POST
        file_type = request_post['file_type']
        if file_type.upper() == masterconf.file_type_cbr or file_type.upper() == masterconf.file_type_dispense:
            bank_code = request_post['bank_code']
            raw_table_key = str(file_type).upper() + "_" + str(bank_code).upper() + "_TABLE"
            raw_table = db_queries_properties.table_dict[raw_table_key]
            if file_type.upper() == masterconf.file_type_cbr:
                sub_category = masterconf.file_type_cbr
                subject = masterconf.subject_data_cbr
            else:
                sub_category = masterconf.file_type_dispense
                subject = masterconf.subject_data_disp
        elif file_type.upper() == str(masterconf.file_type_vcb_eod).upper():
            raw_table_key = str(masterconf.file_type_vcb_eod).upper() + "__TABLE"
            raw_table = db_queries_properties.table_dict[raw_table_key]
            sub_category = masterconf.file_type_vcb_eod
            subject = masterconf.subject_data_vcb
        elif file_type.upper() == str(masterconf.file_type_C3R_VMIS).upper():
            raw_table_cmis_key = str(masterconf.file_type_C3R).upper() + "_CMIS_ALL_TABLE"
            raw_table = db_queries_properties.table_dict[raw_table_cmis_key]
            sub_category = masterconf.file_type_C3R
            subject = masterconf.subject_data_c3r
        elif file_type.upper() == str(masterconf.file_type_Daily_Loading_Report).upper():
            raw_table_key = str(masterconf.file_type_Daily_Loading_Report).upper() + "ALL_TABLE"
            raw_table = db_queries_properties.table_dict[raw_table_key]
            sub_category = masterconf.file_type_Daily_Loading_Report
            subject = masterconf.subject_data_daily
        else:
            raw_table_key = "IMS__TABLE"
            raw_table = db_queries_properties.table_dict[raw_table_key]
            sub_category = 'IMS'
            subject = masterconf.subject_data_IMS
        metadata_dict['category'] = category
        metadata_dict['sub_category'] = sub_category
        metadata_dict['raw_table'] = raw_table
        metadata_dict['subject'] = subject
        return metadata_dict



    def handle_mail_ticket_processing(self, request):

        # This method is used to handle all ticket and mail related processing

        try:
            print(" in  handle_mail_ticket_processing ")
            metadata_dict = self.get_meta_info_routine(request)
            is_cat_id_exists = ticket_category_master.objects.filter(category=metadata_dict['category'],
                                                                     sub_category=metadata_dict[
                                                                         'sub_category']).exists()
            print(" is_cat_id_exists ",is_cat_id_exists)
            if is_cat_id_exists:
                category_id = ticket_category_master.objects.filter(category=metadata_dict['category'],
                                                                    sub_category=metadata_dict['sub_category']).values(
                                                                    'category_id')
                cat_id = category_id[0]['category_id']
                print(" cat_id ",cat_id)
                is_user_for_cat_id = workflow_master.objects.filter(category_id=cat_id).exists()
                print(" is_user_for_cat_id :: ",is_user_for_cat_id)
                if is_user_for_cat_id:
                    assigned_to_user = workflow_master.objects.filter(category_id=cat_id).values('assigned_to_user')[0][
                                        'assigned_to_user']
                    assigned_to_group = workflow_master.objects.filter(category_id=cat_id).values('assigned_to_group')[0][
                                        'assigned_to_group']
                    user_mail = User.objects.filter(id=assigned_to_user).values('email')
                    if group != None:
                        group = Group.objects.get(id=assigned_to_group)
                        group_users = list(group.user_set.all())
                        print(" group_users :::::::: ",group_users)
                        cc = list()
                        for group_user in group_users:
                            cc.append(group_user.email)
                else:
                    logged_in_user = request.userinfo['username']
                    logged_in_user_mail = logged_in_user.email
                    print(" logged_in_user_mail ::: ",logged_in_user_mail)
                    logged_in_user_group = list(logged_in_user.groups.all())[0]
                    group_users = User.objects.filter(groups__name=logged_in_user_group)
                    print(" group_users ::: ",group_users)
                    cc = list()
                    for group_user in group_users:
                        cc.append(group_user.email)
            else :
                logged_in_user = request.userinfo['username']
                logged_in_user_mail = logged_in_user.email
                print(" logged_in_user_mail ::: " , logged_in_user_mail)
                logged_in_user_group = list ( logged_in_user.groups.all ( ) )[0]
                group_users = User.objects.filter ( groups__name=logged_in_user_group )
                print(" group_users ::: " , group_users)
                cc = list ( )
                for group_user in group_users :
                    cc.append ( group_user.email )
            tickettuple = self.notify_and_raise_ticket_routine(request)
            ticket_id = tickettuple[0]
            print(" ticket_id :: ", ticket_id)
            if ticket_id != 0:
                subject = tickettuple[1]
                json_error_codes = tickettuple[2]
                recipient = cc
                self.notify_mail_operation_status(recipient, subject, json_error_codes)
            return ticket_id
        except Exception as e:
            raise e

    def handle_mail_master_upload_notification( self,  request_data, uploaded_by, reference_id):
        try:
            id_ticket_category_master = list (
                ticket_category_master.objects.filter ( category=masterconf.master_category , sub_category=masterconf.master_category_dict[request_data['file_type'].upper()][0] ,
                                                        record_status='Active' ).values ( 'category_id' ) )
            category_id = id_ticket_category_master[0]['category_id']
            print ("id_ticket_category_master data: " , id_ticket_category_master)
            print ("category_id : " , category_id)
            wrkflow_assigned_group_id = \
            list ( workflow_master.objects.filter ( category_id=category_id ).values ( 'assigned_to_group' ) )[
                0]['assigned_to_group']
            print ("wrkflow_assigned_group_id : " , wrkflow_assigned_group_id)
            group = Group.objects.get ( id=wrkflow_assigned_group_id )
            group_users = list ( group.user_set.all ( ) )
            user_email_list = list ( )
            for single_user in group_users :
                user_email = list ( User.objects.filter ( is_active=1 , username=single_user ).values ( 'email' ) )[0][
                    'email']
                user_email_list.append ( user_email )
            print ("user_email_list : " , user_email_list)
            print(" group_users :::::::: " , group_users)
            subject = masterconf.master_category_dict[request_data['file_type'].upper()][1]
            uploaded_on = str(list(data_update_log_master.objects.filter(created_by = uploaded_by, record_status = masterconf.pending_status, data_for_type = request_data['file_type'].upper(), created_reference_id = reference_id).values('date_created'))[0]['date_created'])
            self.notify_mail_operation_master_upload ( user_email_list , subject, uploaded_by, uploaded_on )
        except Exception as e:
            print ("Exception from handle_mail_master_upload_notification : ",e)
            raise e



    def notify_mail_operation_master_upload(self, recipient, subject, uploaded_by, uploaded_on):

        # This method is used to send mail

        common_util_obj = common_util.common_utils()
        try:
            message = MIMEMultipart('alternative')
            message['Subject'] = masterconf.master_data_upload_subject+" "+subject+ " "+masterconf.master_data_file_upload_time+uploaded_on
            body1 = masterconf.master_data_successful_upload_message
            uploaded_by_body = "Uploaded By : "+uploaded_by
            uploaded_on_body = "Uploaded on : "+uploaded_on
            body2 = masterconf.master_data_upload_message_body
            body = body1+"\n\n"+uploaded_by_body+"\n"+uploaded_on_body+"\n\n"+body2
            message.attach (MIMEText(body))
            server = smtplib.SMTP()
            server.connect(masterconf.MAILHOST, masterconf.MAILPORT)
            server.starttls()
            server.login(masterconf.mail_sender, masterconf.mail_password)
            server.sendmail(masterconf.mail_sender, recipient, message.as_string())
            server.close()
        except Exception as e:
            print ("Exception from notify_mail_operation_master_upload : " , e)
            raise e