from Common import masterconf
import time, datetime
import datetime
import logging
import time
from Common import masterconf
from Common.CommonFunctions import common_util
from Model.models import ticket_category_master, ticket_master, ticket_reply, workflow_master
from django.contrib.auth.models import User, Group


class TicketController():

	def handle_create_ticket_request(self,request):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("INSIDE handle_create_ticket_request METHOD OF TicketController.")
		request_obj = request.data
		created_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		created_by = request.userinfo['username'].username
		created_reference_id = request.userinfo['reference_id']
		category = request_obj['category']
		sub_category = request_obj['sub_category']
		try:
			is_cat_id_exists = ticket_category_master.objects.filter(category=category,
																	 sub_category=sub_category).exists()
			if is_cat_id_exists:
				category_id = ticket_category_master.objects.filter(category=category,
																	sub_category=sub_category).values('category_id')
				print(" category_id ::: ", category_id)
				cat_id = category_id[0]['category_id']
				is_user_for_cat_id = workflow_master.objects.filter(category_id=cat_id).exists()
				if is_user_for_cat_id:
					assigned_to_user = workflow_master.objects.filter(category_id=cat_id).values('assigned_to_user')[0][
						'assigned_to_user']
					assigned_to_group = workflow_master.objects.filter(category_id=cat_id).values('assigned_to_group')[0][
						'assigned_to_group']
				else:
					assigned_to_user = request.userinfo['username'].id
					assigned_to_group = ''  # Group.objects.get(user = assigned_to_user).id
			else:
				cat_id = ''
				assigned_to_user = request.userinfo['username'].id
				assigned_to_group = ''  # Group.objects.get(user=assigned_to_user).id
			data_dict = {
				'subject' : request_obj['subject'],
				'category_id' : cat_id,
				'status' : masterconf.ticket_status_open,  #Status Updated
				'assign_to_user' : assigned_to_user,
				'assign_to_group' : assigned_to_group,
				'created_on' : created_on,
				'created_by' : created_by,
				'created_reference_id' : created_reference_id,
				'remarks' : request_obj['remarks'],
				'record_status' : masterconf.active_status #Record Status Added
			}
			tckt_mstr = ticket_master(**data_dict)
			tckt_mstr.save()
			return tckt_mstr.ticket_id
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('EXCEPTION IN handle_create_ticket_request METHOD OF TicketController', exc_info=True)
			raise e

	def handle_create_reply_request(self,request):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("INSIDE handle_create_reply_request METHOD OF TicketController.")
		request_obj = request.data
		created_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		created_by = request.userinfo['username'].username
		created_reference_id = request.userinfo['reference_id']
		ticket_id = request_obj['ticket_id']
		replymessage = request_obj['replymessage']
		datetimereply = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		data_dict = {
			'ticket_id': ticket_id,
			'replymessage': replymessage,
			'datetimereply' : datetimereply,
			'created_on': created_on,
			'created_by': created_by,
			'created_reference_id': created_reference_id,
			'record_status' : masterconf.active_status #Record Status Updated
		}
		try:
			tckt_reply = ticket_reply(**data_dict)
			tckt_reply.save()
			return tckt_reply.reply_id
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('EXCEPTION IN handle_create_reply_request METHOD OF TicketController', exc_info=True)
			raise e

	def handle_ticket_list_req(self, request):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("INSIDE handle_ticket_list_req METHOD OF TicketController.")
		request_obj = request.data
		list_returned = list()
		inner_obj = {}
		try:
			if 'filters' in request_obj:
				filters_param = request_obj['filters']
				list_db = ticket_master.objects.filter(**filters_param).values()
			else:
				list_db = ticket_master.objects.all().values()
			if 'required_fields' in request_obj.keys():
				required_fields_arr = request_obj['required_fields']
				for i in range(0, len(list_db)):
					inner_obj = {key: list_db[i][key] for key in required_fields_arr}
					list_returned.append(inner_obj)
				return list_returned
			else:
				return list_db

		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('EXCEPTION IN handle_ticket_list_req METHOD OF TicketController', exc_info=True)
			raise e

	def handle_ticket_detail_req(self,request):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("INSIDE handle_ticket_detail_req METHOD OF TicketController.")
		ticketid = request.data['ticket_id']
		try:
			if 'filter' in request.data.keys():
				filters_params = {}
				filters_params = request.data['filter']
				filters_params['ticket_id'] = ticketid
				tcktmastr = list(ticket_master.objects.filter(**filters_params).values())
			else:
				tcktmastr = list(ticket_master.objects.filter(ticket_id=ticketid).values())
			tcktreply = list(ticket_reply.objects.filter(ticket_id=ticketid).values())
			tcktmastr[0]['ticket_reply_data'] = tcktreply
			if 'required_fields' in request.data.keys():
				rqrd_field_resp = {key: tcktmastr[0][key] for key in request.data['required_fields']}
				rqrd_field_resp['ticket_reply_data'] = tcktreply
				return rqrd_field_resp
			return tcktmastr[0]
		except Exception as e:
			#print(e)
			logger.setLevel(logging.ERROR)
			logger.error('EXCEPTION IN handle_ticket_detail_req METHOD OF TicketController', exc_info=True)
			raise e


	def handle_category_menus_req(self,request):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("INSIDE handle_category_menus_req METHOD OF TicketController.")
		try:
			queryset_list = ticket_category_master.objects.all().values('category_id','category','sub_category','project_id','record_status')
			if len(queryset_list):
				return queryset_list
		except Exception as e:
			logger.setLevel(logging.ERROR)
			logger.error('EXCEPTION IN handle_category_menus_req METHOD OF TicketController', exc_info=True)
			raise e

	def handle_update_ticket_request(self,request):

		#This method is used to update the ticket status in ticket_master
		
		common_util_obj = common_util.common_utils()
		request_obj = request.data
		created_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		created_by = request.userinfo['username'].username
		created_reference_id = request.userinfo['reference_id']
		category = request_obj['category']
		subcategory = request_obj['sub_category']
		status_dict = {}
		try:
			tckt_id = request_obj["ticket_id"]
			previous_ticket_record = ticket_master.objects.filter(ticket_id=tckt_id)
			category_id = ticket_category_master.objects.filter(category=category, sub_category=subcategory).values('category_id')
			cat_id = category_id[0]['category_id']
			assigned_to_user = str(request_obj["assign_to_user"]).lower()
			user_id = User.objects.get(username = assigned_to_user)
			group_id = Group.objects.get(user=user_id.id)
			previous_ticket_record.update(record_status=masterconf.history_status)
			data_dict = {
				'subject' : previous_ticket_record[0].subject,
				'category_id' : cat_id,
				'status' : request_obj['status'],
				'assign_to_user' : user_id.id,
				'assign_to_group' : group_id.id,
				'created_on' : previous_ticket_record[0].created_on,
				'created_by' : previous_ticket_record[0].created_by,
				'created_reference_id' : previous_ticket_record[0].created_reference_id,
				'record_status' : masterconf.active_status,
				'modified_on' : created_on,
				'modified_by' : created_by,
				'modified_reference_id' : created_reference_id,
				'remarks' : previous_ticket_record[0].remarks
			}
			tckt_mstr = ticket_master(**data_dict)
			tckt_mstr.save()
			status_dict['status_code'] = masterconf.success_updated
			status_dict['ticket_id'] = tckt_mstr.ticket_id
			return status_dict
		except Exception as e:
			print(e)
			raise e
