from django.http import HttpResponse
import os
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common import db_queries_properties
from Common.models import data_update_log_reference
from django.apps import apps
from datetime import timedelta
from PIL import Image
import base64
from Model.models import Auth_Signatories_Signatures
from django.db import connection
from datetime import timedelta
from Common.models import data_update_log_master

class Signature():
	maintain_log_id = 0
	def handle_signature_upload(self,request,signature_filepath):
		common_util_obj = common_util.common_utils()
		signatory_name = request.POST['signatory_name']
		designation = request.POST['designation']
		bank_code = request.POST['bank_code']
		apiflag = 'F'
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		username = request.userinfo['username'].username
		status_dict = {}
		try:
			with open(signature_filepath, 'rb') as f:
				photo = f.read()
			encodestring = base64.b64encode(photo)
			metadata = {}
			metadata['project_id'] = request.POST["project_id"]
			metadata['signatory_name'] = signatory_name
			metadata['designation'] = designation
			metadata['physical_signature'] = encodestring
			metadata['bank_code'] = bank_code
			metadata['record_status'] = masterconf.uploaded_state
			metadata['created_on'] = upload_time
			metadata['created_by'] = username
			metadata['created_reference_id'] = reference_id
			# print(metadata)
			# record_status = Auth_Signatories_Signatures.objects.filter(signatory_name = metadata['signatory_name'], designation = metadata['designation'], project_id = metadata['project_id'], bank_code = metadata['bank_code'], record_status = metadata['record_status']).exists()
			# if not record_status:
			auth_Signatories_Signatures = Auth_Signatories_Signatures(**metadata)
			auth_Signatories_Signatures.save()
			# self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time, username,reference_id)
			metadata_data_log_entry = {}
			metadata_data_log_entry['project_id'] = request.POST["project_id"]
			metadata_data_log_entry['data_for_type'] = request.POST["file_type"].upper()
			metadata_data_log_entry['record_status'] = masterconf.uploaded_state
			metadata_data_log_entry['date_created'] = upload_time
			metadata_data_log_entry['created_by'] =  username
			metadata_data_log_entry['created_reference_id'] = reference_id
			metadata_data_log_entry['total_count'] = 1
			metadata_data_log_entry['pending_count'] = 1
			data_update_log_master_update = data_update_log_master(**metadata_data_log_entry)
			data_update_log_master_update.save()
			data_code = common_util_obj.validate_reference_data(request.POST['file_type'].upper(), apiflag, username, reference_id)
			# else:
			# 	id = Auth_Signatories_Signatures.objects.filter(signatory_name = metadata['signatory_name'], designation = metadata['designation'], project_id = metadata['project_id'], bank_code = metadata['bank_code'], record_status = metadata['record_status']).values('id')[0]['id']
			# 	t = Auth_Signatories_Signatures.objects.get(id = id)
			# 	t.record_status = 'Inactive'
			# 	t.save()
			# 	auth_Signatories_Signatures = Auth_Signatories_Signatures(**metadata)
			# 	auth_Signatories_Signatures.save()
			# record_present = Auth_Signatories_Signatures.objects.filter(signatory_name = metadata['signatory_name'], designation = metadata['designation'], project_id = metadata['project_id'], bank_code = metadata['bank_code'], record_status = metadata['record_status']).exists()
			print ("data_code returned from SP to check if signature upload validation is successfull or not : ",data_code)
			if data_code == masterconf.data_val_success_mstr_upload:
				status_dict["status_text"] = masterconf.signature_available
				status_dict["status_code"] = data_code
				return status_dict
			else:
				status_dict["status_text"] = masterconf.signature_not_available
				status_dict["status_code"] = data_code
				return status_dict
		except Exception as e:
			print ("Exception thrown from Signature.py file",e)
			raise e
