from django.http import HttpResponse
import os
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Model.models import Auth_Signatories_Signatures
from PIL import Image
import base64
from django.utils.encoding import smart_str

class SignatureController():
	def handle_signature_download(self,request):
		common_util_obj = common_util.common_utils()
		status_dict = {}
		signatory_name = request.data['signatory_name']
		designation = request.data['designation']
		record_status = 'Active'
		try:
			is_record_exists = Auth_Signatories_Signatures.objects.filter(signatory_name = signatory_name, designation = designation,record_status = record_status).exists()
			if is_record_exists:
				physical_signature = Auth_Signatories_Signatures.objects.filter(signatory_name = signatory_name, designation = designation,record_status = record_status).values('physical_signature')[0]['physical_signature']
				data1=base64.b64decode(physical_signature)
				print(data1)
				file_like=io.BytesIO(data1)
				img=Image.open(file_like)
				m = masterconf.signature_output_folder+signatory_name+"_"+designation+"_extracted.png"
				img.save(m)
				file_name = signatory_name+"_"+designation+"_extracted.png"
				path_to_file = masterconf.signature_output_folder
				# image = b"data:img/png;base64, "+physical_signature
				# #print (image)
				# if physical_signature is not None:
				# 	#print ("m inisde")
				# 	status_dict["data"] = image
				# 	status_dict["status"] = masterconf.signature_fetched_successfully
				# 	status_dict["status_desc"] = masterconf.signature_success_status_code
				# 	#print ("status_dict : ",status_dict)
				# 	return status_dict
				# data1=base64.b64decode(data[0][0])
				# file_like=io.BytesIO(data1)
				# img=Image.open(file_like)
				if os.path.exists(path_to_file+file_name):
					with open(path_to_file+file_name, 'rb') as fh:
						#response = HttpResponse(fh, content_type="application/force-download")
						response = HttpResponse(fh, content_type='image/png')
						response['Content-Disposition'] = "'attachment; filename=%s' % "+file_name
						response["status_text"] = masterconf.signature_available
						return response
			else:
				status_dict["status_text"] = masterconf.signature_not_available
				status_dict["status_code"] = masterconf.signature_failure_status_code
				return status_dict
		except Exception as e:
			print("Exception in SignatureController ",e)
			raise e
