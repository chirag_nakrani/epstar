from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import pyexcel
import xlrd
import time, datetime
# from Controller.Routine import BaseCBRController, BaseCDRController, BaseVCBControllerEOD, BaseC3RMISController, DailyLoad_RepCntrlr
from Controller.Master import ATMMasterController
from Controller.ViewData import BankInfoController
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Model.models import menu_access, menu_master
from django.contrib.auth.models import User, Group, Permission
import json
from json import JSONEncoder
from django.db import connection
import pyodbc
from Model.models import user_bank_mapping, user_project_mapping, menu_group_permission_mapping, menu_master, \
    user_bank_project_mapping

from Common import db_queries_properties


class UserMgntController():

    def get_user_roles(self, request):
        try:
            data_list = list()
            sql_dlr_query = "select a.username,c.name as group_name from auth_user a join auth_user_groups b on a.id=b.user_id join auth_group c on c.id=b.group_id"
            cursor = connection.cursor()
            cursor.execute(sql_dlr_query)
            user_data = cursor.fetchall()
            # print ("user data from db::: ",user_data)
            count = 0
            for record in user_data:
                temp_obj = {}
                temp_obj['username'] = record[0]
                temp_obj['usergroup'] = record[1]
                # print("dict1 ::: ",temp_obj)
                data_list.append(temp_obj)
            # print (data_list)
            status_dict = {}
            status_dict['data'] = data_list
            ##print("status_dict :: ",status_dict)
            return status_dict
        except Exception as e:
            raise e


class userBankMapController():

    def UserBankProj_create_delete_request(self, request):

        # This method is used to create the user, bank, project mapping

        username = request.userinfo['username'].username
        changedate = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        reference_id = request.userinfo['reference_id']
        status_dict = {}
        try:
            if request.data['operation_type'] == masterconf.operation_type_add:
                payload_obj_list = request.data['payload']
                for payload_obj in payload_obj_list:
                    user_id = payload_obj['user_id']
                    bank_code_list = payload_obj['bank_code']
                    project_id = payload_obj['project_id']
                    for bank_code in bank_code_list:
                        if_already_exists = user_bank_project_mapping.objects.filter(user_id=user_id,
                                                                                     bank_code=bank_code,
                                                                                     project_id=project_id,
                                                                                     record_status=masterconf.active_status).exists()
                        if not if_already_exists:
                            insert_obj = {
                                "user_id": user_id,
                                "bank_code": bank_code,
                                "project_id": project_id,
                                "created_on": changedate,
                                "created_by": username,
                                "created_reference_id": reference_id,
                                "record_status": masterconf.active_status
                            }
                            user_bank_proj = user_bank_project_mapping(**insert_obj)
                            user_bank_proj.save()
            else:
                payload_obj = request.data['payload']
                user_id = payload_obj['user_id']
                bank_code = payload_obj['bank_code']
                project_id = payload_obj['project_id']
                entry_to_delete = user_bank_project_mapping.objects.filter(user_id=user_id,
                                                                           bank_code=bank_code,
                                                                           project_id=project_id)
                entry_to_delete.update(deleted_by=username, deleted_on=changedate,
                                       deleted_reference_id=reference_id,
                                       record_status=masterconf.status_deleted)
            status_dict['status_code'] = masterconf.success_updated
            print(status_dict)
            return status_dict
        except Exception as e:
            print(e)
            raise e

    def handle_bankMap_delete_request(self, request):

        # This method is used to delete the existing record(marking its status as deleted)

        deleted_by = request.userinfo['username'].username
        deleted_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        deleted_reference_id = request.userinfo['reference_id']
        payload_obj_list = request.data['payload']
        status_dict = {}
        try:
            for payload_obj in payload_obj_list:
                user_id = payload_obj['user_id']
                bank_code_list = payload_obj['bank_code']
                project_id_list = payload_obj['project_id']
                username = User.objects.get(id=user_id).username
                for bank_code in bank_code_list:
                    print("bank_code : ", bank_code)
                    if_bank_already_exists = user_bank_mapping.objects.filter(user_name=username,
                                                                              bank_id=bank_code,
                                                                              record_status=masterconf.status_deleted).exists()
                    print("if_bank_already_exists :: ", if_bank_already_exists)
                    if not if_bank_already_exists:
                        bank_update_id = user_bank_mapping.objects.filter(user_name=username,
                                                                          bank_id=bank_code).update(
                            deleted_on=deleted_on,
                            deleted_by=deleted_by,
                            deleted_reference_id=deleted_reference_id,
                            record_status=masterconf.status_deleted)

                for project_id in project_id_list:
                    print(" project_id ", project_id)
                    if_proj_already_exists = user_project_mapping.objects.filter(user_name=username,
                                                                                 project_id=project_id,
                                                                                 record_status=masterconf.status_deleted).exists()
                    print("if_proj_already_exists :: ", if_proj_already_exists)
                    if not if_proj_already_exists:
                        project_update_id = user_project_mapping.objects.filter(user_name=username,
                                                                                project_id=project_id).update(
                            deleted_on=deleted_on,
                            deleted_by=deleted_by,
                            deleted_reference_id=deleted_reference_id,
                            record_status=masterconf.status_deleted)
            status_dict['status_code'] = masterconf.success_updated
            print(status_dict)
            return status_dict
        except Exception as e:
            # print(e)
            raise e

    def handle_bankMap_read_request(self, request):

        # This method is used to read user bank project information

        common_util_obj = common_util.common_utils()
        request_obj = request.data['payload']
        list_returned = list()
        inner_obj = {}
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                filters_param['record_status'] = masterconf.active_status
                list_db = user_bank_project_mapping.objects.filter(**filters_param).values()
            else:
                list_db = user_bank_project_mapping.objects.filter(record_status=masterconf.active_status).values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            print(e)
            raise e


class userMenuPermController:

    def menu_group_perm_add_delete_req(self, request):

        # This method is used to create and delete group, menu and permission mapping

        username = request.userinfo['username'].username
        changedate = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        reference_id = request.userinfo['reference_id']
        payload_obj_list = request.data['payload']
        status_dict = {}
        try:
            if request.data['operation_type'] == masterconf.operation_type_add:
                for payload_obj in payload_obj_list:
                    group_id = payload_obj["group_id"]
                    menu_list = payload_obj["menu_list"]
                    for menus in menu_list:
                        main_menu_id = menus["menu"]
                        submenu_permission_list = menus["submenu_permission_list"]
                        for submenu_permission in submenu_permission_list:
                            submenu = submenu_permission["submenu"]
                            permission_list = submenu_permission["permission_list"]
                            for permission in permission_list:
                                if_already_exists = menu_group_permission_mapping.objects.filter(group_id=group_id,
                                                                                                 menu_id=main_menu_id,
                                                                                                 sub_menu_id=submenu,
                                                                                                 permission_id=permission,
                                                                                                 record_status=masterconf.active_status).exists()
                                if not if_already_exists:
                                    insert_obj = {
                                        "group_id": group_id,
                                        "menu_id": main_menu_id,
                                        "sub_menu_id": submenu,
                                        "permission_id": permission,
                                        "created_by": username,
                                        "created_on": changedate,
                                        "created_reference_id": reference_id,
                                        "record_status": masterconf.active_status
                                    }
                                    menu_grp_perm_map = menu_group_permission_mapping(**insert_obj)
                                    menu_grp_perm_map.save()
                status_dict['status_code'] = masterconf.data_updated
            else:
                payload_obj = request.data['payload']
                id = payload_obj['id']
                group_id = payload_obj['group_id']
                menu_id = payload_obj['menu_id']
                sub_menu_id = payload_obj['sub_menu_id']
                permission_id = payload_obj['permission_id']
                entry_to_delete = menu_group_permission_mapping.objects.filter(id=id,
                                                                               group_id=group_id,
                                                                               menu_id=menu_id,
                                                                               sub_menu_id=sub_menu_id,
                                                                               permission_id=permission_id)
                entry_to_delete.update(deleted_by=username, deleted_on=changedate,
                                       deleted_reference_id=reference_id,
                                       record_status=masterconf.status_deleted)
                status_dict['status_code'] = masterconf.menu_perm_delete_success_code
            return status_dict
        except Exception as e:
            print(e)
            raise e

    def menu_group_perm_read_req(self, request):

        # This API is used to read group, menu and permission API

        username = request.userinfo['username'].username
        upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        reference_id = request.userinfo['reference_id']
        payload_obj = request.data['payload']
        returned_list = []
        try:
            if 'filters' in payload_obj.keys():
                filters = payload_obj["filters"]
                filters['record_status'] = masterconf.active_status
                query_filter = []
                sql_str = db_queries_properties.menu_permission_query_filter % (masterconf.active_status)
                print('sql_str',sql_str)
                if 'group_id' in filters:
                    query_filter.append(" AND mg.group_id='%s'" % filters['group_id'])
                if 'menu_id' in filters:
                    query_filter.append(" AND mg.menu_id='%s'" % filters['menu_id'])
                if 'permission_id' in filters:
                    query_filter.append(" AND mg.permission_id='%s'" % filters['permission_id'])
                if 'sub_menu_id' in filters:
                    query_filter.append(" AND mg.sub_menu_id='%s'" % filters['sub_menu_id'])
                print('filters',query_filter)
                sql_str += "".join(query_filter)
                print('sql_str', sql_str)
                cursor = connection.cursor()
                cursor.execute(sql_str)
                columns = [column[0] for column in cursor.description]
                for row in cursor.fetchall():
                    returned_list.append(dict(zip(columns, row)))
                    # print('returned_list',returned_list)
                # returned_list.append(cursor.fetchall())
                # print('returned_list', returned_list)
                # returned_list = list(menu_group_permission_mapping.objects.filter(**filters).values())
            else:
                sql_str = db_queries_properties.menu_permission_query % (masterconf.active_status)
                print('sql_str',sql_str)
                cursor = connection.cursor()
                cursor.execute(sql_str)
                columns = [column[0] for column in cursor.description]
                for row in cursor.fetchall():
                    returned_list.append(dict(zip(columns, row)))
                # returned_list.append(cursor.fetchall())
                # print('returned_list',returned_list)
                # returned_list = list(
                #     menu_group_permission_mapping.objects.filter(record_status=masterconf.active_status).values())
            print(len(returned_list))
            return returned_list
        except Exception as e:
            # print(e)
            raise e

    def get_menu_list(self, request):
        try:
            request_obj = request.data
            print(request_obj)
            parent_menu_id = request_obj['parent_menu_id']
            print(" parent_menu_id :: ", parent_menu_id)
            if parent_menu_id != '':
                submenu_list = list(
                    menu_master.objects.filter(parent_menu_id=parent_menu_id).values('menu_id', 'display_name'))
            else:
                submenu_list = list(
                    menu_master.objects.filter(parent_menu_id=None).values('menu_id', 'display_name'))
            print(submenu_list)
            return submenu_list
        except Exception as e:
            print(e)
            raise e
