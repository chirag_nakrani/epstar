from rest_framework.response import Response
from rest_framework import status
from Common.CommonFunctions import common_util
from django.contrib.auth.models import User, Group, Permission
from Common import masterconf
from Model.models import auth_user_group_metadata
import time, datetime
import pyodbc


class GetUsersGroupPermList():

    # This Controller is used to get users info

    def get_users_list(self, request):

        # Method to get user's Id and username from User Model

        try:
            users_list = list(User.objects.all())
            returned_users_list = list()
            for user in users_list:
                temp_obj = {}
                temp_obj["id"] = user.id
                temp_obj["user_name"] = user.username
                returned_users_list.append(temp_obj)
            print(returned_users_list)
            return returned_users_list
        except Exception as e:
            print(e)
            raise e

    def get_group_list(self, request):

        # Method to get group's ID and name from Group Model

        try:
            group_list = list(Group.objects.all())
            returned_groups_list = list()
            for group in group_list:
                temp_obj = {}
                temp_obj["id"] = group.id
                temp_obj["group_name"] = group.name
                returned_groups_list.append(temp_obj)
            print(returned_groups_list)
            return returned_groups_list
        except Exception as e:
            print(e)
            raise e

    def get_permission_list(self, request):

        # Method to get Permission Information from Permission Model

        try:
            perm_list = list(Permission.objects.all())
            returned_perm_list = list()
            for perm in perm_list:
                temp_obj = {}
                temp_obj["id"] = perm.id
                temp_obj["perm_name"] = perm.name
                temp_obj["perm_codename"] = perm.codename
                returned_perm_list.append(temp_obj)
            print(returned_perm_list)
            return returned_perm_list
        except Exception as e:
            print(e)
            raise e

    def get_auth_user_group_id(self, user_id, group_id):

        #This Method is used to return id from auth_user_groups Model

        conn = pyodbc.connect(masterconf.connection_string)
        crsr = conn.cursor()
        try:
            sql_str = "select id from auth_user_groups where user_id = " + str(user_id) + " and group_id = " + str(group_id)
            print(sql_str)
            crsr.execute(sql_str)
            json_error_codes = crsr.fetchone()
            return json_error_codes[0]
        except Exception as e:
            raise e

    def create_delete_user_group_mapping(self, request):

        # This method is used to create new user group mapping

        try:
            request_obj = request.data
            request_user = request.userinfo['username'].username
            date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
            reference_id = request.userinfo['reference_id']
            payload_obj_list = request_obj["payload"]
            operation_type = request_obj["operation_type"]
            status_dict = {}
            print(" In create_user_group_mapping")
            for payload_obj in payload_obj_list:
                user_id = payload_obj["user_id"]
                group_ids = payload_obj["group_ids"]
                user = User.objects.get(id=user_id)
                for group_id in group_ids:
                    group = Group.objects.get(id=group_id)
                    if operation_type == masterconf.operation_type_add:
                        user.groups.add(group)
                        mapping_id = self.get_auth_user_group_id(user.id, group.id)
                        if mapping_id:
                            add_opr_dict = {
                                "id": mapping_id,
                                "created_on": date,
                                "created_by": request_user,
                                "created_reference_id": reference_id,
                                "record_status": masterconf.active_status
                            }
                            add_mapping = auth_user_group_metadata(**add_opr_dict)
                            add_mapping.save()
                    else:
                        mapping_id = self.get_auth_user_group_id(user.id, group.id)
                        if mapping_id:
                            user_group = auth_user_group_metadata.objects.filter(id=mapping_id)
                            user_group.update(deleted_by=request_user, deleted_on=date,
                                              deleted_reference_id=reference_id,
                                              record_status=masterconf.status_deleted)
            status_dict["status_code"] = masterconf.success_updated
            return status_dict
        except Exception as e:
            print(e)
            raise e

    def get_user_group_mapping(self, request):

        # This method is used to create new user group mapping

        try:
            request_obj = request.data
            request_user = request.userinfo['username'].username
            date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
            reference_id = request.userinfo['reference_id']
            payload_obj = request_obj["payload"]
            users_id = payload_obj["users_id"]
            if len(users_id) == 0:
                users_all = list(User.objects.all())
                for user in users_all:
                    users_id.append(user.id)
                #print(users_id)
            returned_list = []
            for user_id in users_id:
                group_ids = Group.objects.filter(user=user_id).values('id')
                for group_id_set in group_ids:
                    temp_obj = {}
                    group_id = group_id_set['id']
                    mapping_id = self.get_auth_user_group_id(user_id,group_id)
                    temp_obj['user_id'] = user_id
                    temp_obj['group_id'] = group_id
                    is_meta_exists = auth_user_group_metadata.objects.filter(id=mapping_id).exists()
                    if is_meta_exists:
                        temp_obj_new = {}
                        meta = dict(auth_user_group_metadata.objects.filter(id=mapping_id,record_status=masterconf.active_status).values()[0])
                        print(meta)
                        temp_obj_new = {**temp_obj,**meta}
                        returned_list.append(temp_obj_new)
                    else:
                        returned_list.append(temp_obj)
            return returned_list
        except Exception as e:
            print(e)
            raise e
