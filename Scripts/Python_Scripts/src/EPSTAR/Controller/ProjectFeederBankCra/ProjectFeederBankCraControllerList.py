import time, datetime
from Common.CommonFunctions import common_util
from Model.models import project_bank_feeder_cra_mapping
import logging
from Common import masterconf,db_queries_properties
from django.apps import apps
from django.db import connection

class ProjectFeederBankCraControllerList:

    def handle_list_req(self,request):

        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_list_req METHOD OF ProjectFeederBankCraControllerList.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        response_data = []
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = project_bank_feeder_cra_mapping.objects.filter(**filters_param).values()
            else:
                list_db = project_bank_feeder_cra_mapping.objects.all().values()

            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                # for data in list_db:
                #     response_data.append(data)
                return list_db
        except Exception as e:
            print(e)
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_list_req METHOD OF ProjectFeederBankCraControllerList', exc_info=True)
            raise e
