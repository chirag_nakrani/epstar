from django.contrib.auth.models import User
from Model.models import project_bank_feeder_cra_mapping, user_bank_project_mapping
import logging
from Common import masterconf,db_queries_properties
import pyodbc

class ProjectFeederBankCraController:
    cnxn = pyodbc.connect(masterconf.connection_string)
    cursor = cnxn.cursor()
    
    def handle_upload_req(self,request):

        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_upload_req METHOD OF ProjectFeederBankCraController.")
        response_obj = {}
        try:
            sql_query = db_queries_properties.project_bank_feeder_cra
            logger.info('Sql query for project bank feeder cra refresh: %s',sql_query)
            result = self.cursor.execute(sql_query)
            self.cnxn.commit()
            if result:
                response_obj['status'] = masterconf.api_status_Success
                response_obj['status_text'] = masterconf.data_uploaded_successful
            else:
                response_obj['status'] = masterconf.api_status_Failure
                response_obj['status_text'] = masterconf.failure_status_desc_resp
            return response_obj
        except Exception as e:
            print(e)
            logger.error('EXCEPTION IN handle_upload_req METHOD OF ProjectFeederBankCraController', exc_info=True)
            raise e

    def handle_list_req(self,request):

        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_list_req METHOD OF ProjectFeederBankCraController.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        response_data = []
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = project_bank_feeder_cra_mapping.objects.filter(**filters_param).values()
            else:
                list_db = project_bank_feeder_cra_mapping.objects.all().values()

            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                # for data in list_db:
                #     response_data.append(data)
                return list_db
        except Exception as e:
            logger.error('EXCEPTION IN handle_list_req METHOD OF ProjectFeederBankCraController', exc_info=True)
            logger.error(e)
            raise e

