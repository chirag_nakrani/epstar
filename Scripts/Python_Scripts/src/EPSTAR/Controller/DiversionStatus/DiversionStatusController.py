import time, datetime
from Common.CommonFunctions import common_util
from Model.models import diversion_status, indent_detail, indent_master, project_bank_feeder_cra_mapping
from Common.models import data_update_log_master
import logging
from Common import masterconf, db_queries_properties
from datetime import timedelta
import pyodbc


class DiversionStatusController:

    ### creating diversion status ####
    def handle_diversion_status_upload_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_diversion_status_upload_req METHOD OF DiversionStatusController.")
        request_data = request.data
        created_by = request.userinfo['username'].username
        created_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        created_reference_id = request.userinfo['reference_id']
        status_dict = {}
        status_dict["data"] = []
        try:
            ##### creating diversion status ###
            for diversion_data in request_data:
                data = common_util_obj.validate_diversion_status_InsertReq(diversion_data)
                data['created_by'] = created_by
                data['created_on'] = created_on
                data['created_reference_id'] = created_reference_id
                data['record_status'] = masterconf.pending_status
                maintain_log_id = 0
                try:
                    diversion_status_data = diversion_status(**data)
                    diversion_status_data.save()
                    if diversion_status_data :
                        status_dict['status_code'] = masterconf.success_code
                        status_dict['status_text'] = masterconf.upload_success_text
                        status_dict["data"].append(data)
                    else:
                        status_dict['status_code'] = masterconf.error_code
                        status_dict['status_text'] = masterconf.error_text
                        status_dict["data"].append(data)
                except Exception as e:
                    logger.error('Exception occured in handle_diversion_status_upload_req of DiversionStatusController: %s',e)

            logger.info('Response from handle_diversion_status_upload_req of DiversionStatusController: %s',status_dict)
            return status_dict

        except Exception as e:
            logger.error('EXCEPTION IN handle_diversion_status_upload_req METHOD OF DiversionStatusController: ',
                         e)
            raise e

    def handle_diversion_status_list_req(self, request):

        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_diversion_status_list_req METHOD OF DiversionStatusController.")
        request_obj = request.data
        list_returned = list()
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = diversion_status.objects.filter(**filters_param).values()
            else:
                list_db = diversion_status.objects.all().values()

            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            print(e)
            status_dict = {}
            status_dict['status_code'] = masterconf.error_code
            status_dict['status_text'] = masterconf.list_error_text
            logger.error('EXCEPTION IN handle_diversion_status_list_req METHOD OF DiversionStatusController: %s', e)
            logger.info('Response from handle_diversion_status_list_req of DiversionStatusController: %s',
                        status_dict)
            raise e

    ### approve diversion status ###
    def handle_diversion_status_approve_req(self, request):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_diversion_status_approve_req METHOD OF DiversionStatusController.")
        approved_by = request.userinfo['username'].username
        approved_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        approved_reference_id = request.userinfo['reference_id']
        status_dict = {}
        try:
            ### updating diversion status to active status ###
            diversion_data = diversion_status.objects.get(diversion_request_no=request.data['id'])
            diversion_data.approved_by = approved_by
            diversion_data.approved_on = approved_on
            diversion_data.approved_reference_id = approved_reference_id
            diversion_data.modified_by = approved_by
            diversion_data.modified_on = approved_on
            diversion_data.modified_reference_id = approved_reference_id
            diversion_data.record_status = masterconf.approved_status
            diversion_data.save()
            if diversion_data:
                status_dict['status_code'] = masterconf.success_code
                status_dict['status_text'] = masterconf.approval_success_text
                status_dict['diversion_request_no'] = diversion_data.diversion_request_no
                status_dict['indent_order_number'] = diversion_data.indent_order_number
            else:
                status_dict['status_code'] = masterconf.error_code
                status_dict['status_text'] = masterconf.approval_error_text
                status_dict['diversion_request_no'] = diversion_data.diversion_request_no
                status_dict['indent_order_number'] = diversion_data.indent_order_number
            logger.info('Returned response from handle_diversion_status_approve_req of DiversionStatusController: %s',status_dict)
            return status_dict

        except Exception as e:
            status_dict['status_code'] = masterconf.error_code
            status_dict['status_text'] = masterconf.approval_error_text
            logger.error('EXCEPTION IN handle_diversion_status_approve_req METHOD OF DiversionStatusController: %s',
                             e)
            logger.info('Returned response from handle_diversion_status_approve_req of DiversionStatusController: %s',
                        status_dict)
            return status_dict

    #### reject diversion status ###
    def handle_diversion_status_reject_req(self, request):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_diversion_status_reject_req METHOD OF DiversionStatusController.")
        rejected_by = request.userinfo['username'].username
        rejected_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        rejected_reference_id = request.userinfo['reference_id']
        status_dict = {}
        try:
            ### updating diversion status to History status ###
            diversion_data = diversion_status.objects.get(diversion_request_no=request.data['id'])
            diversion_data.modified_by = rejected_by
            diversion_data.modified_on = rejected_on
            diversion_data.modified_reference_id = rejected_reference_id
            diversion_data.record_status = masterconf.reject_status
            diversion_data.save()
            if diversion_data:
                status_dict['status_code'] = masterconf.success_code
                status_dict['status_text'] = masterconf.reject_success_text
                status_dict['diversion_request_no'] = diversion_data.diversion_request_no
                status_dict['indent_order_number'] = diversion_data.indent_order_number
            else:
                status_dict['status_code'] = masterconf.error_code
                status_dict['status_text'] = masterconf.reject_error_text
                status_dict['diversion_request_no'] = diversion_data.diversion_request_no
                status_dict['indent_order_number'] = diversion_data.indent_order_number
            logger.info('Returned response from handle_diversion_status_reject_req of DiversionStatusController: ',
                        status_dict)
            return status_dict

        except Exception as e:
            status_dict['status_code'] = masterconf.error_code
            status_dict['status_text'] = masterconf.approval_error_text
            logger.error('EXCEPTION IN handle_diversion_status_reject_req METHOD OF DiversionStatusController: %s',
                         e)
            logger.error('EXCEPTION IN handle_diversion_status_reject_req METHOD OF DiversionStatusController: %s',
                         e)
            return status_dict

    #### populate dropdown diversion status ###
    def handle_dropdown_req(self, request):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_dropdown_req METHOD OF DiversionStatusController.")
        status_dict = {}
        try:
            ### updating diversion status to History status ###
            dropdown_data_1 = indent_master.objects.get(indent_order_number=request.data['indent_order_number'],record_status=masterconf.active_status)
            dropdown_data_2 = indent_detail.objects.filter(indent_order_number=request.data['indent_order_number'],record_status=masterconf.active_status)
            response_obj = {}
            if dropdown_data_2.exists():
                if dropdown_data_1:
                    response_obj['cra'] = dropdown_data_1.cra
                    response_obj['order_date'] = dropdown_data_1.order_date
                    response_obj['bank'] = dropdown_data_1.bank
                    response_obj['feeder_branch'] = dropdown_data_1.feeder_branch
                    dropdown_data_3 = project_bank_feeder_cra_mapping.objects.get(bank_id=response_obj['bank'],
                                                                                  feeder=response_obj['feeder_branch'])
                    if dropdown_data_3:
                        response_obj['region'] = dropdown_data_3.region
                response_obj['atm'] = []
                for data in dropdown_data_2:
                    response_obj['atm'].append(data.atm_id)
                logger.info('Returned response from handle_dropdown_req of DiversionStatusController: %s',
                            response_obj)
            else:
                response_obj['status_code'] = masterconf.error_code
                response_obj['status_text'] = masterconf.dropdown_list_error
                logger.info('Returned response from handle_dropdown_req of DiversionStatusController: %s',
                            response_obj)
            return response_obj

        except Exception as e:
            status_dict['status_code'] = masterconf.error_code
            status_dict['status_text'] = masterconf.dropdown_list_error
            logger.error('EXCEPTION IN handle_dropdown_req METHOD OF DiversionStatusController: %s',
                         e)
            logger.info('Returned response from handle_dropdown_req of DiversionStatusController: %s',
                        status_dict)
            return status_dict

    #### edit diversion status ###
    def handle_diversion_status_edit_req(self, request):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_diversion_status_edit_req METHOD OF DiversionStatusController.")
        modified_by = request.userinfo['username'].username
        modified_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        modified_reference_id = request.userinfo['reference_id']
        status_dict = {}
        try:
            ### updating diversion status to History status ###
            diversion_data = diversion_status.objects.get(diversion_request_no=request.data['id'])
            print(diversion_data)
            data_dict = {}
            data_dict = request.data['data']
            data_dict['modified_by'] = modified_by
            data_dict['modified_on'] = modified_on
            data_dict['modified_reference_id'] = modified_reference_id
            print(data_dict)
            diversion_status.objects.filter(diversion_request_no=request.data['id']).update(**data_dict)
            if diversion_data:
                status_dict['status_code'] = masterconf.edit_success_code
                status_dict['status_text'] = masterconf.edit_success_text
                status_dict['diversion_request_no'] = diversion_data.diversion_request_no
                status_dict['indent_order_number'] = diversion_data.indent_order_number
            else:
                status_dict['status_code'] = masterconf.edit_error_code
                status_dict['status_text'] = masterconf.edit_error_text
                status_dict['diversion_request_no'] = diversion_data.diversion_request_no
                status_dict['indent_order_number'] = diversion_data.indent_order_number
            logger.info('Returned response from handle_diversion_status_edit_req of DiversionStatusController: ',
                        status_dict)
            return status_dict

        except Exception as e:
            status_dict['status_code'] = masterconf.error_code
            status_dict['status_text'] = masterconf.edit_error_text
            logger.error('EXCEPTION IN handle_diversion_status_edit_req METHOD OF DiversionStatusController: %s',
                         e)
            logger.error('EXCEPTION IN handle_diversion_status_edit_req METHOD OF DiversionStatusController: %s',
                         e)
            return status_dict