from Common.CommonFunctions import common_util
from Model.models import zero_cash_dispense
import logging

class ZeroCashDispenseController:

    def handle_list_req(self,request):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_list_req METHOD OF ZeroCashDispenseController.")
        request_obj = request.data
        logger.info('Request received: %s',request_obj)
        list_returned = list()
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = zero_cash_dispense.objects.filter(**filters_param).values()
            else:
                list_db = zero_cash_dispense.objects.all().values()

            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                # for data in list_db:
                #     response_data.append(data)
                logger.info('Response Generated.')
                return list_db
        except Exception as e:
            print(e)
            logger.error('EXCEPTION IN handle_list_req METHOD OF ZeroCashDispenseController', exc_info=True)
            logger.error(e)
            raise e

