import pandas as pd
import datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master,app_config_param
from datetime import timedelta
from Model.models import indent_holiday
from django.db import connection
import logging

class IndentHoliday_Controller():

	## This is the main controller class for Indent Holiday File upload

	maintain_log_id = 0

	def handle_reference_upload(self,request,input_filepath):

		# This method will handle all validations regarding indent holiday file upload
		logger = logging.getLogger(__name__)
		logger.info("Inside handle_reference_upload of IndentHoliday_Controller")
		common_util_obj = common_util.common_utils()
		username = request.userinfo['username'].username
		uploaded_status = masterconf.uploaded_state
		file_type = request.POST['file_type'].upper()
		df_data = common_util_obj.read_file_xls(input_filepath)
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		status_dict = {}
		try:
			format_status = common_util_obj.validate_mstr_format_xls(df_data, file_type)#Going for format validation of the file
			logger.info("Format_status of the file: ", format_status)
			if format_status == masterconf.format_val_success_reference_upload: # If file is valid
				df_data.columns = df_data.columns.str.lower ( )
				duplicated_data = df_data[df_data.duplicated ( subset=["project id","bank code" , "feeder branch" , "cra","holiday date","holiday name" ] , keep='first' )]
				if duplicated_data.empty != True :
					status_dict["level"] = masterconf.data_level
					status_dict["status_desc"] = masterconf.duplicate_data_entry
					status_dict["status_code"] = masterconf.status_code_for_duplicate_data
					return status_dict

				# Adding meta fields to file's data dataframe
				df_enhanced = common_util_obj.add_fields_master(df_data, uploaded_status, upload_time,username, reference_id,file_type)

				# Logging entry in data_update_log_master table
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type,uploaded_status, upload_time, username,reference_id)
				self.load_reference_data(df_enhanced)
				# Validating uploaded data
				data_status = self.validate_indent_holiday_data(file_type, apiflag, username, reference_id)
				logger.info("data_status from validate_indent_holiday_data::: %s ", data_status)
				status_dict["level"] = masterconf.data_level
				status_dict["status_text"] = data_status["status_text"]
				if data_status["status_text"] == masterconf.no_record:
					status_dict["level"] = masterconf.data_level
					status_dict["status_text"] = data_status["status_text"] + data_status["data"]
					status_dict["file_status"] = "complete file failed"
					return status_dict
				elif data_status["status_text"] == masterconf.data_validated_successful or data_status[
					"status_text"] == masterconf.partial_valid_file:
					if status_dict["status_text"] == masterconf.data_validated_successful:
						status_dict["level"] = masterconf.data_level
						status_dict["status_text"] = masterconf.data_uploaded_successful
						status_dict["file_status"] = masterconf.data_validated_successful
						return status_dict
					else:
						status_dict["level"] = masterconf.data_level
						status_dict["status_text"] = data_status["status_text"] + data_status["data"]
						status_dict["file_status"] = data_status["status_text"]
						return status_dict
				else:
					status_dict["level"] = masterconf.data_level
					status_dict["status_text"] = data_status["status_text"] + data_status["data"]
					status_dict["file_status"] = data_status["status_text"]
					return status_dict
			else:
				status_dict["level"] = masterconf.format_level
				status_dict["status_text"] = format_status + ". Mismatching columns, Please upload correct file."
				return status_dict
		except Exception as e:
			print("Exception occured in IndentHoliday_Controller ::: ",e)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e

	def validate_indent_holiday_data(self , file_type , apiflag , username , reference_id ):
		logger = logging.getLogger(__name__)
		logger.info("IN validate_indent_holiday_data METHOD OF IndentHoliday_Controller.")
		try:
			data_status_return = {}
			sql_str = "SET NOCOUNT ON exec uspValidateReferenceFiles " + "'" + file_type + "', " + "'" + apiflag + "', " + "'" + username + "'," + "'" + reference_id + "'"
			logger.info("sql_str for validate_indent_holiday_data: %s ", sql_str)
			cur = connection.cursor()
			cur.execute(sql_str)
			data_seq_no = cur.fetchone()
			data_status = app_config_param.objects.filter(sequence=int(data_seq_no[0])).values('value')
			logger.info("data_seq_no from validate_indent_holiday_data data_seq_no[0]: %s ", data_seq_no[0])
			if data_seq_no[0] == '50001' or data_seq_no[0] == '10001':
				all_codes = data_update_log_master.objects.filter(created_reference_id=reference_id).values(
					'validation_code')
				all_codes_list = list(all_codes[0]['validation_code'].split(","))
				error_code_str = ''
				for i in all_codes_list:
					error_code = app_config_param.objects.filter(sequence=i).values('value')
					error_code_str = error_code_str + ", " + error_code[0]['value']
				# print ("data_status_return : " , data_status_return)
				data_status_return["status_text"] = data_status[0]['value']
				data_status_return["data"] = error_code_str
				return data_status_return
			data_status_return["status_text"] = data_status[0]['value']
			data_status_return["data"] = data_status[0]['value']  # ES1-I44 Issue Fix
			return data_status_return
		except Exception as e:
			logger.error('EXCEPTION IN validate_indent_holiday_data method OF IndentHoliday_Controller. %s ', e)
			raise e

	def load_reference_data(self, df_enhanced):

		# This method is used to upload data to the database using django bulk insert
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				print(row)
				data.append(
					indent_holiday(
						project_id = row['project id'],
						bank_code = row['bank code'],
						feeder_branch = row['feeder branch'],
						cra = row['cra'],
						holiday_date=row['holiday date'],
						is_holiday=row['is holiday'],
						holiday_name=row['holiday name'],
						state_code=row['state'],
						district_code=row['district'],
						city_code=row['city'],
						area_code=row['area'],
						withdrawal_type=row['withdrawal type'],
						is_indent_required=row['is indent required'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)

			print(data)
			indent_holiday.objects.bulk_create(data)#inserting the data into respective table
		except Exception as e:
			print ("In Exception of load_reference_data of IndentHoliday_Controller. : ",e)
			raise e
