import pandas as pd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from datetime import timedelta
from Model.models import atm_revision_cash_availability

class ATMRevisionCashAvailability_Controller():

	## This is the main controller class for ATM Revision Cash Availability File upload

	maintain_log_id = 0

	def handle_reference_upload(self,request,input_filepath):

		# This method will handle all validations regarding brand bill file upload
		print ("Inside handle_reference_upload of ATMRevisionCashAvailability_Controller")
		common_util_obj = common_util.common_utils()
		username = request.userinfo['username'].username
		uploaded_status = masterconf.uploaded_state
		file_type = request.POST['file_type'].upper()
		df_data = common_util_obj.read_file_xls(input_filepath)
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		status_dict = {}
		try:
			format_status = common_util_obj.validate_mstr_format_xls(df_data, file_type)#Going for format validation of the file
			print("Format_status of the file: ", format_status)
			if format_status == masterconf.format_val_success_reference_upload: # If file is valid
				df_data.columns = df_data.columns.str.lower ( )
				duplicated_data = df_data[df_data.duplicated ( subset=["project id", "bank code" , "atm id" , "site code" , "feeder branch code", "for date"] , keep='first' )]
				if duplicated_data.empty != True :
					status_dict["level"] = masterconf.data_level
					status_dict["status_desc"] = masterconf.duplicate_data_entry
					status_dict["status_code"] = masterconf.status_code_for_duplicate_data
					return status_dict

				# Adding meta fields to file's data dataframe
				df_enhanced = common_util_obj.add_fields_master(df_data, uploaded_status, upload_time,username, reference_id,file_type)

				# Logging entry in data_update_log_master table
				self.maintain_log_id = common_util_obj.maintainlog_master(file_type,uploaded_status, upload_time, username,reference_id)
				self.load_reference_data(df_enhanced)
				# Validating uploaded data
				data_code = common_util_obj.validate_reference_data(file_type, apiflag, username, reference_id)
				status_dict["status_code"] = data_code
				return status_dict
			else:
				status_dict["status_code"] = format_status
				return status_dict
		except Exception as e:
			print("Exception occured in ATMRevisionCashAvailability_Controller ::: ",e)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e

	def load_reference_data(self, df_enhanced):

		# This method is used to upload data to the database using django bulk insert
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				print(row)
				data.append(
					atm_revision_cash_availability(
						project_id = row['project id'],
						bank_code = row['bank code'],
						feeder_branch_code = row['feeder branch code'],
						atm_id = row['atm id'],
						site_code = row['site code'],
						for_date = row['for date'],
						denomination_100=row['denomination 100'],
						denomination_200=row['denomination 200'],
						denomination_500=row['denomination 500'],
						denomination_2000=row['denomination 2000'],
						total_amount=row['total'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)

			print(data)
			atm_revision_cash_availability.objects.bulk_create(data)#inserting the data into respective table
		except Exception as e:
			print ("In Exception of load_reference_data of ATMRevisionCashAvailability_Controller. : ",e)
			raise e
