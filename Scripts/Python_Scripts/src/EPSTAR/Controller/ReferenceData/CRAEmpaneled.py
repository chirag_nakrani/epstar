from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
# from Model import models
import csv
from Common import db_queries_properties
from Common.models import data_update_log_master#we are saving data update log of reference data in data_update_log_master as it is updated according to our development and tracking the data
from django.apps import apps
from datetime import timedelta
from Model.models import CRA_Empaneled


class CRAEmpaneled_Controller():

	## This is the main controller class for CRA Empaneled File upload

	maintain_log_id = 0

	def handle_reference_upload(self,request,input_filepath):

		# This method will handle all validations regarding CRA Empaneled file upload

		common_util_obj = common_util.common_utils()
		username = request.userinfo['username'].username
		uploaded_status = 'Uploaded'
		file_type = request.POST['file_type'].upper()
		df_data = common_util_obj.read_file_xls(input_filepath)
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		# print("reference_id from CRAEmpaneled_Controller: ",reference_id)
		status_dict = {}
		try:
			format_status = common_util_obj.validate_reference_format_xls(df_data, file_type)#Going for format validation of the file
			print("Format_status from CRAEmpaneled_Controller: ", format_status)
			if format_status == masterconf.format_val_success_reference_upload: # If file is valid i.e. format validation is successful

				# Adding meta fields to file's data dataframe
				df_enhanced = common_util_obj.add_fields_reference(df_data, uploaded_status, upload_time,username, reference_id,file_type)

				# Logging entry in data_update_log_master table
				self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time, username,reference_id)
				print("After referencelog")

				# Loading data into database
				self.load_CRA_Empaneled_reference_data(df_enhanced,file_type)

				# Validating uploaded data
				data_code = common_util_obj.validate_reference_data(file_type, apiflag, username, reference_id)
				status_dict["status_code"] = data_code
				return status_dict
			else:
				status_dict["status_code"] = format_status
				return status_dict
		except Exception as e:
			print("Exception occured in CRAEmpaneled_Controller ::: ",e)
			#print ("maintain_log_id : ",maintain_log_id)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e

	def load_CRA_Empaneled_reference_data(self, df_enhanced, file_type):

		# This method is used to upload data to the database using django bulk insert
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				data.append(
					CRA_Empaneled(
						cra_code = row['CRA Code'],
						cra_name = row['Name of CRA'],
						registered_office = row['Registered Office'],
						registration_number = row['Registration Number'],
						contact_details = row['Contact Details'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)
			#print(df_enhanced.columns)
			CRA_Empaneled.objects.bulk_create(data)#inserting the data into respective table
		except Exception as e:
			print ("In Exception of load_CRA_Empaneled_reference_data of CRAEmpaneled. : ",e)
			raise e
