
from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Model.models import default_loading
from datetime import timedelta
from Common.models import data_update_log_master#we are using this because data_update_log_reference is not updadted and in future it will get eliminated

class DefaultLoadController():
###############This handle will insert new record in the DB #################################################################################
	def handle_defaultLoad_add_data(self,request_data):
		try:
			common_util_obj = common_util.common_utils()
			print ("Inside handle_defaultLoad_add_data of DefaultLoadController for inserting the record: ",request_data)
			created_by = request_data.userinfo['username'].username
			created_on = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
			created_reference_id = request_data.userinfo['reference_id']
			payload = request_data.data['payload']
			status_dict = {}
			record_status_uploaded_state = masterconf.uploaded_state
			for single_record in payload:
				add_dict = {
						"project_id" : single_record["project_id"],
						"bank_code" : single_record["bank_code"],
						"amount" : single_record["amount"],
						"record_status" : record_status_uploaded_state,
						"created_on" : created_on,
						"created_by" : created_by,
						"created_reference_id" : created_reference_id,
					}
				load_data = default_loading(**add_dict)#inserting all the records together in the default_loading table and saving the same
				load_data.save()
			file_type = masterconf.file_type_default_uploading.upper()
			apiflag = 'S'#there are 2 types of uploading the data i.e. from file or from screen, F=file upload or inserting the record, S = inserting it through screen
			self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,record_status_uploaded_state, created_on,created_by,created_reference_id)
			status_code = common_util_obj.validate_reference_data(file_type, apiflag, created_by, created_reference_id)#validating individual record if they are correct
			print ("data_code returned from validate_reference_data for inserting new record: ",status_code)
			if status_code == masterconf.data_val_success_mstr_upload:#if record is inserted successfully
				status_dict['status_code'] = status_code
				status_dict["data"] = masterconf.data_updated_for_approval
			else:#if record is failed to insert
				status_dict['status_code'] = status_code
				status_dict["data"] = masterconf.data_insert_modify_failed
			print ("status_dict returned from handle_defaultLoad_add_data when record is added : ",status_dict)#debugging purpose about type of response it is returning
			return status_dict
		except Exception as e:
			print("Exception from handle_defaultLoad_add_data of DefaultLoadController : ",e)
			raise e
