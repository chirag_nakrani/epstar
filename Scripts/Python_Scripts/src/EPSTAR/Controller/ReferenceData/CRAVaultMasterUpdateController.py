from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
from Model import models
import csv
from Common import db_queries_properties
from Common.models import data_update_log_master
from django.apps import apps
from Model.models import CRA_vault_master,CRA_vendor_master


class CRAVaultMasterUpdateController():
    def handle_reference_update(self,request):
        common_util_obj = common_util.common_utils()
        update_list = request.data['update']
        file_type = request.data['file_type']
        project_id = request.data['project_id']
        apiflag = 'S'
        username = request.userinfo['username'].username
        reference_id = request.userinfo['reference_id']
        upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        status_dict = {}
        try:
            for update_obj in update_list:
                update_obj['project_id'] = project_id
                update_obj['record_status'] = 'Uploaded'
                update_obj['created_on'] = upload_time
                update_obj['created_by'] = username
                update_obj['created_reference_id'] = reference_id
                if file_type.upper() == masterconf.file_type_vault_master:
                    cra_vault_master = CRA_vault_master(**update_obj)
                    cra_vault_master.save()
                elif file_type.upper() == masterconf.file_type_vendor_master:
                    cra_vendor_master = CRA_vendor_master(**update_obj)
                    cra_vendor_master.save()
                # CRA_vault_master = CRA_vault_master(**update_obj)
                # CRA_vault_master.save()
                data_code = common_util_obj.validate_reference_data(file_type, apiflag, username, reference_id)
                status_dict["level"] = masterconf.data_level
                status_dict["status_code"] = data_code
                return status_dict
        except Exception as e:
            #print("Exception in CRAVaultMasterUpdateController ",e)
            raise e
