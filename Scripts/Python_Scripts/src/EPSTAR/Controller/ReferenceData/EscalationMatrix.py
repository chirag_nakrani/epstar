import pandas as pd
import datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_reference
from datetime import timedelta
from Model.models import cra_escalation_emails, eps_escalation_emails, Bank_Escalation_Emails

class EscalationMatrix_Controller():
    #Controller to handle escalation matrix reference data upload of three types CRA, EPS and BANK

    maintain_log_id = 0
    def handle_reference_upload(self,request,input_filepath):
        common_util_obj = common_util.common_utils()
        username = request.userinfo['username'].username
        uploaded_status = 'Uploaded'
        file_type = request.POST['file_type'].upper()
        df_data = common_util_obj.read_file_xls_escalation(input_filepath) # reading excel file
        upload_time = datetime.datetime.utcnow()+timedelta(hours=5, seconds=1800)
        reference_id = request.userinfo['reference_id']
        apiflag = 'F'
        #print("reference_id from EscalationMatrix_Controller: ",reference_id)
        status_dict = {}
        try:
            #### validating excel file format by checking columns defined in meta data table reference ###
            format_status = common_util_obj.validate_reference_format_xls(df_data, file_type) # validating excel file header format from metadata table
            print("format_status from EscalationMatrix_Controller is : ", format_status)
            #### checking if status received is 'S100' to proceed with the further operations ######
            if format_status == masterconf.format_val_success_reference_upload:
                if file_type == masterconf.file_type_escalation_matrix[2]:
                    df_data.columns = df_data.columns.str.lower ( )
                    duplicated_data = df_data[df_data.duplicated (subset=['location' , 'email id'] ,keep='first' )]
                    if duplicated_data.empty != True :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_desc"] = masterconf.duplicate_data_entry
                        status_dict["status_code"] = masterconf.status_code_for_duplicate_data
                        return status_dict
                elif file_type == masterconf.file_type_escalation_matrix[1]:
                    df_data.columns = df_data.columns.str.lower ( )
                    duplicated_data = df_data[df_data.duplicated (subset=['project code' , 'bank', 'branch'] ,keep='first' )]
                    if duplicated_data.empty != True :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_desc"] = masterconf.duplicate_data_entry
                        status_dict["status_code"] = masterconf.status_code_for_duplicate_data
                        return status_dict
                elif file_type == masterconf.file_type_escalation_matrix[0]:
                    df_data.columns = df_data.columns.str.lower ( )
                    duplicated_data = df_data[df_data.duplicated (subset=['cra' , 'location', 'circle'] ,keep='first' )]
                    if duplicated_data.empty != True :
                        status_dict["level"] = masterconf.data_level
                        status_dict["status_desc"] = masterconf.duplicate_data_entry
                        status_dict["status_code"] = masterconf.status_code_for_duplicate_data
                        return status_dict
                #### checking excel file format with columns defined in reference_attr_dict dictionary in db_queries_properties.py file ###
                ### if columns not present add it to the dataframe with null values #######
                df_enhanced = common_util_obj.add_fields_reference(df_data, uploaded_status, upload_time,username, reference_id,file_type) # adding columns if not present in excel file and inserting null value for the same
                ###### maintaining log in data log table #########
                self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time, username,reference_id) # logging reference request in data log reference table
                #print("After referencelog")
                if file_type == masterconf.file_type_escalation_matrix[0].upper():
                    self.load_cra_escalation_matrix_reference_data(df_enhanced) # function to populate CRA type escalation matrix data
                elif file_type == masterconf.file_type_escalation_matrix[1].upper():
                    self.load_bank_escalation_matrix_reference_data(df_enhanced) # function to populate BANK type escalation matrix data
                elif file_type == masterconf.file_type_escalation_matrix[2].upper():
                    self.load_eps_escalation_matrix_reference_data(df_enhanced) # function to populate EPS type escalation matrix data
                ##### validating reference data with the stored procedure defined ########
                data_code = common_util_obj.validate_reference_data(file_type, apiflag, username, reference_id) # validating populated data
                status_dict["level"] = masterconf.data_level
                status_dict["status_code"] = data_code
                return status_dict
            else:
                status_dict["level"] = masterconf.format_level
                status_dict["status_code"] = masterconf.format_val_failed_reference_upload
                status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.format_val_failed_reference_upload]
                return status_dict
        except Exception as e:
            print("Exception occured in EscalationMatrix_Controller ::: ",e)
            #print ("maintain_log_id : ",maintain_log_id)
            entry = data_update_log_reference.objects.get(id=self.maintain_log_id)
            entry.record_status = 'Failed'
            entry.save()
            raise e

    def load_cra_escalation_matrix_reference_data(self, df_enhanced):
        # function to populate CRA escalation matrix data using django model

        df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
        try:
            data = list()
            for i, row in df_enhanced_new.iterrows():
                data.append(
                    cra_escalation_emails(
                        cra=row['cra'],
                        location=row['location'],
                        location_circle=row['circle'],
                        activity=row['activity'],
                        primary_email_id=row['email id'],
                        primary_contact_no=row['contact no.'],
                        level_1_email_id=row['email id.1'],
                        level_1_contact_no=row['contact no..1'],
                        level_2_email_id=row['email id.2'],
                        level_2_contact_no=row['contact no..2'],
                        level_3_email_id=row['email id.3'],
                        level_3_contact_no=row['contact no..3'],
                        level_4_email_id=row['email id.4'],
                        level_4_contact_no=row['contact no..4'],
                        record_status=row['record_status'],
                        created_on=row['created_on'],
                        created_by=row['created_by'],
                        created_reference_id=row['created_reference_id'])
                )
            cra_escalation_emails.objects.bulk_create(data)
        except Exception as e:
            print ("Exception from load_cra_escalation_matrix_reference_data : ",e)
            raise e

    def load_eps_escalation_matrix_reference_data(self, df_enhanced):
        # function to populate EPS escalation matrix data using django model

        df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
        try:
            data = list()
            for i, row in df_enhanced_new.iterrows():
                data.append(
                    eps_escalation_emails(
                        eps=row['eps'],
                        bank=row['location'],
                        activity=row['activity'],
                        primary_email_id=row['email id'],
                        primary_contact_no=row['contact no.'],
                        level_1_email_id=row['email id.1'],
                        level_1_contact_no=row['contact no..1'],
                        level_2_email_id=row['email id.2'],
                        level_2_contact_no=row['contact no..2'],
                        level_3_email_id=row['email id.3'],
                        level_3_contact_no=row['contact no..3'],
                        level_4_email_id=row['email id.4'],
                        level_4_contact_no=row['contact no..4'],
                        record_status=row['record_status'],
                        created_on=row['created_on'],
                        created_by=row['created_by'],
                        created_reference_id=row['created_reference_id'])
                )
            eps_escalation_emails.objects.bulk_create(data)
        except Exception as e:
            print ("Exception from load_eps_escalation_matrix_reference_data : ",e)
            raise e

    def load_bank_escalation_matrix_reference_data(self, df_enhanced):
        # function to populate Bank escalation matrix data using django model

        df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
        try:
            data = list()
            for i, row in df_enhanced_new.iterrows():
                data.append(
                    Bank_Escalation_Emails(
                        bank=row['bank'],
                        branch=row['branch'],
                        sol_id=row['sol id'],
                        circle=row['circle'],
                        activity=row['activity'],
                        primary_email_id=row['email id'],
                        primary_contact_no=row['contact no.'],
                        level_1_email_id=row['email id.1'],
                        level_1_contact_no=row['contact no..1'],
                        level_2_email_id=row['email id.2'],
                        level_2_contact_no=row['contact no..2'],
                        level_3_email_id=row['email id.3'],
                        level_3_contact_no=row['contact no..3'],
                        level_4_email_id=row['email id.4'],
                        level_4_contact_no=row['contact no..4'],
                        project_id=row['project code'],
                        record_status=row['record_status'],
                        created_on=row['created_on'],
                        created_by=row['created_by'],
                        created_reference_id=row['created_reference_id'])
                )
            Bank_Escalation_Emails.objects.bulk_create(data)
        except Exception as e:
            print ("Exception from load_bank_escalation_matrix_reference_data : ",e)
            raise e
