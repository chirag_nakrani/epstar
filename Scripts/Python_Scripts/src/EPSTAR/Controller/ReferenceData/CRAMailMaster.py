import pandas as pd
import datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master#we are saving data update log of reference data in data_update_log_master as it is updated according to our development and tracking the data
from datetime import timedelta
from Model.models import Mail_Master

class CRAMailMaster_Controller():
    # Controller to handle mail master reference data upload
    maintain_log_id = 0
    def handle_reference_upload(self,request,input_filepath):
        common_util_obj = common_util.common_utils()
        username = request.userinfo['username'].username
        uploaded_status = 'Uploaded'
        file_type = request.POST['file_type'].upper()
        df_data = common_util_obj.read_file_xls(input_filepath)
        upload_time = datetime.datetime.utcnow()+timedelta(hours=5, seconds=1800)
        reference_id = request.userinfo['reference_id']
        apiflag = 'F'
        #print("reference_id from CRAVaultMaster_Controller: ",reference_id)
        status_dict = {}
        try:
            #### validating excel file format by checking columns defined in meta data table reference ###
            format_status = common_util_obj.validate_reference_format_xls(df_data, file_type)#
            print("format_status from CRAMailMaster_Controller: ", format_status)
            #### checking if status received is 'S100' to proceed with the further operations ######
            if format_status == masterconf.format_val_success_reference_upload:
                #### checking excel file format with columns defined in reference_attr_dict dictionary in db_queries_properties.py file ###
                ### if columns not present add it to the dataframe with null values #######
                df_enhanced = common_util_obj.add_fields_reference(df_data, uploaded_status, upload_time,username,reference_id,file_type)
                ###### maintaining log in data log table #########
                self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time,username,reference_id)
                #print("After referencelog")
                self.load_mail_master_reference_data(df_enhanced)
                ##### validating reference data with the stored procedure defined ########
                data_code = common_util_obj.validate_reference_data(file_type, apiflag, username, reference_id)
                status_dict["level"] = masterconf.data_level
                status_dict["status_code"] = data_code
                return status_dict
            else:
                status_dict["level"] = masterconf.format_level
                status_dict["status_code"] = masterconf.format_val_failed_reference_upload
                status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.format_val_failed_reference_upload]
                return status_dict
        except Exception as e:
            print("Exception occured in CRAMailMaster_Controller::: ",e)
            #print ("maintain_log_id : ",maintain_log_id)
            entry = data_update_log_master.objects.get(id=self.maintain_log_id)
            entry.record_status = 'Failed'
            entry.save()
            raise e

    def load_mail_master_reference_data(self, df_enhanced):
        ### function to load mail master reference data using django model #######

        df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
        try:
            data = list()
            for i, row in df_enhanced_new.iterrows():
                data.append(
                    Mail_Master(
                        project_id=row['Project'],
                        bank_code=row['Bank'],
                        feeder_branch=row['FEEDER BRANCH'],
                        sol_id=row['SOL ID'],
                        cra=row['CRA'],
                        to_bank_email_id=row["TO - BANK MAIL ID'S"],
                        to_cra_email_id=row["CC - CRA'S MAIL ID"],
                        bcc_email_id=row['BCC'],
                        subject=row['Subject'],
                        body_text=row['Body text'],
                        state=row['State'],
                        importance=row['Importance'],
                        atm_count=row['ATM Count'],
                        status=row['Status'],
                        from_email_id = row['from email id'],
                        record_status=row['record_status'],
                        created_on=row['created_on'],
                        created_by=row['created_by'],
                        created_reference_id=row['created_reference_id'])
                )
            Mail_Master.objects.bulk_create(data)#inserting the data into respective table
        except Exception as e:
            print ("Exception from load_mail_master_reference_data :",e)
            raise e
