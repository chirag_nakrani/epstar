from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
# from Model import models
import csv
from Common import db_queries_properties
from Common.models import data_update_log
from django.apps import apps
from datetime import timedelta
from Model.models import Holiday_Master


class HolidayMaster_Controller():
	maintain_log_id = 0
	def handle_reference_upload(self,request,input_filepath):
		common_util_obj = common_util.common_utils()
		username = request.userinfo['username'].username
		uploaded_status = 'Uploaded'
		file_type = request.POST['file_type']
		df_data = common_util_obj.read_file_xls(input_filepath)
		project_id = request.POST['project_id']
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		#print("reference_id from HolidayMaster_Controller: ",reference_id)
		status_dict = {}
		try:
			format_status = common_util_obj.validate_reference_format_xls(df_data, file_type.upper())#
			#print("format_status HolidayMaster_Controller: ", format_status)
			if format_status == masterconf.format_val_success_reference_upload:
				df_enhanced = common_util_obj.add_fields_reference(df_data, uploaded_status, upload_time,username, project_id,reference_id,file_type.upper())
				self.maintain_log_id = common_util_obj.maintainlog_reference(file_type.upper(),uploaded_status, upload_time, project_id,username,reference_id)
				#print("After referencelog")
				self.load_holiday_master_reference_data(df_enhanced,file_type)
				data_code = common_util_obj.validate_reference_data(file_type, apiflag, username, reference_id)
				status_dict["level"] = masterconf.data_level
				status_dict["status_code"] = data_code
				return status_dict
			else:
				status_dict["level"] = masterconf.format_level
				status_dict["status_code"] = masterconf.format_val_failed_reference_upload
				status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.format_val_failed_reference_upload]
				return status_dict
		except Exception as e:
			#print("Exception occured in HolidayMaster_Controller ::: ",e)
			#print ("maintain_log_id : ",maintain_log_id)
			entry = data_update_log.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e

	def load_holiday_master_reference_data(self, df_enhanced, file_type):
		file_type = str(file_type).upper()
		table_key = file_type + "_TABLE"
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		field_list = db_queries_properties.reference_field_dict[file_type]
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				data.append(
					Brand_Bill_Capacity(
						id = row['id'],
						holiday_code = row['holiday_code'],
						holiday_name = row['holiday_name'],
						holiday_description = row['holiday_description'],
						holiday_type = row['holiday_type'],
						project_id = row['project_id'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)
			#print(df_enhanced.columns)
			Holiday_Master.objects.bulk_create(data)
		except Exception as e:
			raise e
