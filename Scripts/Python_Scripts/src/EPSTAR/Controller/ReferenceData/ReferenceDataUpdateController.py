import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from Model.models import brand_bill_capacity,cra_vault_master,CRA_Empaneled,cra_escalation_emails,Bank_Escalation_Emails,eps_escalation_emails,default_loading, cypher_code, atm_list_for_revision, atm_revision_cash_availability, Mail_Master, gl_account_number
from datetime import timedelta


class ReferenceDataUpdateController():

	# This is the main class for handling reference data update through Screen

	maintain_log_id = 0

	def handle_reference_update(self,request):

		#This method is for updating the existing records through screen for Reference Menus -- Brand Bill, CRA Empaneled and Vault Master

		common_util_obj = common_util.common_utils()
		update_list = request.data['update']
		file_type = request.data['file_type'].upper()
		apiflag = 'S'
		username = request.userinfo['username'].username
		reference_id = request.userinfo['reference_id']
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		uploaded_status = masterconf.uploaded_state
		status_dict = {}
		try:

			if file_type == masterconf.file_type_RD_brand_BillCapacity.upper():
				## Code block for brand_bill_capacity
				for update_obj in update_list:
					row_obj = {}
					row_obj['capacity_50'] = update_obj['capacity_50']
					row_obj['capacity_100'] = update_obj['capacity_100']
					row_obj['capacity_200'] = update_obj['capacity_200']
					row_obj['capacity_500'] = update_obj['capacity_500']
					row_obj['capacity_2000'] = update_obj['capacity_2000']
					row_obj['brand_code'] = update_obj['brand_code']
					row_obj['description'] = update_obj['description']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					bbc = brand_bill_capacity(**row_obj) # brand bill capacity Model object
					bbc.save()

			elif file_type.upper() == masterconf.file_type_vault_master:
				## Code block for CRA_vault_master
				for update_obj in update_list:
					row_obj = {}
					row_obj['vault_code'] = update_obj['vault_code']
					row_obj['cra_code'] = update_obj['cra_code']
					row_obj['state'] = update_obj['state']
					row_obj['location'] = update_obj['location']
					row_obj['address'] = update_obj['address']
					row_obj['vault_type'] = update_obj['vault_type']
					row_obj['vault_status'] = update_obj['vault_status']
					row_obj['contact_details'] = update_obj['contact_details']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					cra_vault_master_obj = cra_vault_master(**row_obj) # CRA Vault Master Model object
					cra_vault_master_obj.save()
			elif file_type.upper() == masterconf.file_type_Empaneled:
				## Code block for CRA_Empaneled
				for update_obj in update_list:
					row_obj = {}
					row_obj['cra_code'] = update_obj['cra_code']
					row_obj['cra_name'] = update_obj['cra_name']
					row_obj['registered_office'] = update_obj['registered_office']
					row_obj['registration_number'] = update_obj['registration_number']
					row_obj['contact_details'] = update_obj['contact_details']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					cRA_Empaneled = CRA_Empaneled(**row_obj)  # CRA Empaneled Model object
					cRA_Empaneled.save()

			elif file_type.upper() == masterconf.file_type_escalation_matrix[0].upper():
				## Code block for Cra escalation
				for update_obj in update_list:
					row_obj = {}
					row_obj['cra'] = update_obj['cra']
					row_obj['location'] = update_obj['location']
					row_obj['location_circle'] = update_obj['location_circle']
					row_obj['activity'] = update_obj['activity']
					row_obj['primary_email_id'] = update_obj['primary_email_id']
					row_obj['primary_contact_no'] = update_obj['primary_contact_no']
					row_obj['level_1_email_id'] = update_obj['level_1_email_id']
					row_obj['level_1_contact_no'] = update_obj['level_1_contact_no']
					row_obj['level_2_email_id'] = update_obj['level_2_email_id']
					row_obj['level_2_contact_no'] = update_obj['level_2_contact_no']
					row_obj['level_3_email_id'] = update_obj['level_3_email_id']
					row_obj['level_3_contact_no'] = update_obj['level_3_contact_no']
					row_obj['level_4_email_id'] = update_obj['level_4_email_id']
					row_obj['level_4_contact_no'] = update_obj['level_4_contact_no']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Cra_escalation_emails = cra_escalation_emails(**row_obj)  # CRA Empaneled Model object
					Cra_escalation_emails.save()

			elif file_type.upper() == masterconf.file_type_mail_master.upper():
				## Code block for Mail master
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch'] = update_obj['feeder_branch']
					row_obj['sol_id'] = update_obj['sol_id']
					row_obj['cra'] = update_obj['cra']
					row_obj['to_bank_email_id'] = update_obj['to_bank_email_id']
					row_obj['to_cra_email_id'] = update_obj['to_cra_email_id']
					row_obj['bcc_email_id'] = update_obj['bcc_email_id']
					row_obj['subject'] = update_obj['subject']
					row_obj['body_text'] = update_obj['body_text']
					row_obj['state'] = update_obj['state']
					row_obj['importance'] = update_obj['importance']
					row_obj['atm_count'] = update_obj['atm_count']
					row_obj['status'] = update_obj['status']
					row_obj['from_email_id'] = update_obj['from_email_id']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					mail_Master = Mail_Master(**row_obj)  # Mail_Master Model object
					mail_Master.save()

			elif file_type.upper() == masterconf.file_type_escalation_matrix[1].upper():
				## Code block for Bank escalation
				for update_obj in update_list:
					row_obj = {}
					row_obj['bank'] = update_obj['bank']
					row_obj['branch'] = update_obj['branch']
					row_obj['activity'] = update_obj['activity']
					row_obj['primary_email_id'] = update_obj['primary_email_id']
					row_obj['primary_contact_no'] = update_obj['primary_contact_no']
					row_obj['level_1_email_id'] = update_obj['level_1_email_id']
					row_obj['level_1_contact_no'] = update_obj['level_1_contact_no']
					row_obj['level_2_email_id'] = update_obj['level_2_email_id']
					row_obj['level_2_contact_no'] = update_obj['level_2_contact_no']
					row_obj['level_3_email_id'] = update_obj['level_3_email_id']
					row_obj['level_3_contact_no'] = update_obj['level_3_contact_no']
					row_obj['level_4_email_id'] = update_obj['level_4_email_id']
					row_obj['level_4_contact_no'] = update_obj['level_4_contact_no']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					bank_Escalation_Emails = Bank_Escalation_Emails(**row_obj)  # CRA Empaneled Model object
					bank_Escalation_Emails.save()

			elif file_type.upper() == masterconf.file_type_escalation_matrix[2].upper():
				## Code block for Bank escalation
				for update_obj in update_list:
					row_obj = {}
					row_obj['eps'] = update_obj['eps']
					row_obj['location'] = update_obj['location']
					row_obj['activity'] = update_obj['activity']
					row_obj['primary_email_id'] = update_obj['primary_email_id']
					row_obj['primary_contact_no'] = update_obj['primary_contact_no']
					row_obj['level_1_email_id'] = update_obj['level_1_email_id']
					row_obj['level_1_contact_no'] = update_obj['level_1_contact_no']
					row_obj['level_2_email_id'] = update_obj['level_2_email_id']
					row_obj['level_2_contact_no'] = update_obj['level_2_contact_no']
					row_obj['level_3_email_id'] = update_obj['level_3_email_id']
					row_obj['level_3_contact_no'] = update_obj['level_3_contact_no']
					row_obj['level_4_email_id'] = update_obj['level_4_email_id']
					row_obj['level_4_contact_no'] = update_obj['level_4_contact_no']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Eps_escalation_emails = eps_escalation_emails(**row_obj)  # CRA Empaneled Model object
					Eps_escalation_emails.save()

			elif file_type.upper() == masterconf.file_type_default_uploading.upper():#This method is used for updating the active record present in db
				## Code block for Bank escalation
				for update_obj in update_list:
					row_obj = {}
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['amount'] = update_obj['amount']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Default_loading = default_loading(**row_obj)  # default loading Model object
					Default_loading.save()

			elif file_type == masterconf.file_type_cypher_code.upper():
				## Code block for cypher code
				for update_obj in update_list:
					row_obj = {}
					row_obj['category'] = update_obj['category']
					row_obj['value'] = update_obj['value']
					row_obj['cypher_code'] = update_obj['cypher_code']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					cc = cypher_code(**row_obj) # Cypher Code Model object
					cc.save()

			elif file_type == masterconf.revision_via_file_upload_file_type.upper():
				## Code block for revision file upload of atm list
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['site_code'] = update_obj['site_code']
					row_obj['for_date'] = update_obj['for_date']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					cc = atm_list_for_revision(**row_obj) # Cypher Code Model object
					cc.save()
			elif file_type == masterconf.atm_revision_cash_availability_file_type.upper():
				## Code block for revision file upload of atm list
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['site_code'] = update_obj['site_code']
					row_obj['for_date'] = update_obj['for_date']
					row_obj['denomination_100'] = update_obj['denomination_100']
					row_obj['denomination_200'] = update_obj['denomination_200']
					row_obj['denomination_500'] = update_obj['denomination_500']
					row_obj['denomination_2000'] = update_obj['denomination_2000']
					row_obj['total_amount'] = update_obj['total_amount']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					cc = atm_revision_cash_availability(**row_obj) # Cypher Code Model object
					cc.save()
			elif file_type == masterconf.general_ledger_account_number_file_type.upper():
				## Code block for general ledger account number
				for update_obj in update_list:
					row_obj = {}
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['site_code'] = update_obj['site_code']
					row_obj['gl_acc_number'] = update_obj['gl_acc_number']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					cc = gl_account_number(**row_obj) # Cypher Code Model object
					cc.save()
			# Validating Reference Data
			# Inserting entry in data_update_log_master table
			self.maintain_log_id = common_util_obj.maintainlog_reference(file_type.upper(), uploaded_status, upload_time,username, reference_id)
			apiflag = 'S'
			data_code = common_util_obj.validate_reference_data(file_type, apiflag, username, reference_id)
			print ("data_code from handle_reference_update of ReferenceDataUpdateController: ",data_code)
			if data_code == masterconf.data_val_success_upload:
				if file_type == masterconf.general_ledger_account_number_file_type.upper() or file_type == masterconf.atm_revision_cash_availability_file_type.upper():
					status_dict["data"] = masterconf.data_inserted_successfully_active_state
					status_dict["status_code"] = data_code
					return status_dict
				else:
					status_dict["data"] = masterconf.data_inserted_successfully
					status_dict["status_code"] = data_code
					return status_dict
			else:
				status_dict["data"] = masterconf.error_codes_desc["E103"]
				status_dict["status_code"] = data_code
				return status_dict
		except Exception as e:
			print("Exception in ReferenceDataUpdateController ",e)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e
