import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from datetime import timedelta
from Model.models import project_master, bank_master, region_master, CRA_master

class Add_Project_Bank_Region_Controller():

	## This is the main controller class for Adding or modying Project, bank, Region

	maintain_log_id = 0

	def handle_add_request(self,request):

		# This method will handle all validations regarding Adding or modying Project, bank, Region
		print ("Inside handle_add_request of Add_Project_Bank_Region_Controller")
		common_util_obj = common_util.common_utils()
		username = request.userinfo['username'].username
		file_type = request.data['file_type'].lower()
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		payload = request.data['payload']
		status_dict = {}
		active_status = masterconf.active_status
		try:
			if file_type == masterconf.modify_entity[0].lower():#inserting record if it is for adding project
				for single_record in payload:
					add_dict = {
							"project_id" : single_record["project_id"],
							"record_status" : active_status,
							"created_on" : upload_time,
							"created_by" : username,
							"created_reference_id" : reference_id,
						}
					load_data = project_master(**add_dict)#inserting all the records together in the default_loading table and saving the same
					load_data.save()

			elif file_type == masterconf.modify_entity[1].lower():#inserting record if it is for adding bank
				for single_record in payload:
					add_dict = {
							"bank_code" : single_record["bank_code"],
							"bank_name" : single_record["bank_name"],
							"record_status" : active_status,
							"created_on" : upload_time,
							"created_by" : username,
							"created_reference_id" : reference_id,
						}
					load_data = bank_master(**add_dict)#inserting all the records together in the default_loading table and saving the same
					load_data.save()

			elif file_type == masterconf.modify_entity[2].lower():#inserting record if it is for adding region
				for single_record in payload:
					add_dict = {
							"region_name" : single_record["region_name"],
							"record_status" : active_status,
							"created_on" : upload_time,
							"created_by" : username,
							"created_reference_id" : reference_id,
						}
					load_data = region_master(**add_dict)#inserting all the records together in the default_loading table and saving the same
					load_data.save()

			elif file_type == masterconf.modify_entity[3].lower():#inserting record if it is for new cra
				for single_record in payload:
					add_dict = {
							"cra_id" : single_record["cra_id"],
							"cra_name" : single_record["cra_name"] ,
							"record_status" : active_status,
							"created_on" : upload_time,
							"created_by" : username,
							"created_reference_id" : reference_id,
						}
					load_data = CRA_master(**add_dict)#inserting all the records together in the default_loading table and saving the same
					load_data.save()


			self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,active_status, upload_time,username,reference_id)
			status_dict["status_text"] = masterconf.insert_record
			return status_dict
		except Exception as e:
			print("Exception from handle_add_request of Add_Project_Bank_Region_Controller : ",e)
			raise e

	def handle_modify_request(self,request):

		# This method will handle all validations regarding Adding or modying Project, bank, Region
		print ("Inside handle_modify_request of Add_Project_Bank_Region_Controller")
		common_util_obj = common_util.common_utils()
		username = request.userinfo['username'].username
		file_type = request.data['file_type'].lower()
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		payload = request.data['payload']
		status_dict = {}
		active_status = masterconf.active_status
		try:
			if file_type == masterconf.modify_entity[0].lower():#inserting record if it is for adding project
				for single_record in payload:
					history_record_from_table = project_master.objects.get(project_id = single_record['to_be_modified'])
					history_record_from_table.record_status = masterconf.record_status_history
					history_record_from_table.save()
					add_dict = {
							"project_id" : single_record["project_id"],
							"record_status" : active_status,
							"created_on" : upload_time,
							"created_by" : username,
							"created_reference_id" : reference_id,
						}
					load_data = project_master(**add_dict)#inserting all the records together in the project_master table and saving the same
					load_data.save()

			elif file_type == masterconf.modify_entity[1].lower():#inserting record if it is for adding bank
				for single_record in payload:
					history_record_from_table = bank_master.objects.get(bank_code = single_record['to_be_modified'])
					history_record_from_table.record_status = masterconf.record_status_history
					history_record_from_table.save()
					add_dict = {
							"bank_code" : single_record["bank_code"],
							"record_status" : active_status,
							"created_on" : upload_time,
							"created_by" : username,
							"created_reference_id" : reference_id,
						}
					load_data = bank_master(**add_dict)#inserting all the records together in the bank_master table and saving the same
					load_data.save()

			elif file_type == masterconf.modify_entity[2].lower():#inserting record if it is for adding region
				print ("Going to modify region name")
				for single_record in payload:
					history_record_from_table = region_master.objects.get(region_name = single_record['to_be_modified'])
					history_record_from_table.record_status = masterconf.record_status_history
					history_record_from_table.save()
					add_dict = {
							"region_name" : single_record["region_name"],
							"record_status" : active_status,
							"created_on" : upload_time,
							"created_by" : username,
							"created_reference_id" : reference_id,
						}
					load_data = region_master(**add_dict)#inserting all the records together in the region_master table and saving the same
					load_data.save()

			elif file_type == masterconf.modify_entity[3].lower():#inserting record if it is for adding region
				print ("Going to modify cra")
				for single_record in payload:
					history_record_from_table = CRA_master.objects.get(cra_id = single_record['to_be_modified'])
					history_record_from_table.record_status = masterconf.record_status_history
					history_record_from_table.save()
					add_dict = {
							"cra_id" : single_record["cra_id"],
							"cra_name" : single_record["cra_name"],
							"record_status" : active_status,
							"created_on" : upload_time,
							"created_by" : username,
							"created_reference_id" : reference_id,
						}
					load_data = CRA_master(**add_dict)#inserting all the records together in the cra_master table and saving the same
					load_data.save()

			self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,active_status, upload_time,username,reference_id)
			status_dict["status_text"] = masterconf.insert_record
			return status_dict
		except Exception as e:
			print("Exception from handle_add_request of Add_Project_Bank_Region_Controller : ",e)
			raise e
