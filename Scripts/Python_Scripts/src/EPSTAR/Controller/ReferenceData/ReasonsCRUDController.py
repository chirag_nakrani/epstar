from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import pyexcel
import xlrd
import time, datetime
#from Controller.Routine import BaseCBRController, BaseCDRController, BaseVCBControllerEOD, BaseC3RMISController, DailyLoad_RepCntrlr
from Controller.Master import ATMMasterController
from Controller.ViewData import BankInfoController
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Model.models import Data_rejection_reasons
from Model.models import Data_overwrite_reasons
from Model.models import Data_update_reasons



class ReasonsCRUDController():

	# This class handles all operations for Rejection,
	# Overwrite and Update Reasons Menus in Reference data

	def handle_rejection_create_request(self,request):

		# This method handles Rejection create operation

		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		status_dict={}
		try:
			c_category = payload_obj['category']
			r_reason = payload_obj['reason']
			s_status = payload_obj['status']
			##Check if active reord exists or not
			is_exists = Data_rejection_reasons.objects.filter(category=c_category).filter(rejection_reason=r_reason).filter(record_status="Active").exists()
			print("is_exists",is_exists)
			if is_exists:
				status_dict['status_code'] = "E106"
				return status_dict
			else:
				## Insert code block
				add_dict = {
					"category" : c_category,
					"rejection_reason" : r_reason,
					"record_status" : s_status,
					"created_on" : upload_time,
					"created_by" : username,
					"created_reference_id" : reference_id
				}
				print("add_dict",add_dict)
				rejection_data = Data_rejection_reasons(**add_dict)
				rejection_data.save()
				status_dict['status_code'] = "S105"
				return status_dict
		except Exception as e:
			raise e

	def handle_rejection_delete_request(self,request):

		# This method handles Rejection delete operation

		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		status_dict = {}
		i_id = payload_obj['id']
		try:
			## check if already exists ... if not throw exception
			is_exists = Data_rejection_reasons.objects.filter(id=i_id).exists()

			## check if record is already deleted
			is_already_deleted = Data_rejection_reasons.objects.filter(id=i_id).filter(record_status='Deleted').exists()
			if not is_exists:
				status_dict['status_code'] = "E107"
				return status_dict
			elif is_already_deleted:
				status_dict['status_code'] = "E108"
				return status_dict
			else:
				del_dict = {
					"record_status" : 'Deleted',
					"deleted_on" : upload_time,
					"deleted_by" : username,
					"deleted_reference_id" : reference_id
				}
				Data_rejection_reasons.objects.filter(id=i_id).update(**del_dict)
				status_dict['status_code'] = "S106"
				print("status_dict",status_dict)
				return status_dict
		except Exception as e:
			#print(e)
			raise e

	def handle_rejection_modify_request(self,request):

		# This method handles Rejection Modify operation

		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		r_reason = payload_obj['reason']
		i_id = payload_obj['id']
		status_dict = {}
		try:
			## check if already exists ... if not throw exception
			is_exists = Data_rejection_reasons.objects.filter(id=i_id).exists()
			is_record_deleted = Data_rejection_reasons.objects.filter(id=i_id).filter(record_status='Deleted').exists()
			print("is_record_deleted",is_record_deleted)
			print("is_exists",is_exists)
			if not is_exists:
				status_dict['status_code'] = "E105"
				print("status_dict", status_dict)
				return status_dict
			elif is_record_deleted:
				status_dict['status_code'] = "E105"
				return status_dict
			else:
				mod_dict = {
					"rejection_reason" : r_reason,
					"modified_on" : upload_time,
					"modified_by" : username,
					"modified_reference_id" : reference_id
				}
				Data_rejection_reasons.objects.filter(id=i_id).filter(record_status='Active').update(**mod_dict)
				status_dict['status_code'] = "S107"
				print("status_dict", status_dict)
				return status_dict
		except Exception as e:
			print(e)
			raise e

	def handle_rejection_read_request(self,request):

		# This method handles Rejection read operation

		try:
			data = Data_rejection_reasons.objects.filter(record_status='Active').values()
			rejection_data = list(data)
			return rejection_data
		except Exception as e:
			raise e

	def handle_write_create_request(self,request):

		# This method handles Overwrite create operation

		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		c_category = payload_obj['category']
		r_reason = payload_obj['reason']
		s_status = payload_obj['status']
		status_dict = {}
		try:
			##Check if active reord exists or not
			is_exists = Data_overwrite_reasons.objects.filter(category=c_category).filter(overwrite_reason=r_reason).filter(record_status="Active").exists()
			print("is_exists", is_exists)
			if is_exists:
				status_dict['status_code'] = "E106"
				return status_dict
			else:
				add_dict = {
					"category" : c_category,
					"overwrite_reason" : r_reason,
					"record_status" : s_status,
					"created_on" : upload_time,
					"created_by" : username,
					"created_reference_id" : reference_id
				}
				print (add_dict)
				write_data = Data_overwrite_reasons(**add_dict)
				write_data.save()
				status_dict['status_code'] = "S105"
				return status_dict
		except Exception as e:
			raise e


###################################################################################################

	def handle_write_delete_request(self,request):

		# This method handles Overwrite delete operation

		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		i_id = payload_obj['id']
		status_dict = {}
		try:
			## check if already exists ... if not throw exception
			is_exists = Data_overwrite_reasons.objects.filter(id=i_id).exists()

			## check if record is already deleted
			is_already_deleted = Data_overwrite_reasons.objects.filter(id=i_id).filter(record_status='Deleted').exists()
			if not is_exists:
				status_dict['status_code'] = "E107"
				return status_dict
			elif is_already_deleted:
				status_dict['status_code'] = "E108"
				return status_dict
			else:
				del_dict = {
					"record_status": 'Deleted',
					"deleted_on": upload_time,
					"deleted_by": username,
					"deleted_reference_id": reference_id
				}
				Data_overwrite_reasons.objects.filter(id=i_id).update(**del_dict)
				status_dict['status_code'] = "S106"
				print("status_dict", status_dict)
				return status_dict
		except Exception as e:
			print(e)
			raise e

	def handle_write_modify_request(self,request):

		# This method handles Overwrite modify operation

		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		r_reason = payload_obj['reason']
		i_id = payload_obj['id']
		status_dict = {}
		try:
			## check if already exists ... if not throw exception
			is_exists = Data_overwrite_reasons.objects.filter(id=i_id).exists()
			is_record_deleted = Data_overwrite_reasons.objects.filter(id=i_id).filter(record_status='Deleted').exists()
			print("is_record_deleted", is_record_deleted)
			print("is_exists", is_exists)
			if not is_exists:
				status_dict['status_code'] = "E105"
				print("status_dict", status_dict)
				return status_dict
			elif is_record_deleted:
				status_dict['status_code'] = "E105"
				return status_dict
			else:
				mod_dict = {
					"overwrite_reason": r_reason,
					"modified_on": upload_time,
					"modified_by": username,
					"modified_reference_id": reference_id
				}
				Data_overwrite_reasons.objects.filter(id=i_id).filter(record_status='Active').update(**mod_dict)
				status_dict['status_code'] = "S107"
				print("status_dict", status_dict)
				return status_dict
		except Exception as e:
			raise e

	def handle_write_read_request(self,request):

		# This method handles Overwrite read operation

		try:
			data = Data_overwrite_reasons.objects.filter(record_status='Active').values()
			overwrite_data = list(data)
			return overwrite_data
		except Exception as e:
			raise e

	def handle_update_create_request(self,request):

		# This method handles Update create operation

		#print ("request : ",request.userinfo['username'].username)
		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		c_category = payload_obj['category']
		r_reason = payload_obj['reason']
		s_status = payload_obj['status']
		status_dict = {}
		try:
			##Check if active reord exists or not
			is_exists = Data_update_reasons.objects.filter(category=c_category).filter(update_reason=r_reason).filter(record_status="Active").exists()
			print("is_exists", is_exists)
			if is_exists:
				status_dict['status_code'] = "E106"
				return status_dict
			else:
				add_dict = {
					"category": c_category,
					"update_reason": r_reason,
					"record_status": s_status,
					"created_on": upload_time,
					"created_by": username,
					"created_reference_id": reference_id
				}
				print(add_dict)
				write_data = Data_update_reasons(**add_dict)
				write_data.save()
				status_dict['status_code'] = "S105"
				return status_dict
		except Exception as e:
			raise e

	def handle_update_delete_request(self,request):

		# This method handles Update delete operation

		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		i_id = payload_obj['id']
		status_dict = {}
		try:
			## check if already exists ... if not throw exception
			is_exists = Data_update_reasons.objects.filter(id=i_id).exists()

			## check if record is already deleted
			is_already_deleted = Data_update_reasons.objects.filter(id=i_id).filter(record_status='Deleted').exists()
			if not is_exists:
				status_dict['status_code'] = "E107"
				return status_dict
			elif is_already_deleted:
				status_dict['status_code'] = "E108"
				return status_dict
			else:
				del_dict = {
					"record_status": 'Deleted',
					"deleted_on": upload_time,
					"deleted_by": username,
					"deleted_reference_id": reference_id
				}
				Data_update_reasons.objects.filter(id=i_id).update(**del_dict)
				status_dict['status_code'] = "S106"
				print("status_dict", status_dict)
				return status_dict
		except Exception as e:
			print(e)
			raise e

	def handle_update_modify_request(self,request):

		# This method handles Update modify operation

		username = request.userinfo['username'].username
		upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
		reference_id = request.userinfo['reference_id']
		payload_obj = request.data['payload']
		r_reason = payload_obj['reason']
		i_id = payload_obj['id']
		status_dict = {}
		try:
			## check if already exists ... if not throw exception
			is_exists = Data_update_reasons.objects.filter(id=i_id).exists()
			is_record_deleted = Data_update_reasons.objects.filter(id=i_id).filter(record_status='Deleted').exists()
			print("is_record_deleted", is_record_deleted)
			print("is_exists", is_exists)
			if not is_exists:
				status_dict['status_code'] = "E105"
				print("status_dict", status_dict)
				return status_dict
			elif is_record_deleted:
				status_dict['status_code'] = "E105"
				return status_dict
			else:
				mod_dict = {
					"update_reason": r_reason,
					"modified_on": upload_time,
					"modified_by": username,
					"modified_reference_id": reference_id
				}
				Data_update_reasons.objects.filter(id=i_id).filter(record_status='Active').update(**mod_dict)
				status_dict['status_code'] = "S107"
				print("status_dict", status_dict)
				return status_dict
		except Exception as e:
			print(e)
			raise e

	def handle_update_read_request(self,request):

		# This method handles Update read operation

		try:
			data = Data_update_reasons.objects.filter(record_status='Active').values()
			update_data = list(data)
			return update_data
		except Exception as e:
			raise e
