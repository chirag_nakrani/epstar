from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from django.db import connection
from Common.models import app_config_param
# from Model import models
import csv
from Common import db_queries_properties
from Common.models import data_update_log_master#we are saving data update log of reference data in data_update_log_master as it is updated according to our development and tracking the data
from django.apps import apps
from datetime import timedelta
from Model.models import brand_bill_capacity


class BrandBillCapacity_Controller():

	## This is the main controller class for BrandBill Capacity File upload

	maintain_log_id = 0

	def handle_reference_upload(self,request,input_filepath):

		# This method will handle all validations regarding brand bill file upload
		print ("Inside handle_reference_upload of BrandBillCapacity_Controller")
		common_util_obj = common_util.common_utils()
		username = request.userinfo['username'].username
		uploaded_status = masterconf.uploaded_state
		file_type = request.POST['file_type'].upper()
		df_data = common_util_obj.read_file_xls(input_filepath)
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = 'F'
		status_dict = {}
		try:
			format_status = common_util_obj.validate_reference_format_xls(df_data, file_type)#Going for format validation of the file
			print("Format_status from BrandBillCapacity_Controller: ", format_status)
			if format_status == masterconf.format_val_success_reference_upload: # If file is valid

				# Adding meta fields to file's data dataframe
				df_enhanced = common_util_obj.add_fields_reference(df_data, uploaded_status, upload_time,username, reference_id,file_type)

				# Logging entry in data_update_log_master table
				self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time, username,reference_id)
				print("After referencelog")

				# Loading data into database
				self.load_brand_bill_capacity_reference_data(df_enhanced,file_type)

				# Validating uploaded data
				data_code = common_util_obj.validate_reference_data(file_type, apiflag, username, reference_id)
				status_dict["status_code"] = data_code
				return status_dict
			else:
				status_dict["status_code"] = format_status
				return status_dict
		except Exception as e:
			print("Exception occured in BrandBillCapacity_Controller ::: ",e)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e

	def load_brand_bill_capacity_reference_data(self, df_enhanced, file_type):

		# This method is used to upload data to the database using django bulk insert
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				data.append(
					brand_bill_capacity(
						brand_code = row['Brand Code'],
						description = row['Description'],
						capacity_50 = row['Capacity 50'],
						capacity_100 = row['Capacity 100'],
						capacity_200 = row['Capacity 200'],
						capacity_500 = row['Capacity 500'],
						capacity_2000 = row['Capacity 2000'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)
			brand_bill_capacity.objects.bulk_create(data)#inserting the data into respective table
		except Exception as e:
			print ("In Exception of load_brand_bill_capacity_reference_data of brandBillCapacity. : ",e)
			raise e
