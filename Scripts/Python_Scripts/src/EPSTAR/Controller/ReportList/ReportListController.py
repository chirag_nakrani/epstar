from Model.models import Reportconfigforui
import logging
from Common import masterconf
import pyodbc

class ReportListController:
    cnxn = pyodbc.connect(masterconf.connection_string)
    cursor = cnxn.cursor()

    def handle_list_req(self, request):

        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_list_req METHOD OF ReportListController.")
        list_returned = list()
        inner_obj = {}
        response_data = []
        try:
            list_db = Reportconfigforui.objects.all().values()
            print(list_db)
            return list_db
        except Exception as e:
            logger.error('EXCEPTION IN handle_list_req METHOD OF ReportListController', exc_info=True)
            logger.error(e)
            raise e

