from Common import masterconf
import uuid
import time, datetime
from Common.CommonFunctions import common_util
from Model.models import holiday_date,holiday_states,holiday_list
from datetime import timedelta
from Common.models import data_update_log_master#we are using this because data_update_log_reference is not updadted and in future it will get eliminated

class HolidayController():
###############This handle will insert new record in the DB #################################################################################
	def handle_holiday_add_data(self,request_data):
		try:
			common_util_obj = common_util.common_utils()
			print ("Inside handle_holiday_add_data of HolidayController for inserting the record: ",request_data)
			# insert_obj_date = {}
			# insert_obj_state = {}
			# insert_obj_name = {}
			status_dict = {}
			record_status_pending_state = masterconf.pending_status
			created_by = request_data.userinfo['username'].username
			created_on = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
			created_reference_id = request_data.userinfo['reference_id']
			payload = request_data.data['payload']

			for data in payload:
				holidaycode_list = holiday_list.objects.filter(holiday_name=data['holiday_name']).filter(record_status='Active').values_list('holiday_code', flat=True)
				print ("holidaycode_list : ", holidaycode_list)
				# holidaycode_filter = holidaycode_list.objects.values_list('holiday_code', flat=True)
				holiday_list_code = list(holidaycode_list)
				print("holiday_list_code : ",holiday_list_code)
				if len(holiday_list_code) != 0:
					holiday_list_code = list(holidaycode_list.values("holiday_code"))[0]
					holiday_update = holiday_list_code['holiday_code']
					for single_record in data['state']:
						add_dict = {
							"holiday_code": holiday_update,
							"state": single_record,
							"record_status": record_status_pending_state,
							"created_on": created_on,
							"created_by": created_by,
							"created_reference_id": created_reference_id,
						}
					load_data = holiday_states(**add_dict)  # inserting all the records together in the default_loading table and saving the same
					load_data.save()
				else:
					insert_obj_name = {}
					insert_obj_date = {}
					userkey = uuid.uuid4()
					holidaycode = 'H' + str(userkey)[:8]
					insert_obj_name["holiday_code"] = holidaycode
					print(data['holiday_name'])
					insert_obj_name["holiday_name"] = data['holiday_name']
					insert_obj_name["created_on"] = created_on
					insert_obj_name["created_by"] = created_by
					insert_obj_name["created_reference_id"] = created_reference_id
					insert_obj_name["record_status"] = record_status_pending_state

					insert_obj_date["holiday_code"] = holidaycode
					insert_obj_date["start_date"] = data['start_date']
					insert_obj_date["end_date"] = data['end_date']
					insert_obj_date["created_on"] = created_on
					insert_obj_date["created_by"] = created_by
					insert_obj_date["created_reference_id"] = created_reference_id
					insert_obj_date["record_status"] = record_status_pending_state

					for state in data['state']:
						add_dict = {
							"holiday_code": holidaycode,
							"state": state,
							"record_status": record_status_pending_state,
							"created_on": created_on,
							"created_by": created_by,
							"created_reference_id": created_reference_id,
						}
						load_data = holiday_states(**add_dict)  # inserting all the records together in the default_loading table and saving the same
						load_data.save()
					holiday_date_obj = holiday_date(**insert_obj_date)
					holiday_date_obj.save()
					print(data['holiday_name'])
					holiday_list_obj = holiday_list(**insert_obj_name)
					holiday_list_obj.save()

			file_type = masterconf.file_type_holiday_master.upper()
			self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,record_status_pending_state, created_on,created_by,created_reference_id)
			status_dict['status_code'] = masterconf.success_holiday_master
			status_dict['data'] = masterconf.data_inserted_successfully
			return status_dict
		except Exception as e:
			print("Exception from handle_defaultLoad_add_data of DefaultLoadController : ",e)
			raise e

	def handle_view_request(self,request):
		print ("m here in Holiday Master View")
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		menu_type = str(request.data['menu']).upper()
######################Extracting uploaded files from data_update_log_master, according to record status from atm_config_table ###########################
		model_name = data_update_log_master
		filters_param = request.data['record_status']
		data = []
		status_dict = {}
		try:
			for i in request.data['record_status']:#to find the no of files uplaoded into tables
				data_list = model_name.objects.filter(record_status=i,data_for_type = menu_type).values('record_status', 'date_created', 'created_by', 'created_reference_id')#from table only filtered records are needed to further operation
				for i in data_list:
					data.append(i)#to collect all the files uploaded in their respective tables
			if len(data) >= 0:
				status_dict["data"] = data
				status_dict["status_text"] = masterconf.masterApiSuccess
				return status_dict
			else:
				status_dict["data"] = []
				status_dict["status_text"] = masterconf.masterApiFailed
				return status_dict
		except Exception as e:
			print ("Exception from AtmConfigLimitsCont : {}".format(e))
			raise e

	def handle_holiday_update_request(self,request):
		print ("m here in handle_holiday_update_request View")
		common_util_obj = common_util.common_utils()
		update_list = request.data['payload']
		update_obj_name = {}
		update_obj_date = {}
		update_obj_states = {}
		apiflag = 'S'
		username = request.userinfo['username'].username
		reference_id = request.userinfo['reference_id']
		print(update_list)
		upload_time = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
		uploaded_status = masterconf.uploaded_state
		status_dict = {}
		try:
			for update_obj in update_list:
				update_obj_date["holiday_code"] = update_obj['holiday_code']
				update_obj_date["start_date"] = update_obj['start_date']
				update_obj_date["end_date"] = update_obj['end_date']
				update_obj_date["created_on"] = upload_time
				update_obj_date["created_by"] = username
				update_obj_date["created_reference_id"] = reference_id
				update_obj_date["record_status"] = masterconf.pending_status


				holiday_date_obj = holiday_date(**update_obj_date)
				holiday_date_obj.save()

			file_type = masterconf.file_type_holiday_master.upper()
			# apiflag = 'S'#there are 2 types of uploading the data i.e. from file or from screen, F=file upload or inserting the record, S = inserting it through screen
			self.maintain_log_id = common_util_obj.maintainlog_reference(file_type, uploaded_status, upload_time,
																		 username, reference_id)
			# status_code = common_util_obj.validate_reference_data(file_type, apiflag, created_by, created_reference_id)#validating individual record if they are correct
			status_dict['status_code'] = masterconf.success_holiday_master
			status_dict['data'] = masterconf.data_inserted_successfully
			return status_dict

		except Exception as e:
			print("Exception from handle_defaultLoad_add_data of DefaultLoadController : ", e)
			raise e