import time, datetime
from Common.CommonFunctions import common_util
from Model.models import indent_eod_pre_activity,indent_pre_qualify_atm,modify_cassette_pre_config,feeder_denomination_pre_availability,feeder_vaulting_pre_config,limitdays,cash_pre_availability,atm_master,holiday_date,holiday_states,holiday_list
from django.contrib.auth.models import User,Group
import logging
from Common.models import data_update_log_master#we are saving data update log of reference data in data_update_log_master as it is updated according to our development and tracking the data
from Common import masterconf
import pandas as pd
from datetime import timedelta
import uuid
from django.db import connection

class ConfigParamController:
    def handle_config_EOD_req(self,request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_config_EOD_req METHOD OF ConfigController.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        try:
            if 'filter_data' in request_obj:
                filters_param = request_obj['filter_data']
                list_db = indent_eod_pre_activity.objects.filter(**filters_param).values()
            else:
                list_db = indent_eod_pre_activity.objects.all().values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_config_EOD_req METHOD OF ConfigController', exc_info=True)
            raise e


    def handle_config_qualify_req(self,request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_config_qualify_req METHOD OF ConfigController.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        try:
            if 'filter_data' in request_obj:
                filters_param = request_obj['filter_data']
                list_db = indent_pre_qualify_atm.objects.filter(**filters_param).values()
            else:
                list_db = indent_pre_qualify_atm.objects.all().values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_config_qualify_req METHOD OF ConfigController', exc_info=True)
            raise e

    def handle_edit_appr_prequalifytatm(self,request):
        request_obj = request.data
        status_dict = {}
        created_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        created_by = request.userinfo['username'].username
        created_reference_id = request.userinfo['reference_id']
        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_edit_appr_prequalifytatm METHOD OF ConfigParamController.")
        try:
            for req in request_obj:
                id = req['id']
                atm_id = req['atm_id']
                prequalify = indent_pre_qualify_atm.objects.get(id=id,atm_id=atm_id)
                prequalify.to_date = req['to_date']
                prequalify.is_qualified = req['is_qualified']
                prequalify.record_status = req['record_status']
                prequalify.created_on = created_on
                prequalify.created_by = created_by
                prequalify.created_reference_id = created_reference_id
                prequalify.save()
            status_dict['status_code'] = masterconf.success
            status_dict['status_text'] = masterconf.success_codes_desc[masterconf.success]
            logger.info("Records successfully updated in db")
            return status_dict
        except Exception as e:
            logger.error("Exception occured while updating records in db : %s",e)
            raise e

    def handle_modify_cassette_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_modify_cassette_req METHOD OF ConfigController.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = modify_cassette_pre_config.objects.filter(**filters_param).values()
            else:
                list_db = modify_cassette_pre_config.objects.all().values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_feeder_denomination_req METHOD OF ConfigController', exc_info=True)
            raise e

    def handle_feeder_denomination_req(self,request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_feeder_denomination_req METHOD OF ConfigController.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = feeder_denomination_pre_availability.objects.filter(**filters_param).values()
            else:
                list_db = feeder_denomination_pre_availability.objects.all().values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_feeder_denomination_req METHOD OF ConfigController', exc_info=True)
            raise e

    def handle_feeder_vaulting_req(self,request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_feeder_vaulting_req METHOD OF ConfigController.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = feeder_vaulting_pre_config.objects.filter(**filters_param).values()
            else:
                list_db = feeder_vaulting_pre_config.objects.all().values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_feeder_vaulting_req METHOD OF ConfigController', exc_info=True)
            raise e

    def handle_limit_days_req(self,request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_limit_days_req METHOD OF ConfigController.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = limitdays.objects.filter(**filters_param).values()
            else:
                list_db = limitdays.objects.all().values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_limit_days_req METHOD OF ConfigController', exc_info=True)
            raise e

    def holiday_name_dropdown(self,request):
        holiday_state = request.data['state']
        try:
            sql_query =  """select distinct list.holiday_name from holiday_states state
                            JOIN holiday_list list
                            on list.holiday_code = state.holiday_code
                            where state.State = '""" + holiday_state + "'"
            cursor = connection.cursor()
            cursor.execute(sql_query)
            list_db = cursor.fetchall()
            data_list = []
            for record in list_db:
                temp_dict = {}
                temp_dict['holiday_name'] = record[0]
                data_list.append(temp_dict)

            status_dict = {}
            status_dict['data'] = data_list
            return data_list
        except Exception as e:
            print(e)
            raise e

    def holiday_master_add(self, request):
        request_obj = request.data
        created_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        created_by = request.userinfo['username'].username
        created_reference_id = request.userinfo['reference_id']
        record_status = masterconf.pending_status
        try:
            holiday_name = request.data['holiday_name']
            state = request.data['state']
            start_date = request.data['start_date']
            end_date = request.data['end_date']
            userkey = uuid.uuid4()
            holidaycode = 'H'+str(userkey)[:8]
            insert_obj_date = {}
            insert_obj_state = {}
            insert_obj_name = {}
            insert_obj_date["holiday_code"] = holidaycode
            insert_obj_date["start_date"] = start_date
            insert_obj_date["end_date"] = end_date
            insert_obj_date["created_on"] = created_on
            insert_obj_date["created_by"] = created_by
            insert_obj_date["created_reference_id"] = created_reference_id
            insert_obj_date["record_status"] = record_status

            insert_obj_state["holiday_code"] = holidaycode
            insert_obj_state["state"] = state
            insert_obj_state["created_on"] = created_on
            insert_obj_state["created_by"] = created_by
            insert_obj_state["created_reference_id"] = created_reference_id
            insert_obj_state["record_status"] = record_status

            insert_obj_name["holiday_code"] = holidaycode
            insert_obj_name["holiday_name"] = holiday_name
            insert_obj_name["created_on"] = created_on
            insert_obj_name["created_by"] = created_by
            insert_obj_name["created_reference_id"] = created_reference_id
            insert_obj_name["record_status"] = record_status


            holiday_date_obj = holiday_date(**insert_obj_date)
            holiday_date_obj.save()


            holiday_list_obj = holiday_list(**insert_obj_name)
            holiday_list_obj.save()


            holiday_state_obj = holiday_states(**insert_obj_state)
            holiday_state_obj.save()
        except Exception as e:
            print(e)
            raise e

    def holiday_state_dropdown(self,request):
        try:
            state_list = list(atm_master.objects.filter(record_status='Active').order_by('state').values('state').distinct())
            print("holiday_state_dropdown state list : ",state_list)
            return state_list
        except Exception as e:
            print("Exception from holiday_state_dropdown ",e)
            raise e


class EODActivityUpdateController:

    def handle_EOD_update_request(self,request):
        common_util_obj = common_util.common_utils()
        update_list = request.data['update']
        # # file_type = request.data['file_type']
        # atm_id = request.data['atm_id']
        # bank = request.data['bank']
        # feeder_branch = request.data['feeder_branch']
        # project_id = request.data['project']
        # region = request.data['region']
        # # cra_custodian = request.data['custodian']
        # # apiflag = 'S'
        username = request.userinfo['username'].username
        reference_id = request.userinfo['reference_id']
        upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        status_dict = {}
        try:
            for update_obj in update_list:
                row_obj = {}
                row_obj['project_id'] = update_obj['project_id']
                row_obj['atm_id' ] = update_obj['atm_id' ]
                row_obj['bank_code'] = update_obj['bank_code']
                row_obj['branch_code'] = update_obj['branch_code']
                row_obj['region_code'] = update_obj['region_code']
                row_obj['record_status'] = 'Approval Pending'
                row_obj['created_on'] = upload_time
                row_obj['created_by'] = username
                row_obj['created_reference_id'] = reference_id
                eod_activity = indent_eod_pre_activity(**row_obj)
                eod_activity.save()

            return_id = eod_activity.id
            print(return_id)
            if return_id:
                status_dict["data"] = masterconf.data_inserted_successfully
                status_dict["status_code"] = masterconf.status_approval_success
                return status_dict
            #data_code = common_util_obj.validate_reference_data(file_type.upper(), apiflag, username, reference_id)
        except Exception as e:
            #print("Exception in ReferenceDataUpdateController ",e)
            raise e

class CashAvailability:
    ### cash pre availability upload from file ###
    def handle_cash_upload(self,request,input_filepath):
        common_util_obj = common_util.common_utils()
        upload_time = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        username = request.userinfo['username'].username
        file_type = request.POST['file_type'].upper()
        df_data = common_util_obj.read_file_xls(input_filepath)
        df_data.drop_duplicates(subset=['project_id', 'bank_code', 'feeder_branch_code'], keep='first', inplace=True)
        reference_id = request.userinfo['reference_id']
        uploaded_status = masterconf.uploaded_status
        status_dict = {}
        apiflag = 'F'
        try:
            format_status = common_util_obj.validate_config_format_xls(df_data, file_type)#Going for format validation of the file
            print("Format_status from Cash Pre Availability: ", format_status)
            if format_status == masterconf.format_val_success:
            ## if columns not present add it to the dataframe with null values #######
                
                df_enhanced = common_util_obj.add_fields_config(df_data, uploaded_status, upload_time,username, reference_id,file_type)
                ###### maintaining log in data log table #########
                self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time,username,reference_id)
                self.load_cash_pre_avalability_data(df_enhanced)
                data_code = common_util_obj.validate_config_data(file_type, apiflag, username, reference_id)
                status_dict["status_code"] = data_code
                if data_code == masterconf.data_val_success_upload:
                    status_dict['data'] = masterconf.data_updated_for_approval
                else:
                    status_dict['data'] = masterconf.data_validated_failed
                print ("status_dict :",status_dict)
                return status_dict
            else:
                status_dict["status_code"] = format_status
                return status_dict

        except Exception as e:
            print("Exception occured in handle_cash_upload of CashAvailability::: ",e)
            entry = data_update_log_master.objects.get(id=self.maintain_log_id)
            entry.record_status = masterconf.failed_status
            entry.save()
            status_dict["level"] = masterconf.data_level
            status_dict["status_code"] = masterconf.cash_success_code
            status_dict["status_text"] = masterconf.cash_success
            return status_dict

    def load_cash_pre_avalability_data(self, df_enhanced):

        # This method is used to upload data to the database using django bulk insert
        df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
        try:
            data = list()
            for i, row in df_enhanced_new.iterrows():
                data.append(
                    cash_pre_availability(
                        project_id = row['project_id'],
                        bank_code = row['bank_code'],
                        feeder_branch_code = row['feeder_branch_code'],
                        applied_to_level = row['applied_to_level'],
                        available_50_amount = row['available_50_amount'],
                        available_100_amount=row['available_100_amount'] ,
                        available_200_amount = row['available_200_amount'],
                        available_500_amount = row['available_500_amount'],
                        available_2000_amount = row['available_2000_amount'],
                        total_amount_available = row['total_amount_available'],
                        from_date = row['from_date'],
                        to_date = row['to_date'],
                        record_status=row['record_status'],
                        created_on=row['created_on'],
                        created_by=row['created_by'],
                        created_reference_id=row['created_reference_id'])
                )
            cash_pre_availability.objects.bulk_create(data)#inserting the data into respective table
        except Exception as e:
            print ("In Exception of load_cash_pre_avalability_data: ",e)
            raise e



    def load_cash_data(self, df_enhanced,maintain_log_id):
        ### function to check existing cash availability data using django model #######

        df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
        status_list = list()
        data_insert = []
        try:
            for i, row in df_enhanced_new.iterrows():
                status_dict = {}
                try:
                    find_cash_availability = cash_pre_availability.objects.get(feeder_branch_code=row['feeder_branch_code'],
                                                                                           bank_code=row['bank_code'])
                    if find_cash_availability:
                        if find_cash_availability.record_status == masterconf.active_status:
                            cash_pre_availability.objects.filter(feeder_branch_code=row['feeder_branch_code'],
                                                                 bank_code=row['bank_code'],
                                                                 record_status=masterconf.active_status). \
                            update(record_status=masterconf.cash_availability_update_status_for_rejected)
                            ### insert in cash availability model ###
                            data_insert.append(self.insert_cash_data(row))
                        elif find_cash_availability.record_status == masterconf.pending_status:
                                status_dict["level"] = masterconf.data_level
                                status_dict["status_code"] = masterconf.cash_existing_code
                                status_dict["status_text"] = masterconf.cash_existing
                                status_list.append(status_dict)
                        else:
                            data_insert.append(self.insert_cash_data(row))
                    else:
                        data_insert.append(self.insert_cash_data(row))
                except Exception as e:
                    data_insert.append(self.insert_cash_data(row))
            cash_pre_availability.objects.bulk_create(data_insert)  # inserting the data into cash table
            entry = data_update_log_master.objects.get(id=maintain_log_id)
            entry.record_status = masterconf.pending_status
            entry.save()
            status_dict = {}
            status_dict["level"] = masterconf.data_level
            status_dict["status_code"] = masterconf.cash_success_code
            status_dict["status_text"] = masterconf.cash_success
            status_list.append(status_dict)
            return status_list
        except Exception as e:
            print ("Exception from load_cash_data :",e)
            status_dict = {}
            status_dict["level"] = masterconf.data_level
            status_dict["status_code"] = masterconf.cash_error_code
            status_dict["status_text"] = masterconf.cash_error
            status_list.append(status_dict)
            return status_list

    def insert_cash_data(self, row):
        ### insert in cash availability model ###
        data = cash_pre_availability(
                from_date=row['from_date'],
                to_date=row['to_date'],
                project_id=row['project_id'],
                bank_code=row['bank_code'],
                applied_to_level = row['applied_to_level'],
                feeder_branch_code=row['feeder_branch_code'],
                available_100_amount=row['available_100_amount'],
                available_200_amount=row['available_200_amount'],
                available_500_amount=row["available_500_amount"],
                available_2000_amount=row["available_2000_amount"],
                total_amount_available=row['total_amount_available'],
                record_status=masterconf.pending_status,
                created_on=row['created_on'],
                created_by=row['created_by'],
                created_reference_id=row['created_reference_id'])
        return data


