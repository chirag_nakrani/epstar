import pandas as pd
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master#we are saving data update log of reference data in data_update_log_master as it is updated according to our development and tracking the data
from datetime import timedelta
from Model.models import feeder_vaulting_pre_config
import datetime

class feeder_vaulting_pre_config_Controller():

    ## This is the main controller class for limit days File upload

    maintain_log_id = 0

    def handle_feeder_vaulting_pre_config_upload(self,request,input_filepath):

        # This method will handle all validations regarding limit days file upload
        print ("Inside handle_feeder_vaulting_pre_config_upload of feeder_vaulting_pre_config_Controller")
        common_util_obj = common_util.common_utils()
        username = request.userinfo['username'].username
        uploaded_status = masterconf.uploaded_state
        file_type = request.POST['file_type'].upper()
        df_data = common_util_obj.read_file_xls(input_filepath)
        df_data.drop_duplicates(subset=['PROJECT', 'BANK CODE', 'FEEDER BRANCH CODE'], keep='first', inplace=True)
        upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
        reference_id = request.userinfo['reference_id']
        apiflag = masterconf.file_upload_flag
        status_dict = {}
        try:
            format_status = common_util_obj.validate_config_format_xls(df_data, file_type)#Going for format validation of the file
            print("Format_status from feeder_vaulting_pre_config_Controller: ", format_status)
            if format_status == masterconf.format_val_success: # If file is valid

                # Adding meta fields to file's data dataframe
                df_enhanced = common_util_obj.add_fields_config(df_data, uploaded_status, upload_time,username, reference_id,file_type)
                print ("df_enhanced : ", df_enhanced)
                # Logging entry in data_update_log_master table
                self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time, username,reference_id)
                print("After referencelog")

                # Loading data into database
                self.feeder_vaulting_pre_config_data(df_enhanced,file_type)
                # Validating uploaded data
                data_code = common_util_obj.validate_config_data(file_type, apiflag, username, reference_id)
                status_dict["status_code"] = data_code
                if data_code == masterconf.data_val_success_upload:
                    status_dict['data'] = masterconf.data_updated_for_approval
                else:
                    status_dict['data'] = masterconf.data_validated_failed
                return status_dict
            else:
                status_dict["status_code"] = format_status
                return status_dict
        except Exception as e:
            print("Exception occured in feeder_vaulting_pre_config_Controller ::: ",e)
            entry = data_update_log_master.objects.get(id=self.maintain_log_id)
            entry.record_status = masterconf.failed_status
            entry.save()
            status_dict["status_code"] = masterconf.fail_to_add_records
            return status_dict


    def feeder_vaulting_pre_config_data(self, df_enhanced, file_type):

        # This method is used to upload data to the database using django bulk insert
        df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
        try:
            data = list()
            for i, row in df_enhanced_new.iterrows():
                data.append(
                    feeder_vaulting_pre_config(
                        project_id = row['PROJECT'],
                        bank_code = row['BANK CODE'],
                        feeder_branch_code=row['FEEDER BRANCH CODE'] ,
                        is_vaulting_allowed = row['IS VAULTING ALLOWED'],
                        vaulting_percentage = row['VAULTING PERCENTAGE'],
                        from_date=row['FROM DATE'],
                        to_date = row['TO DATE'],
                        record_status=row['record_status'] ,
                        created_on=row['created_on'],
                        created_by=row['created_by'],
                        created_reference_id=row['created_reference_id'])
                )
            feeder_vaulting_pre_config.objects.bulk_create(data)#inserting the data into respective table
        except Exception as e:
            print ("In Exception of feeder_vaulting_pre_config_controller of feeder_vaulting_pre_config. : ",e)
            raise e

