import pandas as pd
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master#we are saving data update log of reference data in data_update_log_master as it is updated according to our development and tracking the data
from datetime import timedelta
from Model.models import feeder_cassette_max_capacity_percentage
import datetime

class feeder_capacity_percentage_Controller():

    ## This is the main controller class for BrandBill Capacity File upload

    maintain_log_id = 0

    def handle_config_upload(self,request,input_filepath):

        # This method will handle all validations regarding brand bill file upload
        print ("Inside handle_config_upload of feeder_capacity_percentage_Controller")
        common_util_obj = common_util.common_utils()
        username = request.userinfo['username'].username
        uploaded_status = masterconf.uploaded_state
        file_type = request.POST['file_type'].upper()
        df_data = common_util_obj.read_file_xls(input_filepath)
        df_data.drop_duplicates(subset=['Project', 'Bank Code', 'Feeder Branch Code'], keep='first', inplace=True)
        upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
        reference_id = request.userinfo['reference_id']
        apiflag = masterconf.file_upload_flag
        status_dict = {}
        try:
            format_status = common_util_obj.validate_config_format_xls(df_data, file_type)#Going for format validation of the file
            print("Format_status from feeder_denomination_priority_Controller: ", format_status)
            if format_status == masterconf.format_val_success: # If file is valid

                # Adding meta fields to file's data dataframe
                df_enhanced = common_util_obj.add_fields_config(df_data, uploaded_status, upload_time,username, reference_id,file_type)
                print ("Mandatory Fileds incorporated")

                # Logging entry in data_update_log_master table
                self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time, username,reference_id)
                print("After referencelog")

                # Loading data into database
                self.load_feeder_capacity_percentage_config_data(df_enhanced,file_type)

                # Validating uploaded data
                data_code = common_util_obj.validate_config_data(file_type, apiflag, username, reference_id)
                status_dict["status_code"] = data_code
                return status_dict
            else:
                status_dict["status_code"] = format_status
                return status_dict
        except Exception as e:
            print("Exception occured in feeder_capacity_percentage_Controller ::: ",e)
            entry = data_update_log_master.objects.get(id=self.maintain_log_id)
            entry.record_status = masterconf.failed_status
            entry.save()
            status_dict["status_code"] = masterconf.fail_to_add_records
            print ("status_dict returned from exception of feeder_capacity_percentage_Controller : ",status_dict)
            return status_dict

    def load_feeder_capacity_percentage_config_data(self, df_enhanced, file_type):

        # This method is used to upload data to the database using django bulk insert
        df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
        try:
            data = list()
            for i, row in df_enhanced_new.iterrows():
                data.append(
                    feeder_cassette_max_capacity_percentage(
                        project_id = row['Project'],
                        bank_code = row['Bank Code'],
                        feeder_branch_code = row['Feeder Branch Code'],
                        denomination_100 = row['Denomination 100'],
                        denomination_200=row['Denomination 200'] ,
                        denomination_500 = row['Denomination 500'],
                        denomination_2000 = row['Denomination 2000'],
                        record_status=row['record_status'],
                        created_on=row['created_on'],
                        created_by=row['created_by'],
                        created_reference_id=row['created_reference_id'])
                )
            feeder_cassette_max_capacity_percentage.objects.bulk_create(data)#inserting the data into respective table
        except Exception as e:
            print ("In Exception of feeder_capacity_percentage_Controller: ",e)
            raise e

