import time, datetime
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master
from Model.models import limitdays, feeder_vaulting_pre_config, feeder_denomination_priority, feeder_cassette_max_capacity_percentage, feeder_denomination_pre_availability, feeder_limit_days, bank_limit_days, cash_pre_availability
from datetime import timedelta


class ConfigDataUpdateController():

	# This is the main class for handling reference data update through Screen

	maintain_log_id = 0

	def handle_config_update(self,request):

		#This method is for updating the existing records through screen for Reference Menus -- Brand Bill, CRA Empaneled and Vault Master

		common_util_obj = common_util.common_utils()
		update_list = request.data['update']
		file_type = request.data['file_type'].upper()
		apiflag = 'S'
		username = request.userinfo['username'].username
		reference_id = request.userinfo['reference_id']
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		uploaded_status = masterconf.uploaded_state
		status_dict = {}
		try:

			if file_type == masterconf.limitdays_file_type.upper():
				## Code block for limit days
				for update_obj in update_list:
					row_obj = {}
					row_obj['eps_site_code'] = update_obj['eps_site_code']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['indent_date'] = update_obj['indent_date']
					row_obj['next_feasible_date'] = update_obj['next_feasible_date']
					row_obj['next_working_date'] = update_obj['next_working_date']
					row_obj['decidelimitdays'] = float(update_obj['decidelimitdays'])
					row_obj['loadinglimitdays'] = float(update_obj['loadinglimitdays'])
					row_obj['default_decide_limit_days'] = float(update_obj['default_decide_limit_days'])
					row_obj['default_loading_limit_days'] = float(update_obj['default_loading_limit_days'])
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					bbc = limitdays(**row_obj) #limitdays Model object
					bbc.save()

			elif file_type == masterconf.feeder_vaulting_pre_config_file_type.upper():
				## Code block for feeder_vaulting_pre_config
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['is_vaulting_allowed'] = update_obj['is_vaulting_allowed']
					row_obj['vaulting_percentage'] = update_obj['vaulting_percentage']
					row_obj['from_date'] = update_obj['from_date']
					row_obj['to_date'] = update_obj['to_date']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Feeder_vaulting_pre_config = feeder_vaulting_pre_config(**row_obj) # feeder_vaulting_pre_config Model object
					Feeder_vaulting_pre_config.save()

			elif file_type == masterconf.feeder_denomination_priority_filetype.upper():
				## Code block for feeder_denomination_priority
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['denomination_100'] = update_obj['denomination_100']
					row_obj['denomination_200'] = update_obj['denomination_200']
					row_obj['denomination_500'] = update_obj['denomination_500']
					row_obj['denomination_2000'] = update_obj['denomination_2000']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Feeder_denomination_priority = feeder_denomination_priority(**row_obj) # feeder_denomination_priority Model object
					Feeder_denomination_priority.save()

			elif file_type == masterconf.file_type_fdr_casstt_max_cap_per.upper():
				## Code block for feeder_denomination_priority
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['denomination_100'] = update_obj['denomination_100']
					row_obj['denomination_200'] = update_obj['denomination_200']
					row_obj['denomination_500'] = update_obj['denomination_500']
					row_obj['denomination_2000'] = update_obj['denomination_2000']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Feeder_cassette_max_capacity_percentage = feeder_cassette_max_capacity_percentage(**row_obj) # feeder_cassette_max_capacity_percentage  Model object
					Feeder_cassette_max_capacity_percentage.save()

			elif file_type == masterconf.file_type_feeder_deno_pre_avail.upper():
				## Code block for feeder_denomination_pre_availability
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['is_50_available'] = update_obj['is_50_available']
					row_obj['is_100_available'] = update_obj['is_100_available']
					row_obj['is_200_available'] = update_obj['is_200_available']
					row_obj['is_500_available'] = update_obj['is_500_available']
					row_obj['is_2000_available'] = update_obj['is_2000_available']
					row_obj['from_date'] = update_obj['from_date']
					row_obj['to_date'] = update_obj['to_date']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Feeder_denomination_pre_availability = feeder_denomination_pre_availability(**row_obj) # feeder_denomination_pre_availability Model object
					Feeder_denomination_pre_availability.save()

			elif file_type == masterconf.file_type_bank_limitdays.upper():
				## Code block for feeder_denomination_pre_availability
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['decide_limit_days'] = update_obj['decide_limit_days']
					row_obj['loading_limit_days'] = update_obj['loading_limit_days']
					row_obj['fordate'] = update_obj['fordate']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					load_data = bank_limit_days(**row_obj) # bank_limit_days Model object
					load_data.save()

			elif file_type == masterconf.file_type_feeder_limitdays.upper():
				## Code block for feeder_denomination_pre_availability
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['decide_limit_days'] = update_obj['decide_limit_days']
					row_obj['loading_limit_days'] = update_obj['loading_limit_days']
					row_obj['fordate'] = update_obj['fordate']
					row_obj['feeder'] = update_obj['feeder']
					row_obj['cra'] = update_obj['cra']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					load_data = feeder_limit_days(**row_obj) # feeder_limit_days Model object
					load_data.save()

			elif file_type == masterconf.cash_avail_file_type.upper():
				## Code block for feeder_denomination_pre_availability
				for update_obj in update_list:
					row_obj = {}
					row_obj['project_id'] = update_obj['project_id']
					row_obj['bank_code'] = update_obj['bank_code']
					row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
					row_obj['applied_to_level'] = update_obj['applied_to_level']
					row_obj['atm_id'] = update_obj['atm_id']
					row_obj['available_50_amount'] = update_obj['available_50_amount']
					row_obj['available_100_amount'] = update_obj['available_100_amount']
					row_obj['available_200_amount'] = update_obj['available_200_amount']
					row_obj['available_500_amount'] = update_obj['available_500_amount']
					row_obj['available_2000_amount'] = update_obj['available_2000_amount']
					row_obj['total_amount_available'] = update_obj['total_amount_available']
					row_obj['from_date'] = update_obj['from_date']
					row_obj['to_date'] = update_obj['to_date']
					row_obj['record_status'] = uploaded_status
					row_obj['created_on'] = upload_time
					row_obj['created_by'] = username
					row_obj['created_reference_id'] = reference_id
					Feeder_denomination_pre_availability = cash_pre_availability(**row_obj) # feeder_denomination_pre_availability Model object
					Feeder_denomination_pre_availability.save()

			file_type = file_type.upper()
			# Inserting entry in data_update_log_master table
			self.maintain_log_id = common_util_obj.maintainlog_reference(file_type, uploaded_status, upload_time,username, reference_id)
			apiflag = 'S'
			data_code = common_util_obj.validate_config_data(file_type, apiflag, username, reference_id)
			# print ("data_code : ",data_code )
			# print ("data_code from handle_config_update of ConfigDataUpdateController: ",data_code)
			status_dict["data"] = masterconf.data_inserted_successfully
			status_dict["status_code"] = data_code
			return status_dict
		except Exception as e:
			print("Exception in ConfigDataUpdateController ",e)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = 'Failed'
			entry.save()
			raise e
