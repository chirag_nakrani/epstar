import pandas as pd
from Common.CommonFunctions import common_util
from Common import masterconf
from Common.models import data_update_log_master#we are saving data update log of reference data in data_update_log_master as it is updated according to our development and tracking the data
from datetime import timedelta
from Model.models import feeder_denomination_priority
import datetime

class feeder_denomination_priority_Controller():

	## This is the main controller class for BrandBill Capacity File upload

	maintain_log_id = 0

	def handle_config_upload(self,request,input_filepath):

		# This method will handle all validations regarding brand bill file upload
		print ("Inside handle_config_upload of feeder_denomination_priority_Controller")
		common_util_obj = common_util.common_utils()
		username = request.userinfo['username'].username
		uploaded_status = masterconf.uploaded_state
		file_type = request.POST['file_type'].upper()
		df_data = common_util_obj.read_file_xls(input_filepath)
		df_data.drop_duplicates(subset=['Project', 'Bank Code', 'Feeder Branch Code'], keep='first', inplace=True)
		upload_time = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
		reference_id = request.userinfo['reference_id']
		apiflag = masterconf.file_upload_flag
		status_dict = {}
		try:
			format_status = common_util_obj.validate_config_format_xls(df_data, file_type)#Going for format validation of the file
			print("Format_status from feeder_denomination_priority_Controller: ", format_status)
			if format_status == masterconf.format_val_success: # If file is valid

				# Adding meta fields to file's data dataframe
				df_enhanced = common_util_obj.add_fields_config(df_data, uploaded_status, upload_time,username, reference_id,file_type)

				# Logging entry in data_update_log_master table
				self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,uploaded_status, upload_time, username,reference_id)
				print("After referencelog")

				# Loading data into database
				self.load_feeder_denomination_priority_reference_data(df_enhanced,file_type)

				# Validating uploaded data
				data_code = common_util_obj.validate_config_data(file_type, apiflag, username, reference_id)
				status_dict["status_code"] = data_code
				if data_code == masterconf.data_val_success_upload:
					status_dict['data'] = masterconf.data_updated_for_approval
				else:
					status_dict['data'] = masterconf.data_validated_failed
				return status_dict
			else:
				status_dict["status_code"] = format_status
				return status_dict
		except Exception as e:
			print("Exception occured in feeder_denomination_priority_Controller ::: ",e)
			entry = data_update_log_master.objects.get(id=self.maintain_log_id)
			entry.record_status = masterconf.failed_status
			entry.save()
			status_dict["status_code"] = masterconf.fail_to_add_records
			return status_dict

	def handle_feeder_denomination_add_data(self,request_data):
		try:
			common_util_obj = common_util.common_utils()
			print ("Inside handle_feeder_denomination_add_data of feeder_denomination_priority for inserting the record: ",request_data)
			created_by = request_data.userinfo['username'].username
			created_on = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
			created_reference_id = request_data.userinfo['reference_id']
			payload = request_data.data['payload']
			status_dict = {}
			record_status_uploaded_state = masterconf.uploaded_state
			file_type = request_data.data['file_type'].upper()
			for single_record in payload:
				add_dict = {
						"project_id" : single_record["project_id"],
						"bank_code" : single_record["bank_code"],
						"feeder_branch_code" : single_record["feeder_branch_code"],
                        "denomination_100" : single_record["denomination_100"],
                        "denomination_200" : single_record["denomination_200"],
						"denomination_500" : single_record["denomination_500"],
						"denomination_2000" : single_record["denomination_2000"] ,
						"record_status" : record_status_uploaded_state,
						"created_on" : created_on,
						"created_by" : created_by,
						"created_reference_id" : created_reference_id,
					}
				load_data = feeder_denomination_priority(**add_dict)#inserting all the records together in the cypher code table and saving the same
				load_data.save()

			apiflag = masterconf.file_upload_flag#there are 2 types of uploading the data i.e. from file or from screen, F=file upload or inserting the record, S = inserting it through screen
			self.maintain_log_id = common_util_obj.maintainlog_reference(file_type,record_status_uploaded_state, created_on,created_by,created_reference_id)
			status_code = common_util_obj.validate_config_data(file_type, apiflag, created_by, created_reference_id)#validating individual record if they are correct
			print ("data_code returned from validate_config_data for inserting new record: ",status_code)
			if status_code == masterconf.data_val_success_mstr_upload:#if record is inserted successfully
				status_dict['status_code'] = status_code
				status_dict["data"] = masterconf.data_updated_for_approval
			else:#if record is failed to insert
				status_dict['status_code'] = status_code
				status_dict["data"] = masterconf.data_insert_modify_failed
			print ("status_dict returned from handle_feeder_denomination_add_data when record is added : ",status_dict)#debugging purpose about type of response it is returning
			return status_dict
		except Exception as e:
			print("Exception from handle_feeder_denomination_add_data of DefaultLoadController : ",e)
			status_dict['status_code'] = status_code
			status_dict["data"] = masterconf.data_insert_modify_failed
			return status_dict


	def load_feeder_denomination_priority_reference_data(self, df_enhanced, file_type):

		# This method is used to upload data to the database using django bulk insert
		df_enhanced_new = df_enhanced.where(pd.notnull(df_enhanced), None)
		try:
			data = list()
			for i, row in df_enhanced_new.iterrows():
				data.append(
					feeder_denomination_priority(
						project_id = row['Project'],
						bank_code = row['Bank Code'],
						feeder_branch_code = row['Feeder Branch Code'],
						denomination_100 = row['Denomination 100'],
						denomination_200=row['Denomination 200'] ,
						denomination_500 = row['Denomination 500'],
						denomination_2000 = row['Denomination 2000'],
						record_status=row['record_status'],
						created_on=row['created_on'],
						created_by=row['created_by'],
						created_reference_id=row['created_reference_id'])
				)
			feeder_denomination_priority.objects.bulk_create(data)#inserting the data into respective table
		except Exception as e:
			print ("In Exception of feeder_denomination_priority_Controller of feeder_denomination_priority. : ",e)
			raise e

