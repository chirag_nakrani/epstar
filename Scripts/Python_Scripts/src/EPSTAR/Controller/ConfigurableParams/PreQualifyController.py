from Common import masterconf
from Model.models import indent_pre_qualify_atm



class PreQualifyController():

    # This is the main class for handling reference data update through Screen

    maintain_log_id = 0

    def handle_modified_request(self,request):

        #This method is for updating the existing records through screen for indent pre qulaify atms

        update_list = request.data['modified_records']
        status_dict = {}
        id_list = []
        try:
            for update_obj in update_list:
                row_obj = {}
                single_object_id = update_obj['id']
                row_obj['atm_id'] = update_obj['atm_id']
                row_obj['project_id'] = update_obj['project_id']
                row_obj['bank_code'] = update_obj['bank_code']
                row_obj['feeder_branch_code'] = update_obj['feeder_branch_code']
                row_obj['site_code'] = update_obj['site_code']
                row_obj['category'] = update_obj['category']
                row_obj['is_qualified'] = update_obj['is_qualified']
                row_obj['from_date'] = update_obj['from_date']
                row_obj['to_date'] = update_obj['to_date']
                row_obj['comment'] = update_obj['comment']
                indent_pre_qualify_atm.objects.filter(created_reference_id = request.data["created_reference_id"], id = single_object_id).update(**row_obj)
                id_list.append(single_object_id)
            status_dict["data"] = masterconf.data_updated_for_approval
            status_dict["id_list"] = id_list
            return status_dict
        except Exception as e:
            print("Exception in PreQualifyController ",e)
            raise e
