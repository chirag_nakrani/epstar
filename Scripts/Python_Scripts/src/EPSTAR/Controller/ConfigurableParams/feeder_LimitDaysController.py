from Common.CommonFunctions import common_util
from Common import masterconf
from datetime import timedelta
from Model.models import feeder_limit_days
import datetime

class feeder_LimitDaysController():
	def handle_feederLimitDays_add_data(self,request):
		try:
			common_util_obj = common_util.common_utils()
			created_by = request.userinfo['username'].username
			created_on = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
			created_reference_id = request.userinfo['reference_id']
			payload = request.data['records']
			status_dict = {}
			record_status_uploaded_state = masterconf.uploaded_state
			file_type = request.data['file_type'].upper()
			for single_record in payload:
				add_dict = {
						"project_id" : single_record["project_id"],
						"bank_code" : single_record["bank_code"],
						"decide_limit_days" : single_record["decide_limit_days"],
                        "loading_limit_days" : single_record["loading_limit_days"],
                        "feeder" : single_record["feeder"],
                        "cra" : single_record["cra"] ,
                        "fordate" : single_record["fordate"],
						"record_status" : record_status_uploaded_state,
						"created_on" : created_on,
						"created_by" : created_by,
						"created_reference_id" : created_reference_id,
					}
				load_data = feeder_limit_days(**add_dict)#inserting all the records together in the cypher code table and saving the same
				load_data.save()

			apiflag = 'S'#there are 2 types of uploading the data i.e. from file or from screen, F=file upload or inserting the record, S = inserting it through screen
			common_util_obj.maintainlog_master(file_type,record_status_uploaded_state, created_on,created_by,created_reference_id)
			status_code = common_util_obj.validate_config_data(file_type, apiflag, created_by, created_reference_id)#validating individual record if they are correct
			print ("data_code returned from validate_config_data for inserting new record: ",status_code)
			if status_code == masterconf.data_val_success_mstr_upload:#if record is inserted successfully
				status_dict['status_code'] = status_code
				status_dict["data"] = masterconf.data_updated_for_approval
			else:#if record is failed to insert
				status_dict['status_code'] = status_code
				status_dict["data"] = masterconf.data_insert_modify_failed
			print ("status_dict returned from handle_defaultLoad_add_data when record is added : ",status_dict)#debugging purpose about type of response it is returning
			return status_dict
		except Exception as e:
			print("Exception from handle_defaultLoad_add_data of DefaultLoadController : ",e)
			raise e