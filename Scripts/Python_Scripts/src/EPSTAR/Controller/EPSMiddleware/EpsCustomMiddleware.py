from django.http import HttpResponse
from django.utils.deprecation import MiddlewareMixin
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.serializers import VerifyJSONWebTokenSerializer
from rest_framework.exceptions import ValidationError
from Model.models import user_project_mapping, user_bank_mapping, MetaDataTable, generate_ref_id, \
    user_bank_project_mapping, api_call_log
from django.contrib.auth.models import User, Group, Permission
import json
import datetime,time
import logging


class EPSMiddleware(MiddlewareMixin):

    def process_request(self, request):
        logger = logging.getLogger(__name__)
        logger.info("INSIDE process_request METHOD OF EPSMiddleware.")
        if request.path.startswith('/login/'):
            return None
        elif 'HTTP_AUTHORIZATION' in request.META and request.META['HTTP_AUTHORIZATION'] != '' and request.META[
            'HTTP_AUTHORIZATION'] != None:
            userinfodict = {}
            token = request.META.get('HTTP_AUTHORIZATION', " ").split(' ')[1]
            data = {'token': token}
            bank_list = list()
            final_bank_list = list()
            project_list = list()
            final_project_list = list()
            try:
                logger.info('Request Path: %s',request.get_full_path)
                valid_data = VerifyJSONWebTokenSerializer().validate(data)
                user = valid_data['user']
                user_id = User.objects.filter(username=user).values('id')[0]['id']
                all_groups = Group.objects.filter(user=user)
                bank_list = list(user_bank_project_mapping.objects.filter(user_id=user_id).values())
                project_list = list(user_bank_project_mapping.objects.filter(user_id=user_id).values())
                if len(bank_list):
                    for bank_obj in bank_list:
                        final_bank_list.append(bank_obj['bank_code'])
                if len(project_list):
                    for project_obj in project_list:
                        final_project_list.append(project_obj['project_id'])
                reference_id = generate_ref_id()
                request.userinfo = userinfodict
                request.userinfo['username'] = user
                request.userinfo['bank_list'] = final_bank_list
                request.userinfo['project_list'] = final_project_list
                request.userinfo['group'] = all_groups
                request.userinfo['reference_id'] = reference_id
                get_data = dict(request.GET)
                post_data = dict(request.POST)
                insert_req = {
                    "request_api_url": request.path,
                    "request_method": request.method,
                    "request_user_info": user,
                    "request_get_info": get_data,
                    "request_post_info": post_data,
                    "reference_id":reference_id,
                    "request_date":datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                }
                insert_api_obj = api_call_log(**insert_req)
                insert_api_obj.save()
            except Exception as e:
                raise e
