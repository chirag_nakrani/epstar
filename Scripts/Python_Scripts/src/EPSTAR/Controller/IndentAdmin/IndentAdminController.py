from Common.CommonFunctions import common_util
import logging
from Common import masterconf, db_queries_properties
from django.db import connection
import json
import pandas as pd
import pyodbc
import datetime
import time
from django.contrib.auth.models import User
from Model.models import user_bank_project_mapping

class IndentAdminController():

    def handle_indent_admin_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_indent_admin_req METHOD OF IndentAdminController.")
        # status_dict ={}
        try:
            #### left join query to get which feeder branch pdf is generated and mail is sent or not #####
            # sql_query = db_queries_properties.admin_query % request.data['bank']
            filters = []

            if not request.data:
                sql_query = db_queries_properties.admin_query_default
                user_id = User.objects.get(username=request.userinfo['username'].username).id
                user_bank = None
                try:
                    user_bank = user_bank_project_mapping.objects.filter(user_id=user_id).values()
                except Exception as e:
                    logger.error('EXCEPTION IN handle_indent_admin_req METHOD OF IndentAdminController', exc_info=True)
                    logger.error('No bank mapped')
                bank_code = []
                for i in user_bank:
                    bank_code.append("'"+i['bank_code']+"'")
                bank_str = ", ".join(bank_code)
                if user_bank:
                    filters.append(" AND f.bank_code IN (%s)" % bank_str)
            else:
                sql_query = db_queries_properties.admin_query
                if 'indent_order_number' in request.data:
                    filters.append(" AND i.indent_order_number='%s'" % request.data['indent_order_number'])
                if 'indent_short_code' in request.data:
                    filters.append(" AND i.indent_short_code='%s'" % request.data['indent_short_code'])
                if 'indent_order_date' in request.data:
                    filters.append(" AND i.order_date='%s'" % request.data['indent_order_date'])
                if 'project_id' in request.data:
                    filters.append(" AND f.project_id='%s'" % request.data['project_id'])
                if 'feeder_branch' in request.data:
                    filters.append(" AND f.feeder_branch='%s'" % request.data['feeder_branch'])
                if 'pdf_status' in request.data:
                    filters.append(" AND p.record_status='%s'" % request.data['pdf_status'])
                if 'mail_status' in request.data:
                    filters.append(" AND m.record_status='%s'" % request.data['mail_status'])
                if 'bank_code' in request.data:
                    filters.append(" AND f.bank_code='%s'" % request.data['bank_code'])

            sql_query += "".join(filters)

            df = pd.read_sql(sql_query, pyodbc.connect(masterconf.connection_string)).fillna('')
            status_list = list()
            #### creating dictionary based on requirements to display in UI #####
            for index,row in df.iterrows():
                data_dict = {}
                data_dict['bank_code'] = row['bank_code']
                data_dict['project_id'] = row['project_id']
                data_dict['feeder_branch'] = row['feeder_branch']
                data_dict['cra'] = row['cra']
                data_dict['indent_short_code'] = row['indent_short_code']
                data_dict['indent_order_number'] = row['indent_order_number']
                data_dict['indent_date'] = row['created_on']
                data_dict['id'] = row['email_indent_mail_master_id']
                data_dict['record_status'] = row['indent_status']
                data_dict['pdf_status'] = row['pdf_status']
                data_dict['mail_status'] = row['mail_status']
                status_list.append(data_dict)

            # status_dict['status_code'] = masterconf.indent_admin_success
            # status_dict['status_text'] = masterconf.indent_admin_success_text
            return status_list

        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_indent_admin_req METHOD OF SystemSettingsController', exc_info=True)
            raise e
