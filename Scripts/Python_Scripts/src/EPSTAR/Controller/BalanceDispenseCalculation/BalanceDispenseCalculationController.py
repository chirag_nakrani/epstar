import datetime
from Common.CommonFunctions import common_util
from Common import masterconf
import logging
from Common.models import data_update_log

class BalanceDispenseCalculationController():

	## This is the main controller class for calculating morning balance, average dispense and default dispense

	def handle_calculation_req(self,request):

		# This method will handle all validations regarding morning balance, average dispense and default dispense calculation
		common_util_obj = common_util.common_utils()
		logger = logging.getLogger(__name__)
		username = request.userinfo['username'].username
		file_type = request.data['file_type']
		# indent_date = datetime.datetime.now().strftime('%Y-%m-%d')
		reference_id = request.userinfo['reference_id']
		status_dict = {}
		logger.info('Inside handle_calculation_req method of BalanceDispenseCalculationController.')
		logger.info('Request parameters received: %s',request.data)
		try:
			bank_code = ",".join(request.data['bank_code'])
			if file_type == masterconf.morning_balance_type:
				indent_date = request.data['indent_date']
				morning_balance_status = common_util_obj.calculate_morning_balance(indent_date, request.data['project_id'], bank_code,reference_id, username)
				status_dict['status_code'] = morning_balance_status
				if morning_balance_status == masterconf.calculation_success_code:
					status_dict['status_text'] = request.data['file_type'] + masterconf.calculation_success
				elif morning_balance_status == masterconf.calculation_error_code:
					status_dict['status_text'] = request.data['file_type'] + masterconf.calculation_error
				elif morning_balance_status == masterconf.calculation_no_records_code:
					status_dict['status_text'] = request.data['file_type'] + masterconf.no_records_error_message
				else:
					status_dict['status_text'] = request.data['file_type'] + masterconf.error_message


			elif file_type == masterconf.default_dispense_type:
				indent_date = request.data['indent_date']
				default_dispense_status = common_util_obj.calculate_default_dispense(indent_date, bank_code, reference_id, username)
				status_dict['status_code'] = default_dispense_status
				if default_dispense_status  == masterconf.calculation_success_code:
					status_dict['status_text'] = request.data['file_type'] + masterconf.calculation_success
				elif default_dispense_status == masterconf.calculation_error_code:
					status_dict['status_text'] = request.data['file_type'] + masterconf.calculation_error
				elif default_dispense_status == masterconf.calculation_no_records_code:
					status_dict['status_text'] = request.data['file_type'] + masterconf.no_records_error_message
				else:
					status_dict['status_text'] = request.data['file_type'] + masterconf.error_message

			elif file_type == masterconf.dispense_type:
				cbr_t_minus_1 = request.data['cbr_t_minus_1']
				cbr_t_minus_2 = request.data['cbr_t_minus_2']
				datafor_date_time = request.data['datafor_date_time']
				c3r_date = request.data['c3r_date']
				bank_code = request.data['bank_code']
				project_id = request.data['project_id']
				dispense_status = common_util_obj.calculate_dispense(datafor_date_time,c3r_date,cbr_t_minus_2,cbr_t_minus_1,bank_code,project_id,username,reference_id)
				status_dict['status_code'] = dispense_status
				if dispense_status  == masterconf.morning_t_minus_1_missing_code:
					status_dict['status_text'] = masterconf.morning_t_minus_1_missing_text
				elif dispense_status == masterconf.morning_t_minus_1_error_code:
					status_dict['status_text'] = masterconf.morning_t_minus_1_error_text
				elif dispense_status == masterconf.morning_t_minus_2_missing_code:
					status_dict['status_text'] = masterconf.morning_t_minus_2_missing_text
				elif dispense_status == masterconf.morning_t_minus_2_error_code:
					status_dict['status_text'] = masterconf.morning_t_minus_2_error_text
				elif dispense_status == masterconf.C3R_entry_missing_code:
					status_dict['status_text'] = masterconf.C3R_entry_missing_text
				elif dispense_status == masterconf.C3R_error_code:
					status_dict['status_text'] = masterconf.C3R_error_text
				elif dispense_status == masterconf.dispense_success_code:
					status_dict['status_text'] = masterconf.dispense_success_text
				elif dispense_status == masterconf.dispense_error_code:
					status_dict['status_text'] = masterconf.dispense_error_text
				else:
					status_dict['status_text'] = request.data['file_type'] + masterconf.error_message
			else:
				status_dict["status_code"] = masterconf.error_code
				status_dict["status_text"] = masterconf.improper_file_type
			return status_dict
		except Exception as e:
			logger.error("Exception occured in handle_calculation_req ::: %s",e)
			raise e

	## This is the controller class for fetching data for dispense file uploads

	def handle_data_req(self,request):

		logger = logging.getLogger(__name__)
		file_type = request.data['file_type']
		# indent_date = datetime.datetime.now().strftime('%Y-%m-%d')
		status_dict = {}
		logger.info('Inside handle_data_req method of BalanceDispenseCalculationController.')
		logger.info('Request parameters received: %s',request.data)
		try:
			if file_type == masterconf.dispense_type:
				data_dict = {}
				t_date = request.data['datafor_date_time']
				current_date = datetime.datetime.strptime(t_date,'%Y-%m-%d')
				# t_minus_2 = datetime.datetime.strptime(t_date,
				# 									   '%Y-%m-%d') - datetime.timedelta(days=2)
				t_plus_1 = datetime.datetime.strptime(t_date,'%Y-%m-%d') + datetime.timedelta(days=1)
				try:
					dispense_data_c3r = data_update_log.objects.filter(datafor_date_time__date=current_date,data_for_type=masterconf.file_type_C3R,record_status=masterconf.active_status).values('datafor_date_time')
					if dispense_data_c3r:
						data_dict['c3r_date'] = datetime.datetime.strftime(dispense_data_c3r[0]['datafor_date_time'],
																	   '%Y-%m-%d')
					else:
						status_dict['status_c3r_text'] = masterconf.no_record + ' for c3r for bank ' + request.data[
							'bank_code']
						status_dict['status_c3r_code'] = masterconf.api_status_Failure
				except Exception as e:
					logger.error('Exception occured in finding c3r data for bank: %s',request.data['bank_code'])
					logger.error(e)
					status_dict['status_c3r_text'] = masterconf.no_record + ' for c3r for bank ' + request.data['bank_code']
					status_dict['status_c3r_code'] = masterconf.api_status_Failure
				try:
					dispense_data_cbr_1 = data_update_log.objects.filter(bank_code=request.data['bank_code'],
																   datafor_date_time__date=current_date,data_for_type=masterconf.file_type_cbr,record_status=masterconf.active_status).values('datafor_date_time')
					if dispense_data_cbr_1:
						data_dict['cbr_1'] = []
						for data_1 in dispense_data_cbr_1:
							data_dict['cbr_1'].append(datetime.datetime.strftime(data_1['datafor_date_time'],
																				 '%Y-%m-%d %H:%M:%S.%f'))
					else:
						status_dict['status_cbr_1_code'] = masterconf.api_status_Failure
						status_dict['status_cbr_1_text'] = masterconf.no_record + ' for cbr for bank ' + request.data[
							'bank_code'] + ' for date ' + datetime.datetime.strftime(current_date,'%Y-%m-%d')
				except Exception as e:
					logger.error('Exception occured in finding cbr data for bank %s for date %s',
								 (request.data['bank_code'], datetime.datetime.strftime(current_date,'%Y-%m-%d')))
					logger.error(e)
					status_dict['status_cbr_1_code'] = masterconf.api_status_Failure
					status_dict['status_cbr_1_text'] = masterconf.no_record + ' for cbr for bank ' + request.data['bank_code'] + ' for date ' + datetime.datetime.strftime(current_date,'%Y-%m-%d')
				try:
					dispense_data_cbr_2 = data_update_log.objects.filter(bank_code=request.data['bank_code'],
																	 datafor_date_time__date=t_plus_1,
																	 data_for_type=masterconf.file_type_cbr,record_status=masterconf.active_status).values(
					'datafor_date_time')
					if dispense_data_cbr_2:
						data_dict['cbr_2'] = []
						for data_2 in dispense_data_cbr_2:
							data_dict['cbr_2'].append(datetime.datetime.strftime(data_2['datafor_date_time'],
																				 '%Y-%m-%d %H:%M:%S.%f'))
					else:
						status_dict['status_cbr_2_code'] = masterconf.api_status_Failure
						status_dict['status_cbr_2_text'] = masterconf.no_record + ' for cbr for bank ' + request.data[
							'bank_code'] + ' for date ' + datetime.datetime.strftime(t_plus_1,'%Y-%m-%d')
				except Exception as e:
					logger.error('Exception occured in finding cbr data for bank %s for date %s', (request.data['bank_code'],datetime.datetime.strftime(t_plus_1,'%Y-%m-%d')))
					logger.error(e)
					status_dict['status_cbr_2_code'] = masterconf.api_status_Failure
					status_dict['status_cbr_2_text'] = masterconf.no_record + ' for cbr for bank ' + request.data[
						'bank_code'] + ' for date ' + datetime.datetime.strftime(t_plus_1,'%Y-%m-%d')
					return status_dict
				status_dict['data'] = data_dict
				status_dict['status_code'] = masterconf.api_status_Success
			else:
				status_dict["status_code"] = masterconf.error_code
				status_dict["status_text"] = masterconf.improper_file_type
			return status_dict
		except Exception as e:
			logger.error("Exception occured in handle_data_req ::: %s",e)
			raise e
