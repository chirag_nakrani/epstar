import time, datetime
from Common.CommonFunctions import common_util
from Model.models import indent_master, indent_detail, indent_revision_request, distribution_planning_detail_V2, \
    distribution_planning_master_V2, indent_holiday
from django.contrib.auth.models import User, Group
import logging
from Common import masterconf, db_queries_properties
from django.apps import apps
from django.db import connection
from Common.models import data_update_log
from num2words import num2words
from django.db.models import Q
import pyodbc
from Controller.IndentPDF import PDFController

class IndentController:

    def handle_indent_list_req(self, request):

        # This method is used to read indent and indent revision records as a list
        # (indent_master, indent_revision_request model)

        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_indent_list_req METHOD OF IndentController.")
        request_obj = request.data
        list_returned = list()
        inner_obj = {}
        try:
            table_name_key = request_obj["indent_type"]
            print("table_name_key ::: ",table_name_key)
            if table_name_key in db_queries_properties.indent_table_dict.keys():
                model_name_str = db_queries_properties.indent_table_dict[table_name_key]
                model_name = apps.get_model(app_label='Model', model_name=model_name_str)
            else:
                model_name = apps.get_model(app_label='Model', model_name='indent_master')
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                print(" filters_param :::: ", filters_param)
                feeder_list = []
                if table_name_key == 'indent_revision':
                    print("model_name  ::: ",model_name)
                    if 'created_on' in filters_param and 'feeder_branch_code' in filters_param:
                        feeder_list = filters_param['feeder_branch_code']
                        created_on_filter = filters_param['created_on']
                        del filters_param['feeder_branch_code']
                        del filters_param['created_on']
                        list_db = model_name.objects.filter(**filters_param).filter(feeder_branch_code__in=feeder_list).filter(created_on__date=created_on_filter).values()
                    elif 'feeder_branch_code' in filters_param:
                        feeder_list = filters_param['feeder_branch_code']
                        del filters_param['feeder_branch_code']
                        list_db = model_name.objects.filter(**filters_param).filter(feeder_branch_code__in=feeder_list).values()
                    elif 'created_on' in filters_param:
                        created_on_filter = filters_param['created_on']
                        del filters_param['created_on']
                        list_db = model_name.objects.filter(**filters_param).filter(created_on__date=created_on_filter).values()
                    else:
                        list_db = model_name.objects.filter(**filters_param).values()
                else:
                    if 'feeder_branch' in filters_param:
                        feeder_list = filters_param['feeder_branch']
                        del filters_param['feeder_branch']
                        list_db = model_name.objects.filter(**filters_param).filter(feeder_branch__in=feeder_list).values()
                    else:
                        list_db = model_name.objects.filter(**filters_param).values()
            else:
                list_db = model_name.objects.all().values()
            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            print(e)
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_indent_list_req METHOD OF IndentController', exc_info=True)
            raise e

    def handle_indent_detail_req(self, request):

        # This method is used to read detail records for a partricular indent order no (indent_detail model)

        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_indent_detail_req METHOD OF IndentController.")
        indent_order_no = request.data['indent_order_no']
        try:
            if 'filters' in request.data.keys():
                filters_params = {}
                filters_params = request.data['filters']
                filters_params['indent_order_number'] = indent_order_no
                indtmastr = list(indent_master.objects.filter(**filters_params).all().values(*masterconf.indent_mstr_args_list))
                indtdetail = list(indent_detail.objects.filter(**filters_params).all().values(*masterconf.indent_detail_args_list))
            else:
                indtmastr = list(indent_master.objects.filter(indent_order_number=indent_order_no).all().values(*masterconf.indent_mstr_args_list))
                indtdetail = list(indent_detail.objects.filter(indent_order_number=indent_order_no).all().values(*masterconf.indent_detail_args_list))
            distribution_master = distribution_planning_master_V2.objects.filter(indent_code=indent_order_no).all().values(*masterconf.distri_mstr_args_list)
            for record in indtdetail:
                atm_id = record['atm_id']
                distrilist = list(
                    distribution_planning_detail_V2.objects.filter(atm_id=atm_id, indent_code=indent_order_no).all().values(*masterconf.distri_detail_args_list))
                if distrilist:
                    record['distribution_details'] = distrilist[0]
                else:
                    record['distribution_details'] = {}
            indtmastr[0]['indent_detail'] = indtdetail
            indtmastr[0]['distribution_master'] = distribution_master
            if 'required_fields' in request.data.keys():
                rqrd_field_resp = {key: indtmastr[0][key] for key in request.data['required_fields']}
                rqrd_field_resp['indent_detail'] = indtdetail
                rqrd_field_resp['distribution_master'] = distribution_master
                return rqrd_field_resp
            return indtmastr[0]
        except Exception as e:
            # print(e)
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_indent_detail_req METHOD OF IndentController', exc_info=True)
            raise e

    def generate_indent_revision_code(self, temp_obj):

        # This method is used to generate indent code (to be removed)

        bank_code = temp_obj["bank"]
        feeder_branch_code = temp_obj["feeder_branch"]
        cra = temp_obj["cra"]
        indent_date = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d%H%M%S')
        indent_revision_code = str(str(bank_code) + "/" + str(feeder_branch_code) + "/" + str(cra) + "/" + indent_date)
        return indent_revision_code

    def generate_indent_revision(self, request, temp_obj, initiation_flag):

        # This method is used to generate the indent revision. i.e. Call the indent revision procedure

        indent_date = datetime.datetime.fromtimestamp(time.time()).strftime(
            '%Y-%m-%d')  # '2018-11-22' #request.data["indent_revision_date"]
        print("indent_date :: ", str(indent_date))
        is_Indent = 0
        is_Revision = 1
        print(" temp_obj.keys()  ::: ", temp_obj.keys())
        active_indent_code = temp_obj["indent_order_number"]
        project_Id_for_rev = temp_obj["project_id"]
        if initiation_flag:
            rev_bankcode = temp_obj["bank"]
            feederbranch = temp_obj["feeder_branch"]
        else:
            rev_bankcode = temp_obj["bank_code"]
            feederbranch = temp_obj["feeder_branch_code"]
        print(" project_Id_for_rev  ::: ", project_Id_for_rev)
        print(" rev_bankcode  ::: ", rev_bankcode)
        created_by = request.userinfo['username'].username
        created_reference_id = request.userinfo['reference_id']
        sql_str = " DECLARE @out varchar(10) exec usp_indent_calculation_V3 '{}', '{}', '{}', '{}', '{}', '{}', 'Yes', '{}', @out OUTPUT SELECT @out".format(indent_date,created_by,created_reference_id,project_Id_for_rev,rev_bankcode,feederbranch,active_indent_code)
        print("REVISION : sql_str from usp_indent_calculation_V3 : {}".format(sql_str))
        try:
            nocount = """ SET NOCOUNT ON; """
            cur = connection.cursor()
            cur.execute(nocount + sql_str)
            code = cur.fetchone()
            print("code : ", code[0])
            return code[0]
            # code = 'Success'
            # return code
        except Exception as e:
            print(e)
            raise e

    def handle_indent_revision_req(self, request):

        # This method is used by Indent revision API for handling Indent revision related operations

        print(" In handle_indent_revision_req")
        common_util_obj = common_util.common_utils()
        # logger = common_util_obj.initiateLogger()
        # logger.setLevel(logging.INFO)
        # logger.info("INSIDE handle_create_ticket_request METHOD OF TicketController.")
        request_obj = request.data
        print(request_obj)
        created_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        created_by = request.userinfo['username'].username
        created_reference_id = request.userinfo['reference_id']
        try:
            insert_obj = {}
            temp_obj = {}
            status_dict = {}
            is_old_indent_present = False
            # print(request)
            logged_in_user = request.userinfo['username']
            print("logged_in_user :: ", logged_in_user)
            user_permissions = list(logged_in_user.user_permissions.all().values_list('codename', flat=True))
            print(user_permissions)
            if masterconf.auto_approval_permission in user_permissions:
                record_status = masterconf.updated_status
            else:
                record_status = masterconf.pending_status
            print(" record_status :: ", record_status)
            insert_obj["indent_order_number"] = request_obj['indent_order_number']
            insert_obj["indent_short_code"] = request_obj['indent_short_code']
            insert_obj["total_amount_available"] = request_obj['total_amount_available']
            insert_obj["available_50_amount"] = request_obj['available_50_amount']
            insert_obj["available_100_amount"] = request_obj['available_100_amount']
            insert_obj["available_200_amount"] = request_obj['available_200_amount']
            insert_obj["available_500_amount"] = request_obj['available_500_amount']
            insert_obj["available_2000_amount"] = request_obj['available_2000_amount']
            insert_obj["project_id"] = request_obj['project_id']
            insert_obj["bank_code"] = request_obj['bank']
            insert_obj["feeder_branch_code"] = request_obj['feeder_branch']
            insert_obj["is_withdrawal_done"] = request_obj['is_withdrawal_done']
            insert_obj["reason"] = request_obj['reason']
            insert_obj["remarks"] = request_obj['remarks']
            insert_obj["order_date"] = request_obj['order_date']
            insert_obj["created_on"] = created_on
            insert_obj["created_by"] = created_by
            insert_obj["created_reference_id"] = created_reference_id
            insert_obj["record_status"] = record_status
            indent_mstr_queryset = indent_master.objects.filter(indent_order_number=insert_obj["indent_order_number"])
            db_list = list(indent_mstr_queryset.values())
            print(db_list)
            if len(db_list) > 0:
                is_old_indent_present = True
                temp_obj = db_list[0]
                # insert_obj["indent_revision_order_number"] = self.generate_indent_revision_code(temp_obj)
                insert_obj["original_forecast_amount_50"] = temp_obj["total_atm_loading_amount_50"]
                insert_obj["original_forecast_amount_100"] = temp_obj["total_atm_loading_amount_100"]
                insert_obj["original_forecast_amount_200"] = temp_obj["total_atm_loading_amount_200"]
                insert_obj["original_forecast_amount_500"] = temp_obj["total_atm_loading_amount_500"]
                insert_obj["original_forecast_amount_2000"] = temp_obj["total_atm_loading_amount_2000"]
                insert_obj["total_original_forecast_amount"] = temp_obj["total_atm_loading_amount"]
                # temp_obj["bank_code"] = temp_obj["bank"]
            revision_already_exists = indent_revision_request.objects.exclude(record_status = masterconf.reject_status).filter(indent_order_number=insert_obj["indent_order_number"]).exists()
            print("revision_already_exists ::: ",revision_already_exists)
            if revision_already_exists:
                return masterconf.Record_Already_Exists
            else:
                indent_rev_obj = indent_revision_request(**insert_obj)
                indent_rev_obj.save()
            if is_old_indent_present and record_status == masterconf.updated_status:
                print("Need to call revision of indent generation code")
                code = self.generate_indent_revision(request, temp_obj, True)
                return code
            elif is_old_indent_present and record_status == masterconf.pending_status:
                return masterconf.indent_request_success_submit
        except Exception as e:
            print(" asasasasasa ", e)
            raise e

    def update_history_active_status(self, request):

        print(" in update_history_active_status method ")
        request_obj = request.data
        existing_record_obj = request_obj["existing_record"]
        updated_record_status = request_obj["updated_record_status"]
        old_status = existing_record_obj["record_status"]
        original_indent_no = existing_record_obj["indent_order_number"]
        indent_revision_no = existing_record_obj["indent_revision_order_number"]
        user = request.userinfo['username'].username
        time_of_change = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        reference_id = request.userinfo['reference_id']
        request_for_create_pdf = {}
        try:
            orig_indent_master_rec = indent_master.objects.filter(indent_order_number=original_indent_no)
            orig_indent_detail_rec = indent_detail.objects.filter(indent_order_number=original_indent_no)
            orig_distri_plan_mstr_rec = distribution_planning_master_V2.objects.filter(indent_code=original_indent_no)
            orig_distri_plan_det_rec = distribution_planning_detail_V2.objects.filter(indent_code=original_indent_no)

            new_indent_master_rec = indent_master.objects.filter(indent_order_number=indent_revision_no)
            new_indent_detail_rec = indent_detail.objects.filter(indent_order_number=indent_revision_no)
            new_distri_plan_mstr_rec = distribution_planning_master_V2.objects.filter(indent_code=indent_revision_no)
            new_distri_plan_det_rec = distribution_planning_detail_V2.objects.filter(indent_code=indent_revision_no)

            revision_indent_record = indent_revision_request.objects.filter(
                indent_revision_order_number=indent_revision_no)
            data_update_log_obj = data_update_log.objects.filter(destination_info=indent_revision_no)

            orig_indent_master_rec.update(record_status=masterconf.history_status, modified_on=time_of_change,
                                          modified_by=user, modified_reference_id=reference_id)
            orig_indent_detail_rec.update(record_status=masterconf.history_status, modified_on=time_of_change,
                                          modified_by=user, modified_reference_id=reference_id)
            orig_distri_plan_mstr_rec.update(record_status=masterconf.history_status, modified_on=time_of_change,
                                             modified_by=user, modified_reference_id=reference_id)
            orig_distri_plan_det_rec.update(record_status=masterconf.history_status, modified_on=time_of_change,
                                            modified_by=user, modified_reference_id=reference_id)

            new_indent_master_rec.update(record_status=masterconf.active_status, approved_on=time_of_change,
                                         approved_by=user, approved_reference_id=reference_id)
            new_indent_detail_rec.update(record_status=masterconf.active_status, approved_on=time_of_change,
                                         approved_by=user, approved_reference_id=reference_id)
            new_distri_plan_mstr_rec.update(record_status=masterconf.active_status, approved_on=time_of_change,
                                            approved_by=user, approved_reference_id=reference_id)
            new_distri_plan_det_rec.update(record_status=masterconf.active_status, approved_on=time_of_change,
                                           approved_by=user, approved_reference_id=reference_id)

            revision_indent_record.update(record_status=masterconf.active_status, approved_on=time_of_change,
                                          approved_by=user, approved_reference_id=reference_id)
            data_update_log_obj.update(record_status=masterconf.active_status, approved_on=time_of_change,
                                       approved_by=user, approved_reference_id=reference_id)

            request_for_create_pdf['data'] = {}
            request_for_create_pdf['data']['indent_order_number'] = indent_revision_no
            pdf_obj = PDFController.PDFController()
            data = pdf_obj.handle_pdf_create_req(request_for_create_pdf)
            response_list = pdf_obj.create_pdf(data,user,reference_id)
            if response_list != []:
                return masterconf.status_approval_success
            else:
                return masterconf.failed_to_generate_pdf
        except Exception as e:
            print(e)
            raise e

    def handle_indent_status_change_req(self, request):

        # This method is used to update the status of records for indent_master and indent_revision_request model

        request_obj = request.data  # ['full_record','record_status','menu']
        existing_record_obj = request_obj["existing_record"]
        updated_record_status = request_obj["updated_record_status"]
        old_status = existing_record_obj["record_status"]
        original_indent_no = existing_record_obj["indent_order_number"]
        user = request.userinfo['username'].username
        time_of_change = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        reference_id = request.userinfo['reference_id']
        if old_status == masterconf.review_pending_status:
            indent_revision_no = existing_record_obj["indent_revision_order_number"]
        original_indent_record = indent_master.objects.get(indent_order_number=original_indent_no)
        print(" original_indent_record :: ", original_indent_record)
        revision_indent_record = indent_revision_request.objects.filter(Q(indent_order_number=original_indent_no,record_status = masterconf.pending_status) | Q(indent_order_number=original_indent_no,record_status = masterconf.review_pending_status))
        print(" revision_indent_record :: ", revision_indent_record)
        try:
            if old_status == masterconf.pending_status and updated_record_status == masterconf.updated_status:
                revision_indent_record.update(record_status = masterconf.updated_status)
                #revision_indent_record.save()
                code = self.generate_indent_revision(request, existing_record_obj, False)
                return code
            elif old_status == masterconf.review_pending_status and updated_record_status == masterconf.active_status:
                code = self.update_history_active_status(request)
                print(" code :: ", code)
                return code
            elif old_status == masterconf.pending_status and updated_record_status == masterconf.rejected_status:
                indent_rev_req_model = indent_revision_request.objects.filter(indent_order_number=original_indent_no)
                indent_rev_req_model.update(record_status=masterconf.reject_status, rejected_on=time_of_change,
                                            rejected_by=user, reject_reference_id=reference_id)
                # indent_rev_req_model.save()
                return masterconf.status_rejection_success
            elif old_status == masterconf.review_pending_status and updated_record_status == masterconf.rejected_status:

                print(" In Rejected after review pending ")
                indent_rev_req_model = indent_revision_request.objects.filter(
                    indent_revision_order_number=indent_revision_no)
                new_indent_master_rec = indent_master.objects.filter(indent_order_number=indent_revision_no)
                new_indent_detail_rec = indent_detail.objects.filter(indent_order_number=indent_revision_no)
                new_distri_plan_mstr_rec = distribution_planning_master_V2.objects.filter(
                    indent_code=indent_revision_no)
                new_distri_plan_det_rec = distribution_planning_detail_V2.objects.filter(indent_code=indent_revision_no)
                data_update_log_obj = data_update_log.objects.filter(destination_info=indent_revision_no)

                new_indent_master_rec.update(record_status=masterconf.reject_status, rejected_on=time_of_change,
                                             rejected_by=user, reject_reference_id=reference_id)
                new_indent_detail_rec.update(record_status=masterconf.reject_status, rejected_on=time_of_change,
                                             rejected_by=user, reject_reference_id=reference_id)
                new_distri_plan_mstr_rec.update(record_status=masterconf.reject_status, rejected_on=time_of_change,
                                                rejected_by=user, reject_reference_id=reference_id)
                new_distri_plan_det_rec.update(record_status=masterconf.reject_status, rejected_on=time_of_change,
                                               rejected_by=user, reject_reference_id=reference_id)
                indent_rev_req_model.update(record_status=masterconf.reject_status, rejected_on=time_of_change,
                                            rejected_by=user, reject_reference_id=reference_id)
                data_update_log_obj.update(record_status=masterconf.reject_status, rejected_on=time_of_change,
                                           rejected_by=user, reject_reference_id=reference_id)
                # new_indent_master_rec.save()
                # new_indent_detail_rec.save()
                # new_distri_plan_mstr_rec.save()
                # new_distri_plan_det_rec.save()
                # indent_rev_req_model.save()
                return masterconf.status_rejection_success
        except Exception as e:
            print(" in handle_indent_status_change_req :: ", e)
            raise e

    def handle_indent_generate_req(self, request):

        # This method is used to generate the indent. i.e. Call the indent procedure

        request_obj = request.data
        indent_date = request_obj["indent_date"]  # datetime.datetime.fromtimestamp(time.time()).strftime(
        #     '%Y-%m-%d')  # '2018-11-22' #request_obj["indent_date"]
        print("indent_date :: ", indent_date)
        is_Indent = None
        is_Revision = None
        active_indent_code = None
        project_Id = request_obj["project_id"]
        bankcode = request_obj["bank_code"]
        feederbranch = request_obj["feeder_branch"]
        created_by = request.userinfo['username'].username
        created_reference_id = request.userinfo['reference_id']
        if bankcode == "all":
            sql_str = "DECLARE @out varchar(10) exec usp_indent_calculation_V3 '{}' , '{}', '{}', '{}', '{}', '{}', 'NO', '', @out OUTPUT SELECT @out".format(indent_date, created_by, created_reference_id, project_Id, bankcode, feederbranch)
            #sql_str = " DECLARE @out varchar(10) exec usp_indent_calculation_V2 " + "'" + indent_date + "', " + "'" + created_by + "'," + "'" + created_reference_id + "'," + "'" + project_Id + "'," + "'" + bankcode + "'," + "'" + feederbranch + "'," + "'NO'" + "," +  "\' \'" + ", @out OUTPUT SELECT @out"
        else:
            banklist = ",".join(bankcode)
            sql_str = "DECLARE @out varchar(10) exec usp_indent_calculation_V3 '{}' , '{}', '{}', '{}', '{}', '{}', 'NO', '', @out OUTPUT SELECT @out".format(indent_date,created_by,created_reference_id,project_Id,banklist,feederbranch)
        print("sql_str from usp_indent_calculation_V2 : {}".format(sql_str))
        try:
            nocount = """ SET NOCOUNT ON; """
            cur = connection.cursor()
            cur.execute(nocount + sql_str)
            results = []
            results = cur.fetchone()
            return results[0]
        except Exception as e:
            print(e)
            raise e

    def handle_indent_edit_req(self, request):

        # This method is used to update the indent revision after being generated by the system

        request_obj = request.data
        modified_on = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
        modified_by = request.userinfo['username'].username
        modified_reference_id = request.userinfo['reference_id']
        is_feeder_valid = False
        is_atm_feeder_valid = False
        is_load_gdn_insurance_cast_cap = False
        is_load_casette_denom = False
        status_dict = {}
        request_for_create_pdf = {}
        is_generate_pdf = int(request_obj['is_generate_pdf'])
        del request_obj['is_generate_pdf']
        try:
            total_fdr_50 = 0 if request_obj['total_atm_loading_amount_50'] == None else request_obj['total_atm_loading_amount_50']
            total_fdr_100 = 0 if request_obj['total_atm_loading_amount_100'] == None else request_obj['total_atm_loading_amount_100']
            total_fdr_200 = 0 if request_obj['total_atm_loading_amount_200'] == None else request_obj['total_atm_loading_amount_200']
            total_fdr_500 = 0 if request_obj['total_atm_loading_amount_500'] == None else request_obj['total_atm_loading_amount_500']
            total_fdr_2000 = 0 if request_obj['total_atm_loading_amount_2000'] == None else request_obj['total_atm_loading_amount_2000']
            total_fdr_amount = 0 if request_obj['total_atm_loading_amount'] == None else request_obj['total_atm_loading_amount']
            indent_order_no = request_obj['indent_order_number']
            total_load_atm_50 = 0
            total_load_atm_100 = 0
            total_load_atm_200 = 0
            total_load_atm_500 = 0
            total_load_atm_2000 = 0
            total_load_atm_total = 0
            if total_fdr_amount == (total_fdr_50 + total_fdr_100 + total_fdr_200 + total_fdr_500 + total_fdr_2000):
                is_feeder_valid = True
                for indent_detail_record in request_obj['indent_detail']:
                    total_load_atm_50 = total_load_atm_50 + (0 if indent_detail_record['loading_amount_50'] == None else indent_detail_record['loading_amount_50'])
                    total_load_atm_100 = total_load_atm_100 + (0 if indent_detail_record['loading_amount_100'] == None else indent_detail_record['loading_amount_100'])
                    total_load_atm_200 = total_load_atm_200 + (0 if indent_detail_record['loading_amount_200'] == None else indent_detail_record['loading_amount_200'])
                    total_load_atm_500 = total_load_atm_500 + (0 if indent_detail_record['loading_amount_500'] == None else indent_detail_record['loading_amount_500'])
                    total_load_atm_2000 = total_load_atm_2000 + (0 if indent_detail_record['loading_amount_2000'] == None else indent_detail_record['loading_amount_2000'])
                    total_load_atm_total = total_load_atm_total + (0 if indent_detail_record['total'] == None else indent_detail_record['total'])
                if (total_fdr_50 == total_load_atm_50) and (total_fdr_100 == total_load_atm_100) and (
                        total_fdr_200 == total_load_atm_200) and (total_fdr_500 == total_load_atm_500) and (
                        total_fdr_2000 == total_load_atm_2000) and (total_fdr_amount == total_load_atm_total):
                    is_atm_feeder_valid = True
                for indent_detail_record in request_obj['indent_detail']:
                    distribution_record = indent_detail_record['distribution_details']
                    insurance_limit = int(distribution_record['insurance_limit'])
                    bank_cash_limit = int(distribution_record['bank_cash_limit'])
                    # total_cassette_capacity = distribution_record['total_cassette_capacity']
                    if insurance_limit != None and (bank_cash_limit != None and bank_cash_limit > 0):
                        if (0 if indent_detail_record['total'] == None else indent_detail_record['total']) <= insurance_limit and (0 if indent_detail_record[
                            'total'] == None else indent_detail_record['total']) <= bank_cash_limit:
                            is_load_gdn_insurance_cast_cap = True
                        else:
                            is_load_gdn_insurance_cast_cap = False
                            break
                    else:
                        is_load_gdn_insurance_cast_cap = True
                    loading_amount_50 = (0 if indent_detail_record['loading_amount_50'] == None else indent_detail_record['loading_amount_50'])
                    loading_amount_100 = (
                        0 if indent_detail_record['loading_amount_100'] == None else indent_detail_record[
                            'loading_amount_100'])
                    loading_amount_200 = (
                        0 if indent_detail_record['loading_amount_200'] == None else indent_detail_record[
                            'loading_amount_200'])
                    loading_amount_500 = (
                        0 if indent_detail_record['loading_amount_500'] == None else indent_detail_record[
                            'loading_amount_500'])
                    loading_amount_2000 = (
                        0 if indent_detail_record['loading_amount_2000'] == None else indent_detail_record[
                            'loading_amount_2000'])
                    total_capacity_amount_50 = 0 if int(distribution_record['total_capacity_amount_50']) == None else int(distribution_record['total_capacity_amount_50'])
                    total_capacity_amount_100 = 0 if int(distribution_record['total_capacity_amount_100']) == None else \
                    int(distribution_record['total_capacity_amount_100'])
                    total_capacity_amount_200 = 0 if int(distribution_record['total_capacity_amount_200']) == None else \
                    int(distribution_record['total_capacity_amount_200'])
                    total_capacity_amount_500 = 0 if int(distribution_record['total_capacity_amount_500']) == None else \
                    int(distribution_record['total_capacity_amount_500'])
                    total_capacity_amount_2000 = 0 if int(distribution_record['total_capacity_amount_2000']) == None else \
                    int(distribution_record['total_capacity_amount_2000'])

                    if loading_amount_50 <= total_capacity_amount_50 and loading_amount_100 <= total_capacity_amount_100 and loading_amount_200 <= total_capacity_amount_200 and loading_amount_500 <= total_capacity_amount_500 and loading_amount_2000 <= total_capacity_amount_2000:
                        is_load_casette_denom = True
                    else:
                        is_load_casette_denom = False
                        break
            print(" is_feeder_valid :: ",is_feeder_valid)
            print(" is_atm_feeder_valid :: ", is_atm_feeder_valid)
            print(" is_load_gdn_insurance_cast_cap :: ", is_load_gdn_insurance_cast_cap)
            print(" is_load_casette_denom :: ", is_load_casette_denom)
            if is_feeder_valid and is_atm_feeder_valid and is_load_gdn_insurance_cast_cap and is_load_casette_denom:
                for indent_detail_record in request_obj['indent_detail']:
                    atm_id = indent_detail_record['atm_id']
                    # distribution_detail_dnata = indent_detail_record['distribution_details']
                    # distribution_planning_detail_V2.objects.filter(atm_id = atm_id,indent_code=indent_order_no).update(**distribution_detail_data)
                    del indent_detail_record['distribution_details']
                    indent_detail_record['is_manually_updated'] = 'Yes'
                    indent_detail_record['modified_on'] = modified_on
                    indent_detail_record['modified_by'] = modified_by
                    indent_detail_record['modified_reference_id'] = modified_reference_id
                    indent_detail.objects.filter(indent_order_number=indent_order_no, atm_id=atm_id).update(
                        **indent_detail_record)
                del request_obj['indent_detail']
                del request_obj['distribution_master']
                request_obj['is_manually_updated'] = 'Yes'
                request_obj['amount_in_words'] = num2words(request_obj['total_atm_loading_amount'],lang='en_IN').title()
                request_obj['modified_on'] = modified_on
                request_obj['modified_by'] = modified_by
                request_obj['modified_reference_id'] = modified_reference_id
                indent_master.objects.filter(indent_order_number=indent_order_no).update(**request_obj)
                if is_generate_pdf:
                    request_for_create_pdf['data'] = {}
                    request_for_create_pdf['data']['indent_order_number'] = indent_order_no
                    pdf_obj = PDFController.PDFController()
                    try:
                        data = pdf_obj.handle_pdf_create_req(request_for_create_pdf)
                        response_list = pdf_obj.create_pdf(data, modified_by, modified_reference_id)
                        if response_list != None:
                            status_dict['status_code'] = 'S109'
                            return status_dict
                    except Exception as e:
                        status_dict['status_code'] = 'E115'
                        return status_dict
                status_dict['status_code'] = 'S102'
            else:
                status_dict['status_code'] = 'E103'
            return status_dict
        except Exception as e:
            print(" asasasasasa ", e)
            raise e

    def handle_indent_holiday_list_req(self, request):

        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_indent_holiday_list_req METHOD OF IndentController.")
        request_obj = request.data
        list_returned = list()
        try:
            if 'filters' in request_obj:
                filters_param = request_obj['filters']
                list_db = indent_holiday.objects.filter(**filters_param).values()
            else:
                list_db = indent_holiday.objects.all().values()

            if 'required_fields' in request_obj.keys():
                required_fields_arr = request_obj['required_fields']
                for i in range(0, len(list_db)):
                    inner_obj = {key: list_db[i][key] for key in required_fields_arr}
                    list_returned.append(inner_obj)
                return list_returned
            else:
                return list_db
        except Exception as e:
            print(e)
            status_dict = {}
            status_dict['status_code'] = masterconf.error_code
            status_dict['status_text'] = masterconf.list_error_text
            logger.error('EXCEPTION IN handle_indent_holiday_list_req METHOD OF IndentController: %s', e)
            logger.info('Response from handle_indent_holiday_list_req of IndentController: %s',
                        status_dict)
            raise e

    def handle_indent_holiday_edit_req(self, request):

        logger = logging.getLogger(__name__)
        logger.info("INSIDE handle_indent_holiday_edit_req METHOD OF IndentController.")
        request_obj = request.data
        status_dict = {}
        try:
            if 'data' in request_obj:
                for data in request_obj['data']:
                    print(data)
                    update_data = {}
                    update_data['is_indent_required'] = data['is_indent_required']
                    update_data['withdrawal_type'] = data['withdrawal_type']
                    indent_holiday.objects.filter(id=data['id'],record_status=masterconf.active_status).update(**update_data)
                    status_dict['status_code'] = masterconf.edit_success_code
                    status_dict['status_text'] = masterconf.edit_success_text
                    status_dict['id'] = data['id']
            else:
                status_dict['status_code'] = masterconf.edit_error_code
                status_dict['status_text'] = masterconf.edit_error_text

            return status_dict
        except Exception as e:
            status_dict['status_code'] = masterconf.edit_error_code
            status_dict['status_text'] = masterconf.edit_error_text
            logger.error('EXCEPTION IN handle_indent_holiday_edit_req METHOD OF IndentController: %s', e)
            logger.info('Response from handle_indent_holiday_edit_req of IndentController: %s',
                        status_dict)
            raise e

    def handle_indent_holiday_generate_req(self, request):

        # This method is used to generate the indent holiday. i.e. Call the indent holiday procedure

        request_obj = request.data
        indent_date = request_obj["indent_date"]  # datetime.datetime.fromtimestamp(time.time()).strftime(
        #     '%Y-%m-%d')  # '2018-11-22' #request_obj["indent_date"]
        print("indent_date :: ", indent_date)
        created_by = request.userinfo['username'].username
        created_reference_id = request.userinfo['reference_id']
        sql_str = "exec usp_holidays_for_indent " + "'" + indent_date + "', " + "'" + created_by + "', " + "'" +created_reference_id + "' ";
        print("sql_str from usp_holidays_for_indent : {}".format(sql_str))
        try:
            nocount = """ SET NOCOUNT ON; """
            cur = connection.cursor()
            cur.execute(nocount + sql_str)
            results = []
            results = cur.fetchone()
            return results[0]
        except Exception as e:
            print(e)
            raise e