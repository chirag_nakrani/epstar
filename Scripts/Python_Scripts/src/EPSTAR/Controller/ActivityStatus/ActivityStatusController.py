from Common.CommonFunctions import common_util
import logging
from Common import masterconf, db_queries_properties
import pandas as pd
import pyodbc
from django.contrib.auth.models import User
from Model.models import user_bank_project_mapping

class ActivityStatusController():

    def handle_activity_status_req(self, request):
        common_util_obj = common_util.common_utils()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("INSIDE handle_activity_status_req METHOD OF ActivityStatusController.")
        # status_dict ={}
        try:
            #### query to get activity status of specific bank #####
            user_id = User.objects.get(username=request.userinfo['username'].username).id
            user_bank = None
            try:
                user_bank = user_bank_project_mapping.objects.filter(user_id=user_id).values()
            except Exception as e:
                logger.error('EXCEPTION IN handle_indent_admin_req METHOD OF IndentAdminController', exc_info=True)
                logger.error('No bank mapped')
            bank_code = []
            for i in user_bank:
                bank_code.append("'" + i['bank_code'] + "'")
            bank_str = ", ".join(bank_code)
            if user_bank:
                sql_query = db_queries_properties.activity_status_query % (bank_str,bank_str,bank_str,bank_str,bank_str,bank_str,bank_str)
                print(sql_query)
                df = pd.read_sql(sql_query, pyodbc.connect(masterconf.connection_string)).fillna('')
                if not df.empty:
                    grouped_list = common_util_obj.pandas_to_dict(df)
                    return grouped_list
                else:
                    response_obj = {}
                    response_obj['status_code'] = masterconf.api_status_Failure
                    response_obj['status_text'] = masterconf.no_data_found
                    return response_obj

        except Exception as e:
            logger.setLevel(logging.ERROR)
            logger.error('EXCEPTION IN handle_activity_status_req METHOD OF ActivityStatusController', exc_info=True)
            raise e
