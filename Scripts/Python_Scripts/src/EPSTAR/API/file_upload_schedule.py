from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.FileUploadSchedule import FileUploadScheduleController
from Common import masterconf

class Schedule(APIView):
    authentication_classes = ()
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = ()

    def post(self, request, format=None):
        status_dict = {}
        response_list = list()
        try:
            schedule_controller = FileUploadScheduleController.FileUploadScheduleController()
            response_list = schedule_controller.handle_schedule_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            print("Exception: ", e)
            status_dict["level"] = masterconf.system_level
            status_dict["status_code"] = masterconf.system_level_exception_code
            status_dict["status_desc"] = masterconf.system_level_exception
            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
