from Common import masterconf
from Common.CommonFunctions import common_util
from Controller.Ticket import TicketController
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class CreateTicket(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        check_req = common_util_obj.validate_create_ticket_req(request.data)
        response_obj = {}
        if check_req:
            try:
                tckt_cntroller = TicketController.TicketController()
                response_obj['ticket_id'] = tckt_cntroller.handle_create_ticket_request(request)
                response_obj['status'] = 'success'
                return Response(status=status.HTTP_201_CREATED, data=response_obj)
            except Exception as e:
                # print(e)
                response_obj['status'] = 'failure'
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class CreateReply(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        check_req = common_util_obj.validate_create_ticket_reply(request.data)
        response_obj = {}
        if check_req:
            try:
                tckt_cntroller = TicketController.TicketController()
                response_obj['ticket_reply_id'] = tckt_cntroller.handle_create_reply_request(request)
                response_obj['status'] = 'success'
                return Response(status=status.HTTP_201_CREATED, data=response_obj)
            except Exception as e:
                # print(e)
                response_obj['status'] = 'failure'
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class ticket_list(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            tckt_cntroller = TicketController.TicketController()
            response_list = tckt_cntroller.handle_ticket_list_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            # print(e)
            response_obj['status'] = 'failure'
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class ticket_detail(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        response_obj = {}
        common_util_obj = common_util.common_utils()
        check_req_flag = common_util_obj.validate_ticket_detail_req(request.data)
        if check_req_flag:
            try:
                tckt_cntroller = TicketController.TicketController()
                response_list = tckt_cntroller.handle_ticket_detail_req(request)
                return Response(status=status.HTTP_201_CREATED, data=response_list)
            except Exception as e:
                # print(e)
                response_obj['status'] = 'failure'
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"status": "Bad Request"})


class TicketCategoryMenu(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        try:
            tckt_cntroller = TicketController.TicketController()
            response_list = tckt_cntroller.handle_category_menus_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            # print(e)
            response_obj['status'] = 'failure'
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class UpdateTicket(APIView):
    # This API is used to update the ticket.

    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        print(" In post")
        common_util_obj = common_util.common_utils()
        check_req = common_util_obj.validate_update_ticket_req(request.data)
        print("check_req  ", check_req)
        status_dict = {}
        if check_req:
            try:
                tckt_cntroller = TicketController.TicketController()
                status_dict = tckt_cntroller.handle_update_ticket_request(request)
                if status_dict['status_code'] == masterconf.success_updated:
                    status_dict['status_desc'] = masterconf.success_codes_desc[status_dict['status_code']]
                    return Response(status=status.HTTP_201_CREATED, data=status_dict)
                else:
                    status_dict['status_code'] = masterconf.failure_code
                    status_dict['status_desc'] = masterconf.error_codes_desc[status_dict['status_code']]
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
            except Exception as e:
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
