from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import time, datetime
from Controller.Routine import BaseCBRController, BaseCDRController
from Controller.ViewData import ViewController, C3RViewController, AmsAtmController, FeederMasterController, CRAFEASIBILITYController, AtmConfigLimitsCont, ReferenceMenuController, ConfigMenuController
from Common.CommonFunctions import common_util
from rest_framework.settings import api_settings
from Controller.ConfigurableParams import HolidayController
from rest_framework import generics
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.pagination import PageNumberPagination
from Common.serializers import view_serializer_fn
from django.apps import apps
import logging


class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

########################This API is used for viewing the data for CBR, CDR, IMS########################################
class GetData(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	pagination_class = StandardResultsSetPagination
	permission_classes = (IsAuthenticated,)

	def post(self, request,format=None):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In views.py.")
		if str(request.data['file_type']).upper() == masterconf.file_type_ims.upper():
			strfile_type = str(request.data['file_type']).upper()
			check_req_flag = common_util_obj.validate_view_req_ims(request)
		elif str(request.data['file_type']).upper() == masterconf.file_type_loading_confirm.upper():
			strfile_type = str(request.data['file_type']).upper()
			check_req_flag = common_util_obj.validate_view_req_ims(request)
		else:
			strfile_type = str(request.data['file_type']).upper()
			strbank_code = str(request.data['bank_code']).upper()
			check_req_flag = common_util_obj.validate_view_req(request)
		stat = {"status" : "Internal Server Error or Bad Request"}
		print ("check_req_flag from GetData : ",check_req_flag)
		if check_req_flag:
			try:
				logger = common_util_obj.initiateLogger()
				logger.setLevel(logging.INFO)
				logger.info("GOING INTO ViewController")
				view_controller_obj = ViewController.ViewController()
				data = view_controller_obj.handle_view_request(request)
				print(data)
				paginator = self.pagination_class()
				page = paginator.paginate_queryset(data, request)
				return paginator.get_paginated_response(page)
			except Exception as e:
				print("Exception from GetData of views.py : ",e)
				logger = common_util_obj.initiateLogger()
				logger.setLevel(logging.ERROR)
				logger.error('Exception occured while uploading Routine file', exc_info=True)
				return Response(status=status.HTTP_400_BAD_REQUEST,data=stat)
		else:
			return Response(status=status.HTTP_400_BAD_REQUEST,data=stat)


####################This API is used for viewing of C3R data########################################################
class GetDataC3R(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	pagination_class = StandardResultsSetPagination
	permission_classes = (IsAuthenticated,)

	def post(self, request,format=None):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In GetDataC3R.")
		stat = {"status" : "Internal Server Error or Bad Request"}
		status_dict = {}
		check_req_flag = common_util_obj.validate_viewc3r_req(request.data)
		if check_req_flag:
			try:
				if request.data["file_type"] == masterconf.file_type_C3R_CMIS or request.data["file_type"] == masterconf.file_type_C3R_VMIS:
					c3RViewController = C3RViewController.C3RViewController()
					data = c3RViewController.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
				else:
					status_dict["status_text"] = masterconf.c3rapi_wrongTablePassed
					return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
			except Exception as e:
				print("Exception thrown from GetDataC3R : ",e)
				return Response(status=status.HTTP_400_BAD_REQUEST,data=stat)
		else:
			return Response(status=status.HTTP_400_BAD_REQUEST,data=stat)

###########################to get the list of particular menu from master which is in pending state######################################
class GetDataAmsMaster(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	common_util_obj = common_util.common_utils()
	logger = common_util_obj.initiateLogger()

	def post(self, request,format=None):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		stat = {"status" : "Internal Server Error or Bad Request"}
		status_dict = {}
		check_req_flag = common_util_obj.validate_amsMaster_req(request.data)
		if check_req_flag:
			logger.setLevel(logging.INFO)
			logger.info("In post method of GetDataAmsMaster")
			try:
############to View the data uploaded in their respective state, here it is for ATM MASTER data################################
				if request.data["menu"] == masterconf.ams_master:
					amsAtmController = AmsAtmController.AmsAtmController()
					status_dict = amsAtmController.handle_view_request(request)
					if status_dict["status_text"] == masterconf.masterApiSuccess:
						del status_dict["status_text"]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						del status_dict["status_text"]
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
############to View the data uploaded in their respective state, here it is for FEEDER BRANCH MASTER data################################
				elif request.data["menu"].lower() == masterconf.file_type_feeder.lower():
					feederMasterController = FeederMasterController.FeederMasterController()
					status_dict = feederMasterController.handle_view_request(request)
					if status_dict["status_text"] == masterconf.masterApiSuccess:
						del status_dict["status_text"]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						del status_dict["status_text"]
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
############to View the data uploaded in their respective state, here it is for CRA FEASIBILITY data################################
				elif request.data["menu"] == masterconf.file_type_cra_mstr:
					cRAFEASIBILITYController = CRAFEASIBILITYController.CRAFEASIBILITYController()
					status_dict = cRAFEASIBILITYController.handle_view_request(request)
					if status_dict["status_text"] == masterconf.masterApiSuccess:
						del status_dict["status_text"]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						del status_dict["status_text"]
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
############to View the data uploaded in their respective state, here it is for ATM CONFIG LIMITS data################################
				elif request.data["menu"] == masterconf.file_type_atm_cnfg:
					atmConfigLimitsCont = AtmConfigLimitsCont.AtmConfigLimitsCont()
					status_dict = atmConfigLimitsCont.handle_view_request(request)
					if status_dict["status_text"] == masterconf.masterApiSuccess:
						del status_dict["status_text"]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						del status_dict["status_text"]
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
############to View the data uploaded in their respective state, here it is for Holidat Master data################################
				elif request.data["menu"].lower() == masterconf.file_type_holiday_master.lower():
					holidayController = HolidayController.HolidayController()
					status_dict = holidayController.handle_view_request(request)
					if status_dict["status_text"] == masterconf.masterApiSuccess:
						del status_dict["status_text"]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						del status_dict["status_text"]
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
########################################################################################################
#######################Reference menu viewing the data, common fuunctionality is made to view data of all the menu on the bassis of menu input################################################
				elif request.data["menu"].upper() in  masterconf.reference_menu_controller:
					referenceMenuController = ReferenceMenuController.ReferenceMenuController()
					status_dict = referenceMenuController.handle_view_request(request)
					if status_dict["status_text"] == masterconf.masterApiSuccess:
						del status_dict["status_text"]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						del status_dict["status_text"]
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
##########showing those records which are in approval Pending state for config menu############################################
				elif request.data["menu"].lower() in  masterconf.config_menu_list or request.data["menu"].lower() == masterconf.file_type_pre_qualify.lower():
					configMenuController = ConfigMenuController.ConfigMenuController()
					status_dict = configMenuController.handle_view_request(request)
					if status_dict["status_text"] == masterconf.masterApiSuccess:
						del status_dict["status_text"]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						del status_dict["status_text"]
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
#########################################################################################################
				elif request.data["menu"].lower() in  masterconf.operation_menu_list:
					referenceMenuController = ReferenceMenuController.ReferenceMenuController()
					status_dict = referenceMenuController.handle_view_request(request)
					if status_dict["status_text"] == masterconf.masterApiSuccess:
						del status_dict["status_text"]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						del status_dict["status_text"]
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
################################################################################################
				else:
					status_dict["status_text"] = stat#to handle internal server error if any operation is failing to respond as expected
					return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
			except Exception as e:
				print("Exception from GetDataAmsMaster : ",e)
				logger.setLevel(logging.ERROR)
				logger.error('Exception occured in GetDataAmsMaster', exc_info=True)
				return Response(status=status.HTTP_400_BAD_REQUEST,data=stat)
		else:
			return Response(status=status.HTTP_400_BAD_REQUEST,data=stat)
#######################To view the data data for particular menu of master from UI######################################
class GetMasterData(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	pagination_class = StandardResultsSetPagination
	permission_classes = (IsAuthenticated,)

	def post(self, request,format=None):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		logger.info("In GetMasterData.")
		stat = {"status" : "Internal Server Error or Bad Request"}
		status_dict = {}
		check_req_flag = common_util_obj.validate_amsview_req(request.data)
		logger.setLevel(logging.INFO)
		logger.info("In post method of GetMasterData")
		print ("check_req_flag : ",check_req_flag)
		if check_req_flag:
			try:
				print ("request.data[\"menu\"].lower() : ",request.data["menu"].lower())
				if request.data["menu"].lower() == masterconf.ams_master.lower():#created reference id has been moved to filter param variable and filter param is made optional for the flexibility of the API to view the data
					viewControllerMaster = ViewController.ViewControllerMaster()
					data = viewControllerMaster.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
				elif request.data["menu"].lower() == masterconf.file_type_feeder.lower():#created reference id has been moved to filter param variable and filter param is made optional for the flexibility of the API to view the data
					viewControllerMaster = ViewController.ViewControllerMaster()
					data = viewControllerMaster.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
				elif request.data["menu"].lower() == masterconf.file_type_cra_mstr.lower():#created reference id has been moved to filter param variable and filter param is made optional for the flexibility of the API to view the data
					viewControllerMaster = ViewController.ViewControllerMaster()
					data = viewControllerMaster.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
				elif request.data["menu"].lower() == masterconf.file_type_atm_cnfg.lower():#created reference id has been moved to filter param variable and filter param is made optional for the flexibility of the API to view the data
					viewControllerMaster = ViewController.ViewControllerMaster()
					data = viewControllerMaster.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
##############common method has been made to handle all the menu of reference data, it is used for viewing the data from respective table ###################################
				elif request.data["menu"].upper() in masterconf.reference_menu_controller:
					print('request.data["menu"]',request.data["menu"])
					viewControllerMaster = ViewController.ViewControllerMaster()
					data = viewControllerMaster.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
#####################common method for config menu to view the data#################################
				elif request.data["menu"].lower() in masterconf.config_menu_list:
					viewControllerMaster = ViewController.ViewControllerMaster()
					data = viewControllerMaster.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
#########################method to display consolidate data from master menu#############################
				elif request.data["menu"].lower() in masterconf.consolidate_data.lower():
					viewControllerConsolidateMaster = ViewController.ViewControllerConsolidateMaster()
					status_dict = viewControllerConsolidateMaster.handle_view_request(request)
					print ("status_dict from GetMasterData : ",status_dict)
					if status_dict['api_status'] == masterconf.api_status_Success:
					#data = viewControllerConsolidateMaster.handle_view_request(request)
						paginator = self.pagination_class()
						page = paginator.paginate_queryset(status_dict['data'], request)
						return paginator.get_paginated_response(page)
					else:
						return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict)
#################to display holiday master data##########################################################
				elif request.data["menu"].lower() == masterconf.file_type_holiday_master.lower():
					print("inside holidaymaster")
					viewControllerMaster = ViewController.ViewControllerMaster()
					data = viewControllerMaster.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
#####################common method for config menu to view the data#################################
				elif request.data["menu"].lower() in masterconf.operation_menu_list:
					viewControllerMaster = ViewController.ViewControllerMaster()
					data = viewControllerMaster.handle_view_request(request)
					paginator = self.pagination_class()
					page = paginator.paginate_queryset(data, request)
					return paginator.get_paginated_response(page)
				else:
					status_dict["status_text"] = stat
					return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
			except Exception as e:
				print("Exception from GetMasterData of views.py : ",e)
				logger.setLevel(logging.ERROR)
				logger.error('Exception occured in GetMasterData', exc_info=True)
				return Response(status=status.HTTP_400_BAD_REQUEST,data=stat)
		else:
			return Response(status=status.HTTP_400_BAD_REQUEST,data=stat)
