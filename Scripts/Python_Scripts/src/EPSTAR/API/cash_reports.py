from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.CashReports import CashReportsController
from Common import masterconf
from rest_framework.pagination import PageNumberPagination
import datetime
import os
from django.http import HttpResponse
import logging
##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class CashReports(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination  ##Pagination Class Added

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		logger.info('Start of cash reports api.')
		response_list = list()
		export_time = datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d_%H_%M_%S')
		current_date = datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d')
		try:
			#### cash reports for specific bank and date ####
			cash_reports_controller = CashReportsController.CashReportsController()
			if request.data['type'] == masterconf.cash_report_type:
				response_list = cash_reports_controller.handle_cash_reports_req(request)
			else:
				response_list = cash_reports_controller.handle_loading_recommendation_req(request)

			if 'export' in request.data:
				file_name = (request.data['type'] + '_%s.xlsx' % (export_time)).replace(' ', '_')
				file_path = masterconf.report_output_folder + current_date + '/' + request.data['type'] + '/' + file_name
				if not os.path.exists(masterconf.report_output_folder + current_date + '/' + request.data['type'] + '/'):
					os.makedirs(masterconf.report_output_folder + current_date + '/' + request.data['type'] + '/')
				excel_file = response_list.to_excel(file_path,index=False)

				logger.info('Excel File name: %s',file_path)
				with open(file_path,'rb') as file:
					response = HttpResponse(file.read(),
											content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel')
					response['Content-Disposition'] = 'attachment; filename=%s' % file_path
					return response
			else:
				paginator = self.pagination_class()
				page = paginator.paginate_queryset(response_list, request)
				return paginator.get_paginated_response(page)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
			logger.error('Exception occured in cash_reports.py file')
			logger.error(e)
			logger.info('Returned response from exception: %s',invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)
