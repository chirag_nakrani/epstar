from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Common import masterconf
from Common.CommonFunctions import common_util
from Controller.ConfigurableParams import feeder_vaulting_pre_config_Controller



class feeder_vaulting_pre_config(APIView):

    ## This is the Main API endpoint class for feeder_vaulting_pre_config data upload

    authentication_classes = (JSONWebTokenAuthentication, )
    parser_class = (MultiPartParser, FormParser, )
    renderer_classes = (JSONRenderer, )
    permission_classes = (IsAuthenticated, )

    @csrf_exempt
    def post(self, request):
        status_dict = {}
        common_util_obj = common_util.common_utils()
        check_req_flag = common_util_obj.validate_feeder_vaulting_pre_config_InputReq(request)####validating if input request is as expected
        print("check_req_flag from feeder_vaulting_pre_config:{}".format(check_req_flag))
        if check_req_flag:
            print("Input Request is Valid for feeder_limitdays")
            filehandle = request.FILES['file']
            filehandle_name = request.FILES['file'].name
            print("filehandle: {}".format ( filehandle ))
            print("filehandle_name : {}".format ( filehandle_name ))
            input_filepath = common_util_obj.divide_store_file ( request )  # ES1-T218 - Folder re-structuring done
            try:
#############Going for uploading record for feeder_vaulting_pre_config#####################################
                if request.data['file_type'].upper() == masterconf.feeder_vaulting_pre_config_file_type.upper():#validating if input is for feeder_vaulting_pre_config only
                    Feeder_vaulting_pre_config_Controller = feeder_vaulting_pre_config_Controller.feeder_vaulting_pre_config_Controller()
                    status_dict = Feeder_vaulting_pre_config_Controller.handle_feeder_vaulting_pre_config_upload(request, input_filepath)
                    print("printing Status Dict:{}".format(status_dict))
                    if status_dict['data'] == masterconf.data_updated_for_approval:
                        status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)#Returning success if everything is executed correctly
                    else:
                        status_dict["status_desc"] = masterconf.data_validated_failed
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#returning failure if not success in inserting the new record
                else:
                    return Response ( status=status.HTTP_400_BAD_REQUEST)
            except Exception as e:
                print("Exception: ", e)
                status_dict["level"] = masterconf.system_level
                status_dict["status_code"] = masterconf.system_level_exception_code
                status_dict["status_desc"] = masterconf.system_level_exception
                return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
        else:
            invalid_req = {
                "level":masterconf.request_level,
                "status_code":masterconf.request_level_exception_code,
                "status_desc":masterconf.request_level_exception
            }
            return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)
