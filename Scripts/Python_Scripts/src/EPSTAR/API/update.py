from django.http import HttpResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import time, datetime
# from Controller.Routine import BaseCBRController, BaseCDRController, BaseVCBControllerEOD, BaseC3RMISController, DailyLoad_RepCntrlr
from Controller.Master import MasterMenuUpdateController#MasterMenuUpdateController is developed for updating or modifying the active record from master menu
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.ReferenceData import ReferenceDataUpdateController
from datetime import timedelta
from Controller.ConfigurableParams import ConfigDataUpdateController
from Controller.Operations import OperationDataUpdateController

class MasterUpdate(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	@csrf_exempt
	def post(self, request,format=None):
		status_dict = {}
		common_util_obj = common_util.common_utils()
		check_req_flag = common_util_obj.validate_master_update_req(request.data)
		if check_req_flag:
			try:
				print ("Status of check_req_flag : ",check_req_flag)
#########################Going for update of atm config menu##########################################
				if request.data['file_type'].lower() == masterconf.file_type_atm_cnfg.lower():
					atm_cnfg_update = MasterMenuUpdateController.MasterMenuUpdateController()
					status_dict = atm_cnfg_update.handle_master_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload:
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
#########################Going for update of feeder branch menu##########################################
				elif request.data['file_type'].lower() == (masterconf.file_type_feeder).lower():
					print ("Going for screen update of Feeder branch master menu through screen.")
					feeder_mstr_update = MasterMenuUpdateController.MasterMenuUpdateController()
					status_dict = feeder_mstr_update.handle_master_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload:
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
#########################Going for update of cra feasibility menu##########################################
				elif request.data['file_type'].lower() == (masterconf.file_type_cra_mstr).lower():
					craFeasibility_mstr_update = MasterMenuUpdateController.MasterMenuUpdateController()
					status_dict = craFeasibility_mstr_update.handle_master_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload:
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
#########################Going for update of ams atm menu##########################################
				elif request.data['file_type'].lower() == (masterconf.file_type_ams_atm).lower():
					atm_mstr_update = MasterMenuUpdateController.MasterMenuUpdateController()
					status_dict = atm_mstr_update.handle_master_update(request)
					print ("status_dict from update of atm master file is : ",status_dict)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload:
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
########################Going for screen edit of Reference data############################################
				elif request.data['file_type'].upper() in masterconf.reference_menu_controller:
					referenceDataUpdateController =ReferenceDataUpdateController.ReferenceDataUpdateController()
					status_dict = referenceDataUpdateController.handle_reference_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload:
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)

########################Going for screen edit of Config data############################################
				elif request.data['file_type'].lower() in masterconf.config_menu_list:
					configDataUpdateController = ConfigDataUpdateController.ConfigDataUpdateController()
					status_dict = configDataUpdateController.handle_config_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload :
						return Response ( status=status.HTTP_201_CREATED , data=status_dict )
					else :
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						status_dict["data"] = masterconf.data_validated_failed
						return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )

########################Going for screen edit of Operation menu############################################
				elif request.data['file_type'].lower() in masterconf.operation_menu_list:
					operationDataUpdateController = OperationDataUpdateController.OperationDataUpdateController()
					status_dict = operationDataUpdateController.handle_operation_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload :
						return Response ( status=status.HTTP_201_CREATED , data=status_dict )
					else :
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						status_dict["data"] = masterconf.data_validated_failed
						return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
				else:
					return Response(status=status.HTTP_400_BAD_REQUEST)
			except Exception as e:
				print("Exception from MasterUpdate of update.py file : ", e)
				status_dict["level"] = masterconf.system_level
				status_dict["status_code"] = masterconf.system_level_exception_code
				status_dict["status_desc"] = masterconf.system_level_exception
				return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
		else:
			invalid_req = {
				"level": masterconf.request_level,
				"status_code": masterconf.request_level_exception_code,
				"status_desc": masterconf.request_level_exception
			}
			return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)


class ReferenceUpdate(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	@csrf_exempt
	def post(self, request,format=None):
		status_dict = {}
		common_util_obj = common_util.common_utils()
		check_req_flag = common_util_obj.validate_reference_update_req(request.data)
		if check_req_flag:
			try:
				if request.data['file_type'] == masterconf.file_type_RD_brand_BillCapacity:
					referenceDataUpdateController = ReferenceDataUpdateController.ReferenceDataUpdateController()
					status_dict = referenceDataUpdateController.handle_reference_update(request)
					if status_dict["status_code"] == "S102":
						status_dict["status_desc"] = masterconf.success_codes_desc['S102']
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E105']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
				elif request.data['file_type'] == masterconf.file_type_RD_CRAEMPANELED:
					referenceDataUpdateController = ReferenceDataUpdateController.ReferenceDataUpdateController()
					status_dict = referenceDataUpdateController.handle_reference_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_reference_upload:
						status_dict["status_desc"] = masterconf.success_codes_desc['S102']
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
				elif request.data['file_type'] == masterconf.file_type_vault_master:
					referenceDataUpdateController = ReferenceDataUpdateController.ReferenceDataUpdateController()
					status_dict = referenceDataUpdateController.handle_reference_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload:
						status_dict["status_desc"] = masterconf.success_codes_desc['S102']
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
				elif request.data['file_type'] == masterconf.file_type_Empaneled:
					referenceDataUpdateController = ReferenceDataUpdateController.ReferenceDataUpdateController()
					status_dict = referenceDataUpdateController.handle_reference_update(request)
					if status_dict["status_code"] == masterconf.data_val_success_mstr_upload:
						status_dict["status_desc"] = masterconf.success_codes_desc['S102']
						return Response(status=status.HTTP_201_CREATED,data = status_dict)
					else:
						status_dict["status_desc"] = masterconf.error_codes_desc['E103']
						return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
				else:
					return Response(status=status.HTTP_400_BAD_REQUEST)
			except Exception as e:
				print("Exception: ", e)
				status_dict["level"] = masterconf.system_level
				status_dict["status_code"] = masterconf.system_level_exception_code
				status_dict["status_desc"] = masterconf.system_level_exception
				return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
		else:
			invalid_req = {
				"level": masterconf.request_level,
				"status_code": masterconf.request_level_exception_code,
				"status_desc": masterconf.request_level_exception
			}
			return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)
