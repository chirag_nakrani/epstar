from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.ReportList import ReportListController
from rest_framework.pagination import PageNumberPagination
from rest_framework import generics
import logging
from  Common import masterconf

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class Report(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination ##Pagination Class Added

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		logger = logging.getLogger(__name__)
		try:
			logger.info('Start of ProjectFeederBankCraMappingList api.')
			report_list = ReportListController.ReportListController()
			response_list = report_list.handle_list_req(request)
			logger.info('Response count: %s',len(response_list))
			return Response(status=status.HTTP_201_CREATED,data=response_list)
		except Exception as e:
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
			logger.error('Error occured: %s', e)
			logger.error('Error response: %s', invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=invalid_req)
