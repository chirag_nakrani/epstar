from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.Indent import IndentController
from Controller.IndentPDF import ViewPDFController
from rest_framework.pagination import PageNumberPagination
from rest_framework import generics
from Common import masterconf,db_queries_properties
import logging

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class Indent_List(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination ##Pagination Class Added

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		try:
			indt_cntroller = IndentController.IndentController()
			response_list = indt_cntroller.handle_indent_list_req(request) #ES1-T129_PaginationAdded start
			paginator = self.pagination_class()
			page = paginator.paginate_queryset(response_list, request)
			return paginator.get_paginated_response(page)
			# ES1-T129_PaginationAdded end
			#return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			response_obj['status'] = 'failure'
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class List_Count(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		try:
			indt_cntroller = IndentController.IndentController()
			response_list = indt_cntroller.handle_indent_list_req(request) #ES1-T129_PaginationAdded start

			active_list = list(filter(lambda x: (x['record_status'] == masterconf.active_status) , response_list))
			pending_list = list(filter(lambda x: (x['record_status'] == masterconf.pending_status) , response_list))
			review_list = list(filter(lambda x: (x['record_status'] == masterconf.review_pending_status) , response_list))

			count_data = {"pending_approval": len(pending_list), "pending_review": len(review_list), "active": len(active_list)}

			return Response(status=status.HTTP_200_OK, data=count_data)

		except Exception as e:

			response_obj['status'] = 'failure'
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Indent_Detail(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		response_obj = {}
		common_util_obj = common_util.common_utils()
		check_req_flag = common_util_obj.validate_indent_detail_req(request.data)
		if check_req_flag:
			try:
				indt_cntroller = IndentController.IndentController()
				response_list = indt_cntroller.handle_indent_detail_req(request)
				return Response(status=status.HTTP_201_CREATED, data=response_list)
			except Exception as e:
				#print(e)
				response_obj['status'] = 'No Record Found'
				return Response(status=status.HTTP_400_BAD_REQUEST,data=response_obj)
		else:
			return Response(status=status.HTTP_400_BAD_REQUEST, data={"status": "Bad Request"})


class Indent_Revision(APIView):

	## This API is used to revise the generated Indent

	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)


	def post(self, request, format=None):
		response_obj = {}
		# common_util_obj = common_util.common_utils()
		# check_req_flag = common_util_obj.validate_indent_revision_req(request.data)
		try:
			status_dict = {}
			indt_cntroller = IndentController.IndentController()
			code = indt_cntroller.handle_indent_revision_req(request)
			if code == masterconf.indent_gen_success_sp:
				status_dict["status_code"] = code
				status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
				return Response(status=status.HTTP_201_CREATED,data=status_dict)
			elif code == masterconf.indent_gen_fail_sp:
				status_dict["status_code"] = code
				status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
				return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
			elif code == masterconf.indent_gen_no_records_sp:
				status_dict["status_code"] = code
				status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
				return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
			elif code == masterconf.indent_request_success_submit:
				status_dict["status_code"] = code
				status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
				return Response(status=status.HTTP_201_CREATED,data=status_dict)
			elif code == masterconf.Record_Already_Exists:
				status_dict["status_code"] = code
				status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
				return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
			else:
				return Response(status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print( " asasasasasaasasasasaskjdohdjlkqsjd ",e)
			response_obj['status_code'] = masterconf.indent_generation_fail
			response_obj["status_desc"] = masterconf.error_codes_desc[response_obj['status_code']]
			return Response(status=status.HTTP_400_BAD_REQUEST,data=response_obj)


class IndentApprovalRejection(APIView):

	## This API is used to approve and reject records for Indent

	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		print("Hey")
		check_req_flag = common_util_obj.validate_indent_status_req(request.data)
		print("check_req_flag :: ",check_req_flag)
		status_dict = {}
		try:
			if check_req_flag:
				indt_cntroller = IndentController.IndentController()
				code = indt_cntroller.handle_indent_status_change_req(request)
				print("code generated from IndentApprovalRejection : ", code)
				if code == masterconf.indent_gen_success_sp or code == masterconf.status_approval_success or code == masterconf.status_rejection_success:
					status_dict["status_code"] = code
					status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
					return Response(status=status.HTTP_201_CREATED, data=status_dict)
				elif code == masterconf.indent_gen_fail_sp:
					status_dict["status_code"] = code
					status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
					return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
				elif code == masterconf.indent_gen_no_records_sp or code == masterconf.failed_to_generate_pdf:
					status_dict["status_code"] = code
					status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
					return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
			else:
				return Response(status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			print(e)
			return Response(status=status.HTTP_400_BAD_REQUEST)


class Generate_Indent(APIView):

	## This API is used to generated Indent

	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)


	def post(self, request, format=None):
		response_obj = {}
		common_util_obj = common_util.common_utils()
		check_req_flag = common_util_obj.validate_indent_generate_req(request.data)
		if check_req_flag:
			try:
				status_dict = {}
				indt_cntroller = IndentController.IndentController()
				code = indt_cntroller.handle_indent_generate_req(request)
				if code == masterconf.indent_gen_success_sp:
					status_dict["status_code"] = code
					status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
					return Response(status=status.HTTP_201_CREATED,data=status_dict)
				else:
					status_dict["status_code"] = code
					status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
					return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
			except Exception as e:
				#print( " asasasasasaasasasasaskjdohdjlkqsjd ",e)
				response_obj['status_code'] = masterconf.indent_generation_fail
				response_obj["status_desc"] = masterconf.error_codes_desc[response_obj['status_code']]
				return Response(status=status.HTTP_400_BAD_REQUEST,data=response_obj)
		else:
			return Response(status=status.HTTP_400_BAD_REQUEST)

#### view pdf on indent page ####
class Indent_PDF(APIView):

	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)


	def post(self, request, format=None):
		response_obj = {}
		common_util_obj = common_util.common_utils()
		try:
			status_dict = {}
			indent_pdf_cntroller = ViewPDFController.ViewPDFController()
			pdf = indent_pdf_cntroller.handle_indent_pdf_view_req(request)
			if 'status_code' in pdf:
				return Response(status=status.HTTP_400_BAD_REQUEST, data=pdf)
			else:
				return pdf
		except Exception as e:
			response_obj['status_code'] = masterconf.error_pdf_view
			response_obj["status_desc"] = masterconf.error_pdf_view_text
			return Response(status=status.HTTP_400_BAD_REQUEST,data=response_obj)


class Indent_Edit(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		response_obj = {}
		try:
			status_dict = {}
			indt_cntroller = IndentController.IndentController()
			status_dict = indt_cntroller.handle_indent_edit_req(request)
			if status_dict['status_code'] == masterconf.success_updated:
				status_dict['status_desc'] = masterconf.success_codes_desc[status_dict['status_code']]
				return Response(status=status.HTTP_201_CREATED, data=status_dict)
			elif status_dict['status_code'] == masterconf.failed_to_generate_pdf_save_send:
				status_dict['status_desc'] = masterconf.error_codes_desc[status_dict['status_code']]
				return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
			elif status_dict['status_code'] == masterconf.success_edit_save_send:
				status_dict['status_desc'] = masterconf.success_codes_desc[status_dict['status_code']]
				return Response(status=status.HTTP_201_CREATED, data=status_dict)
			else:
				status_dict['status_desc'] = masterconf.error_codes_desc[status_dict['status_code']]
				return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
		except Exception as e:
			#print(e)
			response_obj['status_desc'] = 'Unable to update records'
			return Response(status=status.HTTP_400_BAD_REQUEST,data=response_obj)

class Indent_Holiday(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination ##Pagination Class Added

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		response_list = list()
		response_obj = {}
		try:
			logger.info('Start of indent holiday list api')
			logger.info('Request parameters received: %s', request.data)
			indt_cntroller = IndentController.IndentController()
			response_list = indt_cntroller.handle_indent_holiday_list_req(request) #ES1-T129_PaginationAdded start
			logger.info('Length Response from indent holiday list api: %s', len(response_list))
			paginator = self.pagination_class()
			page = paginator.paginate_queryset(response_list, request)
			return paginator.get_paginated_response(page)
		except Exception as e:
			response_obj['status_code'] = masterconf.list_error_code
			response_obj['status_text'] = masterconf.no_data_found
			logger.error(('Exception occured for finding holiday in indent_holiday: %s', e))
			return Response(status=status.HTTP_404_NOT_FOUND, data=response_obj)

class Indent_Holiday_Edit(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination ##Pagination Class Added

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		response_list = list()
		response_obj = {}
		try:
			logger.info('Start of indent holiday edit api')
			logger.info('Request parameters received: %s', request.data)
			indt_cntroller = IndentController.IndentController()
			response_list = indt_cntroller.handle_indent_holiday_edit_req(request) #ES1-T129_PaginationAdded start
			logger.info('Response from indent holiday edit api: %s',response_list)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			response_obj['status_code'] = masterconf.edit_error_code
			response_obj['status_text'] = masterconf.edit_error_text
			logger.error(('Exception occured for editing holiday in indent_holiday: %s', e))
			return Response(status=status.HTTP_404_NOT_FOUND, data=response_obj)

class Indent_Holiday_Create(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination ##Pagination Class Added

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		response_list = list()
		status_dict = {}
		try:
			logger.info('Start of indent holiday create api')
			logger.info('Request parameters received: %s', request.data)
			indt_cntroller = IndentController.IndentController()
			code = indt_cntroller.handle_indent_holiday_generate_req(request)
			if code == masterconf.hoiday_success_code:
				status_dict["status_code"] = code
				status_dict["status_desc"] = masterconf.hoiday_success_text
				return Response(status=status.HTTP_201_CREATED, data=status_dict)
			else:
				status_dict["status_code"] = code
				status_dict["status_desc"] = masterconf.hoiday_error_text
				return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
		except Exception as e:
			status_dict['status_code'] = masterconf.hoiday_error_code
			status_dict['status_text'] = masterconf.hoiday_error_text
			logger.error('Exception occured for creating holiday in indent_holiday: %s', e)
			return Response(status=status.HTTP_404_NOT_FOUND, data=status_dict)