from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.SytemSettings import SystemSettingsController
from rest_framework import generics
from Common import masterconf,db_queries_properties

class SystemSettings(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		#### checking if all the parameters are passed or not ####
		check_flag = common_util_obj.validate_systemSettings_ParamsReq(request.data)
		if check_flag:
			system_settings_controller = SystemSettingsController.SystemSettingsController()
			try:
				#### creating system settings ####
				response_list = system_settings_controller.handle_system_settings_create_req(request)
				return Response(status=status.HTTP_201_CREATED, data=response_list)
			except Exception as e:
				#print(e)
				response_obj['status'] = masterconf.system_level_exception
				return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=response_obj)
		else:
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)



#### system settings approve function ####
class SystemSettingsApprove(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		system_settings_controller = SystemSettingsController.SystemSettingsController()
		try:
			response_list = system_settings_controller.handle_system_settings_approve_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)

#### system settings reject function ####
class SystemSettingsReject(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		system_settings_controller = SystemSettingsController.SystemSettingsController()
		try:
			response_list = system_settings_controller.handle_system_settings_reject_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)

#### system settings list function ####
class SystemSettingsList(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		system_settings_controller = SystemSettingsController.SystemSettingsController()
		try:
			response_list = system_settings_controller.handle_system_settings_list_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)