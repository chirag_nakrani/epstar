from rest_framework.permissions import IsAuthenticated
import logging

from Common import masterconf
from Common.CommonFunctions import common_util
from Controller.ConfigurableParams import ConfigParamController
from Controller.Master import ATMMasterController , Feeder_master_controller , CRA_master_controller , \
    AMS_ATM_Master_Controller , AMSAPI_Controller
from Controller.ReferenceData import BrandBillCapacity , CRAEmpaneled , CRAVaultMaster , EscalationMatrix , \
    CRAMailMaster , \
    CypherCode, AtmList, ATMRevisionCashAvailability, GeneralLedgerAccountNumber, IndentHoliday
from Controller.Routine import BaseCBRController , BaseCDRController , BaseVCBControllerEOD , BaseC3RMISController , \
    DailyLoad_RepCntrlr , IMSController
from Controller.Signature import Signature
from Controller.Ticket import mailnotification
from Controller.ViewData import BankInfoController , common_bankfeeder_filterinfo , C3R_ApprPend
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.parsers import MultiPartParser , FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from django.contrib.auth.models import User, Group, Permission
from Model.models import ticket_category_master, workflow_master

class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


class Fileupload ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        logger = logging.getLogger(__name__)
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_upload_req ( request )
        logger.info("In FIle upload method of Routine data")
        logger.info(" request params are ::: %s ", request.POST)
        mail_obj = mailnotification.MailNotification ( )
        if check_req_flag :
            logger.info ( "Validated Input request." )
            filehandle = request.FILES['file']
            filehandle_name = request.FILES['file'].name
            logger.info("Filename is ::: %s",filehandle_name)
            str_file_extention = ''
            userinfo = request.userinfo
            input_filepath = common_util_obj.divide_store_file ( request )  # ES1-T218 - Folder re-structuring done
            try :
                if request.POST['file_type'] == masterconf.file_type_cbr :
                    logger.info ( "Input file_type is CBR file" )
                    basecbrcontroller = BaseCBRController.BaseCBRController ( )
                    status_dict = basecbrcontroller.handle_upload_request ( request.POST , input_filepath ,
                                                                            filehandle_name ,
                                                                            str_file_extention , userinfo )
                    logger.info("status_dict returned from  basecbrcontroller::: %s", status_dict)
                    ##################################### WHEN FORMAT VALIDATION GETS FAILED################################################################
                    if status_dict["level"].lower ( ) == masterconf.format_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) != masterconf.format_val_success_status.lower ( ) :
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    #################################### WHEN BOTH FORMAT AND DATA VALIDATION IS SUCCESSFULL############################################
                    if status_dict["level"].lower ( ) == masterconf.data_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) == masterconf.data_uploaded_successful.lower ( ) or status_dict[
                        "file_status"].lower ( ) == masterconf.partial_valid_file.lower ( ) :
                        del status_dict["file_status"]
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    #################################### WHEN FORMAT VALIDATION IS SUCCESSFUL BUT FILE VALDATION IS PARTIAL############################
                    else :
                        if status_dict["file_status"] :
                            del status_dict["file_status"]
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                elif request.POST['file_type'] == masterconf.file_type_dispense :
                    logger.info("Input file_type is Dispense file")
                    basecdrcontroller = BaseCDRController.BaseCDRController ( )
                    status_dict = basecdrcontroller.handle_upload_request ( request.POST , input_filepath ,
                                                                            filehandle_name ,
                                                                            str_file_extention , userinfo )
                    logger.info("status_dict returned from  basecdrcontroller::: %s", status_dict)
                    if status_dict["level"].lower ( ) == masterconf.format_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) != masterconf.format_val_success_status.lower ( ) :
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    if status_dict["level"].lower ( ) == masterconf.data_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) == masterconf.data_uploaded_successful.lower ( ) or status_dict[
                        "file_status"].lower ( ) == masterconf.partial_valid_file.lower ( ) :
                        del status_dict["file_status"]
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else :
                        if status_dict["file_status"] :
                            del status_dict["file_status"]
                            ticket_id = mail_obj.handle_mail_ticket_processing (
                                request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                elif request.POST['file_type'].lower ( ) == masterconf.file_type_vcb_eod.lower ( ) :
                    logger.info("Input file_type is VCB file")
                    basevcbcontrollerEOD = BaseVCBControllerEOD.BaseVCBControllerEOD ( )
                    status_dict = basevcbcontrollerEOD.handle_upload_request ( request.POST , input_filepath ,
                                                                               filehandle_name , str_file_extention ,
                                                                               userinfo )
                    logger.info("status_dict returned from  basevcbcontrollerEOD::: %s", status_dict)
                    if status_dict["level"].lower ( ) == masterconf.format_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) != masterconf.format_val_success_status.lower ( ) :
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    elif status_dict["level"].lower ( ) == masterconf.data_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) == masterconf.data_validated_successful_vcb.lower ( ) or (
                            masterconf.partial_valid_file.lower ( ) in status_dict["file_status"].lower ( )) :

                        if status_dict["file_status"] :
                            del status_dict["file_status"]
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else :
                        if status_dict["file_status"] :
                            del status_dict["file_status"]
                            ticket_id = mail_obj.handle_mail_ticket_processing (
                                request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                elif request.POST['file_type'] == masterconf.file_type_C3R_VMIS :
                    logger.info("Input file_type is C3R_VMIS file")
                    baseC3RMIScontroller = BaseC3RMISController.BaseC3RMISController ( )
                    status_dict = baseC3RMIScontroller.handle_upload_request ( request.POST , input_filepath ,
                                                                               filehandle_name , str_file_extention ,
                                                                               userinfo )
                    logger.info("status_dict returned from  baseC3RMIScontroller::: %s", status_dict)
                    if status_dict["level"].lower ( ) == masterconf.format_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) != masterconf.format_val_success_status.lower ( ) :
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    if status_dict["level"].lower ( ) == masterconf.data_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) == masterconf.data_validated_successful.lower ( ) or status_dict[
                        "status_text"].lower ( ) == masterconf.partial_valid_file.lower ( ) :
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    if status_dict["level"].lower ( ) == masterconf.data_level.lower ( ) and status_dict["status_code"] == masterconf.error_inserting_record:
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                elif request.POST['file_type'] == masterconf.file_type_Daily_Loading_Report :
                    logger.info("Input file_type is Daily_Loading_Report file")
                    dailyload_repcntrlr = DailyLoad_RepCntrlr.DailyLoad_RepCntrlr ( )
                    status_dict = dailyload_repcntrlr.handle_upload_request ( request.POST , input_filepath ,
                                                                              filehandle_name , str_file_extention ,
                                                                              userinfo );
                    logger.info("status_dict returned from  dailyload_repcntrlr::: %s", status_dict)
                    if status_dict["level"].lower ( ) == masterconf.format_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) != masterconf.format_val_success_status.lower ( ) :
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    if status_dict["level"].lower ( ) == masterconf.data_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) == masterconf.data_validated_successful.lower ( ) :
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else :
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                elif request.POST['file_type'].lower ( ) == masterconf.file_type_ims.lower ( ) :
                    logger.info("Input file_type is IMS file")
                    iMSController = IMSController.IMSController ( )
                    status_dict = iMSController.handle_upload_request ( request.POST , input_filepath ,
                                                                        filehandle_name ,
                                                                        str_file_extention , userinfo )
                    logger.info("status_dict returned from  iMSController::: %s", status_dict)
                    if status_dict["level"].lower ( ) == masterconf.format_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) != masterconf.format_val_success_status.lower ( ) :
                        # ticket_id = mail_obj.handle_mail_ticket_processing(request)
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    if status_dict["level"].lower ( ) == masterconf.data_level.lower ( ) and status_dict[
                        "status_text"].lower ( ) == masterconf.data_uploaded_successful.lower ( ) or status_dict[
                        "file_status"].lower ( ) == masterconf.partial_valid_file.lower ( ) :
                        del status_dict["file_status"]
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else :
                        if status_dict["file_status"] :
                            del status_dict["file_status"]
                        ticket_id = mail_obj.handle_mail_ticket_processing ( request )  # ES1-T189_ticket_mail_method
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            except Exception as e :
                status_dict["level"] = masterconf.system_level
                status_dict["status_text"] = masterconf.system_level_exception
                # mail_obj.notify_mail_operation_status("upload",recipient,masterconf.upload_fail_subject,masterconf.upload_failed_msg)
                logger.error ( 'Exception occured while uploading Routine file %s' ,e )
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
        else :
            invalid_req = {
                "level" : masterconf.request_level ,
                "status_text" : masterconf.request_level_exception
            }
            logger.error('Invalid Request... Please check request params')
            return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )


class FileInfo ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_fileinfo_req ( request )
        username = str ( request.userinfo['username'].username )
        if check_req_flag :
            print("Request is Valid")
            bankInfoController = BankInfoController.BankInfoController ( )
            try :
                status_dict = bankInfoController.handle_bankname_request ( request.data , username )
                if status_dict['api_status'] == masterconf.api_status_Success :
                    del status_dict['api_status']
                    return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                else :
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            except Exception as e :
                print("Exception from FileInfo: " , e)
                status_dict["level"] = masterconf.system_level;
                status_dict["status_text"] = masterconf.system_level_exception;
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
        else :
            invalid_req = {
                "level" : masterconf.request_level ,
                "status_text" : masterconf.request_level_exception
            }
            return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )


class MasterUpload ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_master_req ( request.POST )
        logger = common_util_obj.initiateLogger ( )
        logger.setLevel ( logging.INFO )
        logger.info ( "In post method of Master Upload" )
        print("request.POST.keys() : {}".format ( request.POST.keys ( ) ))
        mail_obj = mailnotification.MailNotification ( )
        if request.POST and request.FILES :
            if check_req_flag :
                print("Request is Valid")
                filehandle = request.FILES['file']
                filehandle_name = request.FILES['file'].name
                print("filehandle: {}".format ( filehandle ))
                print("filehandle_name : {}".format ( filehandle_name ))
                # input_filepath = os.path.join(masterconf.upload_file_path1, filehandle_name)
                # print ("input_filepath : ",input_filepath)
                input_filepath = common_util_obj.divide_store_file ( request )  # ES1-T218 - Folder re-structuring done
                print("request.POST['file_type'] : {}".format ( request.POST['file_type'] ))
                master_category = masterconf.master_category
                try :
                    ###############uploading Master Feeder File ##########################################################################
                    if request.POST['file_type'].lower ( ) == (masterconf.file_type_feeder).lower ( ) :
                        print("Inside MasterUpload")
                        feeder_master_controller = Feeder_master_controller.Feeder_master_controller ( )
                        status_dict = feeder_master_controller.handle_master_upload ( request , input_filepath )
                        print("printing Status Dict from MstrRefUpload: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_mstr_upload :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif str ( status_dict["status_code"] ).startswith ( 'E' ) :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict['status_code'] == masterconf.status_code_for_duplicate_data:
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            mail_response = mail_obj.handle_mail_master_upload_notification ( request.data ,
                                                                                              request.userinfo[
                                                                                                  'username'].username ,
                                                                                              request.userinfo[
                                                                                                  'reference_id'] )
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    #########################################################################################################################
                    #################################Uploading atm config file ##############################################################
                    elif request.POST['file_type'].lower ( ) == masterconf.file_type_atm_cnfg.lower ( ) :
                        atm_master_controller = ATMMasterController.ATMMasterController ( )
                        status_dict = atm_master_controller.handle_master_upload ( request , input_filepath )
                        print("Printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_mstr_upload :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif str ( status_dict["status_code"] ).startswith ( 'E' ) :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict['status_code'] == masterconf.status_code_for_duplicate_data:
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            mail_response = mail_obj.handle_mail_master_upload_notification ( request.data ,
                                                                                              request.userinfo[
                                                                                                  'username'].username ,
                                                                                              request.userinfo[
                                                                                                  'reference_id'] )
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    #########################################################################################################################
                    ########################################uploading cra feasibility file ##################################################
                    elif request.POST['file_type'].lower ( ) == (masterconf.file_type_cra_mstr).lower ( ) :
                        cra_master_controller = CRA_master_controller.CRA_master_controller ( )
                        status_dict = cra_master_controller.handle_master_upload ( request , input_filepath )
                        if status_dict["status_code"] == masterconf.format_val_failed_mstr_upload :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif str ( status_dict["status_code"] ).startswith ( 'E' ) :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict['status_code'] == masterconf.status_code_for_duplicate_data:
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            mail_response = mail_obj.handle_mail_master_upload_notification ( request.data ,
                                                                                              request.userinfo[
                                                                                                  'username'].username ,
                                                                                              request.userinfo[
                                                                                                  'reference_id'] )
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    #########################################################################################################################
                    ##############If fileupload is for AMS_ATM ############################################################################
                    elif request.POST['file_type'].lower ( ) == (masterconf.file_type_ams_atm).lower ( ) :
                        aMS_ATM_Master_Controller = AMS_ATM_Master_Controller.AMS_ATM_Master_Controller ( )
                        status_dict = aMS_ATM_Master_Controller.handle_master_upload ( request , input_filepath )
                        # print ("#printing Status Dict: {}".format(status_dict))
                        if status_dict["status_code"] == masterconf.format_val_failed_mstr_upload :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif str ( status_dict["status_code"] ).startswith ( 'E' ) :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict['status_code'] == masterconf.status_code_for_duplicate_data:
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            mail_response = mail_obj.handle_mail_master_upload_notification ( request.data ,
                                                                                              request.userinfo[
                                                                                                  'username'].username ,
                                                                                              request.userinfo[
                                                                                                  'reference_id'] )
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    ######################################################################################################################
                    else :
                        status_dict["level"] = masterconf.type_of_file
                        status_dict["status_desc"] = masterconf.improper_file_type
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                except Exception as e :
                    print("Exception from master upload: " , e)
                    status_dict["level"] = masterconf.system_level
                    status_dict["status_code"] = masterconf.system_level_exception_code
                    status_dict["status_desc"] = masterconf.system_level_exception
                    logger.setLevel ( logging.ERROR )
                    logger.error ( 'Exception occured while uploading Master filr' , exc_info=True )
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            else :
                invalid_req = {
                    "level" : masterconf.request_level ,
                    "status_code" : masterconf.request_level_exception_code ,
                    "status_desc" : masterconf.request_level_exception
                }
                logger.setLevel ( logging.ERROR )
                logger.error ( "Invalid Request" )
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )

        else :
            logger.setLevel ( logging.ERROR )
            logger.error ( "Bad Request" )
            return Response ( status=status.HTTP_400_BAD_REQUEST )


####################To extract the information of bank and feeder relation######################
class bankFeederCommonInfo ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_filterInfo ( request )
        username = str ( request.userinfo['username'].username )
        if check_req_flag :
            print("Request is Valid")
            common_Bankfeeder_filterinfo = common_bankfeeder_filterinfo.common_bankfeeder_filterinfo ( )
            try :
                status_dict = common_Bankfeeder_filterinfo.handle_bankname_request ( request.data , username )
                if status_dict['api_status'] == masterconf.api_status_Success :
                    del status_dict['api_status']
                    return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                else :
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            except Exception as e :
                print("Exception from bankFeederCommonInfo: " , e);
                status_dict["level"] = masterconf.system_level;
                status_dict["status_text"] = masterconf.system_level_exception;
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )


        else :
            invalid_req = {
                "level" : masterconf.request_level ,
                "status_text" : masterconf.request_level_exception
            }
            return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )


class ReferenceDataUpload ( APIView ) :
    ## This is the Main API endpoint class for reference data upload

    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :

        # This is the post method for File upload using Post Method

        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_reference_req ( request.POST )
        print("check_req_flag from  ReferenceDataUpload: {}".format ( check_req_flag ))
        if request.POST and request.FILES :
            if check_req_flag :
                print("Input Request is Valid")
                filehandle = request.FILES['file']
                filehandle_name = request.FILES['file'].name
                print("filehandle: {}".format ( filehandle ))
                print("filehandle_name : {}".format ( filehandle_name ))
                # input_filepath = os.path.join(masterconf.upload_file_path1, filehandle_name)
                # print ("input_filepath : ",input_filepath)
                input_filepath = common_util_obj.divide_store_file ( request )  # ES1-T218 - Folder re-structuring done
                print("request.POST['file_type'] : {}".format ( request.POST['file_type'] ))
                try :
                    if (request.POST['file_type']).upper ( ) == masterconf.file_type_RD_brand_BillCapacity :
                        print("Going for uploading of brand bill capacity.")
                        brandBillCapacity = BrandBillCapacity.BrandBillCapacity_Controller ( )
                        status_dict = brandBillCapacity.handle_reference_upload ( request , input_filepath )
                        print("status dict returned from BrandBillCapacity_Controller is : " ,
                              status_dict["status_code"])
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[
                                masterconf.format_val_failed_upload]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    elif request.POST['file_type'] == masterconf.file_type_vault_master :
                        craVaultMaster = CRAVaultMaster.CRAVaultMaster_Controller ( )
                        status_dict = craVaultMaster.handle_reference_upload ( request , input_filepath )
                        # print ("#printing Status Dict: {}".format(status_dict))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[
                                masterconf.format_val_failed_upload]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    elif request.POST['file_type'].upper ( ) == masterconf.file_type_Empaneled.upper ( ) :
                        craEmpaneled = CRAEmpaneled.CRAEmpaneled_Controller ( )
                        status_dict = craEmpaneled.handle_reference_upload ( request , input_filepath )
                        # print ("#printing Status Dict: {}".format(status_dict))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[
                                masterconf.format_val_failed_upload]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    elif request.POST['file_type'] in masterconf.file_type_escalation_matrix :
                        escalation_matrix = EscalationMatrix.EscalationMatrix_Controller ( )
                        status_dict = escalation_matrix.handle_reference_upload ( request , input_filepath )
                        # print ("#printing Status Dict: {}".format(status_dict))
                        if status_dict["status_code"] == masterconf.format_val_failed_mstr_upload :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict['status_code'] == masterconf.status_code_for_duplicate_data:
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )

                    ################# uploading mail master reference data ##########################
                    elif request.POST['file_type'].upper ( ) == masterconf.file_type_mail_master.upper ( ) :
                        mail_master = CRAMailMaster.CRAMailMaster_Controller ( )
                        status_dict = mail_master.handle_reference_upload ( request , input_filepath )
                        # print ("#printing Status Dict: {}".format(status_dict))
                        if status_dict["status_code"] == masterconf.format_val_failed_mstr_upload :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    #############Going for extracting data from cypher code file#####################################
                    elif request.POST['file_type'].upper ( ) == masterconf.file_type_cypher_code.upper ( ) :
                        cypherCode_Controller = CypherCode.CypherCode_Controller ( )
                        status_dict = cypherCode_Controller.handle_reference_upload ( request , input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_mstr_upload :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )

################################Revision via file upload################################
                    elif request.POST['file_type'].upper ( ) == masterconf.revision_via_file_upload_file_type.upper ( ) :
                        atmList_Controller = AtmList.AtmList_Controller ( )
                        status_dict = atmList_Controller.handle_reference_upload ( request , input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            print("status dict having failed format validation : " , status_dict)
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.data_val_success_upload :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        elif status_dict["status_code"] == masterconf.status_code_for_duplicate_data :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    elif request.POST['file_type'].upper ( ) == masterconf.atm_revision_cash_availability_file_type.upper ( ) :
                        atmRevisionCashAvailability_Controller = ATMRevisionCashAvailability.ATMRevisionCashAvailability_Controller( )
                        status_dict = atmRevisionCashAvailability_Controller.handle_reference_upload ( request , input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            print("status dict having failed format validation : " , status_dict)
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.data_val_success_upload :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        elif status_dict["status_code"] == masterconf.status_code_for_duplicate_data :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    elif request.POST['file_type'].upper ( ) == masterconf.general_ledger_account_number_file_type.upper ( ) :
                        general_ledger_account_number_Controller = GeneralLedgerAccountNumber.GeneralLedgerAccountNumber_Controller( )
                        status_dict = general_ledger_account_number_Controller.handle_reference_upload ( request , input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.error_inserting_record :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            print("status dict having failed format validation : " , status_dict)
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.data_val_success_upload :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        elif status_dict["status_code"] == masterconf.status_code_for_duplicate_data :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    elif request.POST['file_type'].upper ( ) == masterconf.indent_holiday_type.upper ( ) :
                        indent_holiday_Controller = IndentHoliday.IndentHoliday_Controller( )
                        status_dict = indent_holiday_Controller.handle_reference_upload ( request , input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["level"].lower() == masterconf.format_level.lower() and status_dict[
                            "status_text"].lower() != masterconf.format_val_success_status.lower():
                            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
                        #################################### WHEN BOTH FORMAT AND DATA VALIDATION IS SUCCESSFULL############################################
                        if status_dict["level"].lower() == masterconf.data_level.lower() and status_dict[
                            "status_text"].lower() == masterconf.data_uploaded_successful.lower() or status_dict[
                            "file_status"].lower() == masterconf.partial_valid_file.lower():
                            del status_dict["file_status"]
                            return Response(status=status.HTTP_201_CREATED, data=status_dict)
                        #################################### WHEN FORMAT VALIDATION IS SUCCESSFUL BUT FILE VALDATION IS PARTIAL############################
                        else:
                            if status_dict["file_status"]:
                                del status_dict["file_status"]
                            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
                        # if status_dict["status_code"] == masterconf.indent_holiday_success_code :
                        #     status_dict["status_desc"] = masterconf.indent_holiday_success_text
                        #     return Response(status=status.HTTP_201_CREATED, data=status_dict)
                        # elif status_dict["status_code"] == masterconf.indent_holiday_error_code :
                        #     status_dict["status_desc"] = masterconf.indent_holiday_error_text
                        #     return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        # elif status_dict["status_code"] == masterconf.indent_holiday_error_name_not_found_code :
                        #     status_dict["status_desc"] = masterconf.indent_holiday_error_name_not_found_text
                        #     return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        # else :
                        #     status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                        #     return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )

######################################################################################################
                    else :
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                except Exception as e :
                    print("Exception: " , e)
                    status_dict["level"] = masterconf.system_level
                    status_dict["status_code"] = masterconf.system_level_exception_code
                    status_dict["status_desc"] = masterconf.system_level_exception
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            else :
                invalid_req = {
                    "level" : masterconf.request_level ,
                    "status_code" : masterconf.request_level_exception_code ,
                    "status_desc" : masterconf.request_level_exception
                }
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )

        else :
            return Response ( status=status.HTTP_400_BAD_REQUEST )


class Auth_Signature ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_signature_req ( request.POST )
        print("check_req_flag::: " , check_req_flag)
        # reference_id = request.userinfo['reference_id']
        userinfo = request.userinfo
        if request.POST and request.FILES :
            if check_req_flag :
                print("Request is Valid")
                filehandle = request.FILES['physical_signature']
                filehandle_name = request.FILES['physical_signature'].name
                print("filehandle: {}".format ( filehandle ))
                print("filehandle_name : {}".format ( filehandle_name ))
                # signature_filepath = os.path.join(masterconf.upload_signature_path, filehandle_name)
                # print ("signature_filepath : ",signature_filepath)
                signature_filepath = common_util_obj.divide_store_file (
                    request )  # ES1-T218 - Folder re-structuring done
                try :
                    if request.POST['signatory_name'] is not None :
                        signature = Signature.Signature ( )
                        status_dict = signature.handle_signature_upload ( request , signature_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_text"] == masterconf.signature_available :
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        elif status_dict["status_text"] == masterconf.signature_not_available :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        else :
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                except Exception as e :
                    # print("Exception: ", e)
                    status_dict["level"] = masterconf.system_level
                    status_dict["status_code"] = masterconf.system_level_exception_code
                    status_dict["status_desc"] = masterconf.system_level_exception
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            else :
                invalid_req = {
                    "level" : masterconf.request_level ,
                    "status_code" : masterconf.request_level_exception_code ,
                    "status_desc" : masterconf.request_level_exception
                }
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )

        else :
            return Response ( status=status.HTTP_400_BAD_REQUEST )


#####################################################API to convert the record status of C3R file from ApprovalPending to Active or Rejected################
class ApprovalPending ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_approve_req ( request.data )
        if check_req_flag :
            try :
                if request.data[
                    'data_for_type'] == masterconf.file_type_c3r_api :  # and request.data['record_status'] == masterconf.record_status_C3R_AP:
                    c3R_ApprPend = C3R_ApprPend.C3R_ApprPend ( )
                    status_dict = c3R_ApprPend.handle_ApprPendAPI ( request.data )
                    if status_dict["status_text"] == masterconf.C3RApi_statusText :
                        del status_dict["status_text"]
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else :
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                else :
                    status_dict["status_text"] = masterconf.c3rapi_failure
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            except Exception as e :
                print("Exception from ApprovalPending: " , e)
                status_dict["level"] = masterconf.system_level
                status_dict["status_code"] = masterconf.system_level_exception_code
                status_dict["status_desc"] = masterconf.system_level_exception
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
        else :
            invalid_req = {
                "level" : masterconf.request_level ,
                "status_code" : masterconf.request_level_exception_code ,
                "status_desc" : masterconf.request_level_exception
            }
            return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )


#######################Extracting data from API fo AMS Data#######################################
class MasterAMSApiData ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_ams_api_req ( request.data )
        print("check_req_flag from MasterAMSApiData : " , check_req_flag)
        if check_req_flag :
            try :
                if request.data['file_type'].lower ( ) == masterconf.ams_master.lower ( ) :
                    aMSAPI_Controller = AMSAPI_Controller.AMSAPI_Controller ( )
                    status_dict = aMSAPI_Controller.handle_ams_api_data_upload ( request )
                    if status_dict["status_text"] == masterconf.success_codes_desc["S107"]:
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else :
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                else :
                    status_dict['text'] = masterconf.incorrect_file_type
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            except Exception as e :
                print("Exception: " , e)
                status_dict["level"] = masterconf.system_level
                status_dict["status_code"] = masterconf.system_level_exception_code
                status_dict["status_desc"] = masterconf.system_level_exception
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
        else :
            invalid_req = {
                "level" : masterconf.request_level ,
                "status_code" : masterconf.request_level_exception_code ,
                "status_desc" : masterconf.request_level_exception
            }
            return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )


class CashAvailability ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        userinfo = request.userinfo
        if request.POST and request.FILES :
            try :
                if request.POST['file_type'].lower ( ) == masterconf.cash_avail_file_type.lower ( ) :
                    filehandle = request.FILES['file']
                    filehandle_name = request.FILES['file'].name
                    # input_filepath = os.path.join(masterconf.upload_file_path1, filehandle_name)
                    input_filepath = common_util_obj.divide_store_file (
                        request )  # ES1-T218 - Folder re-structuring done
                    cash_avail = ConfigParamController.CashAvailability ( )
                    status_dict = cash_avail.handle_cash_upload ( request , input_filepath )
                    if status_dict['data'] == masterconf.data_updated_for_approval:
                        status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)#Returning success if everything is executed correctly
                    else:
                        status_dict["status_desc"] = masterconf.data_validated_failed
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#returning failure if not success in inserting the new record
                else:
                    return Response ( status=status.HTTP_400_BAD_REQUEST)
            except Exception as e :
                status_dict["level"] = masterconf.system_level
                status_dict["status_code"] = masterconf.system_level_exception_code
                status_dict["status_text"] = masterconf.system_level_exception
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
        else :
            return Response ( status=status.HTTP_400_BAD_REQUEST )
