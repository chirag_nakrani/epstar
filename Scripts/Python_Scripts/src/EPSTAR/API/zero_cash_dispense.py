from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.ZeroCashDispense import ZeroCashDispenseController
from Common import masterconf
from rest_framework.pagination import PageNumberPagination
import logging

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class ZeroCashDispense(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination  ##Pagination Class Added

	def post(self, request, format=None):
		response_list = list()
		logger = logging.getLogger(__name__)
		try:
			#### activity status for specific bank ####
			logger.info('Start of zero cash dispense list api')
			zero_cash_dispense_controller = ZeroCashDispenseController.ZeroCashDispenseController()
			response_list = zero_cash_dispense_controller.handle_list_req(request)
			logger.info('Response list count: %s',len(response_list))
			if response_list:
				paginator = self.pagination_class()
				page = paginator.paginate_queryset(response_list, request)
				return paginator.get_paginated_response(page)
			else:
				status_dict = {
					"status_text": masterconf.data_not_found_text,
					"status_code": masterconf.data_not_found
				}
				logger.info('No Response list generated, sending reponse: %s',status_dict)
				return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=status_dict)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
			logger.error('Error occured: %s',e)
			logger.error('Error response: %s',invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)
