from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework import generics
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.IndentPDF import PDFController
from django.http import HttpResponse,JsonResponse
from Common import masterconf
from Common.CommonFunctions import common_util
import logging
import time
import datetime
from Model.models import indent_pdf_status

class CreatePDF(generics.RetrieveAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,TemplateHTMLRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        response_list = list()
        logger = logging.getLogger(__name__)
        pdf_controller = PDFController.PDFController()
        request_obj = request.data
        response_obj = {}
        username = request.userinfo['username'].username
        reference_id = request.userinfo['reference_id']
        ##### if api request data has email id then send only mail else generate pdf and mail #####
        if 'id' in request_obj and 'indent_order_number' in request_obj:
            logger.info('Start of sending pdf api')
            logger.info('Request parameters received: %s',request.data)
            email_data = {}
            email_data['id'] = request_obj['id']
            email_data['indent_order_number'] = request_obj['indent_order_number']
            email_data['mail_file_name'] = '%s.pdf' % (request_obj['indent_order_number'].replace(' ','_').replace('/','_'))
            status_obj = {}
            status_obj['username'] = username
            status_obj['current_datetime'] = datetime.datetime.fromtimestamp(time.time()).strftime(
                '%Y-%m-%d %H:%M:%S')
            status_obj['reference_id'] = reference_id
            try:
                indent_type = indent_pdf_status.objects.get(indent_order_number=request_obj['indent_order_number'],record_status=masterconf.api_status_Success)
                status_obj['indent_type'] = indent_type.indent_type
                status_obj['bank'] = indent_type.bank_code
                status_obj['feeder_branch'] = indent_type.feeder_branch
                status_obj['project_id'] = indent_type.project_id
                status_obj['cra'] = indent_type.cra
                status_obj['indent_type'] = indent_type.indent_type
                status_obj['file_name'] = indent_type.file_name
            except Exception as e:
                response_obj['status_code'] = masterconf.failure_mail_pdf
                response_obj['status_text'] = masterconf.failure_mail_pdf_text
                logger.error(('Exception occured for finding indent in indent_master: %s',e))
                return Response(status=status.HTTP_404_NOT_FOUND, data=response_obj)
            response_obj['email_status'] = pdf_controller.notify_mail_operation_status(email_data,status_obj)
            return Response(status=status.HTTP_200_OK, data=response_obj)
        else:
            logger.info('Start of creating pdf api')
            logger.info('Request parameters received: %s', request.data)
            try:

                process_list = pdf_controller.handle_pdf_create_req(request)
                response_list = pdf_controller.create_pdf(process_list,username,reference_id)

                #### if response_list not null do operations else return data not found error ####
                if response_list != []:
                    return Response(status=status.HTTP_200_OK, data=response_list)
                else:
                    response_obj['status_code'] = masterconf.data_not_found
                    response_obj['status_text'] = masterconf.data_not_found_text
                    return Response(status=status.HTTP_404_NOT_FOUND, data=response_list)

            except Exception as e:
                response_obj = {}
                response_obj['status_code'] = masterconf.failure_pdf
                response_obj['status_text'] = masterconf.data_not_found_text
                response_obj['exception'] = e
                logger.setLevel(logging.ERROR)
                logger.error('Exception occured while creating pdf and sending email', exc_info=True)
                logger.error(e)
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=response_obj)

#### schedule pdf class ####
### no authentication or permission required to call this class ####
class ScheduleIndentPDF(generics.RetrieveAPIView):
    authentication_classes = ()
    permission_classes = ()
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer, TemplateHTMLRenderer,)
    def post(self,request):
        api_call = CreatePDF.post(self,request)
        return api_call