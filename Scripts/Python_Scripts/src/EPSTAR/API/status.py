from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import time, datetime
from Model.models import holiday_list,holiday_states,holiday_date
from Controller.Routine import BaseCBRController, BaseCDRController, BaseVCBControllerEOD, BaseC3RMISController, DailyLoad_RepCntrlr
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
import logging
from Common.models import data_update_log_master
from Controller.ConfigurableParams import PreQualifyController


class Status(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self,request):
        common_util_obj = common_util.common_utils()
        check_req_flag = common_util_obj.validate_status_update_req(request)
        BaseC3RMISControllerobj = BaseC3RMISController.BaseC3RMISController()
        logger = common_util_obj.initiateLogger()
        logger.setLevel(logging.INFO)
        logger.info("In Status method of API folder")

        if check_req_flag:
            request_status = request.data['status']
            file_type = request.data['file_type']
            data_for_date = request.data['datafor_date_time']
            project_id = 'MOF'
            bank_code = 'ALL'
            systemUser = request.userinfo['username'].username
            referenceid = request.data['created_reference_id']
            result = ''
            status_dict = {}
            # createddate = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
            createddate = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
            if file_type.upper() == masterconf.file_type_C3R:
                result = BaseC3RMISControllerobj.status_update_and_consolidate_C3R(file_type,createddate,bank_code,request_status,referenceid,systemUser,data_for_date,project_id)
                if result == masterconf.Consolidation_Successful:
                    status_dict["result"] = "Data Approved Successfully"
                    return Response(status=status.HTTP_201_CREATED,data = status_dict)
                elif result == masterconf.Not_Consolidated:
                    status_dict["result"] = "Data Rejected Successfully"
                    return Response(status=status.HTTP_201_CREATED,data = status_dict)
                else:
                    status_dict["result"] = masterconf.data_issue
                    return Response(status=status.HTTP_400_BAD_REQUEST, data = status_dict)
            else:
                status_dict["result"] = masterconf.mismatchFileType
                return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
        else:
            status_dict["result"] = masterconf.mismatchparam
            return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)


###########CommentItOut####################
# if file_type.upper() == masterconf.file_type_cbr:
# 				result = BaseCBRControllerobj.status_update_and_consolidate(file_type,createddate,bank_code,request_status,referenceid,systemUser,data_for_date,project_id);
# 			elif file_type.upper() == masterconf.file_type_dispense:
# 				result = BaseCDRControllerobj.status_update_and_consolidate(file_type,createddate,bank_code,request_status,referenceid,systemUser,data_for_date,project_id);
# 			elif file_type.upper() == masterconf.file_type_vcb_eod:
# 				result = BaseVCBControllerEODobj.status_update_and_consolidate(file_type,createddate,bank_code,request_status,referenceid,systemUser,data_for_date,project_id);
# 			elif file_type == masterconf.file_type_Daily_Loading_Report:
# 				result = BaseDailyLoadingRptobj.status_update_and_consolidate_DL(file_type,createddate,bank_code,request_status,referenceid,systemUser,data_for_date,project_id);
# 			elif file_type.upper() == masterconf.file_type_C3R:
# 				result = BaseC3RMISControllerobj.status_update_and_consolidate_C3R(file_type,createddate,bank_code,request_status,referenceid,systemUser,data_for_date,project_id);
#
# 		return Response(status=status.HTTP_201_CREATED)
################################################
class ApproveRejectAll(APIView):   #Class name change i.e. It is used for Master,Reference, Indent
    ## This API is used to approve and reject records for Master and Reference Menus
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        common_util_obj = common_util.common_utils()
        menu = request.data["menu"]
        status_dict = { }
        if menu.lower() == masterconf.file_type_holiday_master.lower():
            code = self.approveRejectHolidayMaster(request)
            if code == masterconf.status_approval_success or code == masterconf.status_rejection_success:
                status_dict['code'] = code
                status_dict['status_desc'] = masterconf.success_codes_desc[code]
                return Response(status=status.HTTP_201_CREATED, data=status_dict)
            else:
                status_dict['code'] = code
                status_dict['status_desc'] = masterconf.error_codes_desc['E104']
                return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
        elif menu.lower() == masterconf.file_type_pre_qualify.lower():
            check_req_flag = common_util_obj.validate_pre_qualify_input_req ( request.data )
            print ("check_req_flag : ",check_req_flag)
            if check_req_flag:
                preQualifyController = PreQualifyController.PreQualifyController()
                status_dict = preQualifyController.handle_modified_request ( request )
                if status_dict['data'] == masterconf.data_updated_for_approval:
                    id_string = ','.join(str(i) for i in status_dict['id_list'])
                    code = common_util_obj.change_status_pre_qualify ( request, id_string )
                    if code == masterconf.status_approval_success or code == masterconf.status_rejection_success:
                        del status_dict['data']
                        del status_dict['id_list']
                        status_dict['code'] = code
                        status_dict['status_desc'] = masterconf.success_codes_desc[code]
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)
                    else:
                        del status_dict['data']
                        del status_dict['id_list']
                        status_dict['code'] = code
                        status_dict['status_desc'] = masterconf.error_codes_desc['E104']
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                else:
                    status_dict['status_desc'] = masterconf.error_codes_desc['E104']
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
            else:
                return Response ( status=status.HTTP_400_BAD_REQUEST )
        else:
            check_req_flag = common_util_obj.validate_status_req(request.data) #Name Changed as per naming convention

            if check_req_flag:
                code = common_util_obj.change_status(request) #Name Changed as per naming convention
                print ("code generated from ApproveRejectAll : ",code)
                if code == masterconf.status_approval_success or code == masterconf.status_rejection_success:
                    status_dict['code'] = code
                    status_dict['status_desc'] = masterconf.success_codes_desc[status_dict['code']]
                    return Response(status=status.HTTP_201_CREATED,data = status_dict)
                else:
                    status_dict['code'] = code
                    status_dict['status_desc'] = masterconf.error_codes_desc['E104']
                    return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)

    def approveRejectHolidayMaster(self, request, format=None):
        common_util_obj = common_util.common_utils()
        print("In ApproveRejectAll method of Holiday Master Method")

        check_req_flag = common_util_obj.validate_status_req_holiday(request.data) #Name Changed as per naming convention
        print(check_req_flag)
        status_dict = {}
        if check_req_flag:
            print("request is valid")
            created_reference_id = request.data['created_reference_id']
            updated_record_status = request.data['updated_record_status']
            reference_id = request.userinfo['reference_id']
            username = request.userinfo['username'].username
            upload_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
            print(request.data.keys())
            if 'payload_list' in request.data.keys():
                payload_list_obj = request.data["payload_list"]
                for holiday_obj in payload_list_obj:
                    if updated_record_status.lower() == masterconf.new_data_record_status.lower():
                        print("in IF status")
                        active_status = holiday_date.objects.filter(holiday_code=holiday_obj["holiday_code"],record_status = masterconf.new_data_record_status)
                        if len(active_status) != 0:
                            print("active record found")
                            holiday_date.objects.filter(holiday_code=holiday_obj["holiday_code"],record_status = masterconf.new_data_record_status).update(
                                                        record_status=masterconf.record_status_history,
                                                        modified_on=upload_time,
                                                        modified_by=username,
                                                        modified_reference_id=reference_id)
                            holiday_date.objects.filter(holiday_code=holiday_obj["holiday_code"],
                                                        created_reference_id=created_reference_id,
                                                        record_status=holiday_obj["record_status"]).update(
                                                        record_status=updated_record_status, approved_on=upload_time,
                                                        approved_by=username, approved_reference_id=reference_id)
                            holiday_states.objects.filter(holiday_code=holiday_obj["holiday_code"],
                                                        created_reference_id=created_reference_id,
                                                        record_status=holiday_obj["record_status"],
                                                        state = holiday_obj["state"]).update(
                                                        record_status=updated_record_status, approved_on=upload_time,
                                                        approved_by=username, approved_reference_id=reference_id)
                            holiday_list.objects.filter(holiday_code=holiday_obj["holiday_code"],
                                                          created_reference_id=created_reference_id,
                                                          record_status=holiday_obj["record_status"]).update(
                                                        record_status=updated_record_status, approved_on=upload_time,
                                                        approved_by=username, approved_reference_id=reference_id)
                        else:
                            print("no active record found")
                            holiday_date.objects.filter(holiday_code=holiday_obj["holiday_code"],created_reference_id=created_reference_id,
                                                        record_status =holiday_obj["record_status"]).update(
                                                        record_status=updated_record_status, approved_on=upload_time,
                                                        approved_by=username,approved_reference_id=reference_id)
                            holiday_states.objects.filter(holiday_code=holiday_obj["holiday_code"],
                                                          created_reference_id=created_reference_id,
                                                          record_status=holiday_obj["record_status"],
                                                          state=holiday_obj["state"]).update(
                                                        record_status=updated_record_status, approved_on=upload_time,
                                                        approved_by=username, approved_reference_id=reference_id)
                            holiday_list.objects.filter(holiday_code=holiday_obj["holiday_code"],
                                                        created_reference_id=created_reference_id,
                                                        record_status=holiday_obj["record_status"]).update(
                                                        record_status=updated_record_status, approved_on=upload_time,
                                                        approved_by=username, approved_reference_id=reference_id)
                    elif updated_record_status.lower() == masterconf.rejected_status.lower():
                        holiday_date.objects.filter(holiday_code=holiday_obj["holiday_code"],
                                                    created_reference_id=created_reference_id).update(
                                                    record_status=updated_record_status, rejected_on=upload_time,
                                                    rejected_by=username, reject_reference_id=reference_id)
            else:
                if updated_record_status.lower() == masterconf.new_data_record_status.lower():
                    holiday_date.objects.filter(created_reference_id=created_reference_id).update(
                        record_status=updated_record_status, approved_on=upload_time,
                        approved_by=username, approved_reference_id=reference_id)
                    holiday_states.objects.filter(created_reference_id=created_reference_id).update(
                        record_status=updated_record_status, approved_on=upload_time,
                        approved_by=username, approved_reference_id=reference_id)
                    holiday_list.objects.filter(created_reference_id=created_reference_id).update(
                        record_status=updated_record_status, approved_on=upload_time,
                        approved_by=username, approved_reference_id=reference_id)
                else:
                    holiday_date.objects.filter(created_reference_id=created_reference_id).update(
                        record_status=updated_record_status, rejected_on=upload_time,
                        rejected_by=username, reject_reference_id=reference_id)
            data_update_log_master.objects.filter(created_reference_id=created_reference_id).update(record_status=updated_record_status,
                                                                                            approved_on=upload_time,
                                                                                            approved_by=username,
                                                                                            approved_reference_id=reference_id)
            return masterconf.status_approval_success
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
