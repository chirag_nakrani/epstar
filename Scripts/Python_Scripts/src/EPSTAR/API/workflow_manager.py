from Common.CommonFunctions import common_util
from Common import masterconf
from Controller.Workflow import WorkflowController
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class WorkFlowManager(APIView):

    #This API is used to handle workflow related activites

    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def validate_assign_category_req(self, request):

        params = masterconf.user_bank_project_params
        status_flag = False
        for param in params:
            if param not in request.keys():
                status_flag = False
                break
            else:
                status_flag = True
        return status_flag


    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_obj = {}
        check_flag = self.validate_assign_category_req(request.data)
        print(" check_flag  ::: ",check_flag)
        if check_flag:
            try:
                wkflw_cntroller = WorkflowController.WorkflowController()
                if request.data['operation_type'] == masterconf.operation_type_add or request.data[
                    'operation_type'] == masterconf.operation_type_delete:
                    status_dict = wkflw_cntroller.create_or_delete_workflow(request)
                    if status_dict["status_code"] == masterconf.success_updated:
                        status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)
                elif request.data['operation_type'] == masterconf.operation_type_read:
                    returned_list = wkflw_cntroller.read_workflow(request)
                    return Response(status=status.HTTP_201_CREATED, data=returned_list)
            except Exception as e:
                print(e)
                response_obj['status'] = 'failure'
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"status": "Bad Request"})
