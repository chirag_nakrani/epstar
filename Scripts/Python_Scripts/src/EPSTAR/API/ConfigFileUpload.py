import os
from Common import masterconf
from Common.CommonFunctions import common_util
from Controller.ConfigurableParams import feeder_denomination_priority , feeder_capacity_percentage ,Eod_Activity_controller, feeder_deno_pre_avail, limitdays
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.parsers import MultiPartParser , FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


class ConfigDataUpload ( APIView ) :
    ## This is the Main API endpoint class for reference data upload

    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request ) :

        # This is the post method for File upload using Post Method

        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_config_req ( request.POST )
        print("check_req_flag from  ConfigDataUpload: {}".format ( check_req_flag ))
        if request.POST :  # and request.FILES:
            if check_req_flag :
                print("Input Request is Valid")
                if request.FILES :
                    filehandle = request.FILES['file']
                    filehandle_name = request.FILES['file'].name
                    print("filehandle: {}".format ( filehandle ))
                    print("filehandle_name : {}".format ( filehandle_name ))
                    input_filepath = common_util_obj.divide_store_file ( request )  # ES1-T218 - Folder re-structuring done
                    print("input filepath : ", input_filepath)
                else :
                    print("Inserting records through UI")
                print("request.POST['file_type'] : {}".format ( request.POST['file_type'] ))
                try :
                    ##############GOing for feeder_denomination_priority#############################################
                    if request.POST[
                        'file_type'].upper ( ) == masterconf.feeder_denomination_priority_filetype.upper ( ) :
                        Feeder_denomination_priority_Controller = feeder_denomination_priority.feeder_denomination_priority_Controller ( )
                        if request.FILES :
                            status_dict = Feeder_denomination_priority_Controller.handle_config_upload ( request ,
                                                                                                         input_filepath )
                        else :
                            status_dict = Feeder_denomination_priority_Controller.handle_feeder_denomination_add_data (
                                request.data )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            print("status dict having failed format validation : " , status_dict)
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.data_val_success_upload :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                    #######################################################################################################
                    if request.POST['file_type'].upper ( ) == masterconf.file_type_fdr_casstt_max_cap_per.upper ( ) :
                        Feeder_capacity_percentage_Controller = feeder_capacity_percentage.feeder_capacity_percentage_Controller ( )
                        status_dict = Feeder_capacity_percentage_Controller.handle_config_upload ( request ,
                                                                                                   input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            print("status dict having failed format validation : " , status_dict)
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.data_val_success_upload :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )

                    #######################################################################################################
                    if request.POST['file_type'].upper ( ) == masterconf.file_type_feeder_deno_pre_avail.upper ( ) :
                        Feeder_deno_pre_avail_Controller = feeder_deno_pre_avail.feeder_deno_pre_avail_Controller ( )
                        status_dict = Feeder_deno_pre_avail_Controller.handle_config_upload ( request , input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            print("status dict having failed format validation : " , status_dict)
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.data_val_success_upload :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )

                    ######################################################################################################################################
                    if request.POST['file_type'].upper ( ) == masterconf.eod_activity_file_type.upper ( ) :
                        eod_Activity_controller = Eod_Activity_controller.eod_activity_controller ( )
                        status_dict = eod_Activity_controller.handle_config_upload ( request ,input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            print("status dict having failed format validation : " , status_dict)
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.data_val_success_upload :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )

                    #################going for file upload of limitdays#######################################################
                    if request.POST['file_type'].upper ( ) == masterconf.config_menu_list[2].upper() :
                        Limitdays_Controller = limitdays.limitdays_Controller ( )
                        status_dict = Limitdays_Controller.handle_config_upload ( request , input_filepath )
                        print("printing Status Dict: {}".format ( status_dict ))
                        if status_dict["status_code"] == masterconf.format_val_failed_upload :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            print("status dict having failed format validation : " , status_dict)
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.no_record_found_update :
                            status_dict["status_desc"] = masterconf.error_codes_desc[masterconf.no_record_found_update]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                        elif status_dict["status_code"] == masterconf.data_val_success_upload :
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                        else :
                            status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                            return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )

                    ######################################################################################################################################

                    else :
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                except Exception as e :
                    print("Exception: " , e)
                    status_dict["level"] = masterconf.system_level
                    status_dict["status_code"] = masterconf.system_level_exception_code
                    status_dict["status_desc"] = masterconf.system_level_exception
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            else :
                invalid_req = {
                    "level" : masterconf.request_level ,
                    "status_code" : masterconf.request_level_exception_code ,
                    "status_desc" : masterconf.request_level_exception
                }
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )

        else :
            return Response ( status=status.HTTP_400_BAD_REQUEST )
