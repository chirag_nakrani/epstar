from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.Indent import IndentController
from Controller.IndentPDF import ViewPDFController
from rest_framework.pagination import PageNumberPagination
from rest_framework import generics
from Common import masterconf, db_queries_properties
import logging
from Controller.Withdrawal import WithdrawalController
import time, datetime
from datetime import timedelta
import os
from PIL import Image



##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
    page_size_query_param = 'page_size'


class WithdrawalConfirmation(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def validate_withdrawalconf_Req(self,request_data):

        #Validate the incoming request

        logger = logging.getLogger(__name__)
        params = masterconf.withdrawal_confirmation
        logger.info("Input params receiving for Withdrawal Status: %s", request_data)
        logger.info("params from validate_withdrawalconf_Req: %s", params)
        status_flag = False
        for param in params:
            if param not in request_data.keys():
                status_flag = False
                break
            else:
                status_flag = True
        logger.info("Returned status flag from validate_withdrawalconf_Req: %s", status_flag)
        return status_flag

    def divide_store_file ( self , request ) :

        # To Compress and store image in application folder

        logger = logging.getLogger(__name__)
        basepath = masterconf.WithdrawalFolder
        current_date = datetime.datetime.fromtimestamp ( time.time ( ) ).strftime ( '%d-%m-%Y' )
        username = request.userinfo['username'].username
        file_type = ''
        bank_code = ''
        bank_project = ''
        cra = ''
        if 'bank_code' in request.POST.keys ( ) :
            project_id = ''
            if 'project_id' in request.POST.keys ( ) :
                project_id = request.POST['project_id']
            bank_code = request.POST['bank_code']
            cra = request.POST['cra']
            bank_project = str ( bank_code ) + "-" + str ( project_id )
            input_path = basepath + "\\" + cra + "\\" + bank_project + "\\" + username + "\\" + current_date + "\\"
        else:
            input_path = basepath + "\\"  + username + "\\" + current_date + "\\"
        input_path = input_path.replace ( "\\" , "/" )
        if not os.path.exists ( input_path ) :
            os.makedirs ( input_path )
        filehandle = request.FILES['file']
        filehandle_name = request.FILES['file'].name
        input_filepath = os.path.join ( input_path , filehandle_name )
        with open ( input_filepath , 'wb+' ) as fout :
            # Iterate through the chunks.
            for chunk in filehandle.chunks ( ) :
                fout.write ( chunk )
            fout.close ( )
        logger.info("input_filepath in withdrawal : %s", input_filepath)
        image = Image.open(input_filepath)
        newImage = image.resize((780, 780))
        newImage.save(input_filepath)
        #print("Image Size ::: ", foo.size())
        return input_filepath

    def post(self, request, format=None):
        response_obj = {}
        #### checking if all the parameters are passed or not ####
        logger = logging.getLogger(__name__)
        check_flag = self.validate_withdrawalconf_Req(request.POST)
        if check_flag:
            logger.info("Incoming request ::::: in withdrawal : %s", request)
            withdrawal_status_obj = WithdrawalController.WithdrawalController()
            try:
                return_file_path = ''
                if 'file' in request.FILES:
                    return_file_path = self.divide_store_file(request)
                status_dict = withdrawal_status_obj.handle_withdrawal_conf_req(return_file_path,request)
                if status_dict['status_code'] == masterconf.success_code:
                    return Response(status=status.HTTP_201_CREATED, data=status_dict)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
            except Exception as e:
                logger.error('Exception occured in create withdrawal api :: %s ', e)
                response_obj['status'] = masterconf.system_level_exception
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)
        else:
            invalid_req = {
                "level": masterconf.request_level,
                "status_text": masterconf.request_level_exception
            }
            logger.info('Bad Request')
            return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)

    def get(self, request, format=None):
        response_obj = {}
        logger = logging.getLogger(__name__)
        withdrawal_status_obj = WithdrawalController.WithdrawalController()
        try:
            response = withdrawal_status_obj.handle_withdrawal_conf_view_req(request)
            if isinstance(response,dict):
                return Response(status=status.HTTP_404_NOT_FOUND, data=response)
            else:
                return response
        except Exception as e:
            logger.error('Exception occured in view  withdrawal image api :: %s ', e)
            response_obj['status'] = masterconf.system_level_exception
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)


class GetAllWithdrawalRecords(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    pagination_class = StandardResultsSetPagination
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        response_obj = {}
        withdrawal_status_obj = WithdrawalController.WithdrawalController()
        logger = logging.getLogger(__name__)
        try:
            response = withdrawal_status_obj.get_all_withdrawal_records(request)
            paginator = self.pagination_class()
            page = paginator.paginate_queryset(response, request)
            return paginator.get_paginated_response(page)
            #return Response(status=status.HTTP_201_CREATED, data=response)
        except Exception as e:
            logger.error('Exception occured in get indent order number api :: %s ', e)
            response_obj['status'] = masterconf.system_level_exception
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)


class DeleteWithdrawalRecords(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def val_del_withdrawal_Req(self,request_data):

        #Validate the incoming request

        logger = logging.getLogger(__name__)
        params = masterconf.delete_withdrawal_confirmation
        logger.info("Input params receiving for Withdrawal Status: %s", request_data)
        logger.info("params from validate_withdrawalconf_Req: %s", params)
        status_flag = False
        for param in params:
            if param not in request_data.keys():
                status_flag = False
                break
            else:
                status_flag = True
        logger.info("Returned status flag from validate_withdrawalconf_Req: %s", status_flag)
        return status_flag



    def post(self, request, format=None):
        response_obj = {}
        #### checking if all the parameters are passed or not ####
        logger = logging.getLogger(__name__)
        check_flag = self.val_del_withdrawal_Req(request.data)
        if check_flag:
            logger.info("Incoming request ::::: in withdrawal : %s", request)
            withdrawal_status_obj = WithdrawalController.WithdrawalController()
            try:
                status_dict = withdrawal_status_obj.delete_withdrawal_records(request)
                if status_dict['status_code'] == 'S106':
                    status_dict['status_desc'] = masterconf.success_codes_desc[status_dict['status_code']]
                    return Response(status=status.HTTP_201_CREATED, data=status_dict)
                else:
                    status_dict['status_desc'] = masterconf.error_codes_desc[status_dict['status_code']]
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
            except Exception as e:
                logger.error('Exception occured in create withdrawal api :: %s ', e)
                response_obj['status'] = masterconf.system_level_exception
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)
        else:
            invalid_req = {
                "level": masterconf.request_level,
                "status_text": masterconf.request_level_exception
            }
            logger.info('Bad Request')
            return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)