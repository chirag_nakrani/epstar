from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework import generics
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.SlaPDF import SlaPDFController
from Common import masterconf
from Common.CommonFunctions import common_util
import logging
from rest_framework.pagination import PageNumberPagination

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class SavePDF(generics.RetrieveAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		response_obj = {}
		try:
			save_pdf_controller = SlaPDFController.SlaPDFController()
			response_obj = save_pdf_controller.handle_pdf_save_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_obj)
		except Exception as e:
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)

class DeletePDF(generics.RetrieveAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		response_obj = {}
		try:
			save_pdf_controller = SlaPDFController.SlaPDFController()
			response_obj = save_pdf_controller.handle_pdf_delete_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_obj)
		except Exception as e:
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)

class ApproveRejectPDF(generics.RetrieveAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		logger = common_util_obj.initiateLogger()
		logger.setLevel(logging.INFO)
		response_obj = {}
		try:
			save_pdf_controller = SlaPDFController.SlaPDFController()
			response_obj = save_pdf_controller.handle_pdf_approve_reject_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_obj)
		except Exception as e:
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=response_obj)

class ListPDF(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination  ##Pagination Class Added

	def post(self, request, format=None):
		save_pdf_controller = SlaPDFController.SlaPDFController()
		try:
			response_list = save_pdf_controller.handle_pdf_list_req(request)
			print(response_list)
			paginator = self.pagination_class()
			page = paginator.paginate_queryset(response_list, request)
			return paginator.get_paginated_response(page)
		except Exception as e:
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
		return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)