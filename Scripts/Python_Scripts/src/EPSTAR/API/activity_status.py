from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.ActivityStatus import ActivityStatusController
from Common import masterconf
from rest_framework.pagination import PageNumberPagination

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class ActivityStatus(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination  ##Pagination Class Added

	def post(self, request, format=None):
		response_list = list()
		try:
			#### activity status for specific bank ####
			activity_status_controller = ActivityStatusController.ActivityStatusController()
			response_list = activity_status_controller.handle_activity_status_req(request)
			if 'status_code' in response_list:
				return Response(status=status.HTTP_201_CREATED, data=response_list)
			else:
				paginator = self.pagination_class()
				page = paginator.paginate_queryset(response_list, request)
				return paginator.get_paginated_response(page)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)
