from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.IndentAdmin import IndentAdminController
from rest_framework import generics
from Common import masterconf,db_queries_properties
from rest_framework.pagination import PageNumberPagination

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class IndentAdmin(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination  ##Pagination Class Added

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		indent_admin_controller = IndentAdminController.IndentAdminController()
		try:
			##### getting response list of the join query from feeder_branch_master, indent_master and indent_mail_status table #####
			response_list = indent_admin_controller.handle_indent_admin_req(request)
			# if response_list != []:
			paginator = self.pagination_class()
			page = paginator.paginate_queryset(response_list, request)
			return paginator.get_paginated_response(page)
			# else:
			# 	return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
		return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)
