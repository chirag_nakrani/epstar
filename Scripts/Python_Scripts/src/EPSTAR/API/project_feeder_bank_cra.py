from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.ProjectFeederBankCra import ProjectFeederBankCraController
from rest_framework.pagination import PageNumberPagination
from rest_framework import generics
import logging
from  Common import masterconf

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class ProjectFeederBankCraMappingList(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination ##Pagination Class Added

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		logger = logging.getLogger(__name__)
		try:
			logger.info('Start of ProjectFeederBankCraMappingList api.')
			project_feeder_bank_cra_controller = ProjectFeederBankCraController.ProjectFeederBankCraController()
			response_list = project_feeder_bank_cra_controller.handle_list_req(request)
			logger.info('Response count: %s',len(response_list))
			if response_list:
				paginator = self.pagination_class()
				page = paginator.paginate_queryset(response_list, request)
				return paginator.get_paginated_response(page)
			else:
				status_dict = {
					"status_text": masterconf.data_not_found_text,
					"status_code": masterconf.data_not_found
				}
				logger.info('No Response list generated, sending reponse: %s',status_dict)
				return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=status_dict)
		except Exception as e:
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
			logger.error('Error occured: %s', e)
			logger.error('Error response: %s', invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=invalid_req)

class ProjectFeederBankCraMapping(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination ##Pagination Class Added

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		response_list = list()
		response_obj = {}
		logger = logging.getLogger(__name__)
		try:
			logger.info('Start of ProjectFeederBankCraMapping api.')
			project_feeder_bank_cra_controller = ProjectFeederBankCraController.ProjectFeederBankCraController()
			response_list = project_feeder_bank_cra_controller.handle_upload_req(request)
			logger.info('Response Generated: %s',response_list)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
			logger.error('Error occured: %s', e)
			logger.error('Error response: %s', invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data=invalid_req)
