from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.BalanceDispenseCalculation import BalanceDispenseCalculationController
from Common import masterconf
import logging

class BalanceDispenseCalculation(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        logger = logging.getLogger(__name__)
        status_dict = {}
        response_list = list()
        try:
            logger.info('Start of BalanceDispenseCalculation api.')
            balance_dispense_calculation_controller = BalanceDispenseCalculationController.BalanceDispenseCalculationController()
            response_list = balance_dispense_calculation_controller.handle_calculation_req(request)
            logger.info('Returned response from BalanceDispenseCalculation api: %s', response_list)
            logger.info('End of BalanceDispenseCalculation api.')
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            logger.error("Exception occured in BalanceDispenseCalculation api: %s", e)
            status_dict["level"] = masterconf.system_level
            status_dict["status_code"] = masterconf.system_level_exception_code
            status_dict["status_desc"] = masterconf.system_level_exception
            logger.info('Returned response from BalanceDispenseCalculation api: %s', status_dict)
            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)

class DispenseData(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        logger = logging.getLogger(__name__)
        status_dict = {}
        response_list = list()
        try:
            logger.info('Start of DispenseData api.')
            balance_dispense_calculation_controller = BalanceDispenseCalculationController.BalanceDispenseCalculationController()
            response_list = balance_dispense_calculation_controller.handle_data_req(request)
            logger.info('Returned response from DispenseData api: %s', response_list)
            logger.info('End of DispenseData api.')
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            logger.error("Exception occured in DispenseData api: %s", e)
            status_dict["level"] = masterconf.system_level
            status_dict["status_code"] = masterconf.system_level_exception_code
            status_dict["status_desc"] = masterconf.system_level_exception
            logger.info('Returned response from DispenseData api: %s', status_dict)
            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
