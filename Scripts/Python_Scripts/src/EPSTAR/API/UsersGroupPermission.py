from django.http import HttpResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.UserManagement import UserGroupPermController
from django.contrib.auth.models import User, Group, Permission
from Common import masterconf


class UsersList(APIView):
	# This API is used to get Users information as list

	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		user_obj = UserGroupPermController.GetUsersGroupPermList()
		try:
			users_list = user_obj.get_users_list(request)
			if users_list is not None:
				return Response(status=status.HTTP_201_CREATED, data=users_list)
			else:
				return Response(status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			return Response(status=status.HTTP_400_BAD_REQUEST)


class GroupList(APIView):
	# This API is used to get Users information as list

	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		group_obj = UserGroupPermController.GetUsersGroupPermList()
		try:
			group_list = group_obj.get_group_list(request)
			if group_list is not None:
				return Response(status=status.HTTP_201_CREATED, data=group_list)
			else:
				return Response(status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			return Response(status=status.HTTP_400_BAD_REQUEST)


class PermissionList(APIView):
	# This API is used to get Users information as list

	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		perm_obj = UserGroupPermController.GetUsersGroupPermList()
		try:
			perm_list = perm_obj.get_permission_list(request)
			if perm_list is not None:
				return Response(status=status.HTTP_201_CREATED, data=perm_list)
			else:
				return Response(status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			return Response(status=status.HTTP_400_BAD_REQUEST)

class UserGroupMapping(APIView):

	#This API is used to Map users to groups

	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		create_user_group_obj = UserGroupPermController.GetUsersGroupPermList()
		status_dict = {}
		common_util_obj = common_util.common_utils()
		check_req_flag = common_util_obj.validate_user_bank_project_req(request.data)
		print(" check_req_flag :: ",check_req_flag)
		if check_req_flag:
			try:
				if request.data['operation_type'] == masterconf.operation_type_add or request.data['operation_type'] == masterconf.operation_type_delete:
					status_dict = create_user_group_obj.create_delete_user_group_mapping(request)
					if status_dict["status_code"] == masterconf.success_updated:
						status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
				elif request.data['operation_type'] == masterconf.operation_type_read:
					user_group_list = create_user_group_obj.get_user_group_mapping(request)
					return Response(status=status.HTTP_201_CREATED, data=user_group_list)
			except Exception as e:
				print(e)
				status_dict["status_code"] = masterconf.fail_to_add_records
				status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
				return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
		else:
			return Response(status=status.HTTP_400_BAD_REQUEST)
