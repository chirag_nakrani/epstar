from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication , BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser , FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.ReferenceData import Add_Project_Bank_Region_Controller


class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


class Add_Project_Bank_Region ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request ) :
        print ("Inside Add_Project_Bank_Region")
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        print ("request : " , request)
        print ("request.data : " , request.data)
        check_req_flag = common_util_obj.validate_add_project_bank_region_input_req ( request )
        if check_req_flag :
            print("Input request is Valid")
            try :
                add_Project_Bank_Region_Controller = Add_Project_Bank_Region_Controller.Add_Project_Bank_Region_Controller ( )
                if request.data['file_type'].lower ( ) in masterconf.modify_entity and request.data[
                    'operation'].lower ( ) == masterconf.operation_type_add.lower ( ) :
                    status_dict = add_Project_Bank_Region_Controller.handle_add_request ( request )
                    print ("status_dict returned from Add_Project_Bank_Region_Controller is : " , status_dict)
                    if status_dict["status_text"] == masterconf.insert_record :
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else :
                        status_dict["status_text"] = masterconf.insert_record_failed
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                elif request.data['file_type'].lower ( ) in masterconf.modify_entity and request.data[
                    'operation'].lower ( ) == masterconf.operation_type_modify.lower ( ) :
                    status_dict = add_Project_Bank_Region_Controller.handle_modify_request ( request )
                    print ("status_dict returned from Add_Project_Bank_Region_Controller is : " , status_dict)
                    if status_dict["status_text"] == masterconf.insert_record :
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else :
                        status_dict["status_text"] = masterconf.insert_record_failed
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                else :
                    status_dict["status_text"] = masterconf.insert_record_failed
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
            except Exception as e :
                print("Exception from Add_Project_Bank_Region: " , e);
                status_dict["level"] = masterconf.system_level;
                status_dict["status_text"] = masterconf.system_level_exception;
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
        else :
            invalid_req = {
                "level" : masterconf.request_level ,
                "status_text" : masterconf.request_level_exception
            }
            return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )
