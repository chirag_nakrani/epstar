from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Common import masterconf
from Common.CommonFunctions import common_util

from Controller.ReferenceData import CypherCode


##############################For inserting fresh records via UI############
class CypherCodeDataUpload(APIView):

    ## This is the Main API endpoint class for Cypher code data upload

    authentication_classes = (JSONWebTokenAuthentication, )
    parser_class = (MultiPartParser, FormParser, )
    renderer_classes = (JSONRenderer, )
    permission_classes = (IsAuthenticated, )

    @csrf_exempt
    def post(self, request):
        status_dict = {}
        common_util_obj = common_util.common_utils()
        check_req_flag = common_util_obj.validate_cyphercode_InputReq(request.data)####validating if input request is as expected
        print("check_req_flag from CypherCodeDataUpload:{}".format(check_req_flag))
        if request.data:
            if check_req_flag:
                print("Input Request is Valid")
                try:
#############Going for extracting data from cypher code file#####################################
                    if request.data['file_type'].upper() == masterconf.file_type_cypher_code.upper():#validating if input is for cypher code only
                        cypherCode_Controller = CypherCode.CypherCode_Controller()
                        status_dict = cypherCode_Controller.handle_cyphercode_add_data(request)
                        print("printing Status Dict:{}".format(status_dict))
                        if status_dict['data'] == masterconf.data_updated_for_approval:
                            status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                            return Response(status=status.HTTP_201_CREATED, data=status_dict)#Returning success if everything is executed correctly
                        else:
                            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#returning failure if not success in inserting the new record
                except Exception as e:
                    print("Exception: ", e)
                    status_dict["level"] = masterconf.system_level
                    status_dict["status_code"] = masterconf.system_level_exception_code
                    status_dict["status_desc"] = masterconf.system_level_exception
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
            else:
                invalid_req = {
                    "level":masterconf.request_level,
                    "status_code":masterconf.request_level_exception_code,
                    "status_desc":masterconf.request_level_exception
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
