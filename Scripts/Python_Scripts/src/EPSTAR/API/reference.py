from django.http import HttpResponse
import openpyxl
import xlsxwriter
import os
import pandas as pd
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import pyexcel
import xlrd
import time, datetime
#from Controller.Routine import BaseCBRController, BaseCDRController, BaseVCBControllerEOD, BaseC3RMISController, DailyLoad_RepCntrlr
from Controller.Master import ATMMasterController
from Controller.ViewData import BankInfoController
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Model.models import Data_rejection_reasons
from Model.models import Data_overwrite_reasons
from Model.models import Data_update_reasons
from django.core import serializers
from Controller.ReferenceData import ReasonsCRUDController
from Controller.ReferenceData import DefaultLoadController
from Controller.ViewData import referenceviewcontroller
from django.contrib.auth.models import User,Group,Permission
import json
from json import JSONEncoder
from django.db import connection
import pyodbc


class CsrfExemptSessionAuthentication(SessionAuthentication):
	def enforce_csrf(self, request):
		return  # To not perform the csrf check previously happening

class RejectionCrud(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	@csrf_exempt
	def post(self, request,format=None):
		status_dict = {}
		common_util_obj = common_util.common_utils();
		check_req_flag = common_util_obj.validate_rejection_req(request.data)
		#print ("request.POST.keys() : {}".format(request.data.keys()))
		#print ("request.POST.values() : {}".format(request.data.values()))
		#print ("request.POST : {}".format(request.data))
		ref_obj = ReasonsCRUDController.ReasonsCRUDController()
		if check_req_flag:
			try:
				if request.data['operation'] == masterconf.operation_type_add:

					status_dict = ref_obj.handle_rejection_create_request(request)
					print("status_dict",status_dict)
					if status_dict['status_code'] == "S105":
						status_dict['status_desc'] = masterconf.success_codes_desc['S105']
						return Response(status=status.HTTP_201_CREATED,data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E106']
						return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)

				elif request.data['operation'] == masterconf.operation_type_delete:

					status_dict = ref_obj.handle_rejection_delete_request(request)
					if status_dict['status_code'] == "S106":
						status_dict['status_desc'] = masterconf.success_codes_desc['S106']
						return Response(status=status.HTTP_201_CREATED,data=status_dict)
					elif status_dict['status_code'] == "E108":
						status_dict['status_desc'] = masterconf.error_codes_desc['E108']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E107']
						return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)

				elif request.data['operation'] == masterconf.operation_type_modify:

					status_dict = ref_obj.handle_rejection_modify_request(request)
					print("status_dict", status_dict)
					if status_dict['status_code'] == "S107":
						status_dict['status_desc'] = masterconf.success_codes_desc['S107']
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E105']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)

				elif request.data['operation'] == masterconf.operation_type_read:
					rejection_data_list = list()
					rejection_data_list = ref_obj.handle_rejection_read_request(request)
					print(rejection_data_list)
					return Response(status=status.HTTP_201_CREATED,data=rejection_data_list)
				else:
					return Response(status=status.HTTP_400_BAD_REQUEST)
			except Exception as e:
				return Response(status=status.HTTP_400_BAD_REQUEST)



class OverwriteCrud(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	@csrf_exempt
	def post(self, request,format=None):
		status_dict = {}
		common_util_obj = common_util.common_utils();
		check_req_flag = common_util_obj.validate_rejection_req(request.data)
		#print ("request.POST.keys() : {}".format(request.data.keys()))
		#print ("request.POST.values() : {}".format(request.data.values()))
		#print ("request.POST : {}".format(request.data))
		ref_obj = ReasonsCRUDController.ReasonsCRUDController()
		if check_req_flag:
			try:
				if request.data['operation'] == masterconf.operation_type_add:
					status_dict = ref_obj.handle_write_create_request(request)
					print("status_dict", status_dict)
					if status_dict['status_code'] == "S105":
						status_dict['status_desc'] = masterconf.success_codes_desc['S105']
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E106']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
				elif request.data['operation'] == masterconf.operation_type_delete:
					status_dict = ref_obj.handle_write_delete_request(request)
					print("status_dict",status_dict)
					if status_dict['status_code'] == "S106":
						status_dict['status_desc'] = masterconf.success_codes_desc['S106']
						return Response(status=status.HTTP_201_CREATED,data=status_dict)
					elif status_dict['status_code'] == "E108":
						status_dict['status_desc'] = masterconf.error_codes_desc['E108']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E107']
						return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
				elif request.data['operation'] == masterconf.operation_type_modify:
					status_dict = ref_obj.handle_write_modify_request(request)
					print("status_dict", status_dict)
					if status_dict['status_code'] == "S107":
						status_dict['status_desc'] = masterconf.success_codes_desc['S107']
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E105']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
				elif request.data['operation'] == masterconf.operation_type_read:
					write_data_list = list()
					write_data_list = ref_obj.handle_write_read_request(request)
					print(write_data_list)
					return Response(status=status.HTTP_201_CREATED, data=write_data_list)
				else:
					return Response(status=status.HTTP_400_BAD_REQUEST)
			except Exception as e:
				return Response(status=status.HTTP_400_BAD_REQUEST)

class UpdateCrud(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	@csrf_exempt
	def post(self, request,format=None):
		status_dict = {}
		common_util_obj = common_util.common_utils();
		check_req_flag = common_util_obj.validate_rejection_req(request.data)
		#print ("request.POST.keys() : {}".format(request.data.keys()))
		#print ("request.POST.values() : {}".format(request.data.values()))
		#print ("request.POST : {}".format(request.data))
		ref_obj = ReasonsCRUDController.ReasonsCRUDController()
		# print (check_req_flag)
		# exit()
		if check_req_flag:
			try:
				if request.data['operation'] == masterconf.operation_type_add:
					status_dict = ref_obj.handle_update_create_request(request)
					print("status_dict", status_dict)
					if status_dict['status_code'] == "S105":
						status_dict['status_desc'] = masterconf.success_codes_desc['S105']
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E106']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
				elif request.data['operation'] == masterconf.operation_type_delete:
					status_dict = ref_obj.handle_update_delete_request(request)
					print("status_dict", status_dict)
					if status_dict['status_code'] == "S106":
						status_dict['status_desc'] = masterconf.success_codes_desc['S106']
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					elif status_dict['status_code'] == "E108":
						status_dict['status_desc'] = masterconf.error_codes_desc['E108']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E107']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
				elif request.data['operation'] == masterconf.operation_type_modify:
					status_dict = ref_obj.handle_update_modify_request(request)
					print("status_dict", status_dict)
					if status_dict['status_code'] == "S107":
						status_dict['status_desc'] = masterconf.success_codes_desc['S107']
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						status_dict['status_desc'] = masterconf.error_codes_desc['E105']
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
				elif request.data['operation'] == masterconf.operation_type_read:
					update_data_list = list()
					update_data_list = ref_obj.handle_update_read_request(request)
					print(update_data_list)
					return Response(status=status.HTTP_201_CREATED, data=update_data_list)
				else:
					return Response(status=status.HTTP_400_BAD_REQUEST)
			except Exception as e:
				return Response(status=status.HTTP_400_BAD_REQUEST)
#########################API for adding data into default loading table in DB related to data to be loadded to atm for specific atm########
class Defaultloading(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	@csrf_exempt
	def post(self, request,format=None):
		status_dict = {}
		common_util_obj = common_util.common_utils()
		print ("Going for Defaultloading Menu.")
		check_req_flag = common_util_obj.validate_defaultLoad_InputReq(request.data)
		print ("request.data from Defaultloading is : ",request.data)
		if check_req_flag:
			print("Input Request is Valid for Defaultloading.")
			defaultLoadController = DefaultLoadController.DefaultLoadController()#invoking DefaultLoadController to add or modify the record
			try:
#################Inserting new record#################################
				if request.data["operation"].upper() == masterconf.add_new_data_for_default_loading.upper():
					status_dict = defaultLoadController.handle_defaultLoad_add_data(request)
					print ("status_dict returned from DefaultLoadController for inserting the data:",status_dict)
					if status_dict["status_code"] == masterconf.data_val_success_reference_upload:#if data is inserted successfully
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#if data is not inserted successfully for any reason
#####################Modifying only Active record##########################################
				elif request.data["operation"].upper() == masterconf.update_data_for_default_loading.upper():
					status_dict = defaultLoadController.handle_defaultLoad_modify_request(request)
					if status_dict["status_code"] == masterconf.data_val_success_reference_upload:#if data is modified successfully
						return Response(status=status.HTTP_201_CREATED, data=status_dict)
					else:
						return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#if data is not inserted successfully for any reason
				else:
					return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#if operation another than Insert or Modifying is trying to performed
			except Exception as e:
				print("Exception from Defaultloading: ",e)
				status_dict["level"] = masterconf.system_level;
				status_dict["status_text"] = masterconf.system_level_exception;
				return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
		else:
			invalid_req = {
				"level":masterconf.request_level,
				"status_text":masterconf.request_level_exception
			}
			return Response(status=status.HTTP_400_BAD_REQUEST,data=invalid_req)

class ReferenceProjectList(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	@csrf_exempt
	def post(self, request,format=None):
		status_dict = {}
		ref_obj = referenceviewcontroller.referenceviewcontroller()
		try:
			project_data_list = ref_obj.show_project_list(request)
			if len(project_data_list):
				return Response(status=status.HTTP_201_CREATED,data=project_data_list)
			else:
				return Response(status=status.HTTP_400_BAD_REQUEST,data={'status':'No Data Found'})
		except Exception as e:
			#print(e)
			return Response(status=status.HTTP_400_BAD_REQUEST)

		# try:
		# 	bank_data_list = ref_obj.show_bank_list(request)
		# 	if len(bank_data_list):
		# 		return Response(status=status.HTTP_201_CREATED,data=bank_data_list)
		# 	else:
		# 		return Response(status=status.HTTP_400_BAD_REQUEST,data={'status':'No Data Found'})
		#
		# except Exception as e:
		# 	#print(e)
		# 	return Response(status=status.HTTP_400_BAD_REQUEST)
