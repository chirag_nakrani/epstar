import json
from Common import masterconf
# from Controller.Routine import BaseCBRController, BaseCDRController, BaseVCBControllerEOD, BaseC3RMISController, DailyLoad_RepCntrlr
from Common.CommonFunctions import common_util
# from Model.models import Default_loading
from Controller.UserManagement import UserMgntController
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class UserRoleView(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    @csrf_exempt
    def post(self, request, format=None):
        status_dict = {}
        ref_obj = UserMgntController.UserMgntController()
        try:
            user_data_list = ref_obj.get_user_roles(request)
            json_string = json.dumps(user_data_list)
            if user_data_list:
                return Response(status=status.HTTP_201_CREATED, data=user_data_list)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data={'status': 'No Data Found'})
        except Exception as e:
            # print(e)
            return Response(status=status.HTTP_400_BAD_REQUEST)


class UserBankProjectMapping(APIView):
    # This API class is used to handle all operations regarding user, bank and project mappings

    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    @csrf_exempt
    def post(self, request, format=None):
        status_dict = {}
        common_util_obj = common_util.common_utils()
        check_req_flag = common_util_obj.validate_user_bank_project_req(request.data)
        ref_obj = UserMgntController.userBankMapController()
        print(check_req_flag)
        if check_req_flag:
            try:
                if request.data['operation_type'] == masterconf.operation_type_add or request.data['operation_type'] == masterconf.operation_type_delete:
                    status_dict = ref_obj.UserBankProj_create_delete_request(request)
                    if status_dict['status_code'] == masterconf.success_updated:
                        status_dict['status_desc'] = masterconf.success_codes_desc[status_dict['status_code']]
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)
                    else:
                        return Response(status=status.HTTP_400_BAD_REQUEST)
                elif request.data['operation_type'] == masterconf.operation_type_read:
                    user_bank_project_list = ref_obj.handle_bankMap_read_request(request)
                    return Response(status=status.HTTP_201_CREATED, data=user_bank_project_list)
            except Exception as e:
                print(e)
                return Response(status=status.HTTP_400_BAD_REQUEST)


class MenuRolePermissionMapping(APIView):

    #This API is used to map group, menu and permission

    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    @csrf_exempt
    def post(self, request, format=None):
        common_util_obj = common_util.common_utils();
        check_req_flag = common_util_obj.validate_menu_perm_req(request.data)
        # print(check_req_flag)
        status_dict = {}
        returned_list = list()
        if check_req_flag:
            user_obj = UserMgntController.userMenuPermController()
            try:
                if request.data['operation_type'] == masterconf.operation_type_add or request.data[
                    'operation_type'] == masterconf.operation_type_delete:
                    status_dict = user_obj.menu_group_perm_add_delete_req(request)
                    if status_dict["status_code"] == masterconf.menu_perm_delete_success_code:
                        status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)
                    elif status_dict["status_code"] == masterconf.data_updated:
                        status_dict["status_desc"] = masterconf.success_codes_desc[status_dict["status_code"]]
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)
                elif request.data['operation_type'] == masterconf.operation_type_read:
                    returned_list = user_obj.menu_group_perm_read_req(request)
                    return Response(status=status.HTTP_201_CREATED, data=returned_list)
            except Exception as e:
                print(e)
                if request.data['operation_type'] == masterconf.operation_type_add:
                    status_dict["status_code"] = masterconf.fail_to_add_records
                    status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                else:
                    status_dict["status_code"] = masterconf.delete_record_failed
                    status_dict["status_desc"] = masterconf.error_codes_desc[status_dict["status_code"]]
                return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
        else:
            status_dict['status'] = masterconf.invalid_req
            status_dict['status_desc'] = masterconf.invalid_req_desc
            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)


class MenuList(APIView):

    #This API is used to get menu list

    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    @csrf_exempt
    def post(self, request, format=None):
        status_dict = {}
        print(" In MenuList ")
        ref_obj = UserMgntController.userMenuPermController()
        try:
            menu_list = ref_obj.get_menu_list(request)
            return Response(status=status.HTTP_201_CREATED, data=menu_list)
        except Exception as e:
            print(e)
            return Response(status=status.HTTP_400_BAD_REQUEST)
