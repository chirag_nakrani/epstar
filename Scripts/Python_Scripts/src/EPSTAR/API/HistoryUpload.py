from rest_framework.permissions import IsAuthenticated
from Common import masterconf
from Common.CommonFunctions import common_util
from Controller.ViewData import BankInfoController , common_bankfeeder_filterinfo , C3R_ApprPend
from django.views.decorators.csrf import csrf_exempt
from rest_framework import status
from rest_framework.authentication import SessionAuthentication
from rest_framework.parsers import MultiPartParser , FormParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from django.contrib.auth.models import User, Group, Permission
from Controller.ViewData import ViewHistoricalData
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'


class HistoryUploadFileStatus ( APIView ) :
	authentication_classes = (JSONWebTokenAuthentication ,)
	parser_class = (MultiPartParser , FormParser ,)
	renderer_classes = (JSONRenderer ,)
	permission_classes = (IsAuthenticated ,)
	pagination_class = StandardResultsSetPagination

	@csrf_exempt
	def post ( self , request) :
		status_dict = { }
		common_util_obj = common_util.common_utils ( )
		userinfo = request.userinfo
		try :
			if request.data['file_type'].lower ( ) == masterconf.historical_data_upload_file_type.lower( ):
				viewHistoricalData = ViewHistoricalData.ViewHistoricalData ( )
				status_list = viewHistoricalData.handle_view_data ( request.data)
				paginator = self.pagination_class()
				page = paginator.paginate_queryset(status_list, request)
				return paginator.get_paginated_response(page)
					
		except Exception as e :
			status_dict["level"] = masterconf.system_level
			status_dict["status_code"] = masterconf.system_level_exception_code
			status_dict["status_text"] = masterconf.system_level_exception
			return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
