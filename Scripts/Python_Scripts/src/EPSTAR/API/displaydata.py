from django.http import HttpResponse
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication , BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser , FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.ViewData import DisplayDataController


class CsrfExemptSessionAuthentication ( SessionAuthentication ) :
    def enforce_csrf ( self , request ) :
        return  # To not perform the csrf check previously happening


############This API will be used for drop down menu in UI#####################
class displaydata ( APIView ) :
    authentication_classes = (JSONWebTokenAuthentication ,)
    parser_class = (MultiPartParser , FormParser ,)
    renderer_classes = (JSONRenderer ,)
    permission_classes = (IsAuthenticated ,)

    @csrf_exempt
    def post ( self , request , format = None ) :
        status_dict = { }
        common_util_obj = common_util.common_utils ( )
        check_req_flag = common_util_obj.validate_displaydata_req (
            request.data )  # to check the input param for data display
        print ("check_req_flag from displaydata : " , check_req_flag)
        if check_req_flag :
            try :
                displayDataController = DisplayDataController.DisplayDataController ( )
                status_dict = displayDataController.handle_displaydata_request ( request.data )
                print ("status_dict from displayDataController : " , status_dict)
                if status_dict["status_desc"] == masterconf.drop_down_data :
                    del status_dict["status_desc"]
                    return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                else :
                    return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )

            except Exception as e :
                print("Exception from displaydata: " , e)
                status_dict["level"] = masterconf.system_level
                status_dict["status_code"] = masterconf.system_level_exception_code
                status_dict["status_desc"] = masterconf.system_level_exception
                return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
        else :
            invalid_req = {
                "level" : masterconf.request_level ,
                "status_code" : masterconf.request_level_exception_code ,
                "status_desc" : masterconf.request_level_exception
            }
            return Response ( status=status.HTTP_400_BAD_REQUEST , data=invalid_req )
