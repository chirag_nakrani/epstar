from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.DiversionStatus import DiversionStatusController
from rest_framework import generics
from Common import masterconf
from rest_framework.pagination import PageNumberPagination
import logging

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

#### diversion status insert function ###
class DiversionStatus(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		common_util_obj = common_util.common_utils()
		logger = logging.getLogger(__name__)
		response_list = list()
		response_obj = {}
		logger.info('Start of diversion status upload api.')
		logger.info('Request parameters: %s',request.data)
		#### checking if all the parameters are passed or not ####
		# check_flag = common_util_obj.validate_diversionStatus_ParamsReq(request.data)
		# if check_flag:
		diversion_status_controller = DiversionStatusController.DiversionStatusController()
		try:
			#### creating system settings ####
			response_list = diversion_status_controller.handle_diversion_status_upload_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			logger.info('Returned response from DiversionStatus upload api: %s', invalid_req)
			response_obj['status'] = masterconf.system_level_exception
			logger.error('Exception occured in DiversionStatus upload api: %s',e)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=response_obj)

#### diversion status approve function ####
class DiversionStatusApprove(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		response_list = list()
		response_obj = {}
		logger.info('Start of diversion status approve api.')
		diversion_status_controller = DiversionStatusController.DiversionStatusController()
		try:
			response_list = diversion_status_controller.handle_diversion_status_approve_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			logger.error('Exception occured in DiversionStatusApprove: %s',e)
			logger.info('Returned response from DiversionStatusApprove: %s',invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)

#### diversion status reject function ####
class DiversionStatusReject(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		response_list = list()
		response_obj = {}
		logger.info('Start of diversion status reject api.')
		diversion_status_controller = DiversionStatusController.DiversionStatusController()
		try:
			response_list = diversion_status_controller.handle_diversion_status_reject_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			logger.error('Exception occured in DiversionStatusReject: %s', e)
			logger.info('Returned response from DiversionStatusReject: %s', invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)

#### diversion status list function ####
class DiversionStatusList(generics.ListAPIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination  ##Pagination Class Added

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		response_list = list()
		logger.info('Start of diversion status list api.')
		diversion_status_controller = DiversionStatusController.DiversionStatusController()
		try:
			response_list = diversion_status_controller.handle_diversion_status_list_req(request)
			if response_list:
				paginator = self.pagination_class()
				page = paginator.paginate_queryset(response_list, request)
				logger.info('Received response paginated.')
				return paginator.get_paginated_response(page)
			else:
				response_obj = {}
				response_obj['status_code'] = masterconf.success_code
				response_obj['status_text'] = masterconf.no_data_found
				logger.info('Returned response from DiversionStatusList: %s',response_obj)
				return Response(status=status.HTTP_201_CREATED, data=response_obj)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			logger.error('Exception occured in DiversionStatusList: %s', e)
			logger.info('Returned response from DiversionStatusList: %s', invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)

#### diversion status dropdown function ####
class DiversionStatusDropdown(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		response_list = list()
		response_obj = {}
		logger.info('Start of diversion status dropdown api.')
		diversion_status_controller = DiversionStatusController.DiversionStatusController()
		try:
			response_list = diversion_status_controller.handle_dropdown_req(request)
			logger.info('Returned response from DiversionStatusDropdown: %s', response_list)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			logger.error('Exception occured in DiversionStatusDropdown: %s', e)
			logger.info('Returned response from DiversionStatusDropdown: %s', invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)

#### diversion status edit request ####
class DiversionStatusEdit(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		response_list = list()
		response_obj = {}
		logger.info('Start of diversion status edit api.')
		diversion_status_controller = DiversionStatusController.DiversionStatusController()
		try:
			response_list = diversion_status_controller.handle_diversion_status_edit_req(request)
			return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.request_level_exception
			}
			logger.error('Exception occured in DiversionStatusEdit: %s', e)
			logger.info('Returned response from DiversionStatusEdit: %s', invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)