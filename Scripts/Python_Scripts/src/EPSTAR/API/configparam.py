from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from Common import masterconf
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.ConfigurableParams import ConfigParamController
from Controller.ConfigurableParams import HolidayController
import logging


class EOD_Activity(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            config_cntroller = ConfigParamController.ConfigParamController()
            response_list = config_cntroller.handle_config_EOD_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            #print(e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Indent_Pre_Qualify(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            config_cntroller = ConfigParamController.ConfigParamController()
            response_list = config_cntroller.handle_config_qualify_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            #print(e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Modify_Cassette(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            config_cntroller = ConfigParamController.ConfigParamController()
            response_list = config_cntroller.handle_modify_cassette_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            #print(e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)


class Feeder_Denomination(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            config_cntroller = ConfigParamController.ConfigParamController()
            response_list = config_cntroller.handle_feeder_denomination_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            #print(e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Feeder_Vaulting(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            config_cntroller = ConfigParamController.ConfigParamController()
            response_list = config_cntroller.handle_feeder_vaulting_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            #print(e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Limit_Days(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            config_cntroller = ConfigParamController.ConfigParamController()
            response_list = config_cntroller.handle_limit_days_req(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            #print(e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class HolidayMaster(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser,FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        status_dict = {}
        common_util_obj = common_util.common_utils()
        print ("Going for Holiday Master Menu.")
        check_req_flag = common_util_obj.validate_holiday_InputReq(request.data)
        print ("request.data from HolidayMaster is : ",request.data)
        if check_req_flag:
            print("Input Request is Valid for HolidayMaster.")
            holidayController = HolidayController.HolidayController()#invoking DefaultLoadController to add or modify the record
            # holidayMasterUpdate =HolidayMasterUpdate.HolidayMasterUpdate()
            try:
#################Inserting new record#################################
                if request.data["operation"].upper() == masterconf.add_new_data_for_holiday_master.upper():
                    status_dict = holidayController.handle_holiday_add_data(request)
                    print ("status_dict returned from HolidayController for inserting the data:",status_dict)
                    if status_dict["status_code"] == masterconf.data_val_success_reference_upload:#if data is inserted successfully
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)
                    else:
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#if data is not inserted successfully for any reason
#####################Modifying only Active record##########################################
                elif request.data["operation"].upper() == masterconf.update_data_for_holiday_master.upper():
                    status_dict = holidayController.handle_holiday_update_request(request)
                    if status_dict["status_code"] == masterconf.data_val_success_reference_upload:#if data is modified successfully
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)
                    else:
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#if data is not inserted successfully for any reason
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)#if operation another than Insert or Modifying is trying to performed
            except Exception as e:
                print("Exception from Holiday Master: ",e)
                status_dict["level"] = masterconf.system_level;
                status_dict["status_text"] = masterconf.system_level_exception;
                return Response(status=status.HTTP_400_BAD_REQUEST,data=status_dict)
        else:
            invalid_req = {
                "level":masterconf.request_level,
                "status_text":masterconf.request_level_exception
            }
            return Response(status=status.HTTP_400_BAD_REQUEST,data=invalid_req)

class Holiday_Master_Dropdown(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            config_cntroller = ConfigParamController.ConfigParamController()
            print ("request from Holiday_Master_Dropdown : ", request)
            response_list = config_cntroller.holiday_state_dropdown(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            print("Exception from Holiday_Master_Dropdown : ",e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class Holiday_name_Dropdown(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        try:
            config_cntroller = ConfigParamController.ConfigParamController()
            response_list = config_cntroller.holiday_name_dropdown(request)
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            #print(e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

class EOD_Activity_update(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        common_util_obj = common_util.common_utils()
        response_list = list()
        response_obj = {}
        print("inside EOD update");
        try:
            eod_update = ConfigParamController.EODActivityUpdateController()
            print("try catch")
            response_list = eod_update.handle_EOD_update_request(request)
            print("after try catch")
            return Response(status=status.HTTP_201_CREATED, data=response_list)
        except Exception as e:
            #print(e)
            response_obj['status'] = masterconf.failure_status_resp
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)



class Edit_Approve_Indent_Pre_Qualify(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        status_dict = {}
        logger = logging.getLogger(__name__)
        try:
            logger.info("Incoming request %s", request.data)
            config_cntroller = ConfigParamController.ConfigParamController()
            status_dict = config_cntroller.handle_edit_appr_prequalifytatm(request)
            logger.info("Status Code from  ConfigParamController  %s", status_dict)
            return Response(status=status.HTTP_201_CREATED, data=status_dict)
        except Exception as e:
            logger.error("Exception in Edit_Approve_Indent_Pre_Qualify : %s", e)
            status_dict['status_code'] = masterconf.error
            status_dict['status_text'] = masterconf.error_codes_desc[masterconf.error]
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data = status_dict)
