from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common.CommonFunctions import common_util
from Common import masterconf
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.Operations import  VaultingBalanceController
from Controller.ViewData import IonDropDownController

class vaultingBalance(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        common_util_obj = common_util.common_utils()
        status_dict = {}
        check_request_flag = common_util_obj.validate_inputVB_request(request.data)
        print ("check_request_flag : ",check_request_flag)
        if check_request_flag:
            try:
                if request.data['file_type'] == masterconf.file_type_VB:
                    vaultingBalance = VaultingBalanceController.VaultingBalanceController()
                    status_dict = vaultingBalance.handle_vaultingBalance_req(request)
                    if status_dict['status_code'] == masterconf.success:
                        return Response(status=status.HTTP_201_CREATED, data=status_dict)
                    else:
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
                else:
                    status_dict['data'] = masterconf.invalid_req
                    status_dict['status_code'] = masterconf.invalid_request_code
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
            except Exception as e:
                response_obj['status'] = masterconf.failure_status_resp
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:

            status_dict['data'] = masterconf.invalid_req
            status_dict['status_code'] = masterconf.invalid_request_code
            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)

class dropdown(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    parser_class = (MultiPartParser, FormParser,)
    renderer_classes = (JSONRenderer,)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        status_dict = {}
        try:
            if 'indent_order_number' in request.data:
                ionDropDownController = IonDropDownController.IonDropDownController()
                status_dict = ionDropDownController.handle_ion_dropdown_request(request.data)
                if status_dict['status_text'] == masterconf.api_status_Success:
                    del status_dict['status_text']
                    return Response(status=status.HTTP_201_CREATED, data=status_dict)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
            else:
                status_dict['data'] = masterconf.invalid_req
                status_dict['status_text'] = masterconf.invalid_request_code
                return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
        except Exception as e:
            print ("Exception from dropdown of vaulting of Balance: ",e)
            return Response(status=status.HTTP_400_BAD_REQUEST)
