from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.IndentSummaryReport import IndentSummaryReportController
from Common import masterconf
from rest_framework.pagination import PageNumberPagination
import datetime
import os
from django.http import HttpResponse
import logging

##Pagination Class Added
class StandardResultsSetPagination(PageNumberPagination):
	page_size_query_param = 'page_size'

class IndentSummaryReport(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser, FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)
	pagination_class = StandardResultsSetPagination  ##Pagination Class Added

	def post(self, request, format=None):
		logger = logging.getLogger(__name__)
		logger.info('Start of indent summary report api.')
		response_list = list()
		export_time = datetime.datetime.strftime(datetime.datetime.now(),'%Y-%m-%d_%H_%M_%S')
		try:
			#### cash reports for specific bank and date ####
			cash_reports_controller = IndentSummaryReportController.IndentSummaryReportController()
			response_list = cash_reports_controller.handle_indent_summary_report_req(request)
			logger.info('Received response from handle_indent_summary_report_req of length: %s',len(response_list))

			if 'export' in request.data:
				file_name = (request.data['type'] + '_%s.xlsx' % (export_time)).replace(' ', '_')
				file_path = masterconf.report_output_folder + request.data['indent_date'] + '/' + request.data['type'] + '/' + file_name
				if not os.path.exists(masterconf.report_output_folder + request.data['indent_date'] + '/' + request.data['type'] + '/'):
					os.makedirs(masterconf.report_output_folder + request.data['indent_date'] + '/' + request.data['type'] + '/')
				excel_file = response_list.to_excel(file_path,index=False)

				logger.info('Excel File name: %s',file_path)
				with open(file_path,'rb') as file:
					response = HttpResponse(file.read(),
											content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel')
					response['Content-Disposition'] = 'attachment; filename=%s' % file_path
					return response
			else:
				paginator = self.pagination_class()
				page = paginator.paginate_queryset(response_list, request)
				return paginator.get_paginated_response(page)
			# return Response(status=status.HTTP_201_CREATED, data=response_list)
		except Exception as e:
			#print(e)
			invalid_req = {
				"level": masterconf.request_level,
				"status_text": masterconf.system_level_exception
			}
			logger.error('Exception occured in indent summary report api: %s',e)
			logger.error('Returned error response from indent summary report api: %s',invalid_req)
			return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR,data=invalid_req)
