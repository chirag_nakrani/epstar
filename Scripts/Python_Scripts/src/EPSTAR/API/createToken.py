import pyodbc

from Common import masterconf
from EPSTAR.settings import IS_SSO_REQUIRED
from django.contrib.auth import authenticate
from django.contrib.auth.models import User, Group
from rest_framework import status
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
# from ldap3.core.exceptions import LDAPCursorError
# import ldap
from rest_framework_jwt.settings import api_settings


class CreateToken(APIView):
    renderer_classes = (JSONRenderer,)
    authentication_classes = ()
    permission_classes = ()

    def post(self, request, format=None):
        permission_list = list()
        # print(request.data)
        # print(IS_SSO_REQUIRED)
        try:
            if IS_SSO_REQUIRED:
                # usr_key = request.data["v"] #server 3 line
                # username = user_auth_info.objects.filter(userkey=usr_key).values('username')
                # usr = username[0]['username']
                usr = request.data['username']  # server next 2 line
                usr_db = User.objects.get(username=usr)  # .values('username')
                # # #print(usr_db)
                # # db_token = Token.objects.get_or_create(user=usr_db)
                # # #print(db_token)
                jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER  # upto return 64 line no
                jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                payload = jwt_payload_handler(usr_db)
                jwt_token = jwt_encode_handler(payload)
                usr_db_token = User.objects.get(username=usr)
                # db_token = Token.objects.get_or_create(user=usr_db_token)
                # json_token = json.dumps(db_token[0])
                menu_list = self.create_menu(usr_db)
                # print(menu_list)
                return Response(
                    {'msg': 'Login successful', 'jwt_token': jwt_token, 'is_login_success': True,
                     'menus_data': menu_list}, status=status.HTTP_200_OK)
            # if usr:
            # 	with ldap.connection() as connection:
            # 		# Look up the user by username.
            # 		user = connection.get_user(username=usr)
            # 		#print(user)
            # 	if user:
            # 		jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            # 		jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
            # 		payload = jwt_payload_handler(user)
            # 		jwt_token = jwt_encode_handler(payload)
            # 		usr_db = User.objects.get(username=usr)
            # 		db_token = Token.objects.get_or_create(user=usr_db)
            # 		#json_token = json.dumps(db_token[0])
            # 		menu_list = self.create_menu(user)
            # 		#print(menu_list)
            # 		return Response({'msg':'Login successful', 'jwt_token': jwt_token, 'is_login_success': True,'menus_data':menu_list}, status=status.HTTP_200_OK)
            # 	else:
            # 		return Response({'msg': 'Credentials are not valid!'}, status=status.HTTP_400_BAD_REQUEST)
            # else:
            # 	return Response({'msg': 'Credentials are not valid!'}, status=status.HTTP_400_BAD_REQUEST)
            else:
                if 'username' in request.data and 'password' in request.data:
                    usr = request.data['username']
                    pwd = request.data['password']
                    user = authenticate(request, username=usr, password=pwd)
                    if user is not None:
                        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER  # upto return 64 line no
                        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
                        payload = jwt_payload_handler(user)
                        jwt_token = jwt_encode_handler(payload)
                        # usr_db_token = User.objects.get(username=user)
                        # db_token = Token.objects.get_or_create(user=usr_db_token)
                        # json_token = json.dumps(db_token[0])
                        print(user)
                        menu_list = self.create_menu(user)
                        print(menu_list)
                        return Response({'msg': 'Login successful', 'jwt_token': jwt_token, 'is_login_success': True,
                                         'menus_data': menu_list}, status=status.HTTP_200_OK)
                    else:
                        user_not_registered = {'status': 'failed', 'status_desc': 'User Not Registered.',
                                               'redirect_url': 'localhost:8081/admin/'}
                        return Response(status=status.HTTP_400_BAD_REQUEST, data=user_not_registered)
                else:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    def create_menu(self, user):
        conn = pyodbc.connect(masterconf.connection_string);
        crsr = conn.cursor()

        sql_str = """DECLARE @output varchar(max) = (
							select l1_m.[menu_id]
							 ,[display_name]
							 ,[menu_description]
							 ,[keyword]
							 ,[mapped_url]
							 ,[link_type]
							,[seq_no]
							,
							 (
							 SELECT  l2_m.[menu_id]
							 ,[display_name]
							 ,[menu_description]
							 ,[keyword]
							 ,[mapped_url]
							 ,[link_type]
							 ,[parent_menu_id]
							 ,[seq_no]
							 ,[icon_name]
							 ,permission_list.codename as permission_name
							 ,
							 (SELECT  [menu_id]
							 ,[display_name]
							 ,[menu_description]
							 ,[keyword]
							 ,[mapped_url]
							 ,[link_type]
							 ,[parent_menu_id]
							 ,[seq_no]
							 ,[icon_name]
						 FROM menu_master  l3_m
						 where  l3_m.parent_menu_id = l2_m.menu_id
						 and menu_id in
										 (
											select distinct sub_menu_id
											from menu_group_permission_mapping
											where group_id in
											(select ag.id from
											auth_user au
											inner join auth_user_groups aug
											on au.id = aug.user_id
											inner join auth_group ag
											on ag.id = aug.group_id
											JOIN auth_user_group_metadata metadata
											on metadata.id = aug.id
											AND metadata.record_status = 'Active'
											where au.username = '""" + str(user) + """'
											)
											)
										 and is_active = 1
										 order by seq_no

								FOR JSON AUTO ) child
						 FROM menu_master  l2_m
						 JOIN menu_group_permission_mapping mgp
						 on mgp.sub_menu_id = l2_m.menu_id
						 JOIN auth_permission permission_list
						 on permission_list.id = mgp.permission_id
						 AND mgp.record_status = 'Active'
						 AND mgp.group_id = (select aug.group_id from auth_user_groups aug 
						 join auth_user au on au.id = aug.user_id and au.username = '""" + str(user) + """')
						 where  l2_m.parent_menu_id = l1_m.menu_id
						 and l2_m.menu_id in
										 (
											select distinct sub_menu_id
											from menu_group_permission_mapping
											where group_id in
										 (select ag.id from
											auth_user au
											inner join auth_user_groups aug
											on au.id = aug.user_id
											inner join auth_group ag
											on ag.id = aug.group_id
											JOIN auth_user_group_metadata metadata
											on metadata.id = aug.id
											AND metadata.record_status = 'Active'
											where au.username = '""" + str(user) + """'
											)
											)
										 and is_active = 1
										 order by seq_no

								FOR JSON AUTO
								) child
						  FROM menu_master  l1_m
						  where l1_m.menu_level = 1
						   and l1_m.menu_id in
										 (
											select distinct menu_id
											from menu_group_permission_mapping
											where group_id in
										 (	
											select ag.id from
											auth_user au
											inner join auth_user_groups aug
											on au.id = aug.user_id
											inner join auth_group ag
											on ag.id = aug.group_id
											JOIN auth_user_group_metadata metadata
											on metadata.id = aug.id
											AND metadata.record_status = 'Active'
											where au.username = '""" + str(user) + """'
										 )
										 and record_status = 'Active'
										 )
										 order by seq_no
										 FOR JSON AUTO
										 )
										select @output"""
        try:
            print(sql_str)
            output = crsr.execute(sql_str)
            rows = crsr.fetchall()
            data = []
            for row in rows:
                data.append([x for x in row])
            # print(data)
            # list1 = output.fetchall()
            return data[0][0]
        except Exception as e:
            print(e)
            raise e
