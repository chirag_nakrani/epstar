from django.http import HttpResponse
import os
import io
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser,FormParser
from rest_framework.renderers import JSONRenderer
from Common import masterconf
import pyexcel
import xlrd
import time, datetime
from Common.CommonFunctions import common_util
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Controller.Signature import SignatureController


class SignatureDownload(APIView):
	authentication_classes = (JSONWebTokenAuthentication,)
	parser_class = (MultiPartParser,FormParser,)
	renderer_classes = (JSONRenderer,)
	permission_classes = (IsAuthenticated,)

	@csrf_exempt
	def post(self, request,format=None):
		status_dict = {}
		common_util_obj = common_util.common_utils()
		check_req_flag = common_util_obj.validate_signature_download_req(request.data)
		if check_req_flag:
			try:
				signatureController = SignatureController.SignatureController()
				status_dict = signatureController.handle_signature_download(request)
				# if status_dict['status'] == masterconf.signature_fetched_successfully:
				#     return Response(status=status.HTTP_201_CREATED,data = status_dict)
				if status_dict['status_text'] == masterconf.signature_not_available:
					status_dict["status_desc"] = masterconf.error_codes_desc['E103']
					return Response(status=status.HTTP_400_BAD_REQUEST,data = status_dict)
				else:
					print(status_dict)
					#return Response(status=status.HTTP_200_OK, data=status_dict)
					return status_dict
			except Exception as e:
				#print("Exception: ", e)
				status_dict["level"] = masterconf.system_level
				status_dict["status_code"] = masterconf.system_level_exception_code
				status_dict["status_desc"] = masterconf.system_level_exception
				return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
		else:
			invalid_req = {
				"level": masterconf.request_level,
				"status_code": masterconf.request_level_exception_code,
				"status_desc": masterconf.request_level_exception
			}
			return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)
