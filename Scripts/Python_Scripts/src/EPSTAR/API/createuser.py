from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Common import masterconf
from Common.CommonFunctions import common_util
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password


##############################For inserting fresh records via UI############
class createuser(APIView):

    ## This is the Main API endpoint creating new user

    authentication_classes = (JSONWebTokenAuthentication, )
    parser_class = (MultiPartParser, FormParser, )
    renderer_classes = (JSONRenderer, )
    permission_classes = (IsAuthenticated, )

    @csrf_exempt
    def post(self, request):
        status_dict = {}
        common_util_obj = common_util.common_utils()
        check_req_flag = common_util_obj.validate_createUser_InputReq(request.data)####validating if input request is as expected
        if request.data:
            if check_req_flag:
                metadata = {}
                try:
                    check_if_username_exists = User.objects.filter(username = request.data['username'], is_active = 1)
                    if not check_if_username_exists:
                        print ("request.data : ",request.data)
                        metadata['username'] = request.data['username']
                        metadata['first_name'] = request.data['first_name']
                        metadata['last_name'] = request.data['last_name']
                        metadata['email'] = request.data['email']
                        metadata['is_staff'] = request.data['is_active']
                        metadata['is_active'] = request.data['is_staff']
                        metadata['date_joined'] = request.data['date_joined']
                        metadata['password'] = make_password(request.data['password'])
                        metadata['is_superuser'] = request.data['is_superuser']
                        data_insert = User(**metadata)
                        data_insert.save()
                        status_dict['status_desc'] = masterconf.new_user_success_message
                        return Response ( status=status.HTTP_201_CREATED , data=status_dict )
                    else:
                        status_dict['status_desc'] = masterconf.user_exist_message
                        return Response ( status=status.HTTP_400_BAD_REQUEST , data=status_dict )
                except Exception as e:
                    print("Exception: ", e)
                    status_dict["status_desc"] = e
                    return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
            else:
                invalid_req = {
                    "level":masterconf.request_level,
                    "status_code":masterconf.request_level_exception_code,
                    "status_desc":masterconf.request_level_exception
                }
                return Response(status=status.HTTP_400_BAD_REQUEST, data=invalid_req)

        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=status_dict)
