import requests
import json
import socket
from kombu import Connection
import subprocess
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import datetime
import time
import pyodbc
import os
from json2html import *

# is_duplicate_atm_mstr = False
# is_duplicate_atm_cnfg = False
# is_duplicate_cra = False
if str(os.environ.get('DJANGO_CONFIGURATION')) == 'Uat_Sandbox':
    connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=epstar;UID=SQLDEV;PWD=Expo@123'
elif str(os.environ.get('DJANGO_CONFIGURATION')) == 'Uat':
    connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=192.168.208.17;DATABASE=epstar;UID=sqladmin;PWD=Mssql@1234'
elif str(os.environ.get('DJANGO_CONFIGURATION')) == 'Local':
    connection_string = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER=localhost;DATABASE=epstar;UID=sa;PWD=Mysql@123'

MAILHOST = "smtp.office365.com"
MAILPORT = 587
mail_sender = "epstar@electronicpay.in"
mail_password = "Ruh75373"
server_time = datetime.datetime.fromtimestamp(time.time()).strftime('%d-%m-%Y %H:%M:%S')

def get_user_emails():
    conn = pyodbc.connect(connection_string);
    crsr = conn.cursor()
    email_list = []
    try:
        sql_string = """select distinct(email) from auth_user"""
        crsr.execute(sql_string)
        duprecords = crsr.fetchall()
        if len(duprecords) > 0:
            for rec in duprecords:
                print(str(rec[0]))
                if len(str(rec[0])) > 0:
                    email_list.append(str(rec[0]))
        return email_list
    except Exception as e:
        print(e)

recipients = ['sudhakar.chavan@exponentiadata.com','sarthak.vashisth@exponentiadata.com']#get_user_emails()

def send_mail_for_atm_mstr(duprecords):
    app_subject = "ATTENTION : (EPSUPPLY Tech Team) Duplicate Records Found in ATM Master."
    atm_list = []
    #data = json2html.convert(json=duprecords)
    try:
        message = MIMEMultipart('alternative')
        message['Subject'] = app_subject
        #body = MIMEText(data, 'html')
        for rec in duprecords:
            atm_list.append(rec[0])
        body = """
        Dear Stakeholders,

        Duplicate Records found in ATM Master.
        Following ATM IDs are duplicate in ATM Master - """ + str(atm_list) + """
        Please correct data on production.

        This is system generated mail.Kindly do not reply on it.

        Thank You.
        Regards,
        EPS"""
        message.attach(MIMEText(body))
        # message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        server.sendmail(mail_sender, recipients, message.as_string())
        server.close()
    except Exception as e:
        print(e)


def send_mail_for_atm_cnfg(duprecords):
    app_subject = "ATTENTION : (EPSUPPLY Tech Team) Duplicate Records Found in ATM Config Master."
    atm_list = []
    # data = json2html.convert(json=duprecords)
    try:
        message = MIMEMultipart('alternative')
        message['Subject'] = app_subject
        # body = MIMEText(data, 'html')
        for rec in duprecords:
            atm_list.append(rec[0])
        body = """
            Dear Stakeholders,

            Duplicate Records found in ATM Config.
            Following ATM IDs are duplicate in ATM Config - """ + str(atm_list) + """
            Please correct data on production.

            This is system generated mail.Kindly do not reply on it.

            Thank You.
            Regards,
            EPS"""
        message.attach(MIMEText(body))
        # message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        server.sendmail(mail_sender, recipients, message.as_string())
        server.close()
    except Exception as e:
        print(e)


def send_mail_for_cra(duprecords):
    app_subject = "ATTENTION : (EPSUPPLY Tech Team) Duplicate Records Found in CRA Feasibility Master."
    atm_list = []
    # data = json2html.convert(json=duprecords)
    try:
        message = MIMEMultipart('alternative')
        message['Subject'] = app_subject
        # body = MIMEText(data, 'html')
        for rec in duprecords:
            atm_list.append(rec[0])
        body = """
                Dear Stakeholders,

                Duplicate Records found in CRA Feasibility.
                Following ATM IDs are duplicate in CRA Feasibility - """ + str(atm_list) + """
                Please correct data on production.

                This is system generated mail.Kindly do not reply on it.

                Thank You.
                Regards,
                EPS"""
        message.attach(MIMEText(body))
        # message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        server.sendmail(mail_sender, recipients, message.as_string())
        server.close()
    except Exception as e:
        print(e)


def check_atm_master():
    conn = pyodbc.connect(connection_string);
    crsr = conn.cursor()
    try:
        sql_string = """select * from atm_master am join (
                        select atm_id,bank_code,count(*) atm_count 
                        from atm_master where
                        site_status = 'Active' and record_status = 'Active'
                        group by atm_id,bank_code
                        having count(*) > 1 ) 
                        x on am.atm_id = x.atm_id and 
                        am.bank_code = x.bank_code and 
                        am.site_status = 'Active' and am.record_status = 'Active'"""
        crsr.execute(sql_string)
        duprecords = crsr.fetchall()
        print(duprecords)
        if len(duprecords) > 0:
            send_mail_for_atm_mstr(duprecords)
    except Exception as e:
        print(e)


def check_atm_config():
    conn = pyodbc.connect(connection_string);
    crsr = conn.cursor()
    try:
        sql_string = """select * from ATM_Config_limits am join (
                        select atm_id,bank_code,count(*) atm_count 
                        from ATM_Config_limits where
                        record_status = 'Active'
                        group by atm_id,bank_code
                        having count(*) > 1 ) 
                        x on am.atm_id = x.atm_id and 
                        am.bank_code = x.bank_code and 
                        am.record_status = 'Active'"""
        crsr.execute(sql_string)
        duprecords = crsr.fetchall()
        print(duprecords)
        if len(duprecords) > 0:
            send_mail_for_atm_cnfg(duprecords)
    except Exception as e:
        print(e)


def check_cra_feasibility():
    conn = pyodbc.connect(connection_string);
    crsr = conn.cursor()
    try:
        sql_string = """select * from cra_feasibility am join (
                        select atm_id,bank_code,count(*) atm_count 
                        from cra_feasibility where
                        record_status = 'Active'
                        group by atm_id,bank_code
                        having count(*) > 1 ) 
                        x on am.atm_id = x.atm_id and 
                        am.bank_code = x.bank_code and 
                        am.record_status = 'Active'"""
        crsr.execute(sql_string)
        duprecords = crsr.fetchall()
        print(duprecords)
        if len(duprecords) > 0:
            send_mail_for_cra(duprecords)
    except Exception as e:
        print(e)


if __name__ == "__main__":
    #get_user_emails()
    check_atm_master()
    check_atm_config()
    check_cra_feasibility()
    # print(is_duplicate_atm_mstr, is_duplicate_atm_cnfg, is_duplicate_cra)
    # if is_duplicate_atm_mstr:
        #send_mail_for_app()
    #if is_duplicate_atm_cnfg:
     #   send_mail_for_rabbit()
    #if is_duplicate_cra:
     #   send_mail_for_celery()