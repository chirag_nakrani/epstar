from django.conf.urls import include, url
from . import upload_historical_data

app_name = "HistoricalData"

urlpatterns = [
    url(r'^historicaldata/upload/$', upload_historical_data.UploadHistoricalData.as_view(), name='UploadHistoricalData')
]
