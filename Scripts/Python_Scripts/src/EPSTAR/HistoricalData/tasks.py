
from celery import shared_task
import time,os
import datetime
import glob
from Controller.Routine import BaseCDRController, BaseC3RMISController, BaseCBRController
import re
import pyodbc
from datetime import timedelta
from configurations import Configuration
from EPSTAR.settings import Dev,Local,Uat,Sit
from celery import current_task
from Model.models import generate_ref_id

@shared_task()
def upload_dispense_historical_data_task(request_obj):
    extracted_region = ''
    extracted_is_valid = ''
    extracted_validation_code = ''
    count = 0
    username = request_obj['userinfo']['username'].username
    userinfo = {
        "username": request_obj['userinfo']['username'],
    }
    token_generated = request_obj['access_token']
    data = request_obj['post'].copy()
    file_type = data.get('file_type')
    project_id = data.get('project_id')
    bank_code = data.get('bank_code')
    folder_path = data.get('folder_path')
    created_on = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
    created_on = created_on.strftime ( '%Y-%m-%d %H:%M:%S' )
    region = data.get ( 'region' )
    response_list = os.listdir(folder_path)
    for i in glob.glob(folder_path + "\*"):
        with open(i, 'rb') as f:
            try:
                file_name = os.path.basename(f.name)
                file_extension = re.findall(r'(\w+)$', file_name)
                if file_extension[0] == 'xlsx':
                    upload_date_time = file_name[len(file_name)-14:len(file_name)-5]
                elif file_extension[0] == 'xls':
                    upload_date_time = file_name[len(file_name)-13:len(file_name)-4]
                elif bank_code == 'UBI':
                    upload_date_time = file_name[len(file_name)-9:len(file_name)]
                    project_id = re.findall(r'(\w+)\s+-', file_name)
                    project_id= project_id[0].strip()
                if bank_code == 'UBI' and project_id == 'MOF':
                    project_id = 'MOF_MAH'
                if bank_code == 'UBI' and project_id == 'NCR':
                    project_id = 'MOF_BJK' 
                upload_datetime = str(datetime.datetime.strptime(upload_date_time, '%d%b%Y'))
                request_post = {
                    "file_type": file_type,
                    "project_id": project_id,
                    "bank_code": bank_code,
                    "upload_datatime": upload_datetime,
                    "region": region,
                }
                if bank_code == 'UBI':
                    str_file_extention = ''
                else:
                    str_file_extention = file_extension[0]

                reference_id = generate_ref_id()
                userinfo['reference_id'] = reference_id
                basecdrcontroller = BaseCDRController.BaseCDRController()
                status_dict = basecdrcontroller.handle_upload_request(request_post, i, os.path.basename(f.name),
                                                                      str_file_extention, userinfo)
                data_update_log_query = "select region, is_valid_file, validation_code from data_update_log where data_for_type = 'DISPENSE' and bank_code = '" + bank_code + "' and project_id = '" + project_id + "' and datafor_date_time = '" + upload_datetime + "' and created_reference_id = '"+userinfo['reference_id']+"'"
                try:
                    connection_string = eval(os.environ.get('DJANGO_CONFIGURATION')).connection_string
                    conn = pyodbc.connect(connection_string)
                    crsr = conn.cursor()
                    crsr.execute(data_update_log_query)
                    data_update_log_data = crsr.fetchall()
                    
                    for i in data_update_log_data:
                        extracted_region = i[0]
                        extracted_is_valid = i[1]
                        extracted_validation_code = i[2]
                    insert_str = "Insert into historical_data_log (data_for_type,data_for_datetime,file_name,status,level,file_status,bank_code,project_id,region,is_valid_file,created_on,created_by,created_reference_id, validation_code) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                    values = ('DISPENSE',upload_datetime,os.path.basename (f.name ),status_dict['status_text'], status_dict['level'], status_dict['file_status'], bank_code, project_id, extracted_region,extracted_is_valid, created_on, username, reference_id, extracted_validation_code)
                    crsr.execute ( insert_str , values )
                    conn.commit()
                    count += 1
                    if len(response_list) > 1:
                        current_task.update_state(state='PROGRESS',
                                              meta={'current': count, 'total': len(response_list),
                                                    'percent': int((float(count) / len(response_list)) * 100)})
                except Exception as e:
                    print("Exception from tasks.py : ",e)
                    raise e
            except Exception as e:
                print ("Exception : ",e)
                connection_string = eval(os.environ.get('DJANGO_CONFIGURATION')).connection_string
                conn = pyodbc.connect(connection_string)
                crsr = conn.cursor()
                insert_str = "Insert into historical_data_log (data_for_type, file_name,status,level,file_status,bank_code,project_id,region,is_valid_file,created_on,created_by,created_reference_id) values(?,?,?,?,?,?,?,?,?,?,?,?)"
                values = ('DISPENSE',os.path.basename (f.name ),'Failed', 'data', 'improper file type', bank_code, project_id, extracted_region,0, created_on, username, reference_id)
                crsr.execute ( insert_str , values )
                conn.commit()


##################Controller for history data upload of C3R files################################################################
@shared_task()
def upload_C3R_historical_data_task(request_obj):
    extracted_is_valid = ''
    extracted_validation_code = ''
    extracted_cra_name = ''
    username = request_obj['userinfo']['username'].username
    userinfo = {
        "username": request_obj['userinfo']['username'],
    }
    token_generated = request_obj['access_token']
    data = request_obj['post'].copy()
    file_type = data.get('file_type')
    folder_path = data.get('folder_path')
    
    for i in glob.glob(folder_path + "\*.xlsx"):
        created_on = datetime.datetime.utcnow()+timedelta(hours=5,seconds=1800)
        created_on = created_on.strftime ( '%Y-%m-%d %H:%M:%S' )
        with open(i, 'rb') as f:
            try:
                file_name = os.path.basename(f.name)
                upload_datetime = file_name[len(file_name)-14:len(file_name)-5]
                upload_datetime = str(datetime.datetime.strptime(upload_datetime, '%d%b%Y'))
                request_post = {
                    "file_type": file_type,
                    "upload_datatime": upload_datetime,
                    "cra" : 'ALL'
                }
                str_file_extention = 'xlsx'
                reference_id = generate_ref_id()
                userinfo['reference_id'] = reference_id
                baseC3RMIScontroller = BaseC3RMISController.BaseC3RMISController ( )
                status_dict = baseC3RMIScontroller.handle_upload_request(request_post, i, os.path.basename(f.name),
                                                                      str_file_extention, userinfo)
                data_update_log_query = "select is_valid_file, validation_code, cra_name, datafor_date_time from data_update_log where data_for_type = 'C3R' and datafor_date_time = '"+upload_datetime+"' and created_reference_id = '"+userinfo['reference_id']+"'"
                try:
                    connection_string = eval(os.environ.get('DJANGO_CONFIGURATION')).connection_string
                    conn = pyodbc.connect(connection_string)
                    crsr = conn.cursor()
                    crsr.execute(data_update_log_query)
                    data_update_log_data = crsr.fetchall()
                    
                    for i in data_update_log_data:
                        extracted_is_valid = i[0]
                        extracted_validation_code = i[1]
                        extracted_cra_name = i[2]
                    insert_str = "Insert into historical_data_log (data_for_type,data_for_datetime,file_name,status,level,file_status,is_valid_file,created_on,created_by,created_reference_id, validation_code, cra_name) values(?,?,?,?,?,?,?,?,?,?,?,?)"
                    values = ('C3R',upload_datetime,os.path.basename (f.name ),status_dict['status_text'], status_dict['level'], status_dict['status_text'], extracted_is_valid, created_on, username, reference_id, extracted_validation_code, extracted_cra_name)
                    crsr.execute ( insert_str , values )
                    conn.commit()
                except Exception as e:
                    print("Exception from tasks.py : ",e)
                    raise e
            except Exception as e:
                print ("Exception : ",e)
                connection_string = eval(os.environ.get('DJANGO_CONFIGURATION')).connection_string
                conn = pyodbc.connect(connection_string)
                crsr = conn.cursor()
                insert_str = "Insert into historical_data_log (data_for_type, file_name,status,level,file_status,is_valid_file,created_on,created_by,created_reference_id) values(?,?,?,?,?,?,?,?,?)"
                values = ('C3R_VMIS',os.path.basename (f.name ),'Failed', 'data', e, 0, created_on, username, reference_id)
                crsr.execute ( insert_str , values )
                conn.commit()
                



#######################historical data upload for balance files###################################################
@shared_task()
def upload_cbr_historical_data_task(request_obj):
    extracted_region = ''
    extracted_is_valid = ''
    extracted_validation_code = ''
    username = request_obj['userinfo']['username'].username
    userinfo = {
        "username": request_obj['userinfo']['username'],
    }
    token_generated = request_obj['access_token']
    data = request_obj['post'].copy()
    file_type = data.get('file_type')
    project_id = data.get('project_id')
    bank_code = data.get('bank_code')
    region = data.get('region')
    folder_path = data.get('folder_path')
    for i in glob.glob(folder_path + "\*"):
        created_on = datetime.datetime.utcnow() + timedelta(hours=5, seconds=1800)
        created_on = created_on.strftime('%Y-%m-%d %H:%M:%S')
        with open(i, 'rb') as f:
            try:
                file_name = os.path.basename(f.name)
                file_extension = re.findall(r'(\w+)$', file_name)
                if file_extension[0] == 'xlsx':
                    upload_date_time = file_name[len(file_name)-21:len(file_name)-9]
                elif file_extension[0] == 'xls' or file_extension[0] == 'csv' or file_extension[0] == 'txt':
                    upload_date_time = file_name[len(file_name)-20:len(file_name)-8]
                elif bank_code == 'UBI':
                    upload_date_time = file_name[len(file_name)-19:len(file_name)-7]
                upload_datetime = str(datetime.datetime.strptime(upload_date_time, '%d%b%Y_%H'))
                print('upload_datetime',upload_datetime)
                request_post = {
                    "file_type": file_type,
                    "project_id": project_id,
                    "bank_code": bank_code,
                    "upload_datatime": upload_datetime,
                    "region": region,
                }
                str_file_extention = file_extension[0]
                reference_id = generate_ref_id()
                userinfo['reference_id'] = reference_id
                basecbrcontroller = BaseCBRController.BaseCBRController ( )
                status_dict = basecbrcontroller.handle_upload_request(request_post, i, os.path.basename(f.name),
                                                                      str_file_extention, userinfo)
                data_update_log_query = "select region, is_valid_file, validation_code from data_update_log where data_for_type = 'CBR' and bank_code = '"+bank_code+"' and project_id = '"+project_id+"' and datafor_date_time = '"+upload_datetime + "' and created_reference_id = '"+userinfo['reference_id']+"'"
                try:
                    
                    connection_string = eval(os.environ.get('DJANGO_CONFIGURATION')).connection_string
                    conn = pyodbc.connect(connection_string)
                    crsr = conn.cursor()
                    crsr.execute(data_update_log_query)
                    data_update_log_data = crsr.fetchall()
                    
                    for i in data_update_log_data:
                        extracted_region = i[0]
                        extracted_is_valid = i[1]
                        extracted_validation_code = i[2]
                    insert_str = "Insert into historical_data_log (data_for_type,data_for_datetime,file_name,status,level,file_status,bank_code,project_id,region,is_valid_file,created_on,created_by,created_reference_id, validation_code) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
                    values = ('CBR',upload_datetime,os.path.basename (f.name ),status_dict['status_text'], status_dict['level'], status_dict['file_status'], bank_code, project_id, extracted_region,extracted_is_valid, created_on, username, reference_id, extracted_validation_code)
                    crsr.execute ( insert_str , values )
                    conn.commit()
                except Exception as e:
                    print("Exception from tasks.py : ",e)
                    raise e
            except Exception as e:
                print ("exception : ", e)
                connection_string = eval(os.environ.get('DJANGO_CONFIGURATION')).connection_string
                conn = pyodbc.connect(connection_string)
                crsr = conn.cursor()
                insert_str = "Insert into historical_data_log (data_for_type, file_name,status,level,file_status,bank_code,project_id,region,is_valid_file,created_on,created_by,created_reference_id) values(?,?,?,?,?,?,?,?,?,?,?,?)"
                values = ('CBR',os.path.basename (f.name ),'Failed', 'data', 'improper file type', bank_code, project_id, extracted_region,0, created_on, username, reference_id)
                crsr.execute ( insert_str , values )
                conn.commit()
