from configurations import importer
if not importer.installed:
    import configurations
    configurations.setup()