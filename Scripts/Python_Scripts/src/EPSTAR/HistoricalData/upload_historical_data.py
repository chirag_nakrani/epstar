from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.renderers import JSONRenderer
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from Common import masterconf
from Common.CommonFunctions import common_util
from .tasks import upload_dispense_historical_data_task, upload_C3R_historical_data_task, upload_cbr_historical_data_task
import os


class UploadHistoricalData(APIView):

    ## This is the Main API endpoint class for Historical Data Upload

    authentication_classes = (JSONWebTokenAuthentication, )
    parser_class = (MultiPartParser, FormParser, )
    renderer_classes = (JSONRenderer, )
    permission_classes = (IsAuthenticated, )

    def post(self, request):
        access_token = request.META['HTTP_AUTHORIZATION'].split(' ')[1]
        request_obj = {
            'post': request.data,
            'userinfo':request.userinfo,
            'access_token':access_token,
        }
        if (request.data['file_type']).upper() == 'DISPENSE':
            upload_dispense_historical_data_task.delay(request_obj)
        elif (request.data['file_type']).upper() == 'C3R_VMIS':
            upload_C3R_historical_data_task.delay(request_obj)
        elif (request.data['file_type']).upper() == 'CBR':
            upload_cbr_historical_data_task.delay(request_obj)
        status_dict = {}
        if os.path.exists(request.data['folder_path']):
            if len(os.listdir(request.data['folder_path'])) == 0:
                status_dict = {
                "status":"Directory is empty. Kindly store the history files in the respective directory."
                }
                return Response(status=status.HTTP_400_BAD_REQUEST , data=status_dict)
            else:
                status_dict = {
                    "status":"Upload is in progress please check your mailbox"
                }
            return Response(status=status.HTTP_201_CREATED, data=status_dict)
        else:
            status_dict = {
                    "status":"Directory doesn't exists."
                }
            return Response(status=status.HTTP_400_BAD_REQUEST , data=status_dict)