import requests
import json
import socket
from kombu import Connection
import subprocess
import os
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import smtplib
import datetime
import time

is_app_down = False
is_rabbit_mq_down = False
is_celery_down = False

MAILHOST = "smtp.office365.com"
MAILPORT = 587
mail_sender= "epstar@electronicpay.in"
mail_password = "Ruh75373"
recipients = ['eps@exponentiadata.com','vrunda.sherikar@electronicpay.in','rakesh.dandekar@electronicpay.in']
server_time = datetime.datetime.fromtimestamp(time.time()).strftime('%d-%m-%Y %H:%M:%S')

def send_mail_for_app():
    app_subject = "ATTENTION : (EPSUPPLY Tech Team) EPSTAR Application seems to be down."
    try:
        message = MIMEMultipart('alternative')
        message['Subject'] = app_subject
        #body = MIMEText(json_error_codes, 'html')
        body = """
        Dear Stakeholders,

        EPSTAR Application running on Apache server on IP: 192.168.208.16 seems to be down around """ +  str(server_time) + """
        Please ensure to run Apache server on Production.

        This is system generated mail.Kindly do not reply on it.

        Thank You.
        Regards,
        EPS"""
        message.attach(MIMEText(body))
        #message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        server.sendmail(mail_sender, recipients, message.as_string())
        server.close()
    except Exception as e:
        print(e)

def send_mail_for_rabbit():
    rabbit_subject = "ATTENTION : (EPSUPPLY Tech Team) RabbitMQ Application seems to be down."
    try:
        message = MIMEMultipart('alternative')
        message['Subject'] = rabbit_subject
        #body = MIMEText(json_error_codes, 'html')
        body = """
        Dear Stakeholders,

        RabbitMQ Application running on production server on IP: 192.168.208.16 seems to be down around  """ +  str(server_time) + """
        Please ensure to run RabbitMQ server on Production.

        This is system generated mail.Kindly do not reply on it.

        Thank You.
        Regards,
        EPS"""
        message.attach(MIMEText(body))
        #message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        server.sendmail(mail_sender, recipients, message.as_string())
        server.close()
    except Exception as e:
        print(e)

def send_mail_for_celery():
    celery_subject = "ATTENTION : (EPSUPPLY Tech Team) Celery Application seems to be down."
    try:
        message = MIMEMultipart('alternative')
        message['Subject'] = celery_subject
        #body = MIMEText(json_error_codes, 'html')
        body = """
        Dear Stakeholders,
        
        Celery Application running on production server on IP: 192.168.208.16 seems to be down around  """ +  str(server_time) + """
        Please ensure to run RabbitMQ server on Production.

        This is system generated mail.Kindly do not reply on it.

        Thank You.
        Regards,
        EPS"""
        message.attach(MIMEText(body))
        #message = 'Subject: {}\n\n{}'.format(subject, json_error_codes)
        server = smtplib.SMTP()
        server.connect(MAILHOST, MAILPORT)
        server.starttls()
        server.login(mail_sender, mail_password)
        server.sendmail(mail_sender, recipients, message.as_string())
        server.close()
    except Exception as e:
        print(e)

def check_application():
    headers = {'Content-Type': 'application/json'}
    app_url = 'https://epsupply.electronicpay.in/'
    try:
        resp = requests.get(app_url,headers=headers)
    except Exception as e:
        global is_app_down
        is_app_down = True
        print("Application is down")

def check_rabbit_mq():
    celery_broker_url = "amqp://localhost"
    try:
        conn = Connection(celery_broker_url)
        conn.ensure_connection(max_retries=1)
    except socket.error:
        global is_rabbit_mq_down
        is_rabbit_mq_down = True

def check_celery():
    try:
        subprocess.check_output("celery -A EPSTAR status",shell=True)
    except Exception as e:
        global is_celery_down
        is_celery_down = True
        print(" is_celery_down ",is_celery_down)
        print(e)

if __name__ == "__main__":
    check_application()
    check_celery()
    check_rabbit_mq()
    print(is_app_down,is_celery_down,is_rabbit_mq_down)
    if is_app_down:
        send_mail_for_app()
    if is_rabbit_mq_down:
        send_mail_for_rabbit()
    if is_celery_down:
        send_mail_for_celery()