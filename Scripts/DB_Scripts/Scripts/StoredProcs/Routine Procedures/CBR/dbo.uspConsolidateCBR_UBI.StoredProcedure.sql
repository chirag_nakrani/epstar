USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspConsolidateCBR_UBI]    Script Date: 5/21/2019 5:36:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[uspConsolidateCBR_UBI] 
   (@datafor_date_time varchar(100),@projectid varchar(50), @region varchar(50),@referenceid varchar(50),@systemUser varchar(50),@out varchar(200) OUTPUT)
AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;
DECLARE @bankcode nvarchar(10) = 'UBI'
DECLARE @datafor nvarchar(10) = 'CBR'
DECLARE @activestatus nvarchar(20) = 'Active'
DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
DECLARE @approvedstatus nvarchar(20) = 'Approved'

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		IF EXISTS(
			Select 1 as ColumnName
			FROM [dbo].[data_update_log]
			WHERE datafor_date_time = @datafor_date_time AND 
					bank_code = @bankcode AND 
					data_for_type = @datafor AND 
					record_status = @approvedstatus AND
					project_id = @projectid AND 
					region=@region AND
					created_reference_id = @referenceid 
			) 
		BEGIN			--- Check for approved status in DUL

		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			 values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			IF EXISTS   (						
				SELECT 1
				FROM [dbo].[data_update_log]
				WHERE datafor_date_time = @datafor_date_time AND 
					bank_code =@bankcode AND 
					data_for_type = @datafor AND 
					record_status = @activestatus  
			     	--project_id = @projectid
				)	
				BEGIN				--- Check Active status in DUL


					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
					values ('Checking in bank wise file for previously active record', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
							IF EXISTS
							(
								Select 1 as ColumnName
								FROM [dbo].[cash_balance_file_ubi]
								WHERE datafor_date_time =  @datafor_date_time AND  
										record_status = @activestatus  
							) 
							BEGIN						-----Check active in bankwise file
							INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
							values ('Checking in cash balance register for previously active record', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
								IF EXISTS(
										Select 1 as ColumnName
										FROM [dbo].[cash_balance_register]
										WHERE datafor_date_time = @datafor_date_time AND 
												bank_name = @bankcode  AND 
												record_status = @activestatus  
										)
										BEGIN			---- Check Cash Balance Register Table for Active Status (START)	

										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Active record found!! Changing status from Active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
											


											UPDATE history
											SET history.record_status = 'Deleted',
												history.deleted_on = @current_datetime_stmp,
												history.deleted_by = @systemUser,
												history.deleted_reference_id = @referenceid
											FROM cash_balance_file_ubi history
											JOIN cash_balance_file_ubi new
											on new.atm_id = history.atm_id
											AND ISNULL(new.project_id,0) = ISNULL(history.project_id,0)
											AND new.datafor_date_time = @datafor_date_time
											AND history.datafor_date_time = @datafor_date_time
											AND new.record_status = 'Approved'
											AND history.record_status = 'Active'



											UPDATE cash_balance_file_ubi set record_status='Active',
													modified_on=@current_datetime_stmp,
													modified_by=@systemUser , 
													modified_reference_id=@referenceid

														where  datafor_date_time=@datafor_date_time 
														and created_reference_id=@referenceid
														and record_status='Approved'

											--DECLARE @SetWhere_CBR_bankWise1 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
											--		@datafor_date_time   + ''' and  created_reference_id = ''' + @referenceid + ''''
											
											--EXEC dbo.[uspSetStatus] 'dbo.cash_balance_file_ubi',
											--					@approvedstatus,@activestatus,
											--					@current_datetime_stmp,@systemUser,
											--					@referenceid,@SetWhere_CBR_bankWise1,
											--					@out OUTPUT

											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records successfully marked as Deleted. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

											---------inserting data after status update for revision
												INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id,
														error_code
													)
											
													Select datafor_date_time, 
															@bankcode,
															CBH.atm_id,
															type_01*100,
															CASE 
																WHEN substring(CBH.atm_id,1,1) like 'N%' THEN type_02*200 ELSE type_03*200 END,
															CASE 
																WHEN  substring(CBH.atm_id,1,1) like 'N%' THEN type_03*500 ELSE type_02*500 END,
															type_04*2000,
															CASE 
																WHEN  substring(CBH.atm_id,1,1) like 'N%'
																	THEN  type_01*100 + type_02*200+type_03*500+type_04*2000 
																	ELSE   type_01*100 + type_02*500+type_03*200+type_04*2000 
															 END AS closing_bal,
															am.project_id,
															region,
															@activestatus,
															CBH.is_valid_record,
															CBH.created_on,
															CBH.created_by,
															CBH.created_reference_id,
															CBH.approved_on,
															CBH.approved_by,
															CBH.approved_reference_id,
															CBH.modified_on,
															CBH.modified_by,
															CBH.modified_reference_id ,
															CBH.error_code  
														from  
														[dbo].[cash_balance_file_ubi] CBH
														left join atm_master AM ON
														CBH.atm_id=AM.atm_id
														and AM.bank_code=@bankcode
														and AM.record_status='Active'
														and AM.site_status='Active'

														where datafor_date_time= @datafor_date_time             --remove created
														and CBH.record_status = @activestatus
														and CBH.project_id = @projectid   
														and CBH.created_reference_id = @referenceid

														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)



														UPDATE history
														SET history.record_status = 'Deleted',
															history.deleted_on = @current_datetime_stmp,
															history.deleted_by = @systemUser,
															history.deleted_reference_id = @referenceid
														FROM cash_balance_register history
														JOIN cash_balance_register new
														on new.atm_id = history.atm_id
														AND ISNULL(new.project_id,0) = ISNULL(history.project_id,0)
														AND new.bank_name = @bankcode
														AND history.bank_name = @bankcode
														AND new.datafor_date_time  = @datafor_date_time
														AND history.datafor_date_time = @datafor_date_time
														AND new.created_reference_id <> history.created_reference_id
														AND history.created_reference_id <> @referenceid
														and new.record_status = @activestatus
														AND history.record_status = @activestatus


														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
														values ('Start:updateing dataupdate log to mark active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


														DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor + ''' and project_id = ''' + @projectid + ''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
											


														IF EXISTS (
																		SELECT 1 from dbo.cash_balance_register where 
																				datafor_date_time = @datafor_date_time and  
																				bank_name = @bankcode and 
																				record_status = @activestatus and 
																				created_reference_id=@referenceid
																				--region=@region and
																				--project_id = @projectid
																	)
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END


								INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure  Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
								


										END				---- Check Cash Balance Register Table for Active Status (END)

							END
							ELSE		------	BOMH ELSE
							BEGIN
								RAISERROR(50050,16,1)		
										--ELSE			---- Check Cash Balance Register Table ELSE
										--BEGIN 
										--END			

									----- BOMH Active Check Start(END)
							--ELSE		------	BOMH ELSE
							--BEGIN
							--END
				
						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN
					UPDATE cash_balance_file_ubi set record_status='Active',
						modified_on=@current_datetime_stmp,
						modified_by=@systemUser , 
						modified_reference_id=@referenceid

							where  datafor_date_time=@datafor_date_time 
							and created_reference_id=@referenceid
							and record_status='Approved'

					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
					values ('Records have been marked as Active status. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
							
					---------inserting data after status update for revision
												INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id,
														error_code
													)									--hop4cash:100, hop3cash:200, hop
											
													Select datafor_date_time, 
															@bankcode,
															CBH.atm_id,
															type_01*100,
															CASE 
																WHEN substring(CBH.atm_id,1,1) like 'N%' THEN type_02*200 ELSE type_03*200 END,
															CASE 
																WHEN  substring(CBH.atm_id,1,1) like 'N%' THEN type_03*500 ELSE type_02*500 END,
															type_04*2000,
															CASE 
																WHEN  substring(CBH.atm_id,1,1) like 'N%'
																	THEN  type_01*100 + type_02*200+type_03*500+type_04*2000 
																	ELSE   type_01*100 + type_02*500+type_03*200+type_04*2000 
															 END AS closing_bal,
															AM.project_id,
															region,
															'Active',
															CBH.is_valid_record,
															CBH.created_on,
															CBH.created_by,
															CBH.created_reference_id,
															CBH.approved_on,
															CBH.approved_by,
															CBH.approved_reference_id,
															CBH.modified_on,
															CBH.modified_by,
															CBH.modified_reference_id ,
															CBH.error_code  
														from  
														[dbo].[cash_balance_file_ubi] CBH
														left join atm_master AM ON
														CBH.atm_id=AM.atm_id
														and AM.bank_code='UBI'
														and AM.record_status='Active'
														and AM.site_status='Active'

														where datafor_date_time= @datafor_date_time            --remove created
														and CBH.record_status = 'Active'
														and CBH.project_id = @projectid   
														and CBH.created_reference_id = @referenceid

														DECLARE @SetWhere_DataUpdateLog_dul VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor 
																	 + ''' and project_id = ''' + @projectid +
																	  ''' and region= ''' +@region+''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',
																		@approvedstatus,@activestatus,
																		@current_datetime_stmp,@systemUser,
																		@referenceid,@SetWhere_DataUpdateLog_dul,@out OUTPUT
											


														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
														 values ('Data Consolidated Successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														IF EXISTS (SELECT 1 from dbo.cash_balance_register
														 where datafor_date_time = @datafor_date_time
														  and  bank_name = @bankcode 
														  and record_status = @activestatus 
														
														  )
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' 
																		AND sub_category = 'Consolidation Successful'
																		) 
														END
														ELSE
															BEGIN
																RAISERROR (50003,16,1)
															END
															INSERT INTO dbo.execution_log
															 (description,execution_date_time,
															 process_spid,process_reference_id,
															 execution_program,executor_method,
															 executed_by) 
																 values ('Execution of Stored Procedure Completed',
																 DATEADD(MI,330,GETUTCDATE()),@@SPID,
																 @referenceid,'Stored Procedure',
																 OBJECT_NAME(@@PROCID),@systemUser)

				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
			BEGIN
				RAISERROR(50010,16,1)
			END
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
				values ('Execution of [dbo].[uspConsolidateCBR_ubi] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

END
