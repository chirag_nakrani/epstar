USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspValidateCbrFiles]    Script Date: 5/14/2019 4:04:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspValidateCbrFiles] 
@datafor_date_time varchar(50) ,@bankcode nvarchar(10), @projectid varchar(50),@region varchar(50), @recordstatus varchar(50),@referenceid varchar(50),@systemUser varchar(50),@debug BIT = 0
AS
BEGIN
SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRY
				DECLARE @Cmd NVARCHAR(MAX);
				DECLARE @Result varchar(100) = 60006;
				DECLARE @out varchar(100);
				DECLARE @successVal varchar(30);
				DECLARE @failedVal varchar(30);
				DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
				DECLARE @out1 varchar(100);
				DECLARE @procedurename nvarchar(100) = (SELECT OBJECT_NAME(@@PROCID))
		
	
    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	values ('Execution of [dbo].[uspValidateCbrFiles] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			
	------------------------ Testing Variables -----------------------------
	--DECLARE @datafor_date_time varchar(50)  = '2018-11-21 10:00:00'
	--DECLARE @bankcode nvarchar(10) = 'SBI'
	--DECLARE @projectid varchar(50) ='MOF'
	--DECLARE @recordstatus varchar(50)  ='Uploaded'
	--DECLARE @referenceid varchar(50) = 'C10000110332621'
	--DECLARE @systemUser varchar(50) = 'exponentiaadmin'
	----------------------------------------------------------------------------

				SET @Cmd = CASE 
								WHEN @bankcode = 'ALB' 
								THEN 'EXEC uspDataValidationALB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'BOMH' 
								THEN 'EXEC uspDataValidationBOMH ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'BOI' 
								THEN 'EXEC uspDataValidationBOI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'BOB' 																															   
								THEN 'EXEC uspDataValidationBOB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'CBI' 																															
								THEN 'EXEC uspDataValidationCBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'CAB' 																															 
								THEN 'EXEC uspDataValidationCAB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'CORP' 																															 
								THEN 'EXEC uspDataValidationCORP ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'DENA' 																														  
								THEN 'EXEC uspDataValidationDENA ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'IDBI' 																														  
								THEN 'EXEC uspDataValidationIDBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'IOB' 																															   
								THEN 'EXEC uspDataValidationIOB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'LVB' 																															   
								THEN 'EXEC uspDataValidationLVB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'PSB' 																															   
								THEN 'EXEC uspDataValidationPSB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'RSBL' 																															   
								THEN 'EXEC uspDataValidationRSBL ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'SBI' 																															   
								THEN 'EXEC uspDataValidationSBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+', @Result OUTPUT ;'
								WHEN @bankcode = 'UCO' 																															   
								THEN 'EXEC uspDataValidationUCO ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'UBI' 																															   
								THEN 'EXEC uspDataValidationUBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
								WHEN @bankcode = 'VJB' 																															   
								THEN 'EXEC uspDataValidationVJB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+',@Result OUTPUT ;'
					
								END
	
				EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 
		
			IF(@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence from app_config_param where category = 'CBR_Validation' and sub_category = 'Error')
			END

------ Fetching sequence from table for success and error
			SET @successVal =(
							SELECT sequence from  [dbo].[app_config_param]
							where category = 'File Operation' and sub_category = 'Data Validation Successful'
							)
                            
            SET @failedVal =(
							SELECT sequence from  [dbo].[app_config_param]
							where  category = 'File Operation' and sub_category = 'Data Validation Failed'
							)
							
									            
            IF(@Result = @successVal )
				BEGIN	
					SELECT @Result;	
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedurename
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SELECT @ErrorNumber;				
			 END CATCH
	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	values ('Execution of [dbo].[uspValidateCbrFiles] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
END
