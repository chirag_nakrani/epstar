 
/****** Object:  StoredProcedure [dbo].[uspDataValidationSBI]    Script Date: 06-02-2019 12:17:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[uspDataValidationSBI]
( 
	@datafor_date_time varchar(50) ,@projectid varchar(50),@region varchar(50),@ForRecordstatus varchar(50),@referenceid varchar(50),@systemUser varchar(50),@Debug BIT = 0,@outputVal VARCHAR(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50)
	DECLARE @errorcode nvarchar(max)
	DECLARE @bankcode nvarchar(10) = 'SBI'
	DECLARE @datafor nvarchar(10) = 'CBR'
    DECLARE @activestatus nvarchar(20) = 'Active'
	DECLARE @approvedstatus nvarchar(20) = 'Approved'
	DECLARE @uploadedstatus nvarchar(20) = 'Uploaded'

	--SELECT SESSION_ID from sys.dm_exec_sessions; 

	IF (@Debug = 1)
    BEGIN
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
        PRINT '-- Execution of [dbo].[uspDataValidationSBI] Started.';
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';
    END;

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[uspDataValidationSBI] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


------------------ Check if file is present in Data Update Log---------------

	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[data_update_log] WITH (NOLOCK)
				WHERE [datafor_date_time] = @datafor_date_time AND 
					  bank_code = @bankcode AND 
					  data_for_type = @datafor and 
					  record_status = @ForRecordstatus AND
					  project_id = @projectid
					  AND created_reference_id = @referenceid
					  AND region = @region
				)
				
		BEGIN

			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Checking in Data Update Log Table ';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			
------------------ Check if file is present in Bank Wise File Table ----------------------

			IF EXISTS( Select 1 as ColumnName
						FROM [dbo].[cash_balance_file_sbi_staging] WITH (NOLOCK)
						  WHERE [datafor_date_time] = @datafor_date_time AND 
							    record_status = @ForRecordstatus 
					         -- AND project_id = @projectid  --changes for project removal
							    AND created_reference_id = @referenceid
						
				)
				
				BEGIN
					IF (@Debug = 1)
					BEGIN
					    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
					    PRINT '-- Checking in [dbo].[cash_balance_file_sbi]  ';
					    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
					END;
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in [dbo].[cash_balance_file_sbi]', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
---------------- Select total count of records present in SBI file for particular date and status------------
				 

				 -- pasted same code late rin the code to get gount of actual EPS records
				--SET @CountTotal = (	
				--					SELECT count(1) 
				--					FROM [dbo].[cash_balance_file_sbi_staging] WITH (NOLOCK)
				--		            WHERE [datafor_date_time] = @datafor_date_time 
				--						  and record_status = @ForRecordstatus 
				--						  AND project_id = @projectid 
				--						  AND created_reference_id = @referenceid
										
					
				--				   ) 

--------------------- Creating temporary table to process multiple update operations ---------------------------
	
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Creating Temporary Table and storing data ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Creating Temporary Table and storing data', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				SELECT * 
				INTO #temp_cash_balance_file_sbi 
				FROM [dbo].[cash_balance_file_sbi_staging] WITH (NOLOCK)
				WHERE  datafor_date_time=  @datafor_date_time
					   AND record_status = @ForRecordstatus  
					 --AND project_id = @projectid
					  AND created_reference_id = @referenceid
					  AND msvendor in ('NCR (MOF)','EPS (TO)','EPS (MOF)')

					  -- Getting count of required records only instead of entire SBI records
					  SET @CountTotal = (   SELECT count(1) 
											FROM #temp_cash_balance_file_sbi   
											WHERE datafor_date_time= @datafor_date_time
											  AND record_status = @ForRecordstatus  
											  --AND project_id = @projectid
											  AND created_reference_id = @referenceid
											  AND msvendor in ('NCR (MOF)','EPS (TO)','EPS (MOF)')
											  
											  )
					 

			
----------------------------------------Validation 1--------------------------		
--							Check For ATM status in Master Data				--
------------------------------------------------------------------------------
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for AMS master Started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				--select * from #temp_cash_balance_file_sbi

				UPDATE #temp_cash_balance_file_sbi 
				SET   error_code =  
						CASE 
							WHEN (	atm_id IN (
												SELECT atm_id FROM 
												atm_master WITH (NOLOCK) WHERE site_status IN ('Live','Active')
												AND record_status = @activestatus
												AND bank_code = @bankcode
												 --AND project_id = @projectid
											)
								 )
							THEN NULL
							ELSE
								(
									SELECT cast(sequence as varchar(50)) 
									FROM app_config_param
									 where 
									category = 'CBR_Validation' 
									and sub_category = 'atm_not_live'
								)
						END	

				--select * from #temp_cash_balance_file_sbi
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for AMS master Completed ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
										  
----------------------------------------Validation 2------------------------------------		
--		           Hopper wise balance should match with the ending balance			  --
----------------------------------------------------------------------------------------	
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Hopper wise balance should match with the ending balance Started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Hopper wise balance should match with the ending balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				UPDATE #temp_cash_balance_file_sbi 
				SET   error_code =  
				CASE  
					WHEN bills_hopr2*100+bills_hopr3*500+bills_hopr4*2000+[bills_hopr5]*200 <> total_remaining_cash_def_curr	
					THEN	
						CASE
							WHEN (error_code IS NULL)
							THEN	
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Amount Mismatch')
				 			ELSE 
								CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Amount Mismatch')) as varchar(max))
						END
					ELSE error_code
				END

				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Hopper wise balance should match with the ending balance Completed';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Hopper wise balance should match with the ending balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
----------------------------------------Validation 3----------------------------	
------			1.switch balance = 0			
------			2.switch balance < 0
------			3.switch balance is in decimal values
------			4.denomination wise value does not match with the available data
--------------------------------------------------------------------------------
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Switch balance check started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Switch balance check started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	
				UPDATE #temp_cash_balance_file_sbi
				SET   error_code =  
				CASE 
					WHEN	total_remaining_cash_def_curr = 0
						OR  total_remaining_cash_def_curr < 0
						OR	total_remaining_cash_def_curr <> FLOOR(total_remaining_cash_def_curr)
						OR	(
								bills_hopr2*100 %100 <>0
							 ) OR 
							(
								[bills_hopr5]*200 %200 <>0
							 ) OR
							(
								bills_hopr3*500 %500 <>0
							 ) OR
							(
								bills_hopr4*2000 %2000 <>0
							 ) 
					THEN	
						CASE
							WHEN (error_code IS NULL)
							THEN	
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'numeric_validation')
				 			ELSE 
								CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'numeric_validation')) as varchar(max))
						END
					ELSE error_code
				   END		

										
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Switch banalnce check Completed';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;	
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Switch balance check Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	
-------------------------------------------Validation 4 ---------------------------------		
------			1.switch balance > cassette capacity			
------			2.switch balance > Bank Limit (In case of UBI and CBI only)
------			3.switch balance > Insurance Limit
-----------------------------------------------------------------------------------------
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Cassette Capacity, Insurance Limit, Bank Limit check started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Cassette Capacity, Insurance Limit, Bank Limit check started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
					
				UPDATE #temp_cash_balance_file_sbi						
				SET     error_code =  
					CASE 
						WHEN	total_remaining_cash_def_curr >
								(
								select (COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) *		   COALESCE(bc.capacity_50,0)  *50)  +
										(COALESCE(cc.cassette_100_count, clm.cassette_100_count,0)     * COALESCE(bc.capacity_100 ,sa.deno_100_bill_capacity ) *100) +
										(COALESCE(cc.cassette_200_count, clm.cassette_200_count,0)     * COALESCE(bc.capacity_200 ,sa.deno_200_bill_capacity ) *200) +    
										(COALESCE(cc.cassette_500_count, clm.cassette_500_count,0)     * COALESCE(bc.capacity_500 ,sa.deno_500_bill_capacity ) *500) +
										(COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0)   * COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity)*2000) 
                                         AS total_cassette_capacity
                                         FROM 
                                              atm_master AS  mast
                                              INNER JOIN     ATM_Config_limits clm
                                              ON            mast.atm_id = clm.atm_id  AND
															mast.site_status='Active' AND
                                                            mast.site_code = clm.site_code AND
                                                            clm.record_status = @activestatus                              
                                              LEFT JOIN      
                                                   (
                                                   SELECT 
                                                        project_id,
                                                        bank_code, 
                                                        site_code,
                                                        atm_id, 
                                                        cassette_50_count, 
                                                        cassette_100_count, 
                                                        cassette_200_count,
                                                        cassette_500_count,
                                                        cassette_2000_count 
                                                    FROM Modify_cassette_pre_config 
                                                    WHERE @timestamp_date 
													between from_date AND to_date
                                                    AND record_status = @activestatus
                                                   )cc
                                              ON clm.site_code = cc.site_code AND 
                                                 clm.atm_id = cc.atm_id
												 CROSS JOIN system_settings sa
                                                 LEFT JOIN Brand_Bill_Capacity bc ON 
                                                           bc.brand_code = mast.brand 
                                                           AND bc.record_status = @activestatus

                                                  WHERE mast.record_status = @activestatus
                                                  and mast.bank_code = @bankcode
                                                 -- AND mast.project_id = @projectid 
												  AND mast.atm_id = #temp_cash_balance_file_sbi.atm_id
												  AND sa.record_status = @activestatus
								)																					
							OR  total_remaining_cash_def_curr >
								(
									CASE 
                                        WHEN ( SELECT insurance_limit 
												from ATM_Config_limits atm 
												where atm.atm_id = #temp_cash_balance_file_sbi.atm_id 
												    and bank_code = @bankcode 
												--and project_id = @projectid
												    AND record_status = @activestatus 
												) 
												IS NOT NULL 
												OR 
												( SELECT insurance_limit 
												from ATM_Config_limits atm 
												where atm.atm_id = #temp_cash_balance_file_sbi.atm_id 
												and bank_code = @bankcode 
												--and project_id = @projectid
												AND record_status = @activestatus
												 ) <> 0
                                        THEN
                                        (
										SELECT insurance_limit 
										from ATM_Config_limits atm 
										where atm.atm_id = #temp_cash_balance_file_sbi.atm_id 
										and bank_code = @bankcode 
										--and project_id = @projectid
										AND record_status = @activestatus
										)
                                        END
								)	 
						THEN	
							CASE
								WHEN (error_code IS NULL)
								THEN	
									(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'exceeding_limits')
				 				ELSE 
									CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'exceeding_limits')) as varchar(max))
							END
						ELSE error_code
				   END		

				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Cassette Capacity, Insurance Limit, Bank Limit check Completed';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Cassette Capacity, Insurance Limit, Bank Limit check Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
 -----------Updating is_valid_record column as Yes or No-------------

			UPDATE #temp_cash_balance_file_sbi
			SET    is_valid_record = 
					CASE 
						WHEN error_code IS NULL
						THEN 'Yes'
						ELSE 'No'
					END		
			
			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Is Valid Record Field Updated';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Is Valid Record Field Updated', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
-----------------Checking count for valid records in updated temporary table -----------------------------

			SET @countCalculated = (
									SELECT count(1) 
									FROM #temp_cash_balance_file_sbi  
									WHERE is_valid_record = 'Yes' 
									)

-----------------Comparing both the counts ---------------------------------------------------------

			IF(@countCalculated != @CountTotal AND @countCalculated > 0)
				BEGIN
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Count Mismatch (Partial Valid File)';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
			
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'Exception' and sub_category = 'Partial Valid'
									 )				
				END
			ELSE IF (@countCalculated = @CountTotal)
				BEGIN 
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Count Matched (Valid File)';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				SET @outputVal = (
								SELECT sequence from  [dbo].[app_config_param]
								where  category = 'File Operation' and sub_category = 'Data Validation Successful'
							)												
				END

			ELSE
				BEGIN
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- No valid record found in table';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
									 )				
			END

------------------------Inserting distinct error code in new temporary table. Also splitting with , ------------------------

			IF OBJECT_ID('tempdb..#temp_distinct_codes') IS NOT NULL
			BEGIN
				DROP TABLE #temp_distinct_codes
			END
			ELSE
			BEGIN
				 DECLARE @Names VARCHAR(max) 
				;with distinct_error_codes
				as
				(
					select distinct error_code from #temp_cash_balance_file_sbi
					where error_code is not null
				)
				SELECT @Names = COALESCE(@Names + ',','') +  TRIM(error_code)
				FROM  distinct_error_codes

				SELECT DISTINCT VALUE INTO #temp_distinct_codes FROM string_split (@Names,',')
				
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Temporary Table Created for storing error code';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Temporary Table Created for storing error code', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

--------------------- Creating one single error code string with , seperated delimiter---------------------------------------				
				SET @errorcode = (
								SELECT 
									Stuff((
										SELECT N', ' + VALUE FROM #temp_distinct_codes FOR XML PATH(''),TYPE)
										.value('text()[1]','nvarchar(max)'),1,2,N''
										)
								)
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Error code String Created';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;			
			END			
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Started.... Updating columns in bank wise file and data update log', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
			BEGIN TRAN

			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Transaction Started.... Updating columns in bank wise file';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;	


------------------------- Updating columns in bank wise file using temporary table------------------------------------------

				UPDATE SBI 
				SET SBI.is_valid_record =  b.is_valid_record,
					SBI.error_code = b.error_code,
					record_status = @approvedstatus
				FROM #temp_cash_balance_file_sbi SBI
					INNER JOIN #temp_cash_balance_file_sbi b
					on b.atm_id = SBI.atm_id
					--AND SBI.project_id = @projectid 
					AND SBI.record_status = @ForRecordstatus
					AND SBI.created_reference_id = @referenceid


					-- SET IDENTITY_INSERT dbo.[cash_balance_file_sbi]  ON

			     INSERT INTO cash_balance_file_sbi
							  (  
							    [terminal_location]				,
							    [city]							,
							    [cash_dispensed_hopper_1]			,
							    [bills_hopr1]						,
							    [cash_dispensed_hopper_2]			,
							    [bills_hopr2]						,
							    [cash_dispensed_hopper_3]			,
							    [bills_hopr3]						,
							    [cash_dispensed_hopper_4]			,
							    [bills_hopr4]						,
							    [cash_dispensed_hopper_5]			,
							    [bills_hopr5]						,
							    [cash_dispensed_hopper_6]			,
							    [bills_hopr6]						,
							    [atm_id]							,
							    [total_remaining_cash_def_curr]	,
							    [remote_address]					,
							    [cash_increment_hopper_1]			,
							    [cash_increment_hopper_2]			,
							    [cash_increment_hopper_3]			,
							    [cash_increment_hopper_4]			,
							    [cash_increment_hopper_5]			,
							    [cash_increment_hopper_6]			,
							    [circle]							,
							    [sitetype]						,
							    [msvendor]						,
							    [lastwithdrawaltime]				,
							    [switch]							,
							    [district]						,
								 [module]							,		 
								 [currentstatus]					,		 
								 [branchmanagerphone]				,		 
								 [channelmanagercontact]			,		 
								 [jointcustodianphone]				,		 
								 [network]							,		 
								 [populationgroup]					,		 
								 [regionname]						,		 
								 [statename]						,		 
								 [jointcustodianname1]				,		 
								 [jointcustodianphone2]			,		 
								 [last_deposit_txn_time]			,		 
								 [project_id]						,		 
								 [record_status]					,		 
								 [created_on]						,		 
								 [created_by]						,		 
								 [created_reference_id]			,		 
								 [datafor_date_time]				,		 
								 [region]							,		 
								 [approved_on]						,		 
								 [approved_by]						,		 
								 [approved_reference_id]			,		 
								 [rejected_on]						,		 
								 [rejected_by]						,		 
								 [reject_reference_id]				,		 
								 [reject_trigger_reference_id]		,		 
								 [deleted_on]						,		 
								 [deleted_by]						,		 
								 [deleted_reference_id]			,		 
								 [modified_on]						,		 
								 [modified_by]						,		 
								 [modified_reference_id]			,		 
								 [is_valid_record]					,		 
								 [error_code]	 )

                         Select 
							  
							  CBS.[terminal_location]				,
							  CBS.[city]							,
							  CBS.[cash_dispensed_hopper_1]			,
							  CBS.[bills_hopr1]						,
							  CBS.[cash_dispensed_hopper_2]			,
							  CBS.[bills_hopr2]						,
							  CBS.[cash_dispensed_hopper_3]			,
							  CBS.[bills_hopr3]						,
							  CBS.[cash_dispensed_hopper_4]			,
							  CBS.[bills_hopr4]						,
							  CBS.[cash_dispensed_hopper_5]			,
							  CBS.[bills_hopr5]						,
							  CBS.[cash_dispensed_hopper_6]			,
							  CBS.[bills_hopr6]						,
							  CBS.[atm_id]							,
							  CBS.[total_remaining_cash_def_curr]	,
							  CBS.[remote_address]					,
							  CBS.[cash_increment_hopper_1]			,
							  CBS.[cash_increment_hopper_2]			,
							  CBS.[cash_increment_hopper_3]			,
							  CBS.[cash_increment_hopper_4]			,
							  CBS.[cash_increment_hopper_5]			,
							  CBS.[cash_increment_hopper_6]			,
							  CBS.[circle]							,
							  CBS.[sitetype]						,
							  CBS.[msvendor]						,
							  CBS.[lastwithdrawaltime]				,
							  CBS.[switch]							,
							  CBS.[district]						,
							  CBS.[module]							,
							  CBS.[currentstatus]					,
							  CBS.[branchmanagerphone]				,
							  CBS.[channelmanagercontact]			,
							  CBS.[jointcustodianphone]				,
							  CBS.[network]							,
							  CBS.[populationgroup]					,
							  CBS.[regionname]						,
							  CBS.[statename]						,
							  CBS.[jointcustodianname1]				,
							  CBS.[jointcustodianphone2]			,
							  CBS.[last_deposit_txn_time]			,
							  AM.[project_id]						,
							  CBS.[record_status]					,
							  CBS.[created_on]						,
							  CBS.[created_by]						,
							  CBS.[created_reference_id]			,
							  CBS.[datafor_date_time]				,
							  CBS.[region]							,
							  CBS.[approved_on]						,
							  CBS.[approved_by]						,
							  CBS.[approved_reference_id]			,
							  CBS.[rejected_on]						,
							  CBS.[rejected_by]						,
							  CBS.[reject_reference_id]				,
							  CBS.[reject_trigger_reference_id]		,
							  CBS.[deleted_on]						,
							  CBS.[deleted_by]						,
							  CBS.[deleted_reference_id]			,
							  CBS.[modified_on]						,
							  CBS.[modified_by]						,
							  CBS.[modified_reference_id]			,
							  CBS.[is_valid_record]					,
							  CBS.[error_code]		

							  from #temp_cash_balance_file_sbi CBS
							    left join atm_master AM
							  on CBS.atm_id=AM.atm_id 
							  AND AM.bank_code=@bankcode
							  AND AM.record_status='Active' 
							  AND AM.site_status='Active'

							  --where	record_status=  @approvedstatus AND
									--[datafor_date_time] = @datafor_date_time AND 
									--project_id = @projectid 
									--AND created_reference_id = @referenceid
									--AND msvendor in ('NCR (MOF)','EPS (TO)','EPS (MOF)')


									 
							 

						   --SET IDENTITY_INSERT dbo.[cash_balance_file_sbi]  OFF
			
			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Columns has been updated in bank wise file';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
		

			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Updating columns in data update log ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;

------------------------- Updating columns in Data Update Log table------------------------------------------
				
				
				UPDATE dbo.data_update_log
				SET is_valid_file =
					CASE	
							WHEN @outputVal = 50009 
							THEN  1
							WHEN @outputVal = 50001
							THEN 0
							WHEN @outputVal = 10001
							THEN 0
					END,
					validation_code = (SELECT @errorcode),
					record_status = 
					CASE	
							WHEN @outputVal = 50009 
							THEN @approvedstatus
							WHEN @outputVal = 50001
							THEN @approvedstatus
							WHEN @outputVal = 10001
							THEN @uploadedstatus
					END,	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				WHERE [datafor_date_time] =  @datafor_date_time
					AND record_status = @ForRecordstatus 
					AND bank_code = @bankcode 
					AND	data_for_type = @datafor
					AND project_id = @projectid
					AND created_reference_id = @referenceid
					

			COMMIT TRAN;

			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Transaction Completed....Column has been updated in data update log ';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Completed....Column has been updated in data update log and bank wise file', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Execution of [dbo].[uspDataValidationSBI] Completed.';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[uspDataValidationSBI] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	


		END
			ELSE
			BEGIN
				  RAISERROR (50002, 16,1);
			END
		END
	ELSE
		BEGIN
		--SELECT SESSION_ID from sys.dm_exec_sessions;  
			RAISERROR (	50004, 16,1);
		END
END

GO


