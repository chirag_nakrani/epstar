/****** Object:  StoredProcedure [dbo].[uspConsolidateVCB]    Script Date: 31-05-2019 16:56:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspConsolidateVCB] 
   (@datafor_date_time varchar(100),@bankcode varchar(50), @projectid varchar(50),@referenceid varchar(50),@systemUser varchar(50))
AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @errornumber INT;
DECLARE @count1 int;
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;
DECLARE @out varchar(200);
DECLARE @datafor nvarchar(10) = 'VCB'
DECLARE @activestatus nvarchar(20) = 'Active'
DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
DECLARE @approvedstatus nvarchar(20) = 'Approved'

	BEGIN TRY
		BEGIN TRAN
		SET NOCOUNT ON;
--		DECLARE @recordStatus varchar(50);
--DECLARE @successmsg varchar(max);
--DECLARE @errorsmsg varchar(max);
--DECLARE @count1 int;
--DECLARE @current_datetime_stmp datetime = GETDATE()
--DECLARE @rowcount int ;
--DECLARE @out varchar(200);
--DECLARE @referenceid varchar(50) = 'afasf'
--DECLARE @systemUser varchar(50) = 'SA'
--DECLARE @datafor_date_time varchar(100)='2018-10-08 23:00:00.000'
--DECLARE @errornumber INT;

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[uspConsolidateCBR_BOB] Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		IF EXISTS(
			Select 1 as ColumnName
			FROM dbo.data_update_log
			WHERE datafor_date_time =@datafor_date_time AND 
					data_for_type = @datafor AND 
					record_status = @approvedstatus
					AND created_reference_id = @referenceid
			) 
		BEGIN			--- Check for approved status in DUL

			IF EXISTS   (						
				SELECT 1
				FROM dbo.data_update_log
				WHERE datafor_date_time = @datafor_date_time AND 
					data_for_type = @datafor AND 
					record_status = @activestatus
				)	
				BEGIN				--- Check Active status in DUL

					
				--	DECLARE @data_for_type varchar(30) = 'VCB'

					--IF ('PSB'='SBI')
					--	BEGIN	--- For SBI bank (START)
							IF EXISTS
							(
								Select 1 as ColumnName
								FROM dbo.[vault_cash_balance]
								WHERE datafor_date_time =  @datafor_date_time AND  
										record_status = @activestatus
							) 
							BEGIN						-----Check active in bankwise file
								INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Active record found!! Changing status from Active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
							
										--DECLARE @SetWhere_VCB VARCHAR(MAX) = ' datafor_date_time = ''' + 
										--		cast(@datafor_date_time as varchar(50)) +  ''''
										--EXEC dbo.[uspSetStatus] 'dbo.vault_cash_balance',@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_VCB,@out OUTPUT
									
									UPDATE history
									SET history.record_status = @deletedstatus,
										history.deleted_on = @current_datetime_stmp,
										history.deleted_by = @systemUser,
										history.deleted_reference_id = @referenceid
									FROM dbo.[vault_cash_balance] history
									JOIN dbo.[vault_cash_balance] new
									on new.feeder_branch_name = history.feeder_branch_name
									AND new.bank_name = history.bank_name
									AND new.project_id = history.project_id
									AND new.Date = history.Date
									AND new.record_status = @approvedstatus
									AND history.record_status = @activestatus
									
									
										
																		
										DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) = ' datafor_date_time = ''' + 
													cast(@datafor_date_time as varchar(50)) + ''' and data_for_type = ''' + @datafor + ''''

										EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
										

										DECLARE @SetWhere_VCB1 VARCHAR(MAX) = ' datafor_date_time = ''' + 
													cast(@datafor_date_time as varchar(50)) + ''' and  created_reference_id = ''' + @referenceid + ''''
										
										EXEC dbo.[uspSetStatus] 'dbo.vault_cash_balance',@approvedstatus,@activestatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_VCB1,@out OUTPUT

										---------inserting data after status update for revision
									
	
											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records successfully marked as Deleted. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
								
									IF EXISTS (SELECT 1 from vault_cash_balance where created_reference_id = @referenceid and record_status = @activestatus)
									BEGIN
										SET @out = (SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful')
										SELECT @out 
									INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

									END
									ELSE
									BEGIN
										RAISERROR (50003,16,1)
									END
												
									DECLARE @SetWhere_DataUpdateLog3 VARCHAR(MAX) =' datafor_date_time = ''' + 
										cast(@datafor_date_time as varchar(50)) + ''' and  created_reference_id = ''' + @referenceid + ''' and data_for_type = ''' + @datafor + ''''

									EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@approvedstatus,@activestatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog3,@out OUTPUT

						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN

						UPDATE history
									SET history.record_status = @deletedstatus,
										history.deleted_on = @current_datetime_stmp,
										history.deleted_by = @systemUser,
										history.deleted_reference_id = @referenceid
									FROM dbo.[vault_cash_balance] history
									JOIN dbo.[vault_cash_balance] new
									on new.feeder_branch_name = history.feeder_branch_name
									AND new.bank_name = history.bank_name
									AND new.project_id = history.project_id
									AND new.Date = history.Date
									AND new.record_status = @approvedstatus
									AND history.record_status = @activestatus
																	
					DECLARE @SetWhere_CBR_bankWise2 VARCHAR(MAX) = ' datafor_date_time = ''' + 
														cast(@datafor_date_time as varchar(50)) + ''' and  created_reference_id = ''' + @referenceid + ''''
											
					EXEC dbo.[uspSetStatus] 'dbo.vault_cash_balance',@approvedstatus,@activestatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CBR_bankWise2,@out OUTPUT
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records have been marked as Active status. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

						IF EXISTS (SELECT 1 from dbo.vault_cash_balance where created_reference_id = @referenceid and record_status = @activestatus)
						BEGIN
							SET @out = (SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful')
							SELECT @out 
							INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

						END

						ELSE
						BEGIN
							RAISERROR (50003,16,1)
						END

							DECLARE @SetWhere_DataUpdateLog4 VARCHAR(MAX) =' datafor_date_time = ''' + 
										cast(@datafor_date_time as varchar(50)) + ''' and  created_reference_id = ''' + @referenceid + ''' and data_for_type = ''' + @datafor + ''''

									EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@approvedstatus,@activestatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog4,@out OUTPUT
						
				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
			BEGIN
				RAISERROR(50050,16,1)
			
			END
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of stored procedure Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						--DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SELECT @ErrorNumber;

				END
			 END CATCH
END
