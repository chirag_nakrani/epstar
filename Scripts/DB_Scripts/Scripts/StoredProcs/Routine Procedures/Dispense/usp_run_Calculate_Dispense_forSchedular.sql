USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[usp_run_Calculate_Dispense_forSchedular]    Script Date: 6/11/2019 11:45:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[usp_run_Calculate_Dispense_forSchedular]
 @dateT    date,
 @created_reference_id nvarchar(50),
 @cur_user nvarchar(50)

as 

set nocount on 
--exec [usp_run_Calculate_Dispense_forSchedular] '2019-06-03', 'Ref', 'sa'
--this is for schedular - for single day it will calculate minCBR for both day and send it to calculate dispense USP

declare @cmd varchar(max)
declare @MinDestinationDate_T datetime2
declare @MinDestinationDate_Tplus1 datetime2
declare @C3RFileExists varchar(1) = '0'
declare @error_msg varchar(max) = ''
declare @status varchar(30)
declare @bank_code   nvarchar(50)
declare @project_id nvarchar(30) = 'ALL'

declare @mailsubject nvarchar(100)
declare @mailbody nvarchar(max)
declare @email varchar(max) 

begin


	--looping for all  banks 
	DECLARE @banks TABLE (bankname nvarchar(20))
	INSERT INTO @banks VALUES  ('DENA') , ('CORP'), ('CAB'), ('BOB') , ('IOB'), ('IDBI'), ('UCO') , ('CBI')


	declare @myresultset Cursor
	Set @myresultset = CURSOR for  Select bankname from @banks 

	OPEN @myresultset 
	Fetch NEXT from @myresultset into @bank_code
	While @@Fetch_Status=0
	Begin
	

	set @error_msg = ''

	--cursor starts here ----------------
		begin try

			set @MinDestinationDate_T = (Select min(datafor_date_time)
											 from cash_balance_register 									 
											where bank_name=@bank_code 
											and project_id= case when @project_id =  'ALL' then project_id else @project_id end
											and datafor_date_time >= @dateT  and datafor_date_time < dateadd(dd, 1, @dateT)
											and record_status ='Active')
				

			set @MinDestinationDate_Tplus1 = (Select max(datafor_date_time)
											 from cash_balance_register 									 
											where bank_name=@bank_code
											and project_id= case when @project_id =  'ALL' then project_id else @project_id end
											and datafor_date_time >= @dateT  and datafor_date_time < dateadd(dd, 1, @dateT)
											--and datafor_date_time >= dateadd(dd, 1, @from_date)  and datafor_date_time < dateadd(dd, 2, @from_date)
											and record_status ='Active' )
			
			if exists (
						select top 1 * from c3r_CMIS where datafor_date_time = @dateT and record_status = 'Active' and 
						project_id = case when @project_id = 'ALL' then project_id else @project_id end
						 and bank=@bank_code)
			begin
					set @C3RFileExists = '1';
			end
			else
			begin
				set @error_msg = 'No C3R file present for the day : ' + CONVERT(VARCHAR(100), @dateT, 20);
			end

			 										
			if (@MinDestinationDate_T is null )
			begin
				if (@error_msg = '')
				begin set @error_msg = 'No CBR file present for the day:' + CONVERT(VARCHAR(100), @dateT, 20); end
				else begin set @error_msg = @error_msg + ', No CBR file present for the day:' + CONVERT(VARCHAR(100), @dateT, 20) end
				--set @MinDestinationDate_T = @dateT
			end

			if (@MinDestinationDate_Tplus1 is null)
			begin
				if (@error_msg = '')
				begin set @error_msg = 'No CBR file present for the day:' + CONVERT(VARCHAR(100), dateadd(day, 1, @dateT), 20) end
				else begin set @error_msg = @error_msg + ', No CBR file present for the day:' + CONVERT(VARCHAR(100), dateadd(day, 1, @dateT), 20) end
				--set @MinDestinationDate_Tplus1 = dateadd(day, 1, @dateT)
			end

			
			if (@error_msg = '')
			begin
				set @cmd = 'exec usp_calculate_dispense ' +  '''' + CONVERT(VARCHAR(100), @dateT, 20) +  ''', ''' + CONVERT(VARCHAR(100), @MinDestinationDate_Tplus1, 20) +  ''', ''' + CONVERT(VARCHAR(100), @MinDestinationDate_T, 20) +  ''', ''' + cast(@dateT as varchar) + ''', ''' + @created_reference_id + ''', ''' + @cur_user +''', ''' + @bank_code + ''', ''' + @project_id + ''''
				print @cmd
				exec @status =  usp_calculate_dispense @dateT, @MinDestinationDate_Tplus1, @MinDestinationDate_T , @dateT ,@created_reference_id, @cur_user, @bank_code, @project_id

				if (@status = '60057')
				begin

					set @mailsubject = 'System Alert : Automatic Dispense calculation successful for ' + @bank_code + ' ' + CONVERT(VARCHAR(100), @dateT, 20)
					set @mailbody = 'System Alert : Automatic dispense calculation successfull for : ' + @bank_code + ' for : ' + CONVERT(VARCHAR(100), @dateT, 20)
							
						
				end 
				else
				begin
					set @mailsubject = 'System Alert : Automatic Dispense calculation failed for' +@bank_code + ' ' + CONVERT(VARCHAR(100), @dateT, 20)
					declare @errmsg varchar(100) 
					select @errmsg = value from app_config_param where sequence = @status
					set @mailbody = 'System Alert : Automatic dispense calculation failed for : ' + @bank_code + ' ' + CONVERT(VARCHAR(100), @dateT, 20) + ' with error message : ' + @errmsg
						
				end

			end

			else
			begin

				insert into execution_log (description, execution_date_time, process_spid, process_reference_id, execution_program, executor_method, executed_by)
				values ( 'Dispense calculation failed:'+@cmd+' with error:'+@error_msg , getdate(), @@SPID, NULL, 'Stored Procedure', OBJECT_NAME(@@PROCID), @cur_user)

				set @mailsubject = 'System Alert : Automatic Dispense calculation failed for ' +@bank_code + ' ' + CONVERT(VARCHAR(100), @dateT, 20)
				set @mailbody = 'System Alert : Automatic dispense calculation failed for : ' + @bank_code + ' ' + CONVERT(VARCHAR(100), @dateT, 20) + ' with error message : ' + @error_msg
			end	
			

		
			insert into execution_log (description, execution_date_time, process_spid, process_reference_id, execution_program, executor_method, executed_by)
			values ( 'Execution Succeeded for:'+@cmd+' with error:'+@error_msg , getdate(), @@SPID, NULL, 'Stored Procedure', OBJECT_NAME(@@PROCID), @cur_user)

			

		end try
		begin catch
			
			set @error_msg = ERROR_MESSAGE()
			insert into execution_log (description, execution_date_time, process_spid, process_reference_id, execution_program, executor_method, executed_by)
			values ( 'Execution failed for:'+@cmd+' with error:'+@error_msg , getdate(), @@SPID, NULL, 'Stored Procedure', OBJECT_NAME(@@PROCID), @cur_user)
			
			set @mailbody = 'Automatic dispense calculation failed for : ' + @bank_code + ' for : ' + CONVERT(VARCHAR(100), @dateT, 20)	+ ' with error message:' + @error_msg			 
		end catch




		--------sending mail after completed for single bank -------

		SELECT @email = COALESCE(@email + ';', '') + email from
		(
		select distinct email 
		from user_bank_mapping ubm
		inner join auth_user au
		on ubm.user_name = au.username
		and ubm.bank_id = @bank_code
		--and ubm.record_status = 'Active'
		and au.is_active = 1
		and isnull(ltrim(rtrim(email)), '') <> ''
		) s


		if (@email = '')
		begin
			set @email = ''
		end
		else 
		begin
			set @email = @email+';eps@exponentiadata.com'
		end

		set @email = 'sagar.auti@exponentiadata.com'


		set @mailbody = 'Hi All, <br>' + @mailbody + '<br><br>Thanks & Regards<br>EPS Cash Management Team<br>Landline No : 022 - 42379624 / 26 / 27 / 28'
		EXEC msdb.dbo.sp_send_dbmail @profile_name='SQL ALERT',
							@recipients=@email,
							@subject=@mailsubject,
							@body=@mailbody,
							@body_format = 'HTML';
							
	
		



	
		-------------cursor ends here --------------			
	Fetch Next from @myresultset into @bank_code
	End
	close @myresultset


end

