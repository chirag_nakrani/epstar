/****** Object:  StoredProcedure [dbo].[uspConsolidateCDRFiles]    Script Date: 27-11-2018 15:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[uspConsolidateCDRFiles] 
@datafor_date_time varchar(50) ,@bankcode nvarchar(10), @projectid varchar(50),@referenceid varchar(50),@systemUser varchar(50)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
			DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @Result varchar(100);
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())


			--Set @timestamp_date
			--Execute the query according to the Bank code
			SET @Cmd = CASE 
				 WHEN @bankcode = 'BOMH' 
							THEN 'EXEC uspConsolidateCDR_BOMH ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
				WHEN @bankcode = 'CAB' 
							THEN 'EXEC uspConsolidateCDR_CAB ''' +  cast(@datafor_date_time as varchar(50))+ '''' +  ','+ ''''+@projectid+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
				WHEN @bankcode = 'RSBL' 
							THEN 'EXEC uspConsolidateCDR_RSBL ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
				WHEN @bankcode = 'SBI' 
							THEN 'EXEC uspConsolidateCDR_SBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
			 	WHEN @bankcode = 'UBI' 
							THEN 'EXEC uspConsolidateCDR_UBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
						END
			  		--PRINT @cmd
			EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 
			--SELECT @Result
		---	If validation is successfull then proceed with the status update change
		--- Calling Procedure to Update the status 

			SET @successVal =(
							SELECT Sequence from  [dbo].[app_config_param] where 
							category = 'File Operation' AND sub_category = 'Consolidation Successful'
							)
                            
            SET @failedVal =(
							SELECT Sequence from  [dbo].[app_config_param] where 
							category = 'File Operation' AND sub_category = 'Consolidation Failed'
							)

            
            IF(@Result = @successVal)
				BEGIN	
					DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log'
					DECLARE @ForStatus VARCHAR(15) = 'Approved'
					DECLARE @ToStatus VARCHAR(15) = 'Active'
					DECLARE @SetWhereClause VARCHAR(MAX) = ' bank_code = ''' + 
													@bankcode + ''' and datafor_date_time = ''' + 
													cast(@datafor_date_time as varchar(50)) +  ''' and data_for_type = ''Dispense' + ''' and project_id = ''' + @projectid + ''' and  created_reference_id = ''' + @referenceid + ''''

					EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
											
					SELECT @Result;
					
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
 		
		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,case when @ErrorNumber = 50000
								then 'Consolidation failed'
								else @ErrorMessage
								end,@dateAdded
							)
						SELECT @ErrorNumber;

				END
			 END CATCH
END




--exec uspValidateCbrFiles '2018-10-03 15:41:21.000', 'BOMH', 'Uploaded', 'C1234567891', 'Sarthak'

--select * from data_update_log

--select * from cash_balance_file_bomh where created_on =  '2018-10-03 15:41:21.000'

--select * from cash_balance_file_bomh


--select * from app_config_param where sequence = 50001

--EXEC uspDataValidationBOMH 'Oct  3 2018  3:41PM','Uploaded','C1234567891', @Result OUTPUT ;
GO
