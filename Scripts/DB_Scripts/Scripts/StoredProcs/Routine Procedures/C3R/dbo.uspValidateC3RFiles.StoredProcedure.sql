/****** Object:  StoredProcedure [dbo].[uspValidateC3RFiles]    Script Date: 27-11-2018 15:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
 
 --   exec uspValidateC3RFiles '2018-10-19 12:00:00', 'All', '', 'Uploaded', 'C1234567891', 'Mostaque'
 
 CREATE PROCEDURE [dbo].[uspValidateC3RFiles]  

@data_for_datetime varchar
(100) ,@bankcode varchar(20),@projectid varchar (20),
 @recordstatus varchar(50),@referenceid varchar(50),
 @systemUser varchar(50)

AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
			DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @Result varchar(100);
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
			DECLARE @out1 varchar(100);
			
			--DECLARE @data_for_datetime varchar(100) ='2018-10-19 12:00:00'
			--DECLARE @recordstatus varchar(50) ='Uploaded'
			--DECLARE @referenceid varchar(50) = '11212121'
			--DECLARE @outputVal VARCHAR(50)
			--DECLARE @bankcode varchar(10) = 'BOMH'
			--DECLARE @systemUser varchar(50) = 'SA'
			--DECLARE @Cmd NVARCHAR(MAX);
			--DECLARE @Result varchar(100);
			--DECLARE @out varchar(100);
			--Set @timestamp_date
			--Execute the query according to the Bank code

INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			SET @Cmd =   'EXEC [usp_validate_C3R_CMIS]''' +  
										cast(@data_for_datetime as varchar(50))+ '''' + ','+
										  ''''+@recordstatus+'''' + ','+
										  ''''+@referenceid+''''+ ','+
										  ''''+@systemUser+''''+', 
										  @Result OUTPUT ;'
					--print @cmd	
					
					EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 	 
					--Select @Result
					IF(@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence from app_config_param where category = 'C3R_validation' and sub_category = 'Error')
			END
	      
			SET @successVal =
			            
						  (
							SELECT sequence from  [dbo].[app_config_param]
							where category = 'File Operation' and sub_category = 'Data Validation Successful'
							)
                            
            SET @failedVal =
			           
					      (
							SELECT sequence from  [dbo].[app_config_param]
							where  category = 'File Operation' and sub_category = 'Data Validation Failed'
							)

            
            IF(@Result = @successVal OR @Result = 50001)
				BEGIN	
					--DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log'
					--DECLARE @ForStatus VARCHAR(30)     = 'Uploaded'
					--DECLARE @ToStatus VARCHAR(30)      = 'Approval Pending'
					--DECLARE @SetWhereClause VARCHAR(MAX) = '  datafor_date_time = ''' + 
					--				cast(@data_for_datetime as varchar(50)) +  ''' and  data_for_type = ''C3R'''

		   --EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,
					--								@ToStatus,@timestamp_date,@systemUser,
					--								@referenceid,@SetWhereClause,@out OUTPUT
											
	    		SELECT @Result;
					
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
 	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	
		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						
							INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
							SELECT @ErrorNumber;
				END
			END CATCH
END
GO
