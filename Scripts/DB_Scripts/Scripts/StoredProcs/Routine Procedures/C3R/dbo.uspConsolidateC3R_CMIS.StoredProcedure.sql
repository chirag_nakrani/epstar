USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspConsolidateC3R_CMIS]    Script Date: 17-05-2019 18:31:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
  --  declare @out varchar(100)
  --exec [uspConsolidateC3R_CMIS]  '2018-10-22 10:00:00','','C1234567891','Mostaque',@out
 
CREATE PROCEDURE [dbo].[uspConsolidateC3R_CMIS] 
(
	@data_for_datetime varchar(100)
	,@referenceid varchar(50)
	,@cra_name varchar(50)
	,@systemUser varchar(50)
	,@out varchar(200) OUTPUT
)

AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;


INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		IF EXISTS(
			Select 1 as ColumnName
			FROM [dbo].[data_update_log]                          
			WHERE datafor_date_time = @data_for_datetime AND 			 
					data_for_type = 'C3R' AND 
					record_status = 'Approved' 
					AND 
					created_reference_id = @referenceid 
			) 
		BEGIN			--- Check for approved status in DUL

			
			DECLARE @ForStatus VARCHAR(15) = 'Active'
			DECLARE @ToStatus VARCHAR(15) = 'Deleted'
			DECLARE @data_for_type varchar(30) = 'C3R'
			
			
			--marking records deleted if exists in CMIS and VMIS
			update old
			set record_status = 'Deleted', 
			deleted_on=cast(getdate() as varchar(30)), 
			deleted_by = @systemUser,
			deleted_reference_id = @referenceid
				
			from C3R_CMIS old
			inner join C3R_CMIS new
			on old.atm_id = new.atm_id
			and old.bank = new.bank
			and old.feeder_branch = new.feeder_branch
			and old.cra_name = new.cra_name
			and old.datafor_date_time = new.datafor_date_time
			where 
			old.record_status = 'Active'
			and new.created_reference_id = @referenceid
			and new.datafor_date_time = @data_for_datetime
									
			set @count1 = @@rowcount
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
			values (cast(@count1 as varchar) + ' Records Deleted from CMIS',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			
				
			update old
			set record_status = 'Deleted', 
			deleted_on=cast(getdate() as varchar(30)), 
			deleted_by = @systemUser,
			deleted_reference_id = @referenceid
				
			from C3R_VMIS old
			inner join C3R_VMIS new
			on old.bank = new.bank
			and old.feeder_branch_name = new.feeder_branch_name
			and old.cra_name = new.cra_name
			and old.datafor_date_time = new.datafor_date_time
			where 
			old.record_status = 'Active'
			and new.created_reference_id = @referenceid
			and new.datafor_date_time = @data_for_datetime
			
			set @count1 = @@rowcount
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
			values (cast(@count1 as varchar) + ' Records Deleted from VMIS',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			
			
			
			
			--Marking records deleted in DUL if exists for particular datetime and cra
			if exists (
						select 1 from data_update_log
						where datafor_date_time = @data_for_datetime
						and cra_name = case 
									when @cra_name = 'All' 
									then cra_name
									else @cra_name
									end
					)
			begin
			
				DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) = ' datafor_date_time = ''' + 
						@data_for_datetime  + ''' and data_for_type = ''' + @data_for_type 
						+ ''' and  cra_name = ''' + @cra_name 
						+ ''''
																									  

				EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,    -- set status  deleted for data update log table
					@current_datetime_stmp,@systemUser,
					@referenceid,@SetWhere_DataUpdateLog,
					@out OUTPUT
				
			end
			
			
			--Marking status from Approved to Active in CMIS and VMIS
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
			values ('Marking records Active in CMIS and VMIS',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)	
			
			update c3r_CMIS
			set record_status = 'Active'
			where record_status = 'Approved'
			and created_reference_id = @referenceid
			and datafor_date_time = @data_for_datetime
			
			set @count1 = @@rowcount
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
			values (cast(@count1 as varchar) + ' Records Activated in CMIS',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			update c3r_VMIS
			set record_status = 'Active'
			where record_status = 'Approved'
			and created_reference_id = @referenceid
			and datafor_date_time = @data_for_datetime
			
			set @count1 = @@rowcount
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
			values (cast(@count1 as varchar) + ' Records Activated in VMIS',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			
		
			
			--Verifying if recoreds are marked active
			IF EXISTS (
							SELECT 1 from dbo.c3r_CMIS where 
							datafor_date_time = @data_for_datetime and
							created_reference_id = @referenceid and
							record_status = 'Active' 

						)
			AND
				EXISTS
					  (
							SELECT 1 from dbo.c3r_VMIS where 
							datafor_date_time = @data_for_datetime and
							record_status = 'Active' and
							created_reference_id = @referenceid
						
					   )
			BEGIN
															
				SET @out = (
								SELECT Sequence from  [dbo].[app_config_param] where 
								category = 'File Operation' AND sub_category = 'Consolidation Successful'  --chg
							) 
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			END
			ELSE
			BEGIN
				RAISERROR (50003,16,1)
			END
			
			
	
		end    ----Approved
		
		ELSE  ------no approved records 
		BEGIN
			RAISERROR(50010,16,1)
		END
			
		
end	
		
		
