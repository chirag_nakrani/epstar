USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspConsolidateC3RFiles]    Script Date: 4/25/2019 5:33:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

 
 --   EXEC uspConsolidateC3RFiles '2018-10-22 10:00:00','','MOF','C1234567891','Mostaque'

CREATE PROCEDURE [dbo].[uspConsolidateC3RFiles] 
@data_for_datetime varchar(50) ,@cra_name varchar(20), @projectid varchar(10),@referenceid varchar(50),@systemUser varchar(50)
AS

BEGIN
	BEGIN TRY
		BEGIN TRAN
			DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @Result varchar(100);
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
 


 --Declare @data_for_datetime varchar(50) ,@bankcode varchar(20), @projectid varchar(10),@referenceid varchar(50),@systemUser varchar(50)

 --SET @data_for_datetime= '2018-10-22 10:00:00'
 --SET @bankcode=''
 --SET @projectid='MOF'
 --Set @referenceid='C1234567891'
 --SET @systemUser='Mostaque'
 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			--Execute the query according to the Bank code
			SET @Cmd =  'uspConsolidateC3R_CMIS ''' +  
									cast(@data_for_datetime as varchar(50))+
									
									'''' + ','+ ''''+@referenceid+''''+
									','+ ''''+@cra_name+''''+
									 ','+ ''''+@systemUser+''''+ 
									 ', @Result OUTPUT ;'
						    
			--print @Cmd   
			EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 

			--print @Cmd
 

			SET @successVal =
			              (
							SELECT Sequence from  [dbo].[app_config_param] where 
							category = 'File Operation' AND sub_category = 'Consolidation Successful'
							)
                            
            SET @failedVal =(
							SELECT Sequence from  [dbo].[app_config_param] where 
							category = 'File Operation' AND sub_category = 'Consolidation Failed'
							)

  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Updating record status in data update log table',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
          
            IF(@Result = @successVal)
				BEGIN	
					DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log'
					DECLARE @ForStatus VARCHAR(15) = 'Approved'
					DECLARE @ToStatus VARCHAR(15) = 'Active'
					DECLARE @SetWhereClause VARCHAR(MAX) = ' datafor_date_time = ''' + 
													cast(@data_for_datetime as varchar(50)) + 
													''' and  created_reference_id = ''' + @referenceid 
													+ ''' and  record_status = ''Approved'' and data_for_type = ''C3R'''

					EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,
															@timestamp_date,@systemUser,@referenceid,
															@SetWhereClause,@out OUTPUT
											
					SELECT @Result;
					
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of stored procdure completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
    
		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SELECT @ErrorNumber;
				END
			 END CATCH
END




