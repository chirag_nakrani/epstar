 
/****** Object:  StoredProcedure [dbo].[usp_calculated_avg_dispense]    Script Date: 14-01-2019 19:46:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 /*
 --- =========================================================================
 --- Created By  :Rubina Q
 --- Created Date: 12-12-2018
 --- Description : calcualte average of the dispense amount based on the dates from each month over a period of three months
 --                The dispense is calculated by calling BELOW	functions 
 --             [fn_max 28days_avg]             --  Calculates maximum of average over 28 days (7 days from each month around indentdate)
 --				[fn_Calculate_max_of_thirdmax] --   Calculates third largest dispense amount
 --				[fn_Calculate_max_of_secondmax] --  Calculates second largest dispense amount
 --  			[fn_Calculate_max_of_max]       --  Calculates maximum of all maximum dispense amounts
--				[fn_Calculate_max_of_avg]       --  Calculates maximum of all average dispense amounts
--				[fn_Calculate_avg_of_thirdmax]  --  Calculates average of third largest dispense amounts
--				[fn_Calculate_avg_of_secondmax] --  Calculates average of second largest dispense amounts
--				[fn_Calculate_avg_of_max]       --  Calculates average of  maximum dispense amounts
--				[fn_Calculate_avg_of_avg]       --  Calculates average of average dispense amounts
--				[fn_28datapoints]               --  For each day of the three minus and three plus days around the indentdate
--                                                  from each month from past three months
--    For e.g If indentdate =22-dec-2018
--         daterange1 = 3 days before and after of onemonth ago for same indentdate i.e 19-nov-2018 to 25-nov-2018
--         daterange2 = 3 days before and after of two months ago for same indentdate i.e 19-oct-2018 to 25-oct-2018
--         daterange3 = 3 days before and after of three months ago for same indentdate i.e 19-sept-2018 to 25-sept-2018
--         daterange4 =  seven days from (indentdate-2) i.e T-2 date i.e  14-dec-2018 to 20-dec-2018
Transaction count after EXECUTE indicates a mismatching number of BEGIN and COMMIT statements. Previous count = 1, current count = 0.
 --- =========================================================================
 */
 -- EXEC  [usp_calculated_avg_dispense] '2018-12-26','BOMH','MOF','rubina','abc123'

 --select * from data_update_log order by id desc

 --select * from calculated_dispense order by id desc
 --select * from error_log order by id desc
  --select * from execution_log order by id desc
 --Conversion failed when converting the varchar value 'Average dispense calculated for  DENA and for Project MOF for date 2018-10-12 and no. of rows inserted : ' to data type int.
 --Conversion failed when converting the varchar value 'inserting completed in table [cash_dispense_month_minus_3] for DENA and date 2018-10-12 and rows ' to data type int.
 ---- Select * from [calculated_dispense]

 CREATE procedure [dbo].[usp_calculated_avg_dispense]
 
 (@date_T date ,
 @bank varchar(50),
 @project varchar (10),
  @cur_user varchar(50),
 @created_reference_id varchar(50)
 )
 AS

 BEGIN
  
	DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
	DECLARE @out varchar(50) 
	DECLARE @outputVal varchar(100)
	DECLARE @operation_type  varchar(40)
	SET @operation_type='Average Dispense'
	declare @Execution_log_str nvarchar(max)
	DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
	set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@date_T as nvarchar(50)) 
	Declare @default_dispense_amount int = (Select default_average_dispense from system_settings where record_status='Active')

	BEGIN TRY
	
			 ---**************************testing*******************************
			 --declare AM.atm_id nvarchar(50),@date_T date ,@bank varchar(50),@project varchar (10)
			 --set AM.atm_id='DA0068C1'
			 --SET @date_T='2018-12-06'
			 --Set @project='MOF'
			 --Set @bank='BOMH'
			  ---**************************testing*******************************


			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			values ('Execution of [dbo].[usp_calculated_dispense] Started for bank and calling procedure [usp_pre_dispense_calc] ' +@bank+' and for date ' +cast(@date_T as varchar(20)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

			

			BEGIN TRANSACTION
 
			
			set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Calculating  4 data sets.' 
		    
			/**************************This procedure prepares four datasets for required dates in four tables**************/
			EXEC [dbo].[usp_pre_dispense_calc]  @date_T,@bank,@project,@cur_user,@created_reference_id

			set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Calculated  4 data sets.' 
			DECLARE @ForStatus VARCHAR(15) = 'Active'
			DECLARE @ToStatus VARCHAR(15) = 'Deleted'

			IF EXISTS(
						SELECT 1 
						FROM data_update_log
						WHERE record_status = 'Active'
						AND data_for_type = @operation_type
						AND datafor_date_time = @date_T
					)

					BEGIN
					
					       set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old data update log entries for ' + @operation_type

							DECLARE @SetWhere_data_update VARCHAR(MAX) = ' data_for_type = ''' + @operation_type
																		+ ''' and datafor_date_time =  ''' + cast(@date_T as nvarchar(50))
																		+ ''' and bank_code = ''' + @bank 
																		+ ''' and project_id = ''' + @project + ''''

							EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_data_update,@out OUTPUT
					
					set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for ' + @operation_type

					
					END	

			IF EXISTS(
						SELECT 1
						FROM [calculated_dispense]
						WHERE record_status = 'Active'
						AND datafor_date_time = @date_T
						AND project_id = @project
						AND bank = @bank
					)

					BEGIN
 
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in calculated_dispense ' 
					
							DECLARE @SetWhere_calculate_dispense VARCHAR(MAX) = ' datafor_date_time = ''' + cast(@date_T as nvarchar(50)) 
																				+ ''' and bank = ''' + @bank 
																				+ ''' and project_id = ''' + @project + ''''

							EXEC dbo.[uspSetStatus] 'dbo.[calculated_dispense]',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_calculate_dispense,@out OUTPUT
					
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in calculated_dispense table ' 
				
					END	

			/****************************Below code calculates the dispense by calling functions respectively*******/
			--truncate table [calculated_dispense]
			set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Start loading data into [calculated_dispense].' 

			--**********************Testing praameters*******************************

			--    Select * from [calculated_dispense] where atm_id='NA1400C1' order by created_on desc
			--	  declare @date_T date='2018-12-21'
			--	, @project varchar(30)='MOF'
			--	, @bank varchar(30) ='BOMH'
			--  , @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120)
			-- ,  @cur_user varchar(40)='abc'
			-- ,  @created_reference_id varchar(10)='123rubina'

			--**********************Testing praameters*******************************
			  

                  Select * into #process_calculation	
				  from
				     (  Select  AM.atm_id         as atm_id,
						AM.bank_code              as bank,
						AM.project_id             as Project_id,
						cast(@date_T as datetime) as indentdate,
						[dbo].[fn_avg_pre_datapoints](AM.atm_id,1,@date_T,@bank,@project) as [avg_of_5days_Tminus5],
						[dbo].[fn_avg_pre_datapoints](AM.atm_id,2,@date_T,@bank,@project) as [avg_of_2days_Tminus2],
						[dbo].[fn_avg_pre_datapoints](AM.atm_id,3,@date_T,@bank,@project) as [avg_month_minus_3],
						[dbo].[fn_avg_pre_datapoints](AM.atm_id,4,@date_T,@bank,@project) as [avg_month_minus_2],
						[dbo].[fn_avg_pre_datapoints](AM.atm_id,5,@date_T,@bank,@project) as [avg_month_minus_1],
						[dbo].[fn_avg_pre_datapoints](AM.atm_id,6,@date_T,@bank,@project) as [avg_month_minus_0],
						--[dbo].[fn_avg_pre_datapoints](AM.atm_id,7,@date_T,@bank,@project) as [max_of_four_months_avg],
						@default_dispense_amount										  as default_dispense_amount,						 
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,1,@project) as  [T_M3_minus_3],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,2,@project) as  [T_M3_minus_2],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,3,@project) as  [T_M3_minus_1],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,4,@project) as  [T_M3_minus_0],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,5,@project) as  [T_M3_plus_1],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,6,@project) as  [T_M3_plus_2],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,7,@project) as  [T_M3_plus_3],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,8,@project) as  [T_M2_minus_3],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,9,@project) as  [T_M2_minus_2],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,10,@project) as [T_M2_minus_1],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,11,@project) as [T_M2_minus_0],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,12,@project) as [T_M2_plus_1],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,13,@project) as [T_M2_plus_2],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,14,@project) as [T_M2_plus_3],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,15,@project) as [T_M1_minus_3],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,16,@project) as [T_M1_minus_2],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,17,@project) as [T_M1_minus_1],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,18,@project) as [T_M1_minus_0],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,19,@project) as [T_M1_plus_1],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,20,@project) as [T_M1_plus_2],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,21,@project) as [T_M1_plus_3],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,22,@project) as [T_minus_7],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,23,@project) as [T_minus_6],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,24,@project) as [T_minus_5],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,25,@project) as [T_minus_4],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,26,@project) as [T_minus_3],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,27,@project) as [T_minus_2],
						[dbo].[fn_28datapoints](AM.atm_id,@date_T,@bank,28,@project) as [T_minus_1],
						[dbo].[fn_Calculate_max_of_max] (AM.atm_id,@date_T,@bank,@project) as      max_of_max,
						[dbo].[fn_Calculate_avg_of_max] (AM.atm_id,@date_T,@bank,@project) as      avg_of_max ,
						[dbo].[fn_Calculate_max_of_avg] (AM.atm_id,@date_T,@bank,@project) as      max_of_avg,
						[dbo].[fn_Calculate_avg_of_avg] (AM.atm_id,@date_T,@bank,@project) as      avg_of_avg,
						[dbo].[fn_Calculate_max_of_secondmax](AM.atm_id,@date_T,@bank,@project) as max_of_secondmax,
						[dbo].[fn_Calculate_avg_of_secondmax](AM.atm_id,@date_T,@bank,@project) as avg_of_secondmax,
						[dbo].[fn_Calculate_max_of_thirdmax](AM.atm_id,@date_T,@bank,@project) as  max_of_thirdmax,
						[dbo].[fn_Calculate_avg_of_thirdmax](AM.atm_id,@date_T,@bank,@project) as  avg_of_thirdmax,
						[dbo].[fn_max 28days_avg](AM.atm_id,@date_T,@bank,@project) as               _28days_or_T2dispense
						, 'Active' as                 record_status 
						, @timestamp_date as          timestamp_date
						, @cur_user as              cur_user
						, @created_reference_id as reference_id


						 from atm_master AM						
						--from cash_dispense_register CD                 --  as per old logic
						where 
						--atm_id=AM.atm_id and datafor_date_time=@date_T -- as per old logic
						 project_id=@project
						and bank_code=@bank
						and record_status='Active'
						and site_status = 'Active'
						)s


						Select atm_id,
										  bank,
										  Project_id,
										 indentdate,
										 [avg_of_5days_Tminus5],
										 [avg_of_2days_Tminus2],
										 [avg_month_minus_3],
										 [avg_month_minus_2],
										 [avg_month_minus_1],
										 [avg_month_minus_0],
										 (select MAX(v)
												  FROM (VALUES ([avg_month_minus_3]),
												 ([avg_month_minus_2]), 
												 ([avg_month_minus_1]),
												 (avg_month_minus_0)) AS i(v) ) as max_of_four_months_avg,										 
										 default_dispense_amount,
										  (select AVG(v)
												  FROM (VALUES (case when [avg_month_minus_3] > 0 then [avg_month_minus_3] end),
												 (case when [avg_month_minus_2] > 0 then [avg_month_minus_2] end), 
												 (case when [avg_month_minus_1]>0 then [avg_month_minus_1] end),
												 (case when avg_month_minus_0 > 0 then avg_month_minus_0 end)) AS i(v) ) as avg_of_four_months_avg,
										[T_M3_minus_3],
										[T_M3_minus_2],
										[T_M3_minus_1],
										[T_M3_minus_0],
										[T_M3_plus_1],
										[T_M3_plus_2],
										[T_M3_plus_3],
										[T_M2_minus_3],
										[T_M2_minus_2],
										[T_M2_minus_1],
										[T_M2_minus_0],
										[T_M2_plus_1],
										[T_M2_plus_2],
										[T_M2_plus_3],
										[T_M1_minus_3],
										[T_M1_minus_2],
										[T_M1_minus_1],
										[T_M1_minus_0],
										[T_M1_plus_1],
										[T_M1_plus_2],
										[T_M1_plus_3],										 
										[T_minus_7],
										[T_minus_6],
										[T_minus_5],
										[T_minus_4],
										[T_minus_3],
										[T_minus_2],
										[T_minus_1],
										 max_of_max,
										 avg_of_max ,
										 max_of_avg,
										 avg_of_avg,
										 max_of_secondmax,
										 avg_of_secondmax,
										 max_of_thirdmax,
										 avg_of_thirdmax,
										 _28days_or_T2dispense,	
										  record_status ,
										  timestamp_date,
										  cur_user,
										  reference_id				 
						INTO #calculated_dispense
										from #process_calculation
								  insert into calculated_dispense
								  (										
										atm_id,
										bank,
										project_id,
										datafor_date_time,
										avg_of_5days_Tminus5,
										avg_of_2days_Tminus2,
										avg_month_minus_3,
										avg_month_minus_2,
										avg_month_minus_1,
										avg_month_minus_0,
										max_of_four_months_avg,
										avg_of_four_months_avg,
										default_dispense_amount,
										max_of_current,
										T_M3_minus_3,
										T_M3_minus_2,
										T_M3_minus_1,
										T_M3_minus_0,
										T_M3_plus_1,
										T_M3_plus_2,
										T_M3_plus_3,
										T_M2_minus_3,
										T_M2_minus_2,
										T_M2_minus_1,
										T_M2_minus_0,
										T_M2_plus_1,
										T_M2_plus_2,
										T_M2_plus_3,
										T_M1_minus_3,
										T_M1_minus_2,
										T_M1_minus_1,
										T_M1_minus_0,
										T_M1_plus_1,
										T_M1_plus_2,
										T_M1_plus_3,
										T_minus_7,
										T_minus_6,
										T_minus_5,
										T_minus_4,
										T_minus_3,
										T_minus_2,
										T_minus_1,
										max_of_max,
										avg_of_max,
										max_of_avg,
										avg_of_avg,
										max_of_secondmax,
										avg_of_secondmax,
										max_of_thirdmax,
										avg_of_thirdmax,
										_28days_or_T2dispense,
										record_status,
										created_on,
										created_by,
										created_reference_id	
								  )

								  Select atm_id,
										  bank,
										  Project_id,
										 indentdate,
										 [avg_of_5days_Tminus5],
										 [avg_of_2days_Tminus2],
										 [avg_month_minus_3],
										 [avg_month_minus_2],
										 [avg_month_minus_1],
										 [avg_month_minus_0],
										 [max_of_four_months_avg],
										 avg_of_four_months_avg,
										 default_dispense_amount,
										 CEILING((SELECT MAX(v)
												  FROM (VALUES ([avg_of_5days_Tminus5]),
												 ([avg_of_2days_Tminus2]), 
												 (avg_of_four_months_avg),
                                                 (default_dispense_amount)
                                                 ) AS i(v) ) *1.0 / 10000)*10000  as max_of_current,
										[T_M3_minus_3],
										[T_M3_minus_2],
										[T_M3_minus_1],
										[T_M3_minus_0],
										[T_M3_plus_1],
										[T_M3_plus_2],
										[T_M3_plus_3],
										[T_M2_minus_3],
										[T_M2_minus_2],
										[T_M2_minus_1],
										[T_M2_minus_0],
										[T_M2_plus_1],
										[T_M2_plus_2],
										[T_M2_plus_3],
										[T_M1_minus_3],
										[T_M1_minus_2],
										[T_M1_minus_1],
										[T_M1_minus_0],
										[T_M1_plus_1],
										[T_M1_plus_2],
										[T_M1_plus_3],										 
										[T_minus_7],
										[T_minus_6],
										[T_minus_5],
										[T_minus_4],
										[T_minus_3],
										[T_minus_2],
										[T_minus_1],
										 max_of_max,
										 avg_of_max ,
										 max_of_avg,
										 avg_of_avg,
										 max_of_secondmax,
										 avg_of_secondmax,
										 max_of_thirdmax,
										 avg_of_thirdmax,
										 _28days_or_T2dispense,	
										  record_status ,
										  timestamp_date,
										  cur_user,
										  reference_id
										from #calculated_dispense					 

                        DECLARE @row INT			 
			            DECLARE @ErrorCode INT		 
                        SELECT @row=@@ROWCOUNT,@ErrorCode = @@error

                        INSERT into data_update_log 
									(bank_code,
									project_id,
									operation_type,
									datafor_date_time,
									data_for_type,			 
									status_info_json,			 
									record_status,
									date_created,
									created_by,
									created_reference_id
									)
                        values
									(@bank,
									@project,
									'Average Dispense',
									@date_T,
									'Average Dispense',			 
									'Average dispense calculated for  '+@bank+' and for Project '+ @project + ' for date '+cast(@date_T as varchar(30)) + ' and no. of rows inserted : '+ cast(@row as nvarchar(5)),
									'Active',
									@timestamp_date,
									@cur_user,
									@created_reference_id			 			
									)

			 IF (@ErrorCode = 0)
                        BEGIN
                            IF(@row > 0)
                            BEGIN
                                    SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'success') 
                                    SELECT @outputval
                            END
							ELSE
							BEGIN
									SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'no_records') 
                                    SELECT @outputval
							END
						END
        COMMIT;
				set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Execution of [dbo].[usp_calculated_dispense] completed' 
		------------------------------------------Logging------------------------------------------------------------------------
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
                values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
    END TRY

    BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						 SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
                         SELECT @outputval				
			 END CATCH
			    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
                values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
  END

  --*****************************************End OF Proc****************************************************************
