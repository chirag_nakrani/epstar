 
/****** Object:  StoredProcedure [dbo].[usp_pre_dispense_calc]    Script Date: 18-12-2018 18:04:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
 --- =========================================================================
 --- Created By  :Rubina Q
 --- Created Date: 12-12-2018
 --- Description : prepare datasets for calculation of dispense amounts based on the date range
 --- ==========================================================================
 */
 

 CREATE procedure [dbo].[usp_pre_dispense_calc]

 (
 @date_T date,
 @bank varchar(50),
 @project varchar (10)
  )

 AS

 BEGIN

  BEGIN TRY
  --add project too
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
    values ('Execution of [dbo].[usp_pre_dispense_calc] Started for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

  -- inserting dispense details for last three month's same week period
            TRUNCATE table [cash_dispense_month_minus_3]
		 

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			values ('inserting started in table [cash_dispense_month_minus_3] for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
			INSERT into [cash_dispense_month_minus_3] 
			(   atm_id,
				bank,
				project,
				total_dispense_amount)

				 Select atm_id,
				 bank_name,
				 project_id,
				 total_dispense_amount 
				 from cash_dispense_register
				  where 
				 datafor_date_time between 
				 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-3, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
				 and 
				 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-3, DATEADD(day,3,@date_T)  )  , 105) ),105) 
				 and bank_name=@bank and project_id=@project
				 and record_status='Active' 
				 and is_valid_record is null 


				 DECLARE @row INT			 
				 SET @row=@@ROWCOUNT

			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('inserting completed in table [cash_dispense_month_minus_3] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ @row , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

 
  -- inserting dispense details for last two month's same week period
           TRUNCATE table [cash_dispense_month_minus_2] 
 

			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('inserting started in table [cash_dispense_month_minus_2] for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
				 insert into [cash_dispense_month_minus_2] 
				 (   atm_id,
					 bank,
					 project,
					 total_dispense_amount)
				 Select atm_id,
				 bank_name,
				 project_id,
				 total_dispense_amount
				 from cash_dispense_register
				   where 
				 datafor_date_time between 
				 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
				 and 
				 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,3,@date_T)  )  , 105) ),105)
				  and bank_name=@bank and project_id=@project
				  and record_status='Active' 
				  and is_valid_record is null 

  	         			 
				 SET @row=@@ROWCOUNT

			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('inserting completed in table [cash_dispense_month_minus_2] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ @row , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

 -- inserting dispense details for last  month's same week period

         TRUNCATE table  [cash_dispense_month_minus_1]
  
		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
		  values ('inserting started in table [cash_dispense_month_minus_1] for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
				  insert into [cash_dispense_month_minus_1] 
							  (
							  atm_id,
							  bank,
							  project,
							  total_dispense_amount)
					 Select atm_id,
					 bank_name,
					 project_id,
					 total_dispense_amount 
				 
					 from cash_dispense_register where 
					 datafor_date_time between 
					 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-1, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
					 and 
					 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-1, DATEADD(day,3,@date_T)  )  , 105) ),105) 
					  and bank_name=@bank and project_id=@project
					  and record_status='Active' 
					  and is_valid_record is null

			 SET @row=@@ROWCOUNT

		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
		values ('inserting completed in table [cash_dispense_month_minus_1] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ @row , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
        -- inserting dispense details for current  month's  T-2 to T-9 period

		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
         values ('inserting started in table [cash_dispense_month_minus_0] for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
           TRUNCATE table  [cash_dispense_month_minus_0]   
 
					INSERT into [cash_dispense_month_minus_0]
					(atm_id,
					bank,
					project,
					total_dispense_amount)
					 Select atm_id,
					 bank_name,
					 project_id,
					 total_dispense_amount 
					 from cash_dispense_register
					  where 
					 datafor_date_time between 
					 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-2,@date_T)  )  , 105) ),105) 
					 and 
					 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-9,@date_T)  )  , 105) ),105) 
					  and bank_name=@bank and project_id=@project
					  and record_status='Active' 
					  and is_valid_record is null

					  SET @row=@@ROWCOUNT

			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('inserting completed in table [cash_dispense_month_minus_0] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ @row , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
 

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			values ('Execution of [dbo].[usp_pre_dispense_calc] Completed for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

COMMIT;
END TRY

BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						--SELECT @ErrorNumber;				
			 END CATCH

 END
