USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[usp_default_dispense_bank_wise]    Script Date: 4/25/2019 3:15:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_default_dispense_bank_wise]
(
	@datafor_date_time varchar(100) 
	,@bank varchar(500)
	,@referenceid varchar(20)
	--,@current_date varchar(100) -- REMOVED THIS FROM PARAMETER AND ADDED IN VARIABLE FOR CAPTURE EXACT DATETIME OF EXECUTION.
	,@systemuser varchar(30)
 )
AS
begin
	DECLARE @row INT
	DECLARE @outputVal varchar (10)
	declare @procedure_name varchar(max)  =(SELECT OBJECT_NAME(@@PROCID))
	declare @current_date datetime = dateadd(mi,330,getutcdate())
	declare @Execution_log_str nvarchar(max) = ''
	BEGIN TRY		
		--set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_detail Completed'


	INSERT INTO dbo.execution_log 
		(
			description
			,execution_date_time
			,process_spid
			,process_reference_id
			,execution_program
			,executor_method
		)
		values 
		(
			'Execution of [dbo].usp_default_dispense_bank_wise_V2 Started for @datafor_date_time = ' + @datafor_date_time + ', @bank = ' + @bank + ',@referenceid = ' + @referenceid +', @systemuser =  '+ @systemuser  
			, DATEADD(MI,330,GETUTCDATE())
			,@@SPID
			,NULL
			,'Stored Procedure'
			,OBJECT_NAME(@@PROCID)
		)
	
		--- Split banks and insert into temporary table for further use
		SELECT * INTO #bankDataset FROM dbo.splitstring(@bank,',')
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Parse List of banks', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))	

		begin tran

			IF EXISTS(
						SELECT 1 
						FROM 
						cash_dispense_register 
						where 
						cast(datafor_date_time as date) = cast(@datafor_date_time as date)
						AND bank_name in
							(
								SELECT distinct Name 
								from 
								#bankDataset 
							)
						AND record_status = 'Active'
					)	
			BEGIN
							
				set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Found old entries of cash_dispense_register.'
				
				UPDATE cash_dispense_register
					SET 
						record_status = 'Deleted',
						deleted_by = @systemuser,
						deleted_on = @current_date,
						deleted_reference_id = @referenceid
					WHERE 
						cast(datafor_date_time as date) = cast(@datafor_date_time as date)
						AND 
						bank_name in
							(
								SELECT distinct Name 
								from 
								#bankDataset 
							)
						AND record_status = 'Active'
				set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Updated old entries of cash_dispense_register as Deleted'
			END -- End of if old entries found in cash_dispense_register

			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Start Insert into cash_dispense_register'
			--Insert new entries into cash_dispense_register
			insert into cash_dispense_register 
				(
					bank_name
					,atm_id
					,project_id
					,total_dispense_amount
					,datafor_date_time
					,record_status
					,is_valid_record
					,created_reference_id
					,dispense_type
					,created_on
					,created_by
				)
				select	m.bank_code
					,m.atm_id
					,m.project_id
					,ROUND((( isnull(day_3.total_dispense_amount,0)+isnull(day_2.total_dispense_amount,0))/2),-2) as total_dispense_amount
					,cast(@datafor_date_time as datetime) as datafor_date_time
					,'Active' as record_status,null as is_valid_record
					,@referenceid as created_reference_id
					,'Default' as dispense_type
					,@current_date as created_on
					,@systemuser as created_by
				from atm_master m
				left join
				(
					select	date
							,atm_id
							,bank_name
							,total_dispense_amount
							,reference_id
							,project_id
							,created_on
							,created_by
							,record_status
							,is_valid_record
							,cast(datafor_date_time as date) datafor_date_time
					from cash_dispense_register a
					where 
						cast(datafor_date_time as date) = cast(dateadd(day,-2,@datafor_date_time) as date)
						and
						a.record_status = 'Active'
						--and a.bank_name = @bank 
						and a.bank_name in 
						(
							SELECT distinct Name 
							from 
							#bankDataset 
						)
						--and a.is_valid_record is null
				) day_3
				on	m.bank_code = day_3.bank_name
					and m.atm_id = day_3.atm_id
				left join
				(
					select	date
							,atm_id
							,bank_name
							,total_dispense_amount
							,reference_id
							,project_id
							,created_on
							,created_by
							,record_status
							,is_valid_record
							,cast(datafor_date_time as date) datafor_date_time
					from cash_dispense_register a
					where 
						cast(datafor_date_time as date) = cast(dateadd(day,-1,@datafor_date_time) as date)  
						and a.record_status = 'Active'
						--and a.bank_name = @bank 
						and a.bank_name in 
						(
							SELECT distinct Name 
							from 
							#bankDataset 
						)
						--and a.is_valid_record is null
				) day_2
				on m.bank_code = day_2.bank_name
				and m.atm_id = day_2.atm_id
				where m.site_status = 'Active'
				and m.record_status ='Active'
				--and m.bank_code =@bank
				and m.bank_code in 
					(
						SELECT distinct Name 
						from 
						#bankDataset 
					)

			SET @row=@@ROWCOUNT
			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Inserted ' + cast(@row as varchar(10)) + ' Records into cash_dispense_register'

		commit tran

		IF(@row > 0)
		BEGIN
			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Get success value from app_config_param'

			SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'success') 
			SELECT @outputval
		END
		ELSE
		BEGIN
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Get no_records value from app_config_param'

			SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'no_records') 
			SELECT @outputval
		END

		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Execution completed successfully.'

	END TRY
	BEGIN CATCH
		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Found Exception / Error'

		IF(@@TRANCOUNT > 0 ) -- IF THERE IS ANY EXISTING UNCOMMITED TRANSICTION THEN ROLLBACK
		BEGIN			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Rollback transactions'
			ROLLBACK TRAN;
		END
		-- DEFINE VARIABLES
		DECLARE @ErrorNumber INT = ERROR_NUMBER();
		DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
		DECLARE @ErrorState INT = ERROR_STATE();
		DECLARE @ErrorProcedure varchar(50) = @procedure_name;
		DECLARE @ErrorLine INT = ERROR_LINE();
		DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
		DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
		
		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Write Error Log'
		--INSERT INTO ERROR LOG
		INSERT INTO dbo.error_log
			(
				[errorNumber]
				,[errorSeverity]
				,[errorState]
				,[errorProcedure]
				,[errorLine]
				,[errorMessage]
				,[dateAdded]
			)
			values
			(
				@ErrorNumber
				,@ErrorSeverity
				,@ErrorState
				,@ErrorProcedure
				,@ErrorLine
				,@ErrorMessage
				,@dateAdded
			)
		
		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Get Failed Status value from app_config_param'

		SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
        SELECT @outputval

		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Execution completed with exception / error'
	END CATCH -- END OF CATCH

	INSERT INTO dbo.execution_log 
		(
			description
			,execution_date_time
			,process_spid
			,process_reference_id
			,execution_program
			,executor_method
		)
		values 
		(
			@Execution_log_str
			, DATEADD(MI,330,GETUTCDATE())
			,@@SPID
			,NULL
			,'Stored Procedure'
			,OBJECT_NAME(@@PROCID)
		)

END -- END OF PROCEDURE
