 
/****** Object:  StoredProcedure [dbo].[usp_Dispense_Calculator_12_banks]    Script Date: 19-12-2018 12:51:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: Calculation of  dispense for the banks which do not provide dispense amount directly.
  --              calculating Dispense amount with morning balance of T-1,T-2 and C3R balance.
 --- =========================================================================


-- EXEC [usp_Dispense_Calculator_12_banks] '2018-08-04','MOF','DENA','abc123' 

CREATE PROCEDURE [dbo].[usp_Dispense_Calculator_12_banks]
	-- Add the parameters for the stored procedure here
	(
	 @date_T datetime,
	 @project_id nvarchar(50),
	 @bank_name nvarchar(50),
	 @created_reference_id nvarchar(50)
	 )
AS

BEGIN
    BEGIN TRY
 
            DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @Result varchar(100);
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime =GETUTCDATE();


 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
		values ('Execution of [dbo].[usp_Dispense_Calculator_12_banks] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
   	
	/***********-------Inserting values in Dispense calculation table--------------******************/

 
	Insert into dbo.Dispense_Amount_calc 
	([project_id],
	[bank_name],
	[atm_id],
	site_code,
	indent_date,
	[morning_t_minus_1_bal_50],
	 [morning_t_minus_1_bal_100],
     [morning_t_minus_1_bal_200],
	 [morning_t_minus_1_bal_500],
	 [morning_t_minus_1_bal_2000],
	 morning_total_balance_t_minus_1,
	 [morning_t_minus_2_bal_50],
	 [morning_t_minus_2_bal_100],
	 [morning_t_minus_2_bal_200],
	 [morning_t_minus_2_bal_500],
	 [morning_t_minus_2_bal_2000],
	 morning_total_balance_t_minus_2,
	 [loading_t_minus_2_amt_100],
	 [loading_t_minus_2_amt_200],
	 [loading_t_minus_2_amt_500],
	 [loading_t_minus_2_amt_2000],
	 loading_total_amount_t_minus_2,
	dispense_amount,
	[record_status],
	[created_on],
	[created_reference_id],
	[is_Valid_Record])

	 
	select 
	@project_id   as  project_id,
	@bank_name as bank_name,
	 tm1.atm_id,
	am.site_code,
	@date_T as Indent_Date,
	tm1.remaining_balance_50 as [morning_t_minus_1_bal_50],
	tm1.remaining_balance_100 as [morning_t_minus_1_bal_100],
	tm1.remaining_balance_200 as [morning_t_minus_1_bal_200],
	tm1.remaining_balance_500 as [morning_t_minus_1_bal_500],
	tm1.remaining_balance_2000 as [morning_t_minus_1_bal_2000],
	tm1.morning_balance_t_minus_1,
	tm2.remaining_balance_50 as [morning_t_minus_2_bal_50],
	tm2.remaining_balance_100 as [morning_t_minus_2_bal_100],
	tm2.remaining_balance_200 as [morning_t_minus_2_bal_200],
	tm2.remaining_balance_500 as [morning_t_minus_2_bal_500],
	tm2.remaining_balance_2000 as [morning_t_minus_2_bal_2000],
	tm2.morning_balance_t_minus_1 as morning_balance_t_minus_2,
	CMIS.sw_loading_100 as [loading_t_minus_2_amt_100],
	CMIS.sw_loading_200 as [loading_t_minus_2_amt_200],
	CMIS.sw_loading_500 as [loading_t_minus_2_amt_500],
	CMIS.sw_loading_2000 as [loading_t_minus_2_amt_2000],
	CMIS.[sw_loading_total] as loading_amount_t_minus_2,
	(tm2.morning_balance_t_minus_1 + CMIS.[sw_loading_total] - tm1.morning_balance_t_minus_1) as dispense_amount,
    'Active',
	DATEADD(MI,330,GETUTCDATE()) AS [created_on],
	@created_reference_id as created_reference_id,
    null as isvalidrecord

	
	
	FROM atm_master am
	
	JOIN 	morningbalance tm1   -- Morning balance for date(T-1)
	on 
		am.atm_id=tm1.atm_id	        and 
		am.project_id=tm1.project_id    and
		am.bank_code=tm1.bank_name      and	  
		cast(tm1.datafor_date_time as date)= cast( dateadd(dd,-1,@date_T) as date)


    JOIN morningbalance tm2   -- Morning balance for date(T-2)
	on 
		am.atm_id=tm2.atm_id	    and 
		am.bank_code=tm2.bank_name	and 
		cast(tm2.datafor_date_time as date)=dateadd(dd,-2,@date_T)

    JOIN       [c3r_CMIS] CMIS
    on 
	   am.atm_id=CMIS.atm_id and -- C3R_CMIS for loading amount
	   am.bank_code=CMIS.bank and
	   cast(CMIS.date as date)= dateadd(dd,-2,@date_T) and
	   CMIS.record_status ='Active' and  CMIS.is_valid_record ='yes'

  

		where 
		 am.project_id=@project_id
		 and 
		 am.bank_code=@bank_name
		 and		
		 tm2.morning_balance_t_minus_1 is not null
		 and  
		 CMIS.[sw_loading_total] is not null
		 and  
		 tm1.morning_balance_t_minus_1 is not null

		-----------****************** data_update_log entry no of records**********************---

		DECLARE @row INT			 
			SET @row=@@ROWCOUNT

			INSERT into data_update_log 
			(bank_code,
			project_id,
			operation_type,
			datafor_date_time,
			data_for_type,			 
			status_info_json,			 
			record_status,
			date_created,
			created_reference_id)

			values
			(@bank_name,
			@project_id,
			'Dispense_12_banks',
			@date_T,
			'Dispense_12_banks',			 
			'Data inserted for dispense calculation 12 banks for  '+@bank_name+ ' for date '+@date_T +' and no. of rows inserted : '+ cast(@row as varchar(10)),
			'Active',
			DATEADD(MI,330,GETUTCDATE()),
			@created_reference_id
			
			)

--************************************Logging*******************************************************
 
  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
		values ('Execution of [dbo].[usp_Dispense_Calculator_12_banks] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
	/*    Inserting into CDR table from the Dispense_Amount_calc table with details
		  of dispense amount of other 12 banks
		  Checking the status of the inserted records and changing accordingly
	*/
	

BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @count1 int; 
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())

			IF EXISTS   (						
						SELECT 1
						FROM data_update_log
						WHERE datafor_date_time = @date_T AND 
						 bank_code = @bank_name AND 
					    data_for_type = 'DISPENSE' and 
					    record_status = 'Active'  	  and
					    project_id = @project_id          --chg
				)	
				BEGIN				--- Check Active status in DUL

					DECLARE @ForStatus VARCHAR(15) = 'Active'
					DECLARE @ToStatus VARCHAR(15) = 'Deleted'
					DECLARE @data_for_type varchar(30) = 'DISPENSE'

				 				-----Check active in bankwise file
								IF EXISTS(
										Select 1 as ColumnName
										FROM [cash_dispense_register]
										WHERE
										      datafor_date_time =@date_T AND 
											  bank_name = @bank_name     AND 
											  record_status = 'Active' 	 AND
					                          project_id = @project_id          --chg
										)
										BEGIN			---- Check Cash Balance Register Table for Active Status (START)	

										 
											

											-- update status in  consolidated dispense table  cash_dispense_resgister
											DECLARE @SetWhere_CDR VARCHAR(MAX) = ' bank_name = ''' + 
														@bank_name +  ''' and datafor_date_time = ''' + 
														@date_T         +  ''' and project_id = ''' + @project_id + ''''
							
											EXEC dbo.[uspSetStatus] 'cash_dispense_register',
											@ForStatus,@ToStatus,@current_datetime_stmp,
											'','',
											@SetWhere_CDR,
											@out OUTPUT
										
										 -- update status of Data Update Log  for that bank /date 
											DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
														@bank_name     + ''' and datafor_date_time = ''' + 
														@date_T             + ''' and data_for_type = ''' + 
														@data_for_type  + ''' and project_id = ''' + @project_id + ''''


											EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,@current_datetime_stmp,'','',@SetWhere_DataUpdateLog,@out OUTPUT
											
										 
											insert into data_update_log(bank_code,project_id,datafor_date_time,
											                                                 data_for_type,record_status,date_created)
                                             values(@bank_name,@project_id,@date_T,'Dispense','Active',getdate())


											---------inserting data after status update for revision
													Insert into [dbo].[cash_dispense_register]
	                                                         ([atm_id],
															 [bank_name],
															 [total_dispense_amount],
															 [project_id],
															 [datafor_date_time],
															 record_status,
															 [is_valid_record])

	                                                      select 
															[atm_id],
															[bank_name],
															[dispense_amount],
															[project_id],
															[indent_date],
															record_status,
															is_valid_record
															 from dbo.Dispense_Amount_calc 			
															 where record_status='Active'


                      END
						
			    END			 
				 
 		END	
		
		
		COMMIT;
END TRY

BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						--SELECT @ErrorNumber;				
			 END CATCH				 
	
END

 



