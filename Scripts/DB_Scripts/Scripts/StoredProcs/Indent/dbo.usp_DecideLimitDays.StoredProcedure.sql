/****** Object:  StoredProcedure [dbo].[usp_DecideLimitDays]    Script Date: 08-01-2019 16:50:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	
-- EXEC [usp_DecideLimitDays] '2018-12-24','abc123','malay'

CREATE Procedure [dbo].[usp_DecideLimitDays]

 (  @dateT date,
	@created_reference_id nvarchar(50),
    @cur_user nvarchar(50)
 )
as
BEGIN

BEGIN TRY

	BEGIN TRANSACTION

	DECLARE @out varchar(100)
	DECLARE @outputVal varchar(100)
	DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
	DECLARE @operation_type varchar(50) = 'Decide_Limit_days'
	DECLARE @procedure_name varchar(50) = (SELECT OBJECT_NAME(@@PROCID))

	--declare @dateT datetime
	--set @dateT ='2018-12-24'


	--************************************Logging*******************************************************
         INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
         values ('Execution of [dbo].[usp_DecideLimitDays] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
      --------------- Calculating decide limit days based on the CRA_feasilibilty matrix --------------------------

      --------------- Creating calender of seven days for the indent date @dateT-----------------------------------
	  DROP TABLE IF EXISTS #feasibilityDataset
	  drop table if exists #feasibilityDataset_p1
	    drop table if exists #calender 
		drop table if exists #cte
		;with calendar
		 as
		 (
		  select @dateT startDt,dateadd(day,7,@dateT) endDt, 1 rn
		  union all
		  select dateadd(day,1,startDt) , endDt,rn +1
		  from calendar
		  WHERE  dateadd(day,1,startDt)<endDt
          )
		  
		

		select * into #calender from calendar
      ------------- Creating pivot of of the weekdays with those of cra_feasibility columns--------------------------------
		;with cte 
		 as 
		 (
			select 	cra.atm_id,
					cra.bank_code,
					cra.site_code,
					cra.project_id,
					state,
					v.*					
		    from atm_master atm 
			inner JOIN CRA_feasibility cra
			on atm.atm_id = cra.atm_id
			AND atm.bank_code = cra.bank_code
			AND atm.site_code = cra.site_code
			and atm.record_status ='Active'
			and atm.site_status = 'Active'
			AND CRA.record_status = 'Active'
			--AND atm.project_id = cra.project_id
			     cross apply (values ('monday'   ,is_feasible_mon), 
									 ('tuesday'  ,is_feasible_tue),
									 ('wednesday',is_feasible_wed),
							         ('thursday' ,is_feasible_thu),
					                 ('friday'   ,is_feasible_fri),
						             ('saturday' ,is_feasible_sat)
							  ) v(name,flag)
		 )

		SELECT * into #cte FROM cte 
			

	--	select * from cte where atm_id = 'NA0541C1'
		-- select * from calendar
			---	calculating the next feasible day
			--- above two datasets checking 2nd and 4th Sat also for the same
			--- Also If theres any holiday in feasibility date, skipping the holiday 
			--- and considering the next day
			

                    Select * into #feasibilityDataset_p1
					from
					(
					select  atm_id,
							bank_code,
							site_code,
							project_id,
						    @dateT today,
						    min(case 
									when flag = 'Yes'  
										  and (DATENAME(dw,startDt)<> 'Saturday'
										  or (DATENAME(dw,startDt) = 'Saturday' 
										  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, startDt), 0), startDt) +1) 
										              in (1,3,5) ))
										  THEN
											CASE WHEN 
												(
												startDt NOT BETWEEN dte.start_date AND dte.end_date
												) or 
												(
												dte.holiday_code is null
												)
												THEN
													startDt
									
												END
									end 
								)     nextYDay
								,
								SUM (case 
									when flag = 'Yes'  
										  and (DATENAME(dw,startDt)<> 'Saturday'
										  or (DATENAME(dw,startDt) = 'Saturday' 
										  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, startDt), 0), startDt) +1) 
										              in (1,3,5) ))
										  THEN
											CASE WHEN 
												(
												startDt NOT BETWEEN dte.start_date AND dte.end_date
												) or 
												(
												dte.holiday_code is null
												)
												THEN
													1
									
												END
									end 
								)     as feasibility_frequency
								
									--( SELECT t1.current_day_feasible 
									--  from cte t1
									--  JOIN cte t2
									--  on datename(dw,@dateT) = t1.name
									--) as current_day_feasible


									--,CASE 
									--	WHEN (
									--			SELECT flag from cte 
									--			where name = DATENAME(dw,'2018-12-24')
									--		)  = 'Yes'
									--	THEN 'Yes'
									--	ELSE 
									--		'No'
									--	END as current_feasibility
										
							
										from #calender t1 
										join #cte t2 
										on datename(dw,startDt) = t2.name
										LEFT JOIN holiday_states s 
										on s.State = t2.state
										and s.record_status = 'Active'
										LEFT JOIN holiday_date dte
										on dte.holiday_code = s.holiday_code
										and dte.record_status ='Active'
										WHERE  startDt>@dateT
										group by atm_id,
												bank_code,
												site_code,
												project_id
										)a
					drop table if exists #feasibilityDataset
					
					select a.*,b.flag as is_feasible_today into #feasibilityDataset from #feasibilityDataset_p1 a left join #cte b
					on a.atm_id = b.atm_id and a.site_code = b.site_code
					and DATENAME(dw,a.today) = b.name
					
					
					--select * from #feasibilityDataset
					--select * from #cte

				DROP TABLE IF EXISTS #finaldataset
				
				---	 Calculating decide limit days and loading limit days
				---	 If theres no feasible date in the next 7 days for an ATM, then assigning
				---	 bydefault decide limit days as 7
				
				select atm_id,
				bank_code,
				project_id,
				site_code
				,today
				,nextYday
				,isnull(DATEDIFF(day, @dateT,nextYDay),7) as [decidelimitdays]
					, case 
						when  
							nextYDay IS NULL
							THEN 7
							WHEN 
								 isnull(DATEDIFF(day, @dateT,nextYDay),7) >3
						   then  isnull(DATEDIFF(day, @dateT,nextYDay),7) +1 -- old 0.5
					       else	 isnull(DATEDIFF(day, @dateT,nextYDay),7) +1 
					       end as loadinglimitdays
				,feasibility_frequency
				,is_feasible_today

				into #finaldataset
				from #feasibilityDataset

			
				
				-----************************************Logging******************************************************

           INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Insert into limitdays Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
				--- Check if any record exists in active state for same indent date
				IF EXISTS(
							SELECT 1 FROM limitdays
							WHERE indent_date = @dateT
							and record_status	=	'Active'
						) 
						BEGIN
							
							DECLARE @ForStatus VARCHAR(15) = 'Active'
							DECLARE @ToStatus VARCHAR(15) = 'Deleted'
							
							DECLARE @SetWhere_limitdays VARCHAR(MAX) = ' indent_date = ''' + 
															cast(@dateT as nvarchar(50))  + ''''
							
							EXEC dbo.[uspSetStatus] 'dbo.limitdays',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_limitdays,@out OUTPUT
							
										
						END

						     DECLARE @row INT 
							 DECLARE @ErrorCode INT

							
							--Finally inserting data in limitdays table
							Insert  into limitdays 
							(
								atm_id,
								bank_code,
								eps_site_code,
								project_id,
								indent_date,
								next_feasible_date,
								default_decide_limit_days,
								decidelimitdays,
								default_loading_limit_days,
								loadinglimitdays,
								feasibility_frequency,
								is_feasible_today,
								record_status,
								created_on,
								created_by,
								created_reference_id
							)
							select 
								atm_id,
								bank_code,
								site_code,
								project_id,
								today, 
								nextYday , 
								decidelimitdays,
								decidelimitdays
								, loadinglimitdays
								, loadinglimitdays
								, feasibility_frequency
								, is_feasible_today
								,'Active'
								, @timestamp_date
								, @cur_user
								, @created_reference_id

							from #finaldataset
	
			   		

					-- Generating output if rowcount is > 0 and no error
					
			 		SELECT @row=@@ROWCOUNT,@ErrorCode = @@ERROR

					
					
					IF (@ErrorCode = 0)
                        BEGIN
                            IF(@row > 0)
                            BEGIN
                                    SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'common_success' AND sub_category = 'success_scenario') 
                                    SELECT @outputval
                            END
							ELSE
							BEGIN
									SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'common_failure' AND sub_category = 'failure_scenario') 
                                    SELECT @outputval
							END
						END

			--*************************************Data_update_log entry*****************************************

 

					--INSERT into data_update_log 
					--(
					--bank_code,
					--project_id,
					--operation_type,
					--datafor_date_time,
					--data_for_type,			 
					--status_info_json,			 
					--record_status,
					--date_created
					--)

					--values
					--(NULL,
					--NULL,
					--'Decide_Limit_days', --
					--@dateT,
					--'Decide_Limit_days',			 
					--'Decide Limit days calculated and no. of rows inserted : '+cast(@row as varchar(10)),
					--'Active',
					--DATEADD(MI,330,GETUTCDATE())			
					--)
			---****************************************Logging****************************************************
		 
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			 values ('Insert into limitdays Completed and Execution of [dbo].[usp_DecideLimitDays] Completed with recordcount ' + CAST(@row as nvarchar(10)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	 

COMMIT;
END TRY

BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name;
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)

						SELECT @ErrorNumber
						 			
			 END CATCH
 END



