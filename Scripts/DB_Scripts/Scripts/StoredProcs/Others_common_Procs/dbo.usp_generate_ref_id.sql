
/****** Object:  StoredProcedure [dbo].[generate_ref_id]    Script Date: 29-11-2018 13:52:01 ******/
/****** This procedure will generate reference id with specific format as C100000000112211 ******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[generate_ref_id] 
as BEGIN
DECLARE @date varchar(50) = (SELECT CONVERT(VARCHAR(8), GETDATE(), 8) 'hh:mm:ss')
DECLARE @date_str varchar(50) = (SELECT SUBSTRING(REPLACE(@date,':',''),2,6))
DECLARE @nextval varchar(50) = cast(next value for generate_ref_seq as varchar(50))
select 'C' + @nextval + @date_str
END;