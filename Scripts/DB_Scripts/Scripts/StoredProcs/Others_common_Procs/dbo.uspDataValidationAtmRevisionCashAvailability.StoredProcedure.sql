/****** Object:  StoredProcedure [dbo].[uspDataValidationAtmRevisionCashAvailability]    Script Date: 5/31/2019 6:53:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


CREATE PROCEDURE [dbo].[uspDataValidationAtmRevisionCashAvailability]
( 
	@api_flag    VARCHAR(50),
	@systemUser  VARCHAR(50),
	@referenceid VARCHAR(50),
	@outputVal   VARCHAR(50) OUTPUT
)
AS
BEGIN
	BEGIN TRY
	BEGIN TRAN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar       (max)
	DECLARE @bank_code nvarchar (max)
	DECLARE @project_id nvarchar(max)
	DECLARE @uploaded_record_status nvarchar(50) = 'Uploaded'
	DECLARE @tableName nvarchar(50) = 'atm_revision_cash_availability'
	DECLARE @ToStatus nvarchar(50) = 'Active'
	DECLARE @SetWhereClause VARCHAR(MAX) ='is_valid_record = ''Yes'' AND  created_reference_id = '''+@referenceid+''''
	--DECLARE @api_flag    VARCHAR(50) = 'F'
	--DECLARE @systemUser  VARCHAR(50) = 'sa'
	--DECLARE @referenceid VARCHAR(50) = 'C10061289140540'




	DECLARE @procedurename nvarchar(100) = (SELECT OBJECT_NAME(@@PROCID))

 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	 values ('Execution of [dbo].[uspDataValidationAtmRevisionCashAvailability] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

	IF EXISTS (

	
		
		SELECT 1 as ColumnName
		from data_update_log_master WITH (NOLOCK)
		where record_status = @uploaded_record_status
		and created_reference_id = @referenceid
	)
	BEGIN
		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('data exists in data update log master', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		DECLARE @total_count_rows int = (SELECT count(1) from atm_revision_cash_availability where created_reference_id = @referenceid and record_status = @uploaded_record_status)
	
		UPDATE history
		SET record_status = 'History'
		FROM atm_revision_cash_availability history
		JOIN atm_revision_cash_availability active
		on history.atm_id = active.atm_id
		AND history.site_code = active.site_code
		AND history.for_date = active.for_date
		and history.feeder_branch_code = active.feeder_branch_code
		AND history.bank_code = active.bank_code
		WHERE history.record_status = 'Active'
		and active.record_status = 'Uploaded'




		UPDATE atm_revision_cash_availability
		SET error_code = 
						isnull(case when denomination_100 < 0 then 'denomination_100,' end, '')+
						 isnull(case when denomination_200 < 0 then 'denomination_200,' end, '')+
						 isnull(case when denomination_500 < 0 then 'denomination_500,' end, '')+
						 isnull(case when denomination_2000 < 0 then 'denomination_2000,' end, '')+
						 isnull(case when total_amount <> isnull(denomination_100,0)+isnull(denomination_200,0)+isnull(denomination_500,0)+isnull(denomination_2000,0) then 'total_available_not_matching,' end, '')+
						 isnull(case when ISDATE(cast(for_date as nvarchar)) <> 1 then 'error_for_date,' end, '')
						 --case 
							--		when project_id   in ( select DISTINCT cf.project_id from cra_feasibility cf where cf.record_status = 'Active' )
							--		AND bank_code  in  (select DISTINCT cf.bank_code from cra_feasibility cf where cf.record_status = 'Active')	
							--		AND feeder_branch_code in (select DISTINCT cf.feeder_branch_code from cra_feasibility cf Where cf.record_status = 'Active' )
							--		AND atm_id in (select DISTINCT cf.atm_id from atm_master cf Where cf.record_status = 'Active' and cf.site_status = 'Active')
							--		AND site_code in (select DISTINCT cf.site_code from atm_master cf Where cf.record_status = 'Active' and cf.site_status = 'Active')
							--		THEN '' 
							--	else
							--	'record_not_present,'
							--	end

						where created_reference_id = @referenceid
						and record_status = @uploaded_record_status


					
			UPDATE cash
			set error_code = 
						case when atm.atm_id is null then 'record_not_present,'
						else ''
						end
			FROM atm_revision_cash_availability cash
			LEFT JOIN atm_master atm 
			on cash.atm_id = atm.atm_id
			AND cash.site_code = atm.site_code
			AND cash.bank_code = atm.bank_code
			and cash.project_id = atm.project_id
			and atm.record_status = 'Active'
			and atm.site_status = 'Active'
			where cash.created_reference_id = @referenceid
			and cash.record_status = @uploaded_record_status



		--select * from atm_revision_cash_availability where  created_reference_id = @referenceid


		UPDATE atm_revision_cash_availability
		set is_valid_record = case when error_code = '' then 'Yes' else 'No' end
		where created_reference_id = @referenceid
		and record_status = @uploaded_record_status




		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('update atm_revision_cash_availability table completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		DECLARE @count_valid_records int = (select count(1) from atm_revision_cash_availability where created_reference_id = @referenceid and record_status = @uploaded_record_status and is_valid_record = 'Yes')

		if (@total_count_rows = @count_valid_records)
			BEGIN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('total count rows matched with valid count rows.. updating data update log master', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						 
				update data_update_log_master set record_status = 'Active' where created_reference_id = @referenceid and record_status = @uploaded_record_status
				UPDATE data_update_log_master 
				SET pending_count = 
				(	SELECT COUNT(*) FROM atm_revision_cash_availability 
		 			where record_status = @uploaded_record_status
					and created_reference_id = @referenceid 
				),
				total_count = 
				(
					SELECT COUNT(*) from atm_revision_cash_availability
					where created_reference_id = @referenceid
					and record_status = @uploaded_record_status
				)
				where created_reference_id = @referenceid
				and record_status = 'Active'


			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		  values ('Update complete.. going for status update ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			EXEC  dbo.[uspSetStatus] @tableName,'Uploaded','Active',@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
				SET @outputVal = 'S101'
			END
		else if (@total_count_rows != @count_valid_records and @count_valid_records > 0)
			BEGIN
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('total count rows not matched with valid count rows.. updating data update log master', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				update data_update_log_master set record_status = 'Active' where created_reference_id = @referenceid and record_status = @uploaded_record_status
				UPDATE data_update_log_master 
				SET pending_count = 
				(	SELECT COUNT(*) FROM atm_revision_cash_availability 
		 			where record_status = @uploaded_record_status
					and created_reference_id = @referenceid 
				),
				total_count = 
				(
					SELECT COUNT(*) from atm_revision_cash_availability
					where created_reference_id = @referenceid
					and record_status = @uploaded_record_status
				)
				where created_reference_id = @referenceid
				and record_status = 'Active'

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('Update complete.. going for status update ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				EXEC  dbo.[uspSetStatus] @tableName,'Uploaded','Active',@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
				SET @outputVal = 'S101' 
			END
		else 
			BEGIN
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
								)
			END

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		  values ('execution of [uspDataValidationAtmRevisionCashAvailability] completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Cassette Capacity, Insurance Limit, Bank Limit check started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
					
				UPDATE atm_revision_cash_availability						
				SET     error_code =  
					CASE 
							WHEN	total_amount >
									(
									select (COALESCE(cc.cassette_100_count, clm.cassette_100_count,0)     * COALESCE(bc.capacity_100 ,sa.deno_100_bill_capacity ) *100) +
											(COALESCE(cc.cassette_200_count, clm.cassette_200_count,0)     * COALESCE(bc.capacity_200 ,sa.deno_200_bill_capacity ) *200) +    
											(COALESCE(cc.cassette_500_count, clm.cassette_500_count,0)     * COALESCE(bc.capacity_500 ,sa.deno_500_bill_capacity ) *500) +
											(COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0)   * COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity)*2000) 
                                         AS total_cassette_capacity
                                         FROM 
                                              atm_master AS  mast
                                              INNER JOIN     ATM_Config_limits clm
                                              ON            mast.atm_id = clm.atm_id  AND
															mast.site_status='Active' AND
                                                            mast.site_code = clm.site_code AND
                                                            clm.record_status = @ToStatus
                                              LEFT JOIN      
                                                   (
                                                   SELECT 
                                                        project_id,
                                                        bank_code, 
                                                        site_code,
                                                        atm_id, 
                                                        cassette_50_count, 
                                                        cassette_100_count, 
                                                        cassette_200_count,
                                                        cassette_500_count,
                                                        cassette_2000_count 
                                                    FROM Modify_cassette_pre_config 
                                                    WHERE @current_datetime_stmp between from_date AND to_date
                                                    AND record_status = @ToStatus
                                                   )cc
                                              ON clm.site_code = cc.site_code AND 
                                                 clm.atm_id = cc.atm_id
												 CROSS JOIN system_settings sa
                                                 LEFT JOIN Brand_Bill_Capacity bc ON 
                                                           bc.brand_code = mast.brand 
                                                           AND bc.record_status = @ToStatus
                                                  WHERE mast.record_status = @ToStatus
                                                  and mast.bank_code = @bank_code
                                                  --AND mast.project_id = @projectid 
												  AND mast.atm_id = atm_revision_cash_availability.atm_id  
												  AND sa.record_status = @ToStatus
									)																					
								OR  total_amount >
									(
										CASE 
                                            WHEN ( 
													SELECT insurance_limit 
													from ATM_Config_limits atm 
													where atm.atm_id = atm_revision_cash_availability.atm_id 
													and bank_code = @bank_code 
													--and project_id = @projectid 
													and record_status = @ToStatus
													) 
													IS NOT NULL 
													OR 
													( 
													SELECT insurance_limit 
													from ATM_Config_limits atm 
													where atm.atm_id = atm_revision_cash_availability.atm_id 
													and bank_code = @bank_code 
													--and project_id = @projectid 
													and record_status = @ToStatus
													) <> 0
                                            THEN
                                            (
											SELECT insurance_limit 
											from ATM_Config_limits atm 
											where atm.atm_id = atm_revision_cash_availability.atm_id 
											and bank_code = @bank_code 
											--and project_id = @projectid
											and record_status = @ToStatus
											)
                                            END
									)	 
									OR  total_amount >
									(
										CASE 
                                            WHEN ( 
													SELECT bank_cash_limit 
													from ATM_Config_limits atm 
													where atm.atm_id = atm_revision_cash_availability.atm_id 
													and bank_code = @bank_code 
													--and project_id = @projectid 
													and record_status = @ToStatus
													) 
													IS NOT NULL 
													OR 
													( 
													SELECT bank_cash_limit 
													from ATM_Config_limits atm 
													where atm.atm_id = atm_revision_cash_availability.atm_id 
													and bank_code = @bank_code 
													--and project_id = @projectid 
													and record_status = @ToStatus
													) <> 0
                                            THEN
                                            (
											SELECT bank_cash_limit 
											from ATM_Config_limits atm 
											where atm.atm_id = atm_revision_cash_availability.atm_id 
											and bank_code = @bank_code 
											--and project_id = @projectid
											and record_status = @ToStatus
											)
                                            END
									)	 
							THEN	
								CASE
									WHEN (error_code IS NULL)
									THEN	
										(SELECT cast(sequence as varchar(50)) 
										FROM app_config_param where 
										category = 'CBR_Validation'
										 and sub_category = 'exceeding_limits')
				 					ELSE 
										CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'exceeding_limits')) as varchar(max))
								END
							ELSE error_code
				   END		
				   INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Cassette Capacity, Insurance Limit, Bank Limit check Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
				   UPDATE atm_revision_cash_availability
					SET    is_valid_record = 
					CASE 
						WHEN error_code = ''
						THEN 'Yes'
						ELSE 'No'
					END		

	END--37


	ELSE
		BEGIN
			RAISERROR(90002,16,1)
		END
	--DECLARE @CountTotal  int 
	--DECLARE @countCalculated  int
	COMMIT;
END TRY
BEGIN CATCH
IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedurename
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SELECT @ErrorNumber;				
			 END CATCH
	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	values ('execution of [uspDataValidationAtmRevisionCashAvailability] completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
END