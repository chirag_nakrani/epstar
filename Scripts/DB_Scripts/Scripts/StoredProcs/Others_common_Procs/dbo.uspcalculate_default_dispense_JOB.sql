/****** Object:  StoredProcedure [dbo].[uspcalculate_default_dispense]    Script Date: 25-01-2019 22:07:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspcalculate_default_dispense] 
(
	@datafor_date_time varchar(100)
)  
AS 
BEGIN

DECLARE @referenceid varchar(50) = (next value for generate_ref_seq)
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @username varchar(20)= (SELECT SYSTEM_USER)

EXEC usp_default_dispense_bank_wise @datafor_date_time,'BOMH',@referenceid,@current_datetime_stmp,@username

EXEC usp_default_dispense_bank_wise @datafor_date_time,'CAB',@referenceid,@current_datetime_stmp,@username

EXEC usp_default_dispense_bank_wise @datafor_date_time,'SBI',@referenceid,@current_datetime_stmp,@username

EXEC usp_default_dispense_bank_wise @datafor_date_time,'UBI',@referenceid,@current_datetime_stmp,@username

END


