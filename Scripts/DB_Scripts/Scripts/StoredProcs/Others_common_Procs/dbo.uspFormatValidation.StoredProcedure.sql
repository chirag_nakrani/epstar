/****** Object:  StoredProcedure [dbo].[uspFormatValidation]    Script Date: 27-11-2018 15:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspFormatValidation]
    @datafor varchar(50),@bankcode varchar(50),@ColumnNames varchar(max)
AS
----------Declaring Variables------------------------
BEGIN
DECLARE @MetaDataHeaders varchar(max)
DECLARE @FileHeader varchar(max)
DECLARE @IsRequired varchar(max)
DECLARE @Counter int 
DECLARE @splitcolumncount int
DECLARE @requiredcolcount int = 0
DECLARE @output int
DECLARE @valid_column varchar(max) = ' Valid Columns - '
DECLARE @procedurename nvarchar(200) = (SELECT OBJECT_NAME(@@PROCID))

----------------Declaring and initializing counter---------------------
SET @Counter = 0
DECLARE @MetaDataColumnCount int
--SET @ColumnNames = (SELECT CONCAT(@columnNames,'+'))

---------------Checking the no of columns in Meta Data table and storing it in variable----------
SET @MetaDataColumnCount = (SELECT Count(1) FROM MetaDataTable WHERE datafor = @datafor and bankcode = @bankcode and IsRequired = 'True')
--SET @splitcolumncount = (SELECT count(1) FROM fn_split(@ColumnNames,'+') OPTION(MAXRECURSION 1000)

	;WITH Split
    AS(
        SELECT CAST(0 as varchar(max)) AS stpos, cast(CHARINDEX('+',@ColumnNames) as varchar(max)) AS endpos
        UNION ALL
        SELECT cast(endpos+1 as varchar(max)), CAST(CHARINDEX('+',@ColumnNames,endpos+1) as varchar(max))
            FROM Split
            WHERE endpos > 0
		)
		select SubString(@ColumnNames,cast(stpos as int),cast(endpos as int)-cast(stpos as int)) as ColumnHeaders into #temporary1 from Split where endpos <> 0 OPTION (MAXRECURSION 1000)

		SET @splitcolumncount = (SELECT count(1) from #temporary1)
		--SELECT @splitcolumncount
--;with splitcolumn
-- as
--(
--	SELECT count(1) FROM fn_split(@ColumnNames,'+') OPTION (MAXRECURSION 1000)
--)a
--select * from splitcolumn

    IF(@MetaDataColumnCount > 0)
        BEGIN
        --------------Declaring Cursor1 that will loop through all the records of Meta data table--------------------
        DECLARE cursor1 CURSOR READ_ONLY
            FOR
                SELECT columnHeaders,IsRequired 
                FROM MetaDataTable
                WHERE datafor = @datafor and bankcode = @bankcode
        
            OPEN cursor1
        ------------fetching first record------------------
                FETCH NEXT FROM cursor1 INTO @MetaDataHeaders,@IsRequired
                    WHILE @@FETCH_STATUS = 0 
                    BEGIN
        
                            DECLARE cursor2 cursor Read_Only
                            For
                                SELECT ColumnHeaders FROM fn_split(@ColumnNames,'+') OPTION (MAXRECURSION 1000)
                                OPEN cursor2
                                    FETCH NEXT FROM cursor2 INTO @FileHeader
                                    
                                        WHILE @@FETCH_STATUS=0
                                            BEGIN
                                            
                                            IF(charindex(',',@MetaDataHeaders)>1)
                                            BEGIN
                                            --SELECT @Counter
                                                IF(charindex(@FileHeader,@MetaDataHeaders)>=1 and @IsRequired = 'True')
                                                BEGIN
                                                    SET @Counter = @Counter + 1
                                                    
                                                END
                                            END
                                            
                                            IF(ltrim(rtrim(@FileHeader)) = ltrim(rtrim(@MetaDataHeaders)) and @IsRequired = 'True')
                                                BEGIN
                                                
                                                SET @Counter = @Counter + 1
                                                SET @valid_column = @valid_column + ' ' + @FileHeader
												END
	                                            FETCH NEXT FROM cursor2 INTO @FileHeader
                                END
                                CLOSE cursor2 
                                DEALLOCATE cursor2
                                FETCH NEXT FROM cursor1 INTO @MetaDataHeaders,@IsRequired    
                    END
            CLOSE cursor1 
        DEALLOCATE cursor1
        --select @Counter
        --select @MetaDataColumnCount
        IF( @Counter = @MetaDataColumnCount)
            BEGIN
            DECLARE cursor1 CURSOR READ_ONLY
            FOR
                SELECT columnHeaders
                FROM MetaDataTable
                WHERE datafor = @datafor and bankcode = @bankcode
        
                OPEN cursor1
        ------------fetching first record------------------
                FETCH NEXT FROM cursor1 INTO @MetaDataHeaders
                    WHILE @@FETCH_STATUS = 0 
                    BEGIN
                            DECLARE cursor2 cursor Read_Only
                            For
                                SELECT ColumnHeaders FROM fn_split(@ColumnNames,'+') OPTION (MAXRECURSION 1000)
                                OPEN cursor2
                                    FETCH NEXT FROM cursor2 INTO @FileHeader
                                    
                                        WHILE @@FETCH_STATUS=0
                                            BEGIN
                                            
                                            IF(charindex(',',@MetaDataHeaders)>1)
                                            BEGIN
                                        
                                                IF(charindex(@FileHeader,@MetaDataHeaders)>=1 )
                                                BEGIN
                                                    SET @requiredcolcount = @requiredcolcount + 1
                                                    
                                                END
                                            END
                                            
                                            IF(ltrim(rtrim(@FileHeader)) = ltrim(rtrim(@MetaDataHeaders)))
                                                BEGIN
                                                
                                                SET @requiredcolcount = @requiredcolcount + 1
												SET @valid_column = @valid_column + ' ' + @FileHeader
                                                
                                                END
                                            FETCH NEXT FROM cursor2 INTO @FileHeader
                                END
                                CLOSE cursor2 
                                DEALLOCATE cursor2
                                FETCH NEXT FROM cursor1 INTO @MetaDataHeaders    
                    END
            CLOSE cursor1 
        DEALLOCATE cursor1--SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Success')
        --select @requiredcolcount
        --select @splitcolumncount
            IF(@requiredcolcount = @splitcolumncount)
            BEGIN
            SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Success')
            SELECT @output
            END
            Else
            BEGIN
                SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Failed')
				INSERT INTO error_log (errorProcedure,errorMessage,dateAdded) VALUES (@procedurename,'Meta Data count - ' + cast(@requiredcolcount as varchar(10)) + ', File Header Count - ' + cast(@splitcolumncount as varchar(10)) + @valid_column , DATEADD(MI,330,GETUTCDATE()))
                SELECT @output
            END

            END
        Else
            BEGIN
                SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Failed')
				INSERT INTO error_log (errorProcedure,errorMessage,dateAdded) VALUES (@procedurename,'Meta Data count - ' + cast(@Counter as varchar(10)) + ', File Header Count - ' + cast(@splitcolumncount as varchar(10)) + @valid_column , DATEADD(MI,330,GETUTCDATE()))
                SELECT @output
            END
    END
ELSE
        BEGIN
        INSERT INTO error_log (errorMessage,errorProcedure,errorSeverity) values ('Column information not defined in Meta Data Table',@procedurename,16)

        SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Failed')
        SELECT @output
        END
END


--        ------ Comparing output from both variables-----------------
--        IF( @Counter = @MetaDataColumnCount)
--            BEGIN
--                SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Success')
--                SELECT @output
--            END
--        Else
--            BEGIN
--                SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Failed')
--                SELECT @output
--            END
--    END
--ELSE
--        BEGIN
--        SET @output = ( SELECT sequence from app_config_param where category = 'Format Validation' and sub_category = 'Not Defined')
--        SELECT @output
--        END
--END
GO
