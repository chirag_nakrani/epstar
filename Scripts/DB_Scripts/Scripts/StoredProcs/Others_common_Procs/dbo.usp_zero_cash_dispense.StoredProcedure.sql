CREATE PROCEDURE usp_zero_cash_dispense  --exec usp_zero_cash_dispense '2018-12-05',3,'all','all','all','all','all'
(
	--@datafor_date_time date=getdate(),
	--@n_days int =3,
	--@for_project nvarchar(50) = 'all',
	--@for_bank nvarchar(50) = 'all',
	--@for_feeder_branch nvarchar(50) = 'all',
	--@for_cra nvarchar(50) = 'all',
	--@for_atm_id nvarchar(50) = 'all'
	@datafor_date_time date,
	@n_days int ,
	@for_project nvarchar(50),
	@for_bank nvarchar(50),
	@for_feeder_branch nvarchar(50),
	@for_cra nvarchar(50) ,
	@for_atm_id nvarchar(50) 
) 
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN

		DECLARE @referenceid varchar(50) = (next value for generate_ref_seq)
		DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
		DECLARE @execution_log_str nvarchar(max)='',
				@activestatus nvarchar(10) = 'Active',
				@deletedstatus nvarchar(10) = 'Deleted',
				@timestamp_date date =  DATEADD(MI,330,GETUTCDATE()),
				@cur_user varchar(20)= (select system_user),
				--@created_reference_id varchar(50) = (next value for generate_ref_seq),
				@out varchar(100)
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
		       VALUES ('execution of [dbo].[usp_zero_cash_dispense] started', dateadd(mi,330,getutcdate()),@@spid,null,'stored procedure',object_name(@@procid))
	
		SET @execution_log_str = @execution_log_str + convert(varchar(50),dateadd(mi,330,getutcdate()),120) + ' : execution of [dbo].[usp_zero_cash_dispense] started'

		SELECT cdr.project_id,cdr.atm_id,cdr.bank_name,f.site_code as site_code,f.feeder_branch_code,f.new_cra    ,sum(total_dispense_amount) as tda
						INTO #zero_cash_dispense
						FROM cash_dispense_register cdr
						left outer join 
						cra_feasibility f on
						cdr.atm_id=f.atm_id and 
						cdr.project_id =f.project_id and 
						cdr.bank_name =f.bank_code and
						cdr.record_status = @activestatus and f.record_status = @activestatus 
						WHERE 
						cdr.is_valid_record  is null 
						and datafor_date_time>=  dateadd(day, -@n_days, convert(date,@datafor_date_time)) and datafor_date_time<@datafor_date_time
						and cdr.project_id =case when ( @for_project = 'all') then cdr.project_id else  @for_project   end
						and cdr.bank_name = case when ( @for_bank = 'all') then cdr.bank_name else  @for_bank   end
						and cdr.atm_id = case when ( @for_atm_id = 'all') then cdr.atm_id else  @for_atm_id   end  
						and f.feeder_branch_code = case when ( @for_feeder_branch = 'all') then f.feeder_branch_code else  @for_feeder_branch   end 
						and f.new_cra = case when ( @for_cra = 'all') then f.new_cra else  @for_cra   end  
						GROUP BY cdr.project_id,cdr.atm_id,cdr.bank_name,f.site_code,f.feeder_branch_code,f.new_cra    
						HAVING sum(total_dispense_amount) = 0
		
		SET @execution_log_str = @execution_log_str + convert(varchar(50),dateadd(mi,330,getutcdate()),120) + ' : Insert into temp table completed'

		IF EXISTS(
				SELECT 1 
				FROM zero_cash_dispense
				WHERE project_id =case when ( @for_project = 'all') then project_id else  @for_project   end
				and atm_id = case when ( @for_atm_id = 'all') then atm_id else  @for_atm_id   end
				and bank_name =case when ( @for_bank = 'all') then bank_name else  @for_bank   end
				and convert(date,datafor_date_time) = @datafor_date_time
					)
				
				BEGIN


				SET @execution_log_str = @execution_log_str + char(13) + char(10) + convert(varchar(50),dateadd(mi,330,getutcdate()),120) + ': found old entries for zero_cash_dispense ' 
				
				DECLARE @setwhere_data_update varchar(max) = ' project_id = case when ( ''' + @for_project + '''=''all'') then project_id else '''
											+ @for_project+''' end and atm_id = case when ( ''' + @for_atm_id + '''=''all'') then atm_id else '''
											+ @for_atm_id+''' end and bank_name= case when ( ''' + @for_bank + '''=''all'') then bank_name else '''
											+ @for_bank+''' end and datafor_date_time =  ''' + cast(@datafor_date_time as nvarchar(10))+ ''''

				EXEC dbo.[uspsetstatus] 'dbo.zero_cash_dispense',@activestatus,@deletedstatus,@timestamp_date,@cur_user,@referenceid,@setwhere_data_update,@out output
				
				SET @execution_log_str = @execution_log_str + char(13) + char(10) + convert(varchar(50),dateadd(mi,330,getutcdate()),120) + ': status updated in zero_cash_dispense table for old entries for same  project_id,atm_id and bank_name' 
				
				END 

	SET @execution_log_str = @execution_log_str + char(13) + char(10) + convert(varchar(50),dateadd(mi,330,getutcdate()),120) + ': Insert into zero_cash_dispense started' 
				

	INSERT INTO zero_cash_dispense (project_id,atm_id,bank_name,site_code,datafor_date_time,feeder_branch,cra ,total_dispense_amount,record_status,created_on,created_by,created_reference_id)
	SELECT project_id,atm_id,bank_name,site_code,@datafor_date_time,feeder_branch_code,new_cra ,tda,@activestatus,@timestamp_date,@cur_user,@referenceid
	FROM #zero_cash_dispense ;

	
	SET @execution_log_str = @execution_log_str + char(13) + char(10) + convert(varchar(50),dateadd(mi,330,getutcdate()),120) + ': Insert into zero_cash_dispense Completed' 
				
	COMMIT;

	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	END TRY
	BEGIN CATCH

	IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)

						 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
						  values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))				
			 END CATCH
end