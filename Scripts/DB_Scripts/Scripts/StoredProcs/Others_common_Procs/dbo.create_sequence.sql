/****** Object:  Sequence [dbo].[generate_ref_seq]    Script Date: 29-11-2018 13:07:01 ******/
/****** Generate random sequence starting with 100000000 and then increment it by 1 ****/

CREATE SEQUENCE [dbo].[generate_ref_seq] 
 AS [bigint]
 START WITH 100000000
 INCREMENT BY 1
 MINVALUE 100000000
 MAXVALUE 999999999
 CACHE 
GO


