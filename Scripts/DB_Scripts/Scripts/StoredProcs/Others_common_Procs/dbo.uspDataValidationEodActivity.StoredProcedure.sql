/****** Object:  StoredProcedure [dbo].[uspDataValidationEodActivity]    Script Date: 03-04-2019 20:54:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspDataValidationEodActivity]
( 
	 @api_flag nvarchar(50), @systemUser nvarchar(50), @referenceid nvarchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN
	
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime =  DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50)
	DECLARE @errorcode nvarchar(max)
	--DECLARE @bankcode nvarchar(10) = 'IOB'
	DECLARE @datafor nvarchar(10) = 'EOD_ACTIVITY'
    DECLARE @activestatus nvarchar(20) = 'Active'
	DECLARE @approvedstatus nvarchar(20) = 'Approved'
	DECLARE @uploadedstatus nvarchar(20) = 'Uploaded'

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[uspDataValidationIOB] Started',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


------------------ Check if file is present in Data Update Log---------------


    SET @CountTotal  = (
									select count(1) from 
									[dbo].[indent_eod_pre_activity] 
									where created_reference_id = @referenceid and
									record_status = 'Uploaded'
								 )
	
	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[indent_eod_pre_activity] WITH (NOLOCK)
				WHERE created_reference_id = @referenceid and 
					  record_status = 'Uploaded'
				)
	BEGIN
 		
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Started',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


				--UPDATE [indent_eod_pre_activity] 
				--SET     error_code =  
				--	CASE 
				--		WHEN (	atm_id IN (
				--								SELECT atm_id from 
				--								atm_master where site_status in ('Live','Active')
				--								AND record_status = @activestatus
				--							 )
				--			AND site_code IN	
				--							(
				--								SELECT site_code from 
				--								atm_master where site_status in ('Live','Active')
				--								AND record_status = @activestatus
				--							)
				--			 )
				--		THEN NULL
				--		ELSE
				--			(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'atm_not_live')
				--	END	


				
					
				UPDATE eod 
				SET error_code = 
							CASE WHEN atm.atm_id IS NULL
							THEN (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'Master_Exception' and sub_category = 'id_not_present')
							ELSE 
							CASE WHEN atm.site_status <> 'Active'
							THEN (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'atm_not_live')
							END 
							END
				FROM [indent_eod_pre_activity] eod
				LEFT JOIN atm_master atm 
				on atm.atm_id = eod.atm_id AND
				atm.site_code = eod.site_code
				AND atm.record_status = 'Active'
				WHERE EOD.created_reference_id  = @referenceid
		
	
	
			 -----------Updating is_valid_record column as Yes or No-------------

			UPDATE [indent_eod_pre_activity]
			SET    is_valid_record = 
					CASE 
						WHEN error_code IS NULL
						THEN 'Yes'
						ELSE 'No'
					END		
			
		
			SET @countCalculated = (
									SELECT count(1) 
									FROM [indent_eod_pre_activity]  
									WHERE is_valid_record = 'Yes' 
									)

-----------------Comparing both the counts ---------------------------------------------------------

			IF(@countCalculated != @CountTotal AND @countCalculated > 0)
				BEGIN
		
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'Exception' and sub_category = 'Partial Valid'
									 )	

						SET @outputVal = 'S101' 
			
				END
			ELSE IF (@countCalculated = @CountTotal)
				BEGIN 
			
				SET @outputVal = (
								SELECT sequence from  [dbo].[app_config_param]
								where  category = 'File Operation' and sub_category = 'Data Validation Successful'
							)	
							
					SET @outputVal = 'S101' 									
				END

			ELSE
				BEGIN
				
				
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
									 )		
									 
									 SET @outputVal = 'E112'		
			END


------------------------- Updating columns in Data Update Log table------------------------------------------
				
			UPDATE indent_eod_pre_activity
			SET record_status = 
					CASE 
						WHEN is_valid_record = 'Yes' 
						THEN 'Approval Pending'
						WHEN is_valid_record = 'No' 
						THEN 'Uploaded'
					END
			WHERE created_reference_id = @referenceid



			UPDATE data_update_log_master 
			SET pending_count =	
					(
						SELECT COUNT(1) FROM dbo.indent_eod_pre_activity WHERE created_reference_id = @referenceid
						and record_status = 'Approval Pending'
						),
				total_count = 
					(
						SELECT COUNT(1) FROM dbo.indent_eod_pre_activity WHERE created_reference_id = @referenceid							
					),
				record_status = 'Approval Pending'
			WHERE record_status = 'Uploaded' 
			AND created_reference_id = @referenceid



	
	END
	ELSE
		BEGIN
			RAISERROR (	50004, 16,1);
		END
END



----select * from MetaDataTableMaster where file_type = 'EOD_ACTIVITY'

--SET NOCOUNT ON exec uspValidateConfigFiles 'EOD_ACTIVITY', 'F', 'eps_systemadmin','C10013020502411'
