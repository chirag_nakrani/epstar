/****** Object:  StoredProcedure [dbo].[uspValidateConfigFiles]    Script Date: 15-01-2019 18:36:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspValidateConfigFiles]
@file_type nvarchar(50), @api_flag nvarchar(50), @systemUser nvarchar(50), @referenceid nvarchar(50)

AS

BEGIN
	BEGIN TRY
		BEGIN TRAN
			DECLARE @Cmd NVARCHAR(MAX)
			DECLARE @Result nvarchar(50)
			DECLARE @out nvarchar(100)
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = GETDATE()
			DECLARE @out1 varchar(100)
			
			SET @Cmd = CASE
							WHEN @file_type = 'FEEDER_DENOMINATION'
								THEN 'EXEC [uspDataValidationFeederDenomination] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'FDR_CASSTT_MAX_CAP_PER' 
								THEN 'EXEC [uspDataValidationfdr_casstt_max_capacity_percentage] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
			END --End for case 
			--print (@cmd)
			EXECUTE sp_executesql @Cmd, N'@Result varchar(100) Output ', @Result output
--If dynamic command returns NULL then below will get executed			
			IF (@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Error')
			END
		
--If dynamic command return success code then below steps will be followed
			if (@Result = 'S101')
				BEGIN
					DECLARE @tablename nvarchar(50) = 'dbo.data_update_log_master'
					DECLARE @ForStatus nvarchar(50) = 'Uploaded'
					DECLARE @ToStatus nvarchar(50) = 'Approval Pending'
					DECLARE @SetWhereClause VARCHAR(MAX) =' data_for_type = ''' + 
													@file_type+''' and created_reference_id = '''+@referenceid+''''
					EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
					SELECT @Result
				END
				ELSE
					BEGIN
						Select @Result
					END
					
		COMMIT TRAN
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						DECLARE @code varchar(100)
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						IF @ErrorNumber = 90001
						BEGIN
							SET @code = 'E101'
						END
						ELSE IF @ErrorNumber = 90002
						BEGIN
							SET @code = 'E102'
						END
						SELECT @code
						
				END
			 END CATCH
END