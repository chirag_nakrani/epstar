/****** Object:  StoredProcedure [dbo].[uspDataValidationCRAEscalationEmails]    Script Date: 12-04-2019 17:28:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[uspDataValidationCRAEscalationEmails]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @primaryemailid nvarchar(max)
	DECLARE @cra nvarchar(max)

	-- Checking for any record that is present in uploaded status in table
	
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].cra_escalation_emails
				WHERE record_status = 'Uploaded'
				and created_reference_id = @referenceid
			 )
				
			BEGIN

		-- Proceed if we found any record in uploaded status
		
			DECLARE @tableName VARCHAR(30) = 'dbo.cra_escalation_emails'
			DECLARE @ForStatus VARCHAR(15) = 'Uploaded'
			DECLARE @ToStatus VARCHAR(30) = 'Approval Pending'
			DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''

				-- Check if there are records present in active status in table

				IF EXISTS(SELECT 1 FROM dbo.cra_escalation_emails WHERE record_status = 'Active')
					BEGIN

					-- Check for API Flag if it is for File Upload or screen edit
					-- 'F' - File Upload ,  else - Screen Edit

					IF (@api_flag = 'F')
						BEGIN
							
							INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in cra_escalation_emails started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


					 UPDATE a
                            set  
								a.cra=COALESCE(a.cra,b.cra),
								a.location=COALESCE(a.location,b.location),
								a.location_circle=COALESCE(a.location_circle,b.location_circle),
								a.activity=COALESCE(a.activity,b.activity),
								-- a.[primary]=COALESCE(a.[primary],b.[primary]),
								a.primary_email_id=COALESCE(a.primary_email_id,b.primary_email_id),
								a.primary_contact_no=COALESCE(a.primary_contact_no,b.primary_contact_no),
								-- a.level_1=COALESCE(a.level_1,b.level_1),
								a.level_1_email_id=COALESCE(a.level_1_email_id,b.level_1_email_id),
								a.level_1_contact_no=COALESCE(a.level_1_contact_no,b.level_1_contact_no),
								-- a.level_2=COALESCE(a.level_2,b.level_2),
								a.level_2_email_id=COALESCE(a.level_2_email_id,b.level_2_email_id),
								a.level_2_contact_no=COALESCE(a.level_2_contact_no,b.level_2_contact_no),
								-- a.level_3=COALESCE(a.level_3,b.level_3),
								a.level_3_email_id=COALESCE(a.level_3_email_id,b.level_3_email_id),
								a.level_3_contact_no=COALESCE(a.level_3_contact_no,b.level_3_contact_no),
								-- a.level_4=COALESCE(a.level_4,b.level_4),
								a.level_4_email_id=COALESCE(a.level_4_email_id,b.level_4_email_id),
								a.level_4_contact_no=COALESCE(a.level_4_contact_no,b.level_4_contact_no),
								-- a.project_id=COALESCE(a.project_id,b.project_id),
								 
								-- a.created_on=COALESCE(a.created_on,b.created_on),
								-- a.created_by=COALESCE(a.created_by,b.created_by),
								-- a.created_reference_id=COALESCE(a.created_reference_id,b.created_reference_id),
								-- a.approved_on=COALESCE(a.approved_on,b.approved_on),
								-- a.approved_by=COALESCE(a.approved_by,b.approved_by),
								-- a.approved_reference_id=COALESCE(a.approved_reference_id,b.approved_reference_id),
								-- a.approve_reject_comment=COALESCE(a.approve_reject_comment,b.approve_reject_comment),
								-- a.rejected_on=COALESCE(a.rejected_on,b.rejected_on),
								-- a.rejected_by=COALESCE(a.rejected_by,b.rejected_by),
								-- a.reject_reference_id=COALESCE(a.reject_reference_id,b.reject_reference_id),								 
								-- a.is_valid_record=COALESCE(a.is_valid_record,b.is_valid_record),
								-- a.error_code=COALESCE(a.error_code,b.error_code),

							a.record_status = 'Approval Pending',
							a.modified_on = @current_datetime_stmp,
							a.modified_by = @systemUser,
							a.modified_reference_id = @referenceid
							from  cra_escalation_emails a 
							LEFT join cra_escalation_emails b 
								on
							a.cra = b.cra
							and a.primary_email_id = b.primary_email_id 
							and b.record_status = 'Active'
							WHERE a.record_status = 'Uploaded' 
							


					     INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in cra_escalation_emails completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						 END

					ELSE
						BEGIN
							-- Screen Edit Scenario --
							-- For screen edit we have to directly update the record status from uploaded to approval pending

							EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
						END
					END

					-- If there are no records in active status then directly update record status to approval pending 

					ELSE 
						BEGIN
								EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
							END
							
						
						UPDATE data_update_log_master 
						SET pending_count =	
						(
							SELECT COUNT(*) FROM dbo.cra_escalation_emails WHERE created_reference_id = @referenceid
							and record_status = 'Approval Pending'
						),
						total_count = 
						(
							SELECT COUNT(*) FROM dbo.cra_escalation_emails WHERE created_reference_id = @referenceid
						)
						WHERE record_status = 'Uploaded'
						AND data_for_type = 'CRAESCALATIONMATRIX' 
						AND created_reference_id = @referenceid
							
						SET @outputVal = 'S101'
								
					END


												
						ELSE
						
						BEGIN
						-- Raise error in case of no record found in uploaded status
							RAISERROR(90002,16,1)
						END
						
			END



			



			
