/****** Object:  StoredProcedure [dbo].[uspDataValidationCypherCode]    Script Date: 09-01-2019 11:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


CREATE PROCEDURE [dbo].[uspDataValidationCypherCode]
( 
	@api_flag    VARCHAR(50),
	@systemUser  VARCHAR(50),
	@referenceid VARCHAR(50),
	@outputVal   VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar       (max)
	DECLARE @bank_code nvarchar (max)
	DECLARE @project_id nvarchar(max)

 
	
	--DECLARE @CountTotal  int 
	--DECLARE @countCalculated  int
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].Cypher_Code
				WHERE record_status = 'Uploaded'
				and created_reference_id = @referenceid
			 )
				
			BEGIN
			
			DECLARE @tableName VARCHAR(30) = 'Cypher_Code'
			DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
			DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
			DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''


	IF EXISTS(SELECT 1 FROM Cypher_Code WHERE record_status = 'Active')
			BEGIN
		IF (@api_flag = 'F')
			
			BEGIN
						 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in Cypher_Code started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


					 UPDATE a
                            set  
							a.category=COALESCE(a.category,b.category),
							a.value=COALESCE(a.value,b.value),
							a.cypher_code=COALESCE(a.cypher_code,b.cypher_code),
							a.is_applicable_to_all_projects=COALESCE(a.is_applicable_to_all_projects,b.is_applicable_to_all_projects),
							a.project_id=COALESCE(a.project_id,b.project_id),
							a.is_applicable_to_all_banks=COALESCE(a.is_applicable_to_all_banks,b.is_applicable_to_all_banks),
							a.bank_code=COALESCE(a.bank_code,b.bank_code),
							 
							-- a.created_on=             COALESCE(a.created_on,b.created_on),
							-- a.created_by=             COALESCE(a.created_by,b.created_by),
							-- a.created_reference_id=   COALESCE(a.created_reference_id,b.created_reference_id),
							-- a.approved_on=            COALESCE(a.approved_on,b.approved_on),
							-- a.approved_by=            COALESCE(a.approved_by,b.approved_by),
							-- a.approved_reference_id=  COALESCE(a.approved_reference_id,b.approved_reference_id),
							-- a.approve_reject_comment= COALESCE(a.approve_reject_comment,b.approve_reject_comment),
							-- a.rejected_on=            COALESCE(a.rejected_on,b.rejected_on),
							-- a.rejected_by=            COALESCE(a.rejected_by,b.rejected_by),
							-- a.reject_reference_id=    COALESCE(a.reject_reference_id,b.reject_reference_id),							 
							-- a.is_valid_record=        COALESCE(a.is_valid_record,b.is_valid_record),
							-- a.error_code=             COALESCE(a.error_code,b.error_code),
							a.record_status =         'Approval Pending',
							a.modified_on =           @current_datetime_stmp,
							a.modified_by =           @systemUser,
							a.modified_reference_id = @referenceid
							from        Cypher_Code a 
							LEFT join  Cypher_Code b 
							on
							    a.project_id = b.project_id
							and a.bank_code = b.bank_code 
							and b.record_status =  'Active'
							WHERE a.record_status =  'Uploaded' 
							


					     INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in Cypher_Code completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			END
	ELSE
		BEGIN
		

			EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
		END
	END
			ELSE 
				BEGIN

						EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
					END
					
					UPDATE data_update_log_master 
					SET pending_count =	
							(
								SELECT COUNT(*) FROM dbo.Cypher_Code WHERE created_reference_id = @referenceid
								and record_status = 'Approval Pending'
							),
						total_count = 
						(
							SELECT COUNT(*) FROM dbo.Cypher_Code WHERE created_reference_id = @referenceid
								
						)
					WHERE record_status = 'Uploaded'
					AND data_for_type = 'CYPHERCODE' 
					AND created_reference_id = @referenceid 

					SET @outputVal = 'S101'
				
	END
						
						ELSE
						
						BEGIN
							
							RAISERROR(90002,16,1)
						END
						
		END

