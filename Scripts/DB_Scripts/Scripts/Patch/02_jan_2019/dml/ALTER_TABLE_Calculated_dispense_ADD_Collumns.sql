



ALTER TABLE calculated_dispense
add avg_of_5days_Tminus5  int

ALTER TABLE calculated_dispense
add avg_of_2days_Tminus2  int

ALTER TABLE calculated_dispense
add avg_month_minus_3  int

ALTER TABLE calculated_dispense
add avg_month_minus_2  int

ALTER TABLE calculated_dispense
add avg_month_minus_1  int

ALTER TABLE calculated_dispense
add avg_month_minus_0  int

ALTER TABLE calculated_dispense
add max_of_four_months_avg  int

ALTER TABLE calculated_dispense 
add default_dispense_amount int

ALTER TABLE calculated_dispense 
add final_avg_dispense_amount int
 
