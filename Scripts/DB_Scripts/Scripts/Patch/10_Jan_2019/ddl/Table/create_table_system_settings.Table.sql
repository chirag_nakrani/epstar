/****** Object:  Table [dbo].[system_settings]    Script Date: 1/10/2019 4:06:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[system_settings](
	[id] [bigint] IDENTITY(1,1) NOT NULL PRIMARY KEY,
	[confidence_factor] [varchar](50) NULL,
	[buffer_percentage] [float] NULL,
	[denomination_wise_round_off_100] [int] NULL,
	[denomination_wise_round_off_200] [int] NULL,
	[denomination_wise_round_off_500] [int] NULL,
	[denomination_wise_round_off_2000] [int] NULL,
	[fixed_loading_amount] [int] NULL,
	[default_average_dispense] [int] NULL,
	[vaulting_for_normal_weekday_percentage] [float] NULL,
	[vaulting_for_normal_weekend_percentage] [float] NULL,
	[vaulting_for_extended_weekend_percentage] [float] NULL,
	[dispenseformula] [nvarchar](50) NULL,
	[deno_100_max_capacity_percentage] [float] NULL,
	[deno_200_max_capacity_percentage] [float] NULL,
	[deno_500_max_capacity_percentage] [float] NULL,
	[deno_2000_max_capacity_percentage] [float] NULL,
	[deno_100_priority] [int] NULL,
	[deno_200_priority] [int] NULL,
	[deno_500_priority] [int] NULL,
	[deno_2000_priority] [int] NULL,
	[deno_100_bill_capacity] [int] NULL,
	[deno_200_bill_capacity] [int] NULL,
	[deno_500_bill_capacity] [int] NULL,
	[deno_2000_bill_capacity] [int] NULL,
	[cash_out_logic] [varchar](50) NULL,
	[forecasting_algorithm] [varchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL)
GO

