DROP TABLE ticket_master

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ticket_master](
	[ticket_id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[additional_info] [nvarchar](255) NULL,
	[category_id] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
	[assign_to_user] [nvarchar](100) NULL,
	[assign_to_group] [nvarchar](100) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [date] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [date] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[subject] [nvarchar](100) NULL,
	[remarks] [nvarchar](max) NULL,
	[record_status] [nvarchar](50) NULL
)
GO