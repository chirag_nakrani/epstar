/****** Object:  StoredProcedure [dbo].[uspSetStatus_pycaller]    Script Date: 27-11-2018 15:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspSetStatus_pycaller]
			(
			 @tablename  varchar(100),			 
			 @ForStatus   varchar (30),
             @ToStatus    varchar(30) ,
			 @date        datetime    ,
			 @statusby    varchar(50) , -- e.g approvedby,deletedby
			 @referenceid nvarchar(50),			
			 @whereclause nvarchar(max),
			 @outputVal varchar(100) OUTPUT
			 )

AS

BEGIN
BEGIN TRY	
 	
       EXEC dbo.[uspSetStatus] @tablename,@ForStatus,@ToStatus,@date,@statusby,@referenceid,@whereclause,@outputVal OUTPUT

END TRY

 BEGIN CATCH 
 BEGIN TRANSACTION
    DECLARE @ErrorNumber INT = ERROR_NUMBER();
	DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
	DECLARE @ErrorState INT = ERROR_STATE();
	DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
	DECLARE @ErrorLine INT = ERROR_LINE();
	DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
	DECLARE @dateAdded datetime = GETDATE();
		
		
	INSERT INTO dbo.error_log values
	(@ErrorNumber,@ErrorSeverity,
	 @ErrorState,@ErrorProcedure,
	 @ErrorLine,@ErrorMessage,@dateAdded)
COMMIT TRANSACTION
 END CATCH
END
GO
