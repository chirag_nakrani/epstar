GO
/****** Object:  Table [dbo].[ims_master]    Script Date: 10-01-2019 15:28:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ims_master_tmp](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[ims_sync_master_id] [bigint] NULL,
	[ticketno] [nvarchar](50) NOT NULL,
	[vendorticketno] [nvarchar](50) NULL,
	[site_code] [nvarchar](100) NULL,
	[atmid] [nvarchar](50) NOT NULL,
	[bankname] [nvarchar](100) NULL,
	[ticket_generation_type] [nvarchar](255) NULL,
	[site_details] [nvarchar](255) NULL,
	[fault] [nvarchar](255) NULL,
	[fault_description] [nvarchar](255) NULL,
	[call_status] [nvarchar](255) NULL,
	[ticket_status] [nvarchar](255) NULL,
	[message_date_time] [nvarchar](50) NULL,
	[open_date] [date] NULL,
	[opentime] [datetime] NULL,
	[duration] [nvarchar](50) NULL,
	[remarks] [nvarchar](max) NULL,
	[latestremarks] [nvarchar](max) NULL,
	[follow_up_date] [nvarchar](max) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [nvarchar](50) NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](100) NULL
)
GO


SET IDENTITY_INSERT dbo.ims_master_tmp  ON


INSERT INTO dbo.ims_master_tmp (
	id,
	ims_sync_master_id,
	ticketno,
	vendorticketno,
	site_code,
	atmid,
	bankname,
	ticket_generation_type,
	site_details,
	fault,
	fault_description,
	call_status,
	ticket_status,
	message_date_time,
	open_date,
	opentime,
	duration,
	remarks,
	latestremarks,
	follow_up_date,
	project_id,
	record_status,
	created_on,
	created_by,
	created_reference_id,
	datafor_date_time,
	approved_on,
	approved_by,
	approved_reference_id,
	rejected_on,
	rejected_by,
	reject_reference_id,
	reject_trigger_reference_id,
	deleted_on,
	deleted_by,
	deleted_reference_id,
	modified_on,
	modified_by,
	modified_reference_id,
	is_valid_record,
	error_code
	)

SELECT 

	id,
	ims_sync_master_id,
	ticketno,
	vendorticketno,
	site_code,
	atmid,
	bankname,
	ticket_generation_type,
	site_details,
	fault,
	fault_description,
	call_status,
	ticket_status,
	message_date_time,
	open_date,
	opentime,
	duration,
	remarks,
	latestremarks,
	follow_up_date,
	project_id,
	record_status,
	created_on,
	created_by,
	created_reference_id,
	datafor_date_time,
	approved_on,
	approved_by,
	approved_reference_id,
	rejected_on,
	rejected_by,
	reject_reference_id,
	reject_trigger_reference_id,
	deleted_on,
	deleted_by,
	deleted_reference_id,
	modified_on,
	modified_by,
	modified_reference_id,
	is_valid_record,
	error_code
	FROM ims_master;


SET IDENTITY_INSERT dbo.ims_master_tmp  OFF

EXEC sp_rename 'ims_master', 'ims_master_tmp_bckp_10012019_1532'

EXEC sp_rename 'ims_master_tmp', 'ims_master'

drop table ims_master_tmp_bckp_10012019_1532;


