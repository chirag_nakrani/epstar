/****** Object:  Table [dbo].[action_reference_master_log]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create table [dbo].[ReportConfigForUI]
(
    [id] [int] IDENTITY(1,1) PRIMARY KEY,
    [BankCode] nvarchar(50) NOT NULL,
    [ReportName] nvarchar(100) NOT NULL,
    [ReportType] nvarchar(50) NOT NULL
)
GO