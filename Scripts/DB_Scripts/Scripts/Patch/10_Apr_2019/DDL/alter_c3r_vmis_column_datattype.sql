	ALTER TABLE c3r_VMIS ALTER COLUMN vault_balance_1000 int
	ALTER TABLE c3r_VMIS ALTER COLUMN total_vault_balance bigint
	ALTER TABLE c3r_VMIS ALTER COLUMN [withdrawalfrom_bank_1000] [int]
	ALTER TABLE c3r_VMIS ALTER COLUMN [total_withdrawalfrom_bank] [bigint]
	ALTER TABLE c3r_VMIS ALTER COLUMN [replenishto_atm_1000] [int]
	ALTER TABLE c3r_VMIS ALTER COLUMN [total_replenishto_atm] [bigint]
	ALTER TABLE c3r_VMIS ALTER COLUMN [cashreturn_atm_1000] [int]
	ALTER TABLE c3r_VMIS ALTER COLUMN [total_cashreturn_atm] [bigint]	
	ALTER TABLE c3r_VMIS ALTER COLUMN [unfit_currency_1000] [int]
	ALTER TABLE c3r_VMIS ALTER COLUMN [total_unfit_currency] [bigint]	
	ALTER TABLE c3r_VMIS ALTER COLUMN [atm_unfit_currency_return_1000] [int]
	ALTER TABLE c3r_VMIS ALTER COLUMN [total_atm_unfit_currency_return] [bigint]	
	ALTER TABLE c3r_VMIS ALTER COLUMN [closing_balance_1000] [int]
	ALTER TABLE c3r_VMIS ALTER COLUMN [total_closing_balance] [bigint]
	ALTER TABLE c3r_VMIS ALTER COLUMN [fit_currency_1000] [int]
	ALTER TABLE c3r_VMIS ALTER COLUMN [total_fit_currency] [bigint]	

