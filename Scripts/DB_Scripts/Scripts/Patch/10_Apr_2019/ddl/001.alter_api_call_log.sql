
DROP TABLE api_call_log
CREATE TABLE [dbo].[api_call_log](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_api_url] [nvarchar] (100) NULL,
	[request_method] [nvarchar] (50) NULL,
	[request_meta_info] [nvarchar] (max) NULL,
	[request_user_info] [nvarchar] (100) NULL,
	[request_get_info]  [nvarchar] (max) NULL,
	[request_post_info]  [nvarchar] (max) NULL,
	[reference_id] [nvarchar](50) NULL,
	[request_date] [datetime] NULL,	
	[record_status] [nvarchar](50) NULL
)