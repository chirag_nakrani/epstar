
/****** Object:  Table [dbo].[system_settings]    Script Date: 27-12-2018 21:37:33 ******/
DROP TABLE [system_settings]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[system_settings](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[confidence_factor] [varchar](50) NULL,
	[buffer_percentage] [float] NULL,
	[denomination_wise_round_off_50] [int] NULL,
	[denomination_wise_round_off_100] [int] NULL,
	[denomination_wise_round_off_200] [int] NULL,
	[denomination_wise_round_off_500] [int] NULL,
	[denomination_wise_round_off_2000] [int] NULL,
	[total_rounding_amount] [int] NULL,
	[fixed_loading_amount] [int] NULL,
	[default_average_dispense] [int] NULL,
	[vaulting_for_normal_weekday_percentage] [float] NULL,
	[vaulting_for_normal_weekend_percentage] [float] NULL,
	[vaulting_for_extended_weekend_percentage] [float] NULL,
	[dispenseformula] [nvarchar](50) NULL,
	[deno_100_max_capacity_percentage] [float] NULL,
	[deno_200_max_capacity_percentage] [float] NULL,
	[deno_500_max_capacity_percentage] [float] NULL,
	[deno_2000_max_capacity_percentage] [float] NULL,
	[deno_100_priority] [int] NULL,
	[deno_200_priority] [int] NULL,
	[deno_500_priority] [int] NULL,
	[deno_2000_priority] [int] NULL,
	[deno_100_bill_capacity] [int] NULL,
	[deno_200_bill_capacity] [int] NULL,
	[deno_500_bill_capacity] [int] NULL,	
	[deno_2000_bill_capacity] [int] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL
)

SET IDENTITY_INSERT [dbo].[system_settings] ON 
GO
INSERT [dbo].[system_settings] ([id], [confidence_factor], [buffer_percentage], [denomination_wise_round_off_50], [denomination_wise_round_off_100], [denomination_wise_round_off_200], [denomination_wise_round_off_500], [denomination_wise_round_off_2000], [total_rounding_amount], [fixed_loading_amount], [default_average_dispense], [vaulting_for_normal_weekday_percentage], [vaulting_for_normal_weekend_percentage], [vaulting_for_extended_weekend_percentage], [dispenseformula], [deno_100_max_capacity_percentage], [deno_200_max_capacity_percentage], [deno_500_max_capacity_percentage], [deno_2000_max_capacity_percentage], [deno_100_priority], [deno_200_priority], [deno_500_priority], [deno_2000_priority],[deno_100_bill_capacity],[deno_200_bill_capacity],[deno_500_bill_capacity],[deno_2000_bill_capacity], [record_status], [created_on], [created_by], [created_reference_id], [approved_on], [approved_by], [approved_reference_id], [deleted_on], [deleted_by], [deleted_reference_id], [modified_on], [modified_by], [modified_reference_id]) VALUES (1, N'20', 20, NULL, 100000, 100000, 200000, 200000, NULL, 500000, 50000, 50, 60, 70, N'avg_of_avg', 80, 80, 80, 80, 2, 1, 3, 4,2000,2000,2000,2000, N'Active', NULL, N'Malay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[system_settings] OFF
GO
