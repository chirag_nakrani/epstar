				   ALTER TABLE  distribution_planning_detail
                     DROP COLUMN cypher_code_date;

				   ALTER TABLE  distribution_planning_detail
                      DROP COLUMN cypher_code_day;

				   ALTER TABLE  distribution_planning_detail
                      DROP COLUMN cypher_code_month;	
				   
				   ALTER TABLE  distribution_planning_detail
                      DROP COLUMN cypher_code_year;
				 
				   ALTER TABLE  distribution_planning_detail
                      DROP COLUMN cypher_code_amount;
				     
			       ALTER TABLE  distribution_planning_detail
                      DROP COLUMN cypher_code;	