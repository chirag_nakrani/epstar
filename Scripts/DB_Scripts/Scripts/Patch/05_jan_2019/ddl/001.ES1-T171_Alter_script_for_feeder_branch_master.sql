drop table [dbo].[feeder_branch_master]

/****** Object:  Table [dbo].[feeder_branch_master]    Script Date: 05-01-2019 14:46:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feeder_branch_master](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[bank_code] [nvarchar](max) NULL,
	[project_id] [nvarchar](50) NULL,
	[region_code] [nvarchar](50) NULL,
	[sol_id] [nvarchar](50) NULL,
	[feeder_branch] [nvarchar](50) NULL,
	[district] [nvarchar](max) NULL,
	[circle] [nvarchar](max) NULL,
	[feeder_linked_count] [nvarchar](max) NULL,
	[contact_details] [nvarchar](max) NULL,
	[is_vaulting_enabled] [nvarchar](max) NULL,
	[email_id] [nvarchar](max) NULL,
	[alternate_cash_balance] [nvarchar](max) NULL,
	[is_currency_chest] [nvarchar](max) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
	)
