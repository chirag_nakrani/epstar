/****** Object:  StoredProcedure [dbo].[uspDataValidationBrandBillCapacity]    Script Date: 09-01-2019 11:50:45 ******/
/****** FUNCTIONALITY - This procedure takes two cases 
						1. Update through File Upload 
						2. Update through Screen Edit	
						In case of file upload procedure checks for the previously active entry and update the column  
						value from previously active record to the new one if it found the column value for the new 
						record to be null.
						If column value is not null, it will retain the original value for the new record
******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

ALTER PROCEDURE [dbo].[uspDataValidationBrandBillCapacity]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar            (max)
	DECLARE @brand_code nvarchar     (max)
	DECLARE @sitecode nvarchar       (max)


 
	
	--DECLARE @CountTotal  int 
	--DECLARE @countCalculated  int
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].Brand_Bill_Capacity
				WHERE record_status = 'Uploaded'
				AND created_reference_id = @referenceid
			 )
				
			BEGIN
			DECLARE @tableName VARCHAR(30) = 'Brand_Bill_Capacity'
			DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
			DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
			DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''


	IF EXISTS(SELECT 1 FROM Brand_Bill_Capacity WHERE record_status = 'Active')
			BEGIN
		IF (@api_flag = 'F')
			BEGIN
						 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in Brand_Bill_capacity started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

					    UPDATE a
                        set  
                            a.brand_code=COALESCE(a.brand_code,b.brand_code),
							a.description=COALESCE(a.description,b.description),
							a.capacity_50=COALESCE(a.capacity_50,b.capacity_50),
							a.capacity_100=COALESCE(a.capacity_100,b.capacity_100),
							a.capacity_200=COALESCE(a.capacity_200,b.capacity_200),
							a.capacity_500=COALESCE(a.capacity_500,b.capacity_500),
							a.capacity_2000=COALESCE(a.capacity_2000,b.capacity_2000),							 
							-- a.project_id=COALESCE(a.project_id,b.project_id),
							-- a.created_on=COALESCE(a.created_on,b.created_on),
							-- a.created_by=COALESCE(a.created_by,b.created_by),
							-- a.created_reference_id=COALESCE(a.created_reference_id,b.created_reference_id),
							-- a.approved_on=COALESCE(a.approved_on,b.approved_on),
							-- a.approved_by=COALESCE(a.approved_by,b.approved_by),
							-- a.approved_reference_id=COALESCE(a.approved_reference_id,b.approved_reference_id),
							-- a.approve_reject_comment=COALESCE(a.approve_reject_comment,b.approve_reject_comment),
							-- a.rejected_on=COALESCE(a.rejected_on,b.rejected_on),
							-- a.rejected_by=COALESCE(a.rejected_by,b.rejected_by),
							-- a.reject_reference_id=COALESCE(a.reject_reference_id,b.reject_reference_id),
							-- a.is_valid_record=COALESCE(a.is_valid_record,b.is_valid_record),
							-- a.error_code=COALESCE(a.error_code,b.error_code),

							a.record_status = 'Approval Pending',
							a.modified_on = @current_datetime_stmp,
							a.modified_by = @systemUser,
							a.modified_reference_id = @referenceid
							from  Brand_Bill_Capacity a 
							LEFT join Brand_Bill_Capacity b 
							on  a.brand_code = b.brand_code	
							and b.record_status = 'Active'					 
							WHERE a.record_status = 'Uploaded' 
							

						INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						values ('Update in Brand_Bill_capacity Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			END
	ELSE
		BEGIN
		
	
			EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
		END
	END
			ELSE 
				BEGIN
					 	

						EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
				END
					
						UPDATE data_update_log_master 
						SET pending_count =	
						(
							SELECT COUNT(*) FROM dbo.Brand_Bill_Capacity WHERE created_reference_id = @referenceid
							and record_status = 'Approval Pending'
						),
						total_count = 
						(
							SELECT COUNT(*) FROM dbo.Brand_Bill_Capacity WHERE created_reference_id = @referenceid
						)
						WHERE record_status = 'Uploaded'
						AND data_for_type = 'BRANDBILLCAPACITY' 
						AND created_reference_id = @referenceid



					SET @outputVal = 'S102'
				
	END
						
		ELSE
			BEGIN
						SET @outputVal = 'E105'
						RAISERROR(90002,16,1)
			END
						
		END
