/****** Object:  StoredProcedure [dbo].[uspDataValidationCRAVaultMaster]    Script Date: 09-01-2019 11:55:05 ******/
/****** FUNCTIONALITY - This procedure takes two cases 
						1. Update through File Upload 
						2. Update through Screen Edit	
						In case of file upload procedure checks for the previously active entry and update the column  
						value from previously active record to the new one if it found the column value for the new 
						record to be null.
						If column value is not null, it will retain the original value for the new record
******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 --select * from cra_vault_master
ALTER PROCEDURE [dbo].[uspDataValidationCRAVaultMaster]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @vault_code nvarchar(max)
	DECLARE @sitecode nvarchar(max)

 
	
	--DECLARE @CountTotal  int 
	--DECLARE @countCalculated  int
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].CRA_vault_master
				WHERE record_status = 'Uploaded'
				and created_reference_id = @referenceid
			 )
				
			BEGIN

			DECLARE @tableName VARCHAR(30) = 'CRA_vault_master'
			DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
			DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
			DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''


	IF EXISTS(SELECT 1 FROM CRA_vault_master WHERE record_status = 'Active')
			BEGIN
		IF (@api_flag = 'F')
			BEGIN
					 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in CRA_vault_master started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	
					 UPDATE a
                            set  
						a.vault_code=COALESCE(a.vault_code,b.vault_code),
						a.cra_code=COALESCE(a.cra_code,b.cra_code),
						a.state=COALESCE(a.state,b.state),
						a.region=COALESCE(a.region,b.region),
						a.location=COALESCE(a.location,b.location),
						a.address=COALESCE(a.address,b.address),
						a.vault_type=COALESCE(a.vault_type,b.vault_type),
						a.vault_status=COALESCE(a.vault_status,b.vault_status),
						a.vaulting_allowed=COALESCE(a.vaulting_allowed,b.vaulting_allowed),
						a.contact_details=COALESCE(a.contact_details,b.contact_details),
						 
						-- a.project_id=COALESCE(a.project_id,b.project_id),
						-- a.created_on=COALESCE(a.created_on,b.created_on),
						-- a.created_by=COALESCE(a.created_by,b.created_by),
						-- a.created_reference_id=COALESCE(a.created_reference_id,b.created_reference_id),
						-- a.approved_on=COALESCE(a.approved_on,b.approved_on),
						-- a.approved_by=COALESCE(a.approved_by,b.approved_by),
						-- a.approved_reference_id=COALESCE(a.approved_reference_id,b.approved_reference_id),
						-- a.approve_reject_comment=COALESCE(a.approve_reject_comment,b.approve_reject_comment),
						-- a.rejected_on=COALESCE(a.rejected_on,b.rejected_on),
						-- a.rejected_by=COALESCE(a.rejected_by,b.rejected_by),
						-- a.rejected_reference_id=COALESCE(a.rejected_reference_id,b.rejected_reference_id),						 
						-- a.is_valid_record=COALESCE(a.is_valid_record,b.is_valid_record),
						-- a.error_code=COALESCE(a.error_code,b.error_code),

						a.record_status = 'Approval Pending',
						a.modified_on = @current_datetime_stmp,
						a.modified_by = @systemUser,
						a.modified_reference_id = @referenceid
						from       CRA_vault_master a 
						LEFT join CRA_vault_master b 
						    on
							a.vault_code = b.vault_code			
							and b.record_status = 'Active'			 
							WHERE a.record_status = 'Uploaded' 
							


					     INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in CRA_vault_master completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			END
	ELSE
		BEGIN
		
			EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
		END
	END
			ELSE 
				BEGIN
					 	
						EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
					END
					
					UPDATE data_update_log_master 
					SET pending_count =	
							(
								SELECT COUNT(*) FROM dbo.CRA_vault_master WHERE created_reference_id = @referenceid
								and record_status = 'Approval Pending'
							),
						total_count = 
						(
							SELECT COUNT(*) FROM dbo.CRA_vault_master WHERE created_reference_id = @referenceid
								
						)
					WHERE record_status = 'Uploaded'
					AND data_for_type = 'CRAVAULTMASTER	' 
					AND created_reference_id = @referenceid 

					SET @outputVal = 'S101'
				
	END
						
						ELSE
						
						BEGIN
							
							RAISERROR(90002,16,1)
						END
						
		END

