/****** Object:  StoredProcedure [dbo].[uspDataValidationFeederMaster]    Script Date: 4/24/2019 5:41:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uspDataValidationFeederMaster]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @solid nvarchar(max)
	DECLARE @feederbranch nvarchar(max)
	DECLARE @record_status  nvarchar(max) = 'record_status'

 
	
	-- Checking for any record that is present in uploaded status in table
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].feeder_branch_master
				WHERE record_status = 'Uploaded'
			 )
				
			BEGIN

				-- Check if there are records present in active status in table
				IF EXISTS(SELECT 1 FROM dbo.feeder_branch_master WHERE record_status = 'Active')
				BEGIN
				IF (@api_flag = 'F')
					BEGIN
						 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in feeder_branch_master started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


					 UPDATE a
                            set  
					    a.bank_code=COALESCE(a.bank_code,b.bank_code),
						a.project_id=COALESCE(a.project_id,b.project_id),
						a.region_code=COALESCE(a.region_code,b.region_code),
						a.sol_id=COALESCE(a.sol_id,b.sol_id),
						a.feeder_branch=COALESCE(a.feeder_branch,b.feeder_branch),
						a.district=COALESCE(a.district,b.district),
						a.circle=COALESCE(a.circle,b.circle),
						a.feeder_linked_count=COALESCE(a.feeder_linked_count,b.feeder_linked_count),
						a.contact_details=COALESCE(a.contact_details,b.contact_details),
						a.is_vaulting_enabled=COALESCE(a.is_vaulting_enabled,b.is_vaulting_enabled),
						a.email_id=COALESCE(a.email_id,b.email_id),
						a.alternate_cash_balance=COALESCE(a.alternate_cash_balance,b.alternate_cash_balance),
						a.is_currency_chest=COALESCE(a.is_currency_chest,b.is_currency_chest),
						 
						-- a.created_on=COALESCE(a.created_on,b.created_on),
						-- a.created_by=COALESCE(a.created_by,b.created_by),
						-- a.created_reference_id=COALESCE(a.created_reference_id,b.created_reference_id),
						-- a.approved_on=COALESCE(a.approved_on,b.approved_on),
						-- a.approved_by=COALESCE(a.approved_by,b.approved_by),
						-- a.approved_reference_id=COALESCE(a.approved_reference_id,b.approved_reference_id),
						-- a.approve_reject_comment=COALESCE(a.approve_reject_comment,b.approve_reject_comment),
						-- a.rejected_on=COALESCE(a.rejected_on,b.rejected_on),
						-- a.rejected_by=COALESCE(a.rejected_by,b.rejected_by),
						-- a.reject_reference_id=COALESCE(a.reject_reference_id,b.reject_reference_id),
						 
						-- a.is_valid_record=COALESCE(a.is_valid_record,b.is_valid_record),
						-- a.error_code=COALESCE(a.error_code,b.error_code),

							a.record_status = 'Approval Pending',
							a.modified_on = @current_datetime_stmp,
							a.modified_by = @systemUser,
							a.modified_reference_id = @referenceid
							from       feeder_branch_master a 
							LEFT join feeder_branch_master b 
								on
							    a.bank_code = b.bank_code
							and a.project_id = b.project_id 
							and a.feeder_branch = b.feeder_branch
							and b.record_status = 'Active' 
							WHERE a.record_status = 'Uploaded' 
							


					     INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in feeder_branch_master completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			END
	ELSE
		BEGIN
			-- Screen Edit Scenario --
			-- For screen edit we have to directly update the record status from uploaded to approval pending

			DECLARE @tableName VARCHAR(30) = 'dbo.feeder_branch_master'
			DECLARE @ForStatus1 VARCHAR(30) = 'Uploaded'
			DECLARE @ToStatus1 VARCHAR(30) =  'Approval Pending'
			DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''

			EXEC  dbo.[uspSetStatus] @tableName,@ForStatus1,@ToStatus1,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
		END
	END
	ELSE 
		BEGIN
			 	-- If there are no records in active status then directly update record status to approval pending 

				DECLARE @tableName2 VARCHAR(30) = 'dbo.feeder_branch_master'
				DECLARE @ForStatus2 VARCHAR(30) = 'Uploaded'
				DECLARE @ToStatus2 VARCHAR(30) =  'Approval Pending'
				DECLARE @SetWhereClause2 VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''

				EXEC  dbo.[uspSetStatus] @tableName2,@ForStatus2,@ToStatus2,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause2,@out OUTPUT
			END
			

			SET @outputVal = 'S101'
				
	END

						
						ELSE
						
						BEGIN
						
							RAISERROR(90002,16,1)
						END
						
			END

