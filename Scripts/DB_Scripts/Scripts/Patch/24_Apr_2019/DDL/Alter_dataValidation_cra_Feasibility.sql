/****** Object:  StoredProcedure [dbo].[uspDataValidationCRAFeasibility]    Script Date: 4/24/2019 5:00:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uspDataValidationCRAFeasibility]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @atmid nvarchar(max)
	DECLARE @sitecode nvarchar(max)
	DECLARE @errorcode varchar(max)


	
	-- Checking for any record that is present in uploaded status in table

	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].cra_feasibility
				WHERE record_status = 'Uploaded'
				and created_reference_id = @referenceid
			)
				
			BEGIN

				-- Proceed if we found any record in uploaded status
				
				SET @CountTotal = (
									SELECT count(1) FROM dbo.cra_feasibility
									WHERE record_status = 'Uploaded'
									and created_reference_id = @referenceid
								  )	   
	
				SET @countCalculated = (
										SELECT count(1) FROM dbo.cra_feasibility cf
										INNER JOIN atm_master clma 
										ON cf.atm_id = clma.atm_id and cf.site_code = clma.site_code
										WHERE cf.record_status = 'Uploaded'
										and cf.created_reference_id = @referenceid
									)

----------------- Updating isValidRecord as Valid or Invalid based on validation ---------------------------
		
-------------- Check if all atm ids and site code are present in the master data -----------------
				UPDATE dbo.cra_feasibility
				SET is_valid_record = 
				CASE WHEN atm_id in (
											SELECT cf.atm_id FROM dbo.cra_feasibility cf
											INNER JOIN atm_master clma 
											ON cf.atm_id = clma.atm_id 
											WHERE cf.record_status = 'Uploaded'
											and cf.created_reference_id = @referenceid
											)
										AND

							site_code in (
											SELECT cf.site_code FROM dbo.cra_feasibility cf
											INNER JOIN atm_master clma 
											ON cf.site_code = clma.site_code 
											WHERE cf.record_status = 'Uploaded'
											and cf.created_reference_id = @referenceid
									      )
						THEN 'Yes'
							  ELSE
							'No'
					END 
					WHERE record_status = 'Uploaded'
					and created_reference_id = @referenceid

-------------- Check if all atm ids and site code are present in the master data -----------------
					DECLARE @tableName VARCHAR(30) = 'dbo.cra_feasibility'
					DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
					DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'

					--- If all records found in config limits and atm master table

		IF ((@CountTotal = @countCalculated) OR (@CountTotal <> @countCalculated AND @countCalculated > 0))
		BEGIN
					--- Update is_valid_file in data update log as YES

						UPDATE dbo.data_update_log_master 
						SET is_valid_file =
								CASE WHEN @CountTotal = @countCalculated
								THEN	'Yes'
								ELSE	'No'
								END
						WHERE record_status = 'Uploaded'
						AND data_for_type = 'CRAFEASIBILITY'
						and created_reference_id = @referenceid

				IF EXISTS( SELECT 1 FROM dbo.cra_feasibility WHERE record_status = 'Active')
				BEGIN
							-- Check for API Flag if it is for File Upload or screen edit
							-- 'F' - File Upload ,  else - Screen Edit

					IF (@api_flag = 'F')
						BEGIN
								 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
								 values ('Update in cra_feasibility started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


					 UPDATE a
                            set  
							a.site_code=COALESCE(a.site_code,b.site_code),
							a.atm_id=COALESCE(a.atm_id,b.atm_id),
							a.project_id=COALESCE(a.project_id,b.project_id),
							a.bank_code=COALESCE(a.bank_code,b.bank_code),
							a.feeder_branch_code=COALESCE(a.feeder_branch_code,b.feeder_branch_code),
							a.new_cra=COALESCE(a.new_cra,b.new_cra),
							a.feasibility_or_loading_frequency=COALESCE(a.feasibility_or_loading_frequency,b.feasibility_or_loading_frequency),
							a.distance_from_hub_to_site=COALESCE(a.distance_from_hub_to_site,b.distance_from_hub_to_site),
							a.distance_from_atm_site_to_nodal_branch=COALESCE(a.distance_from_atm_site_to_nodal_branch,b.distance_from_atm_site_to_nodal_branch),
							a.distance_from_nodal_branch=COALESCE(a.distance_from_nodal_branch,b.distance_from_nodal_branch),
							a.flm_tat=COALESCE(a.flm_tat,b.flm_tat),
							a.cra_spoc=COALESCE(a.cra_spoc,b.cra_spoc),
							a.cra_spoc_contact_no=COALESCE(a.cra_spoc_contact_no,b.cra_spoc_contact_no),
							a.br_document_status=COALESCE(a.br_document_status,b.br_document_status),
							a.cash_van=COALESCE(a.cash_van,b.cash_van),
							a.gunman=COALESCE(a.gunman,b.gunman),
							a.lc_nearest_hub_or_spoke_or_branch_from_site=COALESCE(a.lc_nearest_hub_or_spoke_or_branch_from_site,b.lc_nearest_hub_or_spoke_or_branch_from_site),
							a.accessibility=COALESCE(a.accessibility,b.accessibility),
							a.first_call_dispatch_time=COALESCE(a.first_call_dispatch_time,b.first_call_dispatch_time),
							a.last_call_dispatch_time=COALESCE(a.last_call_dispatch_time,b.last_call_dispatch_time),
							a.reason_for_limited_access=COALESCE(a.reason_for_limited_access,b.reason_for_limited_access),
							a.vaulting=COALESCE(a.vaulting,b.vaulting),
							a.feasibility_received_date=COALESCE(a.feasibility_received_date,b.feasibility_received_date),
							a.feasibility_send_date=COALESCE(a.feasibility_send_date,b.feasibility_send_date),
							a.br_request_date=COALESCE(a.br_request_date,b.br_request_date),
							a.br_send_date=COALESCE(a.br_send_date,b.br_send_date),
							a.is_feasible_mon=COALESCE(a.is_feasible_mon,b.is_feasible_mon),
							a.is_feasible_tue=COALESCE(a.is_feasible_tue,b.is_feasible_tue),
							a.is_feasible_wed=COALESCE(a.is_feasible_wed,b.is_feasible_wed),
							a.is_feasible_thu=COALESCE(a.is_feasible_thu,b.is_feasible_thu),
							a.is_feasible_fri=COALESCE(a.is_feasible_fri,b.is_feasible_fri),
							a.is_feasible_sat=COALESCE(a.is_feasible_sat,b.is_feasible_sat),							 
							-- a.created_on=COALESCE(a.created_on,b.created_on),
							-- a.created_by=COALESCE(a.created_by,b.created_by),
							-- a.created_reference_id=COALESCE(a.created_reference_id,b.created_reference_id),

							a.record_status = 'Approval Pending',
							a.modified_on = @current_datetime_stmp,
							a.modified_by = @systemUser,
							a.modified_reference_id = @referenceid
							from      cra_feasibility a 
							LEFT join cra_feasibility b 
								on
							    a.atm_id = b.atm_id
							and a.site_code = b.site_code 
							and b.record_status = 'Active'
							WHERE a.record_status = 'Uploaded' 
							and a.is_valid_record = 'Yes'
							


					     INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in cra_feasibility completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

									END
									ELSE
									BEGIN

										-- Screen Edit Scenario --
										-- For screen edit we have to directly update the record status from uploaded to approval pending
										DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''' and is_valid_record = ''Yes'''

										EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
										
									END
							END
							ELSE 
								BEGIN
										
										-- If there are no records in active status then directly update record status to approval pending 
										
										DECLARE @SetWhereClause2 VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''' and is_valid_record = ''Yes'''


										EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause2,@out OUTPUT
									END
									
									UPDATE data_update_log_master 
										SET pending_count =	
												(
													SELECT COUNT(*) FROM dbo.cra_feasibility WHERE created_reference_id = @referenceid
													and record_status = 'Approval Pending'
												),
											total_count = 
											(
												SELECT COUNT(*) FROM dbo.cra_feasibility WHERE created_reference_id = @referenceid
													
											)
										WHERE record_status = 'Uploaded'
										AND data_for_type = 'CRAFEASIBILITY' 
										AND created_reference_id = @referenceid 
									
									SET @outputVal = 'S101'
										
							END
	
					ELSE
								BEGIN
						-----------------------Updated Part START-----------------------------------------
									UPDATE data_update_log_master 
									SET is_valid_file = 'No'
									WHERE record_status = 'Uploaded'
									AND data_for_type = 'CRAFEASIBILITY' 
									
									RAISERROR(90001,16,1)
								END

						END
						
						ELSE
						
						BEGIN
							--SELECT 'In rasierror 90002'
							RAISERROR(90002,16,1)
						END
						
			END