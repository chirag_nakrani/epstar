CREATE CLUSTERED INDEX [ClusteredIndex_cash_balance_file_bob] ON [dbo].[cash_balance_file_bob]
(
	[id] ASC,
	[atmid] ASC,
	[record_status] ASC,
	[created_reference_id] ASC,
	[datafor_date_time] ASC
)

