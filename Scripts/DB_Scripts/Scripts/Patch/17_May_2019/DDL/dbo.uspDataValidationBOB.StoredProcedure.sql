USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspConsolidateCBR_BOB]    Script Date: 5/17/2019 1:08:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uspConsolidateCBR_BOB] 
   (@datafor_date_time varchar(100),@projectid varchar(50),@region varchar(50),@referenceid varchar(50),@systemUser varchar(50),@out varchar(200) OUTPUT)
AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @current_datetime_stmp datetime =  DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;
DECLARE @bankcode nvarchar(10) = 'BOB'
DECLARE @datafor nvarchar(10) = 'CBR'
DECLARE @activestatus nvarchar(20) = 'Active'
DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
DECLARE @approvedstatus nvarchar(20) = 'Approved'
--DECLARE @out varchar(200);


	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
  values ('Execution of [dbo].[uspConsolidateCBR_BOB] Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		IF EXISTS(
			Select 1 as ColumnName
			FROM [dbo].[data_update_log]
			WHERE datafor_date_time = @datafor_date_time AND 
					bank_code = @bankcode AND 
					data_for_type = @datafor AND 
					record_status = @approvedstatus AND
					project_id = @projectid AND 
					region=@region AND
					created_reference_id = @referenceid
			) 
		BEGIN			--- Check for approved status in DUL

		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			 values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			IF EXISTS   (						
				SELECT 1
				FROM [dbo].[data_update_log]
				WHERE datafor_date_time = @datafor_date_time AND 
				bank_code = @bankcode AND 
					data_for_type = @datafor AND 
					record_status = @activestatus
					--region=@region AND
					--project_id = @projectid
				)	
				BEGIN				--- Check Active status in DUL

		

				    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
					values ('Checking in bank wise file for previously active record', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
							IF EXISTS
							(
								Select 1 as ColumnName
								FROM [dbo].[cash_balance_file_bob]
								WHERE datafor_date_time =  @datafor_date_time AND  
										record_status = @activestatus --AND
										--region=@region AND
										--project_id = @projectid
							) 
							BEGIN						-----Check active in bankwise file
						INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
						values ('Checking in cash balance register for previously active record', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
								IF EXISTS(
										Select 1 as ColumnName
										FROM [dbo].[cash_balance_register]
										WHERE datafor_date_time = @datafor_date_time AND 
												bank_name = @bankcode  AND 
												record_status = @activestatus --AND
												--region=@region AND
												--project_id = @projectid
										)
										BEGIN			---- Check Cash Balance Register Table for Active Status (START)	

											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
											values ('Active record found!! Changing status from Active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
											
											UPDATE history
											SET history.record_status = 'Deleted',
												history.deleted_on = @current_datetime_stmp,
												history.deleted_by = @systemUser,
												history.deleted_reference_id = @referenceid
											FROM cash_balance_file_bob history
											JOIN cash_balance_file_bob new
											on  new.project_id = history.project_id
											AND new.datafor_date_time = history.datafor_date_time
											AND new.record_status = 'Approved'
											AND history.record_status = 'Active'
											
											UPDATE cash_balance_file_bob set record_status='Active',
													modified_on=@current_datetime_stmp,
													modified_by=@systemUser , 
													modified_reference_id=@referenceid

														where  datafor_date_time=@datafor_date_time 
														and created_reference_id=@referenceid
														and record_status='Approved'
										 
											 
											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
											values ('Records successfully marked as Deleted. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

											---------inserting data after status update for revision
												INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id
													)
											
													Select datafor_date_time, 
															@bankcode,
															atmid,
															CASE
																WHEN hop1bill = 100 THEN hop1endcash
																WHEN hop2bill = 100 THEN hop2endcash
																ELSE
																0
															END as Deno100,
															CASE
																WHEN hop2bill = 200 THEN hop2endcash
																ELSE 
																0
															END as Deno200,
															hop3endcash,
															hop4endcash,
															endcash as closing_bal,
															am.project_id,
															region,
															@activestatus,
															CBH.is_valid_record,
															CBH.created_on,
															CBH.created_by,
															CBH.created_reference_id,
															CBH.approved_on,
															CBH.approved_by,
															CBH.approved_reference_id,
															CBH.modified_on,
															CBH.modified_by,
															CBH.modified_reference_id   
														from  
														[dbo].[cash_balance_file_bob] CBH
														left join atm_master AM ON
														 AM.bank_code=@bankcode
														and AM.record_status='Active'
														and AM.site_status='Active'

														where CBH.datafor_date_time= @datafor_date_time             --remove created
														and CBH.record_status = @activestatus   
														and CBH.created_reference_id = @referenceid

							
														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
								            
                            UPDATE history
														SET history.record_status = 'Deleted',
															history.deleted_on = @current_datetime_stmp,
															history.deleted_by = @systemUser,
															history.deleted_reference_id = @referenceid
														FROM cash_balance_register history
														JOIN cash_balance_register new
														on new.atm_id = history.atm_id
														AND new.project_id = history.project_id
														AND new.bank_name = @bankcode
														AND history.bank_name = @bankcode
														AND new.datafor_date_time  = @datafor_date_time
														AND new.created_reference_id <> history.created_reference_id
														AND history.created_reference_id <> @referenceid
														and new.record_status = @activestatus
														AND history.record_status = @activestatus

                            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
														values ('Start:updateing dataupdate log to mark active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


														DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor + ''' and project_id = ''' + @projectid + ''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
											
															--Set status in bank wise table as Approved
											
														---------inserting data after status update for revision
																							
										
														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
														
                            
                            IF EXISTS (SELECT 1 from dbo.cash_balance_register 
																	where datafor_date_time = @datafor_date_time and  
																	bank_name = @bankcode and 
																	record_status = @activestatus and 
																	created_reference_id=@referenceid 
																	--region=@region and
																	--project_id = @projectid
                                  )
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'
																		) 
														END
														ELSE
															BEGIN
																RAISERROR(50003,16,1)
															END

											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[uspConsolidateCBR_BOB] Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
										END				---- Check Cash Balance Register Table for Active Status (END)

								END
							ELSE		------	BOMH ELSE
							BEGIN
								RAISERROR(50050,16,1)
										--ELSE			---- Check Cash Balance Register Table ELSE
										--BEGIN 
										--END			

									----- BOB Active Check Start(END)
							--ELSE		------	BOB ELSE
							--BEGIN
							--END
				
						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN
				
					UPDATE cash_balance_file_bob set record_status='Active',
						modified_on=@current_datetime_stmp,
						modified_by=@systemUser , 
						modified_reference_id=@referenceid

							where  datafor_date_time=@datafor_date_time 
							and created_reference_id=@referenceid
							and record_status='Approved'
								
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
					values ('Consolidation into cash balance register started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
					

          ---------inserting data after status update for revision
													INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id
													)
											
													Select datafor_date_time, 
															@bankcode,
															atmid,
															CASE
																WHEN hop1bill = 100 THEN hop1endcash
																WHEN hop2bill = 100 THEN hop2endcash
																ELSE
																0
															END as Deno100,
															CASE
																WHEN hop2bill = 200 THEN hop2endcash
																ELSE 
																0
															END as Deno200,
															hop3endcash,
															hop4endcash,
															endcash as closing_bal,
															am.project_id,
															region,
															@activestatus,
															CBH.is_valid_record,
															CBH.created_on,
															CBH.created_by,
															CBH.created_reference_id,
															CBH.approved_on,
															CBH.approved_by,
															CBH.approved_reference_id,
															CBH.modified_on,
															CBH.modified_by,
															CBH.modified_reference_id   
														from  
														[dbo].[cash_balance_file_bob] CBH
														left join atm_master AM ON
														--CBH.termid=AM.atm_id
														 AM.bank_code=@bankcode
														and AM.record_status='Active'
														and AM.site_status='Active'
														
                            where CBH.datafor_date_time= @datafor_date_time 
														and CBH.record_status = @activestatus
														and CBH.created_reference_id = @referenceid



														DECLARE @SetWhere_DataUpdateLog_dul VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor 
																	 + ''' and project_id = ''' + @projectid +
																	  ''' and region= ''' +@region+''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',
																		@approvedstatus,@activestatus,
																		@current_datetime_stmp,@systemUser,
																		@referenceid,@SetWhere_DataUpdateLog_dul,@out OUTPUT
											

														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data Consolidated Successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														    
                            IF EXISTS (SELECT 1 from dbo.cash_balance_register 
																	where datafor_date_time = @datafor_date_time and  
																	bank_name = @bankcode and 
																	record_status = @activestatus and 
																	created_reference_id=@referenceid 
																	--region=@region and
																	--project_id = @projectid
                                  )
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' 
                                    AND sub_category = 'Consolidation Successful'
																		) 
														END
														ELSE
														BEGIN
															RAISERROR(50003,16,1) 
														END

				                    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
                            values ('Execution of [dbo].[uspConsolidateCBR_BOB] Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
			BEGIN
				RAISERROR(50010,16,1)
			END
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
				values ('Execution of [dbo].[uspConsolidateCBR_BOB] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

END
