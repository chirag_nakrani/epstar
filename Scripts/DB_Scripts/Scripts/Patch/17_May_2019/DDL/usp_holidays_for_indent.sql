USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[usp_holidays_for_indent]    Script Date: 17-05-2019 15:02:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec usp_holidays_for_indent @dateH='2019-04-22 20:09:52.187', @Created_by='sagar', @Created_Reference_Id='ref123'


ALTER procedure [dbo].[usp_holidays_for_indent] (
	@dateH date,
@Created_by nvarchar(50),
@Created_Reference_Id nvarchar(50)

)
as
begin
	
--- =========================================================================
 --- Created By :Sagar 
 --- Created Date: 22-04-2019
 --- Description: Populate indent_holiday table which contains holiday for which indents reuired/not required to be generated.
 --- so when this SP runs, for given date, at first it makes previous data inactive for taht day then it generates holiday data for that particular day .	
 ----and from the UI User can mark records for which holiday day they want indent generation
 ----note : this SP should run before Indent Generation Process	so that data is available for indent generation.	  
--- =========================================================================

--making previous record inactive for particular day

declare @rowcount int;
DECLARE @outputval varchar(100)

Begin try 

INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
 values ('Execution of [dbo].[usp_holidays_for_indent] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
 values ('Making data inactive for:'+cast(@dateH as varchar), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

update indent_holiday set record_status = 'Inactive'
where holiday_date = @dateH

INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
 values (cast(@@ROWCOUNT as varchar)+' Records marked inactive for:'+cast(@dateH as varchar), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
 values ('Populating holiday data for:'+cast(@dateH as varchar), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
--populating table
insert into indent_holiday
(
	 holiday_date
	,is_holiday
	,holiday_code
	,holiday_name
	,state_code
	,Feeder_branch
	,cra
	,created_on
	,created_by
	,created_reference_id
	,is_indent_required
	,record_status
	,Project_id
	,Bank_code
)
select
@dateH as holiday_date
,'Yes' as is_holiday
,hlist.holiday_code
,hlist.holiday_name
, hstate.State as state_code
,fbm.feeder_branch
,cf.new_cra

, dateadd(mi,330,getutcdate()) as created_on --Get IST datetime
,@Created_by as created_by --From Parameter
,@Created_Reference_Id as created_reference_id -- From parameter
, 'No' as is_indent_required
, 'Active' as record_status
,fbm.project_id
,fbm.bank_code

from holiday_date hdate
inner join holiday_states hstate
on hdate.holiday_code = hstate.holiday_code 
and @dateH  between hdate.start_date and  hdate.end_date
and hdate.record_status='Active' 
and hstate.record_status='Active' 
inner join holiday_list hlist
on hstate.holiday_code = hlist.holiday_code
and hlist.record_status='Active' 
inner join feeder_branch_master fbm
on  fbm.circle = hstate.State
and fbm.record_status = 'Active'
left join 
	(select distinct
				project_id
				,bank_code
				,feeder_branch_code
				,new_cra
		from  cra_feasibility 
		where record_status = 'Active'
	) cf
	on fbm.project_id = cf.project_id
	and fbm.bank_code = cf.bank_code
	and fbm.feeder_branch = cf.feeder_branch_code
   
set @rowcount = @@ROWCOUNT

INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
 values (cast(@rowcount as varchar)+' Records populated for:'+cast(@dateH as varchar), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

end try
begin catch
	SELECT ERROR_MESSAGE() AS ErrorMessage;  
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Error while execution:'+ERROR_MESSAGE(), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	SET @outputVal = (select sequence from app_config_param where category = 'Indent Holiday Validation' and sub_category = 'Holiday_Calculation_Failed')
	SELECT @outputVal
end catch

	SET @outputVal = (select sequence from app_config_param where category = 'Indent Holiday Validation' and sub_category = 'Holiday_Calculation_Success')
	SELECT @outputVal

end 

