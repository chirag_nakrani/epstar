/****** Object:  StoredProcedure [dbo].[uspDataValidationVCB]    Script Date: 26-04-2019 21:53:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspDataValidationVCB]
( 
	@datafor_date_time varchar(50) ,@ForRecordstatus varchar(50),@referenceid varchar(50),@systemUser varchar(50),@Debug BIT = 0
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50)
	DECLARE @errorcode nvarchar(max)
	DECLARE @outputVal varchar(max) 
	DECLARE @datafor varchar(10) = 'VCB'

	--SELECT SESSION_ID from sys.dm_exec_sessions; 

	IF (@Debug = 1)
    BEGIN
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
        PRINT '-- Execution of [dbo].[uspDataValidationVCB] Started.';
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';
    END;

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[uspDataValidationVCB] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

	DECLARE @atmcount int  = (SELECT count(atm_id) from atm_master where record_status = 'Active')
------------------ Check if file is present in Data Update Log---------------

	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[data_update_log] WITH (NOLOCK)
				WHERE [datafor_date_time] = @datafor_date_time AND 
                      data_for_type = @datafor and 
                      record_status = @ForRecordstatus
					  AND created_reference_id = @referenceid
				)
				
		BEGIN

			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Checking in Data Update Log Table ';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			
------------------ Check if file is present in Bank Wise File Table ----------------------

			IF EXISTS( Select 1 as ColumnName
						FROM [dbo].vault_cash_balance WITH (NOLOCK)
						WHERE [datafor_date_time] = @datafor_date_time AND 
							  record_status = @ForRecordstatus 
							  AND created_reference_id = @referenceid
				)
				
				BEGIN
					IF (@Debug = 1)
					BEGIN
					    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
					    PRINT '-- Checking in [dbo].vault_cash_balance  ';
					    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
					END;
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in [dbo].vault_cash_balance', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
---------------- Select total count of records present inb file for particular date and status------------
				
				SET @CountTotal = (	
									SELECT count(1) 
									FROM [dbo].vault_cash_balance WITH (NOLOCK)
						            WHERE [datafor_date_time] = @datafor_date_time 
										  and record_status = @ForRecordstatus 
										  AND created_reference_id = @referenceid					
								   ) 

--------------------- Creating temporary table to process multiple update operations ---------------------------
	
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Creating Temporary Table and storing data ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Creating Temporary Table and storing data', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				DROP TABLE IF EXISTS #temp_vault_cash_balance
				SELECT * 
				INTO #temp_vault_cash_balance 
				FROM [dbo].vault_cash_balance WITH (NOLOCK)
				WHERE datafor_date_time= @datafor_date_time
					  AND record_status = @ForRecordstatus  
					  AND created_reference_id = @referenceid

				DECLARE @vcbcount int = (
											SELECT count(atm_id) 
											from vault_cash_balance 
											where datafor_date_time= @datafor_date_time
											AND record_status = @ForRecordstatus  
											AND created_reference_id = @referenceid
										)
			
----------------------------------------Validation 1--------------------------		
--							Check For ATM status in Master Data				--
------------------------------------------------------------------------------
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Feeder Branch master Started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for VCB ATM id Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				UPDATE vcb
				SET error_code =
						CASE  
						WHEN feeder.feeder_branch is null THEN (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'Master_Exception' and sub_category = 'id_not_present')
						ELSE NULL
						END
				FROM #temp_vault_cash_balance vcb
				LEFT JOIN feeder_branch_master feeder
				ON vcb.feeder_branch_name = feeder.feeder_branch
				AND feeder.record_status = 'Active'


				--UPDATE #temp_vault_cash_balance 
				--	SET     error_code =  
				--	CASE 
				--	    WHEN (    feeder_branch_name IN (
				--	                            SELECT feeder_branch from 
				--								feeder_branch_master where record_status = 'Active'
				--	                         )
				--	         )
				--	    THEN NULL
				--	    ELSE
				--	        (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'Master_Exception' and sub_category = 'id_not_present')
				--	END    

				--select * from #temp_vault_cash_balance
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Feeder Branch master Completed ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for VCB ATM id Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
										  
----------------------------------------Validation 2------------------------------------		
--		           Hopper wise balance should match with the ending balance			  --
----------------------------------------------------------------------------------------	
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Hopper wise balance should match with the ending balance Started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Hopper wise balance should match with the ending balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				UPDATE #temp_vault_cash_balance 
				SET     error_code =  
					  CASE  
					          WHEN vault_balance_100+vault_balance_200+vault_balance_500+vault_balance_2000 <> total_vault_balance 
					          THEN 
					          CASE
					                          WHEN (error_code IS NULL)
					                          THEN    
					                              (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Amount Mismatch')
					                           ELSE 
					                              CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Amount Mismatch')) as varchar(max))
					                      END
					                  ELSE error_code 
					 END

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Hopper wise balance should match with the ending balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
 -----------Updating is_valid_record column as Yes or No-------------

			UPDATE #temp_vault_cash_balance
			SET    is_valid_record = 
					CASE 
						WHEN error_code IS NULL
						THEN 'Yes'
						ELSE 'No'
					END		
			
			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Is Valid Record Field Updated';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Is Valid Record Field Updated', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
-----------------Checking count for valid records in updated temporary table -----------------------------

			SET @countCalculated = (
									SELECT count(1) 
									FROM #temp_vault_cash_balance  
									WHERE is_valid_record = 'Yes' 
									)

-----------------Comparing both the counts ---------------------------------------------------------

				IF(@countCalculated != @CountTotal AND @countCalculated > 0)
					BEGIN
					IF (@Debug = 1)
					BEGIN
					    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
					    PRINT '-- Count Mismatch (Partial Valid File)';
					    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
					END;
			
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'Exception' and sub_category = 'Partial Valid'
									 )	
							
				END
				--ELSE IF(@countCalculated != @CountTotal AND @countCalculated > 0 and @atmcount > @vcbcount)
				--	BEGIN
				--	IF (@Debug = 1)
				--	BEGIN
				--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--	    PRINT '-- Count Mismatch (Partial Valid File) and ATM ID missing in VCB file';
				--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--	END;
			
				--SET @outputVal = (
				--						SELECT																																						
				--						[sequence] from 
				--						[dbo].[app_config_param]												
				--						where category = 'VCB_Validation' and sub_category = 'Partial_missing_id'
				--				)								
				--END
				--ELSE IF(@countCalculated != @CountTotal AND @countCalculated > 0 and @atmcount < @vcbcount)
				--	BEGIN
				--	IF (@Debug = 1)
				--	BEGIN
				--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--	    PRINT '-- Count Mismatch (Partial Valid File) and ATM ID extra in VCB file';
				--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--	END;
			
				--SET @outputVal = (
				--					SELECT																																						
				--					[sequence] from 
				--					[dbo].[app_config_param]												
				--					where category = 'VCB_Validation' and sub_category = 'Partial_extra_id'
				--				)	
				--	END
				ELSE IF (@countCalculated = @CountTotal )
				BEGIN 
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Count Matched (Valid File)';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				SET @outputVal = (
								SELECT sequence from  [dbo].[app_config_param]
								where  category = 'File Operation' and sub_category = 'Data Validation Successful'
							)												
				END
				--ELSE IF (@countCalculated = @CountTotal)
				--BEGIN 
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Count Matched (Valid File)';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				--SET @outputVal = (
				--				SELECT sequence from  [dbo].[app_config_param]
				--				where  category = 'VCB_Validation' and sub_category = 'Valid_extra_id'
				--			)												
				--END
				--ELSE IF (@countCalculated = @CountTotal AND @atmcount > @vcbcount )
				--BEGIN 
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Count Matched (Valid File)';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				--SET @outputVal = (
				--				SELECT sequence from  [dbo].[app_config_param]
				--				where  category = 'VCB_Validation' and sub_category = 'Valid_missing_id'
				--			)												
				--END
			ELSE
				BEGIN
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- No valid record found in table';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
									 )				
			END

------------------------Inserting distinct error code in new temporary table. Also splitting with , ------------------------

			IF OBJECT_ID('tempdb..#tempbankfile') IS NOT NULL
			BEGIN
				DROP TABLE #tempbankfile
			END
			ELSE
			BEGIN
				SELECT DISTINCT VALUE 
				INTO #tempbankfile 
				FROM  #temp_vault_cash_balance
				CROSS APPLY STRING_SPLIT(error_code, ',') 
				
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Temporary Table Created for storing error code';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Temporary Table Created for storing error code', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

--------------------- Creating one single error code string with , seperated delimiter---------------------------------------				
				SET @errorcode = (
								SELECT 
									Stuff((
										SELECT N', ' + VALUE FROM #tempbankfile FOR XML PATH(''),TYPE)
										.value('text()[1]','nvarchar(max)'),1,2,N''
										)
								)
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Error code String Created';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;			
			END			
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Started.... Updating columns in VCB file and data update log', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
			BEGIN TRAN

			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Transaction Started.... Updating columns in bank wise file';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;	


------------------------- Updating columns in bank wise file using temporary table------------------------------------------

				UPDATE vcb 
				SET vcb.is_valid_record =  b.is_valid_record,
					vcb.error_code = b.error_code,
					record_status = 'Approved',	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				FROM vault_cash_balance vcb
					INNER JOIN #temp_vault_cash_balance b
					on b.feeder_branch_name = vcb.feeder_branch_name
					AND b.bank_name = vcb.bank_name
					AND b.cra_name = vcb.cra_name
					AND vcb.[datafor_date_time] = @datafor_date_time 
					AND vcb.record_status = @ForRecordstatus
					AND vcb.created_reference_id = @referenceid
			
			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Columns has been updated in bank wise file';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
		

			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Updating columns in data update log ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;

------------------------- Updating columns in Data Update Log table------------------------------------------
				
				
				UPDATE dbo.data_update_log
				SET is_valid_file =
					CASE	
							WHEN @outputVal in (10002,10003,10004,10005,50009) 
							THEN  1
							WHEN @outputVal = 50001
							THEN 0
							WHEN @outputVal = 10001
							THEN 0
					END,
					validation_code = (SELECT @errorcode),
					record_status = 
					CASE	
							WHEN @outputVal in (50001,10002,10003,10004,10005,50009)
							THEN 'Approved'
							WHEN @outputVal = 10001
							THEN 'Uploaded'
					END,	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				WHERE [datafor_date_time] =  @datafor_date_time
					AND record_status = @ForRecordstatus 
					AND	data_for_type = @datafor
					AND created_reference_id = @referenceid

			COMMIT TRAN;

			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Transaction Completed....Column has been updated in data update log ';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Completed....Column has been updated in data update log and bank wise file', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Execution of [dbo].[uspDataValidationVCB] Completed.';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[uspDataValidationVCB] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			    --DECLARE @tableName VARCHAR(30) = 'dbo.vault_cash_balance'
       --         DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
       --         DECLARE @ToStatus VARCHAR(30) =  'Approved'
       --         DECLARE @SetWhereClause VARCHAR(MAX) = '  datafor_date_time = ''' + 
       --                                         cast(@datafor_date_time as varchar(50)) + ''' and  created_reference_id = ''' + @referenceid + ''''


       --         EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT


       --         DECLARE @tableName_DUL VARCHAR(30) = 'dbo.data_update_log'
       --         DECLARE @ForStatus_DUL VARCHAR(30) = 'Uploaded'
       --         DECLARE @ToStatus_DUL VARCHAR(30) = 'Approved'
       --         DECLARE @SetWhereClause_DUL VARCHAR(MAX) ='datafor_date_time = ''' + 
       --                                             cast(@datafor_date_time as varchar(50)) + ''' and  created_reference_id = ''' + @referenceid + ''' and  data_for_type = '@datafor'' 

       --         EXEC  dbo.[uspSetStatus] @tableName_DUL,@ForStatus_DUL,@ToStatus_DUL,@timestamp_date,@systemUser,@referenceid,@SetWhereClause_DUL,@out OUTPUT
                      SELECT @outputVal

	
		END
			ELSE
			BEGIN
				  RAISERROR (50002, 16,1);
			END
		END
	ELSE
		BEGIN
		--SELECT SESSION_ID from sys.dm_exec_sessions;  
			RAISERROR (	50004, 16,1);
		END
END

