ALTER TABLE indent_master 
ADD	total_closing_vault_balance BIGINT,
closing_vault_balance_50    BIGINT,
closing_vault_balance_100   BIGINT,
closing_vault_balance_200	BIGINT,
closing_vault_balance_500	BIGINT,
closing_vault_balance_2000	BIGINT


ALTER TABLE distribution_planning_master_v2 
ADD	total_closing_vault_balance BIGINT,
closing_vault_balance_50    BIGINT,
closing_vault_balance_100   BIGINT,
closing_vault_balance_200	BIGINT,
closing_vault_balance_500	BIGINT,
closing_vault_balance_2000	BIGINT