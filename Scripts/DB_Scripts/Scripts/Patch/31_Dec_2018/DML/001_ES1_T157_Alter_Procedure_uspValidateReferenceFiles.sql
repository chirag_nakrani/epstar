/****** Object:  StoredProcedure [dbo].[uspValidateReferenceFiles]    Script Date: 29-12-2018 11:37:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspValidateReferenceFiles] 
@file_type varchar(50) ,@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			VALUES ('Execution of [dbo].[uspValidateReferenceFiles] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @procedureName varchar(max) = (SELECT OBJECT_NAME(@@PROCID));
			DECLARE @Result varchar(50)			
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE());
			DECLARE @out1 varchar(100);

			SET @Cmd = CASE 
							WHEN @file_type = 'CRAVAULTMASTER' 
							THEN 'EXEC [uspDataValidationCRAVaultMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'MAILMASTER' 
							THEN 'EXEC [uspDataValidationMailMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'BANKESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationBANKEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'EPSESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationEPSEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'CRAESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationCRAEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'CRAEMPANELED' 
							THEN 'EXEC [uspDataValidationCRAEmpaneled] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'DEFAULTLOADING' 
							THEN 'EXEC [uspDataValidationDefaultLoading] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'BRANDBILLCAPACITY' 
							THEN 'EXEC [uspDataValidationBrandBillCapacity] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
						
							END

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			VALUES ('Execution Dynamic Procedure according to parameters started ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			VALUES ('Execution Dynamic Procedure according to parameters Completed ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			IF(@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence from app_config_param where category = 'CBR_Validation' and sub_category = 'Error')
			END
						
------ Calling Procedure to Update the status 
		            
            IF(@Result = 'S101')
				BEGIN	
					
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
					VALUES ('Status update in data_update_log_master started ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

					DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log_master'
					DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
					DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
					DECLARE @SetWhereClause VARCHAR(MAX) =' data_for_type = ''' + 
													@file_type+''' and created_reference_id = '''+@referenceid+''''

					EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT

					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
					VALUES ('Status update in data_update_log_master completed ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

					SELECT @Result;	
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
 		
		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(MAX) = @procedureName;
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						DECLARE @code varchar(100)
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						
						IF @ErrorNumber = 90001
						BEGIN
							SET @code = 'E101'
						END
						ELSE IF @ErrorNumber = 90002
						BEGIN
							SET @code = 'E102'
						END
						SELECT @code
						
				END
			 END CATCH
END


