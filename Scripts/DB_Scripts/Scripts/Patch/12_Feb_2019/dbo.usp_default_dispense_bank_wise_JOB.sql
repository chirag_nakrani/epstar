/****** Object:  StoredProcedure [dbo].[usp_default_dispense_bank_wise]    Script Date: 25-01-2019 22:07:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter PROCEDURE [dbo].[usp_default_dispense_bank_wise]
(
 @datafor_date_time varchar(100) , @bank varchar(20),@referenceid varchar(20),@current_date varchar(100),@systemuser varchar(30)
 )
AS
begin

insert into cash_dispense_register (bank_name,atm_id,project_id,total_dispense_amount,datafor_date_time,record_status,is_valid_record,created_reference_id,dispense_type,created_on,created_by)

select m.bank_code
              ,m.atm_id
              ,m.project_id
, ROUND((( isnull(day_3.total_dispense_amount,0)+isnull(day_2.total_dispense_amount,0))/2),-2)
 as total_dispense_amount,
cast(@datafor_date_time as datetime) as datafor_date_time,
'Active' as record_status,null as is_valid_record,
@referenceid as created_reference_id,
'Default' as dispense_type,
@current_date as created_on,
@systemuser as created_by
from
atm_master m
left join
(
select date,atm_id,bank_name,total_dispense_amount,reference_id,project_id,
created_on,created_by,record_status,is_valid_record,cast(datafor_date_time as date) datafor_date_time
from cash_dispense_register a
where cast(datafor_date_time as date) = cast(dateadd(day,-2,@datafor_date_time) as date)
and
a.record_status = 'Active'
and a.bank_name = @bank --and a.is_valid_record is null
) day_3
on m.bank_code = day_3.bank_name
and m.atm_id = day_3.atm_id
left join
(
select date,atm_id,bank_name,total_dispense_amount,reference_id,project_id,
created_on,created_by,record_status,is_valid_record,cast(datafor_date_time as date) datafor_date_time
from cash_dispense_register a
where cast(datafor_date_time as date) = cast(dateadd(day,-1,@datafor_date_time) as date)  and a.record_status = 'Active'
and a.bank_name = @bank --and a.is_valid_record is null
) day_2
on m.bank_code = day_2.bank_name
and m.atm_id = day_2.atm_id
where m.site_status = 'Active'
and m.record_status ='Active'
and m.bank_code =@bank
END
