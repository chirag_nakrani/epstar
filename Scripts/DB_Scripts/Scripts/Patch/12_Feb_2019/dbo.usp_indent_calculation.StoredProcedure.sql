
/****** Object:  StoredProcedure [dbo].[usp_indent_calculation]    Script Date: 05-01-2019 21:49:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC [usp_indent_calculation] '2018-11-22','Malay','abc223'

--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: Calculation of Indent for the indentdate @dateT
 --- =========================================================================

alter PROCEDURE [dbo].[usp_indent_calculation]
 (		
        @dateT DATETIME,
		@cur_user nvarchar(50),
		@created_reference_id nvarchar(50)
 )

AS 
BEGIN
	
BEGIN TRY
	-- Temp_variable to calculate indent
	--DECLARE @dateT DATETIME = '2018-12-26'
	declare @timestamp_date datetime =  DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50) 
	DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
	declare @Execution_log_str nvarchar(max)
	DECLARE @outputVal varchar(100)
	declare @feederbranchcode varchar(max)
	--DECLARE @cur_user varchar(50) = 'SA'
	--DECLARE @created_reference_id varchar(40) = '1212'
	
	
	DECLARE @forecast_amt BIGINT
	
	set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@dateT as nvarchar(50)) 
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Execution of [dbo].[usp_indent_calculation_SIT] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		--DECLARE @dateT DATETIME
		--SET     @dateT ='2018-11-05'

		DECLARE @MaxDestinationDate DATE
		DECLARE @dispenseformula NVARCHAR(50)
		DECLARE @confidence_factor NVARCHAR(50)
		DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		DECLARE @denomination_wise_round_off_500 int
		DECLARE @denomination_wise_round_off_2000 int
		DECLARE @denomination_wise_round_off_100 INT
		declare @default_avg_dispense int
		declare @vaulting_for_normal_weekday_percentage float,
				@vaulting_for_normal_weekend_percentage float,
				@vaulting_for_extended_weekend_percentage float
		
		
		Select	@confidence_factor = confidence_factor
		, @dispenseformula = dispenseformula
		, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		,@default_avg_dispense = default_average_dispense
		, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
		, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
		, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
		 from system_settings where record_status = 'Active'

		-- select * from system_settings
	/*******************************************Get Vaulting Percentage*************************************************/

 
	--IF @is_Indent=1
	--BEGIN
	--declare @dateT date
	--SET @dateT ='2018-11-22'
	--Set @MaxDestinationDate=(Select ISNULL(max(for_date),'1900-01-01') from Applied_Vaulting)
  
	/*********************************Calculate Applied vaulting******************************************************/														 
 

		drop table if exists #Applied_Vaulting
		
		Select * 
		into #Applied_Vaulting 
	    from(  
	
			SELECT 
			 @dateT as    indent_date,
						am.project_id,
						am.bank_code,
						am.atm_id,
			          case when cra.vaulting ='Yes'
							 then 
								case when fvpc.is_vaulting_allowed = 'Yes' 
									    then fvpc.vaulting_percentage 
										  else (
										 case  when 
									     --If current day is Friday of 1st, 3rd and 5th Week of month then apply vault % as normal weekday percentage.
											  DATENAME(dw,@dateT) = 'Friday' 							 
											  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
												  in (1,3,5) 
																		then @vaulting_for_normal_weekday_percentage

							 --***********************************************Start Friday as last day of week Check*****************************************************

							 --*****Start Check--->If current day is Friday of 2nd and 4th Week of month then apply vault % as per Extended Check
							 --** Extended Check -->     For weekend considered, if next day after weekend is not holiday then apply vault % as normal weekend percentage.
							                          -- For week end considered, if 1 more day is holiday apart from weekend then apply vault % as extended weekend percentage.
													  -- Using DecideLimitdays toc verify above two conditions

							   when  DATENAME(dw,@dateT) = 'Friday' 							 
								  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
									  in (2,4)
								  and LD.decidelimitdays <3 then @vaulting_for_normal_weekend_percentage

							   when  DATENAME(dw,@dateT) = 'Friday' 							 
								  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
									  in (2,4)
								  and LD.decidelimitdays >=3 then @vaulting_for_extended_weekend_percentage
                              --***********************************************End Friday as last day of week Check*****************************************************


							   --*****Start Check---> If current day is Saturday of 1st,3rd and 5th Week of month then apply vault % as per Extended Check
							   --** Extended Check -->     For weekend considered, if next day after weekend is not holiday then apply vault % as normal weekend percentage.
							                         --  For week end considered, if 1 more day is holiday apart from weekend then apply vault % as extended weekend percentage.
													  -- Using DecideLimitdays toc verify above two conditions
							   when  DATENAME(dw,@dateT) = 'Saturday' 							 
								  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
									  in (1,3,5)
								  and LD.decidelimitdays <=2 then @vaulting_for_normal_weekend_percentage

							   when  DATENAME(dw,@dateT) = 'Saturday' 							 
								  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
									  in (1,3,5)
								  and LD.decidelimitdays >=2 then @vaulting_for_extended_weekend_percentage

                              --***********************************************End saturday as last day of week Check*****************************************************
 
						 end
									 ---*************************old logic*********************************
									--case 
									--	  when DATEPART(dw, @dateT)  NOT IN (1,7)   
									--	  then  @vaulting_for_normal_weekday_percentage -- 50
									--    when  DATEPART(dw, @dateT) IN (1,7)  
									--	  then  @vaulting_for_normal_weekend_percentage --  60
									--    else  @vaulting_for_extended_weekend_percentage --   70 
									--end
									 ---*************************old logic*********************************
				  )
				
				end 
				 
			    end applied_vaulting_percentage
			  
	 
	         	 	 
		         from atm_master am
				 inner join cra_feasibility cra
				 on cra.site_code=am.site_code 
				and cra.atm_id=am.atm_id
				and am.record_status ='Active'
				and am.site_status = 'Active'
				AND cra.record_status = 'Active'
				
		
				 left join  Feeder_Vaulting_pre_config fvpc
				 on cra.feeder_branch_code=fvpc.feeder_branch_code
					and cra.project_id=fvpc.project_id
					and cra.bank_code=fvpc.bank_code
				    and fvpc.record_status='Active'
				

				 left join limitdays LD
				 on am.atm_id=LD.atm_id
				 and am.bank_code=LD.bank_code
				 and am.project_id=LD.project_id
				 and cast(ld.indent_date as date)=  @dateT   
				 and LD.record_status='Active'
				  
				  
				 )d 
				 
				 where d.applied_vaulting_percentage is not null
  
	
  /******************************************Calculate Loading amount************************************************************************************/
        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
         values ('Insert into #pre_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #pre_loading_amount Started'
	
     DROP TABLE IF exists #pre_loading_amount 	
	
		SELECT 
			atm_id,
			site_code,
			sol_id,
			bank_code,
			project_id,
			indentdate,
			bank_cash_limit,
			insurance_limit	,
			feeder_branch_code,
			CASE 
					WHEN atm_band = 'Platinum'
					THEN 1
					WHEN atm_band = 'Gold'
					THEN 2
					WHEN CashOut = 1
					THEN 3
					WHEN atm_band = 'Silver'
					THEN 4
					ELSE 5
				END as atm_priority, 
			CASE 
				WHEN  bank_cash_limit< insurance_limit 
					THEN bank_cash_limit
				ELSE insurance_limit 
			END	AS limit_amount,
			max_of_bankcash_insurancelimit, --Correction base limit
			base_limit,
			dispenseformula,
			decidelimitdays,
			loadinglimitdays,
			avgdispense,
			cassette_50_count,
			cassette_100_count,
			cassette_200_count,
			cassette_500_count,
			cassette_2000_count,
			capacity_50,
			capacity_100,
			capacity_200,
			capacity_500,
			capacity_2000,
			total_capacity_amount_50,
			total_capacity_amount_100,
			total_capacity_amount_200,
			total_capacity_amount_500,
			total_capacity_amount_2000,
			total_cassette_capacity,
			avgdecidelimit,
			DecideLimit,
			threshold_limit,			
			loadinglimit, 
			applied_vaulting_percentage,
			ignore_code,
			ignore_description, 
			dist_Purpose,
			defaultamt,	
			default_flag,		
			remaining_balance_50,
			remaining_balance_100,
			remaining_balance_200,
			remaining_balance_500,
			remaining_balance_2000,
			Morning_balance_T_minus_1,
			curbal_div_avgDisp,
			loadingGap_cur_bal_avg_disp,
			CashOut,		               
             loadingamount,
			 denomination_100_max_capacity_percentage,
			 denomination_200_max_capacity_percentage,
			 denomination_500_max_capacity_percentage,
			 denomination_2000_max_capacity_percentage,
			cassette_50_count   as cassette_50_count_original  ,
			cassette_100_count  as cassette_100_count_original ,
			cassette_200_count  as cassette_200_count_original ,
			cassette_500_count  as cassette_500_count_original ,
			cassette_2000_count as cassette_2000_count_original,
			deno_100_priority,
			deno_200_priority,
			deno_500_priority,
			deno_2000_priority
		 
		 --drop table if exists #pre_loading_amount 
			INTO #pre_loading_amount
			FROM
			(
			Select 
			atm_id
			,site_code
			,sol_id
			,bank_code
			,project_id
			,indentdate
			,bank_cash_limit
			,insurance_limit
			,feeder_branch_code
			,atm_band
			,max_of_bankcash_insurancelimit
			,base_limit
			,dispenseformula
			,decidelimitdays
			,loadinglimitdays
			,avgdispense
			,cassette_50_count
			,cassette_100_count
			,cassette_200_count
			,cassette_500_count
			,cassette_2000_count
			,capacity_50
			,capacity_100
			,capacity_200
			,capacity_500
			,capacity_2000
			,total_capacity_amount_50
			,total_capacity_amount_100
			,total_capacity_amount_200
			,total_capacity_amount_500
			,total_capacity_amount_2000
			,total_cassette_capacity
			,avgdecidelimit
			,DecideLimit
			,threshold_limit
			,loadinglimit
			,applied_vaulting_percentage
			,ignore_code
			,ignore_description
			,dist_Purpose
			,defaultamt
			,case when Morning_balance_T_minus_1 is NULL then 1 
										else 0 end 
										as default_flag  -- Flag for default loading
			,remaining_balance_50
			,remaining_balance_100
			,remaining_balance_200
			,remaining_balance_500
			,remaining_balance_2000
			,Morning_balance_T_minus_1
			,curbal_div_avgDisp
			,loadingGap_cur_bal_avg_disp
			,CashOut			
      --      ,CASE when ignore_code IS NOT NULL THEN 0   -- calculating loading amount on ATM level 
      --         ELSE		     
			   --   CASE WHEN Morning_balance_T_minus_1 is NULL  then defaultamt
						--  ELSE
						--	(CASE 
						--		WHEN insurance_limit  < 
						--				CASE 
						--					WHEN  Morning_balance_T_minus_1 < DecideLimit
						--						THEN (loadinglimit-Morning_balance_T_minus_1) + 	
						--						 (  cast(@confidence_factor as float)/cast(100 as float))
						--															 * avgdispense 	
						--						ELSE  0 --DecideLimit
						--		 END 
						--		THEN insurance_limit 
						--	ELSE 
						--		CASE 
						--			WHEN  Morning_balance_T_minus_1 < DecideLimit
						--				THEN (loadinglimit- Morning_balance_T_minus_1)  + 
						--					 (  cast(@confidence_factor as float)/cast(100 as float))* avgdispense 													
						--                ELSE  0 --DecideLimit 
						--				END 
						--END ) END END  loadingamount ,
-- Correction loading Amount
            ,CASE when ignore_code IS NOT NULL THEN 0   -- calculating loading amount on ATM level 
                WHEN Morning_balance_T_minus_1 is NULL or Morning_balance_T_minus_1 = 0 then 
						defaultamt
				WHEN  Morning_balance_T_minus_1 < DecideLimit THEN
						ceiling((loadinglimit- Morning_balance_T_minus_1  + 
											 ((  cast(@confidence_factor as float)/cast(100 as float))* avgdispense))*1.0/100000)*100000
               ELSE		     
						0 
				END as loadingamount ,

						denomination_100_max_capacity_percentage,
						denomination_200_max_capacity_percentage,
						denomination_500_max_capacity_percentage,
						denomination_2000_max_capacity_percentage,
						deno_100_priority,
						deno_200_priority,
						deno_500_priority,
						deno_2000_priority
	 
            FROM 
              (
					SELECT b.*,
					AV.applied_vaulting_percentage,
				case    when qa.is_qualified = 'Disqualified'  then 20001  
						when fault_description is NOT NULL then 20003
						when OPS.feeder_branch_code is NOT NULL then 20002 						 
						else NULL
						end ignore_code,  -- putting code for disqualified, faulty,start/stop  cases

				case when qa.is_qualified = 'Disqualified'  then 'Disqualified'  
				when fault_description is not NULL then  fault_description 
				 else NULL
				end ignore_description,   

				case when eod.atm_id is not null then  'EOD' 
				      else NULL
					  end dist_Purpose,     

					  --Case when mb.Morning_balance_T_minus_1 is NULL then DLL.amount 														
					  --else 0 end  defaultamt,
					DLL.amount  as defaultamt,				
					mb.remaining_balance_50,
					mb.remaining_balance_100,
					mb.remaining_balance_200,
					mb.remaining_balance_500,
					mb.remaining_balance_2000,
					mb.Morning_balance_T_minus_1,
					ROUND((CAST(Morning_balance_T_minus_1 AS FLOAT)/ CAST(avgdispense  AS FLOAT) ),1) AS curbal_div_avgDisp,
					(decidelimitdays- (Morning_balance_T_minus_1/avgdispense)) * avgdispense AS loadingGap_cur_bal_avg_disp,
					CASE 
						WHEN Morning_balance_T_minus_1 <10000 
						THEN 1
						ELSE 0 
					END CashOut

					FROM 
					(
						SELECT a.*,avgdispense*decidelimitdays AS avgdecidelimit, 
						--Correction Decide limit
						--CASE 
						--	WHEN (
						--		CASE 
						--			WHEN base_limit > (avgdispense*decidelimitdays) 
						--				THEN base_limit
						--				ELSE  avgdispense*decidelimitdays END) > total_cassette_capacity 
						--		THEN  total_cassette_capacity
						--		ELSE 
						--		( CASE 
						--			WHEN base_limit > (avgdispense*decidelimitdays) 
						--			THEN base_limit
						--			ELSE  avgdispense*decidelimitdays END)
						--	END DecideLimit, 

							(select min (v) from (values
									(ceiling(case when (avgdispense * decidelimitdays) > base_limit then
								case when total_cassette_capacity < (avgdispense * decidelimitdays) then
									total_cassette_capacity
									else
										(avgdispense * decidelimitdays)
								end
								else
									base_limit
							end * 1.0 / 100000)*100000),
							(insurance_limit)) As i(v))
							as
							 DecideLimit,
					
					 avgdispense*loadinglimitdays  AS threshold_limit ,
					 --Correction loading limit
					 --CASE 
						--WHEN (
						--	 CASE 
						--		WHEN  base_limit >  (avgdispense*loadinglimitdays)    
						--			THEN  base_limit 
						--			ELSE  avgdispense*loadinglimitdays   
						--		END ) > total_cassette_capacity 
						--	THEN total_cassette_capacity
						--		ELSE  
						--			( CASE 
						--				WHEN  base_limit >  (avgdispense*loadinglimitdays)    
						--					THEN  base_limit 
						--					ELSE  avgdispense*loadinglimitdays   
						--				END )
					 --	  END loadinglimit					
					 
					 (select min (v) from (values
									(ceiling(case when (avgdispense * loadinglimitdays) > base_limit then
								case when total_cassette_capacity < (avgdispense * loadinglimitdays) then
									total_cassette_capacity
									else
										(avgdispense * loadinglimitdays)
								end
								else
									base_limit
							end * 1.0 / 100000)*100000),
							(insurance_limit)) As i(v))
							as
					  loadinglimit
 					  FROM  
					  (    
					  SELECT mast.atm_id,mast.site_code,
							 sol_id,
							 mast.bank_code,
							 mast.project_id,
							 @dateT AS indentdate,
							 clm.bank_cash_limit,
							 clm.insurance_limit,
							 cra.feeder_branch_code as feeder_branch_code,
							 mast.atm_band,
							 CASE 
								WHEN clm.bank_cash_limit <  clm.insurance_limit 
									THEN clm.bank_cash_limit
									ELSE clm.insurance_limit 
								END AS  max_of_bankcash_insurancelimit,
							 clm.base_limit,
							 @dispenseformula AS dispenseformula ,
							 dl.[decidelimitdays],
							 dl.loadinglimitdays,
							COALESCE( CASE 
								WHEN @dispenseformula='max_of_max'    
									THEN cd.max_of_max		
								WHEN @dispenseformula='avg_of_max'    
									THEN cd.avg_of_max  
								WHEN @dispenseformula='max_of_avg'    
									THEN cd.max_of_avg  
								WHEN @dispenseformula='avg_of_avg'    
									THEN cd.avg_of_avg  
								WHEN @dispenseformula='max_of_secondmax'    
									THEN cd.max_of_secondmax
								WHEN @dispenseformula='avg_of_secondmax'    
									THEN cd.avg_of_secondmax
								WHEN @dispenseformula='max_of_thirdmax'    
									THEN cd.max_of_thirdmax
								WHEN @dispenseformula='avg_of_thirdmax'    
									THEN cd.avg_of_thirdmax
								WHEN @dispenseformula='_28days_or_T2dispense'    
									THEN  cd._28days_or_T2dispense
							  -----9-Jan-2019 Added as per requirement for dispense calculation
								WHEN @dispenseformula='max_of_avg_current'
								    THEN  cd.max_of_current
							END,  @default_avg_dispense) avgdispense
					          , COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) AS cassette_50_count
					         , COALESCE(cc.cassette_100_count, clm.cassette_100_count,0) AS cassette_100_count
					         , COALESCE(cc.cassette_200_count, clm.cassette_200_count,0) AS cassette_200_count
					         , COALESCE(cc.cassette_500_count, clm.cassette_500_count,0) AS cassette_500_count
					         , COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0) AS cassette_2000_count  
						     ,COALESCE(bc.capacity_50,2000) as capacity_50
						     ,COALESCE(bc.capacity_100, sa.deno_100_bill_capacity) as capacity_100
						     ,COALESCE(bc.capacity_200,sa.deno_200_bill_capacity) as capacity_200
						     ,COALESCE(bc.capacity_500,sa.deno_500_bill_capacity) as capacity_500
						     ,COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity) as capacity_2000
						     ,COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) * COALESCE(bc.capacity_50,2000) *50 AS total_capacity_amount_50
						     ,COALESCE(cc.cassette_100_count, clm.cassette_100_count,0) * COALESCE(bc.capacity_100,sa.deno_100_bill_capacity) *100 AS total_capacity_amount_100
						     ,COALESCE(cc.cassette_200_count, clm.cassette_200_count,0) * COALESCE(bc.capacity_200,sa.deno_200_bill_capacity) *200 AS total_capacity_amount_200
						     ,COALESCE(cc.cassette_500_count, clm.cassette_500_count,0) *COALESCE(bc.capacity_500,sa.deno_500_bill_capacity)*500 AS total_capacity_amount_500
						     ,COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0) * COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity) *2000 AS total_capacity_amount_2000
						     
						     ,(COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) * COALESCE(bc.capacity_50,2000) *50) +
					          (COALESCE(cc.cassette_100_count, clm.cassette_100_count,0) *COALESCE(bc.capacity_100,sa.deno_100_bill_capacity)  *100) +
						      (COALESCE(cc.cassette_200_count, clm.cassette_200_count,0) *COALESCE(bc.capacity_200,sa.deno_200_bill_capacity) *200)+	   
						      (COALESCE(cc.cassette_500_count, clm.cassette_500_count,0) * COALESCE(bc.capacity_500,sa.deno_500_bill_capacity) *500) +
						      (COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0) * COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity) *2000)
							  AS total_cassette_capacity,
							  COALESCE(max_cap.denomination_100,sa.deno_100_max_capacity_percentage) as   denomination_100_max_capacity_percentage,
							  COALESCE(max_cap.denomination_200,sa.deno_200_max_capacity_percentage) as   denomination_200_max_capacity_percentage,
							  COALESCE(max_cap.denomination_500,sa.deno_500_max_capacity_percentage) as   denomination_500_max_capacity_percentage,
							  COALESCE(max_cap.denomination_2000,sa.deno_2000_max_capacity_percentage) as denomination_2000_max_capacity_percentage,
							  COALESCE(fdp.denomination_100,sa.deno_100_priority) as   deno_100_priority,
							  COALESCE(fdp.denomination_200,sa.deno_200_priority) as   deno_200_priority,
							  COALESCE(fdp.denomination_500,sa.deno_500_priority) as   deno_500_priority,
							  COALESCE(fdp.denomination_2000,sa.deno_2000_priority) as deno_2000_priority
							  


							  --DECLARE @dateT DATETIME
							  --SET @dateT ='2018-11-11'
							  
							  --SELECT mast.* 
							  FROM  atm_master AS  mast
							  inner JOIN		ATM_Config_limits clm
							  ON			mast.atm_id = clm.atm_id  AND
											mast.site_code = clm.site_code AND
											clm.record_status = 'Active'
							  AND mast.record_status = 'Active'
							  and mast.site_status = 'Active'
							  inner JOIN cra_feasibility cra on
								mast.site_code = cra.site_code
								AND mast.atm_id = cra.atm_id AND
								cra.record_status = 'Active'

							  LEFT JOIN 	calculated_dispense cd
								ON mast.atm_id=cd.atm_id AND 
								mast.project_id = cd.project_id AND
								mast.bank_code = cd.bank AND
								CAST(cd.datafor_date_time as date) = cast(@dateT as date)
								and cd.record_status  = 'Active'
							  
							  LEFT JOIN		limitdays dl
									  ON mast.atm_id=dl.atm_id AND 
									mast.bank_code=dl.bank_code AND 
									mast.project_id=dl.project_id AND
									CAST(dl.indent_date as date) = cast(@dateT as date) AND
									dl.record_status = 'Active'	
					
							  LEFT JOIN		
										feeder_cassette_max_capacity_percentage max_cap
										ON	max_cap.bank_code = mast.bank_code AND
											max_cap.project_id = mast.project_id AND
											max_cap.feeder_branch_code = cra.feeder_branch_code
											AND max_cap.record_status = 'Active'							 
							  LEFT JOIN   feeder_branch_master fb on
											cra.bank_code = fb.bank_code
											AND cra.feeder_branch_code = fb.feeder_branch 
											AND fb.project_id = cra.project_id 
											AND fb.record_status = 'Active'
							  LEFT JOIN [feeder_denomination_priority] fdp on
											 fdp.bank_code = mast.bank_code AND
											 fdp.feeder_branch_code = cra.feeder_branch_code AND
											 fdp.project_id = mast.project_id

							 CROSS JOIN system_settings sa

							  LEFT JOIN      
							       (
							       SELECT 
										project_id,
										bank_code, 
										site_code,
										atm_id, 
										cassette_50_count, 
										cassette_100_count, 
										cassette_200_count,
										cassette_500_count,
										cassette_2000_count 
									FROM Modify_cassette_pre_config 
									WHERE @dateT between from_date AND to_date
							        AND record_status = 'Active'
							       )cc
							  ON clm.site_code = cc.site_code AND 
								 clm.atm_id = cc.atm_id
								 LEFT JOIN Brand_Bill_Capacity bc ON 
										   bc.brand_code = mast.brand 
								           AND bc.record_status = 'Active'
										WHERE mast.record_status = 'Active'

								
							       )a
					 )b 

					 	LEFT JOIN 
					morningbalance mb
					ON b.atm_id=mb.atm_id
					and b.bank_code = mb.bank_name
					and mb.record_status = 'Active'
					and mb.datafor_date_time = @dateT

					LEFT Join #Applied_Vaulting AV
					on  b.atm_id=AV.atm_id
					and b.project_id=AV.project_id
					and b.bank_code= AV.bank_code

					LEFT join Indent_Pre_Qualify_ATM  QA
					 on   b.atm_id=QA.atm_id
					 and b.site_code = QA.site_code					 
					 and   QA.record_status='Active'					 
					 and @dateT between QA.from_date and QA.to_date

					LEFT join	(
								SELECT atmid,site_code,
										STUFF((
											  SELECT ',' + T.fault_description
											  FROM dbo.ims_master T
											  WHERE A.atmid = T.atmid
											  FOR XML PATH('')), 1, 1, '') as fault_description 
										 from ims_master A
								where fault_description not in ('Any Cassette Faulty')
								and record_status='Active'
								group by atmid,site_code
								
								) im
					on b.atm_id=im.atmid  and b.site_code = im.site_code					

				    LEFT join [indent_eod_pre_activity] EOD
					on b.atm_id=EOD.atm_id
					and b.bank_code=EOD.bank_code
					and b.project_id=EOD.project_id
					and EOD.record_status='Active'

					left join ops_stop_batch OPS
					on b.feeder_branch_code=OPS.feeder_branch_code
					and  b.project_id=OPS.project_id
					and b.bank_code=OPS.bank_code
					and OPS.record_status='Active'
					and @dateT between cast(OPS.from_date as date) and cast(OPS.to_date as date)

					left join Default_loading DLL
					on b.project_id=DLL.project_id
					and b.bank_code=DLL.bank_code
					and DLL.record_status='Active'
			
				) c
								 
		            ) d
					where bank_code = 'BOMH'
      INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Fetching data from tables and insert into #pre_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	  
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Fetching data from tables and insert into #pre_loading_amount Completed'
	
	 --select project_id,bank_code,atm_id, count(*) from #pre_loading_amount group by project_id,bank_code,atm_id
	 --having count(*) > 1

	 --delete from calculated_dispense where id = 13729 atm_id = 'NA1400C1'
 /****************************  Rounding of loading amount /negativebalance  ************************************/



	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Start prepare dataset  to handle negative amount, Insert into #loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Start prepare dataset  to handle negative amount, Insert into #loading_amount Started'

	DROP TABLE IF exists #loading_amount   
	SELECT [atm_id],
			site_code,
			sol_id,
			bank_code,
			project_id,
			indentdate,
			bank_cash_limit,
			insurance_limit	,
			feeder_branch_code, 
			atm_priority,
			base_limit,
			dispenseformula,
			 decidelimitdays,
			 loadinglimitdays,
			 avgdispense
			,COALESCE([loadingamount],0)				AS loading_amount
			,COALESCE(remaining_balance_50,0)			AS [morning_balance_50]
			,COALESCE(remaining_balance_100,0)			AS morning_balance_100
			,COALESCE(remaining_balance_200,0)			AS morning_balance_200
			,COALESCE(remaining_balance_500,0)			AS morning_balance_500
			,COALESCE(remaining_balance_2000,0)			AS morning_balance_2000
			,COALESCE(morning_balance_T_minus_1,0)		AS total_morning_balance
			,COALESCE(capacity_50,0)					AS cassette_50_brand_capacity
			,COALESCE(capacity_100,0)					AS cassette_100_brand_capacity
			,COALESCE(capacity_200,0)					AS cassette_200_brand_capacity
			,COALESCE(capacity_500,0)					AS cassette_500_brand_capacity
			,COALESCE(capacity_2000,0)					AS cassette_2000_brand_capacity
			,COALESCE(cassette_50_count ,0)				AS cassette_50_count
			,COALESCE(cassette_100_count,0)				AS cassette_100_count
			,COALESCE(cassette_200_count,0)				AS cassette_200_count
			,COALESCE(cassette_500_count,0)				AS cassette_500_count
			,COALESCE(cassette_2000_count,0)			AS cassette_2000_count 
			,COALESCE(limit_amount,0)					AS limit_amount 
			,total_capacity_amount_50
			,total_capacity_amount_100
			,total_capacity_amount_200
			,total_capacity_amount_500
			,total_capacity_amount_2000
			,total_cassette_capacity
			,avgdecidelimit
			,DecideLimit
			,threshold_limit
			,loadinglimit
			,applied_vaulting_percentage
			,ignore_code
			,ignore_description 
			,dist_Purpose
			,defaultamt
			,default_flag
			,curbal_div_avgDisp
			,loadingGap_cur_bal_avg_disp
			,CashOut
			,remaining_balance_50
			,remaining_balance_100
			,remaining_balance_200
			,remaining_balance_500
			,remaining_balance_2000
			,denomination_100_max_capacity_percentage
			,denomination_200_max_capacity_percentage
			,denomination_500_max_capacity_percentage
			,denomination_2000_max_capacity_percentage
			,cassette_50_count_original  
			,cassette_100_count_original 
			,cassette_200_count_original 
			,cassette_500_count_original 
			,cassette_2000_count_original,
			deno_100_priority,
			deno_200_priority,
			deno_500_priority,
			deno_2000_priority,
			0 as variation_amount
			-- For correction of distribution amount
			,case when floor((total_capacity_amount_100-COALESCE(remaining_balance_100,0))*1.0/@denomination_wise_round_off_100)*@denomination_wise_round_off_100 > 0 then 
			 floor((total_capacity_amount_100-COALESCE(remaining_balance_100,0))*1.0/@denomination_wise_round_off_100)*@denomination_wise_round_off_100
			else
			0 end as max_loading_capacity_amount_100
			, case when floor((total_capacity_amount_200-COALESCE(remaining_balance_200,0))*1.0/@denomination_wise_round_off_200)*@denomination_wise_round_off_200 > 0 then 
			 floor((total_capacity_amount_200-COALESCE(remaining_balance_200,0))*1.0/@denomination_wise_round_off_200)*@denomination_wise_round_off_200
			else
			0 end as max_loading_capacity_amount_200
			, case when floor((total_capacity_amount_500-COALESCE(remaining_balance_500,0))*1.0/@denomination_wise_round_off_500)*@denomination_wise_round_off_500 > 0 then 
			 floor((total_capacity_amount_500-COALESCE(remaining_balance_500,0))*1.0/@denomination_wise_round_off_500)*@denomination_wise_round_off_500
			else
			0 end as max_loading_capacity_amount_500
			, case when floor((total_capacity_amount_2000-COALESCE(remaining_balance_2000,0))*1.0/@denomination_wise_round_off_2000)*@denomination_wise_round_off_2000 > 0 then 
			 floor((total_capacity_amount_2000-COALESCE(remaining_balance_2000,0))*1.0/@denomination_wise_round_off_2000)*@denomination_wise_round_off_2000
			else
			0 end as max_loading_capacity_amount_2000
			,0 as total_loading_capacity_amount
			,loadingamount as loading_amount_original
			INTO #loading_amount
			FROM #pre_loading_amount
			-- Update total capacity
			update #loading_amount set total_loading_capacity_amount = max_loading_capacity_amount_100 + max_loading_capacity_amount_200 + max_loading_capacity_amount_500 + max_loading_capacity_amount_2000
			--If loading amount is less than capacity then reduce loading amount
			update #loading_amount set loading_amount = total_loading_capacity_amount where loading_amount > total_loading_capacity_amount

			--SELECT * FROM #loading_amount
      INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Insert into #loading_amount completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #loading_amount completed'
		DECLARE @ITERATION AS INT
		SET @ITERATION = 0
		WHILE 1=1
			BEGIN
				
		
				SET @ITERATION = @ITERATION + 1
				-- Calculate Cassette capacity amount
	
				DROP TABLE IF exists #cassette_capacity_amount

				
      INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Insert into #cassette_capacity_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #cassette_capacity_amount Started'

				SELECT * 
					,[cassette_50_brand_capacity]*[cassette_50_count]*50       AS cassette_50_capacity_amount
					,[cassette_100_brand_capacity]*[cassette_100_count]*100    AS cassette_100_capacity_amount
					,[cassette_200_brand_capacity]*[cassette_200_count]*200    AS cassette_200_capacity_amount
					,[cassette_500_brand_capacity]*[cassette_500_count]*500    AS cassette_500_capacity_amount
					,[cassette_2000_brand_capacity]*[cassette_2000_count]*2000 AS cassette_2000_capacity_amount
					,([cassette_50_brand_capacity]*[cassette_50_count]*50) + 
					 ([cassette_100_brand_capacity]*[cassette_100_count]*100) +
					 ([cassette_200_brand_capacity]*[cassette_200_count]*200) +
					 ([cassette_500_brand_capacity]*[cassette_500_count]*500) +
					 ([cassette_2000_brand_capacity]*[cassette_2000_count]*2000)AS total_cassette_capacity_amount
				 INTO #cassette_capacity_amount	  
				 FROM #loading_amount
				 --BREAK;

	
	--SELECT * FROM #cassette_capacity_amount

		
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #cassette_capacity_amount Started'
			  
			   INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #cassette_capacity_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
			DROP TABLE IF EXISTS #cassette_capacity_percentage
	
			-- Calculate Cassette capacity percentage
/* Update logic
			SELECT *,
				COALESCE(cassette_50_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)     AS cassette_capacity_percentage_50
				,COALESCE(cassette_100_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)   AS cassette_capacity_percentage_100
				,COALESCE(cassette_200_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)   AS cassette_capacity_percentage_200
				,COALESCE(cassette_500_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)   AS cassette_capacity_percentage_500
				,COALESCE(cassette_2000_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)  AS cassette_capacity_percentage_2000
			INTO #cassette_capacity_percentage
			FROM #cassette_capacity_amount
*/
			SELECT *,
				0     AS cassette_capacity_percentage_50
				,case when max_loading_capacity_amount_100 >0 then COALESCE(max_loading_capacity_amount_100 * 100.0 /isnull(total_loading_capacity_amount,0) ,0) else 0 end  AS cassette_capacity_percentage_100
				,case when max_loading_capacity_amount_200 >0 then COALESCE(max_loading_capacity_amount_200 * 100.0 /isnull(total_loading_capacity_amount,0) ,0) else 0 end  AS cassette_capacity_percentage_200
				,case when max_loading_capacity_amount_500 >0 then COALESCE(max_loading_capacity_amount_500 * 100.0 /isnull(total_loading_capacity_amount,0) ,0) else 0 end  AS cassette_capacity_percentage_500
				,case when max_loading_capacity_amount_2000 >0 then COALESCE(max_loading_capacity_amount_2000 * 100.0 /isnull(total_loading_capacity_amount,0) ,0) else 0 end AS cassette_capacity_percentage_2000
			INTO #cassette_capacity_percentage
			FROM #cassette_capacity_amount
			
			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #cassette_capacity_percentage Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #cassette_capacity_amount Completed'
	
			--SELECT * FROM #cassette_capacity_percentage

			DROP TABLE IF exists #cassette_capacity_amount
			DROP TABLE IF exists #tentative_loading_amount			

				set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #tentative_loading_amount Started'
	
			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #tentative_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
/*	Update logic
			SELECT *
				,CASE WHEN cassette_capacity_percentage_50 > 0 and loading_amount >0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_50/100 ) -morning_balance_50 - variation_amount ELSE 0 END AS tentative_loading_amount_50
				,CASE WHEN cassette_capacity_percentage_100 > 0  and loading_amount >0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_100/100 ) -morning_balance_100 - variation_amount  ELSE 0 END AS tentative_loading_amount_100
				,CASE WHEN cassette_capacity_percentage_200 > 0  and loading_amount >0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_200/100 ) -morning_balance_200 - variation_amount ELSE 0 END AS tentative_loading_amount_200
				,CASE WHEN cassette_capacity_percentage_500 > 0  and loading_amount >0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_500/100 ) -morning_balance_500 - variation_amount  ELSE 0 END AS tentative_loading_amount_500
				,CASE WHEN cassette_capacity_percentage_2000 > 0  and loading_amount >0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_2000/100 ) -morning_balance_2000 - variation_amount ELSE 0 END AS tentative_loading_amount_2000
			--,@ITERATION AS iterate_counter
			INTO #tentative_loading_amount
			FROM #cassette_capacity_percentage
*/
			SELECT *
				,CASE WHEN cassette_capacity_percentage_50 > 0 and loading_amount >0 THEN ( ( loading_amount) * cassette_capacity_percentage_50/100 )  - variation_amount ELSE 0 END AS tentative_loading_amount_50
				,CASE WHEN cassette_capacity_percentage_100 > 0  and loading_amount >0 THEN ( ( loading_amount) * cassette_capacity_percentage_100/100 )  - variation_amount  ELSE 0 END AS tentative_loading_amount_100
				,CASE WHEN cassette_capacity_percentage_200 > 0  and loading_amount >0 THEN ( ( loading_amount) * cassette_capacity_percentage_200/100 )  - variation_amount ELSE 0 END AS tentative_loading_amount_200
				,CASE WHEN cassette_capacity_percentage_500 > 0  and loading_amount >0 THEN ( ( loading_amount) * cassette_capacity_percentage_500/100 )  - variation_amount  ELSE 0 END AS tentative_loading_amount_500
				,CASE WHEN cassette_capacity_percentage_2000 > 0  and loading_amount >0 THEN ( ( loading_amount) * cassette_capacity_percentage_2000/100 )  - variation_amount ELSE 0 END AS tentative_loading_amount_2000
			--,@ITERATION AS iterate_counter
			INTO #tentative_loading_amount
			FROM #cassette_capacity_percentage			
			--BREAK;
			--END
			--SELECT * FROM #tentative_loading_amount

			
				set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #tentative_loading_amount completed'


			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #tentative_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

			DROP TABLE IF EXISTS #cassette_capacity_percentage			

			IF exists(
				SELECT 1 FROM #tentative_loading_amount 
				WHERE tentative_loading_amount_50 < 0 OR
				 tentative_loading_amount_100 <0 OR 
				 tentative_loading_amount_200 <0 OR 
				 tentative_loading_amount_500 <0 OR 
				 tentative_loading_amount_2000 <0
				)
			BEGIN

				TRUNCATE TABLE #loading_amount				
				
		   INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Insert into #loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
				set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #loading_amount Started'

				INSERT INTO #loading_amount
				SELECT
					[atm_id],
					site_code,					
					sol_id,
					bank_code,
					project_id,
					indentdate,
					bank_cash_limit,
					insurance_limit	,
					feeder_branch_code, 
					atm_priority,
					base_limit,
					dispenseformula,
					 decidelimitdays,
					 loadinglimitdays,
					 avgdispense
					,loading_amount
					,morning_balance_50
					,morning_balance_100
					,morning_balance_200
					,morning_balance_500	
					,morning_balance_2000
					,total_morning_balance
					,cassette_50_brand_capacity
					,cassette_100_brand_capacity
					,cassette_200_brand_capacity
					,cassette_500_brand_capacity
					,cassette_2000_brand_capacity
				  ,CASE WHEN tentative_loading_amount_50 <0 THEN 0 	ELSE  cassette_50_count END AS cassette_50_count
				,CASE WHEN tentative_loading_amount_100 <0 THEN 0 	ELSE  cassette_100_count END AS cassette_100_count
				,CASE WHEN tentative_loading_amount_200 <0 THEN 0 	ELSE  cassette_200_count END AS cassette_200_count
				,CASE WHEN tentative_loading_amount_500 <0 THEN 0 	ELSE  cassette_500_count END AS cassette_500_count
				,CASE WHEN tentative_loading_amount_2000 <0 THEN 0 	ELSE  cassette_2000_count END AS cassette_2000_count				 
				  ,limit_amount
				  ,total_capacity_amount_50
				  ,total_capacity_amount_100
				  ,total_capacity_amount_200
				  ,total_capacity_amount_500
				  ,total_capacity_amount_2000
				  ,total_cassette_capacity
				  ,avgdecidelimit
				  ,DecideLimit
				  ,threshold_limit
				  ,loadinglimit
				  ,applied_vaulting_percentage
				  ,ignore_code
			      ,ignore_description 
				  ,dist_Purpose
				  ,defaultamt
				  ,default_flag
				  ,curbal_div_avgDisp
				  ,loadingGap_cur_bal_avg_disp
				  ,CashOut		
				  ,remaining_balance_50
				  ,remaining_balance_100
				  ,remaining_balance_200
				  ,remaining_balance_500
				  ,remaining_balance_2000
				   ,denomination_100_max_capacity_percentage
				,denomination_200_max_capacity_percentage
				,denomination_500_max_capacity_percentage
				,denomination_2000_max_capacity_percentage	
				,cassette_50_count_original  
				,cassette_100_count_original 
				,cassette_200_count_original 
				,cassette_500_count_original 
				,cassette_2000_count_original	
				,deno_100_priority,
				 deno_200_priority,
				 deno_500_priority,
				 deno_2000_priority
				 
				 , ISNULL(CASE WHEN tentative_loading_amount_50 <0 THEN morning_balance_50 END,0) + 
				 ISNULL(CASE WHEN tentative_loading_amount_100 <0 THEN morning_balance_100 END,0) +
				 ISNULL(CASE WHEN tentative_loading_amount_200 <0 THEN morning_balance_200 END,0) +
				 ISNULL(CASE WHEN tentative_loading_amount_500 <0 THEN morning_balance_500 END,0) +
				 ISNULL(CASE WHEN tentative_loading_amount_2000 <0 THEN morning_balance_2000 END,0) 
				  AS variation_amount ---MALAY: UPDATE FOR VARIATION
				--Add new fields for updated logic
				,max_loading_capacity_amount_100
				,max_loading_capacity_amount_200
				,max_loading_capacity_amount_500
				,max_loading_capacity_amount_2000
				,total_loading_capacity_amount
				,loading_amount_original
				 FROM #tentative_loading_amount
						
				-- Continue until negative amount
				 --SELECT 'Iteration' , @ITERATION
				 --BREAK
			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Insert into #loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #loading_amount Completed'


			-- Logic goes here: The loop can be broken with the BREAK command.
			END -- END 
			ELSE
			BEGIN -- BEGIN of ELSE

			
			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Update into #loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
							

	
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Update into #loading_amount Started'
			-- Add 20% more cash for 100 AND 200 denomination IF capacity is allowed
/* Old Logic
			update #tentative_loading_amount
			Set tentative_loading_amount_100= case when tentative_loading_amount_100 > 0 then case when ( morning_balance_100+tentative_loading_amount_100
																						+((cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_100)) < cassette_100_capacity_amount
																						    then  ( tentative_loading_amount_100 + (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_100)
																							else (cassette_100_capacity_amount-morning_balance_100)
																							end
																							else 0
																							end
			, tentative_loading_amount_200= case when tentative_loading_amount_200 > 0 then case when (  morning_balance_200+tentative_loading_amount_200
																						+( (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_200)) < cassette_200_capacity_amount
																						    then  ( tentative_loading_amount_200 +  (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_200)
																							else (cassette_200_capacity_amount-morning_balance_200)
																							end
																								else
																								0
																								end
*/
			update #tentative_loading_amount
			Set tentative_loading_amount_100= case when tentative_loading_amount_100 > 0 then case when ( tentative_loading_amount_100
																						+((cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_100)) < max_loading_capacity_amount_100
																						    then  ( tentative_loading_amount_100 + (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_100)
																							else max_loading_capacity_amount_100
																							end
																							else 0
																							end
			, tentative_loading_amount_200= case when tentative_loading_amount_200 > 0 then case when (  tentative_loading_amount_200
																						+( (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_200)) < max_loading_capacity_amount_200
																						    then  ( tentative_loading_amount_200 +  (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_200)
																							else max_loading_capacity_amount_200
																							end
																								else
																								0
																								end

				break

			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
              values ('Update into #loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				
			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Update into #loading_amount Completed'
			END -- END of ELSE
		
		END -- END of WHILE loop


		--Round amount		 
		 
		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
          values ('Insert into #deno_rounded_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		  
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #deno_rounded_loading_amount Started'

		 -- SELECT * FROM #tentative_loading_amount
									
		  drop table if exists #deno_rounded_loading_amount	
		

/*
			select	*
					,tentative_loading_amount_50 as rounded_amount_50,
					case when (cast(tentative_loading_amount_100 as bigint)  % @denomination_wise_round_off_100 ) > 0  
						then  
						case when (cast( (tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) +1) * @denomination_wise_round_off_100 > cassette_100_capacity_amount
						then  (cast(( tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) -1) * @denomination_wise_round_off_100
							else (cast( (tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) +1) * @denomination_wise_round_off_100
						end
					 else    0 end  rounded_amount_100,
				 case when (cast( tentative_loading_amount_200 as bigint)  % @denomination_wise_round_off_200 ) > 0  then 
						case when (cast( (tentative_loading_amount_100 /@denomination_wise_round_off_200) as int) +1) * @denomination_wise_round_off_200 > cassette_200_capacity_amount then
							(cast((tentative_loading_amount_100 /@denomination_wise_round_off_200)  as int) -1) * @denomination_wise_round_off_200
						else
							(cast( (tentative_loading_amount_100 /@denomination_wise_round_off_200)  as int) +1) * @denomination_wise_round_off_200
						end
					else  
						0 
					end  rounded_amount_200,
				 case when (cast( (tentative_loading_amount_500 ) as bigint)  % @denomination_wise_round_off_500 ) > 0  then 
						case when (cast(  (tentative_loading_amount_500 /@denomination_wise_round_off_500)  as int) +1) * @denomination_wise_round_off_500 > cassette_500_capacity_amount then 
							(cast(  (tentative_loading_amount_500 /@denomination_wise_round_off_500) as int) -1) * @denomination_wise_round_off_500
						else
							(cast(  (tentative_loading_amount_500 /@denomination_wise_round_off_500) as int) +1) * @denomination_wise_round_off_500
						end
					else    
						0 
					end  rounded_amount_500,
				 case when (cast((tentative_loading_amount_2000 ) as bigint)  % @denomination_wise_round_off_2000 ) > 0  	then 
						case when (cast( (tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) +1) * @denomination_wise_round_off_2000 > cassette_2000_capacity_amount then
							(cast((tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) -1) * @denomination_wise_round_off_2000
						else
							(cast((tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) +1) * @denomination_wise_round_off_2000
						end 
					else    
						0 
					end  rounded_amount_2000
		into #deno_rounded_loading_amount
		from #tentative_loading_amount
*/
--Updated logic
			select	*
					,tentative_loading_amount_50 as rounded_amount_50,
			
					case when (cast(tentative_loading_amount_100 as bigint)  % @denomination_wise_round_off_100 ) > 0  
						then  
						case when (cast( (tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) +1) * @denomination_wise_round_off_100 > max_loading_capacity_amount_100
						then case when  (cast(( tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) -1) * @denomination_wise_round_off_100 > 0 then
									(cast(( tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) -1) * @denomination_wise_round_off_100
									else
										0
									end
							else (cast( (tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) +1) * @denomination_wise_round_off_100
						end
					 else    tentative_loading_amount_100 end  rounded_amount_100,
					 
				 case when (cast(tentative_loading_amount_200 as bigint)  % @denomination_wise_round_off_200 ) > 0  
						then  
						case when (cast( (tentative_loading_amount_200 /@denomination_wise_round_off_200) as int) +1) * @denomination_wise_round_off_200 > max_loading_capacity_amount_200
						then case when  (cast(( tentative_loading_amount_200 /@denomination_wise_round_off_200) as int) -1) * @denomination_wise_round_off_200 > 0 then
									(cast(( tentative_loading_amount_200 /@denomination_wise_round_off_200) as int) -1) * @denomination_wise_round_off_200
					else  
						0 
									end
							else (cast( (tentative_loading_amount_200 /@denomination_wise_round_off_200) as int) +1) * @denomination_wise_round_off_200
						end
					 else    tentative_loading_amount_200 end  rounded_amount_200,

				 case when (cast(tentative_loading_amount_500 as bigint)  % @denomination_wise_round_off_500 ) > 0  
						then  
						case when (cast( (tentative_loading_amount_500 /@denomination_wise_round_off_500) as int) +1) * @denomination_wise_round_off_500 > max_loading_capacity_amount_500
						then case when  (cast(( tentative_loading_amount_500 /@denomination_wise_round_off_500) as int) -1) * @denomination_wise_round_off_500 > 0 then
							(cast(  (tentative_loading_amount_500 /@denomination_wise_round_off_500) as int) -1) * @denomination_wise_round_off_500
						else
						0 
									end
							else (cast( (tentative_loading_amount_500 /@denomination_wise_round_off_500) as int) +1) * @denomination_wise_round_off_500
						end
					 else    tentative_loading_amount_500 end  rounded_amount_500,

				 case when (cast(tentative_loading_amount_2000 as bigint)  % @denomination_wise_round_off_2000 ) > 0  
						then  
						case when (cast( (tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) +1) * @denomination_wise_round_off_2000 > max_loading_capacity_amount_2000
						then case when  (cast(( tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) -1) * @denomination_wise_round_off_2000 > 0 then
							(cast((tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) -1) * @denomination_wise_round_off_2000
						else
						0 
									end
							else (cast( (tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) +1) * @denomination_wise_round_off_2000
						end
					 else    tentative_loading_amount_2000 end  rounded_amount_2000
		into #deno_rounded_loading_amount
		from #tentative_loading_amount
		--SELECT * FROM #deno_rounded_loading_amount
		
		-- select * from #deno_rounded_loading_amount
		-- Select * from  #tentative_loading_amount
		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
          values ('Insert into #deno_rounded_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #deno_rounded_loading_amount Completed'
					
		---  drop table  #deno_rounded_loading_amount
		WHILE 1=1
		BEGIN	
		DROP TABLE IF exists #gap_amount

		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
          values ('Insert into #gap_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
		
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #gap_amount Started'
		
		SELECT *
		, rounded_amount_100 - tentative_loading_amount_100 AS rounding_gap_100
		, rounded_amount_200 - tentative_loading_amount_200 AS rounding_gap_200
		, rounded_amount_500 - tentative_loading_amount_500 AS rounding_gap_500
		, rounded_amount_2000 - tentative_loading_amount_2000 AS rounding_gap_2000
		, (rounded_amount_100 + rounded_amount_200 + rounded_amount_500 + rounded_amount_2000) AS total_rounded_loading_amount
		, (rounded_amount_100 + rounded_amount_200 + rounded_amount_500 + rounded_amount_2000) - loading_amount  AS total_loading_amount_rounding_gap
		 INTO #gap_amount
		 FROM #deno_rounded_loading_amount
		
		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
          values ('Insert into #gap_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
		
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #gap_amount Completed'

		--SELECT * FROM execution_log order by id desc	
		
		IF exists ( SELECT 1 FROM #gap_amount WHERE (total_rounded_loading_amount + total_morning_balance) > limit_amount ) 
		BEGIN
			-- find max gap
			
			DROP TABLE IF exists #max_gap 

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			values ('Insert into #max_gap Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #max_gap Started'
		

			SELECT *, 
				(
					SELECT MAX(v) FROM (VALUES (rounding_gap_100),
					 (rounding_gap_200), 
					 (rounding_gap_500),
					 (rounding_gap_2000)
				 )
					  AS value(v)) AS max_gap
					INTO #max_gap
				    FROM #gap_amount

			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
             values ('Insert into #max_gap Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
		
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #max_gap Completed'
			-- Re-distribute max gap amount to lower

			TRUNCATE TABLE #deno_rounded_loading_amount			

			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #deno_rounded_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #deno_rounded_loading_amount Started'

			INSERT INTO #deno_rounded_loading_amount
			SELECT      atm_id
					,   site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	atm_priority  --Add variable
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	denomination_100_max_capacity_percentage
					,	denomination_200_max_capacity_percentage
					,	denomination_500_max_capacity_percentage
					,	denomination_2000_max_capacity_percentage
					,   cassette_50_count_original  
					,   cassette_100_count_original 
					,   cassette_200_count_original 
					,   cassette_500_count_original 
					,   cassette_2000_count_original
					,   deno_100_priority
					,   deno_200_priority
					,   deno_500_priority
					,   deno_2000_priority
					--Add variables
					,	variation_amount
					,	max_loading_capacity_amount_100
					,	max_loading_capacity_amount_200
					,	max_loading_capacity_amount_500
					,	max_loading_capacity_amount_2000
					,	total_loading_capacity_amount
					,	loading_amount_original
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50

					, CASE -- total rounded vaulting amnt> original vaulting amt
						WHEN (total_rounded_loading_amount + total_morning_balance) > limit_amount AND rounding_gap_100 = max_gap 
							THEN   rounded_amount_100 - @denomination_wise_round_off_100 
							ELSE rounded_amount_100 
						END AS rounded_amount_100_updated
					, CASE 
						WHEN (total_rounded_loading_amount + total_morning_balance) > limit_amount AND rounding_gap_200 = max_gap 
							THEN   rounded_amount_200 - @denomination_wise_round_off_200 
							ELSE rounded_amount_200 
						END AS rounded_amount_200_updated
					, CASE 
						WHEN (total_rounded_loading_amount + total_morning_balance) > limit_amount AND rounding_gap_500 = max_gap 
							THEN   rounded_amount_500 - @denomination_wise_round_off_500 
							ELSE rounded_amount_500 
						END AS rounded_amount_500_updated
					, CASE 
						WHEN (total_rounded_loading_amount + total_morning_balance) > limit_amount AND rounding_gap_2000 = max_gap 
							THEN   rounded_amount_2000 - @denomination_wise_round_off_2000 
							ELSE rounded_amount_2000 
						END AS rounded_amount_2000_updated
				FROM #max_gap
			
			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #deno_rounded_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #deno_rounded_loading_amount completed'

			
		END
		ELSE
		BEGIN
					BREAK
		END
	END -- END of WHILE loop for rounding
--END--END of procedure
	
	--select * from #deno_rounded_loading_amount
		DROP TABLE IF exists  #pre_vault_amounting


		Select * into #pre_vault_amounting
		from
		(
			SELECT      atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	atm_priority
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
				    ,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 

					,   case when  (rounded_amount_50 +
									rounded_amount_100+
									rounded_amount_200+
									rounded_amount_500+
									rounded_amount_2000) = 0
							 and dist_Purpose='EOD'
							 then 'EOD' 
							 else 'Add Cash' end as dist_Purpose

					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	denomination_100_max_capacity_percentage
					,	denomination_200_max_capacity_percentage
					,	denomination_500_max_capacity_percentage
					,	denomination_2000_max_capacity_percentage
					,	cassette_50_count_original  
					,	cassette_100_count_original 
					,	cassette_200_count_original 
					,	cassette_500_count_original 
					,	cassette_2000_count_original
					,	deno_100_priority
					,	deno_200_priority
					,	deno_500_priority
					,	deno_2000_priority
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50
					,   rounded_amount_100
					,   rounded_amount_200
					,   rounded_amount_500
					,   rounded_amount_2000
					,   case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
						( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) 
											  end
											  as total_expected_balanceT1
					, case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
					( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_50,2)) as float)/100)
											  end 
											  as expected_balanceT1_50
                ,case when  ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then  
				( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_100,2)) as float)/100)
											  end 
											  as expected_balanceT1_100
                ,case when  ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then  
				( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_200,2)) as float)/100)
											end 
											  as expected_balanceT1_200
                 ,case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
					 ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) *( cast((Round(cassette_capacity_percentage_500,2)) as float)/100)
											end
											  as expected_balanceT1_500
                 , case when  ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
							( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_2000,2)) as float)/100)
											  end 
											  as expected_balanceT1_2000
				, case when  ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 				
				(( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(applied_vaulting_percentage,2)) as float)/100)) 
											  end 
									as total_vault_amount
				, case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
				(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_50,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_50		
											   
				, case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
				(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											   ((Round(cassette_capacity_percentage_100,2)) /100)
											   *(cast(applied_vaulting_percentage as float)/100))	
											   end 
											   as vaultingamount_100
 
				, case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
				(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											   ((Round(cassette_capacity_percentage_200,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_200	

			    , case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
					(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_500,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))
											   end 
											   as vaultingamount_500	

             , case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
					(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_2000,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_2000	
					FROM 
               #deno_rounded_loading_amount 
			   
			   )a
--select * FROM #pre_vault_amounting
	--PENDING 23 Dec 2018: Amount for total_expected_balanceT1 is getting in negative. Ned to check.

	--****************************************Rounding of Vaulting amount*************************************************************/
	--select * FROM #pre_vault_amounting
	 drop table if exists #vault_amounting
		
		/* For testing dont remove below commented code */
		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT

		-- Select	
		--  @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		-- from system_settings where record_status = 'Active'	
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Insert into vault_amounting Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


					select	*
					,vaultingamount_50 as rounded_vaultingamount_50
					,
						case when (cast(vaultingamount_100 as bigint)  % @denomination_wise_round_off_100 ) > 0  
						   then  
						  (cast( (vaultingamount_100 /@denomination_wise_round_off_100) as int) +1) * @denomination_wise_round_off_100  
							else    0 
							end  rounded_vaultingamount_100
			
					,
						case when (cast(vaultingamount_200 as bigint)  % @denomination_wise_round_off_200 ) > 0  
						   then  
						  (cast( (vaultingamount_200 /@denomination_wise_round_off_200) as int) +1) * @denomination_wise_round_off_200  
							else    0 
							end  rounded_vaultingamount_200
				 
					,
						case when (cast(vaultingamount_500 as bigint)  % @denomination_wise_round_off_500 ) > 0  
						   then  
						  (cast( (vaultingamount_500 /@denomination_wise_round_off_500) as int) +1) * @denomination_wise_round_off_500  
							else    0 
							end  rounded_vaultingamount_500, 

					
						case when (cast(vaultingamount_2000 as bigint)  % @denomination_wise_round_off_2000 ) > 0  
						   then  
						  (cast( (vaultingamount_2000 /@denomination_wise_round_off_2000) as int) +1) * @denomination_wise_round_off_2000  
							else    0 
							end  rounded_vaultingamount_2000


		into #vault_amounting
		from #pre_vault_amounting


		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	    values ('Insert into vault_amounting Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		--   Select * from #vault_amounting		
		--   select * from #pre_vault_amounting
		---  drop table  #deno_rounded_loading_amount
		

		--DECLARE @dateT DATETIME
		--SET @dateT ='2018-11-05'

		--DECLARE @MaxDestinationDate DATE
		--DECLARE @dispenseformula NVARCHAR(50)
		--DECLARE @confidence_factor NVARCHAR(50)
		--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT
		--Select	@confidence_factor = confidence_factor
		--, @dispenseformula = dispenseformula
		--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		-- from system_settings where record_status = 'Active'			



		/***************************************Roundig of calcualted vault amount*********************************/
		WHILE 1=1
		BEGIN	

		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Insert into #vault_gap_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		DROP TABLE IF exists #vault_gap_amount		 
		
		SELECT *
		, ROUND((rounded_vaultingamount_100 - vaultingamount_100),2) AS rounding_gap_100
		, ROUND((rounded_vaultingamount_200 - vaultingamount_200),2) AS rounding_gap_200
		, ROUND((rounded_vaultingamount_500 - vaultingamount_500),2) AS rounding_gap_500
		, ROUND((rounded_vaultingamount_2000 - vaultingamount_2000),2) AS rounding_gap_2000
		, (rounded_vaultingamount_100 + rounded_vaultingamount_200 + rounded_vaultingamount_500 + rounded_vaultingamount_2000) AS total_rounded_vault_amount
		, (rounded_vaultingamount_100 + rounded_vaultingamount_200 + rounded_vaultingamount_500 + rounded_vaultingamount_2000) - total_vault_amount  AS total_rounded_vault_amount_gap
		 INTO   #vault_gap_amount
		 FROM   #vault_amounting
		
		  -- Select * from #vault_gap_amount
		 -- total rounded vaulting amnt> original vaulting amt

		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into #vault_gap_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

		IF exists ( SELECT * FROM #vault_gap_amount
		 WHERE (total_rounded_vault_amount ) > limit_amount ) 
		
		BEGIN
			-- find max gap
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #max_vault_gap Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

			DROP TABLE IF exists #max_vault_gap 	
			SELECT *, 
				(
					SELECT MAX(v) FROM (VALUES (rounding_gap_100), 
					(rounding_gap_200), 
					(rounding_gap_500),
					(rounding_gap_2000)) AS value(v)
		    	) 
					AS max_gap
					INTO #max_vault_gap
				    FROM #vault_gap_amount

				
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #max_vault_gap Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	
			/* For testing dont remove below commented code */	

		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT

		--Select	
		-- @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		-- from system_settings where record_status = 'Active'	
		

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #vault_amounting Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			TRUNCATE TABLE #vault_amounting		

			INSERT INTO #vault_amounting
			SELECT  atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	atm_priority
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	denomination_100_max_capacity_percentage
					,	denomination_200_max_capacity_percentage
					,	denomination_500_max_capacity_percentage
					,	denomination_2000_max_capacity_percentage
					,	cassette_50_count_original  
					,	cassette_100_count_original 
					,	cassette_200_count_original 
					,	cassette_500_count_original 
					,	cassette_2000_count_original
					,	deno_100_priority
					,	deno_200_priority
					,	deno_500_priority
					,	deno_2000_priority
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50
					,   rounded_amount_100
					,   rounded_amount_200
					,   rounded_amount_500
					,   rounded_amount_2000
					,	total_expected_balanceT1
					,	expected_balanceT1_50
					,	expected_balanceT1_100
					,	expected_balanceT1_200
					,	expected_balanceT1_500
					,	expected_balanceT1_2000
					,	total_vault_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	rounded_vaultingamount_50
					, CASE  
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_100 = max_gap 
							THEN   rounded_vaultingamount_100 - @denomination_wise_round_off_100 
							ELSE rounded_vaultingamount_100 
						END AS rounded_Vault_amt_100_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_200 = max_gap 
							THEN   rounded_vaultingamount_200 - @denomination_wise_round_off_200 
							ELSE rounded_vaultingamount_200 
						END AS rounded_Vault_amt_200_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_500 = max_gap 
							THEN  ( rounded_vaultingamount_500 - @denomination_wise_round_off_500 )
							ELSE rounded_vaultingamount_500 
						END AS rounded_Vault_amt_500_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_2000 = max_gap 
							THEN   rounded_vaultingamount_2000 - @denomination_wise_round_off_2000 
							ELSE rounded_vaultingamount_2000 
						END AS rounded_Vault_amt_2000_updated
								,denomination_100_max_capacity_percentage
								,denomination_200_max_capacity_percentage
								,denomination_500_max_capacity_percentage
								,denomination_2000_max_capacity_percentage
								,cassette_50_count_original  
								,cassette_100_count_original 
								,cassette_200_count_original 
								,cassette_500_count_original 
								,cassette_2000_count_original
								,deno_100_priority
								,deno_200_priority
								,deno_500_priority
								,deno_2000_priority
				FROM 
					 #max_vault_gap  

			   -- SELECT * FROM #vault_amounting

			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #vault_amounting Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
						
		END
	ELSE
		BEGIN
		
			BREAK
		END

END

		--	select * from #vault_amounting 	    
  ----------------------------------Inserting into distribution planning detail----------------------
  
	--	select * from #vault_amounting 
 

            DROP table if exists #distribution

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #distribution Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				
			Select * into #distribution
				from(
				Select atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	atm_priority
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	denomination_100_max_capacity_percentage
					,	denomination_200_max_capacity_percentage
					,	denomination_500_max_capacity_percentage
					,	denomination_2000_max_capacity_percentage
					,	cassette_50_count_original  
					,	cassette_100_count_original 
					,	cassette_200_count_original 
					,	cassette_500_count_original 
					,	cassette_2000_count_original
					,	deno_100_priority
					,	deno_200_priority
					,	deno_500_priority
					,	deno_2000_priority
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50	as forecasted_amt_50 
					,   rounded_amount_100	as forecasted_amt_100
					,   rounded_amount_200	as forecasted_amt_200
					,   rounded_amount_500	as forecasted_amt_500
					,   rounded_amount_2000	as forecasted_amt_2000
					,  (rounded_amount_100 + rounded_amount_200 + rounded_amount_500 + rounded_amount_2000) as total_forecasted_amt
					,	total_expected_balanceT1
					,	expected_balanceT1_50
					,	expected_balanceT1_100
					,	expected_balanceT1_200
					,	expected_balanceT1_500
					,	expected_balanceT1_2000
					,	total_vault_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	rounded_vaultingamount_50
					,	rounded_vaultingamount_100
					,	rounded_vaultingamount_200
					,	rounded_vaultingamount_500
					,	rounded_vaultingamount_2000
					,	(rounded_vaultingamount_50 + rounded_vaultingamount_100	+ rounded_vaultingamount_200
					+    rounded_vaultingamount_500+ rounded_vaultingamount_2000 )
					     as total_rounded_vault_amt
							from  #vault_amounting

					)a	
					
						   

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #distribution Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #distribution Completed'
		
			  -- generate indent code.
			  
				drop table if exists #dist_with_cra	
				DROP TABLE IF EXISTS #final_feeder_level_indent
			DROP TABLE IF EXISTS #feeder_level_forecast
			DROP TABLE IF EXISTS #feeder_level_dataset

				SELECT dist.* ,
					cra.new_cra as cra
					,concat(dist.bank_code,'/',dist.project_id,'/',dist.feeder_branch_code,'/',cra.new_cra,'/',format(DATEADD(MI,330,GETUTCDATE()),'yyyyMMddHHmmss')) AS indentcode
				INTO #dist_with_cra
				FROM #distribution dist 
				JOIN cra_feasibility cra
				ON dist.atm_id = cra.atm_id 
				AND dist.site_code = cra.site_code 				
				AND cra.record_status = 'Active'

			--SELECT * FROM #dist_with_cra
		
		--

		

		-- Calculate vaulting in case of no cash pre availability
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #feeder_level_forecast Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

		SELECT 
		indentcode,
		indentdate,
		project_id, 
		bank_code, 
		feeder_branch_code
		,sol_id
		,cra
		,SUM(max_loading_amount) max_loading_amount
		,SUM(cassette_50_capacity_amount)	AS max_loading_amount_50
		,SUM(cassette_100_capacity_amount)	AS max_loading_amount_100
		,SUM(cassette_200_capacity_amount)	AS max_loading_amount_200
		,SUM(cassette_500_capacity_amount)	AS max_loading_amount_500
		,SUM(cassette_2000_capacity_amount) AS max_loading_amount_2000
		,SUM(min_loading_amount)			AS min_loading_amount
		,SUM(forecast_loading_amount)		AS forecast_loading_amount
		,SUM(forecast_loading_amount_50)	AS forecast_loading_amount_50
		,SUM(forecast_loading_amount_100)	AS forecast_loading_amount_100
		,SUM(forecast_loading_amount_200)	AS forecast_loading_amount_200
		,SUM(forecast_loading_amount_500)	AS forecast_loading_amount_500
		,SUM(forecast_loading_amount_2000)	AS forecast_loading_amount_2000
		,SUM(total_cassette_capacity)		AS total_cassette_capacity
		,SUM(cassette_50_capacity_amount)	AS cassette_50_capacity_amount
		,SUM(cassette_100_capacity_amount)	AS cassette_100_capacity_amount
		,SUM(cassette_200_capacity_amount)	AS cassette_200_capacity_amount
		,SUM(cassette_500_capacity_amount)	AS cassette_500_capacity_amount
		,SUM(cassette_2000_capacity_amount) AS cassette_2000_capacity_amount

	INTO #feeder_level_forecast
	FROM
		(
			SELECT 
				indentcode,
				indentdate,
				atm_id,
				sol_id, 
				project_id, 
				bank_code, 
				feeder_branch_code,
				cra,
				forecasted_amt_50 AS forecast_loading_amount_50, 
				forecasted_amt_100 AS forecast_loading_amount_100,
				forecasted_amt_200 AS forecast_loading_amount_200,
				forecasted_amt_500 AS forecast_loading_amount_500,
				forecasted_amt_2000 AS forecast_loading_amount_2000,
				(COALESCE(forecasted_amt_50,0) + COALESCE(forecasted_amt_100,0) + 
				COALESCE(forecasted_amt_200,0) + COALESCE(forecasted_amt_500,0) + 
				COALESCE(forecasted_amt_2000,0)
				) AS forecast_loading_amount
				,total_cassette_capacity
				,(
					SELECT MIN(v) FROM (VALUES (bank_cash_limit), 
					(insurance_limit),
					 (total_cassette_capacity)) AS value(v)) 
					 AS max_loading_amount
				,
				(	 CASE  
						WHEN curbal_div_avgDisp>= decidelimitdays 
							THEN 0         
						WHEN decidelimitdays=1 
							THEN 1.5*avgdispense
						WHEN cashout=1 
							THEN (CASE 
									WHEN  loadingGap_cur_bal_avg_disp >100000 
										THEN loadingGap_cur_bal_avg_disp
										ELSE loadingGap_cur_bal_avg_disp 
									END)
							ELSE  loadingGap_cur_bal_avg_disp  
						END	) 
						AS min_loading_amount	
						
				,  cassette_50_capacity_amount - morning_balance_50		AS cassette_50_capacity_amount,
				  cassette_100_capacity_amount - morning_balance_100	AS cassette_100_capacity_amount,
				  cassette_200_capacity_amount - morning_balance_200	AS cassette_200_capacity_amount,
				  cassette_500_capacity_amount - morning_balance_500	AS cassette_500_capacity_amount,
				  cassette_2000_capacity_amount - morning_balance_2000	AS cassette_2000_capacity_amount

				FROM #dist_with_cra

				)atm
				 GROUP by project_id, bank_code, feeder_branch_code,indentdate,indentcode,cra,sol_id


				-- select * from #dist_with_cra

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #feeder_level_forecast Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			

			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #feeder_level_forecast Completed'
			--select * from #feeder_level_forecast
			--select * from #final_feeder_level_indent

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #final_feeder_level_indent Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			  Select FL.*  ,
					a.vaultingamount_50,
					a.vaultingamount_100,
					a.vaultingamount_200,
					a.vaultingamount_500,
					a.vaultingamount_2000,
					a.total_rounded_vault_amt,
					VCB.vault_balance_100 ,
					VCB.vault_balance_200,
					VCB.vault_balance_500,
					VCB.vault_balance_2000,
					VCB.total_vault_balance,
					FL.forecast_loading_amount_50 as indent_50,
					FL.forecast_loading_amount_100 -ISNULL(VCB.vault_balance_100,0)+ ISNULL(a.vaultingamount_100 ,0)   as indent_100,
					FL.forecast_loading_amount_200 -ISNULL(VCB.vault_balance_200,0)+ ISNULL(a.vaultingamount_200 ,0)   as indent_200,
					FL.forecast_loading_amount_500 -ISNULL(VCB.vault_balance_500,0)+ ISNULL(a.vaultingamount_500 ,0)   as indent_500,
					FL.forecast_loading_amount_2000 -ISNULL(VCB.vault_balance_2000,0)+ ISNULL(a.vaultingamount_2000,0) as indent_2000

          into #final_feeder_level_indent
		  from #feeder_level_forecast FL

		   left join (

			Select 
			bank_code,project_id,feeder_branch_code,indentdate,
			 
			SUM(rounded_vaultingamount_50)   as vaultingamount_50,
			SUM(rounded_vaultingamount_100)  as vaultingamount_100,
			SUM(rounded_vaultingamount_200)  as vaultingamount_200,
			SUM(rounded_vaultingamount_500)  as vaultingamount_500,   
			SUM(rounded_vaultingamount_2000) as vaultingamount_2000,
			SUM(total_rounded_vault_amt)     as total_rounded_vault_amt			      
			   from #dist_with_cra 			
					group by  bank_code,feeder_branch_code,project_id,indentdate-- add cra and indent_code
					)a
					on FL.bank_code=a.bank_code
					and FL.project_id=a.project_id
					and FL.feeder_branch_code=a.feeder_branch_code

					LEFT JOIN vault_cash_balance VCB
					on FL.bank_code=VCB.bank_name
					and FL.project_id=VCB.project_id
					and FL.feeder_branch_code=VCB.feeder_branch_name
					and cast(VCB.Date as date) = cast(@dateT as date)
					and VCB.record_status ='Active'
					and len(ltrim(rtrim(isnull(atm_id,'')))) < 4 

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #final_feeder_level_indent Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			  

			  --SELECT * FROM #feeder_level_forecast WHERE bank_code = 'DENA'

			  -- SELECT * FROM distribution_planning_detail

			  ------------------------------------------ Cash Pre Availability Code ---------------------------------------------------------

			--  SELECT * FROM #dist_with_cra
			 -- SELECT * from #feeder_level_forecast
			--  SELECT * FROM Cash_pre_availability
				--DECLARE @dateT DATETIME = '2018-11-05'
			-- select f.*,a.Total_Amount_Available,a.available_100_amount, a.available_200_amount, a.available_500_amount, a.available_2000_amount  
			--,case when (a.available_100_amount + a.available_200_amount + a.available_500_amount + a.available_2000_amount) = a.Total_Amount_Available then
			--1 else 0 end as is_deno_wise_cash_available 
			--into #feeder_level_dataset
			--from #feeder_level_forecast f 
			--LEFT join Cash_pre_availability a
			--on f.project_id = a.project_id 
			--	and f.bank_code=a.bank_code 
			--	and a.Applied_to_level = 'Feeder'
			--	and f.feeder_branch_code = a.Feeder_Branch_Code
			--	and ( @dateT between a.from_date and a.to_date )
			--	and a.record_status = 'Active'

			 select f.*,
					COALESCE(a.Total_Amount_Available,0) as Total_Amount_Available,
					COALESCE(a.available_100_amount,0) as available_100_amount,
					COALESCE(a.available_200_amount, 0) as available_200_amount,
					COALESCE(a.available_500_amount, 0) as available_500_amount,
					COALESCE(a.available_2000_amount,0) as available_2000_amount
			,
				case 
				when (COALESCE(a.available_100_amount,0) + 
					  COALESCE(a.available_200_amount,0) + 
					    COALESCE(a.available_500_amount,0) + 
						 COALESCE(a.available_2000_amount,0)) =  COALESCE(a.Total_Amount_Available,0) then
			1 else 0 end as is_deno_wise_cash_available 
			into #feeder_level_dataset
			from #final_feeder_level_indent f 
			LEFT join Cash_pre_availability a
			on f.project_id = a.project_id 
				and f.bank_code=a.bank_code 
				and a.Applied_to_level = 'Feeder'
				and f.feeder_branch_code = a.Feeder_Branch_Code
				and ( @dateT between a.from_date and a.to_date )
				and a.record_status = 'Active'

		--select * from #feeder_level_dataset
		
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into ##feeder_level_dataset Completed'

 DROP TABLE IF EXISTS #temp_cash_pre_availability

CREATE TABLE #temp_cash_pre_availability
(
	project_id		nvarchar(50)
,	bank_code		nvarchar(50)
,   feeder_branch_code nvarchar(50)
,	atmid		nvarchar(50)
,	total_opening_remaining_available_amount		bigint
,	opening_remaining_available_amount_100		bigint
,	opening_remaining_available_amount_200		bigint
,	opening_remaining_available_amount_500		bigint
,	opening_remaining_available_amount_2000		bigint
,	original_total_forecasted_amt		bigint
,	original_forecasted_amt_100		bigint
,	original_forecasted_amt_200		bigint
,	original_forecasted_amt_500		bigint
,	original_forecasted_amt_2000		bigint
,	morning_balance_100		bigint
,	morning_balance_200		bigint
,	morning_balance_500		bigint
,	morning_balance_2000		bigint
,	total_morning_balance		bigint
,	cassette_100_count_original		bigint
,	cassette_200_count_original		bigint
,	cassette_500_count_original		bigint
,	cassette_2000_count_original		bigint
,	cassette_100_brand_capacity		bigint
,	cassette_200_brand_capacity		bigint
,	cassette_500_brand_capacity		bigint
,	cassette_2000_brand_capacity		bigint
,	total_capacity_amount_100		bigint
,	total_capacity_amount_200		bigint
,	total_capacity_amount_500		bigint
,	total_capacity_amount_2000		bigint
,	denomination_100_max_capacity_percentage		bigint
,	denomination_200_max_capacity_percentage		bigint
,	denomination_500_max_capacity_percentage		bigint
,	denomination_2000_max_capacity_percentage		bigint
,	max_amt_allowed_100		bigint
,	max_amt_allowed_200		bigint
,	max_amt_allowed_500		bigint
,	max_amt_allowed_2000		bigint
,	denomination_wise_round_off_100		bigint
,	denomination_wise_round_off_200		bigint
,	denomination_wise_round_off_500		bigint
,	denomination_wise_round_off_2000		bigint
,	tentative_loading_100		bigint
,	tentative_loading_200		bigint
,	tentative_loading_500		bigint
,	tentative_loading_2000		bigint
,	rounded_tentative_loading_100		bigint
,	rounded_tentative_loading_200		bigint
,	rounded_tentative_loading_500		bigint
,	rounded_tentative_loading_2000		bigint
,	deno_100_priority		bigint
,	deno_200_priority		bigint
,	deno_500_priority		bigint
,	deno_2000_priority		bigint
,	is_deno_wise_cash_available		int
,	priority_1_is_denomination_100		int
,	priority_1_is_denomination_200		int
,	priority_1_is_denomination_500		int
,	priority_1_is_denomination_2000		int
,	priority_1_is_remaining_amount_available_100		int
,	priority_1_is_remaining_amount_available_200		int
,	priority_1_is_remaining_amount_available_500		int
,	priority_1_is_remaining_amount_available_2000		int
,	priority_1_is_remaining_capacity_available_100		int
,	priority_1_is_remaining_capacity_available_200		int
,	priority_1_is_remaining_capacity_available_500		int
,	priority_1_is_remaining_capacity_available_2000		int
,	priority_1_loading_amount_100		bigint
,	priority_1_loading_amount_200		bigint
,	priority_1_loading_amount_500		bigint
,	priority_1_loading_amount_2000		bigint
,	priority_2_is_denomination_100		int
,	priority_2_is_denomination_200		int
,	priority_2_is_denomination_500		int
,	priority_2_is_denomination_2000		int
,	priority_2_is_remaining_amount_available_100		int
,	priority_2_is_remaining_amount_available_200		int
,	priority_2_is_remaining_amount_available_500		int
,	priority_2_is_remaining_amount_available_2000		int
,	priority_2_is_remaining_capacity_available_100		int
,	priority_2_is_remaining_capacity_available_200		int
,	priority_2_is_remaining_capacity_available_500		int
,	priority_2_is_remaining_capacity_available_2000		int
,	priority_2_loading_amount_100		bigint
,	priority_2_loading_amount_200		bigint
,	priority_2_loading_amount_500		bigint
,	priority_2_loading_amount_2000		bigint
,	priority_3_is_denomination_100		int
,	priority_3_is_denomination_200		int
,	priority_3_is_denomination_500		int
,	priority_3_is_denomination_2000		int
,	priority_3_is_remaining_amount_available_100		int
,	priority_3_is_remaining_amount_available_200		int
,	priority_3_is_remaining_amount_available_500		int
,	priority_3_is_remaining_amount_available_2000		int
,	priority_3_is_remaining_capacity_available_100		int
,	priority_3_is_remaining_capacity_available_200		int
,	priority_3_is_remaining_capacity_available_500		int
,	priority_3_is_remaining_capacity_available_2000		int
,	priority_3_loading_amount_100		bigint
,	priority_3_loading_amount_200		bigint
,	priority_3_loading_amount_500		bigint
,	priority_3_loading_amount_2000		bigint
,	priority_4_is_denomination_100		int
,	priority_4_is_denomination_200		int
,	priority_4_is_denomination_500		int
,	priority_4_is_denomination_2000		int
,	priority_4_is_remaining_amount_available_100		int
,	priority_4_is_remaining_amount_available_200		int
,	priority_4_is_remaining_amount_available_500		int
,	priority_4_is_remaining_amount_available_2000		int
,	priority_4_is_remaining_capacity_available_100		int
,	priority_4_is_remaining_capacity_available_200		int
,	priority_4_is_remaining_capacity_available_500		int
,	priority_4_is_remaining_capacity_available_2000		int
,	priority_4_loading_amount_100		bigint
,	priority_4_loading_amount_200		bigint
,	priority_4_loading_amount_500		bigint
,	priority_4_loading_amount_2000		bigint
,	loading_amount_100		bigint
,	loading_amount_200		bigint
,	loading_amount_500		bigint
,	loading_amount_2000		bigint
,	total_loading_amount		bigint
,	remaining_capacity_amount_100		bigint
,	remaining_capacity_amount_200		bigint
,	remaining_capacity_amount_500		bigint
,	remaining_capacity_amount_2000		bigint
,   closing_remaining_available_amount_100      bigint
,   closing_remaining_available_amount_200		bigint
,   closing_remaining_available_amount_500		bigint
,   closing_remaining_available_amount_2000		bigint
,   total_closing_remaining_available_amount	bigint

,	total_forecasted_remaining_amt		bigint
)

DROP TABLE IF EXISTS #temp_feeder_forecast
CREATE TABLE #temp_feeder_forecast
(
	project_id							nvarchar(50)
,	bank_code							nvarchar(50)
,	feeder_branch_code					nvarchar(50)
,	is_deno_wise_cash_available			INT    NULL
,	total_remaining_available_amount	BIGINT NULL
,   remaining_avail_100					BIGINT NULL
,   remaining_avail_200					BIGINT NULL
,   remaining_avail_500					BIGINT NULL
,   remaining_avail_2000				BIGINT NULL

)


--declare @feederbranchcode varchar(max)
--declare @atmid varchar(50)
declare @availableAmount BIGINT
DECLARE @avail_100 BIGINT
DECLARE @avail_200 BIGINT
DECLARE @avail_500 BIGINT
DECLARE @avail_2000 BIGINT
--DECLARE @forecast_amt BIGINT

		
		
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Cursor strated for allocating amount'


 DECLARE cursor1 CURSOR READ_ONLY
   FOR
    SELECT project_id, bank_code, feeder_branch_code,total_amount_available,available_100_amount,available_200_amount,available_500_amount,available_2000_amount,is_deno_wise_cash_available
	FROM #feeder_level_dataset  where Total_Amount_Available > 0
	ORDER BY feeder_branch_code


	declare @total_remaining_available_amount BIGINT
	, @remaining_avail_100 BIGINT
	, @remaining_avail_200 BIGINT
	, @remaining_avail_500 BIGINT
	, @remaining_avail_2000 BIGINT
	, @total_original_available_amount BIGINT
	, @original_avail_100 BIGINT
	, @original_avail_200 BIGINT
	, @original_avail_500 BIGINT
	, @original_avail_2000 BIGINT
	, @loading_amount_100 BIGINT
	, @loading_amount_200 BIGINT
	, @loading_amount_500 BIGINT
	, @loading_amount_2000 BIGINT
	, @is_deno_wise_cash_available INT
	, @project_id nvarchar(50)
	, @bank_code nvarchar(50)
	OPEN cursor1

	FETCH NEXT FROM cursor1 INTO @project_id,@bank_code, @feederbranchcode, @total_original_available_amount, @original_avail_100, @original_avail_200, @original_avail_500, @original_avail_2000, @is_deno_wise_cash_available
    
	--SELECT @feederbranchcode	
    WHILE @@FETCH_STATUS = 0  
    BEGIN		
				SET @total_remaining_available_amount	= @total_original_available_amount
				SET @remaining_avail_100				= @original_avail_100
				SET @remaining_avail_200				= @original_avail_200
				SET @remaining_avail_500				= @original_avail_500
				SET @remaining_avail_2000				= @original_avail_2000
				
						------ While Begin (cursor1 )]
				--DECLARE @availableAmount,@avail_100,@avail_200,@avail_500,@avail_2000

				--IF (@total_remaining_available_amount > 0  AND  @total_remaining_available_amount = @remaining_avail_100 + @remaining_avail_200 + @remaining_avail_500 + @remaining_avail_2000)
				--BEGIN		
						---- Available amount > 0 Begin
					DECLARE cursor2 cursor Read_Only
					For
					
					-- New
					SELECT	
							--project_id
							--, bank_code  
							 atm_id							
							, denomination_100_max_capacity_percentage
							, denomination_200_max_capacity_percentage
							, denomination_500_max_capacity_percentage
							, denomination_2000_max_capacity_percentage
							, morning_balance_100
							, morning_balance_200
							, morning_balance_500
							, morning_balance_2000
							, total_morning_balance
							, forecasted_amt_100
							, forecasted_amt_200
							, forecasted_amt_500
							, forecasted_amt_2000	
							, total_forecasted_amt
							, cassette_50_count_original					
							, cassette_100_count_original 
							, cassette_200_count_original 
							, cassette_500_count_original 
							, cassette_2000_count_original
							, deno_100_priority
							, deno_200_priority
							, deno_500_priority
							, deno_2000_priority
							, cassette_100_brand_capacity
							, cassette_200_brand_capacity
							, cassette_500_brand_capacity
							, cassette_2000_brand_capacity
							
					from #dist_with_cra  
					WHERE project_id = @project_id and bank_code = @bank_code and feeder_branch_code = @feederbranchcode
					ORDER BY atm_priority
					
					----SELECT * FROM #dist_with_cra where feeder_branch_code = 'GONDIA' and project_id = 'MOF' and bank_code = 'BOMH' and atm_id = 'NA0058C1'
					--select * from morningbalance where --feeder_branch_code = 'GONDIA' and 
					--project_id = 'MOF' and bank_name = 'BOMH' and atm_id = 'NA0058C1'

					--select * from cash_balance_register where --feeder_branch_code = 'GONDIA' and 
					--project_id = 'MOF' and bank_name = 'BOMH' and atm_id = 'NA0058C1'

					--Select project_id,bank_name,datafor_date_time,record_status, count(*)
					--				from cash_balance_register 
					--				where bank_name='BOMH' 
					--				and project_id='MOF' 
					--				group by datafor_date_time,record_status,project_id,bank_name
					--				order by datafor_date_time
					--	delete from 	cash_balance_register
					--	where bank_name='BOMH' 
					--				and project_id='MOF' 
					--				and datafor_date_time = '2018-12-29 22:00:00.000

									--and record_status ='Active'

					declare @atmid varchar(50)												
							, @denomination_100_max_capacity_percentage bigint
							, @denomination_200_max_capacity_percentage bigint
							, @denomination_500_max_capacity_percentage bigint
							, @denomination_2000_max_capacity_percentage bigint
							, @morning_balance_100 bigint
							, @morning_balance_200 bigint
							, @morning_balance_500 bigint
							, @morning_balance_2000 bigint
							, @total_morning_balance BIGINT
							, @original_forecasted_amt_100  bigint
							, @original_forecasted_amt_200  bigint
							, @original_forecasted_amt_500  bigint
							, @original_forecasted_amt_2000 bigint
							, @original_total_forecasted_amt BIGINT
							, @cassette_50_count_original   BIGINT							
							, @cassette_100_count_original 	bigint
							, @cassette_200_count_original 	bigint
							, @cassette_500_count_original 	bigint
							, @cassette_2000_count_original	bigint		
							, @deno_100_priority   BIGINT
							, @deno_200_priority	  BIGINT
							, @deno_500_priority	  BIGINT
							, @deno_2000_priority  BIGINT
							, @cassette_100_brand_capacity 	bigint		
							, @cassette_200_brand_capacity	bigint		
							, @cassette_500_brand_capacity	bigint		
							, @cassette_2000_brand_capacity	bigint		
				    OPEN cursor2
					FETCH NEXT FROM cursor2 
						  INTO  							
							 @atmid							
							, @denomination_100_max_capacity_percentage
							, @denomination_200_max_capacity_percentage
							, @denomination_500_max_capacity_percentage
							, @denomination_2000_max_capacity_percentage
							, @morning_balance_100
							, @morning_balance_200
							, @morning_balance_500
							, @morning_balance_2000
							, @total_morning_balance
							, @original_forecasted_amt_100
							, @original_forecasted_amt_200
							, @original_forecasted_amt_500
							, @original_forecasted_amt_2000
							, @original_total_forecasted_amt
							, @cassette_50_count_original  
							, @cassette_100_count_original 
							, @cassette_200_count_original 
							, @cassette_500_count_original 
							, @cassette_2000_count_original
							, @deno_100_priority
							, @deno_200_priority
							, @deno_500_priority
							, @deno_2000_priority
							, @cassette_100_brand_capacity
							, @cassette_200_brand_capacity
							, @cassette_500_brand_capacity
							, @cassette_2000_brand_capacity
					WHILE @@FETCH_STATUS=0
					BEGIN			---- Cursor 2 Begin
						DECLARE @forecasted_remaining_amt_100  bigint
							, @forecasted_remaining_amt_200  bigint
							, @forecasted_remaining_amt_500  bigint
							, @forecasted_remaining_amt_2000 bigint

							, @total_forecasted_remaining_amt BIGINT
							, @remaining_capacity_amount_100  bigint
							, @remaining_capacity_amount_200  bigint
							, @remaining_capacity_amount_500  bigint
							, @remaining_capacity_amount_2000 bigint
							, @total_remaining_capacity_amount BIGINT

							, @total_opening_remaining_available_amount BIGINT
							, @opening_remaining_available_amount_100 BIGINT
							, @opening_remaining_available_amount_200 BIGINT
							, @opening_remaining_available_amount_500 BIGINT
							, @opening_remaining_available_amount_2000 BIGINT


						SET  @total_opening_remaining_available_amount = @total_remaining_available_amount
						SET	 @opening_remaining_available_amount_100 =  @remaining_avail_100
						SET	 @opening_remaining_available_amount_200 =  @remaining_avail_200
						SET	 @opening_remaining_available_amount_500 = @remaining_avail_500
						SET	 @opening_remaining_available_amount_2000 = @remaining_avail_2000

						set @forecasted_remaining_amt_100 = @original_forecasted_amt_100
						SET	@forecasted_remaining_amt_200 = @original_forecasted_amt_200
						SET	@forecasted_remaining_amt_500 = @original_forecasted_amt_500
						SET	@forecasted_remaining_amt_2000 = @original_forecasted_amt_2000
						SET @total_forecasted_remaining_amt = @original_total_forecasted_amt
						SET @remaining_capacity_amount_100   = @remaining_capacity_amount_100
						SET @remaining_capacity_amount_200   = @remaining_capacity_amount_200
						SET @remaining_capacity_amount_500  = @remaining_capacity_amount_500
						SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000
						SET @total_remaining_capacity_amount = @total_remaining_capacity_amount

						-- GET TOTAL CAPACITY AMOUNT
						DECLARE @total_capacity_amount_100 bigint
							, @total_capacity_amount_200 bigint
							, @total_capacity_amount_500 bigint 
							, @total_capacity_amount_2000 bigint

						SET @total_capacity_amount_100 = @cassette_100_count_original * @cassette_100_brand_capacity * 100
						SET @total_capacity_amount_200 = @cassette_200_count_original * @cassette_200_brand_capacity * 200
						SET @total_capacity_amount_500 = @cassette_500_count_original * @cassette_500_brand_capacity * 500
						SET @total_capacity_amount_2000 = @cassette_2000_count_original * @cassette_2000_brand_capacity * 2000
						
						
						-- GET TOTAL CAPACITY AMOUNT LIMIT TO CASSETTE CAPACITY PERCENTAGE

						DECLARE @max_amt_allowed_100 BIGINT,
								@max_amt_allowed_200 BIGINT,
								@max_amt_allowed_500 BIGINT,
								@max_amt_allowed_2000 BIGINT

						SET @max_amt_allowed_100 =  @total_capacity_amount_100 * @denomination_100_max_capacity_percentage / 100
						SET @max_amt_allowed_200 =  @total_capacity_amount_200 * @denomination_200_max_capacity_percentage / 100
						SET @max_amt_allowed_500 =  @total_capacity_amount_500 * @denomination_500_max_capacity_percentage / 100
						SET @max_amt_allowed_2000 =  @total_capacity_amount_2000 * @denomination_2000_max_capacity_percentage / 100

						-- 
						--DECLARE @max_amt_capacity_100 BIGINT,
						--		@max_amt_capacity_200 BIGINT,
						--		@max_amt_capacity_500 BIGINT,
						--		@max_amt_capacity_2000 BIGINT

						---- Calculate Maximum capacity amount
						--set @max_amt_capacity_100 = @total_capacity_amount_100  *  @denomination_100_max_capacity_percentage / 100
						--set @max_amt_capacity_200 = @total_capacity_amount_200  *  @denomination_200_max_capacity_percentage / 100
						--set @max_amt_capacity_500 = @total_capacity_amount_500  *  @denomination_500_max_capacity_percentage / 100
						--set @max_amt_capacity_2000 = @total_capacity_amount_2000  *  @denomination_2000_max_capacity_percentage / 100						

						-- Calculate  Deduct Morning balance
						DECLARE @tentative_loading_100 BIGINT,
								@tentative_loading_200 BIGINT,
								@tentative_loading_500 BIGINT,
								@tentative_loading_2000 BIGINT

						SET @tentative_loading_100   =  @max_amt_allowed_100 - @morning_balance_100
						SET @tentative_loading_200   =  @max_amt_allowed_200 - @morning_balance_200
						SET @tentative_loading_500   =  @max_amt_allowed_500 - @morning_balance_500
						SET @tentative_loading_2000  =  @max_amt_allowed_2000 - @morning_balance_2000

						--- Calculate rounded tentative loading

						DECLARE @rounded_tentative_loading_100 BIGINT,
								@rounded_tentative_loading_200 BIGINT,
								@rounded_tentative_loading_500 BIGINT,
								@rounded_tentative_loading_2000 BIGINT

						SET @rounded_tentative_loading_100   =   @tentative_loading_100 - (@tentative_loading_100 % @denomination_wise_round_off_100 )
						SET @rounded_tentative_loading_200   =   @tentative_loading_200 - (@tentative_loading_200 % @denomination_wise_round_off_200 )
						SET @rounded_tentative_loading_500   =   @tentative_loading_500 - (@tentative_loading_500 % @denomination_wise_round_off_500 )
						SET @rounded_tentative_loading_2000   =   @tentative_loading_2000 - (@tentative_loading_2000 % @denomination_wise_round_off_2000 )

						DECLARE @priority_1_is_denomination_100  BIGINT = 0,
									@priority_1_is_denomination_200 BIGINT = 0,
									@priority_1_is_denomination_500 BIGINT = 0,
									@priority_1_is_denomination_2000 BIGINT = 0,
									@priority_1_is_remaining_amount_available_100 BIGINT = 0,
									@priority_1_is_remaining_amount_available_200 BIGINT = 0,
									@priority_1_is_remaining_amount_available_500 BIGINT = 0,
									@priority_1_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_1_loading_amount_100 BIGINT = 0,
									@priority_1_loading_amount_200 BIGINT = 0,
									@priority_1_loading_amount_500 BIGINT = 0,
									@priority_1_loading_amount_2000 BIGINT = 0,
									@priority_2_is_denomination_100  BIGINT = 0,
									@priority_2_is_denomination_200 BIGINT = 0,
									@priority_2_is_denomination_500 BIGINT = 0,
									@priority_2_is_denomination_2000 BIGINT = 0,
									@priority_2_is_remaining_amount_available_100 BIGINT = 0,
									@priority_2_is_remaining_amount_available_200 BIGINT = 0,
									@priority_2_is_remaining_amount_available_500 BIGINT = 0,
									@priority_2_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_2_loading_amount_100 BIGINT = 0,
									@priority_2_loading_amount_200 BIGINT = 0,
									@priority_2_loading_amount_500 BIGINT = 0,
									@priority_2_loading_amount_2000 BIGINT = 0,
									@priority_3_is_denomination_100  BIGINT = 0,
									@priority_3_is_denomination_200 BIGINT = 0,
									@priority_3_is_denomination_500 BIGINT = 0,
									@priority_3_is_denomination_2000 BIGINT = 0,
									@priority_3_is_remaining_amount_available_100 BIGINT = 0,
									@priority_3_is_remaining_amount_available_200 BIGINT = 0,
									@priority_3_is_remaining_amount_available_500 BIGINT = 0,
									@priority_3_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_3_loading_amount_100 BIGINT = 0,
									@priority_3_loading_amount_200 BIGINT = 0,
									@priority_3_loading_amount_500 BIGINT = 0,
									@priority_3_loading_amount_2000 BIGINT = 0,
									@priority_4_is_denomination_100  BIGINT = 0,
									@priority_4_is_denomination_200 BIGINT = 0,
									@priority_4_is_denomination_500 BIGINT = 0,
									@priority_4_is_denomination_2000 BIGINT = 0,
									@priority_4_is_remaining_amount_available_100 BIGINT = 0,
									@priority_4_is_remaining_amount_available_200 BIGINT = 0,
									@priority_4_is_remaining_amount_available_500 BIGINT = 0,
									@priority_4_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_4_loading_amount_100 BIGINT = 0,
									@priority_4_loading_amount_200 BIGINT = 0,
									@priority_4_loading_amount_500 BIGINT = 0,
									@priority_4_loading_amount_2000 BIGINT = 0
							

							SET @loading_amount_100 = 0
							SET @loading_amount_200 = 0
							SET @loading_amount_500 = 0
							SET @loading_amount_2000 = 0
		

						IF(@is_deno_wise_cash_available = 1)
						BEGIN
							-- CONTINUE FOR ALLOCATION LOGIC
							
							--SELECT 'total forecasted remaining amt' + cast(@total_forecasted_remaining_amt as nvarchar(max))
							--select @deno_100_priority
							--SELECT @deno_200_priority
							--SELECT @deno_500_priority
							--SELECT @deno_2000_priority
							--SELECT @total_forecasted_remaining_amt
							--SELECT @remaining_avail_200
							--SELECT @rounded_tentative_loading_200
							--SELECT @priority_1_max_loading_capacity_amount_200
							IF (@total_forecasted_remaining_amt > 0)
							BEGIN
								IF (@deno_100_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_100 = 1
											SET @priority_1_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_1_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_100 = @priority_1_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_100 = @priority_1_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_1_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_1_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 1)

								ELSE IF (@deno_200_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_200 = 1
											SET @priority_1_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_1_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_200 = @priority_1_max_loading_capacity_amount_200
												--SELECT @priority_1_loading_amount_200
											END	-- END of @remaining_avail_200 > @priority_1_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_200 = @remaining_avail_200
												--SELECT @priority_1_loading_amount_200
											END		-- END of else
											--SELECT @priority_1_loading_amount_200
											--SELECT @priority_2_loading_amount_200
											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_200 = @priority_1_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_1_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_1_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 1)
									ELSE IF (@deno_500_priority = 1)
									BEGIN
										SET @priority_1_is_denomination_500 = 1
										IF (@remaining_avail_500 > 0)
										BEGIN
											SET @priority_1_is_remaining_amount_available_500 = 1
											IF (@rounded_tentative_loading_500 > 0 )
											BEGIN
												SET @priority_1_is_remaining_capacity_available_500 = 1
												SET @priority_1_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
												IF @remaining_avail_500 > @priority_1_max_loading_capacity_amount_500
												BEGIN
													-- assigining amount as per available capacity
													SET @priority_1_loading_amount_500 = @priority_1_max_loading_capacity_amount_500
												END
												ELSE
												BEGIN
													--	When available amount is less than capacity then allocate only available amount 
													SET @priority_1_loading_amount_500 = @remaining_avail_500
												END

												--Loading for priority 1 done
												--deduct from available amount
												SET @loading_amount_500 = @priority_1_loading_amount_500
												SET @remaining_avail_500 = @remaining_avail_500 - @priority_1_loading_amount_500
												SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_500
												SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_500
												SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_500
												SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_1_loading_amount_500
											
											END	-- END OF  (@rounded_tentative_loading_500 > 0 )
										END		-- END of (@remaining_avail_500 > 0)
										END		-- END OF (@deno_500_priority = 1)
								ELSE IF (@deno_2000_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_2000 = 1
											SET @priority_1_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_1_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_2000 = @priority_1_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_2000 = @priority_1_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_1_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_1_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 1)
					
		  -------------------------------------- Checking for 2nd priority denomination --------------------------------- 
								IF (@deno_100_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_100 = 1
											SET @priority_2_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_2_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_100 = @priority_2_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_2_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_2_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_2_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 2)

								ELSE IF (@deno_200_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_200 = 1
											SET @priority_2_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_2_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_200 = @priority_2_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_2_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_2_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_2_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_2_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 2)
								ELSE IF (@deno_500_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_500 = 1
											SET @priority_2_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF @remaining_avail_500 > @priority_2_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_500 = @priority_2_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_2_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_2_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_2_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 2)
								ELSE IF (@deno_2000_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_2000 = 1
											SET @priority_2_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_2_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_2000 = @priority_2_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_2_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_2_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_2_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 2)
			
			
			--------------------------------- Checking for 3rd priority denomination --------------------------------- 
								IF (@deno_100_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_100 = 1
											SET @priority_3_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_3_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_100 = @priority_3_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 3 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_3_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_3_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_3_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 3)

								ELSE IF (@deno_200_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_200 = 1
											SET @priority_3_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_3_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_200 = @priority_3_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_3_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_3_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_3_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_3_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 3)
								ELSE IF (@deno_500_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_500 = 1
											SET @priority_3_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF @remaining_avail_500 > @priority_3_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_500 = @priority_3_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 3 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_3_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_3_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_3_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 3)
								ELSE IF (@deno_2000_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_2000 = 1
											SET @priority_3_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_3_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_2000 = @priority_3_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_3_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_3_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_3_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 3)
							
							
				---------------------------------------- Checking for 4th priority denomination --------------------------------- 
								IF (@deno_100_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_100 = 1
											SET @priority_4_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_4_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_100 = @priority_4_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 4 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_4_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_4_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_4_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 4)

								ELSE IF (@deno_200_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_200 = 1
											SET @priority_4_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_4_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_200 = @priority_4_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_4_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 4 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_4_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_4_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_4_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 4)
								ELSE IF (@deno_500_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_500 = 1
											SET @priority_4_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF @remaining_avail_500 > @priority_4_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_500 = @priority_4_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_4_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_4_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_4_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 4)
								ELSE IF (@deno_2000_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_2000 = 1
											SET @priority_4_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_4_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_2000 = @priority_4_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_4_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_4_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_4_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 4)


							-- INSERT INTO TEMP TABLE
							INSERT INTO #temp_cash_pre_availability
							(
										project_id
									,	bank_code
									,   feeder_branch_code
									,	atmid
									,	total_opening_remaining_available_amount
									,	opening_remaining_available_amount_100
									,	opening_remaining_available_amount_200
									,	opening_remaining_available_amount_500
									,	opening_remaining_available_amount_2000
									,	original_total_forecasted_amt
									,	original_forecasted_amt_100
									,	original_forecasted_amt_200
									,	original_forecasted_amt_500
									,	original_forecasted_amt_2000
									,	morning_balance_100
									,	morning_balance_200
									,	morning_balance_500
									,	morning_balance_2000
									,	total_morning_balance
									,	cassette_100_count_original
									,	cassette_200_count_original
									,	cassette_500_count_original
									,	cassette_2000_count_original
									,	cassette_100_brand_capacity
									,	cassette_200_brand_capacity
									,	cassette_500_brand_capacity
									,	cassette_2000_brand_capacity
									,	total_capacity_amount_100
									,	total_capacity_amount_200
									,	total_capacity_amount_500
									,	total_capacity_amount_2000
									,	denomination_100_max_capacity_percentage
									,	denomination_200_max_capacity_percentage
									,	denomination_500_max_capacity_percentage
									,	denomination_2000_max_capacity_percentage
									,	max_amt_allowed_100
									,	max_amt_allowed_200
									,	max_amt_allowed_500
									,	max_amt_allowed_2000
									,	denomination_wise_round_off_100
									,	denomination_wise_round_off_200
									,	denomination_wise_round_off_500
									,	denomination_wise_round_off_2000
									,	tentative_loading_100
									,	tentative_loading_200
									,	tentative_loading_500
									,	tentative_loading_2000
									,	rounded_tentative_loading_100
									,	rounded_tentative_loading_200
									,	rounded_tentative_loading_500
									,	rounded_tentative_loading_2000
									,	deno_100_priority
									,	deno_200_priority
									,	deno_500_priority
									,	deno_2000_priority
									,	is_deno_wise_cash_available
									,	priority_1_is_denomination_100
									,	priority_1_is_denomination_200
									,	priority_1_is_denomination_500
									,	priority_1_is_denomination_2000
									,	priority_1_is_remaining_amount_available_100
									,	priority_1_is_remaining_amount_available_200
									,	priority_1_is_remaining_amount_available_500
									,	priority_1_is_remaining_amount_available_2000
									,	priority_1_is_remaining_capacity_available_100
									,	priority_1_is_remaining_capacity_available_200
									,	priority_1_is_remaining_capacity_available_500
									,	priority_1_is_remaining_capacity_available_2000
									,	priority_1_loading_amount_100
									,	priority_1_loading_amount_200
									,	priority_1_loading_amount_500
									,	priority_1_loading_amount_2000
									,	priority_2_is_denomination_100
									,	priority_2_is_denomination_200
									,	priority_2_is_denomination_500
									,	priority_2_is_denomination_2000
									,	priority_2_is_remaining_amount_available_100
									,	priority_2_is_remaining_amount_available_200
									,	priority_2_is_remaining_amount_available_500
									,	priority_2_is_remaining_amount_available_2000
									,	priority_2_is_remaining_capacity_available_100
									,	priority_2_is_remaining_capacity_available_200
									,	priority_2_is_remaining_capacity_available_500
									,	priority_2_is_remaining_capacity_available_2000
									,	priority_2_loading_amount_100
									,	priority_2_loading_amount_200
									,	priority_2_loading_amount_500
									,	priority_2_loading_amount_2000
									,	priority_3_is_denomination_100
									,	priority_3_is_denomination_200
									,	priority_3_is_denomination_500
									,	priority_3_is_denomination_2000
									,	priority_3_is_remaining_amount_available_100
									,	priority_3_is_remaining_amount_available_200
									,	priority_3_is_remaining_amount_available_500
									,	priority_3_is_remaining_amount_available_2000
									,	priority_3_is_remaining_capacity_available_100
									,	priority_3_is_remaining_capacity_available_200
									,	priority_3_is_remaining_capacity_available_500
									,	priority_3_is_remaining_capacity_available_2000
									,	priority_3_loading_amount_100
									,	priority_3_loading_amount_200
									,	priority_3_loading_amount_500
									,	priority_3_loading_amount_2000
									,	priority_4_is_denomination_100
									,	priority_4_is_denomination_200
									,	priority_4_is_denomination_500
									,	priority_4_is_denomination_2000
									,	priority_4_is_remaining_amount_available_100
									,	priority_4_is_remaining_amount_available_200
									,	priority_4_is_remaining_amount_available_500
									,	priority_4_is_remaining_amount_available_2000
									,	priority_4_is_remaining_capacity_available_100
									,	priority_4_is_remaining_capacity_available_200
									,	priority_4_is_remaining_capacity_available_500
									,	priority_4_is_remaining_capacity_available_2000
									,	priority_4_loading_amount_100
									,	priority_4_loading_amount_200
									,	priority_4_loading_amount_500
									,	priority_4_loading_amount_2000
									,	loading_amount_100
									,	loading_amount_200
									,	loading_amount_500
									,	loading_amount_2000
									,	total_loading_amount
									,	remaining_capacity_amount_100
									,	remaining_capacity_amount_200
									,	remaining_capacity_amount_500
									,	remaining_capacity_amount_2000
									,   closing_remaining_available_amount_100
									,   closing_remaining_available_amount_200
									,   closing_remaining_available_amount_500
									,   closing_remaining_available_amount_2000
									,   total_closing_remaining_available_amount									
									,	total_forecasted_remaining_amt
							)
							SELECT  
									    @project_id								                                                            as project_id								
								   , @bank_code									                                                            as bank_code									
								   , @feederbranchcode																						as feeder_branch_code
								   , @atmid										                                                            as atmid										
									, @total_opening_remaining_available_amount																as total_opening_remaining_available_amount
								   , @opening_remaining_available_amount_100																as opening_remaining_available_amount_100	
								   , @opening_remaining_available_amount_200	                                                            as opening_remaining_available_amount_200	
								   , @opening_remaining_available_amount_500	                                                            as opening_remaining_available_amount_500	
								   , @opening_remaining_available_amount_2000	                                                            as opening_remaining_available_amount_2000	
								
								   , @original_total_forecasted_amt							                                                as original_total_forecasted_amt							
								   , @original_forecasted_amt_100				                                                            as original_forecasted_amt_100				
								   , @original_forecasted_amt_200                                                                           as original_forecasted_amt_200
								   , @original_forecasted_amt_500                                                                           as original_forecasted_amt_500
								   , @original_forecasted_amt_2000                                                                          as original_forecasted_amt_2000
								   , @morning_balance_100                                                                                   as morning_balance_100
								   , @morning_balance_200                                                                                   as morning_balance_200
								   , @morning_balance_500                                                                                   as morning_balance_500
								   , @morning_balance_2000                                                                                  as morning_balance_2000
								   , @total_morning_balance                                                                                 as total_morning_balance
								   , @cassette_100_count_original                                                                           as cassette_100_count_original
								   , @cassette_200_count_original                                                                           as cassette_200_count_original
								   , @cassette_500_count_original                                                                           as cassette_500_count_original
								   , @cassette_2000_count_original                                                                          as cassette_2000_count_original
								   , @cassette_100_brand_capacity                                                                           as cassette_100_brand_capacity
								   , @cassette_200_brand_capacity                                                                           as cassette_200_brand_capacity
								   , @cassette_500_brand_capacity                                                                           as cassette_500_brand_capacity
								   , @cassette_2000_brand_capacity                                                                          as cassette_2000_brand_capacity
								   , @total_capacity_amount_100                                                                             as total_capacity_amount_100
								   , @total_capacity_amount_200                                                                             as total_capacity_amount_200
								   , @total_capacity_amount_500                                                                             as total_capacity_amount_500
								   , @total_capacity_amount_2000                                                                            as total_capacity_amount_2000
								   , @denomination_100_max_capacity_percentage                                                              as denomination_100_max_capacity_percentage
								   , @denomination_200_max_capacity_percentage                                                              as denomination_200_max_capacity_percentage
								   , @denomination_500_max_capacity_percentage                                                              as denomination_500_max_capacity_percentage
								   , @denomination_2000_max_capacity_percentage                                                             as denomination_2000_max_capacity_percentage
								   , @max_amt_allowed_100                                                                                   as max_amt_allowed_100
								   , @max_amt_allowed_200                                                                                   as max_amt_allowed_200
								   , @max_amt_allowed_500                                                                                   as max_amt_allowed_500
								   , @max_amt_allowed_2000                                                                                  as max_amt_allowed_2000
								   , @denomination_wise_round_off_100                                                                       as denomination_wise_round_off_100
								   , @denomination_wise_round_off_200                                                                       as denomination_wise_round_off_200
								   , @denomination_wise_round_off_500                                                                       as denomination_wise_round_off_500
								   , @denomination_wise_round_off_2000                                                                      as denomination_wise_round_off_2000
								   , @tentative_loading_100                                                                                 as tentative_loading_100 
								   , @tentative_loading_200                                                                                 as tentative_loading_200 
								   , @tentative_loading_500                                                                                 as tentative_loading_500 
								   , @tentative_loading_2000                                                                                as tentative_loading_2000
								   -- Max Capacity                                                                                          
								   , @rounded_tentative_loading_100		                                                                    as rounded_tentative_loading_100		
								   , @rounded_tentative_loading_200	                                                                        as rounded_tentative_loading_200	    
								   , @rounded_tentative_loading_500	                                                                        as rounded_tentative_loading_500	    
								   , @rounded_tentative_loading_2000	                                                                    as rounded_tentative_loading_2000	
								   , @deno_100_priority                                                                                     as deno_100_priority
								   , @deno_200_priority                                                                                     as deno_200_priority
								   , @deno_500_priority                                                                                     as deno_500_priority
								   , @deno_2000_priority                                                                                    as deno_2000_priority
								   , @is_deno_wise_cash_available                                                                           as is_deno_wise_cash_available
									---- If denomination wise is availabble                                                                 
									-- Priority of denomination                                                                             
								   , @priority_1_is_denomination_100                                                                        as priority_1_is_denomination_100
								   , @priority_1_is_denomination_200                                                                        as priority_1_is_denomination_200
								   , @priority_1_is_denomination_500                                                                        as priority_1_is_denomination_500
								   , @priority_1_is_denomination_2000 								   	                                    as priority_1_is_denomination_2000 								   	
								   , @priority_1_is_remaining_amount_available_100                                                          as priority_1_is_remaining_amount_available_100
								   , @priority_1_is_remaining_amount_available_200                                                          as priority_1_is_remaining_amount_available_200
								   , @priority_1_is_remaining_amount_available_500                                                          as priority_1_is_remaining_amount_available_500
								   , @priority_1_is_remaining_amount_available_2000                                                         as priority_1_is_remaining_amount_available_2000
								   , @priority_1_is_remaining_capacity_available_100                                                        as priority_1_is_remaining_capacity_available_100
								   , @priority_1_is_remaining_capacity_available_200                                                        as priority_1_is_remaining_capacity_available_200
								   , @priority_1_is_remaining_capacity_available_500                                                        as priority_1_is_remaining_capacity_available_500
								   , @priority_1_is_remaining_capacity_available_2000								                        as priority_1_is_remaining_capacity_available_2000								
								   , @priority_1_loading_amount_100                                                                         as priority_1_loading_amount_100
								   , @priority_1_loading_amount_200                                                                         as priority_1_loading_amount_200
								   , @priority_1_loading_amount_500                                                                         as priority_1_loading_amount_500
								   , @priority_1_loading_amount_2000								                                        as priority_1_loading_amount_2000								   
								   , @priority_2_is_denomination_100                                                                        as priority_2_is_denomination_100
								   , @priority_2_is_denomination_200                                                                        as priority_2_is_denomination_200
								   , @priority_2_is_denomination_500                                                                        as priority_2_is_denomination_500
								   , @priority_2_is_denomination_2000 								                                        as priority_2_is_denomination_2000 								   
								   , @priority_2_is_remaining_amount_available_100                                                          as priority_2_is_remaining_amount_available_100
								   , @priority_2_is_remaining_amount_available_200                                                          as priority_2_is_remaining_amount_available_200
								   , @priority_2_is_remaining_amount_available_500                                                          as priority_2_is_remaining_amount_available_500
								   , @priority_2_is_remaining_amount_available_2000                                                         as priority_2_is_remaining_amount_available_2000
								   , @priority_2_is_remaining_capacity_available_100                                                        as priority_2_is_remaining_capacity_available_100
								   , @priority_2_is_remaining_capacity_available_200                                                        as priority_2_is_remaining_capacity_available_200
								   , @priority_2_is_remaining_capacity_available_500                                                        as priority_2_is_remaining_capacity_available_500
								   , @priority_2_is_remaining_capacity_available_2000  								                        as priority_2_is_remaining_capacity_available_2000  								
								   , @priority_2_loading_amount_100                                                                         as priority_2_loading_amount_100
								   , @priority_2_loading_amount_200                                                                         as priority_2_loading_amount_200
								   , @priority_2_loading_amount_500                                                                         as priority_2_loading_amount_500
								   , @priority_2_loading_amount_2000								                                        as priority_2_loading_amount_2000								   
								   , @priority_3_is_denomination_100                                                                        as priority_3_is_denomination_100
								   , @priority_3_is_denomination_200                                                                        as priority_3_is_denomination_200
								   , @priority_3_is_denomination_500                                                                        as priority_3_is_denomination_500
								   , @priority_3_is_denomination_2000 								                                        as priority_3_is_denomination_2000 								   
								   , @priority_3_is_remaining_amount_available_100                                                          as priority_3_is_remaining_amount_available_100
								   , @priority_3_is_remaining_amount_available_200                                                          as priority_3_is_remaining_amount_available_200
								   , @priority_3_is_remaining_amount_available_500                                                          as priority_3_is_remaining_amount_available_500
								   , @priority_3_is_remaining_amount_available_2000								                            as priority_3_is_remaining_amount_available_2000								   
								   , @priority_3_is_remaining_capacity_available_100                                                        as priority_3_is_remaining_capacity_available_100
								   , @priority_3_is_remaining_capacity_available_200                                                        as priority_3_is_remaining_capacity_available_200
								   , @priority_3_is_remaining_capacity_available_500                                                        as priority_3_is_remaining_capacity_available_500
								   , @priority_3_is_remaining_capacity_available_2000  						                                as priority_3_is_remaining_capacity_available_2000  						
								   , @priority_3_loading_amount_100                                                                         as priority_3_loading_amount_100
								   , @priority_3_loading_amount_200                                                                         as priority_3_loading_amount_200
								   , @priority_3_loading_amount_500                                                                         as priority_3_loading_amount_500
								   , @priority_3_loading_amount_2000								                                        as priority_3_loading_amount_2000								   
								   , @priority_4_is_denomination_100                                                                        as priority_4_is_denomination_100
								   , @priority_4_is_denomination_200                                                                        as priority_4_is_denomination_200
								   , @priority_4_is_denomination_500                                                                        as priority_4_is_denomination_500
								   , @priority_4_is_denomination_2000 								                                        as priority_4_is_denomination_2000 								   
								   , @priority_4_is_remaining_amount_available_100                                                          as priority_4_is_remaining_amount_available_100
								   , @priority_4_is_remaining_amount_available_200                                                          as priority_4_is_remaining_amount_available_200
								   , @priority_4_is_remaining_amount_available_500                                                          as priority_4_is_remaining_amount_available_500
								   , @priority_4_is_remaining_amount_available_2000								                            as priority_4_is_remaining_amount_available_2000								   
								   , @priority_4_is_remaining_capacity_available_100                                                        as priority_4_is_remaining_capacity_available_100
								   , @priority_4_is_remaining_capacity_available_200                                                        as priority_4_is_remaining_capacity_available_200
								   , @priority_4_is_remaining_capacity_available_500                                                        as priority_4_is_remaining_capacity_available_500
								   , @priority_4_is_remaining_capacity_available_2000		                                                as priority_4_is_remaining_capacity_available_2000		
								   , @priority_4_loading_amount_100                                                                         as priority_4_loading_amount_100
								   , @priority_4_loading_amount_200                                                                         as priority_4_loading_amount_200
								   , @priority_4_loading_amount_500                                                                         as priority_4_loading_amount_500
								   , @priority_4_loading_amount_2000								                                        as priority_4_loading_amount_2000								   
								   , @loading_amount_100                                                                                    as loading_amount_100
								   , @loading_amount_200                                                                                    as loading_amount_200
								   , @loading_amount_500                                                                                    as loading_amount_500
								   , @loading_amount_2000                                                                                   as loading_amount_2000
									-- Total loading amount								                                                    
								   , @loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000                 as total_loading_amount
								   , @remaining_capacity_amount_100                                                                         as remaining_capacity_amount_100
								   , @remaining_capacity_amount_200                                                                         as remaining_capacity_amount_200
								   , @remaining_capacity_amount_500                                                                         as remaining_capacity_amount_500
								   , @remaining_capacity_amount_2000								                                        as remaining_capacity_amount_2000								   
								   --closing available amount	                                                                           
								   , @remaining_avail_100			                                                                        as closing_avail_100			
								   , @remaining_avail_200			                                                                        as closing_avail_200			
								   , @remaining_avail_500			                                                                        as closing_avail_500			
								   , @remaining_avail_2000                                                                                  as closing_avail_2000
								   , @total_remaining_available_amount                                                                      as total_closing_available_amount
								   , @total_forecasted_remaining_amt                                                                        as total_forecasted_remaining_amt

							END -- END OF (@total_forecasted_remaining_amt > 0)						

						 
										
									
							  
						END
						
						ELSE --If denoination wise cash is not available.
						BEGIN							-- Denomination wise not available BEGIN
							IF (@total_remaining_available_amount > @original_total_forecasted_amt)
							BEGIN						--
								SET @loading_amount_100 = @original_forecasted_amt_100
								SET @loading_amount_200 = @original_forecasted_amt_200
								SET @loading_amount_500 = @original_forecasted_amt_500
								SET @loading_amount_2000 = @original_forecasted_amt_2000								

								SET @total_remaining_available_amount = @total_remaining_available_amount - (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)
							END
							ELSE -- IF REMAINING AMOUNT IS LESS THAN FORCASTED AMOUNT
							BEGIN
								
								IF (@deno_100_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 1
										SET @priority_1_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 0
										SET @priority_1_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_1_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_1_loading_amount_100
								END		-- end of (@deno_100_priority = 1 )

								ELSE IF (@deno_200_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 1
										SET @priority_1_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 0
										SET @priority_1_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_1_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_1_loading_amount_200
								END		-- end of (@deno_200_priority = 1 )

								ELSE IF (@deno_500_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_500 = 1
										SET @priority_1_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_500 = 0
										SET @priority_1_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_1_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_1_loading_amount_500
								END		-- end of (@deno_500_priority = 1 )

								ELSE IF (@deno_2000_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 1
										SET @priority_1_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 0
										SET @priority_1_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_1_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_1_loading_amount_2000
								END		-- end of (@deno_2000_priority = 1 )
								----------------------------------Priority 2 start -------------------------------------------
								IF (@deno_100_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 1
										SET @priority_2_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 0
										SET @priority_2_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_2_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_2_loading_amount_100
								END		-- end of (@deno_100_priority = 2 )

								ELSE IF (@deno_200_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 1
										SET @priority_2_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 0
										SET @priority_2_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_2_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_2_loading_amount_200
								END		-- end of (@deno_200_priority = 2 )

								ELSE IF (@deno_500_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 1
										SET @priority_2_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 0
										SET @priority_2_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_2_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_2_loading_amount_500
								END		-- end of (@deno_500_priority = 2 )

								ELSE IF (@deno_2000_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 1
										SET @priority_2_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 0
										SET @priority_2_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_2_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_2_loading_amount_2000
								END		-- end of (@deno_2000_priority = 2 )

								---------------------------------Priority 3 start -------------------------------
								IF (@deno_100_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 1
										SET @priority_3_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 0
										SET @priority_3_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_3_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_3_loading_amount_100
								END		-- end of (@deno_100_priority = 3 )

								ELSE IF (@deno_200_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 1
										SET @priority_3_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 0
										SET @priority_3_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_3_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_3_loading_amount_200
								END		-- end of (@deno_200_priority = 1 )

								ELSE IF (@deno_500_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 1
										SET @priority_3_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 0
										SET @priority_3_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_3_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_3_loading_amount_500
								END		-- end of (@deno_500_priority = 3 )

								ELSE IF (@deno_2000_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 1
										SET @priority_3_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 0
										SET @priority_3_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_3_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_3_loading_amount_2000
								END		-- end of (@deno_2000_priority = 3 )

								----------------------------------priority 4 start------------------------------
								IF (@deno_100_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 1
										SET @priority_4_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 0
										SET @priority_4_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_4_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_4_loading_amount_100
								END		-- end of (@deno_100_priority = 4 )

								ELSE IF (@deno_200_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 1
										SET @priority_4_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 0
										SET @priority_4_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_4_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_4_loading_amount_200
								END		-- end of (@deno_200_priority = 4 )

								ELSE IF (@deno_500_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 1
										SET @priority_4_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 0
										SET @priority_4_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_4_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_4_loading_amount_500
								END		-- end of (@deno_500_priority = 4 )

								ELSE IF (@deno_2000_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 1
										SET @priority_4_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 0
										SET @priority_4_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_4_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_4_loading_amount_2000
								END		-- end of (@deno_2000_priority = 4 )
							END	-- END OF ELSE -- IF REMAINING AMOUNT IS LESS THAN FORCASTED AMOUNT

							-- insert into temp table
							
							INSERT INTO #temp_cash_pre_availability 
							(
													project_id
												,	bank_code
												,	feeder_branch_code
												,	atmid
												,	total_opening_remaining_available_amount
												,	opening_remaining_available_amount_100
												,	opening_remaining_available_amount_200
												,	opening_remaining_available_amount_500
												,	opening_remaining_available_amount_2000
												,	original_total_forecasted_amt
												,	original_forecasted_amt_100
												,	original_forecasted_amt_200
												,	original_forecasted_amt_500
												,	original_forecasted_amt_2000
												,	morning_balance_100
												,	morning_balance_200
												,	morning_balance_500
												,	morning_balance_2000
												,	total_morning_balance
												,	cassette_100_count_original
												,	cassette_200_count_original
												,	cassette_500_count_original
												,	cassette_2000_count_original
												,	cassette_100_brand_capacity
												,	cassette_200_brand_capacity
												,	cassette_500_brand_capacity
												,	cassette_2000_brand_capacity
												,	total_capacity_amount_100
												,	total_capacity_amount_200
												,	total_capacity_amount_500
												,	total_capacity_amount_2000
												,	denomination_100_max_capacity_percentage
												,	denomination_200_max_capacity_percentage
												,	denomination_500_max_capacity_percentage
												,	denomination_2000_max_capacity_percentage
												,	max_amt_allowed_100
												,	max_amt_allowed_200
												,	max_amt_allowed_500
												,	max_amt_allowed_2000
												,	denomination_wise_round_off_100
												,	denomination_wise_round_off_200
												,	denomination_wise_round_off_500
												,	denomination_wise_round_off_2000
												,	tentative_loading_100
												,	tentative_loading_200
												,	tentative_loading_500
												,	tentative_loading_2000
												,	rounded_tentative_loading_100
												,	rounded_tentative_loading_200
												,	rounded_tentative_loading_500
												,	rounded_tentative_loading_2000
												,	deno_100_priority
												,	deno_200_priority
												,	deno_500_priority
												,	deno_2000_priority
												,	is_deno_wise_cash_available
												,	priority_1_is_denomination_100
												,	priority_1_is_denomination_200
												,	priority_1_is_denomination_500
												,	priority_1_is_denomination_2000
												,	priority_1_is_remaining_amount_available_100
												,	priority_1_is_remaining_amount_available_200
												,	priority_1_is_remaining_amount_available_500
												,	priority_1_is_remaining_amount_available_2000
												,	priority_1_loading_amount_100
												,	priority_1_loading_amount_200
												,	priority_1_loading_amount_500
												,	priority_1_loading_amount_2000
												,	priority_2_is_denomination_100
												,	priority_2_is_denomination_200
												,	priority_2_is_denomination_500
												,	priority_2_is_denomination_2000
												,	priority_2_is_remaining_amount_available_100
												,	priority_2_is_remaining_amount_available_200
												,	priority_2_is_remaining_amount_available_500
												,	priority_2_is_remaining_amount_available_2000
												,	priority_2_loading_amount_100
												,	priority_2_loading_amount_200
												,	priority_2_loading_amount_500
												,	priority_2_loading_amount_2000
												,	priority_3_is_denomination_100
												,	priority_3_is_denomination_200
												,	priority_3_is_denomination_500
												,	priority_3_is_denomination_2000
												,	priority_3_is_remaining_amount_available_100
												,	priority_3_is_remaining_amount_available_200
												,	priority_3_is_remaining_amount_available_500
												,	priority_3_is_remaining_amount_available_2000
												,	priority_3_loading_amount_100
												,	priority_3_loading_amount_200
												,	priority_3_loading_amount_500
												,	priority_3_loading_amount_2000
												,	priority_4_is_denomination_100
												,	priority_4_is_denomination_200
												,	priority_4_is_denomination_500
												,	priority_4_is_denomination_2000
												,	priority_4_is_remaining_amount_available_100
												,	priority_4_is_remaining_amount_available_200
												,	priority_4_is_remaining_amount_available_500
												,	priority_4_is_remaining_amount_available_2000
												,	priority_4_loading_amount_100
												,	priority_4_loading_amount_200
												,	priority_4_loading_amount_500
												,	priority_4_loading_amount_2000
												,	loading_amount_100
												,	loading_amount_200
												,	loading_amount_500
												,	loading_amount_2000
												,	total_loading_amount
												,	total_closing_remaining_available_amount
												,	total_forecasted_remaining_amt
			)
			SELECT
									 @project_id								                                                            as project_id								
								   , @bank_code									                                                            as bank_code									
								   , @feederbranchcode																						as feeder_branch_code
								   , @atmid										                                                            as atmid
								   , @total_opening_remaining_available_amount																as total_opening_remaining_available_amount
								   , @opening_remaining_available_amount_100																as opening_remaining_available_amount_100	
								   , @opening_remaining_available_amount_200	                                                            as opening_remaining_available_amount_200	
								   , @opening_remaining_available_amount_500	                                                            as opening_remaining_available_amount_500	
								   , @opening_remaining_available_amount_2000	                                                            as opening_remaining_available_amount_2000											
								   , @original_total_forecasted_amt							                                                as original_total_forecasted_amt							
								   , @original_forecasted_amt_100				                                                            as original_forecasted_amt_100				
								   , @original_forecasted_amt_200                                                                           as original_forecasted_amt_200
								   , @original_forecasted_amt_500                                                                           as original_forecasted_amt_500
								   , @original_forecasted_amt_2000                                                                          as original_forecasted_amt_2000
								   , @morning_balance_100                                                                                   as morning_balance_100
								   , @morning_balance_200                                                                                   as morning_balance_200
								   , @morning_balance_500                                                                                   as morning_balance_500
								   , @morning_balance_2000                                                                                  as morning_balance_2000
								   , @total_morning_balance                                                                                 as total_morning_balance
								   , @cassette_100_count_original                                                                           as cassette_100_count_original
								   , @cassette_200_count_original                                                                           as cassette_200_count_original
								   , @cassette_500_count_original                                                                           as cassette_500_count_original
								   , @cassette_2000_count_original                                                                          as cassette_2000_count_original
								   , @cassette_100_brand_capacity                                                                           as cassette_100_brand_capacity
								   , @cassette_200_brand_capacity                                                                           as cassette_200_brand_capacity
								   , @cassette_500_brand_capacity                                                                           as cassette_500_brand_capacity
								   , @cassette_2000_brand_capacity                                                                          as cassette_2000_brand_capacity
								   , @total_capacity_amount_100                                                                             as total_capacity_amount_100
								   , @total_capacity_amount_200                                                                             as total_capacity_amount_200
								   , @total_capacity_amount_500                                                                             as total_capacity_amount_500
								   , @total_capacity_amount_2000                                                                            as total_capacity_amount_2000
								   , @denomination_100_max_capacity_percentage                                                              as denomination_100_max_capacity_percentage
								   , @denomination_200_max_capacity_percentage                                                              as denomination_200_max_capacity_percentage
								   , @denomination_500_max_capacity_percentage                                                              as denomination_500_max_capacity_percentage
								   , @denomination_2000_max_capacity_percentage                                                             as denomination_2000_max_capacity_percentage
								   , @max_amt_allowed_100                                                                                   as max_amt_allowed_100
								   , @max_amt_allowed_200                                                                                   as max_amt_allowed_200
								   , @max_amt_allowed_500                                                                                   as max_amt_allowed_500
								   , @max_amt_allowed_2000                                                                                  as max_amt_allowed_2000
								   , @denomination_wise_round_off_100                                                                       as denomination_wise_round_off_100
								   , @denomination_wise_round_off_200                                                                       as denomination_wise_round_off_200
								   , @denomination_wise_round_off_500                                                                       as denomination_wise_round_off_500
								   , @denomination_wise_round_off_2000                                                                      as denomination_wise_round_off_2000
								   , @tentative_loading_100                                                                                 as tentative_loading_100 
								   , @tentative_loading_200                                                                                 as tentative_loading_200 
								   , @tentative_loading_500                                                                                 as tentative_loading_500 
								   , @tentative_loading_2000                                                                                as tentative_loading_2000
								   -- Max Capacity                                                                                          
								   , @rounded_tentative_loading_100		                                                                    as rounded_tentative_loading_100		
								   , @rounded_tentative_loading_200	                                                                        as rounded_tentative_loading_200	    
								   , @rounded_tentative_loading_500	                                                                        as rounded_tentative_loading_500	    
								   , @rounded_tentative_loading_2000	                                                                    as rounded_tentative_loading_2000	
								   , @deno_100_priority                                                                                     as deno_100_priority
								   , @deno_200_priority                                                                                     as deno_200_priority
								   , @deno_500_priority                                                                                     as deno_500_priority
								   , @deno_2000_priority                                                                                    as deno_2000_priority
								   , @is_deno_wise_cash_available                                                                           as is_deno_wise_cash_available
									---- If denomination wise is availabble                                                                 
									-- Priority of denomination                                                                             
								   , @priority_1_is_denomination_100                                                                        as priority_1_is_denomination_100
								   , @priority_1_is_denomination_200                                                                        as priority_1_is_denomination_200
								   , @priority_1_is_denomination_500                                                                        as priority_1_is_denomination_500
								   , @priority_1_is_denomination_2000 								   	                                    as priority_1_is_denomination_2000 								   	
								   , @priority_1_is_remaining_amount_available_100                                                          as priority_1_is_remaining_amount_available_100
								   , @priority_1_is_remaining_amount_available_200                                                          as priority_1_is_remaining_amount_available_200
								   , @priority_1_is_remaining_amount_available_500                                                          as priority_1_is_remaining_amount_available_500
								   , @priority_1_is_remaining_amount_available_2000                                                         as priority_1_is_remaining_amount_available_2000
								   , @priority_1_loading_amount_100                                                                         as priority_1_loading_amount_100
								   , @priority_1_loading_amount_200                                                                         as priority_1_loading_amount_200
								   , @priority_1_loading_amount_500                                                                         as priority_1_loading_amount_500
								   , @priority_1_loading_amount_2000								                                        as priority_1_loading_amount_2000								   
								   , @priority_2_is_denomination_100                                                                        as priority_2_is_denomination_100
								   , @priority_2_is_denomination_200                                                                        as priority_2_is_denomination_200
								   , @priority_2_is_denomination_500                                                                        as priority_2_is_denomination_500
								   , @priority_2_is_denomination_2000 								                                        as priority_2_is_denomination_2000 								   
								   , @priority_2_is_remaining_amount_available_100                                                          as priority_2_is_remaining_amount_available_100
								   , @priority_2_is_remaining_amount_available_200                                                          as priority_2_is_remaining_amount_available_200
								   , @priority_2_is_remaining_amount_available_500                                                          as priority_2_is_remaining_amount_available_500
								   , @priority_2_is_remaining_amount_available_2000                                                         as priority_2_is_remaining_amount_available_2000
								   , @priority_2_loading_amount_100                                                                         as priority_2_loading_amount_100
								   , @priority_2_loading_amount_200                                                                         as priority_2_loading_amount_200
								   , @priority_2_loading_amount_500                                                                         as priority_2_loading_amount_500
								   , @priority_2_loading_amount_2000								                                        as priority_2_loading_amount_2000								   
								   , @priority_3_is_denomination_100                                                                        as priority_3_is_denomination_100
								   , @priority_3_is_denomination_200                                                                        as priority_3_is_denomination_200
								   , @priority_3_is_denomination_500                                                                        as priority_3_is_denomination_500
								   , @priority_3_is_denomination_2000 								                                        as priority_3_is_denomination_2000 								   
								   , @priority_3_is_remaining_amount_available_100                                                          as priority_3_is_remaining_amount_available_100
								   , @priority_3_is_remaining_amount_available_200                                                          as priority_3_is_remaining_amount_available_200
								   , @priority_3_is_remaining_amount_available_500                                                          as priority_3_is_remaining_amount_available_500
								   , @priority_3_is_remaining_amount_available_2000								                            as priority_3_is_remaining_amount_available_2000								   
								   , @priority_3_loading_amount_100                                                                         as priority_3_loading_amount_100
								   , @priority_3_loading_amount_200                                                                         as priority_3_loading_amount_200
								   , @priority_3_loading_amount_500                                                                         as priority_3_loading_amount_500
								   , @priority_3_loading_amount_2000								                                        as priority_3_loading_amount_2000								   
								   , @priority_4_is_denomination_100                                                                        as priority_4_is_denomination_100
								   , @priority_4_is_denomination_200                                                                        as priority_4_is_denomination_200
								   , @priority_4_is_denomination_500                                                                        as priority_4_is_denomination_500
								   , @priority_4_is_denomination_2000 								                                        as priority_4_is_denomination_2000 								   
								   , @priority_4_is_remaining_amount_available_100                                                          as priority_4_is_remaining_amount_available_100
								   , @priority_4_is_remaining_amount_available_200                                                          as priority_4_is_remaining_amount_available_200
								   , @priority_4_is_remaining_amount_available_500                                                          as priority_4_is_remaining_amount_available_500
								   , @priority_4_is_remaining_amount_available_2000								                            as priority_4_is_remaining_amount_available_2000								   
								   , @priority_4_loading_amount_100                                                                         as priority_4_loading_amount_100
								   , @priority_4_loading_amount_200                                                                         as priority_4_loading_amount_200
								   , @priority_4_loading_amount_500                                                                         as priority_4_loading_amount_500
								   , @priority_4_loading_amount_2000								                                        as priority_4_loading_amount_2000								   
								   , @loading_amount_100                                                                                    as loading_amount_100
								   , @loading_amount_200                                                                                    as loading_amount_200
								   , @loading_amount_500                                                                                    as loading_amount_500
								   , @loading_amount_2000                                                                                   as loading_amount_2000
									-- Total loading amount								                                                    
								   , @loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000                 as total_loading_amount
								   --closing available amount	                                                                           
								   , @total_remaining_available_amount                                                                      as total_closing_remaining_available_amount
								   , @total_forecasted_remaining_amt                                                                        as total_forecasted_remaining_amt
				
						END	--ELSE --If denoination wise cash is not available.						
			
						FETCH NEXT FROM cursor2 
									 INTO  @atmid							
									, @denomination_100_max_capacity_percentage
									, @denomination_200_max_capacity_percentage
									, @denomination_500_max_capacity_percentage
									, @denomination_2000_max_capacity_percentage
									, @morning_balance_100
									, @morning_balance_200
									, @morning_balance_500
									, @morning_balance_2000
									, @total_morning_balance
									, @original_forecasted_amt_100
									, @original_forecasted_amt_200
									, @original_forecasted_amt_500
									, @original_forecasted_amt_2000
									, @original_total_forecasted_amt
									, @cassette_50_count_original  
									, @cassette_100_count_original 
									, @cassette_200_count_original 
									, @cassette_500_count_original 
									, @cassette_2000_count_original
									, @deno_100_priority
									, @deno_200_priority
									, @deno_500_priority
									, @deno_2000_priority
									, @cassette_100_brand_capacity
									, @cassette_200_brand_capacity
									, @cassette_500_brand_capacity
									, @cassette_2000_brand_capacity
					END			-- While End (cusror2)
				CLOSE cursor2 
				DEALLOCATE cursor2
			
		
			--- Insert in temp table for feeder 

				
				INSERT INTO #temp_feeder_forecast
				(
						project_id						
					,	bank_code						
					,	feeder_branch_code							
					,	is_deno_wise_cash_available		
					,	total_remaining_available_amount
					,   remaining_avail_100				
					,   remaining_avail_200				
					,   remaining_avail_500				
					,   remaining_avail_2000			
				)

				SELECT	  @project_id
						, @bank_code
						, @feederbranchcode
						, @is_deno_wise_cash_available
						, @total_remaining_available_amount
						, @remaining_avail_100			
						, @remaining_avail_200			
						, @remaining_avail_500			
						, @remaining_avail_2000			

	FETCH NEXT FROM cursor1 INTO @project_id,@bank_code,@feederbranchcode, @total_original_available_amount, @Original_avail_100, @Original_avail_200, @Original_avail_500, @Original_avail_2000,@is_deno_wise_cash_available
	END

	--SELECT @feederbranchcode

   CLOSE cursor1 
 DEALLOCATE cursor1
 
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Cursor completed for allocating amount'
 
	drop table if exists #forecasted_dataset

	-- Create final dataset for atm level here

	-- Add flag indicating if cash pre availability is there
	select a.*
			, case when b.Total_Amount_Available > 0 then 1 else 0 end as is_cash_pre_available
			, b.available_100_amount as		feeder_available_100_amount
			, b.available_200_amount as		feeder_available_200_amount
			, b.available_500_amount as		feeder_available_500_amount
			, b.available_2000_amount as	feeder_available_2000_amount
			, b.Total_Amount_Available as	feeder_total_amount_available					
	into #forecasted_dataset
	from #dist_with_cra  a
	left join #feeder_level_dataset b
	on a.project_id = b.project_id
	and a.bank_code = b.bank_code 
	and a.feeder_branch_code = b.feeder_branch_code
	
	--Prepare final dataset to insert into database
	drop table if exists #final_atm_level_allocated_dataset


	select	a.project_id
			,a.bank_code
			,a.feeder_branch_code
			,a.atm_priority
			,b.total_opening_remaining_available_amount
			,a.atm_id
			,a.is_cash_pre_available
			,a.total_forecasted_amt
			,b.total_loading_amount
			,case when a.is_cash_pre_available = 1 then coalesce(b.total_loading_amount,0) else coalesce(a.total_forecasted_amt,0) end 	as final_total_loading_amount ---total
			,case when a.is_cash_pre_available = 1 then coalesce(b.loading_amount_100,0) else coalesce(a.forecasted_amt_100,0) end 	as final_total_loading_amount_100		
			,case when a.is_cash_pre_available = 1 then coalesce(b.loading_amount_200,0) else coalesce(a.forecasted_amt_200,0) end 	as final_total_loading_amount_200
			,case when a.is_cash_pre_available = 1 then coalesce(b.loading_amount_500,0) else coalesce(a.forecasted_amt_500,0) end 	as final_total_loading_amount_500
			,case when a.is_cash_pre_available = 1 then coalesce(b.loading_amount_2000,0) else coalesce(a.forecasted_amt_2000,0) end 	as final_total_loading_amount_2000
			,case when a.is_cash_pre_available = 1 then 0 else a.total_rounded_vault_amt end as total_atm_vaulting_amount_after_pre_availability --total
			,case when a.is_cash_pre_available = 1 then 0 else a.rounded_vaultingamount_100 end as atm_vaulting_amount_after_pre_availability_100
			,case when a.is_cash_pre_available = 1 then 0 else a.rounded_vaultingamount_200 end as atm_vaulting_amount_after_pre_availability_200
			,case when a.is_cash_pre_available = 1 then 0 else a.rounded_vaultingamount_500 end as atm_vaulting_amount_after_pre_availability_500
			,case when a.is_cash_pre_available = 1 then 0 else a.rounded_vaultingamount_2000 end as atm_vaulting_amount_after_pre_availability_2000
			--Add dennomination wise
	into #final_atm_level_allocated_dataset
	from #forecasted_dataset a
	left join 
		#temp_cash_pre_availability b
			on 
				a.project_id COLLATE DATABASE_DEFAULT = b.project_id COLLATE DATABASE_DEFAULT
				and a.bank_code COLLATE DATABASE_DEFAULT = b.bank_code COLLATE DATABASE_DEFAULT
				and a.feeder_branch_code COLLATE DATABASE_DEFAULT = b.feeder_branch_code COLLATE DATABASE_DEFAULT
				and a.atm_id = b.atmid  COLLATE DATABASE_DEFAULT	
	
	--Feeder level
	
	--select * from #final_atm_level_allocated_dataset

	drop table if exists #final_feeder_level_allocated_dataset

	select a.*
			
			,case when a.Total_Amount_Available > 0 then isnull(b.total_remaining_available_amount,0) else isnull(a.total_rounded_vault_amt,0) end as total_vaulting_after_pre_availabiliy_allocation
			,case when a.available_100_amount > 0 then isnull(b.remaining_avail_100,0) else isnull(a.vaultingamount_100,0) end as vaulting_after_pre_availabiliy_allocation_100
			,case when a.available_200_amount > 0 then isnull(b.remaining_avail_200,0) else isnull(a.vaultingamount_200,0) end as vaulting_after_pre_availabiliy_allocation_200
			,case when a.available_500_amount > 0 then isnull(b.remaining_avail_500,0) else isnull(a.vaultingamount_500,0) end as vaulting_after_pre_availabiliy_allocation_500
			,case when a.available_2000_amount > 0 then isnull(b.remaining_avail_2000,0) else isnull(a.vaultingamount_2000,0) end as vaulting_after_pre_availabiliy_allocation_2000
			--Add for all denomination
			, c.final_total_loading_amount
			, case when a.Total_Amount_Available > 0 then (isnull(c.final_total_loading_amount,0) + isnull(b.total_remaining_available_amount,0)) else (isnull(c.final_total_loading_amount,0) + isnull(a.total_rounded_vault_amt,0)) end as total_indent_after_pre_availabiliy_allocation
			, case when a.available_100_amount > 0 then (isnull(c.final_total_loading_amount_100,0) + isnull(b.remaining_avail_100,0)) else (isnull(c.final_total_loading_amount_100,0) + isnull(a.vaultingamount_100,0)) end as indent_after_pre_availabiliy_allocation_100
			, case when a.available_200_amount > 0 then (isnull(c.final_total_loading_amount_200,0) + isnull(b.remaining_avail_200,0)) else (isnull(c.final_total_loading_amount_200,0) + isnull(a.vaultingamount_200,0)) end as indent_after_pre_availabiliy_allocation_200
			, case when a.available_500_amount > 0 then (isnull(c.final_total_loading_amount_500,0) + isnull(b.remaining_avail_500,0)) else (isnull(c.final_total_loading_amount_500,0) + isnull(a.vaultingamount_500,0)) end as indent_after_pre_availabiliy_allocation_500
			, case when a.available_2000_amount > 0 then (isnull(c.final_total_loading_amount_2000,0) + isnull(b.remaining_avail_2000,0)) else (isnull(c.final_total_loading_amount_2000,0) + isnull(a.vaultingamount_2000,0)) end as indent_after_pre_availabiliy_allocation_2000
			-- add for all allocation
	into #final_feeder_level_allocated_dataset
	from
	#feeder_level_dataset a	
	left join #temp_feeder_forecast b
	on a.project_id = b.project_id COLLATE DATABASE_DEFAULT
	and a.bank_code = b.bank_code COLLATE DATABASE_DEFAULT
	and a.feeder_branch_code = b.feeder_branch_code COLLATE DATABASE_DEFAULT
	left join 
	(
	select project_id
		 , bank_code
		 , feeder_branch_code
		 , sum(final_total_loading_amount) as final_total_loading_amount
		 , SUM(final_total_loading_amount_100) as final_total_loading_amount_100
		 , SUM(final_total_loading_amount_200) as final_total_loading_amount_200
		 , SUM(final_total_loading_amount_500) as final_total_loading_amount_500
		 , SUM(final_total_loading_amount_2000) as final_total_loading_amount_2000
			--Add for all denomination
	  from #final_atm_level_allocated_dataset
	  group by project_id, bank_code, feeder_branch_code
	  ) c
	  on c.project_id = a.project_id
	  and c.bank_code = a.bank_code
	  and c.feeder_branch_code = a.feeder_branch_code

	 --************************************Cypher Code********************************************************
	  	-- Generate cypher code and amount to work here.(Pending)
		drop table if exists #cypher_code

	    Select *into #cypher_code
		from
		(
		  Select 
		  FL.bank_code,
		  FL.project_id,
		  FL.feeder_branch_code,
		  FL.indentdate,
		  FL.sol_id,
		  dbo.fn_sum_of_digits( ISNULL(FL.sol_id,0))  as  sol_id_cypher_code, --As per req :Function to get sum of all digits in Sol_id
		  CC_date.value  as date_value,
		  CC_date.cypher_code   as cypher_code_date,		
		  CC_day.value  as day_value,
		  CC_day.cypher_code as cypher_code_day,
		  CC_month.value as month_value,
		  CC_month.cypher_code as cypher_code_month, 
		  CC_year.value as year_value,
		  CC_year.cypher_code as cypher_code_year,
		  FL.total_indent_after_pre_availabiliy_allocation,
		  CC_amount.value as feeder_amount_value,
		  CC_amount.cypher_code as cypher_code_amount,
		  CC_date.cypher_code+dbo.fn_sum_of_digits( ISNULL(FL.sol_id,0)) +CC_month.cypher_code+CC_year.cypher_code +CC_day.cypher_code+ CC_amount.cypher_code 
		        as final_cypher_Code	  
	 
		from #final_feeder_level_allocated_dataset   FL

		  --Select  dbo.fn_sum_of_digits( ISNULL(FL.sol_id,0)) from #final_feeder_level_allocated_dataset   FL

		 join cypher_code CC_date
		on
		    FL.bank_code=CC_date.bank_code
		and FL.project_id=CC_date.project_id
		and CC_date.record_status='Active'
		and CC_date.category='Date' 
		and CC_date.value=datepart(dd,indentdate)


		 join cypher_code CC_day
		on
		    FL.bank_code=CC_day.bank_code
		and FL.project_id=CC_day.project_id
		and CC_day.record_status='Active'
		and CC_day.category='Day' 
		and CC_day.value= DATENAME(DW,indentdate)


		join cypher_code CC_month
		on
		    FL.bank_code=CC_month.bank_code
		and FL.project_id=CC_month.project_id
		and CC_month.record_status='Active'
		and CC_month.category='Month' 
		and CC_month.value= datename(month,indentdate)

		join cypher_code CC_year
		on
		    FL.bank_code=CC_year.bank_code
		and FL.project_id=CC_year.project_id
		and CC_year.record_status='Active'
		and CC_year.category='Year' 
		and CC_year.value= datename(YEAR,indentdate)

		join cypher_code CC_amount
		on
		    FL.bank_code=CC_amount.bank_code
		and FL.project_id=CC_amount.project_id
		and CC_amount.record_status='Active'
		and CC_amount.category='Amount' 
		and CC_amount.value=  FL.total_indent_after_pre_availabiliy_allocation

		)a

		--Select * from #cypher_code
		   
		--***************************************************************************************
      
	
 
 


			  --------------------------INSERT INTO DATABASE-----------------------------
			  begin transaction

			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_detail Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			DECLARE @ForStatus VARCHAR(15) = 'Active'
			DECLARE @ToStatus VARCHAR(15) = 'Deleted'
			

			
			IF EXISTS(
						SELECT 1 
						FROM data_update_log
						WHERE record_status = 'Active'
						AND data_for_type = 'Indent'
						AND datafor_date_time = @dateT
					)

					BEGIN
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old data update log entries for indent ' 
	
						DECLARE @SetWhere_data_update VARCHAR(MAX) = ' data_for_type = ''' + 'Indent' + ''' and datafor_date_time =  ''' + cast(@dateT as nvarchar(50))+ ''''

						EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_data_update,@out OUTPUT

						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for indent' 

					END	

			IF EXISTS(
						SELECT 1 
						FROM distribution_planning_detail
						WHERE record_status = 'Active'
						AND indentdate = @dateT
						AND indent_counter = 1
					)

					BEGIN
							set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in distribution_planning_detail ' 
	
							DECLARE @SetWhere_distribution_detail VARCHAR(MAX) = ' indentdate = ''' + cast(@dateT as nvarchar(50))+ ''' and indent_counter =  1 '

							EXEC dbo.[uspSetStatus] 'dbo.distribution_planning_detail',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_distribution_detail,@out OUTPUT
						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in distribution_planning_detail table for old entries for indent ' 
					
					END	
			IF EXISTS(
						SELECT 1 
						FROM distribution_planning_master
						WHERE record_status = 'Active'
						AND indentdate = @dateT
						AND indent_counter = 1
					)

					BEGIN
					
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in distribution_planning_master ' 
	
						DECLARE @SetWhere_distribution_master VARCHAR(MAX) =  ' indentdate = ''' + cast(@dateT as nvarchar(50))+ ''' and indent_counter =  1 '
							
						EXEC dbo.[uspSetStatus] 'dbo.distribution_planning_master',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_distribution_master,@out OUTPUT

						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in distribution_planning_master table for old entries for indent  ' 
					
					END
		
			IF EXISTS(
						SELECT *
						FROM dbo.indent_master
						WHERE record_status = 'Active'
						AND order_date = @dateT
						AND indent_counter = 1
					
					)
					BEGIN
						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in indent master ' 
						
						DECLARE @SetWhere_indent_master VARCHAR(MAX) = ' order_date = ''' + cast(@dateT as nvarchar(50))+ ''' and indent_counter =  1 '
							
						EXEC dbo.[uspSetStatus] 'dbo.indent_master',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_indent_master,@out OUTPUT
												
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in indent master table for old entries for indent '
					
					END
					
			IF EXISTS(
							SELECT 1
							FROM dbo.indent_detail id
							JOIN indent_master im
							on im.indent_order_number = id.indent_order_number
							WHERE id.record_status = 'Active'
							AND id.indent_counter = 1
						)

					BEGIN	
						UPDATE 	id
						SET id.record_status = 'Deleted'
						FROM indent_detail id
						JOIN indent_master im
						on im.indent_order_number = id.indent_order_number
						WHERE id.record_status = 'Active'
						AND id.indent_counter = 1					
					END
			
			--select * from #dist_with_cra
			--select * from distribution_planning_d
			   

  INSERT INTO  distribution_planning_detail
				(
				[indent_code]
				,[atm_id]
				,[site_code]
				,[sol_id]
				,[bank_code]
				,[project_id]
				,[indentdate]
				,[bank_cash_limit]
				,[insurance_limit]
				,[feeder_branch_code]
				,[cra]
				,[base_limit]
				,[dispenseformula]
				,[decidelimitdays]
				,[loadinglimitdays]
				,[avgdispense]
				,[loading_amount]
				,[morning_balance_50]
				,[morning_balance_100]
				,[morning_balance_200]
				,[morning_balance_500]
				,[morning_balance_2000]
				,[total_morning_balance]
				,[cassette_50_brand_capacity]
				,[cassette_100_brand_capacity]
				,[cassette_200_brand_capacity]
				,[cassette_500_brand_capacity]
				,[cassette_2000_brand_capacity]
				,[cassette_50_count]
				,[cassette_100_count]
				,[cassette_200_count]
				,[cassette_500_count]
				,[cassette_2000_count]
				,[limit_amount]
				,[total_capacity_amount_50]
				,[total_capacity_amount_100]
				,[total_capacity_amount_200]
				,[total_capacity_amount_500]
				,[total_capacity_amount_2000]
				,[total_cassette_capacity]
				,[avgdecidelimit]
				,[DecideLimit]
				,[threshold_limit]
				,[loadinglimit]
				,[applied_vaulting_percentage]
				,[ignore_code]
				,[ignore_description]
				,[dist_Purpose]
				,[defaultamt]
				,[default_flag]
				,[curbal_div_avgDisp]
				,[loadingGap_cur_bal_avg_disp]
				,[CashOut]
				,[remaining_balance_50]
				,[remaining_balance_100]
				,[remaining_balance_200]
				,[remaining_balance_500]
				,[remaining_balance_2000]
				,[cassette_50_capacity_amount]
				,[cassette_100_capacity_amount]
				,[cassette_200_capacity_amount]
				,[cassette_500_capacity_amount]
				,[cassette_2000_capacity_amount]
				,[total_cassette_capacity_amount]
				,[cassette_capacity_percentage_50]
				,[cassette_capacity_percentage_100]
				,[cassette_capacity_percentage_200]
				,[cassette_capacity_percentage_500]
				,[cassette_capacity_percentage_2000]
				,[tentative_loading_amount_50]
				,[tentative_loading_amount_100]
				,[tentative_loading_amount_200]
				,[tentative_loading_amount_500]
				,[tentative_loading_amount_2000]
				,[rounded_amount_50]
				,[rounded_amount_100]
				,[rounded_amount_200]
				,[rounded_amount_500]
				,[rounded_amount_2000]
				,[total_expected_balanceT1]
				,[expected_balanceT1_50]
				,[expected_balanceT1_100]
				,[expected_balanceT1_200]
				,[expected_balanceT1_500]
				,[expected_balanceT1_2000]
				,[total_vault_amount]
				,[vaultingamount_50]
				,[vaultingamount_100]
				,[vaultingamount_200]
				,[vaultingamount_500]
				,[vaultingamount_2000]
				,[rounded_vaultingamount_50]
				,[rounded_vaultingamount_100]
				,[rounded_vaultingamount_200]
				,[rounded_vaultingamount_500]
				,[rounded_vaultingamount_2000]
				,[total_rounded_vault_amt]
				, total_opening_remaining_available_amount	
				, opening_remaining_available_amount_100	
				, opening_remaining_available_amount_200	
				, opening_remaining_available_amount_500	
				, opening_remaining_available_amount_2000	
				, cassette_100_count_original				
				, cassette_200_count_original				
				, cassette_500_count_original				
				, cassette_2000_count_original				
				, denomination_100_max_capacity_percentage	
				, denomination_200_max_capacity_percentage	
				, denomination_500_max_capacity_percentage	
				, denomination_2000_max_capacity_percentage	
				, max_amt_allowed_100						
				, max_amt_allowed_200						
				, max_amt_allowed_500						
				, max_amt_allowed_2000						
				, denomination_wise_round_off_100			
				, denomination_wise_round_off_200			
				, denomination_wise_round_off_500			
				, denomination_wise_round_off_2000			
				, tentative_loading_100						
				, tentative_loading_200						
				, tentative_loading_500						
				, tentative_loading_2000					
				, rounded_tentative_loading_100				
				, rounded_tentative_loading_200				
				, rounded_tentative_loading_500				
				, rounded_tentative_loading_2000			
				, deno_100_priority							
				, deno_200_priority							
				, deno_500_priority							
				, deno_2000_priority							
				, is_deno_wise_cash_available						
				, priority_1_is_denomination_100					
				, priority_1_is_denomination_200					
				, priority_1_is_denomination_500					
				, priority_1_is_denomination_2000					
				, priority_1_is_remaining_amount_available_100		
				, priority_1_is_remaining_amount_available_200		
				, priority_1_is_remaining_amount_available_500		
				, priority_1_is_remaining_amount_available_2000		
				, priority_1_is_remaining_capacity_available_100	
				, priority_1_is_remaining_capacity_available_200	
				, priority_1_is_remaining_capacity_available_500	
				, priority_1_is_remaining_capacity_available_2000	
				, priority_1_loading_amount_100						
				, priority_1_loading_amount_200						
				, priority_1_loading_amount_500						
				, priority_1_loading_amount_2000					
				, priority_2_is_denomination_100					
				, priority_2_is_denomination_200					
				, priority_2_is_denomination_500					
				, priority_2_is_denomination_2000					
				, priority_2_is_remaining_amount_available_100		
				, priority_2_is_remaining_amount_available_200		
				, priority_2_is_remaining_amount_available_500		
				, priority_2_is_remaining_amount_available_2000		
				, priority_2_is_remaining_capacity_available_100	
				, priority_2_is_remaining_capacity_available_200	
				, priority_2_is_remaining_capacity_available_500	
				, priority_2_is_remaining_capacity_available_2000	
				, priority_2_loading_amount_100						
				, priority_2_loading_amount_200						
				, priority_2_loading_amount_500						
				, priority_2_loading_amount_2000					
				, priority_3_is_denomination_100					
				, priority_3_is_denomination_200					
				, priority_3_is_denomination_500					
				, priority_3_is_denomination_2000					
				, priority_3_is_remaining_amount_available_100		
				, priority_3_is_remaining_amount_available_200		
				, priority_3_is_remaining_amount_available_500		
				, priority_3_is_remaining_amount_available_2000		
				, priority_3_is_remaining_capacity_available_100	
				, priority_3_is_remaining_capacity_available_200	
				, priority_3_is_remaining_capacity_available_500	
				, priority_3_is_remaining_capacity_available_2000	
				, priority_3_loading_amount_100						
				, priority_3_loading_amount_200						
				, priority_3_loading_amount_500						
				, priority_3_loading_amount_2000					
				, priority_4_is_denomination_100					
				, priority_4_is_denomination_200					
				, priority_4_is_denomination_500					
				, priority_4_is_denomination_2000					
				, priority_4_is_remaining_amount_available_100		
				, priority_4_is_remaining_amount_available_200		
				, priority_4_is_remaining_amount_available_500		
				, priority_4_is_remaining_amount_available_2000		
				, priority_4_is_remaining_capacity_available_100	
				, priority_4_is_remaining_capacity_available_200	
				, priority_4_is_remaining_capacity_available_500	
				, priority_4_is_remaining_capacity_available_2000	
				, priority_4_loading_amount_100						
				, priority_4_loading_amount_200						
				, priority_4_loading_amount_500						
				, priority_4_loading_amount_2000					
				, pre_avail_loading_amount_100								
				, pre_avail_loading_amount_200								
				, pre_avail_loading_amount_500								
				, pre_avail_loading_amount_2000								
				, pre_avail_total_loading_amount								
				, remaining_capacity_amount_100						
				, remaining_capacity_amount_200						
				, remaining_capacity_amount_500						
				, remaining_capacity_amount_2000					
				, closing_remaining_available_amount_100			
				, closing_remaining_available_amount_200			
				, closing_remaining_available_amount_500			
				, closing_remaining_available_amount_2000			
				, total_closing_remaining_available_amount			
				, total_forecasted_remaining_amt					
				, indent_counter 									
				, record_status
				,created_on
				,created_by
				,created_reference_id
				,final_total_loading_amount                         
				,final_total_loading_amount_100						
				,final_total_loading_amount_200						
				,final_total_loading_amount_500						
				,final_total_loading_amount_2000					
				,total_atm_vaulting_amount_after_pre_availability	
				,atm_vaulting_amount_after_pre_availability_100		
				,atm_vaulting_amount_after_pre_availability_200		
				,atm_vaulting_amount_after_pre_availability_500		
				,atm_vaulting_amount_after_pre_availability_2000	
				,atm_priority									
				,is_cash_pre_available								
				)
			  SELECT	indentcode
					,	dist.atm_id
					,   dist.site_code
					,	dist.sol_id
					,	dist.bank_code
					,	dist.project_id
					,	dist.indentdate
					,	dist.bank_cash_limit
					,	dist.insurance_limit
					,	dist.feeder_branch_code
					,	dist.cra
					,   dist.base_limit
					,	dist.dispenseformula
					,	dist.decidelimitdays
					,	dist.loadinglimitdays
					,	dist.avgdispense
					,	dist.loading_amount
					,	dist.morning_balance_50
					,	dist.morning_balance_100
					,	dist.morning_balance_200
					,	dist.morning_balance_500
					,	dist.morning_balance_2000
					,	dist.total_morning_balance
					,	cassette_50_brand_capacity
					,	COALESCE(temp.cassette_100_brand_capacity,dist.cassette_100_brand_capacity) as cassette_100_brand_capacity
					,	COALESCE(temp.cassette_200_brand_capacity,dist.cassette_200_brand_capacity) as cassette_200_brand_capacity
					,	COALESCE(temp.cassette_500_brand_capacity,dist.cassette_500_brand_capacity) as cassette_500_brand_capacity
					,	COALESCE(temp.cassette_2000_brand_capacity,dist.cassette_2000_brand_capacity) as cassette_2000_brand_capacity
					,	dist.cassette_50_count
					,	dist.cassette_100_count
					,	dist.cassette_200_count
					,	dist.cassette_500_count
					,	dist.cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	COALESCE(temp.total_capacity_amount_100,dist.total_capacity_amount_100) as total_capacity_amount_100
					,	COALESCE(temp.total_capacity_amount_200,dist.total_capacity_amount_200) as total_capacity_amount_200
					,	COALESCE(temp.total_capacity_amount_500,dist.total_capacity_amount_500) as total_capacity_amount_500
					,	COALESCE(temp.total_capacity_amount_2000,dist.total_capacity_amount_2000) as total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   forecasted_amt_50
					,   forecasted_amt_100
					,   forecasted_amt_200
					,   forecasted_amt_500
					,   forecasted_amt_2000
					,	total_expected_balanceT1
					,	expected_balanceT1_50
					,	expected_balanceT1_100
					,	expected_balanceT1_200
					,	expected_balanceT1_500
					,	expected_balanceT1_2000
					,	total_vault_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	rounded_vaultingamount_50
					,	rounded_vaultingamount_100
					,	rounded_vaultingamount_200
					,	rounded_vaultingamount_500
					,	rounded_vaultingamount_2000
					,	total_rounded_vault_amt
					, atm.total_opening_remaining_available_amount	
					, opening_remaining_available_amount_100	
					, opening_remaining_available_amount_200	
					, opening_remaining_available_amount_500	
					, opening_remaining_available_amount_2000	
					, COALESCE(temp.cassette_100_count_original,dist.cassette_100_count)  as	 cassette_100_count_original
					, COALESCE(temp.cassette_200_count_original,dist.cassette_200_count)  as	 cassette_200_count_original			
					, COALESCE(temp.cassette_500_count_original,dist.cassette_500_count)  as	 cassette_500_count_original			
					, COALESCE(temp.cassette_2000_count_original,dist.cassette_2000_count)  as	 cassette_2000_count_original				
					, COALESCE(temp.denomination_100_max_capacity_percentage, dist.cassette_capacity_percentage_100)	 as denomination_100_max_capacity_percentage
					, COALESCE(temp.denomination_200_max_capacity_percentage, dist.cassette_capacity_percentage_200) as denomination_200_max_capacity_percentage
					, COALESCE(temp.denomination_500_max_capacity_percentage, dist.cassette_capacity_percentage_500) as denomination_500_max_capacity_percentage
					, COALESCE(temp.denomination_2000_max_capacity_percentage,dist.cassette_capacity_percentage_2000) as denomination_2000_max_capacity_percentage
					, max_amt_allowed_100						
					, max_amt_allowed_200						
					, max_amt_allowed_500						
					, max_amt_allowed_2000						
					, COALESCE(temp.denomination_wise_round_off_100,sa.denomination_wise_round_off_100)			as denomination_wise_round_off_100
					, COALESCE(temp.denomination_wise_round_off_200	,sa.denomination_wise_round_off_200)		as denomination_wise_round_off_200
					, COALESCE(temp.denomination_wise_round_off_500	,sa.denomination_wise_round_off_500)		as denomination_wise_round_off_500
					, COALESCE(temp.denomination_wise_round_off_2000	,sa.denomination_wise_round_off_2000)	as denomination_wise_round_off_2000	
					, COALESCE(temp.tentative_loading_100	, dist.	tentative_loading_amount_100)		 as tentative_loading_100		
					, COALESCE(temp.tentative_loading_200	, dist.	tentative_loading_amount_200)		 as tentative_loading_200							
					, COALESCE(temp.tentative_loading_500	, dist.	tentative_loading_amount_500)		 as tentative_loading_500							
					, COALESCE(temp.tentative_loading_2000	, dist.	tentative_loading_amount_2000)		 as tentative_loading_2000						
					, rounded_tentative_loading_100				
					, rounded_tentative_loading_200				
					, rounded_tentative_loading_500				
					, rounded_tentative_loading_2000			
					, COALESCE(temp.deno_100_priority  , sa.deno_100_priority) as deno_100_priority								
					, COALESCE(temp.deno_200_priority  , sa.deno_200_priority) as deno_200_priority 										
					, COALESCE(temp.deno_500_priority  , sa.deno_500_priority) as deno_500_priority										
					, COALESCE(temp.deno_2000_priority , sa.deno_2000_priority) as deno_2000_priority									
					, is_deno_wise_cash_available							
					, priority_1_is_denomination_100						
					, priority_1_is_denomination_200						
					, priority_1_is_denomination_500						
					, priority_1_is_denomination_2000						
					, priority_1_is_remaining_amount_available_100			
					, priority_1_is_remaining_amount_available_200			
					, priority_1_is_remaining_amount_available_500			
					, priority_1_is_remaining_amount_available_2000			
					, priority_1_is_remaining_capacity_available_100		
					, priority_1_is_remaining_capacity_available_200		
					, priority_1_is_remaining_capacity_available_500		
					, priority_1_is_remaining_capacity_available_2000		
					, priority_1_loading_amount_100							
					, priority_1_loading_amount_200							
					, priority_1_loading_amount_500							
					, priority_1_loading_amount_2000						
					, priority_2_is_denomination_100						
					, priority_2_is_denomination_200						
					, priority_2_is_denomination_500						
					, priority_2_is_denomination_2000						
					, priority_2_is_remaining_amount_available_100			
					, priority_2_is_remaining_amount_available_200			
					, priority_2_is_remaining_amount_available_500			
					, priority_2_is_remaining_amount_available_2000			
					, priority_2_is_remaining_capacity_available_100		
					, priority_2_is_remaining_capacity_available_200		
					, priority_2_is_remaining_capacity_available_500		
					, priority_2_is_remaining_capacity_available_2000		
					, priority_2_loading_amount_100							
					, priority_2_loading_amount_200							
					, priority_2_loading_amount_500							
					, priority_2_loading_amount_2000						
					, priority_3_is_denomination_100						
					, priority_3_is_denomination_200						
					, priority_3_is_denomination_500						
					, priority_3_is_denomination_2000						
					, priority_3_is_remaining_amount_available_100			
					, priority_3_is_remaining_amount_available_200			
					, priority_3_is_remaining_amount_available_500			
					, priority_3_is_remaining_amount_available_2000			
					, priority_3_is_remaining_capacity_available_100		
					, priority_3_is_remaining_capacity_available_200		
					, priority_3_is_remaining_capacity_available_500		
					, priority_3_is_remaining_capacity_available_2000		
					, priority_3_loading_amount_100							
					, priority_3_loading_amount_200							
					, priority_3_loading_amount_500							
					, priority_3_loading_amount_2000						
					, priority_4_is_denomination_100						
					, priority_4_is_denomination_200						
					, priority_4_is_denomination_500						
					, priority_4_is_denomination_2000						
					, priority_4_is_remaining_amount_available_100			
					, priority_4_is_remaining_amount_available_200			
					, priority_4_is_remaining_amount_available_500			
					, priority_4_is_remaining_amount_available_2000			
					, priority_4_is_remaining_capacity_available_100		
					, priority_4_is_remaining_capacity_available_200		
					, priority_4_is_remaining_capacity_available_500		
					, priority_4_is_remaining_capacity_available_2000		
					, priority_4_loading_amount_100							
					, priority_4_loading_amount_200							
					, priority_4_loading_amount_500							
					, priority_4_loading_amount_2000						
					, COALESCE(loading_amount_100,  forecasted_amt_100,0) as pre_avail_loading_amount_100				
					, COALESCE(loading_amount_200	, forecasted_amt_200,0) as pre_avail_loading_amount_200								
					, COALESCE(loading_amount_500	, forecasted_amt_500,0) as pre_avail_loading_amount_500								
					, COALESCE(loading_amount_2000	, forecasted_amt_2000,0) as pre_avail_loading_amount_2000								
					,  COALESCE(loading_amount_100,  forecasted_amt_100,0) + COALESCE(loading_amount_200	, forecasted_amt_200,0) + COALESCE(loading_amount_500	, forecasted_amt_500,0) + COALESCE(loading_amount_2000	, forecasted_amt_2000,0) as pre_avail_total_loading_amount					
					, remaining_capacity_amount_100							
					, remaining_capacity_amount_200							
					, remaining_capacity_amount_500							
					, remaining_capacity_amount_2000						
					, closing_remaining_available_amount_100				
					, closing_remaining_available_amount_200				
					, closing_remaining_available_amount_500				
					, closing_remaining_available_amount_2000				
					, total_closing_remaining_available_amount				
					, total_forecasted_remaining_amt						
					, 1 										 
					,	'Active'
					,	@timestamp_date
					,	@cur_user
					,	@created_reference_id
					,final_total_loading_amount                         
					,final_total_loading_amount_100						
					,final_total_loading_amount_200						
					,final_total_loading_amount_500						
					,final_total_loading_amount_2000					
					,total_atm_vaulting_amount_after_pre_availability	
					,atm_vaulting_amount_after_pre_availability_100		
					,atm_vaulting_amount_after_pre_availability_200		
					,atm_vaulting_amount_after_pre_availability_500		
					,atm_vaulting_amount_after_pre_availability_2000	
					,dist.atm_priority										
					,is_cash_pre_available								
					FROM #dist_with_cra dist
					CROSS JOIN system_settings sa
					LEFT JOIN #final_atm_level_allocated_dataset atm
					on atm.atm_id  = dist.atm_id
					AND atm.bank_code = dist.bank_code
					and atm.project_id = dist.project_id
					LEFT JOIN #temp_cash_pre_availability temp
					on		temp.atmid COLLATE DATABASE_DEFAULT = dist.atm_id COLLATE DATABASE_DEFAULT
					AND		temp.project_id  COLLATE DATABASE_DEFAULT= dist.project_id COLLATE DATABASE_DEFAULT
					AND		temp.bank_code COLLATE DATABASE_DEFAULT = dist.bank_code COLLATE DATABASE_DEFAULT

					
					--select * from #final_atm_level_allocated_dataset
					--delete from #dist_with_cra 
					--where cast(#dist_with_cra.indentdate as date) >(Select max(cast(indentdate as date)) 
					--												from distribution_planning_detail)

				DECLARE @row INT
				SELECT @row	= @@ROWCOUNT

			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_detail Completed'
 
		    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_detail Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				INSERT INTO distribution_planning_master
					( 
					[indent_code]
				   ,[indentdate]
				   ,[project_id]
				   ,[bank_code]
				   ,[feeder_branch_code]
				   ,[cra]
				   ,cypher_code_sol_id
				   ,cypher_code_date         -- added new column
				   ,cypher_code_day          -- added new column
				   ,cypher_code_month        -- added new column
				   ,cypher_code_year         -- added new column
				   ,cypher_code_amount 		 -- added new column	 
				   ,cypher_code              -- added new column
				   ,[max_loading_amount]
				   ,[max_loading_amount_50]
				   ,[max_loading_amount_100]
				   ,[max_loading_amount_200]
				   ,[max_loading_amount_500]
				   ,[max_loading_amount_2000]
				   ,[min_loading_amount]
				   ,[forecast_loading_amount]
				   ,[forecast_loading_amount_50]
				   ,[forecast_loading_amount_100]
				   ,[forecast_loading_amount_200]
				   ,[forecast_loading_amount_500]
				   ,[forecast_loading_amount_2000]
				   ,[total_cassette_capacity]
				   ,[cassette_50_capacity_amount]
				   ,[cassette_100_capacity_amount]
				   ,[cassette_200_capacity_amount]
				   ,[cassette_500_capacity_amount]
				   ,[cassette_2000_capacity_amount]
				   ,[vaultingamount_50]
				   ,[vaultingamount_100]
				   ,[vaultingamount_200]
				   ,[vaultingamount_500]
				   ,[vaultingamount_2000]
				   ,[total_rounded_vault_amt]
				   ,[vault_balance_100]
				   ,[vault_balance_200]
				   ,[vault_balance_500]
				   ,[vault_balance_2000]
				   ,[total_vault_balance]
				   ,is_deno_wise_cash_available		
				   ,Total_Amount_Available
				   ,available_100_amount				
				   ,available_200_amount				
				   ,available_500_amount				
				   ,available_2000_amount			
				   ,[indent_50]
				   ,[indent_100]
				   ,[indent_200]
				   ,[indent_500]
				   ,[indent_2000]
				   ,[record_status]
				   ,[created_on]
				   ,[created_by]
				   ,[created_reference_id]
				   ,[indent_counter]
				   ,[total_vaulting_after_pre_availabiliy_allocation]   
				   ,[vaulting_after_pre_availabiliy_allocation_100]	    
				   ,[vaulting_after_pre_availabiliy_allocation_200]	    
				   ,[vaulting_after_pre_availabiliy_allocation_500]	    
				   ,[vaulting_after_pre_availabiliy_allocation_2000]	
				   ,[final_total_loading_amount]						
				   ,[total_indent_after_pre_availabiliy_allocation]		
				   ,[indent_after_pre_availabiliy_allocation_100]		
				   ,[indent_after_pre_availabiliy_allocation_200]		
				   ,[indent_after_pre_availabiliy_allocation_500]		
				   ,[indent_after_pre_availabiliy_allocation_2000]					   
				   )
				SELECT 	
					 indentcode
					,feeder.indentdate
					,feeder.project_id
					,feeder.bank_code
					,feeder.feeder_branch_code
					,cra
					,sol_id_cypher_code
					,cypher_code_date
					,cypher_code_day
					,cypher_code_month
					,cypher_code_year
					,cypher_code_amount		
					,final_cypher_Code
					,max_loading_amount
					,max_loading_amount_50
					,max_loading_amount_100
					,max_loading_amount_200
					,max_loading_amount_500
					,max_loading_amount_2000
					,min_loading_amount
					,forecast_loading_amount
					,forecast_loading_amount_50
					,forecast_loading_amount_100
					,forecast_loading_amount_200
					,forecast_loading_amount_500
					,forecast_loading_amount_2000
					,total_cassette_capacity
					,cassette_50_capacity_amount
					,cassette_100_capacity_amount
					,cassette_200_capacity_amount
					,cassette_500_capacity_amount
					,cassette_2000_capacity_amount
					,vaultingamount_50
					,vaultingamount_100
					,vaultingamount_200
					,vaultingamount_500
					,vaultingamount_2000
					,total_rounded_vault_amt
					,vault_balance_100
					,vault_balance_200
					,vault_balance_500
					,vault_balance_2000
					,total_vault_balance
					,feeder.is_deno_wise_cash_available		
					,total_remaining_available_amount
					,remaining_avail_100				
					,remaining_avail_200				
					,remaining_avail_500				
					,remaining_avail_2000			
					,indent_50
					,indent_100
					,indent_200
					,indent_500
					,indent_2000
					,	'Active'
					,	@timestamp_date
					,	@cur_user
					,	@created_reference_id
					, 1
					,[total_vaulting_after_pre_availabiliy_allocation]
					,[vaulting_after_pre_availabiliy_allocation_100]	
					,[vaulting_after_pre_availabiliy_allocation_200]	
					,[vaulting_after_pre_availabiliy_allocation_500]	
					,[vaulting_after_pre_availabiliy_allocation_2000]
					,[final_total_loading_amount]					
					,feeder.[total_indent_after_pre_availabiliy_allocation]	
					,[indent_after_pre_availabiliy_allocation_100]	
					,[indent_after_pre_availabiliy_allocation_200]	
					,[indent_after_pre_availabiliy_allocation_500]	
					,[indent_after_pre_availabiliy_allocation_2000]	
				FROM #final_feeder_level_allocated_dataset feeder
				LEFT JOIN #temp_feeder_forecast temp
				on feeder.bank_code COLLATE DATABASE_DEFAULT= temp.bank_code COLLATE DATABASE_DEFAULT
				AND feeder.feeder_branch_code COLLATE DATABASE_DEFAULT= temp.feeder_branch_code COLLATE DATABASE_DEFAULT
				AND feeder.project_id COLLATE DATABASE_DEFAULT = temp.project_id COLLATE DATABASE_DEFAULT

				left join #cypher_code CC          -- Join to get cypher code collums 
				on  feeder.bank_code=CC.bank_code
				and feeder.project_id=CC.project_id
				and  feeder.feeder_branch_code=CC.feeder_branch_code

            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			-- #final_feeder_level_indent -- suggested to use
			-- #feeder_level_dataset from #feeder_level_forecast (replace #feeder_level_forecast with #final_feeder_level_indent

			--select * from distribution_planning_detail
			--select * from distribution_planning_master
			--SELECT *  FROM indent_detail
			--SELECT *  FROM indent_master

			--select * from #temp_feeder_forecast
			 -- TRUNCATE TABLE distribution_planning_detail
			 -- TRUNCATE TABLE distribution_planning_master

				--TRUNCATE TABLE  indent_detail
				--TRUNCATE TABLE  indent_master
			--SELECT * from #final_feeder_level_allocated_dataset


			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_master Completed'
--------- ----------------------------------feeder level indent amount generation---------------------------------------------------
	
 --INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	--		  values ('Insert into #pre_Cipher_details_rev Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	 
	-- INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	-- values ('Insert into #pre_Cipher_details_rev Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		

	------------------------------------------------- Indent Code generation--------------------------------------------------
	--DECLARE @dateT DATE 
	--SET @dateT='2018-11-22'
		 --INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			--  values ('Insert into #indent_code Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))



	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
	INSERT INTO indent_master
		(  [indent_order_number]
	      ,[order_date]
	      ,[collection_date]
	      ,[replenishment_date]
	      ,[cypher_code]
	      ,[bank]
	      ,[feeder_branch]
	      ,[cra]
	      ,[total_atm_loading_amount_50]
	      ,[total_atm_loading_amount_100]
	      ,[total_atm_loading_amount_200]
	      ,[total_atm_loading_amount_500]
	      ,[total_atm_loading_amount_2000]
	      ,[total_atm_loading_amount]
	      ,[cra_opening_vault_balance_50]
	      ,[cra_opening_vault_balance_100]
	      ,[cra_opening_vault_balance_200]
	      ,[cra_opening_vault_balance_500]
	      ,[cra_opening_vault_balance_2000]
	      ,[cra_opening_vault_balance]
	      ,[total_bank_withdrawal_amount_50]
	      ,[total_bank_withdrawal_amount_100]
	      ,[total_bank_withdrawal_amount_200]
	      ,[total_bank_withdrawal_amount_500]
	      ,[total_bank_withdrawal_amount_2000]
	      ,[total_bank_withdrawal_amount]
	      ,[authorized_by_1]
	      ,[authorized_by_2]
	      ,[notes]
	      ,[amount_in_words]
	      ,[indent_type]
	      ,[project_id]
	      ,[record_status]
	      ,[created_on]
		  ,[indent_counter]
		  ,created_reference_id
		  ,created_by
		  ,email_indent_mail_master_id
		  ,signature_authorized_by_1_auth_signatories_signatures_id
		  ,signature_authorized_by_2_auth_signatories_signatures_id
		  )
		  SELECT  indentcode
				 ,feeder.indentdate AS [order_date]
				 ,feeder.indentdate AS [collection_date]
				 ,feeder.indentdate AS [replenishment_date]
				 ,final_cypher_Code as [cypher_code] 
				 ,feeder.[bank_code]
				 ,feeder.[feeder_branch_code]
				 ,feeder.[cra]
				 ,indent_50
				 ,indent_100
				 ,indent_200
				 ,indent_500
				 ,indent_2000
				 ,indent_50+indent_100+indent_200+indent_500+indent_2000 
				 , 0 AS [cra_opening_vault_balance_50]
				 , vault_balance_100   AS [cra_opening_vault_balance_100]
				 , vault_balance_200   AS [cra_opening_vault_balance_200]
				 , vault_balance_500   AS [cra_opening_vault_balance_500]
				 , vault_balance_2000  AS [cra_opening_vault_balance_2000]
				 , total_vault_balance AS [cra_opening_vault_balance]
				 , indent_50 AS [total_bank_withdrawal_amount_50]
				 , indent_100 AS [total_bank_withdrawal_amount_100]
				 , indent_200 AS [total_bank_withdrawal_amount_200]
				 , indent_500 [total_bank_withdrawal_amount_500]
				 , indent_2000 AS [total_bank_withdrawal_amount_2000]
				 , (indent_50+indent_100+indent_200+indent_500+indent_2000 ) AS [total_bank_withdrawal_amount]
				 ,'' AS [authorized_by_1]
				 ,'' AS [authorized_by_2]
				 ,'' AS [notes]
				 , dbo.fnNumberToWords((indent_50+indent_100+indent_200+indent_500+indent_2000 )) AS [amount_in_words]
				 ,'Indent' AS [indent_type]
				 , feeder.[project_id]
				 ,'Active' [record_status]
				 , @timestamp_date  AS [created_on]
				 , 1	
				 ,@created_reference_id
				 ,@cur_user			 
				 , m.id -- id for indent_mail_master
				 , sign_executive.Id -- id for Auth_Signatories_Signatures for designation = 'Cash Executive'
				 , sign_supervisor.Id -- id for Auth_Signatories_Signatures for designation = 'Cash Supervisor'
				from #final_feeder_level_allocated_dataset feeder
				LEFT JOIN indent_mail_master m
				on m.project_id = feeder.project_id
				and m.bank_code = feeder.bank_code
				and m.feeder_branch = feeder.feeder_branch_code
				and m.record_status = 'Active'
				left join Auth_Signatories_Signatures sign_executive
					on sign_executive.project_id = feeder.project_id
					and sign_executive.bank_code = feeder.bank_code
					and sign_executive.designation in ( 'Cash Ops Manager' )
					and sign_executive.record_status = 'Active'
				left join Auth_Signatories_Signatures sign_supervisor
					 on sign_supervisor.project_id = feeder.project_id
					 and sign_supervisor.bank_code = feeder.bank_code
					 and sign_supervisor.designation in ( 'Cash Ops Head' )
					 and sign_supervisor.record_status = 'Active'

				left join #cypher_code CC
				on feeder.project_id=CC.project_id
				and feeder.feeder_branch_code=CC.feeder_branch_code
				and feeder.bank_code = CC.bank_code

                 DECLARE @rowcount INT	
				 DECLARE @ErrorCode INT		 
                 SELECT @rowcount=@@ROWCOUNT,@ErrorCode = @@error


	  	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		 
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into indent_master Completed'
--------------------------------Inserting into Indent Detail table on atm level------------------------------------------

			  
		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_detail Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	  
	
INSERT INTO indent_detail
	(  
	       indent_order_number,
	       [atm_id]
	      ,[location]
	      ,[purpose]
	      ,[loading_amount_50]
	      ,[loading_amount_100]
	      ,[loading_amount_200]
	      ,[loading_amount_500]
	      ,[loading_amount_2000]
	      ,[total]
	      ,[record_status]
	      ,[created_on]
		  ,created_by
		  ,created_reference_id
		  ,indent_counter
	)
	SELECT
		     A.indentcode ,
			 A.atm_id,
			 M.location_name, 
			 A.dist_Purpose,
			 NULL,
			 temp.final_total_loading_amount_100,
			 temp.final_total_loading_amount_200,
			 temp.final_total_loading_amount_500,
			 temp.final_total_loading_amount_2000,
			 isnull(temp.final_total_loading_amount_100,0) +	 isnull(temp.final_total_loading_amount_200,0) + isnull(temp.final_total_loading_amount_500,0) +isnull( temp.final_total_loading_amount_2000,0) AS [total] ,
			 'Active' AS [record_status],
			 @timestamp_date  AS [created_on],
			 @cur_user,
			 @created_reference_id	
			 ,1		
		 FROM #dist_with_cra   A
		 LEFT JOIN atm_master M
		 ON A.site_code  COLLATE DATABASE_DEFAULT= M.site_code COLLATE DATABASE_DEFAULT
		 AND A.atm_id   COLLATE DATABASE_DEFAULT= M.atm_id COLLATE DATABASE_DEFAULT
		 AND M.record_status ='Active' and M.site_status = 'Active'
		 LEFT JOIN  #final_atm_level_allocated_dataset temp
		 on temp.atm_id COLLATE DATABASE_DEFAULT= A.atm_id COLLATE DATABASE_DEFAULT
		 AND temp.bank_code COLLATE DATABASE_DEFAULT= A.bank_code COLLATE DATABASE_DEFAULT
		 AND temp.project_id COLLATE DATABASE_DEFAULT= A.project_id COLLATE DATABASE_DEFAULT

		-- select * from #final_atm_level_allocated_dataset
		 --TRUNCATE TABLE indent_detail
		 --TRUNCATE TABLE  indent_master

						DECLARE @row_dul INT			 
                        SET @row_dul=@@ROWCOUNT
	
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into indent_detail Completed'

	
                        INSERT into data_update_log 
									(bank_code,
									project_id,
									operation_type,
									datafor_date_time,
									data_for_type,			 
									status_info_json,			 
									record_status,
									date_created,
									created_by,
									created_reference_id
									)
                                values
									(NULL,
									NULL,
									'Indent',
									@dateT,
									'Indent',			 
									'Indent calculated for date '+cast(@dateT as varchar(30)) + ' and no. of rows inserted : '+ cast(@row_dul as varchar(10)),
									'Active',
									@timestamp_date,
									@cur_user,
									@created_reference_id			 			
									)

		
			 IF (@ErrorCode = 0)
                        BEGIN
                            IF(@row > 0)
                            BEGIN
                                    SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'success') 
                                    SELECT @outputval
                            END
							ELSE
							BEGIN
									SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'no_records') 
                                    SELECT @outputval
							END
						END
	  commit transaction
 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Execution of [dbo].[usp_indent_calculation_SIT] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	

END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
                        SELECT @outputval	
							
						 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
						  values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))				
			 END CATCH
END


