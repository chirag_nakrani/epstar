 
/****** Object:  StoredProcedure [dbo].[usp_pre_dispense_calc]    Script Date: 14-01-2019 19:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
 --- =========================================================================
 --- Created By  :Rubina Q
 --- Created Date: 12-12-2018
 --- Description : prepare datasets for calculation of dispense amounts based on the date range
 --- ==========================================================================
 */
 -- EXEC  [usp_pre_dispense_calc_3112] '2018-12-26','BOMH','MOF','rubina','abc123'
 -- EXEC  [usp_pre_dispense_calc]'2018-11-11','DENA','MOF','rubina','abc123'

alter procedure [dbo].[usp_pre_dispense_calc]
 (
 @date_T date,
 @bank varchar(50),
 @project varchar (10),
 @cur_user varchar(50),
 @created_reference_id varchar(50)
  )
 AS
BEGIN
    DECLARE @timestamp_date varchar(50) = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
	DECLARE @row INT	
    BEGIN TRY
        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Execution of [dbo].[usp_pre_dispense_calc] Started for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

        -- inserting dispense details for last three month's same week period
       
	---*****************************Testing**********************
	 --   DECLARE @bank varchar(5)
		--DECLARE @project varchar(10)
		--DECLARE @date_T date
		--set @bank='DENA'
		--set @project='MOF'
		--set @date_T='2018-11-11'
		-- DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
		-- declare  @cur_user varchar(50),
		--		 @created_reference_id varchar(50)
		--		 Set  @cur_user='abc'
		--		 Set @created_reference_id='poi'
     ---*****************************Testing**********************

 

        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('inserting started in table [cash_dispense_month_minus_3] for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		DELETE FROM [cash_dispense_month_minus_3]
		DELETE FROM [cash_dispense_month_minus_2]
		DELETE FROM [cash_dispense_month_minus_1]
		DELETE FROM [cash_dispense_month_minus_0]
	   
	   IF @bank = 'Allbanks'
			BEGIN
 
  --  DECLARE @bank varchar(5)
		--DECLARE @project varchar(10)
		--DECLARE @date_T date
		--set @bank='Allbanks'
		--set @project='MOF'
		--set @date_T='2018-12-26'
		-- DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
		-- declare  @cur_user varchar(50),
		--		 @created_reference_id varchar(50)
		--		 Set  @cur_user='abc'
		--		 Set @created_reference_id='poi'
        INSERT into [cash_dispense_month_minus_3] 
        (      
                atm_id,
				bank,
				project_id,
				datafor_date_time,
				total_dispense_amount,
				created_on,
				created_by,
				created_reference_id,
				record_status
        )
			Select	 distinct
				 atm_id,
				 bank_name,
				 project_id,
				 datafor_date_time,
				 total_dispense_amount ,
				 @timestamp_date,
				 @cur_user,
				 @created_reference_id,
				 record_status
        from cash_dispense_register
        where            
			(
				cast(datafor_date_time as date) between
					CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-3, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
				and 
					DATEADD(day,6,CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-3, DATEADD(day,-3,@date_T)  )  , 105) ),105) )
			)
				and record_status='Active' 

        SET @row=@@ROWCOUNT

            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('inserting completed in table [cash_dispense_month_minus_3] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ cast(@row as varchar(10)) , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	     
		  insert into [cash_dispense_month_minus_2] 
           (      
               atm_id,
				bank,
				project_id,
				datafor_date_time,
				total_dispense_amount,
				created_on,
				created_by,
				created_reference_id,
				record_status				
        )
            Select	 distinct
				 atm_id,
				 bank_name,
				 project_id,
				 datafor_date_time,
				 total_dispense_amount ,
				 @timestamp_date,
				 @cur_user,
				 @created_reference_id,
				 record_status
        from cash_dispense_register
        where 
			(
				cast(datafor_date_time as date) between
				CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
				and 
					DATEADD(day,6,CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@date_T)  )  , 105) ),105) )
			)
				and record_status='Active' 
                     
				SET @row=@@ROWCOUNT

		 
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('inserting completed in table [cash_dispense_month_minus_2] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ cast(@row as varchar(20)) , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		  INSERT into [cash_dispense_month_minus_1] 
         (      
               atm_id,
				bank,
				project_id,
				datafor_date_time,
				total_dispense_amount,
				created_on,
				created_by,
				created_reference_id,
				record_status
        )
            Select	 distinct
				 atm_id,
				 bank_name,
				 project_id,
				 datafor_date_time,
				 total_dispense_amount ,
				 @timestamp_date,
				 @cur_user,
				 @created_reference_id,
				 record_status
        from cash_dispense_register
        where 
			(	
				cast(datafor_date_time as date) between
				CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-1, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
				and 
					DATEADD(day,6,CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-1, DATEADD(day,-3,@date_T)  )  , 105) ),105) )
			)
            and record_status='Active' 

        SET @row=@@ROWCOUNT

        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('inserting completed in table [cash_dispense_month_minus_1] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ cast(@row as varchar(10)) , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('inserting started in table [cash_dispense_month_minus_0] for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		   INSERT into [cash_dispense_month_minus_0]
                 (      
               atm_id,
				bank,
				project_id,
				datafor_date_time,
				total_dispense_amount,
				created_on,
				created_by,
				created_reference_id,
				record_status
        )
            Select	 distinct
				 atm_id,
				 bank_name,
				 project_id,
				 datafor_date_time,
				 total_dispense_amount ,
				 @timestamp_date,
				 @cur_user,
				 @created_reference_id,
				 record_status
           from cash_dispense_register
             where 
           	(
				cast(datafor_date_time as date) BETWEEN
				CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-7,@date_T)  )  , 105) ),105) 
				and  
						CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,1,@date_T)  )  , 105) ),105) 
			)			 
             and record_status='Active' 

            SET @row=@@ROWCOUNT

            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('inserting completed in table [cash_dispense_month_minus_0] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ cast(@row as varchar(10)) , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		  END
		ELSE IF @bank <> 'Allbanks'
		BEGIN
		 
        INSERT into [cash_dispense_month_minus_3] 
        (      
                atm_id,
				bank,
				project_id,
				datafor_date_time,
				total_dispense_amount,
				created_on,
				created_by,
				created_reference_id,
				record_status
        )
			Select	 distinct
				 atm_id,
				 bank_name,
				 project_id,
				 datafor_date_time,
				 total_dispense_amount ,
				 @timestamp_date,
				 @cur_user,
				 @created_reference_id,
				 record_status
        from cash_dispense_register
         where            
			(
				cast(datafor_date_time as date) between
					CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-3, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
				and 
					DATEADD(day,6,CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-3, DATEADD(day,-3,@date_T)  )  , 105) ),105) )
			)
		    and bank_name=@bank and project_id=@project
			and record_status='Active' 
        			 
				SET @row=@@ROWCOUNT

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
					values ('inserting completed in table [cash_dispense_month_minus_3] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ cast(@row  as varchar(10)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	     
		  
        --*********************** inserting dispense details for last two month's same week period*********************
       	
        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('inserting started in table [cash_dispense_month_minus_2] for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	 --   DECLARE @bank varchar(5)
		--DECLARE @project varchar(10)
		--DECLARE @date_T date
		--set @bank='DENA'
		--set @project='MOF'
		--set @date_T='2018-10-11'

        insert into [cash_dispense_month_minus_2] 
           (      
               atm_id,
				bank,
				project_id,
				datafor_date_time,
				total_dispense_amount,
				created_on,
				created_by,
				created_reference_id,
				record_status
        )
            Select	 distinct
				 atm_id,
				 bank_name,
				 project_id,
				 datafor_date_time,
				 total_dispense_amount ,
				 @timestamp_date,
				 @cur_user,
				 @created_reference_id,
				 record_status
        from cash_dispense_register
         where 
			(
				cast(datafor_date_time as date) between
				CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
				and 
					DATEADD(day,6,CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@date_T)  )  , 105) ),105) )
			)
			    and bank_name=@bank and project_id=@project
                and record_status='Active' 
                     
				SET @row=@@ROWCOUNT

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('inserting completed in table [cash_dispense_month_minus_2] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ cast(@row as varchar(10)) , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

        --*************************** inserting dispense details for last  month's same week period*****************
        insert into [cash_dispense_month_minus_1] 
         (      
               atm_id,
				bank,
				project_id,
				datafor_date_time,
				total_dispense_amount,
				created_on,
				created_by,
				created_reference_id,
				record_status
        )
            Select	 distinct
				 atm_id,
				 bank_name,
				 project_id,
				 datafor_date_time,
				 total_dispense_amount ,
				 @timestamp_date,
				 @cur_user,
				 @created_reference_id,
				 record_status
        from cash_dispense_register
        where 
			(	
				cast(datafor_date_time as date) between
				CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-1, DATEADD(day,-3,@date_T)  )  , 105) ),105) 
				and 
					DATEADD(day,6,CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-1, DATEADD(day,-3,@date_T)  )  , 105) ),105) )
			)
			 and bank_name=@bank and project_id=@project
            and record_status='Active' 

        SET @row=@@ROWCOUNT

        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('inserting completed in table [cash_dispense_month_minus_1] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ cast(@row as varchar(10)) , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

        INSERT into [cash_dispense_month_minus_0]
                 (      
               atm_id,
				bank,
				project_id,
				datafor_date_time,
				total_dispense_amount,
				created_on,
				created_by,
				created_reference_id,
				record_status
        )
            Select	 distinct
				 atm_id,
				 bank_name,
				 project_id,
				 datafor_date_time,
				 total_dispense_amount ,
				 @timestamp_date,
				 @cur_user,
				 @created_reference_id,
				 record_status

        from cash_dispense_register
             where 
			(
				cast(datafor_date_time as date) BETWEEN
				CONVERT (date, ( CONVERT(varchar, DATEADD(month,0, DATEADD(day,-7,@date_T)  )  , 105) ),105) 
				and  
					CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,1,@date_T)  )  , 105) ),105) 
			)			
			 and bank_name=@bank and project_id=@project
             and record_status='Active' 

				    SET @row=@@ROWCOUNT

				    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
					values ('inserting completed in table [cash_dispense_month_minus_0] for '+ @bank +' and date '+cast(@date_T as varchar(30)) +' and rows '+ cast(@row as varchar(10)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

			END
			 
        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Execution of [dbo].[usp_pre_dispense_calc] Completed for '+ @bank +' and date '+cast(@date_T as varchar(30)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
    END TRY
    BEGIN CATCH
        IF(@@TRANCOUNT > 0 )
            BEGIN
                ROLLBACK TRAN;
            END
            DECLARE @ErrorNumber INT = ERROR_NUMBER();
            DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
            DECLARE @ErrorState INT = ERROR_STATE();
            DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
            DECLARE @ErrorLine INT = ERROR_LINE();
            DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
            DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
                    
            INSERT INTO dbo.error_log
			(
				[errorNumber]
				,[errorSeverity]
				,[errorState]
				,[errorProcedure]
				,[errorLine]
				,[errorMessage]
				,[dateAdded]
			) 
		   values
                (
                @ErrorNumber
				,@ErrorSeverity
				,@ErrorState
				,@ErrorProcedure
				,@ErrorLine
				,@ErrorMessage
				,@dateAdded
                )
						--SELECT @ErrorNumber;				
     END CATCH
 END
