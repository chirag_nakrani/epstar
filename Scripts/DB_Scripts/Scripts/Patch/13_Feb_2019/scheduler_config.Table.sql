/****** Object:  Table [dbo].[scheduler_config]    Script Date: 2/13/2019 5:48:02 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[scheduler_config](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[region] [nvarchar](100) NULL,
	[file_type] [nvarchar](100) NULL,
	[schedule_frequency] [int] NULL,
	[counter_frequency] [int] NULL,
	[wait_time] [char](10) NULL,
	[hourly_frequency] [text] NULL,
	[1st_escalation] [text] NULL,
	[2nd_escalation] [text] NULL,
	[from_email] [nvarchar](100) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL)
GO

