insert into app_config_param values ('calculate_dispense', 'Morning_T_minus_1_missing', '60051', 'Morning balance of previous day not found')
insert into app_config_param values ('calculate_dispense', 'Morning_T_minus_1_error', '60052', 'Error in morning balance of previous day')
insert into app_config_param values ('calculate_dispense', 'Morning_T_minus_2_missing', '60053', 'Morning balance of day before previous day not found')
insert into app_config_param values ('calculate_dispense', 'Morning_T_minus_2_error', '60054', 'Error in morning balance of day before previous day')
insert into app_config_param values ('calculate_dispense', 'C3R_entry_missing', '60055', 'C3R entry not found')
insert into app_config_param values ('calculate_dispense', 'C3R_error', '60056', 'Error in C3R')
insert into app_config_param values ('calculate_dispense', 'success', '60057', 'Dispense Calculated Successfully')
insert into app_config_param values ('calculate_dispense', 'Error', '60058', 'No records inserted')

