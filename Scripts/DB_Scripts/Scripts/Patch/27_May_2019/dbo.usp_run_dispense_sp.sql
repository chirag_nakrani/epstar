USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[usp_run_dispense_sp]    Script Date: 27-05-2019 17:10:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_run_dispense_sp]
 @date_T      datetime,
 @created_reference_id nvarchar(50),
 @cur_user nvarchar(50),
 @bank_code nvarchar(50),
 @project_id nvarchar(50)

as

begin


--for testing
--exec run_dispense_sp '2019-05-03', '123', 'saa', 'DENA', 'ALL'
--select distinct atm_id, total_dispense_amount, datafor_date_time, created_on from cash_dispense_register
--where bank_name = 'DENA'
--and datafor_date_time = '2019-05-03 00:00:00.000'
--and record_status = 'Active'
--and atm_id = 'DEN17511'


 
	Declare @cmd nvarchar(max)
	declare @MinDestinationDate_T datetime
    declare @MinDestinationDate_Tplus1 datetime

	set @MinDestinationDate_T = (Select min(datafor_date_time)
											 from cash_balance_register 									 
											where bank_name=@bank_code 
											and project_id= case when @project_id =  'ALL' then project_id else @project_id end
											and datafor_date_time >= @date_T  and datafor_date_time < dateadd(dd, 1, @date_T)
											and record_status ='Active')


	set @MinDestinationDate_Tplus1 = (Select min(datafor_date_time)
											 from cash_balance_register 									 
											where bank_name=@bank_code
											and project_id= case when @project_id =  'ALL' then project_id else @project_id end
											and datafor_date_time >= dateadd(dd, 1, @date_T)  and datafor_date_time < dateadd(dd, 2, @date_T)
											and record_status ='Active' )
										



	exec  usp_calculate_dispense @date_T, @MinDestinationDate_Tplus1, @MinDestinationDate_T , @date_T ,@created_reference_id, @cur_user, @bank_code, @project_id

	--print @date_T
	--print @MinDestinationDate_Tplus1
	--print @MinDestinationDate_T
	--print @date_T

end

