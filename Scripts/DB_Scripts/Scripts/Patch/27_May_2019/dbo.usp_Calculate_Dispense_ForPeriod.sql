USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[Calculate_Dispense_ForPeriod]    Script Date: 27-05-2019 17:39:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Calculate_Dispense_ForPeriod]
 @from_date    date,
 @to_date    date,
 @project_id  nvarchar(50),
 @bank_code   nvarchar(50),
 @created_reference_id nvarchar(50),
 @cur_user nvarchar(50)

as 

set nocount on 
--exec [calculate_Dispense_ForPeriod] '2019-05-01','2019-05-19', 'ALL', 'DENA', 'Ref', 'sa'

declare @cmd varchar(max)
declare @MinDestinationDate_T datetime
declare @MinDestinationDate_Tplus1 datetime

begin


	while (@from_date <= @to_date)
	begin

		begin try

			set @MinDestinationDate_T = (Select min(datafor_date_time)
											 from cash_balance_register 									 
											where bank_name=@bank_code 
											and project_id= case when @project_id =  'ALL' then project_id else @project_id end
											and datafor_date_time >= @from_date  and datafor_date_time < dateadd(dd, 1, @from_date)
											and record_status ='Active')


			set @MinDestinationDate_Tplus1 = (Select min(datafor_date_time)
											 from cash_balance_register 									 
											where bank_name=@bank_code
											and project_id= case when @project_id =  'ALL' then project_id else @project_id end
											and datafor_date_time >= dateadd(dd, 1, @from_date)  and datafor_date_time < dateadd(dd, 2, @from_date)
											and record_status ='Active' )

			set @cmd = 'exec usp_calculate_dispense ' +  '''' + CONVERT(VARCHAR(100), @from_date, 20) +  ''', ''' + CONVERT(VARCHAR(100), @MinDestinationDate_Tplus1, 20) +  ''', ''' + CONVERT(VARCHAR(100), @MinDestinationDate_T, 20) +  ''', ''' + cast(@from_date as varchar) + ''', ''' + @created_reference_id + ''', ''' + @cur_user +''', ''' + @bank_code + ''', ''' + @project_id + ''''
			--exec  usp_calculate_dispense @from_date, @MinDestinationDate_Tplus1, @MinDestinationDate_T , @from_date ,@created_reference_id, @cur_user, @bank_code, @project_id
			
			print @cmd
			--exec @cmd

			set @from_date = dateadd(dd,1 ,@from_date)

			insert into execution_log (description, execution_date_time, process_spid, process_reference_id, execution_program, executor_method, executed_by)
			values ( 'Execution success for:'+@cmd , getdate(), @@SPID, NULL, 'Stored Procedure', OBJECT_NAME(@@PROCID), @cur_user)

		end try
		begin catch
		
			insert into execution_log (description, execution_date_time, process_spid, process_reference_id, execution_program, executor_method, executed_by)
			values ( 'Execution failed for:'+@cmd+' with error:'+ERROR_MESSAGE() , getdate(), @@SPID, NULL, 'Stored Procedure', OBJECT_NAME(@@PROCID), @cur_user)
							 
		end catch

	end


end

