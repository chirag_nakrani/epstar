ALTER TABLE limitdays
ADD feasibility_frequency INT

ALTER TABLE limitdays
ADD is_feasible_today varchar(10)

ALTER TABLE [distribution_planning_detail_V2]
ADD feasibility_frequency INT

ALTER TABLE [distribution_planning_detail_V2]
ADD is_feasible_today varchar(10)

ALTER TABLE [distribution_planning_detail_V2]
ADD reason_for_priority varchar(30)

-----------------------------------------------------------

ALTER TABLE [distribution_planning_detail_V2]
ADD bank_decide_limit_days FLOAT

ALTER TABLE [distribution_planning_detail_V2]
ADD bank_loading_limit_days FLOAT

ALTER TABLE [distribution_planning_detail_V2]
ADD feeder_decide_limit_days FLOAT

ALTER TABLE [distribution_planning_detail_V2]
ADD feeder_loading_limit_days FLOAT

ALTER TABLE [distribution_planning_detail_V2]
ADD feasibility_decide_limit_days FLOAT

ALTER TABLE [distribution_planning_detail_V2]
ADD feasibility_loading_limit_days FLOAT



ALTER TABLE [distribution_planning_detail_V2]
ADD indent_type  nvarchar(50)

ALTER TABLE [distribution_planning_master_V2]
ADD indent_type  nvarchar(50)

ALTER TABLE [indent_detail]
ADD indent_type  nvarchar(50)

------------------------------------------------------------


alter table [distribution_planning_detail_V2]
add atm_band nvarchar(20)

alter table [distribution_planning_detail_V2]
add reason_for_priority nvarchar(10)

alter table [distribution_planning_detail_V2]
add is_zero_dispense int

ALTER TABLE indent_detail
ADD is_manually_updated nvarchar(10)

ALTER TABLE indent_master
ADD is_manually_updated nvarchar(10)