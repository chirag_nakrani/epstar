USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspConsolidateC3R_CMIS]    Script Date: 4/26/2019 2:54:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
  --  declare @out varchar(100)
  --exec [uspConsolidateC3R_CMIS]  '2018-10-22 10:00:00','','C1234567891','Mostaque',@out
 
ALTER PROCEDURE [dbo].[uspConsolidateC3R_CMIS] 
   (@data_for_datetime varchar(100), @referenceid varchar(50),@cra_name varchar(50) , @systemUser varchar(50),@out varchar(200) OUTPUT)
AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;

INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		IF EXISTS(
			Select 1 as ColumnName
			FROM [dbo].[data_update_log]                          
			WHERE datafor_date_time = @data_for_datetime AND 			 
					data_for_type = 'C3R' AND 
					record_status = 'Approved' 
					AND 
					created_reference_id = @referenceid 
			) 
		BEGIN			--- Check for approved status in DUL

			IF EXISTS   (						
				SELECT 1
				FROM [dbo].[data_update_log]
				WHERE datafor_date_time = @data_for_datetime AND 			 
					data_for_type = 'C3R' AND cra_name = @cra_name AND
					record_status = 'Active'  
				)	
				BEGIN				--- Check Active status in DUL

					DECLARE @ForStatus VARCHAR(15) = 'Active'
					DECLARE @ToStatus VARCHAR(15) = 'Deleted'
					DECLARE @data_for_type varchar(30) = 'C3R'


					--	BEGIN	--- For bank (START)
							IF
							 EXISTS
							(
								Select 1 as ColumnName
								FROM [dbo].c3r_CMIS
								WHERE datafor_date_time =  @data_for_datetime AND    --remove created on
										cra_name = @cra_name AND
										record_status = 'Active'                     --chg
							) 

							AND EXISTS
								(
								Select 1 as ColumnName
								FROM [dbo].c3r_VMIS
								WHERE datafor_date_time =  @data_for_datetime AND    --remove created on
										cra_name = @cra_name AND record_status = 'Active'                     --chg
							) 

							BEGIN						-----Check active in bankwise file
							
										BEGIN	
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Active record found!! Changing status from Active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			
------------------------------------------------------C3R_CMIS and  C3R_VMIS status to deleted--------------------------------------------------------------------------------------
							DECLARE @SetWhere_C3R_bankWise VARCHAR(MAX) = ' datafor_date_time = ''' +       --chg every projectid
									@data_for_datetime  + ''' and  cra_name = ''' + @cra_name + ''''

										EXEC dbo.[uspSetStatus] 'dbo.c3r_CMIS',@ForStatus,            --  set status deleted for C3R_CMIS
										@ToStatus,@current_datetime_stmp,@systemUser,
										@referenceid,@SetWhere_C3R_bankWise,@out OUTPUT
										
											EXEC dbo.[uspSetStatus] 'dbo.c3r_VMIS',@ForStatus,     --    set status  deleted for C3R_VMIS
										@ToStatus,@current_datetime_stmp,@systemUser,
										@referenceid,@SetWhere_C3R_bankWise,@out OUTPUT	
	------------------------------------------------------------------dataupdatelog status to deleted----------------------------------------------------------------------------------				
								DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) = ' datafor_date_time = ''' + 
														@data_for_datetime  + ''' and data_for_type = ''' + @data_for_type + ''' and  cra_name = ''' + @cra_name + ''''
														                                          

					         		EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,    -- set status  deleted for data update log table
																	@current_datetime_stmp,@systemUser,
																	@referenceid,@SetWhere_DataUpdateLog,
																	@out OUTPUT
											
----------------------------------------------------------status in C3_CMIS and VMIS to Active from approved for revised data------------------------------------------------------------------------------------------
											DECLARE @SetWhere_C3R_bankWise1 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
							@data_for_datetime   + ''' and  cra_name = ''' + @cra_name + ''' and  created_reference_id = ''' + @referenceid + ''''
											
											EXEC dbo.[uspSetStatus] 'dbo.c3r_CMIS','Approved','Active',
															@current_datetime_stmp,@systemUser,@referenceid,
															@SetWhere_C3R_bankWise1,@out OUTPUT

	                                                EXEC dbo.[uspSetStatus] 'dbo.c3r_VMIS','Approved','Active',
															@current_datetime_stmp,@systemUser,@referenceid,
															@SetWhere_C3R_bankWise1,@out OUTPUT
                                               

					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records successfully marked as Deleted. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			

														IF EXISTS (
																		SELECT 1 from dbo.c3r_CMIS where 
																				datafor_date_time = @data_for_datetime and  
																				cra_name = @cra_name AND																		 
																				record_status = 'Active'  
																				   --chg
																	)
																	AND
																	EXISTS
																	(
																		SELECT 1 from dbo.c3r_VMIS where 
																				datafor_date_time = @data_for_datetime and
																				cra_name = @cra_name AND																		 
																				record_status = 'Active'  
																				    --chg
																	)
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'  --chg
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END

								
													INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


										END				---- Check Cash Balance Register Table for Active Status (END)

									 
				
						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN
					
					DECLARE @SetWhere_C3R_bankWise2 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
							@data_for_datetime   + ''' and  cra_name = ''' + @cra_name + ''' and  created_reference_id = ''' + @referenceid + ''''
					
					EXEC dbo.[uspSetStatus] 'C3R_CMIS','Approved','Active',@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_C3R_bankWise2,@out OUTPUT
						
                    EXEC dbo.[uspSetStatus] 'C3R_VMIS','Approved','Active',@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_C3R_bankWise2,@out OUTPUT
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records have been marked as Active status. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			
														IF EXISTS (
														SELECT 1 from dbo.C3R_CMIS where 
														datafor_date_time = @data_for_datetime and 
														cra_name = @cra_name AND
														 record_status = 'Active' 
														  )
														   AND 
														   EXISTS (
														SELECT 1 from dbo.c3r_VMIS where 
														datafor_date_time = @data_for_datetime and 
														cra_name = @cra_name AND
														record_status = 'Active' 
														  )

														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'      --chg
																		) 
														END
														ELSE
														BEGIN
															RAISERROR(50003,16,1)
														END

										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data Consolidated Successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
			BEGIN
				RAISERROR(50010,16,1)
			END
END
