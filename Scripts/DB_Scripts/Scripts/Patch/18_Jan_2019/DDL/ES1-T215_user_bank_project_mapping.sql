DROP TABLE user_bank_project_mapping
GO
/****** Object:  Table [dbo].[user_bank_project_mapping]    Script Date: 18-01-2019 21:07:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_bank_project_mapping](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[user_id] [int] NULL,
	[bank_code] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [date] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [date] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL
)
GO
