
/****** Object:  StoredProcedure [dbo].[usp_calculate_dispense]    Script Date: 03-06-2019 18:59:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- =========================================================================
 --- Created By :	Rohit Saini, Malay Dhami
 --- Created Date: 29-04-2019
 --- Description: Calculation of  dispense for the banks which do not provide dispense amount directly.
  --              calculating Dispense amount with morning balance of T-1,T-2 and C3R balance.
 --- =========================================================================


ALTER PROCEDURE [dbo].[usp_calculate_dispense]
	-- Add the parameters for the stored procedure here
    (
        @datafor_date date,        
		@CBR_tm1_datafor_datetime datetime,
		@CBR_tm2_datafor_datetime datetime,
		@C3R_tm2_datafor_date date,
        @created_reference_id nvarchar(50),
        @cur_user nvarchar(50),
		@bank_code nvarchar(50),
		@project_id nvarchar(50) = 'All' 
		
    )
AS

--select max(datafor_date_time) from cash_balance_register where bank_name = 'BOMH' and datafor_date_time <='2019-03-29 22:00:00.000' AND record_status = 'Active'
----BOMH, 2019-03-28 22:00:00.000, 2019-03-29 22:00:00.000
----C3R, 2019-03-28 12:00:00.000

 

--select max(datafor_date_time) from c3r_cmis where datafor_date_time < '2019-03-30 12:00:00.000'
--select *  from c3r_cmis where datafor_date_time = '2019-03-28 12:00:00.000' and record_status = 'Active' and bank = 'bomh'
--group by datafor_date_time


BEGIN
	DECLARE @Execution_log_str nvarchar(max)
		
	BEGIN TRY
			--Define variables to be used for procedure.

			--declare @Execution_log_str nvarchar(max) = ''
			--declare @datafor_date date = '2019-03-28',        
			--	@CBR_tm1_datafor_datetime datetime = '2019-03-28 22:00:00',
			--	@CBR_tm2_datafor_datetime datetime = '2019-03-29 22:00:00',
			--	@C3R_tm2_datafor_date date = '2019-03-28',
			--	@created_reference_id nvarchar(50) = 'ABC123',
			--	@cur_user nvarchar(50) ='Malay',
			--	@bank_code nvarchar(50) = 'BOMH',
			--	@project_id nvarchar(50) = 'MOF_MAH'       

			--	select * from cash_dispense_register where bank_name = @bank_code and datafor_date_time = @datafor_date
			--	and created_by = @cur_user and created_reference_id = @created_reference_id

			DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @Result varchar(100);
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @Dispense_Calculation_Process_table_type varchar(50)
			DECLARE @datafor_type_dispense varchar(50)
			DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
			DECLARE @ForStatus_active nvarchar(50)
			DECLARE @ToStatus_deleted nvarchar(50)
			Declare @record_count int
				 
			SET @Dispense_Calculation_Process_table_type ='Dispense_Calculation_Process'
			SET @datafor_type_dispense = 'Calculated_Dispense'
				 
			--Add execution log indicating procedure started.
				
			set @ForStatus_active = 'Active'
			set @ToStatus_deleted = 'Deleted'
				
      
			set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@datafor_date as nvarchar(50)) +' , Project = ' + @project_id + ' , Bank = ' + @bank_code;

			-- To calculate morning balance, find maximum cbr for T minus 1
			/***********-------Inserting values in Dispense calculation table--------------******************/

			declare @error_morning_balance_t_minus_1_not_found nvarchar(50) = '101' --PENDING: Mapping of error codes
			declare @error_in_morning_balance_t_minus_1 nvarchar(50) = '102' --PENDING: Mapping of error codes
			declare @error_morning_balance_t_minus_2_not_found nvarchar(50) = '103' --PENDING: Mapping of error codes
			declare @error_in_morning_balance_t_minus_2 nvarchar(50) = '104' --PENDING: Mapping of error codes
			declare @error_C3R_entry_not_found nvarchar(50) = '105' --PENDING: Mapping of error codes
			declare @error_in_C3R nvarchar(50) = '106'--PENDING: Mapping of error codes

			SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) +convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Insert into Dispense_Amount_calc started. '

			drop table if exists #temp_calculated_disepnse				

			select *, 
					case when len(ltrim(rtrim(error_code))) > 0  then 'No' else 'Yes' end as is_valid_record
					into #temp_calculated_disepnse
			from
			(
				select 
						am.project_id,
						am.bank_code,
						am.atm_id,
						am.site_code,				    
						@datafor_date as datafor_date, 
						tm1.datafor_date_time  as datafor_datetime_t_minus_1,
						tm1.created_on  as created_on_t_minus_1, 
						tm1.created_reference_id  as created_reference_id_t_minus_1,
						tm1.remaining_balance_50 as morning_balance_t_minus_1_50,
						tm1.remaining_balance_100 as morning_balance_t_minus_1_100,
						tm1.remaining_balance_200 as morning_balance_t_minus_1_200,
						tm1.remaining_balance_500 as morning_balance_t_minus_1_500,
						tm1.remaining_balance_2000 as morning_balance_t_minus_1_2000,
						tm1.total_remaining_balance as total_morning_balance_t_minus_1,
						tm2.datafor_date_time  as datafor_datetime_t_minus_2,
						tm2.created_on  as created_on_t_minus_2, --New field
						tm2.created_reference_id  as created_reference_id_t_minus_2,
						tm2.remaining_balance_50 as morning_balance_t_minus_2_50,
						tm2.remaining_balance_100 as morning_balance_t_minus_2_100,
						tm2.remaining_balance_200 as morning_balance_t_minus_2_200,
						tm2.remaining_balance_500 as morning_balance_t_minus_2_500,
						tm2.remaining_balance_2000 as morning_balance_t_minus_2_2000,
						tm2.total_remaining_balance as total_morning_balance_t_minus_2,
						CMIS.created_on as created_on_loading_t_minus_2,
						CMIS.datafor_date_time as datafor_date_time_loading_t_minus_2,
						CMIS.created_reference_id as created_reference_id_loading_t_minus_2,
						CMIS.sw_loading_100 as loading_t_minus_2_amt_100,
						CMIS.sw_loading_200 as loading_t_minus_2_amt_200,
						CMIS.sw_loading_500 as loading_t_minus_2_amt_500,
						CMIS.sw_loading_2000 as loading_t_minus_2_amt_2000,
						CMIS.sw_loading_total as loading_amount_t_minus_2,
						(isnull(tm2.remaining_balance_50,0) + 0 - isnull(tm1.remaining_balance_50,0)) as dispense_amount_50
						,(isnull(tm2.remaining_balance_100,0) + + isnull(CMIS.sw_loading_100,0) - isnull(tm1.remaining_balance_100,0)) as dispense_amount_100
						,(isnull(tm2.remaining_balance_200,0) + + isnull(CMIS.sw_loading_200,0) - isnull(tm1.remaining_balance_200,0)) as dispense_amount_200
						,(isnull(tm2.remaining_balance_500,0) + + isnull(CMIS.sw_loading_500,0) - isnull(tm1.remaining_balance_500,0)) as dispense_amount_500
						,(isnull(tm2.remaining_balance_2000,0) + + isnull(CMIS.sw_loading_2000,0) - isnull(tm1.remaining_balance_2000,0)) as dispense_amount_2000
						,(isnull(tm2.total_remaining_balance,0) + isnull(CMIS.sw_loading_total,0) - isnull(tm1.total_remaining_balance,0)) as total_dispense_amount
						,CONCAT_WS(	','
									, case when tm1.total_remaining_balance is null then @error_morning_balance_t_minus_1_not_found end
									, case when tm1.error_code is not null then @error_in_morning_balance_t_minus_1 end
									, case when tm2.total_remaining_balance is null then @error_morning_balance_t_minus_2_not_found end
									, case when tm2.error_code is not null then @error_in_morning_balance_t_minus_2 end
									, case when CMIS.sw_loading_total is null then @error_in_C3R end
									, case when CMIS.error_code is not null  then @error_in_C3R end
								) as error_code,
						'Calculated' as dispense_type
				FROM atm_master am	
				left JOIN 
					cash_balance_register tm1																				
				on 
					am.atm_id=tm1.atm_id	        and 
					am.project_id=tm1.project_id    and
					am.bank_code=tm1.bank_name 		and										
					tm1.record_status = 'Active'
					and tm1.bank_name = @bank_code
					and tm1.datafor_date_time = @CBR_tm1_datafor_datetime
					and (tm1.project_id = case when ( @project_id = 'All') then tm1.project_id else  @project_id   end)
				left join	
					cash_balance_register tm2																	
				on 
					am.atm_id=tm2.atm_id	        and 
					am.project_id=tm2.project_id    and
					am.bank_code=tm2.bank_name 		and										
					tm2.record_status = 'Active'
					and tm2.bank_name = @bank_code
					and tm2.datafor_date_time = @CBR_tm2_datafor_datetime
					and (tm2.project_id = case when ( @project_id = 'All') then tm2.project_id else  @project_id   end)
				left JOIN       
					c3r_CMIS CMIS
				on 
					am.atm_id=CMIS.atm_id and -- C3R_CMIS for loading amount
					am.bank_code=CMIS.bank and
					cast(CMIS.datafor_date_time as date)= @C3R_tm2_datafor_date
					and CMIS.record_status ='Active' --and  CMIS.is_valid_record ='yes'
				where 
					am.bank_code = @bank_code
					and am.project_id = (case when ( @project_id = 'All') then am.project_id else  @project_id   end)
					and am.record_status ='Active' 
					and am.site_status = 'Active'						
			)d


			
			-----------****************** updating records having -ve disp or no CBR or No C3R with avg dispense**********************---
			SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) +  convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Updating dispense_amount_cal for -ve, no cbr, c3r'
				
			update 		temp set temp.total_dispense_amount = cdr.total_dispense_amount, temp.dispense_type = 'Default'
			from
			#temp_calculated_disepnse temp
			inner join cash_dispense_register cdr
			on temp.project_id = cdr.project_id
			and temp.bank_code = cdr.bank_name
			and temp.atm_id = cdr.atm_id
			and temp.datafor_date = cdr.datafor_date_time

			where 
			temp.datafor_date = @datafor_date and
			cdr.bank_name = @bank_code and
			cdr.project_id = case when @project_id = 'ALL' then cdr.project_id else @project_id end and
			cdr.record_status = 'Active' and
			(
				temp.total_dispense_amount < 0   or
				temp.loading_amount_t_minus_2 is null  or
				temp.total_morning_balance_t_minus_1 is null or
				temp.total_morning_balance_t_minus_2 is null
			)

						 
			SET @record_count=@@ROWCOUNT
			SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) +  convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Updating dispense_amount_cal for -ve, no cbr, c3r : Records Updated:'  + cast(@record_count as nvarchar(50))
				



						-- delete from dispense_amount_calc
				-- Checking for active records in dispense_amount_calc
			BEGIN TRANSACTION
				IF EXISTS(						
								SELECT 1
								FROM dispense_amount_calc
								WHERE 
				                    datafor_date = @datafor_date
				                    and 
									bank_code = @bank_code
									AND 
									(project_id = case when ( @project_id = 'All') then project_id else  @project_id   end)	
				                    and 
				                    record_status = 'Active'				                    
						)	
				BEGIN
					
				 -- Active records found . Going for status update from active to deleted
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old records in dispense_amount_cal entries. ' 

					--declare @date_T date= '2019-04-25'
					--declare @project_id nvarchar(500) = 'All'
					--declare @bank_code nvarchar(500) = 'SBI'

				    DECLARE @SetWhere_dispense_amount_calc VARCHAR(MAX) =  ' bank_code = ''' + @bank_code + '''
																AND (project_id = case when ( ''' + @project_id + ''' = ''All'') then project_id else  ''' + @project_id +'''  end) 
																and datafor_date = ''' + cast(@datafor_date as nvarchar(50))  + ''''																 
					--print @SetWhere_dispense_amount_calc
				    EXEC dbo.uspSetStatus 'dbo.dispense_amount_calc',@ForStatus_active,@ToStatus_deleted,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_dispense_amount_calc,@out OUTPUT

					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in dispense_amount_calc table for old entries. '

				END											
			
				INSERT INTO dbo.dispense_amount_calc
					(
						project_id
					   ,bank_code
					   ,atm_id
					   ,site_code
					   ,datafor_date
					   ,datafor_datetime_t_minus_1
					   ,created_on_t_minus_1
					   ,created_reference_id_t_minus_1
					   ,morning_balance_t_minus_1_50
					   ,morning_balance_t_minus_1_100
					   ,morning_balance_t_minus_1_200
					   ,morning_balance_t_minus_1_500
					   ,morning_balance_t_minus_1_2000
					   ,total_morning_balance_t_minus_1
					   ,datafor_datetime_t_minus_2
					   ,created_on_t_minus_2
					   ,created_reference_id_t_minus_2
					   ,morning_balance_t_minus_2_50
					   ,morning_balance_t_minus_2_100
					   ,morning_balance_t_minus_2_200
					   ,morning_balance_t_minus_2_500
					   ,morning_balance_t_minus_2_2000
					   ,total_morning_balance_t_minus_2
					   ,created_on_loading_t_minus_2
					   ,datafor_date_time_loading_t_minus_2
					   ,created_reference_id_loading_t_minus_2
					   ,loading_t_minus_2_amt_100
					   ,loading_t_minus_2_amt_200
					   ,loading_t_minus_2_amt_500
					   ,loading_t_minus_2_amt_2000
					   ,loading_total_amount_t_minus_2
					   ,dispense_amount_50
					   ,dispense_amount_100
					   ,dispense_amount_200
					   ,dispense_amount_500
					   ,dispense_amount_2000
					   ,dispense_amount
					   ,error_code
					   ,is_valid_record
					   ,record_status
					   ,created_on
					   ,created_by
					   ,created_reference_id           
					)
					select 
							project_id
							,bank_code
							,atm_id
							,site_code
							,datafor_date
							,datafor_datetime_t_minus_1
							,created_on_t_minus_1
							,created_reference_id_t_minus_1
							,morning_balance_t_minus_1_50
							,morning_balance_t_minus_1_100
							,morning_balance_t_minus_1_200
							,morning_balance_t_minus_1_500
							,morning_balance_t_minus_1_2000
							,total_morning_balance_t_minus_1
							,datafor_datetime_t_minus_2
							,created_on_t_minus_2
							,created_reference_id_t_minus_2
							,morning_balance_t_minus_2_50
							,morning_balance_t_minus_2_100
							,morning_balance_t_minus_2_200
							,morning_balance_t_minus_2_500
							,morning_balance_t_minus_2_2000
							,total_morning_balance_t_minus_2
							,created_on_loading_t_minus_2
							,datafor_date_time_loading_t_minus_2
							,created_reference_id_loading_t_minus_2
							,loading_t_minus_2_amt_100
							,loading_t_minus_2_amt_200
							,loading_t_minus_2_amt_500
							,loading_t_minus_2_amt_2000
							,loading_amount_t_minus_2
							,dispense_amount_50
							,dispense_amount_100
							,dispense_amount_200
							,dispense_amount_500
							,dispense_amount_2000
							,total_dispense_amount
							,error_code
							,is_valid_record
							,'Active' as record_status
							,@timestamp_date as created_on
							,@cur_user as created_by
							,@created_reference_id as created_reference_id							
					from #temp_calculated_disepnse
				



				-- Check for record count (no of rows inserted)				
				--DECLARE @record_count INT			 
				SET @record_count=@@ROWCOUNT
								
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) +  convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Insert into Data_update_log Completed with record count '  + cast(@record_count as nvarchar(50))
				
				
				
				
					--- Check Active status in cash_dispense_register 				 
				IF EXISTS(
				        Select 
							1 as ColumnName
				        FROM cash_dispense_register
				        WHERE
				            cast(datafor_date_time as date) = @datafor_date	AND 				                   
				            record_status = 'Active'			AND		
							bank_name = @bank_code				AND		                  
							(project_id = case when ( @project_id = 'All') then project_id else  @project_id   end)
				        )
				BEGIN	
					-- Active records found . Going for status update from active to deleted
					SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Old record found in cash_dispense_register. '
				   
				    DECLARE @SetWhere_CDR VARCHAR(MAX) = ' bank_name = ''' + @bank_code +  ''' AND
														   cast(datafor_date_time as date) = ''' + cast(@datafor_date as nvarchar(50)) + ''' AND      
														  (project_id = case when ( ''' + @project_id + ''' = ''All'') then project_id else  ''' + @project_id +'''  end) '

				    EXEC dbo.uspSetStatus 'cash_dispense_register', @ForStatus_active, @ToStatus_deleted, @timestamp_date,@cur_user,@created_reference_id,@SetWhere_CDR,@out OUTPUT        
				
					SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Status updated in cash_dispense_register. '
				END

				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into cash_dispense_register started.  '
				
				-- Inserting data into cash_dispense_register from Dispense_Amount_calc				
				Insert into [dbo].[cash_dispense_register]
					(
						[atm_id],
						[bank_name],
						[total_dispense_amount],
						[project_id],
						[datafor_date_time],
						created_on,
						created_by,
						created_reference_id,         
						record_status,
						dispense_type
					)
					select 
						[atm_id],
						[bank_code],
						total_dispense_amount,
						[project_id],
						datafor_date,
						@timestamp_date as created_on,
						@cur_user as created_by,
						@created_reference_id as created_reference_id,
						'Active' as record_status,
						 dispense_type
					 from #temp_calculated_disepnse 			
				 
				 				
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into cash_dispense_register Completed.  '
			
				SET @record_count=@@ROWCOUNT
				 
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into data_update_log started.  '											
			
			COMMIT transaction;			
		     
    END TRY

	BEGIN CATCH
		IF(@@TRANCOUNT > 0 )
			BEGIN
				ROLLBACK TRAN;
			END
					DECLARE @ErrorNumber INT = ERROR_NUMBER();
					DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
					DECLARE @ErrorState INT = ERROR_STATE();
					DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
					DECLARE @ErrorLine INT = ERROR_LINE();
					DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
					DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
					INSERT INTO dbo.error_log values
						(
							@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
						)
					--SELECT @ErrorNumber;	
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		                      
	END CATCH

	declare @returntext varchar(30)
	if (@record_count > 0 )
	begin
		
		select @returntext = cast(sequence as varchar) from app_config_param where category = 'calculate_dispense' and sub_category = 'success'
		return @returntext
	end
	else
	begin
		select  @returntext = cast(sequence as varchar) from app_config_param where category = 'calculate_dispense' and sub_category = 'Error';
		return @returntext
	end
END

 