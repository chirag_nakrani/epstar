INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'add_project_id', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'add_bank_code', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'add_region_code', N'Cash_Executive', 4)

INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'add_project_id', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'add_bank_code', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'add_region_code', N'Cash_Admin', 1)


INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'add_project_id', N'Add Project ID', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 12, NULL)

INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'add_bank_code', N'Add Bank Code', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 13, NULL)

INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'add_region_code', N'Add Region Code', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 14, NULL)
