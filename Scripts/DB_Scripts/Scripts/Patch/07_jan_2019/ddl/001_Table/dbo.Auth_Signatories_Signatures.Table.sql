Drop table if exists auth_signatories_signatures

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create table [dbo].[auth_signatories_signatures](
	[id] [bigint] identity(1,1) primary key,
	[project_id] [nvarchar](50) null,
	[bank_code] [nvarchar](50) null,
	[signatory_name] [nvarchar](50) null,
	[designation] [nvarchar](50) null,
	[physical_signature] [image] null,
	[digital_signature] [nvarchar](50) null,
	[record_status] [nvarchar](50) null,
	[created_on] [datetime] null,
	[created_by] [nvarchar](50) null,
	[created_reference_id] [nvarchar](50) null,
	[approved_on] [datetime] null,
	[approved_by] [nvarchar](50) null,
	[approved_reference_id] [nvarchar](50) null,
	[approve_reject_comment] [nvarchar](50) null,
	[rejected_on] [datetime] null,
	[rejected_by] [nvarchar](50) null,
	[rejected_reference_id] [nvarchar](50) null,
	[deleted_on] [datetime] null,
	[deleted_by] [nvarchar](50) null,
	[deleted_reference_id] [nvarchar](50) null,
	[modified_on] [datetime] null,
	[modified_by] [nvarchar](50) null,
	[modified_reference_id] [nvarchar](50) null,
	[is_valid_record] [nvarchar](20) null,
	[error_code] [nvarchar](50) null
)
go


