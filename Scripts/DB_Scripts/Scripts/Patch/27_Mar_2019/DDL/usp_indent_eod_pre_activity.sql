/****** Object:  StoredProcedure [dbo].[usp_indent_eod_pre_activity]    Script Date: 27-03-2019 12:18:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_indent_eod_pre_activity]
AS
BEGIN
	BEGIN TRY
	
	DECLARE @timestamp_date datetime = convert(date,DATEADD(MI,330,GETUTCDATE()))
	DECLARE @date varchar(50) = (SELECT CONVERT(VARCHAR(8), GETDATE(), 8) 'hh:mm:ss')
	DECLARE @date_str varchar(50) = (SELECT SUBSTRING(REPLACE(@date,':',''),2,6))
	DECLARE @nextval varchar(50) = cast(next value for generate_ref_seq as varchar(50))
	DECLARE @referenceid varchar(100) = (select 'C' + @nextval + @date_str)
	DECLARE @curuser varchar(50) = (SELECT SYSTEM_USER)

	DROP TABLE IF EXISTS #EOD_Records
	DROP TABLE IF EXISTS #no_eod_records

	--------------------- Fetching records from C3R whose status of loading is either EOD or LOADING in last 7 days --------------------------
	
	SELECT * 
	INTO #EOD_Records 
	FROM 
	(
		SELECT distinct a.atm_id,a.bank FROM c3r_CMIS a WHERE
		a.datafor_date_time BETWEEN  CAST(DATEADD(dd,-7,GETDATE()) as DATE) AND CAST (GETDATE()-1 as DATE) and a.status_of_loading IN ('LOADING','EOD') AND record_status = 'Active'
	)a

	--------------------- Excluding above records from ATM Master and manipulating rest of the data ---------------------------------

	SELECT DISTINCT atm.atm_id,site_code,bank_code,project_id 
	INTO #no_eod_records
	FROM atm_master atm 
	LEFT JOIN #EOD_Records eod
	ON eod.atm_id = atm.atm_id
	AND eod.bank = atm.bank_code
	WHERE atm.record_status = 'Active' 
	AND atm.site_status = 'Active'
	AND eod.atm_id IS NULL 
	AND eod.bank IS NULL
	

	UPDATE ind
	SET record_status = 'Deleted',
		deleted_by = @curuser,
		deleted_on = GETDATE(),
		deleted_reference_id = @referenceid
	FROM indent_eod_pre_activity ind
	JOIN #no_eod_records temp
	on ind.atm_id = temp.atm_id
	WHERE ind.record_status = 'Approval Pending'
	AND ind.datafor_date_time = @timestamp_date+1

	IF not exists (
					SELECT 1 FROM indent_eod_pre_activity 
					WHERE record_status = 'Approval Pending'
					AND created_reference_id <> @referenceid
				)
			begin 
			
				UPDATE data_update_log_master
				SET record_status = 'Deleted',
					deleted_by = @curuser,
					deleted_on = GETDATE(),
					deleted_reference_id = @referenceid
				WHERE record_status = 'Approval Pending'
				and data_for_type = 'EOD_ACTIVITY'
				AND created_reference_id <> @referenceid
			end
	
	IF EXISTS ( SELECT 1 from indent_eod_pre_activity ipa
				JOIN #no_eod_records temp on 
				temp.atm_id = ipa.atm_id 
				where ipa.record_status NOT IN ('Approval Pending','Active')
			  )
			BEGIN

				INSERT INTO indent_eod_pre_activity  (atm_id,site_code,project_id,bank_code,record_status,datafor_date_time,created_reference_id)
				SELECT temp.atm_id,temp.site_code,temp.project_id,temp.bank_code,'Approval Pending',@timestamp_date+1, @referenceid
				from #no_eod_records temp
				JOIN indent_eod_pre_activity c3r
				on c3r.atm_id = temp.atm_id
				and c3r.record_status NOT IN ('Approval Pending','Active')

				
				INSERT INTO data_update_log_master (data_for_type,datafor_date_time, record_status,created_reference_id,date_created)
				VALUES ('EOD_ACTIVITY',@timestamp_date+1 ,'Approval Pending',@referenceid,@timestamp_date)


			END
			ELSE
				BEGIN
				IF EXISTS	(
									SELECT 1 from indent_eod_pre_activity ipa
									JOIN #no_eod_records temp on 
									temp.atm_id = ipa.atm_id
									where ipa.record_status = 'Active'
									and ipa.datafor_date_time = @timestamp_date+1
								)
				BEGIN
					INSERT INTO indent_eod_pre_activity  (atm_id,site_code,project_id,bank_code,record_status,datafor_date_time,created_reference_id)
					SELECT DISTINCT temp.atm_id,temp.site_code,temp.project_id,temp.bank_code,'Approval Pending',@timestamp_date+1,@referenceid
					from #no_eod_records temp
					JOIN indent_eod_pre_activity ipa
					on ipa.atm_id <> temp.atm_id
					and ipa.record_status IN ('Active')		
					
					INSERT INTO data_update_log_master (data_for_type,datafor_date_time, record_status,created_reference_id,date_created)
					VALUES ('EOD_ACTIVITY',@timestamp_date+1 ,'Approval Pending',@referenceid,@timestamp_date)

				END

				ELSE
				BEGIN 
					INSERT INTO indent_eod_pre_activity  (atm_id,site_code,project_id,bank_code,record_status,datafor_date_time,created_reference_id)
					SELECT DISTINCT temp.atm_id,temp.site_code,temp.project_id,temp.bank_code,'Approval Pending',@timestamp_date+1,@referenceid
					from #no_eod_records temp

					INSERT INTO data_update_log_master (data_for_type,datafor_date_time, record_status,created_reference_id,date_created)
					VALUES ('EOD_ACTIVITY',@timestamp_date+1,'Approval Pending',@referenceid,@timestamp_date)

				END
				END

		END TRY 
		BEGIN CATCH
			DECLARE @ErrorNumber INT = ERROR_NUMBER();
			DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
			DECLARE @ErrorState INT = ERROR_STATE();
			DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
			DECLARE @ErrorLine INT = ERROR_LINE();
			DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
			DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
				
				
			INSERT INTO dbo.error_log values
			(@ErrorNumber,@ErrorSeverity,
			 @ErrorState,@ErrorProcedure,
			 @ErrorLine,@ErrorMessage,@dateAdded)

		END CATCH

END


