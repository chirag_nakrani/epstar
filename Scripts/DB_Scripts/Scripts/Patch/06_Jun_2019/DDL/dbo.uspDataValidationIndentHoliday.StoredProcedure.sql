USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspDataValidationIndentHoliday]    Script Date: 6/6/2019 7:41:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspDataValidationIndentHoliday]
( 
	@api_flag    VARCHAR(50),
	@systemUser  VARCHAR(50),
	@referenceid VARCHAR(50),
	@outputVal   VARCHAR(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50)
	DECLARE @errorcode nvarchar(max)
	DECLARE @activestatus nvarchar(20) = 'Active'
	DECLARE @datafor nvarchar(50) =  'INDENT_HOLIDAY'
	DECLARE @approvedstatus nvarchar(20) = 'Approved'
	DECLARE @uploadedstatus nvarchar(20) = 'Uploaded'

	--SELECT SESSION_ID from sys.dm_exec_sessions; 

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
	values ('Execution of [dbo].[uspDataValidationIndentHoliday] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[data_update_log_master] WITH (NOLOCK)
				WHERE data_for_type = @datafor and 
                      record_status = @uploadedstatus
					  AND created_reference_id = @referenceid
				)
	begin 

		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
		values ('Record exists in DUL', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		begin try	
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
			values ('Validation Starts', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			SET @errorcode = (SELECT CAST(sequence AS nvarchar) from app_config_param where category = 'Indent Holiday Validation' and sub_category = 'Holiday_Name_not_found')			
			
			update ih set ih.record_status = case when s.holiday_name is null then 'Rejected' else 'Active' end , 
				   ih.error_code = case when s.holiday_name is null then @errorcode else null end,
				   ih.holiday_code = case when s.holiday_name is null then null else s.holiday_code end


			--select 
			--		case when s.holiday_name is null then 'Rejected' else 'Active' end as record_status,
			--		case when s.holiday_name is null then '101' else null end as error_code,
			--		case when s.holiday_name is null then null else s.holiday_code end as holiday_code, *
			from 
			indent_holiday ih 
			left join 
			(
				select hl.holiday_name, hl.holiday_code, hd.start_date, hd.end_date, hs.State 
				from holiday_list hl
				inner join holiday_date hd
				on hl.holiday_code = hd.holiday_code
				inner join holiday_states hs
				on hl.holiday_code = hs.holiday_code
				where 
				hl.record_status = 'Active' and
				hd.record_status = 'Active' and
				hs.record_status = 'Active' 

			) s
				on s.holiday_name = ih.holiday_name and
				s.State = ih.state_code and
				( cast(ih.holiday_date as date) >= s.start_date and cast(ih.holiday_date as date) <=s.end_date )
	
			where 
			ih.record_status = 'Uploaded' and
			ih.created_reference_id = @referenceid

			update data_update_log_master set record_status = 'Active'
				where 
				data_for_type = @datafor
				and created_reference_id = @referenceid
				and record_status = 'Uploaded'

			set @CountTotal = @@ROWCOUNT
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
			values ('Validation Ends. '+CAST(@CountTotal AS VARCHAR(30))+ 'records updated' , DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			
			if exists (	select 1 from data_update_log_master where 
						data_for_type = @datafor
						and created_reference_id <> @referenceid
						and record_status = 'Active'
				)
			begin

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Old record found in DUL', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				update data_update_log_master set record_status = 'History'
				where 
				data_for_type = @datafor
				and created_reference_id <> @referenceid
				and record_status = 'Active'


				update old set record_status = 'History'
				from indent_holiday old
				left join indent_holiday new
				on old.holiday_name = new.holiday_name
				and old.holiday_date = new.holiday_date
				and old.holiday_code = new.holiday_code
				and old.state_code = new.state_code
				and old.Feeder_branch = new.Feeder_branch
				and old.Bank_Code = new.Bank_Code 
				and old.Project_id = new.Project_id
				and old.cra = new.cra

				where 
				new.created_reference_id = @referenceid and
				new.record_status = 'Active' and
				old.record_status = 'Active' and
	
				new.holiday_code is not null and
				old.created_reference_id <> @referenceid


				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Old records marked history', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			end

			set @outputVal = (select cast(sequence as varchar(10)) from app_config_param where category = 'Indent Holiday Validation' and sub_category = 'Holiday_Validation_Success')
			
		end try

		begin catch
			set @outputVal = (select cast(sequence as varchar(10)) from app_config_param where category = 'Indent Holiday Validation' and sub_category = 'Holiday_Validation_Error')
		end catch
	
	end  

	else  -- not exists in DUL

	begin
		set @outputVal = ( select cast(sequence as varchar(10)) from app_config_param where category = 'Indent Holiday Validation' and sub_category = 'Holiday_Validation_Error')
	end

end