
CREATE Procedure [dbo].[usp_RenameFilesReport]
@path nvarchar(max),
@filename nvarchar(max),
@NewFileName nvarchar(200) out


--declare @path nvarchar(max) = 'E:\TestFolder\Temp'
--declare @filename nvarchar(max) = 'MOF_MAH&BOB&WARDHA&LOGICASH&Apr102018'
--declare @ReturnCode nvarchar(100) = ''
as

--Extract parameters from filename
Declare @ReturnCode nvarchar(1000)

declare @project nvarchar(20)
declare @bank nvarchar(20)
declare @feeder nvarchar(20)
declare @cra nvarchar(20)
declare @st nvarchar(200) = @FileName


--extracting information from filename
set @project = SUBSTRING (@st, 0, CHARINDEX('&', @st))
set @st = SUBSTRING(@st, CHARINDEX('&', @st)+1, 200)

set @bank = SUBSTRING (@st, 0, CHARINDEX('&', @st))
set @st = SUBSTRING(@st, CHARINDEX('&', @st)+1, 200)

set @feeder = SUBSTRING (@st, 0, CHARINDEX('&', @st))
set @st = SUBSTRING(@st, CHARINDEX('&', @st)+1, 200)

set @cra = SUBSTRING (@st, 0, CHARINDEX('&', @st))


--Renaming --prepare rename format like BOB_WARDHA_04172019_LOGICASH


print @bank
--For BOB, BOB MS
if (@bank in ('BOB', 'BOMH'))
begin
	set @NewFileName = @bank + '_' + @feeder + '_' +
	cast(CONVERT(nvarchar(2), GETDATE(), 101)as nvarchar) + cast(CONVERT(nvarchar(2), GETDATE(), 103)as nvarchar) + cast(DATEPART(year, getdate())as nvarchar)
	+ '_' + @cra + '.xlsx'
end
else
Begin
	RAISERROR ('Bank Configuration not present in usp_RenameFilesReport',16,1); 
end


--actual rename
declare @cmd nvarchar(4000) = ''
set @cmd = 'rename "' + @path + '\' + @filename + '" "' + @NewFileName + '"'
print @cmd
EXEC @ReturnCode =master.dbo.xp_cmdshell @cmd
if(@ReturnCode = 1)
RAISERROR ('Error while renaming files in usp_RenameFilesReport:%s',@cmd ,16,1); 




