

CREATE procedure [dbo].[SendMailReport] 
as 
begin

--this procedure will pick up the files from the directory, find its format and mail it to respective receipient 
--this works by taking file list from ReportFiles project , move file to temp one by one, then get its format, then acc to format 
--it will send mail and again file will get moved to archive 

Declare @RootDir nvarchar(100) = 'E:\C3R_Reports' 
Declare @Directory nvarchar(100) = @RootDir + '\ReportFiles\' 
Declare @TempFolder nvarchar(100) = @RootDir + '\Temp' 
--declare @ReturnCode nvarchar(10)


Declare @cmd nvarchar(100) = 'dir ' + @Directory + ' /b' 

--Load filenames in @files from selected directory
declare @files table (ID int IDENTITY, FileName nvarchar(100))
insert into @files execute xp_cmdshell @cmd    


declare @WorkingTable table (id int, FileName nvarchar(max), Project nvarchar(50), Bank nvarchar(30), feeder_branch nvarchar(100), CRA nvarchar(100), to_email_id nvarchar(200))

--Extracting fileformat from filename and getting to_mail from mail_master
insert into @WorkingTable
select a.*, to_CRA_email_id from (
select *,SUBSTRING (FileName, 0, CHARINDEX('&', FileName)) as Project
,SUBSTRING (SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200), 0, CHARINDEX('&', SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200))) as Bank 
,SUBSTRING (SUBSTRING(SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200), CHARINDEX('&', SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200))+1, 200), 0, CHARINDEX('&', SUBSTRING(SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200), CHARINDEX('&', SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200))+1, 200))) as Feeder_Branch
,SUBSTRING (SUBSTRING(SUBSTRING(SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200), CHARINDEX('&', SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200))+1, 200), CHARINDEX('&', SUBSTRING(SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200), CHARINDEX('&', SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200))+1, 200))+1, 200), 0, CHARINDEX('&', SUBSTRING(SUBSTRING(SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200), CHARINDEX('&', SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200))+1, 200), CHARINDEX('&', SUBSTRING(SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200), CHARINDEX('&', SUBSTRING(FileName, CHARINDEX('&', FileName)+1, 200))+1, 200))+1, 200))) as CRA
 from @files
 ) a 
 left join      --avoiding duplicate entries from mail_master below
 (select distinct project_id, bank_code, feeder_branch, cra, to_CRA_email_id from Mail_Master where record_status = 'Active') m
 on a.Project = m.project_id and a.Bank=m.bank_code and a.Feeder_Branch = m.feeder_branch and a.CRA = m.cra
 where FileName is not null



Declare @Guid UNIQUEIDENTIFIER = NEWID();
Declare @FileName nvarchar(Max)
Declare @BankCode nvarchar(10)
Declare @Project nvarchar(30)
declare @feeder_branch nvarchar(100)
declare @cra nvarchar(100)
declare @to_email nvarchar(200)


--declaring cursor whil will point to the files in the given directory
DECLARE FilePointer CURSOR FOR 
select FileName, Project, Bank, Feeder_branch, cra, to_email_id from @WorkingTable

OPEN FilePointer  
FETCH NEXT FROM FilePointer INTO @FileName, @Project, @BankCode, @feeder_branch, @cra, @to_email

WHILE @@FETCH_STATUS = 0  
BEGIN  
	if (ISNULL(@FileName, '') <> '')         --checking if @FileNmes is not empty
	begin

		Begin try


			--Move file to Temp folder
			Declare @src_path nvarchar(max) =  @Directory + @FileName
			exec [usp_MoveFiles_Report] @dest_path = @TempFolder, @src_path =@src_path, @IsArchive = '0'
				
			--Rename files
			print @filename
			EXEC usp_RenameFilesReport @path=@TempFolder,  @Filename=@FileName, @newFileName=@FileName output
			print @filename

					

			if (ISNULL(@to_email, '') <> '')     --exists in mail master
			begin
				---Sending Mails
				declare @fileattachment nvarchar(max) = @TempFolder +'\' + @FileName;
				declare @mailsubject nvarchar(100) = 'C3R Report For : ' +@BankCode
				print @fileattachment
				EXEC msdb.dbo.sp_send_dbmail @profile_name='TestProfile',
				@recipients=@to_email,
				@subject=@mailsubject,
				@body='Dear Sir / Madam, 
Kindly refer attached C3R Report.


Thanks & Regards
EPS Cash Management Team
Landline No : 022 - 42379624 / 26 / 27 / 28',
				@file_attachments= @fileattachment;

				--Logging status
				Insert into ReportLog values (@Guid, GetDate(), @FileName, 'OK', 'Email Queued :'+@to_email);
					

			end    --exists in mail_master 
			else   --not exists in mail_master  --log error
				begin
					Insert into ReportLog values (@Guid, GetDate(), @FileName, 'Error', 'No Configuration present in Mail_Master table for particular file : ' + @FileName);
				end
			

			    
				--move to archive from temp
			set @src_path  =  @TempFolder + '\'+ @FileName
			print @src_path
			Exec [usp_MoveFiles_Report] @dest_path = @RootDir, @src_path =@src_path, @IsArchive = '1'
				
				
		End try

		Begin Catch 
			Insert into ReportLog values (@Guid, GetDate(), @FileName, 'Error', ERROR_MESSAGE());
		End catch


	end   --(ISNULL(@FileName, '') <> '')     

	else   
	begin
		Insert into ReportLog values (@Guid, GetDate(), '', 'Error', 'No files present in the directory:'+@Directory);
	end


	
	FETCH NEXT FROM FilePointer INTO @FileName, @Project, @BankCode, @feeder_branch, @cra, @to_email 

End --While


CLOSE FilePointer  
DEALLOCATE FilePointer 

--Move all files after email
--exec usp_MoveFiles2 'E:\TestFolder',  'E:\TestFolder\ReportFiles\*'

END





--exec SendMailReport
--select * from ReportLog