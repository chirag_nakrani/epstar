
CREATE procedure [dbo].[usp_MoveFiles_Report]
(
@dest_path nvarchar(max),
@src_path nvarchar(max),
@IsArchive nvarchar(10)
)
as 


--this sp will take src and destination and move file accordingly. If @IsArchive=1 then it will archive, create archive folder and move file there

--Exec [usp_MoveFiles2] @dest_path = 'E:\TestFolder\Temp', @src_path = "E:\TestFolder\ReportFiles\adsas   asfd  asdf.xlsx", @IsArchive = '0'
--Exec [usp_MoveFiles2] @dest_path = 'E:\TestFolder', @src_path = "E:\TestFolder\Temp\adsas   asfd  asdf.xlsx", @IsArchive = '1'

--declare @path nvarchar(30) = 'E:\TestFolder'
--declare @FilePathToMove nvarchar(max) = 'E:\TestFolder\ReportFiles\adsas.xlsx'
Declare @cmd nvarchar(1000);
Declare @ReturnCode nvarchar(1000)

declare @files table (ID int IDENTITY, FileName nvarchar(100))
set @cmd = 'dir ' + @dest_path + ' /b'
insert into @files execute xp_cmdshell  @cmd 


if (@IsArchive = '1')  
begin 
	Declare @Foldername nvarchar(100) = 'ReportFiles_'+cast(day(getdate()) as nvarchar) + '_' + cast(month(getdate()) as nvarchar) + '_' + cast(year(getdate()) as nvarchar)
	if not exists (select * from @files where FileName = @Foldername)
	begin                     --checking if folder exitst for today, if no they create
		--print 'NO'
		SET @cmd = 'mkdir ' + @dest_path + '\' + @Foldername
		EXEC master..xp_cmdshell @cmd
	end

	set @cmd = 'MOVE  "' + @src_path + '" "' + @dest_path + '\'+ @Foldername + '"'
	print @cmd
	--EXEC master..xp_cmdshell @cmd
	EXEC @ReturnCode =master..xp_cmdshell @cmd
	if(@ReturnCode = '1')
	RAISERROR ('Error While moving files in usp_MoveFiles_Report:%s', @cmd ,16,1);


end

else 
begin 
	set @cmd = 'MOVE  "' + @src_path + '" "' + @dest_path + '"'
	print @cmd
	EXEC @ReturnCode =master..xp_cmdshell @cmd
	if(@ReturnCode = '1')
	RAISERROR ('Error While moving files in usp_MoveFiles_Report:%s', @cmd ,16,1);
end







