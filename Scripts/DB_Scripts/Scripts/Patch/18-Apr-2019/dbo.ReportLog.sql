USE [epstar]
GO

/****** Object:  Table [dbo].[ReportErrorLog]    Script Date: 18-04-2019 16:23:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ReportErrorLog](
	[Id] [uniqueidentifier] NULL,
	[TimeStamp] [datetime] NULL,
	[FileName] [varchar](max) NULL,
	[Status] [varchar](50) NULL,
	[MsgLog] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


