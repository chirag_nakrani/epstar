/****** Object:  Table [dbo].[atm_revision]    Script Date: 09-05-2019 10:35:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[atm_revision_cash_availability](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[feeder_branch_code] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[site_code] [nvarchar](50) NULL,
	[for_date] [datetime] NULL,
	[denomination_100] [int] NULL,
	[denomination_200] [int] NULL,
	[denomination_500] [int] NULL,
	[denomination_2000] [int] NULL,
	[total_amount] [int] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)
GO