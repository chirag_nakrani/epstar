/****** Object:  StoredProcedure [dbo].[uspSetStatus_master_ref]    Script Date: 24-04-2019 13:02:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uspSetStatus_master_ref]
            (
            @tablename  nvarchar(100),             
            @ToStatus	nvarchar(30) ,
            @date       nvarchar(50)  ,
            @statusby   nvarchar(50) , -- e.g approvedby,deletedby
            @id			nvarchar(max),            -- CSV
            @referenceid nvarchar(100) 
            )

AS

 DECLARE             @sql_set_p2  varchar(max)
 DECLARE             @sqls_p1 varchar(max)
 DECLARE             @sql_set_p3 varchar(max)
 DECLARE             @sql_WHERE varchar(max)
 DECLARE             @strQuery varchar(max)
 DECLARE			 @NumRowsChanged int
 DECLARE			 @errorcode int
 DECLARE			 @sqls_deleted varchar(max)
 DECLARE			 @outputval varchar(50)
 DECLARE             @refid VARCHAR(MAX)
 DECLARE			 @procedureName varchar(max)  = (SELECT OBJECT_NAME(@@PROCID))

BEGIN
    BEGIN TRY 
	SET NOCOUNT ON          
	   
	   DECLARE @datalogid      INT      
       DECLARE @totalcount     INT      
       DECLARE @pendingcount   INT      
       DECLARE @approvedcount  INT      
       DECLARE @rejectcount    INT    
	   DECLARE @pendingStatus varchar(50) = 'Approval Pending'
	   
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	values ('Execution of [dbo].[uspSetStatus_master_ref] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@statusby)



     BEGIN TRANSACTION   
		IF  @ToStatus='Active' 
        BEGIN
            -- Set variables to update the values
            SET @sql_set_p3 = ', modified_on= ''' + cast(@date as varchar(100))
            SET @sql_set_p3 = @sql_set_p3 + ''', modified_by = ''' + @statusby
            SET @sql_set_p3 = @sql_set_p3 + ''', modified_reference_id = ''' + @referenceid +''''

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			values ('Update status logic started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@statusby)
						
			----- Master Menus --------
            IF @tablename = 'atm_config_limits' OR @tablename = 'atm_master' OR @tablename = 'cra_feasibility'
                BEGIN
					
					 -- Set variables to update the values
					 SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
					 SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
					 SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
						
						-- Check if @id is 0 or list of conatenated string
						IF @id = '0' 
							BEGIN

							   -- Creating query to change status from active to history for old records on the basis of reference id
							   DECLARE @query_all_records  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							  'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							  ' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							  'join' + CHAR(10) +  
							  '(select atm_id, site_code FROM ' + @tablename+ ' WHERE created_reference_id = ''' +@referenceid+ ''' and record_status = ''' + @pendingStatus + ''') b on '+CHAR(10)+
							  'a.atm_id = b.atm_id and a.site_code = b.site_code and a.record_status = ''Active'''
							 
							  -- Creating query to change status from Approval Pending to Active for received reference id							  
							  DECLARE @query_all_records_active  nvarchar(max) = 'Update ' +@tablename +
							  ' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							  ' WHERE created_reference_id = ''' + @referenceid + ''' AND record_status = ''' + @pendingStatus +''''
							  print (@query_all_records)
							  print(@query_all_records_active)
							  EXEC (@query_all_records)
							  EXEC (@query_all_records_active)
						END
					ELSE
						BEGIN
							
							   -- Creating query to change status from active to history for old records on the basis of concatenated string received in @id
							   DECLARE @query  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							  'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							  ' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							  'join' + CHAR(10) +  
							  '(select atm_id, site_code FROM ' + @tablename+
							   ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							  'a.atm_id = b.atm_id and a.site_code = b.site_code and a.record_status = ''Active'''
							  	
							  -- Creating query to change status from Approval Pending to Active on the basis of concatenated string received in @id						  
							  DECLARE @query2  nvarchar(max) = 'Update ' +@tablename +
							  ' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							  ' WHERE id in  (' +@id +')'

							  EXEC (@query)
							  EXEC (@query2)
						END
                
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR

					-- Check if theres any error in executing the commands
                    IF (@ErrorCode = 0)
                    BEGIN

						-- Check if no. of rows updated are greater than 0
                        IF(@NumRowsChanged > 0)
                            BEGIN
								-- Store the record counts in variables and at the end update those in data_update_log_master table
                                IF(@tablename = 'atm_config_limits')
                                BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM atm_config_limits WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM atm_config_limits WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM atm_config_limits WHERE created_reference_id = @referenceid)
                                END    
                                IF(@tablename = 'atm_master')
                                BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM atm_master WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM atm_master WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM atm_master WHERE created_reference_id = @referenceid)
                                END    
                                IF(@tablename = 'cra_feasibility')
                                BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM cra_feasibility WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM cra_feasibility WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cra_feasibility WHERE created_reference_id = @referenceid) 
                                END    
                                SET @outputVal = 'S103'
                                SELECT @outputval    
                            END    
                            ELSE
                            BEGIN
                                 RAISERROR (70005,16,1)
                            END
                    END
                    ELSE
                    BEGIN 
                        RAISERROR (50005,16,1)    
                    END
                END
			IF @tablename = 'bank_limit_days'
				BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM bank_limit_days a JOIN 
							(SELECT bank_code,project_id, fordate
							FROM bank_limit_days WHERE created_reference_id = @referenceid and record_status = @pendingStatus) b 
							on CAST(a.fordate as date) = cast(b.fordate as date) and a.bank_code = b.bank_code and a.project_id = b.project_id 
							AND a.record_status = 'Active'
							
							UPDATE bank_limit_days
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							 DECLARE @bank_limit  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							 'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							 ' from'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							 'join' + CHAR(10) +  
							 '(select fordate, bank_code, project_id from ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							 'CAST(a.fordate as date) = cast(b.fordate as date) and a.bank_code = b.bank_code and a.project_id = b.project_id and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							 DECLARE @bank_limit1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' where id in  (' +@id +')'

							EXEC (@bank_limit)
							EXEC (@bank_limit1)
						END
					 
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM bank_limit_days WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM bank_limit_days WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM bank_limit_days WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END

				END

			IF @tablename = 'feeder_limit_days'
				BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM feeder_limit_days a JOIN 
							(SELECT bank_code,project_id, fordate, feeder, cra
							FROM feeder_limit_days WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on CAST(a.fordate as date) = cast(b.fordate as date) and a.bank_code = b.bank_code and a.project_id = b.project_id
							and a.feeder = b.feeder and a.cra = b.cra
							AND a.record_status = 'Active'
							
							UPDATE feeder_limit_days
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							 DECLARE @feeder_limit  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							 'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							 ' from'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							 'join' + CHAR(10) +  
							 '(select fordate, bank_code, project_id, feeder, cra from ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							 'CAST(a.fordate as date) = cast(b.fordate as date) and a.feeder = b.feeder and a.cra = b.cra and a.bank_code = b.bank_code and a.project_id = b.project_id and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							 DECLARE @feeder_limit1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' where id in  (' +@id +')'

							EXEC (@feeder_limit)
							EXEC (@feeder_limit1)
						END
					 
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM feeder_limit_days WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM feeder_limit_days WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM feeder_limit_days WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END

				END

			IF @tablename = 'vaulting_of_balance'
				BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM vaulting_of_balance a JOIN 
							(SELECT bank_code,feeder_branch_code, atm_id, cra
							FROM vaulting_of_balance WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on  a.bank_code = b.bank_code and a.feeder_branch_code = b.feeder_branch_code
							and a.atm_id = b.atm_id and a.cra = b.cra
							AND a.record_status = 'Active'
							
							UPDATE vaulting_of_balance
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							 DECLARE @vob  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							 'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							 ' from'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							 'join' + CHAR(10) +  
							 '(select bank_code, feeder_branch_code, atm_id, cra from ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							 'a.feeder_branch_code = b.feeder_branch_code and a.cra = b.cra and a.bank_code = b.bank_code and a.atm_id = b.atm_id and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							 DECLARE @vob1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' where id in  (' +@id +')'

							EXEC (@vob)
							EXEC (@vob1)
						END
					 
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM vaulting_of_balance WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM vaulting_of_balance WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM vaulting_of_balance WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END

				END
            IF @tablename = 'feeder_branch_master'
                BEGIN
				 SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
				 SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
				 SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''

					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM feeder_branch_master a JOIN 
							(SELECT bank_code,project_id,region_code, feeder_branch FROM feeder_branch_master WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.bank_code = b.bank_code AND a.project_id=b.project_id AND a.region_code = b.region_code AND a.feeder_branch = b.feeder_branch and a.record_status = 'Active'
							
							UPDATE feeder_branch_master
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
								  AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN

							DECLARE @queryFeeder  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select bank_code,project_id,region_code, feeder_branch FROM ' + @tablename+ ' WHERE id in (' +@id + ')) b on '+CHAR(10)+
							'a.bank_code = b.bank_code AND a.feeder_branch=b.feeder_branch AND a.region_code = b.region_code AND a.feeder_branch = b.feeder_branch and a.record_status = ''Active'''
			
							DECLARE @queryfeeder2  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  ('+ @id +')'

							--PRINT(@queryFeeder)
							--PRINT(@queryfeeder2)
							EXEC (@queryFeeder)
							EXEC (@queryfeeder2)
						END

                    
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM feeder_branch_master WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM feeder_branch_master WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM feeder_branch_master WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END

                END 
			IF @tablename = 'brand_bill_capacity'
                BEGIN
                    SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
					SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
					SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
					
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM brand_bill_capacity a JOIN 
							(SELECT brand_code FROM brand_bill_capacity WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.brand_code = b.brand_code  and a.record_status = 'Active'
							
							UPDATE brand_bill_capacity
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							 AND record_status = @pendingStatus

						END
					ELSE 
						BEGIN
							DECLARE @querybrandbill  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select brand_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.brand_code = b.brand_code and a.record_status = ''Active'''
							
							DECLARE @querybrandbill1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@querybrandbill)
							EXEC (@querybrandbill1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM brand_bill_capacity WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM brand_bill_capacity WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM brand_bill_capacity WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END

                END
			IF @tablename = 'cash_pre_availability'
				BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM cash_pre_availability a JOIN 
							(SELECT Feeder_Branch_Code, bank_code,project_id 
							FROM cash_pre_availability WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.Feeder_Branch_Code = b.Feeder_Branch_Code and a.bank_code = b.bank_code and a.project_id = b.project_id 
							AND a.record_status = 'Active'
							
							UPDATE cash_pre_availability
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							 DECLARE @preAvail  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							 'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							 ' from'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							 'join' + CHAR(10) +  
							 '(select feeder_branch_code, bank_code, project_id from ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							 'a.feeder_branch_code = b.feeder_branch_code and a.bank_code = b.bank_code and a.project_id = b.project_id and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							 DECLARE @preAvail1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' where id in  (' +@id +')'

							EXEC (@preAvail)
							EXEC (@preAvail1)
						END
					 
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM cash_pre_availability WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM cash_pre_availability WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cash_pre_availability WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END

				END
			IF @tablename = 'CRA_Empaneled'
                BEGIN
				IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM CRA_Empaneled a JOIN 
							(SELECT cra_code FROM CRA_Empaneled WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.cra_code = b.cra_code AND a.record_status = 'Active'
							
							UPDATE CRA_Empaneled
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							 DECLARE @queryEmpaneled  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							 'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							 ' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							 'join' + CHAR(10) +  
							 '(select cra_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							 'a.cra_code = b.cra_code and a.record_status = ''Active'''

							 
							 SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							 SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							 SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							 
							 DECLARE @queryEmpaneled1  nvarchar(max) = 'Update ' +@tablename +
							 ' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							 ' WHERE id in  (' +@id +')'

							 EXEC (@queryEmpaneled)
							 EXEC (@queryEmpaneled1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM CRA_Empaneled WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM CRA_Empaneled WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM CRA_Empaneled WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
			IF @tablename = 'cra_vault_master'
                BEGIN
                   IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM cra_vault_master a JOIN 
							(SELECT vault_code FROM cra_vault_master WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.vault_code = b.vault_code AND a.record_status = 'Active'
							
							UPDATE cra_vault_master
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							DECLARE @query_vault_master  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select vault_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.vault_code = b.vault_code and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_vault_master1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'
							EXEC (@query_vault_master)
							EXEC (@query_vault_master1)
						END
                   
				    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM cra_vault_master WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM cra_vault_master WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cra_vault_master WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
			IF @tablename = 'cypher_code'
                BEGIN
				IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM cypher_code a JOIN 
							(SELECT bank_code,project_id FROM cypher_code WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.bank_code = b.bank_code AND a.project_id = b.project_id AND a.record_status = 'Active'
							
							UPDATE cypher_code
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							 DECLARE @query_cypher_code  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							 'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							 ' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							 'join' + CHAR(10) +  
							 '(select bank_code,project_id FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							 'a.bank_code = b.bank_code and a.project_id = b.project_id and a.record_status = ''Active'''

							 
							 SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							 SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							 SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							 
							 DECLARE @query_cypher_code1  nvarchar(max) = 'Update ' +@tablename +
							 ' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							 ' WHERE id in  (' +@id +')'

							 EXEC (@query_cypher_code)
							 EXEC (@query_cypher_code1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM cypher_code WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM cypher_code WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cypher_code WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
			IF @tablename = 'default_loading'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM default_loading a JOIN 
							(SELECT bank_code,project_id FROM default_loading WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.bank_code = b.bank_code AND a.project_id = b.project_id AND a.record_status = 'Active'
							
							UPDATE default_loading
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							DECLARE @query_default_loading  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select bank_code,project_id FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.bank_code = b.bank_code and a.project_id = b.project_id and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_default_loading1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'
							EXEC (@query_cypher_code)
							EXEC (@query_cypher_code1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM default_loading WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM default_loading WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM default_loading WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
			IF @tablename = 'bank_escalation_emails'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM bank_escalation_emails a JOIN 
							(SELECT bank,branch FROM bank_escalation_emails WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.bank = b.bank AND a.branch = b.branch AND a.record_status = 'Active'
							
							UPDATE bank_escalation_emails
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							DECLARE @query_bank_emails  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select bank,branch FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.bank = b.bank and a.branch = b.branch and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_bank_emails1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'
							EXEC (@query_bank_emails)
							EXEC (@query_bank_emails1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM bank_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM bank_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM bank_escalation_emails WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
			IF @tablename = 'cra_escalation_emails'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM cra_escalation_emails a JOIN 
							(SELECT cra FROM cra_escalation_emails WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.cra = b.cra AND a.record_status = 'Active'
							
							UPDATE cra_escalation_emails
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							DECLARE @query_cra_emails  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select cra FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.cra = b.cra and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_cra_emails1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'
							EXEC (@query_cra_emails)
							EXEC (@query_cra_emails1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM cra_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(1) FROM cra_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cra_escalation_emails WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END

				IF @tablename = 'eps_escalation_emails'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM eps_escalation_emails a JOIN 
							(SELECT primary_email_id,bank FROM eps_escalation_emails WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.primary_email_id = b.primary_email_id AND a.bank = b.bank and a.record_status = 'Active'
							
							UPDATE eps_escalation_emails
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							
						END
					ELSE 
						BEGIN
							DECLARE @query_eps_emails  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select primary_email_id,bank FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.primary_email_id = b.primary_email_id AND a.bank = b.bank and a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_eps_emails1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'
							EXEC (@query_eps_emails)
							EXEC (@query_eps_emails1)
							--print(@query_eps_emails)
							--print(@query_eps_emails1)
						END		  
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(*) FROM eps_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(*) FROM eps_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(*) FROM eps_escalation_emails WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
				IF @tablename = 'limitdays'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM limitdays a JOIN 
							(SELECT atm_id,eps_site_code FROM limitdays WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.atm_id = b.atm_id AND a.eps_site_code = b.eps_site_code AND a.record_status = 'Active'
							
							UPDATE limitdays
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
						END
					ELSE 
						BEGIN
							DECLARE @query_limitdays  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select atm_id,eps_site_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.atm_id = b.atm_id AND a.eps_site_code = b.eps_site_code AND a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_limitdays1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@query_limitdays)
							EXEC (@query_limitdays1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM limitdays WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT  count(1) FROM limitdays WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT    count(1) FROM limitdays WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
				IF @tablename = 'indent_pre_qualify_atm'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM indent_pre_qualify_atm a JOIN 
							(SELECT atm_id,site_code FROM indent_pre_qualify_atm WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.atm_id = b.atm_id AND a.site_code = b.site_code AND a.record_status = 'Active'
							
							UPDATE indent_pre_qualify_atm
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							DECLARE @query_indent_Qualify  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select atm_id,site_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.atm_id = b.atm_id AND a.site_code = b.site_code AND a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_indent_Qualify1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@query_indent_Qualify)
							EXEC (@query_indent_Qualify1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM indent_pre_qualify_atm WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT  count(1) FROM indent_pre_qualify_atm WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT    count(1) FROM indent_pre_qualify_atm WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
				IF @tablename = 'indent_eod_pre_activity'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM indent_eod_pre_activity a JOIN 
							(SELECT atm_id,site_code FROM indent_eod_pre_activity WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.atm_id = b.atm_id AND a.site_code = b.site_code AND a.record_status = 'Active'
							
							UPDATE indent_eod_pre_activity
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							DECLARE @query_indent_eod  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select atm_id,site_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.atm_id = b.atm_id AND a.site_code = b.site_code AND a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_indent_eod1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@query_indent_eod)
							EXEC (@query_indent_eod1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM indent_eod_pre_activity WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT  count(1) FROM indent_eod_pre_activity WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT    count(1) FROM indent_eod_pre_activity WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
				IF @tablename = 'modify_cassette_pre_config'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM modify_cassette_pre_config a JOIN 
							(SELECT atm_id,site_code FROM modify_cassette_pre_config WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.atm_id = b.atm_id AND a.site_code = b.site_code AND a.record_status = 'Active'
							
							UPDATE modify_cassette_pre_config
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							DECLARE @query_modify_cassettee  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select atm_id,site_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.atm_id = b.atm_id AND a.site_code = b.site_code AND a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_modify_cassettee1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@query_modify_cassettee)
							EXEC (@query_modify_cassettee1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM modify_cassette_pre_config WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT  count(1) FROM modify_cassette_pre_config WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT    count(1) FROM modify_cassette_pre_config WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
				IF @tablename = 'feeder_vaulting_pre_config'
                BEGIN
					IF @id = '0'
						BEGIN			
						--	UPDATE a 
						--	BEGIN			
							--UPDATE a 
							--SET a.record_status = 'History',
							--	modified_on =  cast(@date as varchar(100)),
							--	modified_by = @statusby,
							--	modified_reference_id = @referenceid
							--FROM feeder_vaulting_pre_config a JOIN 
							--(SELECT feeder_branch_code FROM feeder_vaulting_pre_config WHERE created_reference_id = @referenceid and record_status =  @pendingStatus) b 
							--on a.feeder_branch_code = b.feeder_branch_code AND a.record_status = 'Active'
							

							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM feeder_vaulting_pre_config  a
							where  a.record_status = 'Active'
							and a.feeder_branch_code  in
							(SELECT feeder_branch_code FROM feeder_vaulting_pre_config WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
							
							UPDATE feeder_vaulting_pre_config
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
						END
					ELSE 
						BEGIN
							DECLARE @query_feeder_vaulting  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select feeder_branch_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							'a.feeder_branch_code = b.feeder_branch_code AND a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_feeder_vaulting1 nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@query_feeder_vaulting)
							EXEC (@query_feeder_vaulting1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM feeder_vaulting_pre_config WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT  count(1) FROM feeder_vaulting_pre_config WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT    count(1) FROM feeder_vaulting_pre_config WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
				IF @tablename = 'feeder_cassette_max_capacity_percentage'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM feeder_cassette_max_capacity_percentage a JOIN 
							(SELECT project_id,bank_code,feeder_branch_code FROM feeder_cassette_max_capacity_percentage WHERE created_reference_id = @referenceid and record_status =  @pendingStatus )  b 
							on a.project_id = b.project_id AND a.bank_code = b.bank_code AND a.feeder_branch_code = b.feeder_branch_code AND a.record_status = 'Active'
							
							UPDATE feeder_cassette_max_capacity_percentage
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
						END
					ELSE 
						BEGIN
							DECLARE @query_feeder_cap_max  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select project_id,bank_code,feeder_branch_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							' a.project_id = b.project_id AND a.bank_code = b.bank_code AND a.feeder_branch_code = b.feeder_branch_code AND a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_feeder_cap_max1 nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@query_feeder_cap_max)
							EXEC (@query_feeder_cap_max1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM feeder_cassette_max_capacity_percentage WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT  count(1) FROM feeder_cassette_max_capacity_percentage WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT    count(1) FROM feeder_cassette_max_capacity_percentage WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
				IF @tablename = 'feeder_denomination_priority'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM feeder_denomination_priority a JOIN 
							(SELECT project_id,bank_code,feeder_branch_code FROM feeder_denomination_priority WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.project_id = b.project_id AND a.bank_code = b.bank_code AND a.feeder_branch_code = b.feeder_branch_code AND a.record_status = 'Active'
							
							UPDATE feeder_denomination_priority
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
						END
					ELSE 
						BEGIN
							DECLARE @query_feeder_deno  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select project_id,bank_code,feeder_branch_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							' a.project_id = b.project_id AND a.bank_code = b.bank_code AND a.feeder_branch_code = b.feeder_branch_code AND a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_feeder_deno1 nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@query_feeder_deno)
							EXEC (@query_feeder_deno1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM feeder_denomination_priority WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT  count(1) FROM feeder_denomination_priority WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT    count(1) FROM feeder_denomination_priority WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END
				IF @tablename = 'feeder_denomination_pre_availability'
                BEGIN
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM feeder_denomination_pre_availability a JOIN 
							(SELECT project_id,bank_code,feeder_branch_code FROM feeder_denomination_pre_availability WHERE created_reference_id = @referenceid and record_status =  @pendingStatus ) b 
							on a.project_id = b.project_id AND a.bank_code = b.bank_code AND a.feeder_branch_code = b.feeder_branch_code AND a.record_status = 'Active'
							
							UPDATE feeder_denomination_pre_availability
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid
							AND record_status = @pendingStatus
							
						END
					ELSE 
						BEGIN
							DECLARE @query_feeder_deno_pre_avail  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select project_id,bank_code,feeder_branch_code FROM ' + @tablename+ ' WHERE id in (' +@id+ ')) b on '+CHAR(10)+
							' a.project_id = b.project_id AND a.bank_code = b.bank_code AND a.feeder_branch_code = b.feeder_branch_code AND a.record_status = ''Active'''

							
							SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
							SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
							DECLARE @query_feeder_deno_pre_avail1 nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  (' +@id +')'

							EXEC (@query_feeder_deno_pre_avail)
							EXEC (@query_feeder_deno_pre_avail1)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(1) FROM feeder_denomination_pre_availability WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT  count(1) FROM feeder_denomination_pre_availability WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT    count(1) FROM feeder_denomination_pre_availability WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END
                END

				IF @tablename = 'Auth_Signatories_Signatures'
                BEGIN
                    SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
					SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
					SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
					
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM Auth_Signatories_Signatures a JOIN
							(select distinct project_id, bank_code, signatory_name from Auth_Signatories_Signatures where created_reference_id = @referenceid and record_status =  @pendingStatus) b
							on a.project_id = b.project_id and a.bank_code = b.bank_code and a.signatory_name = b.signatory_name and a.record_status = 'Active'
							
							
							UPDATE Auth_Signatories_Signatures
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid and record_status = 'Approval Pending'

						END
					ELSE 
						BEGIN
						
							DECLARE @authquery nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select bank_code,project_id ,signatory_name FROM ' + @tablename+ ' WHERE id in (' +@id + ')) b on '+CHAR(10)+
							' a.bank_code = b.bank_code AND a.project_id = b.project_id AND a.signatory_name = b.signatory_name and a.record_status = ''Active'''
			
							DECLARE @authquery1  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  ('+ @id +')'
							
							
							EXEC (@authquery)
							EXEC (@authquery1)
							--UPDATE a 
							--SET a.record_status = 'History',
							--	modified_on =  cast(@date as varchar(100)),
							--	modified_by = @statusby,
							--	modified_reference_id = @referenceid
							--FROM Auth_Signatories_Signatures a JOIN
							--(select distinct project_id, bank_code, signatory_name from Auth_Signatories_Signatures where id = @id) b
							--on a.project_id = b.project_id and a.bank_code = b.bank_code and a.signatory_name = b.signatory_name and a.record_status = 'Active'
							
							
							--UPDATE Auth_Signatories_Signatures
							--SET record_status = @ToStatus,
							--	approved_on =  cast(@date as varchar(100)),
							--	approved_by = @statusby,
							--	approved_reference_id = @referenceid
							--WHERE created_reference_id = @referenceid and record_status = 'Approval Pending' and id = @id


						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(*) FROM Auth_Signatories_Signatures WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(*) FROM Auth_Signatories_Signatures WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(*) FROM Auth_Signatories_Signatures WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END

                END
				IF @tablename = 'mail_master'
                BEGIN
                    SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
					SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
					SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
					
					IF @id = '0'
						BEGIN			
							UPDATE a 
							SET a.record_status = 'History',
								modified_on =  cast(@date as varchar(100)),
								modified_by = @statusby,
								modified_reference_id = @referenceid
							FROM mail_master a JOIN
							(select distinct project_id, bank_code, feeder_branch from mail_master where created_reference_id = @referenceid and record_status =  @pendingStatus) b
							on a.project_id = b.project_id and a.bank_code = b.bank_code and a.feeder_branch = b.feeder_branch and a.record_status = 'Active'
							
							
							UPDATE mail_master
							SET record_status = @ToStatus,
								approved_on =  cast(@date as varchar(100)),
								approved_by = @statusby,
								approved_reference_id = @referenceid
							WHERE created_reference_id = @referenceid and record_status = 'Approval Pending'

						END
					ELSE 
						BEGIN
						DECLARE @querymailmaster nvarchar(max) = 'UPDATE a ' +CHAR(10)+
							'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
							' FROM'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
							'join' + CHAR(10) +  
							'(select bank_code,project_id ,feeder_branch FROM ' + @tablename+ ' WHERE id in (' +@id + ')) b on '+CHAR(10)+
							' a.bank_code = b.bank_code AND a.project_id = b.project_id AND a.feeder_branch = b.feeder_branch and a.record_status = ''Active'''
			
							DECLARE @querymailmaster2  nvarchar(max) = 'Update ' +@tablename +
							' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
							' WHERE id in  ('+ @id +')'
							
							
							EXEC (@querymailmaster)
							EXEC (@querymailmaster2)
						END
                    SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
                    IF (@ErrorCode = 0)
                        BEGIN
                            IF(@NumRowsChanged > 0)
                            BEGIN
                                    SET @approvedcount = (SELECT count(*) FROM mail_master WHERE created_reference_id = @referenceid and record_status = 'Active')
                                    SET @pendingcount = (SELECT count(*) FROM mail_master WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(*) FROM mail_master WHERE created_reference_id = @referenceid)
                                    SET @outputVal = 'S103'
                                    SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                        END
                        ELSE
                            BEGIN
                                RAISERROR (50005,16,1)    
                            END

                END
				IF @tablename = 'atm_revision_cash_availability'
					BEGIN
						IF @id = '0'
							BEGIN			
								UPDATE a 
								SET a.record_status = 'History',
									modified_on =  cast(@date as varchar(100)),
									modified_by = @statusby,
									modified_reference_id = @referenceid
								FROM atm_revision_cash_availability a JOIN 
								(SELECT bank_code,project_id,feeder_branch_code,atm_id,site_code
								FROM atm_revision_cash_availability WHERE created_reference_id = @referenceid and record_status = @pendingStatus) b 
								on a.bank_code = b.bank_code and a.project_id = b.project_id and a.feeder_branch_code = b.feeder_branch_code and a.atm_id = b.atm_id and a.site_code = b.site_code
								AND a.record_status = 'Active'
							
								UPDATE atm_revision_cash_availability
								SET record_status = @ToStatus,
									approved_on =  cast(@date as varchar(100)),
									approved_by = @statusby,
									approved_reference_id = @referenceid
								WHERE created_reference_id = @referenceid
								AND record_status = @pendingStatus
							
							END
						ELSE 
							BEGIN
								 DECLARE @atm_revision  nvarchar(max) = 'UPDATE a ' +CHAR(10)+
								 'set a.record_status = ''History'''+CHAR(10)+ @sql_set_p3 + 
								 ' from'+CHAR(10)+ @tablename + ' a '+ CHAR(10) +
								 'join' + CHAR(10) +  
								 '(select bank_code, project_id, feeder_branch_code, atm_id, site_code from ' + @tablename+ ' WHERE id in (' +@id+ ')) b on'+CHAR(10)+' a.bank_code = b.bank_code and a.project_id = b.project_id and a.feeder_branch_code = b.feeder_branch_code and a.atm_id = b.atm_id and a.site_code = b.site_code and a.record_status = ''Active'''

							
								SET @sql_set_p3 = @sql_set_p3 + ', approved_on = ''' + cast(@date as varchar(100))
								SET @sql_set_p3 = @sql_set_p3 + ''', approved_by = ''' + @statusby
								SET @sql_set_p3 = @sql_set_p3 + ''', approved_reference_id = ''' + @referenceid +''''
							
								 DECLARE @atm_revision1  nvarchar(max) = 'Update ' +@tablename +
								' SET record_status = ''' + @ToStatus + ''''+ @sql_set_p3 + 
								' where id in  (' +@id +')'

								EXEC (@atm_revision)
								EXEC (@atm_revision1)
							END
					 
						SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
						IF (@ErrorCode = 0)
							BEGIN
								IF(@NumRowsChanged > 0)
								BEGIN
										SET @approvedcount = (SELECT count(1) FROM atm_revision_cash_availability WHERE created_reference_id = @referenceid and record_status = 'Active')
										SET @pendingcount = (SELECT count(1) FROM atm_revision_cash_availability WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
										SET @totalcount = (SELECT count(1) FROM atm_revision_cash_availability WHERE created_reference_id = @referenceid)
										SET @outputVal = 'S103'
										SELECT @outputval
								END
								ELSE
								BEGIN
									RAISERROR (70005,16,1)
								END
							END
							ELSE
								BEGIN
									RAISERROR (50005,16,1)    
								END

					END
				 
					  UPDATE data_update_log_master
					  SET approved_count = @approvedcount,
						  pending_count = @pendingcount,
						  total_count = @totalcount
						  WHERE created_reference_id =@referenceid
				END

    
	IF  @ToStatus='Rejected' 
		-- If approver reject the record then mark status as rejected
		-- but need to consider the same case
		-- if @id = 0, then reject all the records based on reference id
		-- else reject on the basis of values received in @id parameter
        BEGIN

            SET @sql_set_p3 = ', modified_on= ''' + cast(@date as varchar(100))
            SET @sql_set_p3 = @sql_set_p3 + ''', modified_by = ''' + @statusby
            SET @sql_set_p3 = @sql_set_p3 + ''', modified_reference_id = ''' + @referenceid +''''

            SET @sqls_p1 = ', rejected_on= ''' + cast(@date as varchar(100))
            SET @sqls_p1 = @sqls_p1 + ''', rejected_by = ''' + @statusby
            SET @sqls_p1 = @sqls_p1 + ''', reject_reference_id = ''' + @referenceid +''''
            
			IF @id = '0'
				BEGIN
					DECLARE @query3  nvarchar(max) = 'Update ' +@tablename +
					' SET record_status = ''' + @ToStatus + ''''+ @sqls_p1 + @sql_set_p3 +
					' WHERE created_reference_id = ''' + @referenceid + ''' and record_status = ''' + @pendingStatus + ''''

					EXEC (@query3)
				END
			ELSE
				BEGIN
					DECLARE @query_id  nvarchar(max) = 'Update ' +@tablename +
					' SET record_status = ''' + @ToStatus + ''''+ @sqls_p1 + @sql_set_p3 +
					' WHERE id in  (' +@id +')'
					
				EXEC (@query_id)
			END        

        
            SELECT @NumRowsChanged = @@ROWCOUNT , @ErrorCode = @@ERROR
            IF (@ErrorCode = 0)
                BEGIN
                        IF(@NumRowsChanged > 0)
                            BEGIN
                                IF(@tablename = 'atm_config_limits')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM atm_config_limits WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM atm_config_limits WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM atm_config_limits WHERE created_reference_id = @referenceid)
                                END
                                IF(@tablename = 'atm_master')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM atm_master WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM atm_master WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM atm_master WHERE created_reference_id = @referenceid)
                                END
                                IF(@tablename = 'cra_feasibility')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM cra_feasibility WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM cra_feasibility WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cra_feasibility WHERE created_reference_id = @referenceid)
                                END
                                IF(@tablename = 'feeder_branch_master')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM feeder_branch_master WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM feeder_branch_master WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM feeder_branch_master WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'brand_bill_capacity')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM brand_bill_capacity WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM brand_bill_capacity WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM brand_bill_capacity WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'cash_pre_availability')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM cash_pre_availability WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM cash_pre_availability WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cash_pre_availability WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'CRA_Empaneled')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM CRA_Empaneled WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM CRA_Empaneled WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM CRA_Empaneled WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'cra_vault_master')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM cra_vault_master WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM cra_vault_master WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cra_vault_master WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'default_loading')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM default_loading WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM default_loading WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM default_loading WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'cypher_code')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM cypher_code WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM cypher_code WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cypher_code WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'bank_escalation_emails')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM bank_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM bank_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM bank_escalation_emails WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'cra_escalation_emails')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM cra_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM cra_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM cra_escalation_emails WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'eps_escalation_emails')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM eps_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM eps_escalation_emails WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(*) FROM eps_escalation_emails WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'limitdays')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM limitdays  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM limitdays WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM limitdays   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'indent_eod_pre_activity')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM indent_eod_pre_activity  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM indent_eod_pre_activity WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM indent_eod_pre_activity   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'indent_pre_qualify_atm')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM indent_pre_qualify_atm  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM indent_pre_qualify_atm WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM indent_pre_qualify_atm   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'modify_cassette_pre_config')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM modify_cassette_pre_config  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM modify_cassette_pre_config WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM modify_cassette_pre_config   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'feeder_vaulting_pre_config')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM feeder_vaulting_pre_config  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM feeder_vaulting_pre_config WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM feeder_vaulting_pre_config   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'feeder_cassette_max_capacity_percentage')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM feeder_cassette_max_capacity_percentage  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM feeder_cassette_max_capacity_percentage WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM feeder_cassette_max_capacity_percentage   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'feeder_denomination_priority')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM feeder_denomination_priority  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM feeder_denomination_priority WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM feeder_denomination_priority   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'feeder_denomination_pre_availability')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM feeder_denomination_pre_availability  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM feeder_denomination_pre_availability WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM feeder_denomination_pre_availability   WHERE created_reference_id = @referenceid)
                                END
								

								IF(@tablename = 'feeder_limit_days')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM feeder_limit_days  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM feeder_limit_days WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM feeder_limit_days   WHERE created_reference_id = @referenceid)
                                END

								IF(@tablename = 'bank_limit_days')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM bank_limit_days  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM bank_limit_days WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM bank_limit_days   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'vaulting_of_balance')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM vaulting_of_balance  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM vaulting_of_balance WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM vaulting_of_balance   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'Auth_Signatories_Signatures')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM Auth_Signatories_Signatures  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM Auth_Signatories_Signatures WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM Auth_Signatories_Signatures   WHERE created_reference_id = @referenceid)
                                END
								IF(@tablename = 'atm_revision_cash_availability')
                                BEGIN
                                    SET @rejectcount = (SELECT count(1) FROM atm_revision_cash_availability  WHERE created_reference_id = @referenceid and record_status = 'Rejected')
                                    SET @pendingcount = (SELECT count(1) FROM atm_revision_cash_availability WHERE created_reference_id = @referenceid and record_status = 'Approval Pending')
									SET @totalcount = (SELECT count(1) FROM atm_revision_cash_availability   WHERE created_reference_id = @referenceid)
                                END
		                        SET @outputVal = 'S104'
                                SELECT @outputval
                            END
                            ELSE
                            BEGIN
                                RAISERROR (70005,16,1)
                            END
                END
                ELSE 
                BEGIN
                    RAISERROR (50005,16,1)    
                END

					UPDATE data_update_log_master
					SET reject_count = @rejectcount,
					    pending_count = @pendingcount,
						total_count = @totalcount
					    WHERE created_reference_id = @referenceid

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			values ('Update status logic Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@statusby)
					
        END

	-- Check if pending count is 0 or not
	-- if it is not 0 then show record_status as approval pending until all records has been approved

	DECLARE @dataup int = (SELECT pending_count FROM data_update_log_master WHERE created_reference_id = @referenceid)
		IF (@dataup = 0)
		BEGIN
		        UPDATE data_update_log_master
		        SET record_status = 
		          CASE WHEN approved_count > 0
		              THEN 'Active'
		              WHEN reject_count > 0
		              THEN 'Rejected'
		          END
		        WHERE created_reference_id = @referenceid
		END
   COMMIT TRANSACTION 

END TRY
BEGIN CATCH 
      IF(@@TRANCOUNT > 0 )
		BEGIN
			ROLLBACK TRAN;
		END 
            DECLARE @ErrorNumber INT = ERROR_NUMBER();
            DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
            DECLARE @ErrorState INT = ERROR_STATE();
            DECLARE @ErrorProcedure varchar(50) = @procedureName;
            DECLARE @ErrorLine INT = ERROR_LINE();
            DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
            DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE())
                

            INSERT INTO dbo.error_log values
            (@ErrorNumber,@ErrorSeverity,
            @ErrorState,@ErrorProcedure,
            @ErrorLine,@ErrorMessage,@dateAdded)

            IF @ErrorNumber = 50005
            BEGIN
                SET @outputVal = 'E104'
            END
            ELSE IF @ErrorNumber = 70005
            BEGIN
                SET @outputVal = 'E105'
            END
			ELSE 
			BEGIN
				SET @outputval = @ErrorNumber
			END
         SELECT @outputVal
   
 END CATCH
 END