ALTER TABLE system_settings 
ADD denomination_wise_round_off_50_currency_chest INT,
	denomination_wise_round_off_100_currency_chest INT,
	denomination_wise_round_off_200_currency_chest INT,
	denomination_wise_round_off_500_currency_chest INT,
	denomination_wise_round_off_2000_currency_chest INT


UPDATE system_settings 
SET denomination_wise_round_off_100_currency_chest = 100000	 ,
	denomination_wise_round_off_200_currency_chest = 200000	 ,
	denomination_wise_round_off_500_currency_chest = 500000	 ,
	denomination_wise_round_off_2000_currency_chest = 200000
WHERE record_status = 'Active'