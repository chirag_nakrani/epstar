 --- =========================================================================
 --- Created By  :Rubina Q
 --- Created Date: 12-12-2018
 --- Description  : Calculate dispense amount for each of 28 datapoints as mentioned in FSD.
 --                Note* The datapoints for which the data i snot present the result will be NULL
 --                for that datapoint.

  -- Select dbo.[fn_28datapoints] ('DBD00951','2018-11-11','DENA',9,'MOF')
 -- ===========================================================================      

 
 ALTER FUNCTION [dbo].[fn_28datapoints]
(@atm_id varchar(20),@datetoday date,@bank varchar(50), @fordatapoint int ,@project varchar(30))
RETURNS int


AS
BEGIN 
    --  ***********For Testing Purpose********************************
	--  select [dbo].[fn_28datapoints]('NA1400C1','2018-12-26','BOMH',20,'MOF')

	--	declare @atm_id varchar(20),@datetoday date,@bank varchar(50), @fordatapoint int ,@project varchar(30)
	--	set @datetoday = '2018-12-26'
	--	set @atm_id = 'NA1400C1'
	--	set @bank = 'BOMH'
	--	set @fordatapoint = 1
	--	set @project = 'MOF'
	--  Select (CONVERT (date, ( CONVERT(varchar,    DATEADD(month,-3, DATEADD(day,-3,'2018-12-26 00:00:00.000')  )  , 105) ),105) )
    --  **************************************************************
	 
	 
	   DECLARE  @nreturn int
	   DECLARE  @date    date
        DECLARE  @start_date    date
	 --******************************************************** 

		  IF @fordatapoint in(1,2,3,4,5,6,7)
		  begin
				set @start_date = (CONVERT (date, ( CONVERT(varchar,    DATEADD(day,-3, DATEADD(month,-3,@datetoday)  )  , 105) ),105) )
				IF @fordatapoint =1
				BEGIN			 			 
				set @date =    @start_date 
		
				END
				else IF @fordatapoint =2
				begin
				set @date =  DATEADD(day,1,@start_date )
				end

				else IF @fordatapoint =3
				begin
				Set @date= DATEADD(day,2,@start_date )
				end

				else IF @fordatapoint =4
				begin
					set @date= DATEADD(day,3,@start_date )
				end

				else IF @fordatapoint =5
				begin
				Set @date= DATEADD(day,4,@start_date )
				end

				else IF @fordatapoint =6
				begin
				set @date= DATEADD(day,5,@start_date )
				end

				else IF @fordatapoint =7
				begin
				Set @date= DATEADD(day,6,@start_date )
				end

				Set @nreturn= (Select top 1 total_dispense_amount from cash_dispense_month_minus_3 where 
							cast (datafor_date_time as date) = @date
							and atm_id=@atm_id 
							and bank=@bank 
							and project_id=@project
							and record_status='Active' )
		  end

		  --******************************************************

		ELSE IF @fordatapoint in(8,9,10,11,12,13,14)
		  begin
		  set @start_date = (CONVERT (date, ( CONVERT(varchar,    DATEADD(day,-3, DATEADD(month,-2,@datetoday)  )  , 105) ),105) )

				IF @fordatapoint =8
		  begin
				set @date = @start_date
		  end

		  else IF @fordatapoint =9
		  begin
				Set @date = DATEADD(day,1,@start_date )
		  end

		  else IF @fordatapoint =10
		  begin
				set @date = DATEADD(day,2,@start_date )
		  end
		  
		  else IF @fordatapoint =11
		  begin
				set @date = DATEADD(day,3,@start_date )
		  end
		  else IF @fordatapoint =12
		  begin
			   Set @date= DATEADD(day,4,@start_date )
		  end
		  else IF @fordatapoint =13
		  begin
			set @date = DATEADD(day,5,@start_date )
		  end
		  else IF @fordatapoint =14
		  begin
			set @date = DATEADD(day,6,@start_date )
		  end

				Set @nreturn= (Select top 1 total_dispense_amount from cash_dispense_month_minus_2 where 
						cast (datafor_date_time as date) = @date
						and atm_id=@atm_id 
						and bank=@bank 
						and project_id=@project
						and record_status='Active' )
		  end

		  --*******************************************************

		  ELSE IF @fordatapoint in(15,16,17,18,19,20,21)
		  begin
			set @start_date = (CONVERT (date, ( CONVERT(varchar,    DATEADD(day,-3, DATEADD(month,-1,@datetoday)  )  , 105) ),105) )
		    IF @fordatapoint =15
		  begin
				Set @date= @start_date		
		  end

		  else IF @fordatapoint =16
		  begin
				 Set @date = DATEADD(day,1,@start_date )
		  end
		  else IF @fordatapoint =17
		  begin
				Set @date = DATEADD(day,2,@start_date )
		  end
		  else IF @fordatapoint =18
		  begin
				Set @date = DATEADD(day,3,@start_date )
		  end
		   else IF @fordatapoint =19
		  begin
				Set @date = DATEADD(day,4,@start_date )
		  end
		   else IF @fordatapoint =20
		  begin
				Set @date = DATEADD(day,5,@start_date )
		  end
		   else IF @fordatapoint =21
		  begin
				Set @date = DATEADD(day,6,@start_date )
		  end

		  Set @nreturn= (Select top 1 total_dispense_amount from cash_dispense_month_minus_1 where 
						cast (datafor_date_time as date) = @date
						and atm_id=@atm_id 
						and bank=@bank 
						and project_id=@project
						and record_status='Active' )
		  end

     --- *******************************************************************************************

		  ELSE IF @fordatapoint IN(22,23,24,25,26,27,28)
		  BEGIN
		    IF @fordatapoint =22
		  begin
				Set @date= (CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-7,@datetoday)  )  , 105) ),105) )
		  end

		  else IF @fordatapoint =23
		  begin
				Set @date= (CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-6,@datetoday)  )  , 105) ),105) )
		  end

		  else IF @fordatapoint =24
		  begin
				Set @date= (CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-5,@datetoday)  )  , 105) ),105) )
		  end
		  else IF @fordatapoint =25
		  begin
				Set @date=  (CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-4,@datetoday)  )  , 105) ),105) )
		  end

		   else IF @fordatapoint =26
		  begin
				Set @date=  (CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-3,@datetoday)  )  , 105) ),105) )
		  end

		  else IF @fordatapoint =27
		  begin
				Set @date=  (CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-2,@datetoday)  )  , 105) ),105) )
		  end

		  else IF @fordatapoint =28
		  begin
				Set @date=   (CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-1,@datetoday)  )  , 105) ),105) )
		  end

		  
		  Set @nreturn= (Select top 1 total_dispense_amount from cash_dispense_month_minus_0 where 
						cast (datafor_date_time as date) = @date
						and atm_id=@atm_id 
						and bank=@bank 
						and project_id=@project
						and record_status='Active' )

		  END

	 --*******************************End of Function****************************************
		return @nreturn
 END

GO


