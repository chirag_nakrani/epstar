/****** Object:  StoredProcedure [dbo].[uspDataValidationCashPreAvailability]    Script Date: 02-04-2019 19:58:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


ALTER PROCEDURE [dbo].[uspDataValidationCashPreAvailability]
( 
	@api_flag    VARCHAR(50),
	@systemUser  VARCHAR(50),
	@referenceid VARCHAR(50),
	@outputVal   VARCHAR(50) OUTPUT
)
AS
BEGIN
	BEGIN TRY
	BEGIN TRAN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar       (max)
	DECLARE @bank_code nvarchar (max)
	DECLARE @project_id nvarchar(max)
	DECLARE @uploaded_record_status nvarchar(50) = 'Uploaded'
	DECLARE @tableName nvarchar(50) = 'cash_pre_availability'
	DECLARE @ToStatus nvarchar(50) = 'Active'
	DECLARE @SetWhereClause VARCHAR(MAX) ='is_valid_record = ''Yes'' AND  created_reference_id = '''+@referenceid+''''

	DECLARE @procedurename nvarchar(100) = (SELECT OBJECT_NAME(@@PROCID))

 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	 values ('Execution of [dbo].[uspDataValidationCashPreAvailability] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

	IF EXISTS (
		SELECT 1 as ColumnName
		from data_update_log_master WITH (NOLOCK)
		where record_status = @uploaded_record_status
		and created_reference_id = @referenceid
	)
	BEGIN
		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('data exists in data update log master', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		DECLARE @total_count_rows int = (SELECT count(1) from cash_pre_availability where created_reference_id = @referenceid and record_status = @uploaded_record_status)
		UPDATE cash_pre_availability
		SET error_code = isnull(case when available_50_amount < 0 then 'available_50_amount,' end, '')+
						 isnull(case when available_100_amount < 0 then 'available_100_amount,' end, '')+
						 isnull(case when available_200_amount < 0 then 'available_200_amount,' end, '')+
						 isnull(case when available_500_amount < 0 then 'available_500_amount,' end, '')+
						 isnull(case when available_2000_amount < 0 then 'available_2000_amount,' end, '')+
						 isnull(case when Applied_to_level not in ('feeder', 'FEEDER', 'Feeder') then 'applied_to_level_should_be_feeder_level,' end, '')+
						 isnull(case when total_amount_available <> isnull(available_50_amount,0)+isnull(available_100_amount,0)+isnull(available_200_amount,0)+isnull(available_500_amount,0)+isnull(available_2000_amount,0) then 'total_available_not_matching,' end, '')+
						 isnull(case when ISDATE(cast(from_date as nvarchar)) <> 1 then 'error_from_date,' end, '')+
						 isnull(case when ISDATE(cast(to_date as nvarchar)) <> 1 then 'error_to_date,' end, '')+
						 isnull(case when to_date < from_date then 'error_from_date_greater_than_to_date,' end, '')+
						 isnull(case when project_id  not in ( select DISTINCT cf.project_id from cra_feasibility cf where cf.record_status = 'Active' )
									or bank_code not in  (select DISTINCT cf.bank_code from cra_feasibility cf where cf.record_status = 'Active')	
									or feeder_branch_code not in (select DISTINCT cf.feeder_branch_code from cra_feasibility cf Where cf.record_status = 'Active' )
									THEN 'record_not_present,' end, '')
		where created_reference_id = @referenceid
		and record_status = @uploaded_record_status

		UPDATE cash_pre_availability
		set is_valid_record = case when error_code = '' then 'Yes' else 'No' end
		where created_reference_id = @referenceid
		and record_status = @uploaded_record_status

		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('update cash_pre_availability table completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		DECLARE @count_valid_records int = (select count(1) from cash_pre_availability where created_reference_id = @referenceid and record_status = @uploaded_record_status and is_valid_record = 'Yes')

		if (@total_count_rows = @count_valid_records)
			BEGIN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('total count rows matched with valid count rows.. updating data update log master', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						 
				update data_update_log_master set record_status = 'Approval Pending' where created_reference_id = @referenceid and record_status = @uploaded_record_status
				UPDATE data_update_log_master 
				SET pending_count = 
				(	SELECT COUNT(*) FROM cash_pre_availability 
		 			where record_status = @uploaded_record_status
					and created_reference_id = @referenceid 
				),
				total_count = 
				(
					SELECT COUNT(*) from cash_pre_availability
					where created_reference_id = @referenceid
					and record_status = @uploaded_record_status
				)
				where created_reference_id = @referenceid
				and record_status = 'Approval Pending'


			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		  values ('Update complete.. going for status update ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			EXEC  dbo.[uspSetStatus] @tableName,'Uploaded','Approval Pending',@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
				SET @outputVal = 'S101'
			END
		else if (@total_count_rows != @count_valid_records and @count_valid_records > 0)
			BEGIN
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('total count rows not matched with valid count rows.. updating data update log master', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				update data_update_log_master set record_status = 'Approval Pending' where created_reference_id = @referenceid and record_status = @uploaded_record_status
				UPDATE data_update_log_master 
				SET pending_count = 
				(	SELECT COUNT(*) FROM cash_pre_availability 
		 			where record_status = @uploaded_record_status
					and created_reference_id = @referenceid 
				),
				total_count = 
				(
					SELECT COUNT(*) from cash_pre_availability
					where created_reference_id = @referenceid
					and record_status = @uploaded_record_status
				)
				where created_reference_id = @referenceid
				and record_status = 'Approval Pending'
				and created_reference_id = @referenceid
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		 values ('Update complete.. going for status update ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				EXEC  dbo.[uspSetStatus] @tableName,'Uploaded','Approval Pending',@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
				SET @outputVal = 'S101' 
			END
		else 
			BEGIN
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
								)
			END

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
		  values ('execution of [uspDataValidationFeederVaultingPreConfig] completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


	END--37

	ELSE
		BEGIN
			RAISERROR(90002,16,1)
		END
	--DECLARE @CountTotal  int 
	--DECLARE @countCalculated  int
	COMMIT;
END TRY
BEGIN CATCH
IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedurename
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SELECT @ErrorNumber;				
			 END CATCH
	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	values ('execution of [uspDataValidationFeederVaultingPreConfig] completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
END