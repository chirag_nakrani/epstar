INSERT INTO MetaDataTableMaster (file_type,columnHeaders,IsRequired,table_column,display_column_name,table_name) VALUES ('EOD_ACTIVITY','atm_id','True','atm_id','ATM ID','indent_eod_pre_activity')
INSERT INTO MetaDataTableMaster (file_type,columnHeaders,IsRequired,table_column,display_column_name,table_name) VALUES ('EOD_ACTIVITY','site_code','True','site_code','Site Code','indent_eod_pre_activity')
INSERT INTO MetaDataTableMaster (file_type,columnHeaders,IsRequired,table_column,display_column_name,table_name) VALUES ('EOD_ACTIVITY','project','True','project_id','Project','indent_eod_pre_activity')
INSERT INTO MetaDataTableMaster (file_type,columnHeaders,IsRequired,table_column,display_column_name,table_name) VALUES ('EOD_ACTIVITY','bank_code','True','bank_code','Bank Code','indent_eod_pre_activity')
INSERT INTO MetaDataTableMaster (file_type,columnHeaders,IsRequired,table_column,display_column_name,table_name) VALUES ('EOD_ACTIVITY','region','False','region_code','Region','indent_eod_pre_activity')
INSERT INTO MetaDataTableMaster (file_type,columnHeaders,IsRequired,table_column,display_column_name,table_name) VALUES ('EOD_ACTIVITY','feeder_branch','True','branch_code','Branch Code','indent_eod_pre_activity')
INSERT INTO MetaDataTableMaster (file_type,columnHeaders,IsRequired,table_column,display_column_name,table_name) VALUES ('EOD_ACTIVITY','for_date','True','datafor_date_time','For Date','indent_eod_pre_activity')

