GO
/****** Object:  Table [dbo].[feeder_branch_master]    Script Date: 14-12-2018 19:33:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[feeder_branch_master_tmp](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[bank_code] [nvarchar](10) NULL,
	[project_id] [nvarchar](50) NULL,
	[region_code] [nvarchar](50) NULL,
	[sol_id] [nvarchar](50) NULL,
	[feeder_branch] [nvarchar](50) NULL,
	[district] [nvarchar](50) NULL,
	[circle] [nvarchar](50) NULL,
	[feeder_linked_count] [int] NULL,
	[contact_details] [nvarchar](50) NULL,
	[is_vaulting_enabled] [nvarchar](20) NULL,
	[email_id] [nvarchar](max) NULL,
	[alternate_cash_balance] [nvarchar](30) NULL,
	[is_currency_chest] [nvarchar](30) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL,
)
GO


SET IDENTITY_INSERT dbo.feeder_branch_master_tmp  ON


INSERT INTO dbo.feeder_branch_master_tmp (
	id,
	bank_code,
	project_id,
	sol_id,
	feeder_branch,
	contact_details,
	is_vaulting_enabled,
	record_status,
	created_on,
	created_by,
	created_reference_id,
	approved_on,
	approved_by,
	approved_reference_id,
	approve_reject_comment,
	rejected_on,
	rejected_by,
	reject_reference_id,
	modified_on,
	modified_by,
	modified_reference_id,
	is_valid_record,
	error_code)

SELECT 

	id,
	bank_code,
	project_id,
	sol_id,
	feeder_branch,
	contact_details,
	is_vaulting_enabled,
	record_status,
	created_on,
	created_by,
	created_reference_id,
	approved_on,
	approved_by,
	approved_reference_id,
	approve_reject_comment,
	rejected_on,
	rejected_by,
	reject_reference_id,
	modified_on,
	modified_by,
	modified_reference_id,
	is_valid_record,
	error_code
	FROM feeder_branch_master;


SET IDENTITY_INSERT dbo.feeder_branch_master_tmp  OFF

EXEC sp_rename 'feeder_branch_master', 'feeder_branch_master_tmp_bckp_18122018_1431'

EXEC sp_rename 'feeder_branch_master_tmp', 'feeder_branch_master'

drop table feeder_branch_master_tmp_bckp_18122018_1431;


