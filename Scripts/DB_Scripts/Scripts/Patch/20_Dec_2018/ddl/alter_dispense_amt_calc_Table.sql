
/****** Object:  Table [dbo].[Dispense_Amount_calc]    Script Date: 04-12-2018 16:09:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
DROP TABLE [dispense_amount_calc]

CREATE TABLE [dbo].[dispense_amount_calc](
	[id] BIGINT IDENTITY(1,1) PRIMARY KEY,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[site_code] [nvarchar](50) NULL,
	[datafor_date] [date] NULL,
	[morning_t_minus_1_bal_50] [bigint] NULL,
	[morning_t_minus_1_bal_100] [bigint] NULL,
	[morning_t_minus_1_bal_200] [bigint] NULL,
	[morning_t_minus_1_bal_500] [bigint] NULL,
	[morning_t_minus_1_bal_2000] [bigint] NULL,
	[morning_total_balance_t_minus_1] [bigint] NULL,
	[morning_t_minus_2_bal_50] [bigint] NULL,
	[morning_t_minus_2_bal_100] [bigint] NULL,
	[morning_t_minus_2_bal_200] [bigint] NULL,
	[morning_t_minus_2_bal_500] [bigint] NULL,
	[morning_t_minus_2_bal_2000] [bigint] NULL,
	[morning_total_balance_t_minus_2] [bigint] NULL,
	[loading_t_minus_2_amt_100] [bigint] NULL,
	[loading_t_minus_2_amt_200] [bigint] NULL,
	[loading_t_minus_2_amt_500] [bigint] NULL,
	[loading_t_minus_2_amt_2000] [bigint] NULL,
	[loading_total_amount_t_minus_2] [bigint] NULL,
	[dispense_amount] [bigint] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL
	)

