/****** Object:  StoredProcedure [dbo].[uspValidateMasterFiles]    Script Date: 19-12-2018 12:17:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspValidateMasterFiles] 
@file_type varchar(50) ,@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
			DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @Result varchar(50)			
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE());
			DECLARE @out1 varchar(100);

			
			-- Creating Dynamic query to execute respective procedure according to the file type

			SET @Cmd = CASE 
							WHEN @file_type = 'ATMCNFGLIMIT' 
							THEN 'EXEC [uspDataValidationAtmcnfiglimits] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'FEEDER' 
							THEN 'EXEC [uspDataValidationFeederMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'AMS_ATM' 
							THEN 'EXEC [uspDataValidationAMSAtmMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'CRAFEASIBILITY' 
							THEN 'EXEC [uspDataValidationCRAFeasibility] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							
							END
			-- Executing the dynamic query and storing the output of query in @Result Param

			EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 

			-- If @Result returns null Then Check error log table order by id desc

			IF(@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence from app_config_param where category = 'CBR_Validation' and sub_category = 'Error')
			END

		--  In case of success , update the record status in data_update_log_master from uploaded to approval Pending 
		            
            IF(@Result = 'S101')
				BEGIN	
					DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log_master'
					DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
					DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
					DECLARE @SetWhereClause VARCHAR(MAX) =' data_for_type = ''' + 
													@file_type+''' and created_reference_id = '''+@referenceid+''''

					EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
	
					SELECT @Result;	
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
 		
		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						DECLARE @code varchar(100)
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						IF @ErrorNumber = 90001
						BEGIN
							SET @code = 'E101'
						END
						ELSE IF @ErrorNumber = 90002
						BEGIN
							SET @code = 'E102'
						END
						SELECT @code
						
				END
			 END CATCH
END
