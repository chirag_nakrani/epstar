 
/****** Object:  StoredProcedure [dbo].[usp_Dispense_Calculator_12_banks]    Script Date: 17-12-2018 15:29:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- =========================================================================
 --- Created By :Rohit Saini
 --- Created Date: 20-12-2018
 --- Description: Calculation of  dispense for the banks which do not provide dispense amount directly.
  --              calculating Dispense amount with morning balance of T-1,T-2 and C3R balance.
--				  Discarding old procedure (usp_Dispense_Calculator_12_banks) and creating this 
 --- =========================================================================


-- EXEC [usp_Dispense_Calculator_12_banks] '2018-08-04','MOF','DENA','abc123' 
DROP PROCEDURE [dbo].[usp_Dispense_Calculator_12_banks]

CREATE PROCEDURE [dbo].[usp_calculate_dispense]
	-- Add the parameters for the stored procedure here
    (
        @date_T date,
        @project_id nvarchar(50),
        @bank_code nvarchar(50),
        @created_reference_id nvarchar(50),
        @cur_user nvarchar(50)
    )
AS

BEGIN
	DECLARE @Execution_log_str nvarchar(max)
		BEGIN TRY
			BEGIN TRANSACTION
				--Define variables to be used for procedure.
				DECLARE @Cmd NVARCHAR(MAX);
				DECLARE @Result varchar(100);
				DECLARE @out varchar(100);
				DECLARE @successVal varchar(30);
				DECLARE @failedVal varchar(30);
				--DECLARE @timestamp_date datetime =GETUTCDATE();
				DECLARE @Dispense_Calculation_Process_table_type varchar(50)
				set @Dispense_Calculation_Process_table_type ='Dispense_Calculation_Process'
				DECLARE @datafor_type_dispense varchar(50)
				set @datafor_type_dispense = 'Calculated_Dispense'
				DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
				    
				--Add execution log indicating procedure started.
				
				
				declare @ForStatus_active nvarchar(50)
				declare @ToStatus_deleted nvarchar(50)

				set @ForStatus_active = 'Active'
				set @ToStatus_deleted = 'Deleted'
				
      
				set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(dateadd(dd,-2,@date_T) as nvarchar(50)) +' , Project = ' + @project_id + ' , Bank = ' + @bank_code;

				
				IF EXISTS   (						
								SELECT 1
								FROM data_update_log
								WHERE 
				                    datafor_date_time = dateadd(dd,-2,@date_T) 
				                    AND 
				                    bank_code = @bank_code 
				                    AND 
				                    data_for_type = @Dispense_Calculation_Process_table_type 
				                    and 
				                    record_status = 'Active'
				                    and
				                    project_id = @project_id   --chg
						)	
				BEGIN
    				 
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old data update log entries for ' + @Dispense_Calculation_Process_table_type

				    DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + @bank_code     + 
				                                                ''' and datafor_date_time = ''' + cast(dateadd(dd,-2,@date_T) as nvarchar(50))  + 
				                                                ''' and data_for_type = ''' + @Dispense_Calculation_Process_table_type +
																 ''' and project_id = ''' + @project_id + ''''
				
				    EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus_active,@ToStatus_deleted,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_DataUpdateLog,@out OUTPUT
				
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for ' + @Dispense_Calculation_Process_table_type

				END
   
				IF EXISTS   (						
								SELECT 1
								FROM dispense_amount_calc
								WHERE 
				                    datafor_date = dateadd(dd,-2,@date_T) 
				                    AND 
				                    bank_code = @bank_code                             
				                    and 
				                    record_status = 'Active'
				                    and
				                    project_id = @project_id 
						)	
				BEGIN
					
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old records in dispense_amount_cal entries. ' 

				    DECLARE @SetWhere_dispense_amount_calc VARCHAR(MAX) =  ' bank_code = ''' + @bank_code     + 
				                                                ''' and datafor_date = ''' + cast(dateadd(dd,-2,@date_T) as nvarchar(50))  +                                                         
																 ''' and project_id = ''' + @project_id + ''''
				
				    EXEC dbo.[uspSetStatus] 'dbo.dispense_amount_calc',@ForStatus_active,@ToStatus_deleted,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_dispense_amount_calc,@out OUTPUT

					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in dispense_amount_calc table for old entries. '

				END
   
				/***********-------Inserting values in Dispense calculation table--------------******************/

				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) +convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Insert into Dispense_Amount_calc started. '

				Insert into dbo.Dispense_Amount_calc 
				(
				    [project_id],
				    [bank_code],
				    [atm_id],
				    site_code,
				    datafor_date, 
				    [morning_t_minus_1_bal_50],
				    [morning_t_minus_1_bal_100],
				    [morning_t_minus_1_bal_200],
				    [morning_t_minus_1_bal_500],
				    [morning_t_minus_1_bal_2000],
				    morning_total_balance_t_minus_1,
				    [morning_t_minus_2_bal_50],
				    [morning_t_minus_2_bal_100],
				    [morning_t_minus_2_bal_200],
				    [morning_t_minus_2_bal_500],
				    [morning_t_minus_2_bal_2000],
				    morning_total_balance_t_minus_2,
				    [loading_t_minus_2_amt_100],
				    [loading_t_minus_2_amt_200],
				    [loading_t_minus_2_amt_500],
				    [loading_t_minus_2_amt_2000],
				    loading_total_amount_t_minus_2,
				    dispense_amount,
				    [record_status],
				    [created_on],
				    created_by,
				    [created_reference_id]
				)

	 
				select 
				    @project_id   as  project_id,
				    @bank_code as bank_name,
				     tm1.atm_id,
				    am.site_code,
				    dateadd(dd,-2,@date_T) as datafor_date,
				    tm1.remaining_balance_50 as [morning_t_minus_1_bal_50],
				    tm1.remaining_balance_100 as [morning_t_minus_1_bal_100],
				    tm1.remaining_balance_200 as [morning_t_minus_1_bal_200],
				    tm1.remaining_balance_500 as [morning_t_minus_1_bal_500],
				    tm1.remaining_balance_2000 as [morning_t_minus_1_bal_2000],
				    tm1.morning_balance_t_minus_1,
				    tm2.remaining_balance_50 as [morning_t_minus_2_bal_50],
				    tm2.remaining_balance_100 as [morning_t_minus_2_bal_100],
				    tm2.remaining_balance_200 as [morning_t_minus_2_bal_200],
				    tm2.remaining_balance_500 as [morning_t_minus_2_bal_500],
				    tm2.remaining_balance_2000 as [morning_t_minus_2_bal_2000],
				    tm2.morning_balance_t_minus_1 as morning_balance_t_minus_2,
				    CMIS.sw_loading_100 as [loading_t_minus_2_amt_100],
				    CMIS.sw_loading_200 as [loading_t_minus_2_amt_200],
				    CMIS.sw_loading_500 as [loading_t_minus_2_amt_500],
				    CMIS.sw_loading_2000 as [loading_t_minus_2_amt_2000],
				    CMIS.[sw_loading_total] as loading_amount_t_minus_2,
				    (tm2.morning_balance_t_minus_1 + CMIS.[sw_loading_total] - tm1.morning_balance_t_minus_1) as dispense_amount,
				    'Active',
				    @timestamp_date,
				    @cur_user,
				    @created_reference_id as created_reference_id	
				FROM atm_master am	
				JOIN 	morningbalance tm1   -- Morning balance for date(T-1)
				on 
				    am.atm_id=tm1.atm_id	        and 
				    am.project_id=tm1.project_id    and
				    am.bank_code=tm1.bank_name      and	  
				    cast(tm1.datafor_date_time as date)= cast( dateadd(dd,-1,@date_T) as date)
				JOIN morningbalance tm2   -- Morning balance for date(T-2)
				on 
				    am.atm_id=tm2.atm_id	    and 
				    am.bank_code=tm2.bank_name	and 
				    cast(tm2.datafor_date_time as date)=dateadd(dd,-2,@date_T)
				JOIN       [c3r_CMIS] CMIS
				on 
				am.atm_id=CMIS.atm_id and -- C3R_CMIS for loading amount
				am.bank_code=CMIS.bank and
				cast(CMIS.date as date)= dateadd(dd,-2,@date_T) and
				CMIS.record_status ='Active' and  CMIS.is_valid_record ='yes'
				where 
				am.project_id=@project_id
				and 
				am.bank_code=@bank_code
				and		
				tm2.morning_balance_t_minus_1 is not null
				and  
				CMIS.[sw_loading_total] is not null
				and  
				tm1.morning_balance_t_minus_1 is not null

				
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ':Insert into Dispense_Amount_calc Completed. ' 

				-----------****************** data_update_log entry no of records**********************---
				
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ':Insert into Data_update_log Started. '

				DECLARE @record_count INT			 
				SET @record_count=@@ROWCOUNT

				INSERT into data_update_log 
					(bank_code,
					project_id,
					operation_type,
					datafor_date_time,
					data_for_type,			 
					status_info_json,			 
					record_status,
					date_created,
					created_reference_id)

				values
					(
				        @bank_code,
				        @project_id,
				        @Dispense_Calculation_Process_table_type,
				        dateadd(dd,-2,@date_T),
				        @Dispense_Calculation_Process_table_type,			 
				        'Data inserted for dispense calculation 12 banks for  '+@bank_code+ ' for date '+cast(dateadd(dd,-2,@date_T) as nvarchar(50)) +': and no. of rows inserted : '+ cast(@record_count as nvarchar(50)),
				        'Active',
				        @timestamp_date,
				        @created_reference_id			
					)

				--************************************Logging*******************************************************
 
			SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) +  convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Insert into Data_update_log Completed with record count '  + cast(@record_count as nvarchar(50))
				
				/*    Inserting into CDR table from the Dispense_Amount_calc table with details
				      of dispense amount of other 12 banks
				      Checking the status of the inserted records and changing accordingly
				*/        
				IF EXISTS   (						
				                    SELECT 1
				                    FROM data_update_log
				                    WHERE datafor_date_time = dateadd(dd,-2,@date_T) AND 
				                     bank_code = @bank_code AND 
				                    data_for_type = @datafor_type_dispense and 
				                    record_status = 'Active'  	  and
				                    project_id = @project_id          --chg
				            )	
				BEGIN				--- Check Active status in DUL        
				    

				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Old record found in data_update_log table for ' + @datafor_type_dispense
			
				    -- update status of Data Update Log  for that bank /date 
				    DECLARE @SetWhere_DataUpdateLog1 VARCHAR(MAX) =  ' bank_code = ''' + 
				                @bank_code     + ''' and datafor_date_time = ''' + 
				                cast(dateadd(dd,-2,@date_T) as nvarchar(50))      + ''' and data_for_type = ''' + 
				                @datafor_type_dispense  + ''' and project_id = ''' + @project_id + ''''

				    EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus_active,@ToStatus_deleted,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_DataUpdateLog1,@out OUTPUT
				  
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) +  convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for ' + @datafor_type_dispense                          
				
				END
				        
				IF EXISTS(
				            Select 1 as ColumnName
				            FROM [cash_dispense_register]
				            WHERE
				                  datafor_date_time =dateadd(dd,-2,@date_T)AND 
				                  bank_name = @bank_code     AND 
				                  record_status = 'Active' 	 AND
				                  project_id = @project_id          --chg
				            )
				BEGIN	
				    -- update status in  consolidated dispense table  cash_dispense_resgister

					SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Old record found in cash_dispense_register. '
				   
				    DECLARE @SetWhere_CDR VARCHAR(MAX) = ' bank_name = ''' + 
				                @bank_code +  ''' and datafor_date_time = ''' + 
				                cast(dateadd(dd,-2,@date_T) as nvarchar(50))       +  ''' and project_id = ''' + @project_id + ''''

				    EXEC dbo.[uspSetStatus] 'cash_dispense_register', @ForStatus_active, @ToStatus_deleted, @timestamp_date,@cur_user,@created_reference_id,@SetWhere_CDR,@out OUTPUT        
				
					SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Status updated in cash_dispense_register. '
				END
				
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into cash_dispense_register started.  '
				
				Insert into [dbo].[cash_dispense_register]
				(
				    [atm_id],
				    [bank_name],
				    [total_dispense_amount],
				    [project_id],
				    [datafor_date_time],
				    created_on,
				    created_by,
				    created_reference_id,         
				    record_status,
				    dispense_type
				)

				select 
				    [atm_id],
				    [bank_code],
				    [dispense_amount],
				    [project_id],
				    datafor_date,
				    created_on,
				    created_by,
				    created_reference_id,
				    record_status,
				    'Calculated'
				 from dbo.Dispense_Amount_calc 			
				 where record_status='Active' and datafor_date = dateadd(dd,-2,@date_T) and project_id = @project_id and bank_code = @bank_code and created_reference_id = @created_reference_id
				 

				
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into cash_dispense_register Completed.  '
			
				SET @record_count=@@ROWCOUNT
				 
				SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into data_update_log started.  '

				INSERT into data_update_log 
				    (bank_code,
				    project_id,
				    operation_type,
				    datafor_date_time,
				    data_for_type,			 
				    status_info_json,			 
				    record_status,
				    date_created,
				    created_reference_id)

				values
				    (@bank_code,
				    @project_id,
				    @datafor_type_dispense,
				    dateadd(dd,-2,@date_T),
				    @datafor_type_dispense,			 
				    'Data inserted for dispense calculation 12 banks for  '+@bank_code+ ' for date '+cast(dateadd(dd,-2,@date_T) as nvarchar(50)) +' and no. of rows inserted : '+cast(@record_count as nvarchar(50)),
				    'Active',
				    @timestamp_date,
				    @created_reference_id			
				    )

			SET @Execution_log_str = @Execution_log_str +CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into data_update_log completed wit record count =   ' + cast(@record_count as nvarchar(50))
			
			
			COMMIT transaction;
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			 values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		     
    END TRY

BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						--SELECT @ErrorNumber;	
             INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			 values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		                      
			 END CATCH
END

 