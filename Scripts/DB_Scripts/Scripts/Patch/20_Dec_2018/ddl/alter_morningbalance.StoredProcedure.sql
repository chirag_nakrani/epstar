 
/****** Object:  StoredProcedure [dbo].[usp_morningBalance]    Script Date: 20-12-2018 11:58:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 17-12-2018
 --- Description: Calculation of MorningBalance for all banks
 --- =========================================================================

 /*****************************Inserts morning balance in a table from CBR ******************/
 --  Select  datafor_date_time ,count(*) from morningbalance  
 --  group by datafor_date_time
 --  Select * from morningbalance
 --  EXEC [usp_morningBalance] '2018-08-02','MOF','DENA','abc123','sa'
 /*****************************************************************************************/

ALTER procedure [dbo].[usp_morningBalance] 

	 @date_T      datetime,
	 @project_id  nvarchar(50),
	 @bank_code   nvarchar(50),
	 @created_reference_id nvarchar(50),
	 @cur_user nvarchar(50)


AS
BEGIN
	declare @procedure_name varchar(max) 
	set @procedure_name =OBJECT_NAME(@@PROCID)
	BEGIN TRY
		--Declare @date_T date
		--SET @date_T='2018-08-02'
		--declare @project_id  nvarchar(50),
		--@bank_code   nvarchar(50),
		--@created_reference_id nvarchar(50),
		--@cur_user nvarchar(50)
		--set @project_id ='MOF'
		--set @bank_code = 'DENA'
		--set @created_reference_id = '12dsfd'
		--set @cur_user = 'sa'
		
		DECLARE @out varchar(100);
		declare @ForStatus_active nvarchar(50)
		declare @ToStatus_deleted nvarchar(50)
		DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);

		set @ForStatus_active = 'Active'
		set @ToStatus_deleted = 'Deleted'
		DECLARE @operation_type  varchar(40)
		SET @operation_type='morningbalance'
		declare @Execution_log_str nvarchar(max)
		set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@date_T as nvarchar(50)) +' , Project = ' + @project_id + ' , Bank = ' + @bank_code;

		--declare @date_T date
		--SET @date_T='2018-10-22'
		--***************************************Logging**********************************************

		--INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
		--values ('Execution of [dbo].[usp_morningBalance] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
		
		--select top 100 * from cash_balance_register where datafor_date_time > '2018-08-01 15:38:00.000' order by datafor_date_time
			
			/******* testing paramaters*********/
			-- Declare  
			-- @project_id nvarchar(50),
			-- @bank_code nvarchar(50),
			-- @created_reference_id nvarchar(50),
			-- @date_T date

			-- SET  @bank_code ='DENA'
			--SET @created_reference_id ='abc'
			--SET @project_id ='MOF'
			--SET @date_T='2018-08-03'
			--DECLARE @MaxDestinationDate date

			--Declare @date_T date
			--SET @date_T='2018-10-22'
			--DECLARE @out varchar(100);
			--declare @ForStatus_active nvarchar(50)
			--declare @ToStatus_deleted nvarchar(50)
			--DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);

			--set @ForStatus_active = 'Active'
			--set @ToStatus_deleted = 'Deleted'
		    -- DECLARE @operation_type  varchar(40)
			--SET @operation_type='morningbalance'
			--declare @Execution_log_str nvarchar(max)
			--set @Execution_log_str = @timestamp_date+ '<Date_Time>: Procedure Started with parameters-'

		    /******* END testing paramaters*********/
		
			--**********************************Actual Logic**********************************************
			--log for max cbr data collect

			--INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			--values ('Capturing MaxDestinationDate', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			-- Getting the latest date time for morning balance calculation			
			DECLARE @MaxDestinationDate datetime
			Set @MaxDestinationDate=(Select max(datafor_date_time)
									from cash_balance_register where bank_name=@bank_code and project_id=@project_id and datafor_date_time
									between  
									case when bank_name IN('VJB','PSB') then cast(concat(substring(convert (varchar(20),dateadd(dd,-1,@date_T) ,120),1,10),' 16:00:00.000') as datetime )
																		else cast(concat(substring(convert (varchar(20),dateadd(dd,-1,@date_T) ,120),1,10),' 20:00:00.000') as datetime ) end
									and case when bank_name IN('VJB','PSB') then cast(concat(substring(convert (varchar(20),dateadd(dd,-1,@date_T) ,120),1,10),' 21:00:00.000') as datetime )
																			else cast(concat(substring(convert (varchar(20),@date_T ,120),1,10),' 00:00:00.000') as datetime ) end
			and record_status ='Active' 	 ) 

			SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Latest datetime to get the morning balance : ' + CAST(@MaxDestinationDate as nvarchar(50))
			
			----Get morning balance from Cash balance register in the table morningbalance for the indentdate-----
			
			-- If @MaxDestinationDate returns null means no CBR file available for that date
			--select @MaxDestinationDate
			IF @MaxDestinationDate is NULL
				BEGIN
				
					--INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				 --   values ('No CBR availabale', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				    
					BEGIN transaction
						
						-- Entry in  DUL table with status_info

						INSERT into data_update_log 
									(
									bank_code,
									project_id,
									operation_type,
									datafor_date_time,
									data_for_type,			 
									status_info_json,			 
									record_status,
									date_created,
									created_reference_id
									)
							values
									(	
									@bank_code,
									@project_id,
									@operation_type,
									@date_T,
									@operation_type,			 
									'No CBR data vailable for morningbalance for '+@bank_code+ ' for date '+cast(@date_T as varchar(30)),
									NULL,
									DATEADD(MI,330,GETUTCDATE()),
									@created_reference_id
									)
					
					COMMIT transaction
					 
				END
			ELSE 
				BEGIN
					-- Latest CBR file found in cash balance register 

					BEGIN transaction

						-- Check if previous entry present in DUL for same datetime, bankcode and projectid

						IF EXISTS   (						
									SELECT 1
									FROM data_update_log
									WHERE 
						                datafor_date_time = @date_T 
						                AND 
						                bank_code = @bank_code 
						                AND 
						                data_for_type = @operation_type 
						                and 
						                record_status = 'Active'
						                and
						                project_id = @project_id   --chg
									)	
								BEGIN
									
									-- Active records found. Update status from active to deleted

									set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old data update log entries for ' + @operation_type
									
									DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + @bank_code     + 
									                                            ''' and datafor_date_time = ''' + CAST(@date_T  as varchar(20))+ 
									                                            ''' and data_for_type = ''' + @operation_type +
																				 ''' and project_id = ''' + @project_id + ''''

									EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus_active,@ToStatus_deleted,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_DataUpdateLog,@out OUTPUT
									
									SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for ' + @operation_type

								END

						-- Check if previous entry present in morning balance table for same datetime, bankcode and projectid

						IF EXISTS   (						
								SELECT 1
								FROM morningbalance
								WHERE 
								    datafor_date_time = @date_T
								    AND 
								    bank_name = @bank_code                             
								    and 
								    record_status = 'Active'
								    and
								    project_id = @project_id 
								)	
								 BEGIN
									
									-- Active records found. Update status from active to deleted

									SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in morning balance.'
									
								    DECLARE @SetWhere_morningbalance VARCHAR(MAX) =  ' bank_name = ''' + @bank_code     + 
								                                                 ''' and datafor_date_time = ''' + CAST(@date_T as nvarchar(50))  +                                                         
																				 ''' and project_id = ''' + @project_id + ''''
        
									EXEC dbo.[uspSetStatus] 'dbo.morningbalance',@ForStatus_active,@ToStatus_deleted,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_morningbalance,@out OUTPUT
									
									SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated successfully for old entries in morning balance.'
								 END

								--Add execution log for max cbr record found and inserting data to morning balance

								--INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
								--values (' CBR available', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
								  
								  
								--INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
								--values ('Inserting values into morningbalance table', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
								 
								-- Insert new records into morning balance table with record status as active
								 SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Inserting new records in morning balance table .' 

								INSERT into 
								morningbalance (  
												atm_id,
												bank_name,
												project_id,
												record_status,
												created_on,
												datafor_date_time,
												created_reference_id,
												remaining_balance_50,
												remaining_balance_100,
												remaining_balance_200,
												remaining_balance_500,
												remaining_balance_2000,
												Morning_balance_T_minus_1
											  )
								SELECT		atm_id,
											bank_name, 
											project_id,
											'Active',
											DATEADD(MI,330,GETUTCDATE()),
											@date_T,
											@created_reference_id,	 
											remaining_balance_50,
											remaining_balance_100,
											remaining_balance_200,
											remaining_balance_500,
											remaining_balance_2000,
											([total_remaining_balance]) as Morning_balance_T_minus_1
									from  [dbo].[cash_balance_register]
									where 
									project_id=@project_id
									and bank_name=@bank_code
									and record_status='Active'
									and datafor_date_time =@MaxDestinationDate
									
									DECLARE @row INT			 
									SET @row=@@ROWCOUNT

									SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Data successfully inserted in morning balance table .' 

									-- entry in data update log for cbr data inserted in morning balance
									
									

									INSERT into data_update_log 
												(
												bank_code,
												project_id,
												operation_type,
												datafor_date_time,
												data_for_type,			 
												status_info_json,			 
												record_status,
												date_created,
												created_reference_id
												)
											values
												(
												@bank_code,
												@project_id,
												'morningBalance',
												@date_T,
												'morningBalance',			 
												'Data inserted in morningbalance table for  '+ @bank_code + ' for date '+cast(@date_T as varchar(30)) +' and no. of rows inserted : '+ cast(@row as varchar(10)),
												'Active',
												 DATEADD(MI,330,GETUTCDATE()),
												 @created_reference_id
												)

												SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Data log successfully updated in DUL with no of rows = ' + cast(@row as varchar(10))
			
					COMMIT transaction
				END
	
	--************************************Logging*******************************************************	  

	
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name;
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						
						INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
						values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))			
			 
			END CATCH

	end
--***********************************End of the Procedure***********************************************