drop table Auth_Signatories_Signatures

/****** Object:  Table [dbo].[Auth_Signatories_Signatures]    Script Date: 09-01-2019 12:37:36 ******/

CREATE TABLE [dbo].[Auth_Signatories_Signatures](
	[Id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[signatory_name] [nvarchar](50) NULL,
	[designation] [nvarchar](50) NULL,
	[physical_signature] [image] NULL,
	[digital_signature] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)	
