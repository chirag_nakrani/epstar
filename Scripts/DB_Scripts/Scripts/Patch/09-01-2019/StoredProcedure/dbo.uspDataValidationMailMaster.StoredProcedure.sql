/****** Object:  StoredProcedure [dbo].[uspDataValidationMailMaster]    Script Date: 09-01-2019 12:00:04 PM ******/
/****** FUNCTIONALITY - This procedure takes two cases 
						1. Update through File Upload 
						2. Update through Screen Edit	
						In case of file upload procedure checks for the previously active entry and update the column  
						value from previously active record to the new one if it found the column value for the new 
						record to be null.
						If column value is not null, it will retain the original value for the new record
******/

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspDataValidationMailMaster]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @bank_code nvarchar(max)
	DECLARE @cra nvarchar(max)
	DECLARE @feeder_branch nvarchar(max)

	-- Checking for any record that is present in uploaded status in table

	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].Mail_Master
				WHERE record_status = 'Uploaded'
				and created_reference_id = @referenceid
			 )
				
			BEGIN

		-- Proceed if we found any record in uploaded status
		
			DECLARE @tableName VARCHAR(30) = 'dbo.Mail_Master'
			DECLARE @ForStatus VARCHAR(15) = 'Uploaded'
			DECLARE @ToStatus VARCHAR(30) = 'Approval Pending'
			DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''

				-- Check if there are records present in active status in table

				IF EXISTS(SELECT 1 FROM dbo.Mail_Master WHERE record_status = 'Active')
					BEGIN

					-- Check for API Flag if it is for File Upload or screen edit
					-- 'F' - File Upload ,  else - Screen Edit

					IF (@api_flag = 'F')
						BEGIN
								
							 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in Mail_Master started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


					 UPDATE a
                            set  
									a.project_id=COALESCE(a.project_id,b.project_id),
									a.bank_code=COALESCE(a.bank_code,b.bank_code),
									a.feeder_branch=COALESCE(a.feeder_branch,b.feeder_branch),
									a.sol_id=COALESCE(a.sol_id,b.sol_id),
									a.cra=COALESCE(a.cra,b.cra),
									a.to_bank_email_id=COALESCE(a.to_bank_email_id,b.to_bank_email_id),
									a.to_CRA_email_id=COALESCE(a.to_CRA_email_id,b.to_CRA_email_id),
									a.bcc_email_id=COALESCE(a.bcc_email_id,b.bcc_email_id),
									a.subject=COALESCE(a.subject,b.subject),
									a.body_text=COALESCE(a.body_text,b.body_text),
									a.state=COALESCE(a.state,b.state),
									a.importance=COALESCE(a.importance,b.importance),
									a.atm_count=COALESCE(a.atm_count,b.atm_count),
									a.status=COALESCE(a.status,b.status),
									 
									-- a.created_on=COALESCE(a.created_on,b.created_on),
									-- a.created_by=COALESCE(a.created_by,b.created_by),
									-- a.created_reference_id=COALESCE(a.created_reference_id,b.created_reference_id),
									-- a.approved_on=COALESCE(a.approved_on,b.approved_on),
									-- a.approved_by=COALESCE(a.approved_by,b.approved_by),
									-- a.approved_reference_id=COALESCE(a.approved_reference_id,b.approved_reference_id),
									-- a.approve_reject_comment=COALESCE(a.approve_reject_comment,b.approve_reject_comment),
									-- a.rejected_on=COALESCE(a.rejected_on,b.rejected_on),
									-- a.rejected_by=COALESCE(a.rejected_by,b.rejected_by),
									-- a.rejected_reference_id=COALESCE(a.rejected_reference_id,b.rejected_reference_id),
									 
									-- a.isValidRecord=COALESCE(a.isValidRecord,b.isValidRecord),
									-- a.error_code=COALESCE(a.error_code,b.error_code),



							a.record_status = 'Approval Pending',
							a.modified_on = @current_datetime_stmp,
							a.modified_by = @systemUser,
							a.modified_reference_id = @referenceid
							from        Mail_Master a 
							inner join  Mail_Master b 
							on
							a.bank_code = b.bank_code
							and a.feeder_branch = b.feeder_branch 
							and a.project_id=b.project_id
							and a.record_status = 'Uploaded' 
							and b.record_status = 'Active'


					     INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in Mail_Master completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

						 
							END
					ELSE
						BEGIN
							-- Screen Edit Scenario --
							-- For screen edit we have to directly update the record status from uploaded to approval pending

							EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
						END
					END

					-- If there are no records in active status then directly update record status to approval pending 

					ELSE 
						BEGIN
								EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
							END
							
							UPDATE data_update_log_master 
							SET pending_count =	
									(
										SELECT COUNT(*) FROM dbo.Mail_Master WHERE created_reference_id = @referenceid
										and record_status = 'Approval Pending'
									),
								total_count = 
								(
									SELECT COUNT(*) FROM dbo.Mail_Master WHERE created_reference_id = @referenceid
										
								)
							WHERE record_status = 'Uploaded'
							AND data_for_type = 'MAILMASTER' 
							AND created_reference_id = @referenceid 

							SET @outputVal = 'S101'
								
					END


												
						ELSE
						
						BEGIN
						-- Raise error in case of no record found in uploaded status
							RAISERROR(90002,16,1)
						END
						
			END



			
