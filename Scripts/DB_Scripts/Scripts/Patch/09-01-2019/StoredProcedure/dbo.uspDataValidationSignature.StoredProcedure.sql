/****** Object:  StoredProcedure [dbo].[uspDataValidationSignature]    Script Date: 09-01-2019 12:02:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


ALTER PROCEDURE [dbo].[uspDataValidationSignature]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = GETDATE();
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @brand_code nvarchar(max)
	DECLARE @sitecode nvarchar(max)
	DECLARE @record_status  nvarchar(max) = 'record_status'

 
	
	--DECLARE @CountTotal  int 
	--DECLARE @countCalculated  int
	IF EXISTS(
			    SELECT 1 
				FROM [dbo].Auth_Signatories_Signatures
				WHERE record_status = 'Uploaded'
			 )
				
BEGIN

	--IF EXISTS(SELECT 1 FROM Auth_Signatories_Signatures WHERE record_status = 'Active')
	--BEGIN
		IF (@api_flag = 'F')
			BEGIN
					
				
										DECLARE @ForStatus VARCHAR(15) = 'Uploaded'
										DECLARE @ToStatus VARCHAR(30) = 'Approval Pending'

										DECLARE @SetWhere_CBR_bankWise1 VARCHAR(MAX) = ' created_reference_id = '''+@referenceid+''''
										
										EXEC dbo.[uspSetStatus] 'Auth_Signatories_Signatures',@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CBR_bankWise1,@out OUTPUT


										 
					
			END
	
	--END
			--ELSE 
			--	BEGIN
			--		 	DECLARE @tableName2 VARCHAR(30) = 'dbo.Brand_Bill_Capacity'
			--			DECLARE @ForStatus2 VARCHAR(30) = 'Uploaded'
			--			DECLARE @ToStatus2 VARCHAR(30) =  'Approval Pending'
			--			DECLARE @SetWhereClause2 VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''

			--			EXEC  dbo.[uspSetStatus] @tableName2,@ForStatus2,@ToStatus2,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause2,@out OUTPUT
			--		END
					

					SET @outputVal = 'S101'
				
	END
						
						ELSE
						
						BEGIN
							
							RAISERROR(90002,16,1)
						END
						
		END

