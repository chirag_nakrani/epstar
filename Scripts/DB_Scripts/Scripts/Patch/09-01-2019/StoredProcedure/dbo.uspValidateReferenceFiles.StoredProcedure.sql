/****** Object:  StoredProcedure [dbo].[uspValidateReferenceFiles]    Script Date: 09-01-2019 12:09:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspValidateReferenceFiles] 
@file_type varchar(50) ,@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
			DECLARE @Cmd NVARCHAR(MAX);
			--DECLARE @file_type varchar(50) = 'ATM'
			--DECLARE @api_flag varchar(50) = 'U'
			--DECLARE @systemUser varchar(50) = 'as'
			--DECLARE @referenceid varchar(50) = 'asasf'
			DECLARE @Result varchar(50)			
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = GETDATE();
			DECLARE @out1 varchar(100);

			SET @Cmd = CASE 
							WHEN @file_type = 'CRAVAULTMASTER' 
							THEN 'EXEC [uspDataValidationCRAVaultMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'MAILMASTER' 
							THEN 'EXEC [uspDataValidationMailMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'BANKESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationBANKEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'EPSESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationEPSEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'CRAESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationCRAEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'CRAEMPANELED' 
							THEN 'EXEC [uspDataValidationCRAEmpaneled] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'DEFAULTLOADING' 
							THEN 'EXEC [uspDataValidationDefaultLoading] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'BRANDBILLCAPACITY' 
							THEN 'EXEC [uspDataValidationBrandBillCapacity] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'SIGNATURE' 
							THEN 'EXEC [uspDataValidationSignature] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							--WHEN @file_type = 'CYPHERCODE' 
							--THEN 'EXEC [uspDataValidationAtmMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'

					
							END
							--print(@cmd)
			EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 

			IF(@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence from app_config_param where category = 'CBR_Validation' and sub_category = 'Error')
			END

------	If validation is successfull then proceed with the status update change
				
------ Calling Procedure to Update the status 
		            
            IF(@Result = 'S101')
				BEGIN	
					DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log_master'
					DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
					DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
					DECLARE @SetWhereClause VARCHAR(MAX) =' data_for_type = ''' + 
													@file_type+''' and created_reference_id = '''+@referenceid+''''

					EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT


					SELECT @Result;	
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
 		
		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						DECLARE @code varchar(100)
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						IF @ErrorNumber = 90001
						BEGIN
							SET @code = 'E101'
						END
						ELSE IF @ErrorNumber = 90002
						BEGIN
							SET @code = 'E102'
						END
						SELECT @code
						
				END
			 END CATCH
END
