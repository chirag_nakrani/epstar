/****** Object:  StoredProcedure [dbo].[usp_loading_confirmation]    Script Date: 11-04-2019 12:55:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_loading_confirmation] 
(
	@bank_code varchar(100),@project_id varchar(100),@indent_date datetime
)
AS
BEGIN
DROP TABLE IF EXISTS #loaded_amount
DECLARE @timestamp_date varchar(50) = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
DECLARE @cur_user varchar(50) = (SELECT SYSTEM_USER)
DECLARE @referenceid varchar(50) =  (next value for generate_ref_seq)

--EXEC [usp_loading_confirmation] 'BOMH','MOF_MAH', '2019-02-20'

--select * from loading_status_confirmation
--DECLARE @indent_date datetime =  '2019-02-20'
--DECLARE @bank_code varchar(100) = 'BOMH'
--DECLARE @project_id varchar(100) = 'MOF_MAH'

--select max(datafor_date_time) from cash_balance_register where datafor_date_time >= cast(@indent_date  as datetime) and datafor_date_time < @indent_date +1 
--select * from cash_balance_register

select m.*,d.atm_id,d.site_code into #loaded_amount from indent_master m
join distribution_planning_detail_v2 d
on d.indent_code = m.indent_order_number
where m.order_date = @indent_date and total_atm_loading_amount <> 0 and m.record_status = 'Active' and d.record_status = 'Active'
and m.bank = @bank_code and m.project_id = @project_id

--select distinct atm_id from #loaded_amount
--select distinct datafor_date_time from cash_balance_register where record_status = 'Active' order by datafor_date_time desc

DROP TABLE IF EXISTS #balance_prior_loading
SELECT atm.*,register.total_remaining_balance 
into #balance_prior_loading 
FROM #loaded_amount atm

LEFT JOIN atm_master master
on atm.atm_id = master.atm_id
AND atm.site_code = master.site_code
AND master.record_status = 'Active'
AND master.site_status = 'Active'

LEFT JOIN loading_status_confirmation status
on status.atm_id = atm.atm_id
AND status.site_code = atm.site_code
AND status.record_status = 'Active'

LEFT JOIN
cash_balance_register register 
on atm.atm_id = register.atm_id
AND atm.bank = register.bank_name
AND atm.project_id = register.project_id

WHERE atm.record_status = 'Active' and datafor_date_time = (
															select max(datafor_date_time) from cash_balance_register re
														    where datafor_date_time >= cast(@indent_date-1 as datetime) 
															and datafor_date_time < @indent_date and re.bank_name = @bank_code 
															and project_id = @project_id
															)


DROP TABLE IF EXISTS #loading_confirmation
select atm.*,register.total_remaining_balance as latest_balance, 
CASE WHEN register.total_remaining_balance > atm.total_remaining_balance
THEN 'Confirm Loading'
ELSE '-'
END as loading_status INTO #loading_confirmation FROM #balance_prior_loading atm
LEFT JOIN 
cash_balance_register register 
on atm.atm_id = register.atm_id
AND atm.bank = register.bank_name
AND atm.project_id = register.project_id
WHERE atm.record_status = 'Active' and datafor_date_time = (
															select max(datafor_date_time) from cash_balance_register re
															where datafor_date_time >= cast(@indent_date  as datetime) 
															and datafor_date_time < @indent_date +1	and re.bank_name = @bank_code 
															and project_id = @project_id
															)	

--select * from loading_status_confirmation
IF EXISTS 
	(
		SELECT 1 FROM loading_status_confirmation 
		WHERE bank_code = @bank_code and project_id = @project_id
		AND record_status = 'Active' AND indent_date = @indent_date AND loading_status <> 'Confirm Loading'
	)
	BEGIN
		UPDATE  l
		SET record_status = 'Deleted',
			deleted_on = @timestamp_date,
			deleted_by = @cur_user,
			deleted_reference_id = @referenceid
		FROM loading_status_confirmation l
		JOIN #loading_confirmation l1
		ON l.atm_id = l1.atm_id 
		AND l.site_code = l1.site_code
		WHERE bank_code = @bank_code and l.project_id = @project_id
		AND l.record_status = 'Active' AND indent_date = @indent_date
		AND l.loading_status <> 'Confirm Loading'

		INSERT INTO loading_status_confirmation
					(
					 indent_order_number ,
					 site_code			 ,
					 atm_id				 ,
					 bank_code			 ,
					 project_id			 ,
					 feeder_branch_code	 ,
					 cra				 ,	
					 indent_date		 ,	
					 loading_status		 ,
					 record_status		 ,
					 created_on			 ,
					 created_by			 ,
					 created_reference_id
					 )
		 SELECT indent_order_number,
				site_code,
				atm_id,
				bank,
				project_id,
				feeder_branch,
				cra,
				cast(@indent_date as date),
				loading_status,
				'Active',
				@timestamp_date,
				@cur_user,
				@referenceid
				 FROM 
		 #loading_confirmation
		 where loading_status <> 'Confirm Loading'

		END

		ELSE 
		BEGIN
		
		INSERT INTO loading_status_confirmation
					(
					 indent_order_number ,
					 site_code			 ,
					 atm_id				 ,
					 bank_code			 ,
					 project_id			 ,
					 feeder_branch_code	 ,
					 cra				 ,	
					 indent_date		 ,	
					 loading_status		 ,
					 record_status		 ,
					  created_on		 ,
					 created_by			 ,
					 created_reference_id
					 )
		 SELECT indent_order_number,
				site_code,
				atm_id,
				bank,
				project_id,
				feeder_branch,
				cra,
				cast(@indent_date as date)
				,loading_status
				,'Active' 
				,@timestamp_date,
				@cur_user,
				@referenceid
				FROM 
		 #loading_confirmation
		 END
END