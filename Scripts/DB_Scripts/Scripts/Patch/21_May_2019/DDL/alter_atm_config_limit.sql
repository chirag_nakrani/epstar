/****** Object:  StoredProcedure [dbo].[uspDataValidationAtmcnfiglimits]    Script Date: 4/24/2019 5:36:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspDataValidationAtmcnfiglimits]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @atmid nvarchar(max)
	DECLARE @sitecode nvarchar(max)
	-------- Updated Part START-------------------------
	DECLARE @errorcode varchar(max)
	--------Updated Part END-----------------------------

	
	-- Checking for any record that is present in uploaded status in table
	
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].atm_config_limits
				WHERE record_status = 'Uploaded'
				and created_reference_id =  @referenceid
			)
				
			BEGIN
			-- Proceed if we found any record in uploaded status
				
				--- SELECT total count of records present in   file for particular date and status
				SET @CountTotal = (
									SELECT count(1) FROM atm_config_limits
									WHERE (record_status = 'Uploaded' and 
									created_reference_id = @referenceid)
									OR (record_status = 'Active')
								  )	   
	
				SET @countCalculated = (
										SELECT count(1) FROM atm_config_limits acl
										INNER JOIN atm_master clm 
										ON acl.atm_id = clm.atm_id and acl.site_code = clm.site_code
										and clm.site_status = 'Active' and clm.record_status = 'Active'
										WHERE (acl.record_status = 'Uploaded'
										and acl.created_reference_id = @referenceid)
										OR (acl.record_status = 'Active')
									)

----------------- Updating isValidRecord as Valid or Invalid based on validation ---------------------------
		
-------------- Check if all atm ids and site code are present in the master data -----------------
				update  acl 
					set is_valid_record = case when 
					clm.atm_id is null then 'No'
					else 'Yes'
					END 
					FROM atm_config_limits acl
					LEFT JOIN atm_master clm 
					ON acl.atm_id = clm.atm_id and acl.site_code = clm.site_code
					and clm.site_status = 'Active' and clm.record_status = 'Active'
					WHERE (acl.record_status = 'Uploaded'
					and acl.created_reference_id = @referenceid)
					OR (acl.record_status = 'Active')

					DECLARE @tableName VARCHAR(30) = 'dbo.atm_config_limits'
					DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
					DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'

					--- If all records found in config limits and atm master table

					IF ((@CountTotal = @countCalculated) OR (@CountTotal <> @countCalculated AND @countCalculated > 0))
					BEGIN
						
						--- Update is_valid_file in data update log as YES

						UPDATE data_update_log_master 
						SET is_valid_file = 
								CASE
									WHEN @CountTotal = @countCalculated
									THEN 'Yes'
									ELSE
										'No'
									END
						WHERE record_status = 'Uploaded'
						AND data_for_type = 'ATMCNFGLIMIT'
						AND created_reference_id = @referenceid

						IF EXISTS( SELECT 1 FROM atm_config_limits WHERE record_status = 'Active')
							BEGIN
							
							-- Check for API Flag if it is for File Upload or screen edit
							-- 'F' - File Upload ,  else - Screen Edit
							---Changes
							IF (@api_flag = 'F')
								BEGIN
						-- changes 04-01-2019
						 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in ATM_Config_Limits started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


					 UPDATE a
                            set  
                            a.site_code                      =COALESCE(a.site_code,b.site_code),
                            a.old_atm_id                     =COALESCE(a.old_atm_id,b.old_atm_id),
                            a.atm_id                         =COALESCE(a.atm_id,b.atm_id),
                            a.project_id                     =COALESCE(a.project_id,b.project_id),
                            a.bank_name                      =COALESCE(a.bank_name,b.bank_name),
                            a.bank_code                      =COALESCE(a.bank_code,b.bank_code),
                            a.insurance_limit                =COALESCE(a.insurance_limit,b.insurance_limit),
                            a.whether_critical_atm           =COALESCE(a.whether_critical_atm,b.whether_critical_atm),
                            a.cassette_50_count              =COALESCE(a.cassette_50_count,b.cassette_50_count),
                            a.cassette_100_count             =COALESCE(a.cassette_100_count,b.cassette_100_count),
                            a.cassette_200_count             =COALESCE(a.cassette_200_count,b.cassette_200_count),
                            a.cassette_500_count             =COALESCE(a.cassette_500_count,b.cassette_500_count),
                            a.cassette_2000_count            =COALESCE(a.cassette_2000_count,b.cassette_2000_count),
                            a.total_cassette_count           =COALESCE(a.total_cassette_count,b.total_cassette_count),
                            a.bank_cash_limit                =COALESCE(a.bank_cash_limit,b.bank_cash_limit),
                            a.base_limit                     =COALESCE(a.base_limit,b.base_limit),
                            a.s_g_locker_no                  =COALESCE(a.s_g_locker_no,b.s_g_locker_no),
                            a.type_of_switch                 =COALESCE(a.type_of_switch,b.type_of_switch),                            
                            -- a.created_on                     =COALESCE(a.created_on,b.created_on),
                            -- a.created_by                     =COALESCE(a.created_by,b.created_by),
                            -- a.created_reference_id           =COALESCE(a.created_reference_id,b.created_reference_id),
                            -- a.approved_on                    =COALESCE(a.approved_on,b.approved_on),
                            -- a.approved_by                    =COALESCE(a.approved_by,b.approved_by),
                            -- a.approved_reference_id          =COALESCE(a.approved_reference_id,b.approved_reference_id),
                            -- a.approve_reject_comment         =COALESCE(a.approve_reject_comment,b.approve_reject_comment),
                            -- a.rejected_on                    =COALESCE(a.rejected_on,b.rejected_on),
                            -- a.rejected_by                    =COALESCE(a.rejected_by,b.rejected_by),
                            -- a.reject_reference_id            =COALESCE(a.reject_reference_id,b.reject_reference_id),
                            -- a.deleted_on                     =COALESCE(a.deleted_on,b.deleted_on),
                            -- a.deleted_by                     =COALESCE(a.deleted_by,b.deleted_by),
                            -- a.deleted_reference_id           =COALESCE(a.deleted_reference_id,b.deleted_reference_id),
                            -- a.is_valid_record                =COALESCE(a.is_valid_record,b.is_valid_record),
                            -- a.error_code                     =COALESCE(a.error_code,b.error_code),
							a.record_status = 
										CASE 
											WHEN (a.record_status = 'Uploaded' 
												and a.is_valid_record = 'Yes')
											THEN
												 'Approval Pending'
											WHEN (a.record_status = 'Active' 
												and a.is_valid_record = 'No') 
											THEN
											 'History'
										ELSE 
											a.record_status
										END,
							a.modified_on = @current_datetime_stmp,
							a.modified_by = @systemUser,
							a.modified_reference_id = @referenceid
							from  ATM_Config_limits a 
							LEFT join ATM_Config_limits b 
							on a.atm_id = b.atm_id
							and a.site_code = b.site_code 
							and b.record_status = 'Active'
							WHERE --a.record_status = 'Uploaded' 
							--and a.is_valid_record = 'Yes'
							(a.record_status = 'Uploaded'
							and a.created_reference_id = @referenceid)
							OR (a.record_status = 'Active')
					     INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in ATM_Config_Limits completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

						 END
									ELSE
									BEGIN
										-- Screen Edit Scenario --
										-- For screen edit we have to directly update the record status from uploaded to approval pending

										DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''' and is_valid_record = ''Yes'''

										EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
										

									END
							END
							ELSE 
								BEGIN

									-- If there are no records in active status then directly update record status to approval pending 
										
										DECLARE @SetWhereClause2 VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''' and is_valid_record = ''Yes'''

										EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause2,@out OUTPUT
		
									END
									
									UPDATE data_update_log_master 
										SET pending_count =	
												(
													SELECT COUNT(*) FROM dbo.atm_config_limits WHERE created_reference_id = @referenceid
													and record_status = 'Approval Pending'
													),
											total_count = 
											(
												SELECT COUNT(*) FROM dbo.atm_config_limits WHERE created_reference_id = @referenceid
													
											)
										WHERE record_status = 'Uploaded'
										AND data_for_type = 'ATMCNFGLIMIT' 
										AND created_reference_id = @referenceid
									
									SET @outputVal = 'S101'
										
							END

				ELSE 
					BEGIN
	
	-----------------------Updated Part START-----------------------------------------
						UPDATE data_update_log_master 
						SET is_valid_file = 'No'
						WHERE record_status = 'Uploaded'
						AND data_for_type = 'ATMCNFGLIMIT' 
						AND created_reference_id = @referenceid
						
						RAISERROR(90001,16,1)

					END

				END
						
						ELSE
						
						BEGIN
							--SELECT 'In rasierror 90002'
							RAISERROR(90002,16,1)
						END
						
			END
