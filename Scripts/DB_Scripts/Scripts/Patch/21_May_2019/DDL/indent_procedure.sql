/****** Object:  StoredProcedure [dbo].[usp_indent_calculation_V3_Holiday]    Script Date: 20-05-2019 16:32:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC [usp_indent_calculation_V2] '2019-02-23','Malay','abc223'
--- EXEC [usp_indent_calculation_V2] '2019-02-23', 'malay', 'ref001' , 'ALL','BOMH'

--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: Calculation of Indent for the indentdate @dateT
 --- =========================================================================
--[usp_indent_calculation_V2] '2019-02-23', 'malay', 'ref001' , 'ALL','BOMH'
--[usp_indent_calculation_V2] '2019-03-08', 'malay', 'ref001' , 'MOF_MAH','BOMH','AHMEDPUR'
--DECLARE @out VARCHAR(50)
--EXEC [usp_indent_calculation_V2] '2019-03-08', 'malay', 'ref001' , 'All','All','All','No','',@out OUTPUT
--SELECT @out
ALTER PROCEDURE [dbo].[usp_indent_calculation_V3]
 (		
        @dateT DATETIME,
		@cur_user nvarchar(50),
		@created_reference_id nvarchar(50),
		@for_project_id nvarchar(50) = 'All',
		@for_bank_code nvarchar(50) = 'All',
		@for_feeder_branch nvarchar(50) = 'All',
		@is_revised_forecasting varchar(10) = 'No',
		@active_indent_code	NVARCHAR(max) ,
		@outputVal varchar(500) OUTPUT

 )

AS 
BEGIN
	
BEGIN TRY
	--Temp_variable to calculate indent
	--DECLARE @dateT DATETIME = '2019-04-29'
	--DECLARE @cur_user varchar(50) = 'SA'
	--DECLARE @created_reference_id varchar(40) = 'C10060367031559'
	-----------------DECLARE @for_project_id varchar(50) = 'MOF_MAH'
	---------------------DECLARE @for_bank_code varchar(40) = 'SBI'
	---------------------DECLARE @for_feeder_branch nvarchar(50) = 'PIMPRI, PUNE_00575'
	---------------------DECLARE @is_revised_forecasting varchar(10) = 'Yes'
	---------------------DECLARE @active_indent_code	NVARCHAR(max) = 'SBI/MOF_MAH/PIMPRI, PUNE_00575/Logicash/20190422175115'

	--DECLARE @for_project_id varchar(50) = 'MOF_MAH'
	--DECLARE @for_bank_code varchar(40) = 'BOMH'
	--DECLARE @for_feeder_branch nvarchar(50) = 'AMRAVATI'
	--DECLARE @is_revised_forecasting varchar(10) = 'Yes'
	--DECLARE @active_indent_code	NVARCHAR(max) = 'BOMH/MOF_MAH/AMRAVATI/LOGICASH/20190517131231'
	DECLARE @is_atm_list_available varchar(10)
	SET NOCOUNT ON
	SET ANSI_WARNINGS OFF 
	declare @timestamp_date datetime =  DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50) 
	DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
	declare @Execution_log_str nvarchar(max)
	--DECLARE @outputVal varchar(100)
	declare @feederbranchcode varchar(max)
	DECLARE @indentcounter INT 
	DECLARE @shortcode NVARCHAR(50)

	--SET @for_bank_code = CONCAT(@for_bank_code,'+')
	--DROP TABLE IF EXISTS #BankArray
	--;WITH Split
 --   AS(
 --       SELECT CAST(0 as varchar(max)) AS stpos, cast(CHARINDEX('+',@for_bank_code) as varchar(max)) AS endpos
 --       UNION ALL
 --       SELECT cast(endpos+1 as varchar(max)), CAST(CHARINDEX('+',@for_bank_code,endpos+1) as varchar(max))
 --           FROM Split
 --           WHERE endpos > 0
	--	)
	--	select SubString(@for_bank_code,cast(stpos as int),cast(endpos as int)-cast(stpos as int)) as ColumnHeaders into #BankArray from Split where endpos <> 0 OPTION (MAXRECURSION 1000)

	
	
		DECLARE @forecast_amt BIGINT
	
	--set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@dateT as nvarchar(50)) 
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Execution of [dbo].[usp_indent_calculation_V3] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		--DECLARE @dateT DATETIME
		--SET     @dateT ='2018-11-05'

		DECLARE @MaxDestinationDate DATE
		DECLARE @dispenseformula NVARCHAR(50)
		DECLARE @confidence_factor NVARCHAR(50)
		DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		DECLARE @denomination_wise_round_off_100 INT			 -- for rounding, replace with 100000
		DECLARE @denomination_wise_round_off_200 int      
		DECLARE @denomination_wise_round_off_500 int
		DECLARE @denomination_wise_round_off_2000 int
		DECLARE @denomination_wise_round_off_100_currency_chest int      
		DECLARE @denomination_wise_round_off_200_currency_chest int
		DECLARE @denomination_wise_round_off_500_currency_chest int
		DECLARE @denomination_wise_round_off_2000_currency_chest INT
		declare @default_avg_dispense int
		declare @vaulting_for_normal_weekday_percentage float,
				@vaulting_for_normal_weekend_percentage float,
				@vaulting_for_extended_weekend_percentage float
		
		
		Select	@confidence_factor = confidence_factor
		, @dispenseformula = dispenseformula
		, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		, @denomination_wise_round_off_100_currency_chest = denomination_wise_round_off_100_currency_chest
		, @denomination_wise_round_off_200_currency_chest = denomination_wise_round_off_200_currency_chest
		, @denomination_wise_round_off_500_currency_chest = denomination_wise_round_off_500_currency_chest
		, @denomination_wise_round_off_2000_currency_chest = denomination_wise_round_off_2000_currency_chest
		,@default_avg_dispense = default_average_dispense
		, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
		, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
		, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
		 from system_settings where record_status = 'Active'

		-- select * from system_settings

	/****************************************** CALCULATE AVERAGE DISPENSE ********************************************/
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Average dispense calculation started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

		DROP TABLE IF EXISTS #calculated_avg_dispense
		
		--declare @dateT date = '2019-04-16'

		declare @dateTM3_TM3 date = (CONVERT (date, ( CONVERT(varchar,    DATEADD(day,-3, DATEADD(month,-3,@dateT)  )  , 105) ),105) )
		declare @dateTM2_TM3 date = (CONVERT (date, ( CONVERT(varchar,    DATEADD(day,-3, DATEADD(month,-2,@dateT)  )  , 105) ),105) )
		declare @dateTM1_TM3 date = (CONVERT (date, ( CONVERT(varchar,    DATEADD(day,-3, DATEADD(month,-1,@dateT)  )  , 105) ),105) )
		declare @dateT_minus_7 date = CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-7,@dateT)  )  , 105) ),105)
		
		Declare @default_dispense_amount int = (Select default_average_dispense from system_settings where record_status='Active')
		
		select *, CEILING((SELECT MAX(v)
														  FROM (VALUES ([avg_of_5days_Tminus5]),
														 ([avg_of_2days_Tminus2]), 
														 (avg_of_four_months_avg),
		                                                 (default_dispense_amount)
		                                                 ) AS i(v) ) *1.0 / 10000)*10000  as max_of_current
		INTO #calculated_avg_dispense
		from
		(
			select 
				*
				,(	select MAX(v)
						FROM 
						(
							VALUES 
								([avg_month_minus_3]),
								([avg_month_minus_2]), 
								([avg_month_minus_1]),
								(avg_month_minus_0)
						) AS i(v) 
				 ) as max_of_four_months_avg
				 ,(	select AVG(v)
						FROM 
						(
							VALUES 
								([avg_month_minus_3]),
								([avg_month_minus_2]), 
								([avg_month_minus_1]),
								(avg_month_minus_0)
						) AS i(v) 
				 ) as avg_of_four_months_avg
				 ,@default_dispense_amount	as default_dispense_amount
				 --PENDING: max_of_current
				 ,(	select MAX(v)
						FROM 
						(
							VALUES 
								(max_month_minus_3),
								(max_month_minus_2), 
								(max_month_minus_1),
								(max_month_minus_0)
						) AS i(v) 
				 ) as max_of_four_months_max
				 ,(	select AVG(v)
						FROM 
						(
							VALUES 
								(max_month_minus_3),
								(max_month_minus_2), 
								(max_month_minus_1),
								(max_month_minus_0)
						) AS i(v) 
				 ) as avg_of_four_months_max
				 
				 ,(	select MAX(v)
						FROM 
						(
							VALUES 
								(second_max_month_minus_3),
								(second_max_month_minus_2), 
								(second_max_month_minus_1),
								(second_max_month_minus_0)
						) AS i(v) 
				 ) as max_of_four_months_second_max
		
				 ,(	select AVG(v)
						FROM 
						(
							VALUES 
								(second_max_month_minus_3),
								(second_max_month_minus_2), 
								(second_max_month_minus_1),
								(second_max_month_minus_0)
						) AS i(v) 
				 ) as avg_of_four_months_second_max
		
				 ,(	select MAX(v)
						FROM 
						(
							VALUES 
								(third_max_month_minus_3),
								(third_max_month_minus_2), 
								(third_max_month_minus_1),
								(third_max_month_minus_0)
						) AS i(v) 
				 ) as max_of_four_months_third_max
		
				 ,(	select AVG(v)
						FROM 
						(
							VALUES 
								(third_max_month_minus_3),
								(third_max_month_minus_2), 
								(third_max_month_minus_1),
								(third_max_month_minus_0)
						) AS i(v) 
				 ) as avg_of_four_months_third_max
				,(	select MAX(v)
						FROM 
						(
							VALUES 
								(avg_28_days),
								(avg_of_2days_Tminus2)						
						) AS i(v) 
				 ) as max_28_days_or_2_days 	 
			from
			(
				SELECT 
						atm_id
						,bank_name as bank_code
						,project_id
						,max( case when cast ( datafor_date_time as date) = dateadd(day,0,@dateTM3_TM3) then [total_dispense_amount]  end) as T_M3_minus_3
						,max( case when cast ( datafor_date_time as date) = dateadd(day,1,@dateTM3_TM3) then [total_dispense_amount]  end) as T_M3_minus_2
						,max( case when cast ( datafor_date_time as date) = dateadd(day,2,@dateTM3_TM3) then [total_dispense_amount]  end) as T_M3_minus_1
						,max( case when cast ( datafor_date_time as date) = dateadd(day,3,@dateTM3_TM3) then [total_dispense_amount]  end) as T_M3_minus_0
						,max( case when cast ( datafor_date_time as date) = dateadd(day,4,@dateTM3_TM3) then [total_dispense_amount]  end) as T_M3_plus_1
						,max( case when cast ( datafor_date_time as date) = dateadd(day,5,@dateTM3_TM3) then [total_dispense_amount]  end) as T_M3_plus_2
						,max( case when cast ( datafor_date_time as date) = dateadd(day,6,@dateTM3_TM3) then [total_dispense_amount]  end) as T_M3_plus_3
		
						,max( case when cast ( datafor_date_time as date) = dateadd(day,0,@dateTM2_TM3) then [total_dispense_amount]  end) as T_M2_minus_3
						,max( case when cast ( datafor_date_time as date) = dateadd(day,1,@dateTM2_TM3) then [total_dispense_amount]  end) as T_M2_minus_2
						,max( case when cast ( datafor_date_time as date) = dateadd(day,2,@dateTM2_TM3) then [total_dispense_amount]  end) as T_M2_minus_1
						,max( case when cast ( datafor_date_time as date) = dateadd(day,3,@dateTM2_TM3) then [total_dispense_amount]  end) as T_M2_minus_0
						,max( case when cast ( datafor_date_time as date) = dateadd(day,4,@dateTM2_TM3) then [total_dispense_amount]  end) as T_M2_plus_1
						,max( case when cast ( datafor_date_time as date) = dateadd(day,5,@dateTM2_TM3) then [total_dispense_amount]  end) as T_M2_plus_2
						,max( case when cast ( datafor_date_time as date) = dateadd(day,6,@dateTM2_TM3) then [total_dispense_amount]  end) as T_M2_plus_3
		
						,max( case when cast ( datafor_date_time as date) = dateadd(day,0,@dateTM1_TM3) then [total_dispense_amount]  end) as T_M1_minus_3
						,max( case when cast ( datafor_date_time as date) = dateadd(day,1,@dateTM1_TM3) then [total_dispense_amount]  end) as T_M1_minus_2
						,max( case when cast ( datafor_date_time as date) = dateadd(day,2,@dateTM1_TM3) then [total_dispense_amount]  end) as T_M1_minus_1
						,max( case when cast ( datafor_date_time as date) = dateadd(day,3,@dateTM1_TM3) then [total_dispense_amount]  end) as T_M1_minus_0
						,max( case when cast ( datafor_date_time as date) = dateadd(day,4,@dateTM1_TM3) then [total_dispense_amount]  end) as T_M1_plus_1
						,max( case when cast ( datafor_date_time as date) = dateadd(day,5,@dateTM1_TM3) then [total_dispense_amount]  end) as T_M1_plus_2
						,max( case when cast ( datafor_date_time as date) = dateadd(day,6,@dateTM1_TM3) then [total_dispense_amount]  end) as T_M1_plus_3
			  
						,max( case when cast ( datafor_date_time as date) = dateadd(day,0,@dateT_minus_7) then [total_dispense_amount]  end) as T_minus_7
						,max( case when cast ( datafor_date_time as date) = dateadd(day,1,@dateT_minus_7) then [total_dispense_amount]  end) as T_minus_6
						,max( case when cast ( datafor_date_time as date) = dateadd(day,2,@dateT_minus_7) then [total_dispense_amount]  end) as T_minus_5
						,max( case when cast ( datafor_date_time as date) = dateadd(day,3,@dateT_minus_7) then [total_dispense_amount]  end) as T_minus_4
						,max( case when cast ( datafor_date_time as date) = dateadd(day,4,@dateT_minus_7) then [total_dispense_amount]  end) as T_minus_3
						,max( case when cast ( datafor_date_time as date) = dateadd(day,5,@dateT_minus_7) then [total_dispense_amount]  end) as T_minus_2
						,max( case when cast ( datafor_date_time as date) = dateadd(day,6,@dateT_minus_7) then [total_dispense_amount]  end) as T_minus_1
			  
						, avg( case when cast ( datafor_date_time as date) between dateadd(day,2,@dateT_minus_7) and dateadd(day,6,@dateT_minus_7) and total_dispense_amount > 0 then  total_dispense_amount end ) as avg_of_5days_Tminus5
		
						, avg( case when cast ( datafor_date_time as date) between dateadd(day,5,@dateT_minus_7) and dateadd(day,6,@dateT_minus_7) and total_dispense_amount > 0 then  total_dispense_amount end ) as avg_of_2days_Tminus2
		
						, avg( case when cast ( datafor_date_time as date) between @dateTM3_TM3   and dateadd(day,6,@dateTM3_TM3  ) and total_dispense_amount > 0 then  total_dispense_amount end )	as avg_month_minus_3
						, avg( case when cast ( datafor_date_time as date) between @dateTM2_TM3   and dateadd(day,6,@dateTM2_TM3  ) and total_dispense_amount > 0 then  total_dispense_amount end )	as avg_month_minus_2
						, avg( case when cast ( datafor_date_time as date) between @dateTM1_TM3   and dateadd(day,6,@dateTM1_TM3  ) and total_dispense_amount > 0 then  total_dispense_amount end )	as avg_month_minus_1
						, avg( case when cast ( datafor_date_time as date) between @dateT_minus_7 and dateadd(day,6,@dateT_minus_7) and total_dispense_amount > 0 then  total_dispense_amount end )	as avg_month_minus_0
		
						, max( case when cast ( datafor_date_time as date) between @dateTM3_TM3   and dateadd(day,6,@dateTM3_TM3  ) and total_dispense_amount > 0 then  total_dispense_amount end )	as max_month_minus_3
						, max( case when cast ( datafor_date_time as date) between @dateTM2_TM3   and dateadd(day,6,@dateTM2_TM3  ) and total_dispense_amount > 0 then  total_dispense_amount end )	as max_month_minus_2
						, max( case when cast ( datafor_date_time as date) between @dateTM1_TM3   and dateadd(day,6,@dateTM1_TM3  ) and total_dispense_amount > 0 then  total_dispense_amount end )	as max_month_minus_1
						, max( case when cast ( datafor_date_time as date) between @dateT_minus_7 and dateadd(day,6,@dateT_minus_7) and total_dispense_amount > 0 then  total_dispense_amount end )	as max_month_minus_0				
		
						, max( case when cast ( datafor_date_time as date) between @dateT_minus_7 and dateadd(day,6,@dateT_minus_7) and total_dispense_amount > 0 and max_order_seq = 2 then  total_dispense_amount end )	as second_max_month_minus_0
						, max( case when cast ( datafor_date_time as date) between @dateT_minus_7 and dateadd(day,6,@dateT_minus_7) and total_dispense_amount > 0 and max_order_seq = 3 then  total_dispense_amount end )	as third_max_month_minus_0
		
						, max( case when cast ( datafor_date_time as date) between @dateTM1_TM3 and dateadd(day,6,@dateTM1_TM3) and total_dispense_amount > 0 and max_order_seq = 2 then  total_dispense_amount end )	as second_max_month_minus_1
						, max( case when cast ( datafor_date_time as date) between @dateTM1_TM3 and dateadd(day,6,@dateTM1_TM3) and total_dispense_amount > 0 and max_order_seq = 3 then  total_dispense_amount end )	as third_max_month_minus_1
		
						, max( case when cast ( datafor_date_time as date) between @dateTM2_TM3 and dateadd(day,6,@dateTM2_TM3) and total_dispense_amount > 0 and max_order_seq = 2 then  total_dispense_amount end )	as second_max_month_minus_2
						, max( case when cast ( datafor_date_time as date) between @dateTM2_TM3 and dateadd(day,6,@dateTM2_TM3) and total_dispense_amount > 0 and max_order_seq = 3 then  total_dispense_amount end )	as third_max_month_minus_2
		
						, max( case when cast ( datafor_date_time as date) between @dateTM3_TM3 and dateadd(day,6,@dateTM3_TM3) and total_dispense_amount > 0 and max_order_seq = 2 then  total_dispense_amount end )	as second_max_month_minus_3
						, max( case when cast ( datafor_date_time as date) between @dateTM3_TM3 and dateadd(day,6,@dateTM3_TM3) and total_dispense_amount > 0 and max_order_seq = 3 then  total_dispense_amount end )	as third_max_month_minus_3
		
						,avg( case when total_dispense_amount > 0 then  total_dispense_amount end )	as avg_28_days
		
						 from
						 (
						 select *, 
						  DENSE_RANK() over (partition by [atm_id],bank_name,[project_id],group_name    order by total_dispense_amount desc) as max_order_seq
						 from
						 (
						 select *
						 ,case 
							when cast(datafor_date_time as date) between @dateTM3_TM3 and  dateadd(day,6,@dateTM3_TM3) then 'G_TM3' 
							when cast(datafor_date_time as date) between @dateTM2_TM3 and  dateadd(day,6,@dateTM2_TM3) then 'G_TM2'
							when cast(datafor_date_time as date) between @dateTM1_TM3 and  dateadd(day,6,@dateTM1_TM3) then 'G_TM1'
							when cast(datafor_date_time as date) between @dateT_minus_7 and  dateadd(day,6,@dateT_minus_7) then 'G_TM0'
						 end as group_name				 
					FROM [dbo].cash_dispense_register 
					where record_status = 'Active'
					and 
					(
						cast(datafor_date_time as date) between @dateTM3_TM3 and  dateadd(day,6,@dateTM3_TM3)
						or
						cast(datafor_date_time as date) between @dateTM2_TM3 and  dateadd(day,6,@dateTM2_TM3)
						or
						cast(datafor_date_time as date) between @dateTM1_TM3 and  dateadd(day,6,@dateTM1_TM3)
						or 
						cast(datafor_date_time as date) between @dateT_minus_7 and dateadd(day,6,@dateT_minus_7)
					)
					and 
					(project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
					and 
					(
					bank_name IN 
							( SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank_name else  @for_bank_code end ,','))
					)		
				)main
				)main_p
				group by  [atm_id]
					,bank_name
					,[project_id]
				)a
		)b

	
		
	/******************************************	END OF AVG DISPESNE CALC **********************************************/

	--select * from #calculated_avg_dispense where atm_id = 'NA1220C1'


	/*******************************************Get Vaulting Percentage*************************************************/

 
	--IF @is_Indent=1
	--BEGIN
	--declare @dateT date
	--SET @dateT ='2018-11-22'
	--Set @MaxDestinationDate=(Select ISNULL(max(for_date),'1900-01-01') from Applied_Vaulting)
  
	/*********************************Calculate Applied vaulting******************************************************/														 
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Vaulting percentage calculation started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	--DECLARE @dateT DATETIME
	--	SET     @dateT ='2019-04-17'

	--	DECLARE @MaxDestinationDate DATE
	--	DECLARE @dispenseformula NVARCHAR(50)
	--	DECLARE @confidence_factor NVARCHAR(50)
	--	DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
	--	DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
	--	DECLARE @denomination_wise_round_off_500 int
	--	DECLARE @denomination_wise_round_off_2000 int
	--	DECLARE @denomination_wise_round_off_100 INT
	--	declare @default_avg_dispense int
	--	declare @vaulting_for_normal_weekday_percentage float,
	--			@vaulting_for_normal_weekend_percentage float,
	--			@vaulting_for_extended_weekend_percentage float
		
		
	--	Select	@confidence_factor = confidence_factor
	--	, @dispenseformula = dispenseformula
	--	, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
	--	, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
	--	, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
	--	, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
	--	,@default_avg_dispense = default_average_dispense
	--	, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
	--	, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
	--	, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
	--	 from system_settings where record_status = 'Active'
		
		drop table if exists #Applied_Vaulting
		
		Select * 
		into #Applied_Vaulting 
	    from(  
	
			SELECT 
			 @dateT as    indent_date,
						am.project_id,
						am.bank_code,
						am.atm_id,
			          case when cra.vaulting ='Yes' and fbm.is_vaulting_enabled = 'Yes'
							 then 
								case when fvpc.is_vaulting_allowed = 'Yes' 
									    then fvpc.vaulting_percentage 
										  else (
										 case  when 
									     --If current day is Friday of 1st, 3rd and 5th Week of month then apply vault % as normal weekday percentage.
											  ((DATENAME(dw,@dateT) = 'Friday' 							 
											  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
												  in (1,3,5) ) OR (DATENAME(dw,@dateT) in ('Monday','Tuesday','Wednesday','Thursday')))
																		then @vaulting_for_normal_weekday_percentage

							 --***********************************************Start Friday as last day of week Check*****************************************************

							 --*****Start Check--->If current day is Friday of 2nd and 4th Week of month then apply vault % as per Extended Check
							 --** Extended Check -->     For weekend considered, if next day after weekend is not holiday then apply vault % as normal weekend percentage.
							                          -- For week end considered, if 1 more day is holiday apart from weekend then apply vault % as extended weekend percentage.
													  -- Using DecideLimitdays toc verify above two conditions

							   when( ( DATENAME(dw,@dateT) = 'Friday' 							 
								  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
									  in (2,4) ) --OR (DATENAME(dw,@dateT) in ('Saturday'))
									  )
								  and LD.decidelimitdays <3 then @vaulting_for_normal_weekend_percentage

							   when (( DATENAME(dw,@dateT) = 'Friday' 							 
								  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
									  in (2,4)) --OR (DATENAME(dw,@dateT) in ('Saturday')) 
									  )
								  and LD.decidelimitdays >=3 then @vaulting_for_extended_weekend_percentage
                              --***********************************************End Friday as last day of week Check*****************************************************


							   --*****Start Check---> If current day is Saturday of 1st,3rd and 5th Week of month then apply vault % as per Extended Check
							   --** Extended Check -->     For weekend considered, if next day after weekend is not holiday then apply vault % as normal weekend percentage.
							                         --  For week end considered, if 1 more day is holiday apart from weekend then apply vault % as extended weekend percentage.
													  -- Using DecideLimitdays toc verify above two conditions
							   when  DATENAME(dw,@dateT) = 'Saturday' 							 
								  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
									  in (1,3,5)
								  and LD.decidelimitdays <=2 then @vaulting_for_normal_weekend_percentage

							   when  DATENAME(dw,@dateT) = 'Saturday' 							 
								  and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, @dateT), 0), @dateT) +1) 
									  in (1,3,5)
								  and LD.decidelimitdays >=2 then @vaulting_for_extended_weekend_percentage

								WHEN (DATENAME(dw,@dateT) = 'Thursday' AND ld.decidelimitdays >=3)  							 
									and (DATENAME(dw,@dateT) = 'Wednesday' AND ld.decidelimitdays >=4)
									AND (DATENAME(dw,@dateT) = 'Tuesday' AND ld.decidelimitdays >=5)
									AND (DATENAME(dw,@dateT) = 'Monday' AND ld.decidelimitdays >=6)  
								THEN @vaulting_for_extended_weekend_percentage			 
								
									  							 
								  							 
								

                              --***********************************************End saturday as last day of week Check*****************************************************
 
						 end
									 ---*************************old logic*********************************
									--case 
									--	  when DATEPART(dw, @dateT)  NOT IN (1,7)   
									--	  then  @vaulting_for_normal_weekday_percentage -- 50
									--    when  DATEPART(dw, @dateT) IN (1,7)  
									--	  then  @vaulting_for_normal_weekend_percentage --  60
									--    else  @vaulting_for_extended_weekend_percentage --   70 
									--end
									 ---*************************old logic*********************************
				  )
				
				end 
				 
			    end applied_vaulting_percentage
			  
	 
	         	 	 
		       --SELECT am.atm_id,cra.vaulting,fvpc.is_vaulting_allowed,fvpc.vaulting_percentage  
			    from atm_master am
				 inner join cra_feasibility cra
				 on cra.site_code=am.site_code 
				and cra.atm_id=am.atm_id
				and am.record_status ='Active'
				and am.site_status = 'Active'
				AND cra.record_status = 'Active'
				
				inner join feeder_branch_master fbm 
				on fbm.project_id = cra.project_id
					and fbm.bank_code = cra.bank_code
					and fbm.feeder_branch = cra.feeder_branch_code
					and fbm.record_status = 'Active'

				 left join  Feeder_Vaulting_pre_config fvpc
				 on cra.feeder_branch_code=fvpc.feeder_branch_code
					and cra.project_id=fvpc.project_id
					and cra.bank_code=fvpc.bank_code
				    and fvpc.record_status='Active'
				

				 left join limitdays LD
				 on am.atm_id=LD.atm_id
				 and am.bank_code=LD.bank_code
				 and am.project_id=LD.project_id
				 and cast(ld.indent_date as date)=  @dateT   
				 and LD.record_status='Active'
				  
				  
				 )d 
				 
				 where d.applied_vaulting_percentage is not null
  

  		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Get maximum date bank wise for morning balance started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	--Select * from #Applied_Vaulting
  /**************************************************************************************************************/

  drop table if exists #max_date_bank_wise
  drop table if exists #morning_balance

 Select * into #max_date_bank_wise
 from(
 Select bank_name, project_id,max(datafor_date_time) as max_date
		from cash_balance_register 									 
		where datafor_date_time > dateadd(dd,-2 ,@dateT)
		and 
		(project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
		and 
		(
		bank_name IN 
		( SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank_name else  @for_bank_code end ,',')))
		and record_status ='Active' 
		group by  bank_name, project_id
		)a

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
    values ('morning balance calculation started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

		SELECT  cbr.atm_id,
				cbr.bank_name,
				cbr.project_id,
				cbr.remaining_balance_50,
				cbr.remaining_balance_100,
				cbr.remaining_balance_200,
				cbr.remaining_balance_500,
				cbr.remaining_balance_2000,
				cbr.total_remaining_balance,
				cbr.record_status
				INTO #morning_balance
				FROM [dbo].[cash_balance_register] cbr
					INNER JOIN #max_date_bank_wise MDB
					on CBR.bank_name=MDB.bank_name
					and CBR.project_id=MDB.project_id
					and CBR.datafor_date_time=mdb.max_date
					and cbr.record_status='Active'	
	/***********************************************************************************************************/
	--select * from #max_date_bank_wise
	--select * from #Applied_Vaulting
	--select * from limitdays where indent_date = '2019-02-14'
  /******************************************Calculate Loading amount************************************************************************************/
        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
         values ('Insert into #pre_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #pre_loading_amount Started'
	
     DROP TABLE IF exists #pre_loading_amount_dataset 	
	
	IF EXISTS
				(
					SELECT 1 FROM atm_revision_cash_availability
					WHERE project_id = @for_project_id
					AND bank_code = @for_bank_code
					AND feeder_branch_code = @for_feeder_branch
					AND cast(for_date as date)=  cast(@dateT as date)
					AND record_status = 'Active'
				)
				BEGIN
					SET @is_atm_list_available = 'Yes'
				END
				ELSE
				BEGIN 
					SET @is_atm_list_available = 'No'
				END


		SELECT 
			atm_id,
			site_code,
			sol_id,
			bank_code,
			project_id,
			indentdate,
			bank_cash_limit,
			insurance_limit,
			feeder_branch_code,
			CASE 
				WHEN CashOut = 1
				THEN 1
				WHEN atm_band = 'Platinum'
				THEN 2
				WHEN atm_band = 'Gold'
				THEN 3	
				WHEN atm_band = 'Silver'
				THEN 4
				ELSE 5
				END as atm_priority
			,atm_band
			,
				CASE 
				WHEN cashout = 1 THEN 'CashOut'
				WHEN atm_band in ('Platinum','Gold','Silver')
				THEN 'Band - ' + atm_band 
				Else
				'Not Specified' 
			END AS reason_for_priority,
			limit_amount,
			max_of_bankcash_insurancelimit, --Correction base limit
			base_limit,
			dispenseformula,
			decidelimitdays,
			loadinglimitdays,
			avgdispense,
			cassette_50_count,
			cassette_100_count,
			cassette_200_count,
			cassette_500_count,
			cassette_2000_count,
			capacity_50,
			capacity_100,
			capacity_200,
			capacity_500,
			capacity_2000,
			total_capacity_amount_50,
			total_capacity_amount_100,
			total_capacity_amount_200,
			total_capacity_amount_500,
			total_capacity_amount_2000,			
			total_capacity_amount_50 + total_capacity_amount_100 + total_capacity_amount_200 + total_capacity_amount_500 + total_capacity_amount_2000 as total_cassette_capacity,
			avgdecidelimit,
			DecideLimit,
			threshold_limit,			
			loadinglimit, 
			applied_vaulting_percentage,
			ignore_code,
			ignore_description, 
			dist_Purpose,
			defaultamt,	
			default_flag,		
			remaining_balance_50,
			remaining_balance_100,
			remaining_balance_200,
			remaining_balance_500,
			remaining_balance_2000,
			Morning_balance_T_minus_1,
			curbal_div_avgDisp,
			loadingGap_cur_bal_avg_disp,
			CashOut,		               
            case when loadingamount + Morning_balance_T_minus_1 > limit_amount AND loadingamount > 0
			 then 
			     FLOOR((limit_amount - Morning_balance_T_minus_1)/100000.0)*100000
			 else
			     loadingamount 
			 end as loadingamount,
			denomination_100_max_capacity_percentage,
			denomination_200_max_capacity_percentage,
			denomination_500_max_capacity_percentage,
			denomination_2000_max_capacity_percentage,
			cassette_50_count   as cassette_50_count_original  ,
			cassette_100_count  as cassette_100_count_original ,
			cassette_200_count  as cassette_200_count_original ,
			cassette_500_count  as cassette_500_count_original ,
			cassette_2000_count as cassette_2000_count_original,
			deno_100_priority,
			deno_200_priority,
			deno_500_priority,
			deno_2000_priority,
			is_50_available,
			is_100_available,
			is_200_available,
			is_500_available,
			is_2000_available,
			is_zero_dispense,
			feasibility_frequency,
			is_feasible_today,
			bank_decide_limit_days,
			bank_loading_limit_days,
			feeder_decide_limit_days,
			feeder_loading_limit_days,
			feasibility_decide_limit_days,
			feasibility_loading_limit_days,
			is_currency_chest,
			holiday_code,
			is_indent_required,
			withdrawal_type
		 
		 --drop table if exists #pre_loading_amount 
			INTO #pre_loading_amount_dataset
			FROM
			(
			Select 
			atm_id
			,site_code
			,sol_id
			,bank_code
			,project_id
			,indentdate
			,bank_cash_limit
			,insurance_limit
			,CASE 
                       WHEN  bank_cash_limit< insurance_limit and bank_cash_limit > 0
                           THEN bank_cash_limit
                       ELSE insurance_limit 
                   END	AS limit_amount
			,feeder_branch_code
			,atm_band
			,max_of_bankcash_insurancelimit
			,base_limit
			,dispenseformula
			,decidelimitdays
			,loadinglimitdays
			,avgdispense
			,cassette_50_count
			,cassette_100_count
			,cassette_200_count
			,cassette_500_count
			,cassette_2000_count
			,capacity_50
			,capacity_100
			,capacity_200
			,capacity_500
			,capacity_2000
			,total_capacity_amount_50 
			,total_capacity_amount_100 * denomination_100_max_capacity_percentage / 100 as total_capacity_amount_100
			,total_capacity_amount_200 * denomination_200_max_capacity_percentage / 100 as total_capacity_amount_200
			,total_capacity_amount_500 * denomination_500_max_capacity_percentage / 100 as total_capacity_amount_500
			,total_capacity_amount_2000 * denomination_2000_max_capacity_percentage / 100 as total_capacity_amount_2000
			--,total_cassette_capacity
			,avgdecidelimit
			,DecideLimit
			,threshold_limit
			,loadinglimit
			,applied_vaulting_percentage
			,ignore_code
			,ignore_description
			,dist_Purpose
			,defaultamt
			,case when Morning_balance_T_minus_1 is NULL then 1 
										else 0 end 
										as default_flag  -- Flag for default loading
			,remaining_balance_50
			,remaining_balance_100
			,remaining_balance_200
			,remaining_balance_500
			,remaining_balance_2000
			,Morning_balance_T_minus_1
			,curbal_div_avgDisp
			,loadingGap_cur_bal_avg_disp
			,CashOut			
      --      ,CASE when ignore_code IS NOT NULL THEN 0   -- calculating loading amount on ATM level 
      --         ELSE		     
			   --   CASE WHEN Morning_balance_T_minus_1 is NULL  then defaultamt
						--  ELSE
						--	(CASE 
						--		WHEN insurance_limit  < 
						--				CASE 
						--					WHEN  Morning_balance_T_minus_1 < DecideLimit
						--						THEN (loadinglimit-Morning_balance_T_minus_1) + 	
						--						 (  cast(@confidence_factor as float)/cast(100 as float))
						--															 * avgdispense 	
						--						ELSE  0 --DecideLimit
						--		 END 
						--		THEN insurance_limit 
						--	ELSE 
						--		CASE 
						--			WHEN  Morning_balance_T_minus_1 < DecideLimit
						--				THEN (loadinglimit- Morning_balance_T_minus_1)  + 
						--					 (  cast(@confidence_factor as float)/cast(100 as float))* avgdispense 													
						--                ELSE  0 --DecideLimit 
						--				END 
						--END ) END END  loadingamount ,
--              Correction loading Amount
            ,
					CASE 
						when ignore_code IS NOT NULL 
						THEN 0   -- calculating loading amount on ATM level 
						WHEN Morning_balance_T_minus_1 is NULL or Morning_balance_T_minus_1 = 0 
						then defaultamt
						WHEN is_zero_dispense = 1 AND Morning_balance_T_minus_1 > avgdispense * 1
						THEN 0 
						Else
							case 
								WHEN  Morning_balance_T_minus_1 < DecideLimit 
								THEN ceiling((loadinglimit- Morning_balance_T_minus_1  + ((  cast(@confidence_factor as float)/cast(100 as float))* avgdispense))*1.0/100000)*100000
							ELSE		     
								0 
							END
						END as loadingamount ,
						denomination_100_max_capacity_percentage,
						denomination_200_max_capacity_percentage,
						denomination_500_max_capacity_percentage,
						denomination_2000_max_capacity_percentage,
						deno_100_priority,
						deno_200_priority,
						deno_500_priority,
						deno_2000_priority,
						is_50_available,
						is_100_available,
						is_200_available,
						is_500_available,
						is_2000_available,
						is_zero_dispense,
						feasibility_frequency,
						is_feasible_today,
						bank_decide_limit_days,
						bank_loading_limit_days,
						feeder_decide_limit_days,
						feeder_loading_limit_days,
						feasibility_decide_limit_days,
						feasibility_loading_limit_days,
						is_currency_chest,
						holiday_code,
						is_indent_required,
						withdrawal_type

	 
            FROM 
              (
					SELECT b.*,
					AV.applied_vaulting_percentage,
				case    when qa.is_qualified = 'Disqualified'  then 20001  
						when fault_description is NOT NULL then 20003
						when OPS.feeder_branch_code is NOT NULL then 20002 						 
						else NULL
						end ignore_code,  -- putting code for disqualified, faulty,start/stop  cases

				case when qa.is_qualified = 'Disqualified'  then 'Disqualified'  
				when im.fault_description is not NULL then  im.fault_description 
				 else NULL
				end ignore_description,   

				case when eod.atm_id is not null then  'EOD' 
				      else NULL
					  end dist_Purpose,     

					  --Case when mb.Morning_balance_T_minus_1 is NULL then DLL.amount 														
					  --else 0 end  defaultamt,
					DLL.amount  as defaultamt,				
					mb.remaining_balance_50,
					mb.remaining_balance_100,
					mb.remaining_balance_200,
					mb.remaining_balance_500,
					mb.remaining_balance_2000,
					mb.total_remaining_balance as Morning_balance_T_minus_1,
					ROUND((CAST(mb.total_remaining_balance AS FLOAT)/ CAST(avgdispense  AS FLOAT) ),1) AS curbal_div_avgDisp,
					(decidelimitdays- (mb.total_remaining_balance/avgdispense)) * avgdispense AS loadingGap_cur_bal_avg_disp,
					CASE 
						WHEN mb.total_remaining_balance < base_limit--10000 
						THEN 1
						ELSE 0 
					END CashOut
					 

					FROM 
					(
						SELECT a.*,avgdispense*decidelimitdays AS avgdecidelimit, 
						--Correction Decide limit
						--CASE 
						--	WHEN (
						--		CASE 
						--			WHEN base_limit > (avgdispense*decidelimitdays) 
						--				THEN base_limit
						--				ELSE  avgdispense*decidelimitdays END) > total_cassette_capacity 
						--		THEN  total_cassette_capacity
						--		ELSE 
						--		( CASE 
						--			WHEN base_limit > (avgdispense*decidelimitdays) 
						--			THEN base_limit
						--			ELSE  avgdispense*decidelimitdays END)
						--	END DecideLimit, 

							(select min (v) from (values
									(ceiling(case when (avgdispense * decidelimitdays) > base_limit then
								case when total_cassette_capacity < (avgdispense * decidelimitdays) then
									total_cassette_capacity
									else
										(avgdispense * decidelimitdays)
								end
								else
									base_limit
							end * 1.0 / 100000)*100000),
							(insurance_limit)) As i(v))
							as
							 DecideLimit,
					
					 avgdispense*loadinglimitdays  AS threshold_limit ,
					 --Correction loading limit
					 --CASE 
						--WHEN (
						--	 CASE 
						--		WHEN  base_limit >  (avgdispense*loadinglimitdays)    
						--			THEN  base_limit 
						--			ELSE  avgdispense*loadinglimitdays   
						--		END ) > total_cassette_capacity 
						--	THEN total_cassette_capacity
						--		ELSE  
						--			( CASE 
						--				WHEN  base_limit >  (avgdispense*loadinglimitdays)    
						--					THEN  base_limit 
						--					ELSE  avgdispense*loadinglimitdays   
						--				END )
					 --	  END loadinglimit					
					 
					 (select min (v) from (values
									(ceiling(case when (avgdispense * loadinglimitdays) > base_limit then
								case when total_cassette_capacity < (avgdispense * loadinglimitdays) then
									total_cassette_capacity
									else
										(avgdispense * loadinglimitdays)
								end
								else
									base_limit
							end * 1.0 / 100000)*100000),
							(insurance_limit)) As i(v))
							as
					  loadinglimit
					  
 					  FROM  
					  (    
					  SELECT mast.atm_id,mast.site_code,
							 sol_id,
							 mast.bank_code,
							 mast.project_id,
							 @dateT AS indentdate,
							 clm.bank_cash_limit,
							 ISNULL(clm.insurance_limit,5000000) as insurance_limit ,
							 cra.feeder_branch_code as feeder_branch_code,
							 mast.atm_band,
							 CASE 
								WHEN clm.bank_cash_limit <  ISNULL(clm.insurance_limit,5000000) 
									THEN clm.bank_cash_limit
									ELSE ISNULL(clm.insurance_limit,5000000)
								END AS  max_of_bankcash_insurancelimit,
							 clm.base_limit,
							 @dispenseformula AS dispenseformula ,

							 CASE 
								WHEN feasibility_frequency = 1 AND is_feasible_today <> 'Yes'
								THEN COALESCE(fld.decide_limit_days,bld.decide_limit_days)
								ELSE
									--(SELECT MAX(v) FROM (VALUES (dl.decidelimitdays), 
									--			COALESCE(fld.decide_limit_days,bld.decide_limit_days)
									--			) AS value(v)) 
									CASE WHEN fld.decide_limit_days IS NOT NULL THEN 
											   (SELECT MAX(v) FROM (VALUES (dl.decidelimitdays), 
												(fld.decide_limit_days)
												) AS value(v)) 
										 WHEN fld.decide_limit_days IS NULL THEN 
											   (SELECT MAX(v) FROM (VALUES (dl.decidelimitdays), 
												(bld.decide_limit_days)
												) AS value(v))
										END
									END	as decidelimitdays,
							 CASE 
								WHEN feasibility_frequency = 1 
								THEN 7
								ELSE
									CASE WHEN fld.loading_limit_days IS NOT NULL THEN 
											   (SELECT MAX(v) FROM (VALUES (dl.loadinglimitdays), 
												(fld.loading_limit_days)
												) AS value(v)) 
										 WHEN fld.loading_limit_days IS NULL THEN 
											   (SELECT MAX(v) FROM (VALUES (dl.loadinglimitdays), 
												(bld.loading_limit_days)
												) AS value(v))
										END
										END	as loadinglimitdays,
										bld.decide_limit_days as  bank_decide_limit_days,
										bld.loading_limit_days as bank_loading_limit_days,
										fld.decide_limit_days as  feeder_decide_limit_days,
										fld.loading_limit_days as feeder_loading_limit_days,
										dl.decidelimitdays as   feasibility_decide_limit_days,
										dl.loadinglimitdays as  feasibility_loading_limit_days,
							--(SELECT MAX(v) FROM (VALUES (dl.loadinglimitdays), 
							--(fld.loading_limit_days), 
							--(bld.loading_limit_days)
							--) AS value(v)) as loadinglimitdays,
							 --dl.[decidelimitdays],
							 --dl.loadinglimitdays,
							COALESCE( CASE 
								WHEN @dispenseformula='max_of_max'    
									THEN cd.max_of_four_months_max		
								WHEN @dispenseformula='avg_of_max'    
									THEN cd.avg_of_four_months_max
								WHEN @dispenseformula='max_of_avg'    
									THEN cd.max_of_four_months_avg  
								WHEN @dispenseformula='avg_of_avg'    
									THEN cd.avg_of_four_months_avg  
								WHEN @dispenseformula='max_of_secondmax'    
									THEN cd.max_of_four_months_second_max
								WHEN @dispenseformula='avg_of_secondmax'    
									THEN cd.avg_of_four_months_second_max
								WHEN @dispenseformula='max_of_thirdmax'    
									THEN cd.max_of_four_months_third_max
								WHEN @dispenseformula='avg_of_thirdmax'    
									THEN cd.avg_of_four_months_third_max
								WHEN @dispenseformula='_28days_or_T2dispense'    
									THEN  cd.max_28_days_or_2_days
							  -----9-Jan-2019 Added as per requirement for dispense calculation
								WHEN @dispenseformula='max_of_avg_current'
								    THEN  cd.max_of_current
							END,  @default_avg_dispense) avgdispense
					          , COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) AS cassette_50_count
					         , COALESCE(cc.cassette_100_count, clm.cassette_100_count,0) AS cassette_100_count
					         , COALESCE(cc.cassette_200_count, clm.cassette_200_count,0) AS cassette_200_count
					         , COALESCE(cc.cassette_500_count, clm.cassette_500_count,0) AS cassette_500_count
					         , COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0) AS cassette_2000_count  
						     ,COALESCE(bc.capacity_50,2000) as capacity_50
						     ,COALESCE(bc.capacity_100, sa.deno_100_bill_capacity) as capacity_100
						     ,COALESCE(bc.capacity_200,sa.deno_200_bill_capacity) as capacity_200
						     ,COALESCE(bc.capacity_500,sa.deno_500_bill_capacity) as capacity_500
						     ,COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity) as capacity_2000
						     ,COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) * COALESCE(bc.capacity_50,2000) *50 AS total_capacity_amount_50
						     ,COALESCE(cc.cassette_100_count, clm.cassette_100_count,0) * COALESCE(bc.capacity_100,sa.deno_100_bill_capacity) *100 AS total_capacity_amount_100
						     ,COALESCE(cc.cassette_200_count, clm.cassette_200_count,0) * COALESCE(bc.capacity_200,sa.deno_200_bill_capacity) *200 AS total_capacity_amount_200
						     ,COALESCE(cc.cassette_500_count, clm.cassette_500_count,0) *COALESCE(bc.capacity_500,sa.deno_500_bill_capacity)*500 AS total_capacity_amount_500
						     ,COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0) * COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity) *2000 AS total_capacity_amount_2000
						     
						     ,(COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) * COALESCE(bc.capacity_50,2000) *50) +
					          (COALESCE(cc.cassette_100_count, clm.cassette_100_count,0) *COALESCE(bc.capacity_100,sa.deno_100_bill_capacity)  *100) +
						      (COALESCE(cc.cassette_200_count, clm.cassette_200_count,0) *COALESCE(bc.capacity_200,sa.deno_200_bill_capacity) *200)+	   
						      (COALESCE(cc.cassette_500_count, clm.cassette_500_count,0) * COALESCE(bc.capacity_500,sa.deno_500_bill_capacity) *500) +
						      (COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0) * COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity) *2000)
							  AS total_cassette_capacity,
							  COALESCE(max_cap.denomination_100,sa.deno_100_max_capacity_percentage) as   denomination_100_max_capacity_percentage,
							  COALESCE(max_cap.denomination_200,sa.deno_200_max_capacity_percentage) as   denomination_200_max_capacity_percentage,
							  COALESCE(max_cap.denomination_500,sa.deno_500_max_capacity_percentage) as   denomination_500_max_capacity_percentage,
							  COALESCE(max_cap.denomination_2000,sa.deno_2000_max_capacity_percentage) as denomination_2000_max_capacity_percentage,
							  COALESCE(fdp.denomination_100,sa.deno_100_priority) as   deno_100_priority,
							  COALESCE(fdp.denomination_200,sa.deno_200_priority) as   deno_200_priority,
							  COALESCE(fdp.denomination_500,sa.deno_500_priority) as   deno_500_priority,
							  COALESCE(fdp.denomination_2000,sa.deno_2000_priority) as deno_2000_priority,
							  deno.is_50_available as is_50_available,
							  deno.is_100_available as is_100_available,
							  deno.is_200_available as is_200_available,
							  deno.is_500_available as is_500_available,
							  deno.is_2000_available as  is_2000_available,
							  CASE WHEN zero.total_dispense_amount = 0 then 1 Else 0 end as is_zero_dispense,
							  feasibility_frequency,
							  is_feasible_today,
							  is_currency_chest,
							  holiday_code,
						  	  is_indent_required,
							  withdrawal_type
								  --Select fields from deno.


							  --DECLARE @dateT DATETIME
							  --SET @dateT ='2018-11-11'
							  
							  --SELECT mast.* 
							  FROM  atm_master AS  mast
							  inner JOIN		ATM_Config_limits clm
							  ON			mast.atm_id = clm.atm_id  AND
											mast.site_code = clm.site_code AND
											clm.record_status = 'Active'
							  AND mast.record_status = 'Active'
							  and mast.site_status = 'Active'
							  inner JOIN cra_feasibility cra on
								mast.site_code = cra.site_code
								AND mast.atm_id = cra.atm_id AND
								cra.record_status = 'Active'

							  LEFT JOIN 	#calculated_avg_dispense cd
								ON mast.atm_id=cd.atm_id AND 
								mast.project_id = cd.project_id AND
								mast.bank_code = cd.bank_code 
							  
							  LEFT JOIN		limitdays dl
									  ON mast.atm_id=dl.atm_id AND 
									mast.bank_code=dl.bank_code AND 
									mast.project_id=dl.project_id AND
									CAST(dl.indent_date as date) = cast(@dateT as date) AND
									dl.record_status = 'Active'	
							  
							   LEFT JOIN [feeder_limit_days] fld
											on cra.bank_code = fld.bank_code
											AND cra.project_id = fld.project_id
											AND cra.feeder_branch_code = fld.feeder
											AND CAST(fld.fordate as date) = cast(@dateT as date)
											AND fld.record_status = 'Active'	


								LEFT JOIN bank_limit_days bld
								on mast.bank_code = bld.bank_code
								AND mast.project_id = bld.project_id
								AND CAST(bld.fordate as date) = cast(@dateT as date)
								AND bld.record_status = 'Active'	


							  LEFT JOIN		
										feeder_cassette_max_capacity_percentage max_cap
										ON	max_cap.bank_code = mast.bank_code AND
											max_cap.project_id = mast.project_id AND
											max_cap.feeder_branch_code = cra.feeder_branch_code
											AND max_cap.record_status = 'Active'							 
							  LEFT JOIN   feeder_branch_master fb on
											cra.bank_code = fb.bank_code
											AND cra.feeder_branch_code = fb.feeder_branch 
											AND fb.project_id = cra.project_id 
											AND fb.record_status = 'Active'

							 --LEFT JOIN atm_list_for_revision list
								--	on list.atm_id = mast.atm_id
								--	AND list.site_code = mast.site_code
								--	AND CAST(list.for_date as date)= cast(@dateT as date)
								--	and list.record_status = 'Active'


							  LEFT JOIN [feeder_denomination_priority] fdp on
											 fdp.bank_code = mast.bank_code AND
											 fdp.feeder_branch_code = cra.feeder_branch_code AND
											 fdp.project_id = mast.project_id
											 and fdp.record_status = 'Active'

							LEFT JOIN feeder_denomination_pre_availability deno on
									deno.project_id = mast.project_id
									and deno.feeder_branch_code = cra.feeder_branch_code
									and deno.record_status = 'Active'
									and (@dateT between deno.from_date  and deno.to_date)

							LEFT JOIN zero_cash_dispense zero
							on mast.atm_id = zero.atm_id
							AND mast.site_code = zero.site_code
							AND CAST(zero.datafor_date_time as date) = cast(@dateT as date)
							AND zero.record_status = 'Active'
							 CROSS JOIN system_settings sa

							  LEFT JOIN      
							       (
							       SELECT 
										project_id,
										bank_code, 
										site_code,
										atm_id, 
										cassette_50_count, 
										cassette_100_count, 
										cassette_200_count,
										cassette_500_count,
										cassette_2000_count 
									FROM Modify_cassette_pre_config 
									WHERE @dateT between from_date AND to_date
							        AND record_status = 'Active'
							       )cc
							  ON clm.site_code = cc.site_code AND 
								 clm.atm_id = cc.atm_id
								 LEFT JOIN Brand_Bill_Capacity bc ON 
										   bc.brand_code = mast.brand 
								           AND bc.record_status = 'Active'

								left join indent_holiday ih
									on cra.feeder_branch_code = ih.Feeder_branch
									and mast.bank_code = ih.Bank_Code
									and mast.project_id = ih.Project_id
									and ih.record_status = 'Active'
									and ih.holiday_date = @dateT
									--and ih.is_indent_required = 'Yes'


									WHERE mast.record_status = 'Active'

								
							       )a
					 )b 

					LEFT JOIN #morning_balance mb
					ON mb.bank_name = b.bank_code
					AND mb.atm_id = b.atm_id

					LEFT Join #Applied_Vaulting AV
					on  b.atm_id=AV.atm_id
					and b.project_id=AV.project_id
					and b.bank_code= AV.bank_code

					LEFT join Indent_Pre_Qualify_ATM  QA
					 on   b.atm_id=QA.atm_id
					 and b.site_code = QA.site_code					 
					 and   QA.record_status='Active'					 
					 and @dateT between QA.from_date and QA.to_date

					LEFT join	(
								SELECT atmid,site_code,
										STUFF((
											  SELECT ',' + T.fault_description
											  FROM dbo.ims_master T
											  WHERE A.atmid = T.atmid
											  AND CAST(T.datafor_date_time as date) = (SELECT CAST(MAX(datafor_date_time) as DATE) FROM ims_master where record_status = 'Active')
											  FOR XML PATH('')), 1, 1, '') as fault_description 
										 from ims_master A
								where fault_description not in ('Any Cassette Faulty')
								AND CAST(datafor_date_time as date) = (SELECT CAST(MAX(datafor_date_time) as DATE) FROM ims_master where record_status = 'Active')
								and record_status='Active'
								group by atmid,site_code
								
								) im
					on b.atm_id=im.atmid  and b.site_code = im.site_code					

				    LEFT join [indent_eod_pre_activity] EOD
					on b.atm_id=EOD.atm_id
					and b.bank_code=EOD.bank_code
					and b.project_id=EOD.project_id
					and EOD.record_status='Active'

					left join ops_stop_batch OPS
					on b.feeder_branch_code=OPS.feeder_branch_code
					and  b.project_id=OPS.project_id
					and b.bank_code=OPS.bank_code
					and OPS.record_status='Active'
					and @dateT between cast(OPS.from_date as date) and cast(OPS.to_date as date)

					left join Default_loading DLL
					on b.project_id=DLL.project_id
					and b.bank_code=DLL.bank_code
					and DLL.record_status='Active'
			
				) c
								 
		            ) d
					where bank_code in ('BOMH','SBI')
					and 
					(project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
					and 
					(bank_code IN 
					( SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank_code else  @for_bank_code end ,',')))
					and 
					(feeder_branch_code = case when ( @for_feeder_branch = 'All') then feeder_branch_code else  @for_feeder_branch   end)

		DROP TABLE IF EXISTS #pre_loading_amount_2
		SELECT * INTO #pre_loading_amount_2 
		FROM #pre_loading_amount_dataset
		WHERE ( (is_indent_required='Yes') or (is_indent_required is null) )

		--select * from #pre_loading_amount_2
		DROP TABLE IF Exists #pre_loading_amount
		
		SELECT *,
				0 as is_atm_list_available,
				0 as denomination_100_list,
				0 as denomination_200_list,
				0 as denomination_500_list,
				0 as denomination_2000_list,
				0 as total_amount_list
		INTO #pre_loading_amount	
		FROM #pre_loading_amount_2 
		WHERE 1=2


		--SELECT * FROM #pre_loading_amount

		--DROP TABLE IF EXISTS #pre_loading_amount

		IF @is_atm_list_available = 'Yes'
		BEGIN
			--INSERT INTO #pre_loading_amount
			--SELECT  atm_id
			--	,   site_code
			--	,   sol_id
			--	,	bank_code
			--	,	project_id
			--	,	indentdate
			--	,	bank_cash_limit
			--	,	insurance_limit
			--	,	feeder_branch_code
			--	,	atm_priority
			--	,	atm_band
			--	,	reason_for_priority
			--	,	limit_amount
			--	,	max_of_bankcash_insurancelimit
			--	,	base_limit
			--	,	dispenseformula
			--	,	decidelimitdays
			--	,	load
			
			

			INSERT INTO #pre_loading_amount
			SELECT load2.*,
					CASE WHEN 
						list.atm_id IS NULL THEN 0 
						ELSE 1
						END 
					,	CASE WHEN 
						list.atm_id IS NULL THEN 0 
						ELSE
						ISNULL(list.denomination_100,0)
						END
					,	CASE WHEN 
						list.atm_id IS NULL THEN 0 
						ELSE
						ISNULL(list.denomination_200,0)
						END
					,	CASE WHEN 
						list.atm_id IS NULL THEN 0 
						ELSE
						ISNULL(list.denomination_500,0)
						END
					,	CASE WHEN 
						list.atm_id IS NULL THEN 0 
						ELSE
						ISNULL(list.denomination_2000,0)
						END
					,	CASE WHEN 
						list.atm_id IS NULL THEN 0 
						ELSE
						ISNULL(list.total_amount,0)
						END

			FROM #pre_loading_amount_2 load2
			LEFT JOIN atm_revision_cash_availability list
			on list.atm_id = load2.atm_id
			AND list.site_code = load2.site_code
			AND list.record_status = 'Active'
			AND CAST(list.for_date as date) = CAST( '2019-04-29' as date)

		--	select * from #pre_loading_amount
			
			UPDATE #pre_loading_amount
			SET 
				loadingamount = 
					CASE WHEN is_atm_list_available = 0 
					THEN 0 
					ELSE	
						CASE WHEN
							total_amount_list = 0
								THEN loadingamount
							ELSE
								CASE WHEN 
									total_amount_list <= loadingamount
									THEN total_amount_list
									ELSE
									loadingamount
								END			
						END
				END
		END
		ELSE
		BEGIN
			INSERT INTO #pre_loading_amount
			SELECT *,0,0,0,0,0,0 FROM #pre_loading_amount_2
		END
		
		--SELECT * FROM #pre_loading_amount
	
	
		-- where feeder_branch_code = 'KOLHAPUR'


	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Fetching data from tables and insert into #pre_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	  
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Fetching data from tables and insert into #pre_loading_amount Completed'
	

 /****************************  Rounding of loading amount /negativebalance  ************************************/



	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Start prepare dataset  to handle negative amount, Insert into #loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Start prepare dataset  to handle negative amount, Insert into #loading_amount Started'

	
	
	/* For Testing uncomment the below part */


	--DECLARE @MaxDestinationDate DATE
	--	DECLARE @dispenseformula NVARCHAR(50)
	--	DECLARE @confidence_factor NVARCHAR(50)
	--	DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
	--	DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
	--	DECLARE @denomination_wise_round_off_500 int
	--	DECLARE @denomination_wise_round_off_2000 int
	--	DECLARE @denomination_wise_round_off_100 INT
	--	declare @default_avg_dispense int
	--	declare @vaulting_for_normal_weekday_percentage float,
	--			@vaulting_for_normal_weekend_percentage float,
	--			@vaulting_for_extended_weekend_percentage float
		
		
	--	Select	@confidence_factor = confidence_factor
	--	, @dispenseformula = dispenseformula
	--	, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
	--	, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
	--	, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
	--	, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
	--	,@default_avg_dispense = default_average_dispense
	--	, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
	--	, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
	--	, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
	--	 from system_settings where record_status = 'Active'
	
	
	DROP TABLE IF exists #loading_amount   
	DROP TABLE IF exists #loading_amount_1
	SELECT [atm_id],
			site_code,
			sol_id,
			bank_code,
			project_id,
			indentdate,
			bank_cash_limit,
			insurance_limit	,
			feeder_branch_code, 
			atm_priority,
			atm_band,
			reason_for_priority,
			base_limit,
			dispenseformula,
			 decidelimitdays,
			 loadinglimitdays,
			 avgdispense
			,COALESCE([loadingamount],0)				AS loading_amount
			,COALESCE(remaining_balance_50,0)			AS [morning_balance_50]
			,COALESCE(remaining_balance_100,0)			AS morning_balance_100
			,COALESCE(remaining_balance_200,0)			AS morning_balance_200
			,COALESCE(remaining_balance_500,0)			AS morning_balance_500
			,COALESCE(remaining_balance_2000,0)			AS morning_balance_2000
			,COALESCE(morning_balance_T_minus_1,0)		AS total_morning_balance
			,COALESCE(capacity_50,0)					AS cassette_50_brand_capacity
			,COALESCE(capacity_100,0)					AS cassette_100_brand_capacity
			,COALESCE(capacity_200,0)					AS cassette_200_brand_capacity
			,COALESCE(capacity_500,0)					AS cassette_500_brand_capacity
			,COALESCE(capacity_2000,0)					AS cassette_2000_brand_capacity
			,COALESCE(cassette_50_count ,0)				AS cassette_50_count
			,COALESCE(cassette_100_count,0)				AS cassette_100_count
			,COALESCE(cassette_200_count,0)				AS cassette_200_count
			,COALESCE(cassette_500_count,0)				AS cassette_500_count
			,COALESCE(cassette_2000_count,0)			AS cassette_2000_count 
			,COALESCE(limit_amount,0)					AS limit_amount 
			,total_capacity_amount_50
			,total_capacity_amount_100
			,total_capacity_amount_200
			,total_capacity_amount_500
			,total_capacity_amount_2000
			,total_cassette_capacity
			,COALESCE(total_capacity_amount_50 * 100.0 / NULLIF(total_cassette_capacity,0),0)     AS cassette_capacity_percentage_50
			,COALESCE(total_capacity_amount_100 *  denomination_100_max_capacity_percentage / NULLIF(total_cassette_capacity,0),0)   AS cassette_capacity_percentage_100
			,COALESCE(total_capacity_amount_200 *  denomination_200_max_capacity_percentage / NULLIF(total_cassette_capacity,0),0)   AS cassette_capacity_percentage_200
			,COALESCE(total_capacity_amount_500 *  denomination_500_max_capacity_percentage / NULLIF(total_cassette_capacity,0),0)   AS cassette_capacity_percentage_500
			,COALESCE(total_capacity_amount_2000 * denomination_2000_max_capacity_percentage / NULLIF(total_cassette_capacity,0),0)  AS cassette_capacity_percentage_2000
			,avgdecidelimit
			,DecideLimit
			,threshold_limit
			,loadinglimit
			,applied_vaulting_percentage
			,ignore_code
			,ignore_description 
			,dist_Purpose
			,defaultamt
			,default_flag
			,curbal_div_avgDisp
			,loadingGap_cur_bal_avg_disp
			,CashOut
			,remaining_balance_50
			,remaining_balance_100
			,remaining_balance_200
			,remaining_balance_500
			,remaining_balance_2000
			,denomination_100_max_capacity_percentage
			,denomination_200_max_capacity_percentage
			,denomination_500_max_capacity_percentage
			,denomination_2000_max_capacity_percentage
			,cassette_50_count_original  
			,cassette_100_count_original 
			,cassette_200_count_original 
			,cassette_500_count_original 
			,cassette_2000_count_original,
			deno_100_priority,
			deno_200_priority,
			deno_500_priority,
			deno_2000_priority,
			is_100_available,
			is_200_available,
			is_500_available,
			is_2000_available,
			is_zero_dispense,
			feasibility_frequency,
			is_feasible_today,
			bank_decide_limit_days,
			bank_loading_limit_days,
			feeder_decide_limit_days,
			feeder_loading_limit_days,
			feasibility_decide_limit_days,
			feasibility_loading_limit_days,
			is_currency_chest,
			holiday_code,
			is_indent_required,
			withdrawal_type,
			0 as variation_amount
			-- For correction of distribution amount  --change
			,
			CASE WHEN 
				 is_100_available = 'NO' then 0 --or is_100_available is not null
				 when
				 		floor((total_capacity_amount_100-COALESCE(remaining_balance_100,0))*1.0/@denomination_wise_round_off_100)*@denomination_wise_round_off_100 > 0 
				 		then 
				 	    floor((total_capacity_amount_100-COALESCE(remaining_balance_100,0))*1.0/@denomination_wise_round_off_100)*@denomination_wise_round_off_100
				 		else
				 		0 
					--	END 
				 --ELSE
				 --0 
				 end as max_loading_capacity_amount_100
			,
			CASE WHEN 
				 is_200_available = 'NO' then 0 --or is_200_available is not null
				 --THEN
				 --case when 
				 when floor((total_capacity_amount_200-COALESCE(remaining_balance_200,0))*1.0/@denomination_wise_round_off_200)*@denomination_wise_round_off_200 > 0 then 
				  floor((total_capacity_amount_200-COALESCE(remaining_balance_200,0))*1.0/@denomination_wise_round_off_200)*@denomination_wise_round_off_200
				-- else
				-- 0 
				--END
			ELSE
			0 end as max_loading_capacity_amount_200
			, 
			CASE WHEN 
				 is_500_available = 'NO' then 0 --or is_500_available is not null
				 --THEN
				 --case when 
				 when floor((total_capacity_amount_500-COALESCE(remaining_balance_500,0))*1.0/@denomination_wise_round_off_500)*@denomination_wise_round_off_500 > 0 then 
				 floor((total_capacity_amount_500-COALESCE(remaining_balance_500,0))*1.0/@denomination_wise_round_off_500)*@denomination_wise_round_off_500
				-- else
				-- 0 
				--END
				ELSE 
			0 end as max_loading_capacity_amount_500
			, 
			CASE WHEN 
				 is_2000_available = 'NO' then 0-- or is_2000_available is not null 
				 --THEN 
				 --case when 
				 when floor((total_capacity_amount_2000-COALESCE(remaining_balance_2000,0))*1.0/@denomination_wise_round_off_2000)*@denomination_wise_round_off_2000 > 0 then 
				 floor((total_capacity_amount_2000-COALESCE(remaining_balance_2000,0))*1.0/@denomination_wise_round_off_2000)*@denomination_wise_round_off_2000
				--else
				--0 end
			ELSE
			0 end as max_loading_capacity_amount_2000
			,0 as total_loading_capacity_amount
			,loadingamount as loading_amount_original
			,is_atm_list_available
			,denomination_100_list
			,denomination_200_list
			,denomination_500_list
			,denomination_2000_list
			,total_amount_list
			INTO #loading_amount_1
			FROM #pre_loading_amount
			-- Update total capacity
			update #loading_amount_1 set total_loading_capacity_amount = max_loading_capacity_amount_100 + max_loading_capacity_amount_200 + max_loading_capacity_amount_500 + max_loading_capacity_amount_2000
			--If loading amount is less than capacity then reduce loading amount
			update #loading_amount_1 set loading_amount = total_loading_capacity_amount where loading_amount > total_loading_capacity_amount

	
			
			SELECT *
					,max_loading_capacity_amount_100 as remaining_capacity_amount_100
					,max_loading_capacity_amount_200 as remaining_capacity_amount_200
					,max_loading_capacity_amount_500 as remaining_capacity_amount_500
					,max_loading_capacity_amount_2000 as remaining_capacity_amount_2000	
					,total_loading_capacity_amount as total_remaining_capacity_amount
			INTO #loading_amount
			FROM #loading_amount_1

			--select * from #loading_amount where feeder_branch_code = 'KARAD'
			drop table if exists #priority_1_loading
			drop table if exists #priority_2_loading
			drop table if exists #priority_3_loading
			drop table if exists #priority_4_loading
			------------------------------------------------------------------------------------------------------------------------------
			--revised logic
			----priority 1
		--	drop table if exists #priority_1_loading
			
		--DECLARE @MaxDestinationDate DATE
		--DECLARE @dispenseformula NVARCHAR(50)
		--DECLARE @confidence_factor NVARCHAR(50)
		--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		--DECLARE @denomination_wise_round_off_100 INT			 -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_200 int      
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100_currency_chest int      
		--DECLARE @denomination_wise_round_off_200_currency_chest int
		--DECLARE @denomination_wise_round_off_500_currency_chest int
		--DECLARE @denomination_wise_round_off_2000_currency_chest INT
		--declare @default_avg_dispense int
		--declare @vaulting_for_normal_weekday_percentage float,
		--		@vaulting_for_normal_weekend_percentage float,
		--		@vaulting_for_extended_weekend_percentage float,
		--		@is_atm_list_available VARCHAR(10)

		--SET @is_atm_list_available = 'Yes'
		
		
		--Select	@confidence_factor = confidence_factor
		--, @dispenseformula = dispenseformula
		--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		--, @denomination_wise_round_off_100_currency_chest = denomination_wise_round_off_100_currency_chest
		--, @denomination_wise_round_off_200_currency_chest = denomination_wise_round_off_200_currency_chest
		--, @denomination_wise_round_off_500_currency_chest = denomination_wise_round_off_500_currency_chest
		--, @denomination_wise_round_off_2000_currency_chest = denomination_wise_round_off_2000_currency_chest
		--,@default_avg_dispense = default_average_dispense
		--, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
		--, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
		--, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
		-- from system_settings where record_status = 'Active'
			select	*
					, (isnull(loading_amount_100_p1,0) + isnull(loading_amount_200_p1,0) + isnull(loading_amount_500_p1,0) + isnull(loading_amount_2000_p1,0) ) priority_1_loading_amount
					, loading_amount - (isnull(loading_amount_100_p1,0) + isnull(loading_amount_200_p1,0) + isnull(loading_amount_500_p1,0) + isnull(loading_amount_2000_p1,0) ) priority_1_remaining_loading_amount
				into #priority_1_loading
			from
			(
				select	*
				,case when max_loading_capacity_amount_100 >0 then COALESCE(max_loading_capacity_amount_100 * 100.0 /isnull(total_loading_capacity_amount,0) ,0) else 0 end  AS max_loading_capacity_percentage_100
				,case when max_loading_capacity_amount_200 >0 then COALESCE(max_loading_capacity_amount_200 * 100.0 /isnull(total_loading_capacity_amount,0) ,0) else 0 end  AS max_loading_capacity_percentage_200
				,case when max_loading_capacity_amount_500 >0 then COALESCE(max_loading_capacity_amount_500 * 100.0 /isnull(total_loading_capacity_amount,0) ,0) else 0 end  AS max_loading_capacity_percentage_500
				,case when max_loading_capacity_amount_2000 >0 then COALESCE(max_loading_capacity_amount_2000 * 100.0 /isnull(total_loading_capacity_amount,0) ,0) else 0 end AS max_loading_capacity_percentage_2000
					--,deno_100_priority
					,case when deno_100_priority = 1 then 
						case when loading_amount >= max_loading_capacity_amount_100 then
							max_loading_capacity_amount_100  
						else
							case when cast(loading_amount as int) % @denomination_wise_round_off_100 = 0 
							then
								loading_amount
							else
								--loading_amount is not as per rounding.
								--If ceiling(loading_amount,rounding_off_100) <  max_loading_capacity_amount_100  
								--and 
								--( total_previous_loading + total_current_loading + total_morning_balance < limit_amount) 
								--then								
								--			ceiling(loading_amount,rounding_off_100)
								--		else
								--			floor(loading_amount,rounding_off_100)
								
								case when (CEILING( CAST(loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100) < max_loading_capacity_amount_100 
										and  
										(CEILING(CAST(loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100) + total_morning_balance < limit_amount 
									then
										CEILING( CAST(loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100
									else
										case when floor(CAST(loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100 < 0 
										then
											0
										else
											floor(CAST(loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100
										end 
									end
							end
						end 
					end as loading_amount_100_p1
			
					--,deno_200_priority
					,case when deno_200_priority = 1 then 
						case when loading_amount >= max_loading_capacity_amount_200 then
							max_loading_capacity_amount_200  
						else
							case when cast(loading_amount as int) % @denomination_wise_round_off_200 = 0 then
								loading_amount
							else
						case when (CEILING(CAST(loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200) < max_loading_capacity_amount_200 
										and  
										(CEILING(CAST(loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200) + total_morning_balance < limit_amount 
									then
										CEILING(CAST(loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200
									else
										CASE WHEN floor(CAST(loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200 < 0
										THEN
											0
										ELSE 
											floor(CAST(loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200
										END
									end
								end
						--	loading_amount
						end 
					end as loading_amount_200_p1

					--,deno_500_priority
					,case when deno_500_priority = 1 then 
						case when loading_amount >= max_loading_capacity_amount_500 then
							max_loading_capacity_amount_500  
						else
						case when cast(loading_amount as int) % @denomination_wise_round_off_500 = 0 then
								loading_amount
							else
							case when (CEILING(CAST(loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500) < max_loading_capacity_amount_500 
										and  
										(CEILING(CAST(loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500) + total_morning_balance < limit_amount 
									then
										CEILING(CAST(loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500
									else
										
										CASE WHEN floor(CAST(loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500 < 0
										THEN 
											0
										ELSE 
											floor(CAST(loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500
										END
									end
							--loading_amount
							END
						end 					
					end as loading_amount_500_p1

					--,deno_2000_priority
					,case when deno_2000_priority = 1 then 
						case when loading_amount >= max_loading_capacity_amount_2000 then
							max_loading_capacity_amount_2000  
						else
							case when cast(loading_amount as int) % @denomination_wise_round_off_2000 = 0 then
								loading_amount
							else
						case when (CEILING(CAST(loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000) < max_loading_capacity_amount_2000 
										and  
										(CEILING(CAST(loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000) + total_morning_balance < limit_amount 
									then
										CEILING(CAST(loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000
									else
										CASE WHEN floor(CAST(loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000 < 0
										THEN 
											0
										ELSE
											floor(CAST(loading_amount / @denomination_wise_round_off_2000 AS INT)* 1.0) * @denomination_wise_round_off_2000
										END
									end
						--	loading_amount
						END
						end 					
					end as loading_amount_2000_p1
			from #loading_amount
			)priority_1_loading

			--select * from #priority_1_loading

			UPDATE #priority_1_loading 
			SET remaining_capacity_amount_100 = remaining_capacity_amount_100 - ISNULL(loading_amount_100_p1,0),
				remaining_capacity_amount_200 = remaining_capacity_amount_200 - ISNULL(loading_amount_200_p1,0),
				remaining_capacity_amount_500 = remaining_capacity_amount_500 - ISNULL(loading_amount_500_p1,0),
				remaining_capacity_amount_2000 = remaining_capacity_amount_2000 - ISNULL(loading_amount_2000_p1,0),
				total_remaining_capacity_amount = total_remaining_capacity_amount - 
												(ISNULL(loading_amount_100_p1,0)	+ 
												ISNULL(loading_amount_200_p1,0) + 
												ISNULL(loading_amount_500_p1,0) + 
												ISNULL(loading_amount_2000_p1,0))

			--For priority 2
			drop table if exists #priority_2_loading

			select	*
					, (isnull(loading_amount_100_p2,0) + isnull(loading_amount_200_p2,0) + isnull(loading_amount_500_p2,0) + isnull(loading_amount_2000_p2,0) ) priority_2_loading_amount
					, priority_1_remaining_loading_amount - (isnull(loading_amount_100_p2,0) + isnull(loading_amount_200_p2,0) + isnull(loading_amount_500_p2,0) + isnull(loading_amount_2000_p2,0) ) priority_2_remaining_loading_amount
				into #priority_2_loading
			from
			(
				select	*
						,case when deno_100_priority = 2 then 
							case when priority_1_remaining_loading_amount >= max_loading_capacity_amount_100 then
								max_loading_capacity_amount_100  
							else
							case when cast(priority_1_remaining_loading_amount as int) % @denomination_wise_round_off_100 = 0 then
								priority_1_remaining_loading_amount
							else
							--loading_amount is not as per rounding.
								--If ceiling(loading_amount,rounding_off_100) <  max_loading_capacity_amount_100  
								--and 
								--( total_previous_loading + total_current_loading + total_morning_balance < limit_amount) 
								--then								
								--			ceiling(loading_amount,rounding_off_100)
								--		else
								--			floor(loading_amount,rounding_off_100)								
								case when (CEILING( CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100) < max_loading_capacity_amount_100 
										and  
										(CEILING(CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100) + priority_1_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_100 as INT)* 1.0) * @denomination_wise_round_off_100
									else
										CASE WHEN floor(CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100 < 0
										THEN 
											0
										ELSE
											floor(CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100
										END
									end

									END
								--priority_1_remaining_loading_amount
							end 
						end as loading_amount_100_p2

						,case when deno_200_priority = 2 then 
							case when priority_1_remaining_loading_amount >= max_loading_capacity_amount_200 then
								max_loading_capacity_amount_200  
							else
							case when cast(priority_1_remaining_loading_amount as int) % @denomination_wise_round_off_200 = 0 then
								priority_1_remaining_loading_amount
							else
								case when (CEILING( CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_200 as int ) * 1.0) * @denomination_wise_round_off_200) < max_loading_capacity_amount_200 
										and  
										(CEILING(CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_200 as int )* 1.0) * @denomination_wise_round_off_200) + priority_1_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_200 as int ) * 1.0) * @denomination_wise_round_off_200
									else
										CASE WHEN floor(CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_200 as int ) * 1.0) * @denomination_wise_round_off_200 < 0
										THEN 
											0
										ELSE
											floor(CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_200 as int ) * 1.0) * @denomination_wise_round_off_200
										END
									end
									END
								--priority_1_remaining_loading_amount
							end 
						end as loading_amount_200_p2

						,case when deno_500_priority = 2 then 
							case when priority_1_remaining_loading_amount >= max_loading_capacity_amount_500 then
								max_loading_capacity_amount_500  
							else
							case when cast(priority_1_remaining_loading_amount as int) % @denomination_wise_round_off_500 = 0 then
								priority_1_remaining_loading_amount
							else
								case when (CEILING( CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500) < @denomination_wise_round_off_500 
										and  
										(CEILING(CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500) + priority_1_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500
									else
										CASE WHEN floor(CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500 < 0
										THEN 
											0
										ELSE 
											floor(CAST( priority_1_remaining_loading_amount / @denomination_wise_round_off_500 as INT) * 1.0) * @denomination_wise_round_off_500
										END
									end
							END
								--priority_1_remaining_loading_amount
							end 					
						end as loading_amount_500_p2

						,case when deno_2000_priority = 2 then 
							case when priority_1_remaining_loading_amount >= max_loading_capacity_amount_2000 then
								max_loading_capacity_amount_2000  
							else
							case when cast(priority_1_remaining_loading_amount as int) % @denomination_wise_round_off_2000 = 0 then
								priority_1_remaining_loading_amount
							else
								case when (CEILING( CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000) < @denomination_wise_round_off_2000 
										and  
										(CEILING(CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000) + priority_1_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000
									else
										CASE WHEN floor(CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000 < 0
										THEN 
											0
										ELSE
											floor(CAST(priority_1_remaining_loading_amount / @denomination_wise_round_off_2000 as INT) * 1.0) * @denomination_wise_round_off_2000
										END
									end
									END
								--priority_1_remaining_loading_amount
							end 					
						end as loading_amount_2000_p2
				from #priority_1_loading
			)priority_2_loading

	--		select * from #priority_2_loading

			UPDATE #priority_2_loading 
			SET remaining_capacity_amount_100 = remaining_capacity_amount_100 - ISNULL(loading_amount_100_p2,0),
				remaining_capacity_amount_200 = remaining_capacity_amount_200 - ISNULL(loading_amount_200_p2,0),
				remaining_capacity_amount_500 = remaining_capacity_amount_500 - ISNULL(loading_amount_500_p2,0),
				remaining_capacity_amount_2000 = remaining_capacity_amount_2000 - ISNULL(loading_amount_2000_p2,0),
				total_remaining_capacity_amount = total_remaining_capacity_amount - 
												(ISNULL(loading_amount_100_p2,0)	+ 
												ISNULL(loading_amount_200_p2,0) + 
												ISNULL(loading_amount_500_p2,0) + 
												ISNULL(loading_amount_2000_p2,0))

			--For priority 3	
			
					
			select	*
					, (isnull(loading_amount_100_p3,0) + isnull(loading_amount_200_p3,0) + isnull(loading_amount_500_p3,0) + isnull(loading_amount_2000_p3,0) ) priority_3_loading_amount
					, priority_2_remaining_loading_amount - (isnull(loading_amount_100_p3,0) + isnull(loading_amount_200_p3,0) + isnull(loading_amount_500_p3,0) + isnull(loading_amount_2000_p3,0) ) priority_3_remaining_loading_amount
				into #priority_3_loading
			from
			(
				select	*
						,case when deno_100_priority = 3 then 
							case when priority_2_remaining_loading_amount >= max_loading_capacity_amount_100 then
								max_loading_capacity_amount_100  
							else
							case when cast(priority_2_remaining_loading_amount as int) % @denomination_wise_round_off_100 = 0 then
								priority_2_remaining_loading_amount
							else

							--loading_amount is not as per rounding.
								--If ceiling(loading_amount,rounding_off_100) <  max_loading_capacity_amount_100  
								--and 
								--( total_previous_loading + total_current_loading + total_morning_balance < limit_amount) 
								--then								
								--			ceiling(loading_amount,rounding_off_100)
								--		else
								--			floor(loading_amount,rounding_off_100)								
								case when (CEILING( CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_100 as INT)* 1.0) * @denomination_wise_round_off_100) < max_loading_capacity_amount_100 
										and  
										(CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100) + priority_1_loading_amount + priority_2_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100
									else
										CASE WHEN floor(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100 < 0
										THEN
											0
										ELSE
											floor(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_100 as INT) * 1.0) * @denomination_wise_round_off_100
										END
									end
								
								END								--priority_2_remaining_loading_amount
							end 
						end as loading_amount_100_p3

						,case when deno_200_priority = 3 then 
							case when priority_2_remaining_loading_amount >= max_loading_capacity_amount_200 then
								max_loading_capacity_amount_200  
							else
								case when cast(priority_2_remaining_loading_amount as int) % @denomination_wise_round_off_200 = 0 then
								priority_2_remaining_loading_amount
							else
								case when (CEILING( CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_200 as INT)* 1.0) * @denomination_wise_round_off_200) < max_loading_capacity_amount_200 
										and  
										(CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200) + priority_1_loading_amount + priority_2_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200
									else
										CASE WHEN floor(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200 < 0
										THEN 
											0
										ELSE
											floor(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_200 as INT) * 1.0) * @denomination_wise_round_off_200
										END
									end
								--priority_2_remaining_loading_amount
								END
							end 
						end as loading_amount_200_p3

						,case when deno_500_priority = 3 then 
							case when priority_2_remaining_loading_amount >= max_loading_capacity_amount_500 then
								max_loading_capacity_amount_500  
							else
							case when cast(priority_2_remaining_loading_amount as int) % @denomination_wise_round_off_500 = 0 then
								priority_2_remaining_loading_amount
							else
								case when (CEILING( CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_500  AS INT)* 1.0) * @denomination_wise_round_off_500) < max_loading_capacity_amount_500 
										and  
										(CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_500  AS INT) * 1.0) * @denomination_wise_round_off_500) + priority_1_loading_amount + priority_2_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_500  AS INT) * 1.0) * @denomination_wise_round_off_500
									else
										CASE WHEN floor(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_500  AS INT) * 1.0) * @denomination_wise_round_off_500 < 0
										THEN 0
										ELSE
											floor(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_500  AS INT) * 1.0) * @denomination_wise_round_off_500
										END
									end
								--priority_2_remaining_loading_amount
								END
							end 					
						end as loading_amount_500_p3

						,case when deno_2000_priority = 3 then 
							case when priority_2_remaining_loading_amount >= max_loading_capacity_amount_2000 then
								max_loading_capacity_amount_2000  
							else
							case when cast(priority_2_remaining_loading_amount as int) % @denomination_wise_round_off_2000 = 0 then
								priority_2_remaining_loading_amount
							else
								case when (CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000) < max_loading_capacity_amount_2000 
										and  
										(CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000) + priority_1_loading_amount + priority_2_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000
									else
										CASE WHEN floor(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000 < 0
										THEN
											0
										ELSE 
											floor(CAST(priority_2_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000
										END
									end
									END
								--priority_2_remaining_loading_amount
							end 					
						end as loading_amount_2000_p3
				from #priority_2_loading
			)priority_3_loading

			UPDATE #priority_3_loading 
			SET remaining_capacity_amount_100 = remaining_capacity_amount_100 - ISNULL(loading_amount_100_p3,0),
				remaining_capacity_amount_200 = remaining_capacity_amount_200 - ISNULL(loading_amount_200_p3,0),
				remaining_capacity_amount_500 = remaining_capacity_amount_500 - ISNULL(loading_amount_500_p3,0),
				remaining_capacity_amount_2000 = remaining_capacity_amount_2000 - ISNULL(loading_amount_2000_p3,0),
				total_remaining_capacity_amount = total_remaining_capacity_amount - 
												(ISNULL(loading_amount_100_p3,0)	+ 
												ISNULL(loading_amount_200_p3,0) + 
												ISNULL(loading_amount_500_p3,0) + 
												ISNULL(loading_amount_2000_p3,0))
			--For priority 4
			drop table if exists #priority_4_loading

			select	*
					, (isnull(loading_amount_100_p4,0) + isnull(loading_amount_200_p4,0) + isnull(loading_amount_500_p4,0) + isnull(loading_amount_2000_p4,0) ) priority_4_loading_amount
					, priority_3_remaining_loading_amount - (isnull(loading_amount_100_p4,0) + isnull(loading_amount_200_p4,0) + isnull(loading_amount_500_p4,0) + isnull(loading_amount_2000_p4,0) ) priority_4_remaining_loading_amount
				into #priority_4_loading
			from
			(
				select	*
						,case when deno_100_priority = 4 then 
							case when priority_3_remaining_loading_amount >= max_loading_capacity_amount_100 then
								max_loading_capacity_amount_100  
							else
							case when cast(priority_3_remaining_loading_amount as int) % @denomination_wise_round_off_100 = 0 then
								priority_3_remaining_loading_amount
							else
								--loading_amount is not as per rounding.
								--If ceiling(loading_amount,rounding_off_100) <  max_loading_capacity_amount_100  
								--and 
								--( total_previous_loading + total_current_loading + total_morning_balance < limit_amount) 
								--then								
								--			ceiling(loading_amount,rounding_off_100)
								--		else
								--			floor(loading_amount,rounding_off_100)								
								case when (CEILING(CAST( priority_3_remaining_loading_amount / @denomination_wise_round_off_100 AS INT)* 1.0) * @denomination_wise_round_off_100) < max_loading_capacity_amount_100 
										and  
										(CEILING(CAST( priority_3_remaining_loading_amount / @denomination_wise_round_off_100 AS INT) * 1.0) * @denomination_wise_round_off_100) + priority_1_loading_amount + priority_2_loading_amount + priority_3_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST( priority_3_remaining_loading_amount / @denomination_wise_round_off_100 AS INT) * 1.0) * @denomination_wise_round_off_100
									else
										CASE WHEN floor(CAST( priority_3_remaining_loading_amount / @denomination_wise_round_off_100 AS INT) * 1.0) * @denomination_wise_round_off_100 < 0
										THEN 
											0
										ELSE
											floor(CAST( priority_3_remaining_loading_amount / @denomination_wise_round_off_100 AS INT)* 1.0) * @denomination_wise_round_off_100
										END
									end
								END
								--priority_3_remaining_loading_amount
							end 
						end as loading_amount_100_p4

						,case when deno_200_priority = 4 then 
							case when priority_3_remaining_loading_amount >= max_loading_capacity_amount_200 then
								max_loading_capacity_amount_200  
							else
							case when cast(priority_3_remaining_loading_amount as int) % @denomination_wise_round_off_200 = 0 then
								priority_3_remaining_loading_amount
							else
								case when (CEILING(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_200 AS INT) * 1.0) * @denomination_wise_round_off_200) < max_loading_capacity_amount_200 
										and  
										(CEILING(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_200 AS INT) * 1.0) * @denomination_wise_round_off_200) + priority_1_loading_amount + priority_2_loading_amount + priority_3_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_200 AS INT) * 1.0) * @denomination_wise_round_off_200
									else
										CASE WHEN floor(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_200 AS INT) * 1.0) * @denomination_wise_round_off_200 < 0
										THEN 
											0
										ELSE 
											floor(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_200 AS INT) * 1.0) * @denomination_wise_round_off_200
										END
									end
								--priority_3_remaining_loading_amount
								END
							end 
						end as loading_amount_200_p4

						,case when deno_500_priority = 4 then 
							case when priority_3_remaining_loading_amount >= max_loading_capacity_amount_500 then
								max_loading_capacity_amount_500  
							else
							case when cast(priority_3_remaining_loading_amount as int) % @denomination_wise_round_off_500 = 0 then
								priority_3_remaining_loading_amount
							else
								case when (CEILING(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_500 AS INT)* 1.0) * @denomination_wise_round_off_500) < max_loading_capacity_amount_500 
										and  
										(CEILING(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_500 AS INT) * 1.0) * @denomination_wise_round_off_500) + priority_1_loading_amount + priority_2_loading_amount + priority_3_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_500 AS INT) * 1.0) * @denomination_wise_round_off_500
									else
										CASE WHEN floor(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_500 AS INT) * 1.0) * @denomination_wise_round_off_500 < 0
										THEN 
											0
										ELSE
											floor(CAST(priority_3_remaining_loading_amount / @denomination_wise_round_off_500 AS INT) * 1.0) * @denomination_wise_round_off_500
										END
									end
								--priority_3_remaining_loading_amount
								END
							end 					
						end as loading_amount_500_p4

						,case when deno_2000_priority = 4 then 
							case when priority_3_remaining_loading_amount >= max_loading_capacity_amount_2000 then
								max_loading_capacity_amount_2000  
							else
							case when cast(priority_3_remaining_loading_amount as int) % @denomination_wise_round_off_2000 = 0 then
								priority_3_remaining_loading_amount
							else
								case when (CEILING(CAST (priority_3_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT)* 1.0) * @denomination_wise_round_off_2000) < max_loading_capacity_amount_2000 
										and  
										(CEILING(CAST (priority_3_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000) + priority_1_loading_amount + priority_2_loading_amount + priority_3_loading_amount + total_morning_balance < limit_amount 
									then
										CEILING(CAST (priority_3_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000
									else
										CASE WHEN floor(CAST (priority_3_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000 < 0
										THEN 
											0
										ELSE
											floor(CAST (priority_3_remaining_loading_amount / @denomination_wise_round_off_2000 AS INT) * 1.0) * @denomination_wise_round_off_2000
										END
									end
								--priority_3_remaining_loading_amount
								END
							end 					
						end as loading_amount_2000_p4
				from #priority_3_loading
			)priority_4_loading
			
			UPDATE #priority_4_loading 
			SET remaining_capacity_amount_100 = remaining_capacity_amount_100 - ISNULL(loading_amount_100_p4,0),
				remaining_capacity_amount_200 = remaining_capacity_amount_200 - ISNULL(loading_amount_200_p4,0),
				remaining_capacity_amount_500 = remaining_capacity_amount_500 - ISNULL(loading_amount_500_p4,0),
				remaining_capacity_amount_2000 = remaining_capacity_amount_2000 - ISNULL(loading_amount_2000_p4,0),
				total_remaining_capacity_amount = total_remaining_capacity_amount - 
												(ISNULL(loading_amount_100_p4,0)	+ 
												ISNULL(loading_amount_200_p4,0) + 
												ISNULL(loading_amount_500_p4,0) + 
												ISNULL(loading_amount_2000_p4,0))


			drop table if exists #final_allocated_loading
			select	*
					, loading_amount_100 + loading_amount_200 + loading_amount_500 + loading_amount_2000 as total_loading_amount
			into #final_allocated_loading
			from
			(	
				select	*
					, isnull(loading_amount_100_p1,0) + isnull(loading_amount_100_p2,0) + isnull(loading_amount_100_p3,0) + isnull(loading_amount_100_p4,0) as loading_amount_100 
					, isnull(loading_amount_200_p1,0) + isnull(loading_amount_200_p2,0) + isnull(loading_amount_200_p3,0) + isnull(loading_amount_200_p4,0) as loading_amount_200 
					, isnull(loading_amount_500_p1,0) + isnull(loading_amount_500_p2,0) + isnull(loading_amount_500_p3,0) + isnull(loading_amount_500_p4,0) as loading_amount_500 
					, isnull(loading_amount_2000_p1,0) + isnull(loading_amount_2000_p2,0) + isnull(loading_amount_2000_p3,0) + isnull(loading_amount_2000_p4,0) as loading_amount_2000 
				from #priority_4_loading
			)allocated_loading

		--select * from #final_allocated_loading
			------------------------------------------------------------------------------------------------------------------------------

			--drop table if exists #final_allocated_loading
			--SELECT * FROM #final_allocated_loading where feeder_branch_code = 'KOLHAPUR'

		DROP TABLE IF exists  #pre_vault_amounting_1


		Select * into #pre_vault_amounting_1
		from
		(
			SELECT      atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	atm_priority
					,	atm_band
					,	reason_for_priority
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
				    ,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 

					,   case when  (loading_amount_100 +
									loading_amount_200+
									loading_amount_500+
									loading_amount_2000) = 0
								THEN
								 case when        
								  dist_Purpose='EOD'                              
									 then 'EOD' 
									 else ''
								 end                             
							 else 'Add Cash' end as dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	denomination_100_max_capacity_percentage
					,	denomination_200_max_capacity_percentage
					,	denomination_500_max_capacity_percentage
					,	denomination_2000_max_capacity_percentage
					,	cassette_50_count_original  
					,	cassette_100_count_original 
					,	cassette_200_count_original 
					,	cassette_500_count_original 
					,	cassette_2000_count_original
					,	deno_100_priority
					,	deno_200_priority
					,	deno_500_priority
					,	deno_2000_priority
					,	is_100_available
					,	is_200_available
					,	is_500_available
					,	is_2000_available
					,	is_zero_dispense
					,	feasibility_frequency
					,	is_feasible_today
					,   bank_decide_limit_days
					,   bank_loading_limit_days
					,   feeder_decide_limit_days
					,   feeder_loading_limit_days
					,   feasibility_decide_limit_days
					,   feasibility_loading_limit_days
					,	is_currency_chest
					,	holiday_code
					,	is_indent_required
					,	withdrawal_type
					--,	cassette_50_capacity_amount
					--,	cassette_100_capacity_amount
					--,	cassette_200_capacity_amount
					--,	cassette_500_capacity_amount
					--,	cassette_2000_capacity_amount
					--,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	max_loading_capacity_amount_100
					,	max_loading_capacity_amount_200
					,	max_loading_capacity_amount_500
					,	max_loading_capacity_amount_2000
					--,	remaining_capacity_amount_100
					--,	remaining_capacity_amount_200
					--,	remaining_capacity_amount_500
					--,	remaining_capacity_amount_2000
					,	CASE WHEN 
							 @is_atm_list_available = 'Yes'
							 THEN
								CASE WHEN 
									total_amount_list > 0
									THEN
										CASE WHEN denomination_100_list <= loading_amount_100
											THEN
												max_loading_capacity_amount_100 - denomination_100_list
											ELSE
												max_loading_capacity_amount_100 - loading_amount_100
										END
								ELSE
									 max_loading_capacity_amount_100 -loading_amount_100
								END
							 ELSE
						 max_loading_capacity_amount_100 - loading_amount_100
						END as remaining_capacity_amount_100
						,	CASE WHEN 
							 @is_atm_list_available = 'Yes'
							 THEN
								CASE WHEN 
									total_amount_list > 0
									THEN
										CASE WHEN denomination_200_list <= loading_amount_200
											THEN
												max_loading_capacity_amount_200 - denomination_200_list
											ELSE
												max_loading_capacity_amount_200 - loading_amount_200
										END
								ELSE
									 max_loading_capacity_amount_200 -loading_amount_200
								END
							 ELSE
						 max_loading_capacity_amount_200 - loading_amount_200
						END as remaining_capacity_amount_200
						,	CASE WHEN 
							 @is_atm_list_available = 'Yes'
							 THEN
								CASE WHEN 
									total_amount_list > 0
									THEN
										CASE WHEN denomination_500_list <= loading_amount_500
											THEN
												max_loading_capacity_amount_500 - denomination_500_list
											ELSE
												max_loading_capacity_amount_500 - loading_amount_500
										END
								ELSE
									 max_loading_capacity_amount_500 -loading_amount_500
								END
							 ELSE
						 max_loading_capacity_amount_500 - loading_amount_500
						END as remaining_capacity_amount_500
						,	CASE WHEN 
							 @is_atm_list_available = 'Yes'
							 THEN
								CASE WHEN 
									total_amount_list > 0
									THEN
										CASE WHEN denomination_2000_list <= loading_amount_2000
											THEN
												max_loading_capacity_amount_2000 - denomination_2000_list
											ELSE
												max_loading_capacity_amount_2000 - loading_amount_2000
										END
								ELSE
									 max_loading_capacity_amount_2000 -loading_amount_2000
								END
							 ELSE
						 max_loading_capacity_amount_2000 - loading_amount_2000
						END as remaining_capacity_amount_2000
					,	total_remaining_capacity_amount
					,	denomination_100_list
					,	denomination_200_list
					,	denomination_500_list
					,	denomination_2000_list
					,	loading_amount_100 as loading_amount_100_original
					,	loading_amount_200   as loading_amount_200_original
					,   loading_amount_500	 as loading_amount_500_original
					,	loading_amount_2000	 as loading_amount_2000_original
					,	total_loading_amount as total_loading_amount_original
					,	total_amount_list
					--,	tentative_loading_amount_2000
					,   CASE WHEN 
							 @is_atm_list_available = 'Yes'
							 THEN
								CASE WHEN 
									total_amount_list > 0
									THEN
										CASE WHEN denomination_100_list <= loading_amount_100
											THEN
												denomination_100_list
											ELSE
												loading_amount_100
										END
								ELSE
									loading_amount_100
								END
							 ELSE
						 loading_amount_100
						END as loading_amount_100
					,   CASE WHEN 
							 @is_atm_list_available = 'Yes'
							 THEN
								CASE WHEN 
									total_amount_list > 0
									THEN
										CASE WHEN denomination_200_list <= loading_amount_200
											THEN
												denomination_200_list
											ELSE
												loading_amount_200
										END
								ELSE
									loading_amount_200
								END
							 ELSE
						 loading_amount_200
						END as loading_amount_200
					,   CASE WHEN 
							 @is_atm_list_available = 'Yes'
							 THEN
								CASE WHEN 
									total_amount_list > 0
									THEN
										CASE WHEN denomination_500_list <= loading_amount_500
											THEN
												denomination_500_list
											ELSE
												loading_amount_500
										END
								ELSE
									loading_amount_500
								END
							 ELSE
						 loading_amount_500
						END as loading_amount_500
					,   CASE WHEN 
							 @is_atm_list_available = 'Yes'
							 THEN
								CASE WHEN 
									total_amount_list > 0
									THEN
										CASE WHEN denomination_2000_list <= loading_amount_2000
											THEN
												denomination_2000_list 
											ELSE
												loading_amount_2000
										END
								ELSE
									loading_amount_2000
								END
							 ELSE
						 loading_amount_2000
						END as loading_amount_2000
					--,   loading_amount_200
					--,   loading_amount_500
					--,   loading_amount_2000
					--,   rounded_amount_2000
				--	,   case when  (total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000
				--							   - avgdispense > 0) then 
				--		( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense ) 
				--							  end
				--							  as total_expected_balanceT1
				--	, case when  (total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense > 0) then 
				--	( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_50,2)) as float)/100)
				--							  end 
				--							  as expected_balanceT1_50
    --            ,case when  ( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 - avgdispense > 0) then  
				--( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 - avgdispense ) * (cast((Round(cassette_capacity_percentage_100,2)) as float)/100)
				--							  end 
				--							  as expected_balanceT1_100
    --            ,case when  ( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 - avgdispense > 0) then  
				--( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 - avgdispense ) * (cast((Round(cassette_capacity_percentage_200,2)) as float)/100)
				--							end 
				--							  as expected_balanceT1_200
    --             ,case when  (total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense > 0) then 
				--	 ( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense ) *( cast((Round(cassette_capacity_percentage_500,2)) as float)/100)
				--							end
				--							  as expected_balanceT1_500
    --             , case when  ( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense > 0) then 
				--			( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_2000,2)) as float)/100)
				--							  end 
				--							  as expected_balanceT1_2000
				--, case when  ( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 - avgdispense > 0) then 				
				--(( total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense ) * (cast((Round(applied_vaulting_percentage,2)) as float)/100)) 
				--							  end 
				--					as total_vault_amount
				--, case when  (total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 - avgdispense > 0) then 
				--(( total_morning_balance +( loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 ) - avgdispense ) *
				--							  ( (Round(cassette_capacity_percentage_50,2)) /100)
				--							   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
				--							   end 
				--							   as vaultingamount_50		
											   
				--, case when  (total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense > 0) then 
				--(( total_morning_balance +(loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000) - avgdispense ) *
				--							   ((Round(cassette_capacity_percentage_100,2)) /100)
				--							   *(cast(applied_vaulting_percentage as float)/100))	
				--							   end 
				--							   as vaultingamount_100
 
				--, case when  (total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 - avgdispense > 0) then 
				--(( total_morning_balance +( loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 ) - avgdispense ) *
				--							   ((Round(cassette_capacity_percentage_200,2)) /100)
				--							   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
				--							   end 
				--							   as vaultingamount_200	

			 --   , case when  (total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000  - avgdispense > 0) then 
				--	(( total_morning_balance +( loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 ) - avgdispense ) *
				--							  ( (Round(cassette_capacity_percentage_500,2)) /100)
				--							   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))
				--							   end 
				--							   as vaultingamount_500	

    --         , case when  (total_morning_balance + loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000 - avgdispense > 0) then 
				--	(( total_morning_balance +( loading_amount_100   --take loading amount
				--						      + loading_amount_200
				--							  + loading_amount_500
				--							  + loading_amount_2000) - avgdispense ) *
				--							  ( (Round(cassette_capacity_percentage_2000,2)) /100)
				--							   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
				--							   end 
				--							   as vaultingamount_2000	
				FROM 
               #final_allocated_loading
			   
			   )a


			   UPDATE #pre_vault_amounting_1
			   SET total_remaining_capacity_amount 
				=	ISNULL(remaining_capacity_amount_100,0) + 
					ISNULL(remaining_capacity_amount_200,0)+
					ISNULL(remaining_capacity_amount_500,0)+ 
					ISNULL(remaining_capacity_amount_2000,0)



			  -- select * from #pre_vault_amounting_1
			   DROP TABLE IF EXISTS #pre_vault_amounting
			   SELECT *
				 	,   case when  (total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000
											   - avgdispense > 0) then 
						( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense ) 
											  end
											  as total_expected_balanceT1
					, case when  (total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense > 0) then 
					( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_50,2)) as float)/100)
											  end 
											  as expected_balanceT1_50
                ,case when  ( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 - avgdispense > 0) then  
				( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 - avgdispense ) * (cast((Round(cassette_capacity_percentage_100,2)) as float)/100)
											  end 
											  as expected_balanceT1_100
                ,case when  ( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 - avgdispense > 0) then  
				( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 - avgdispense ) * (cast((Round(cassette_capacity_percentage_200,2)) as float)/100)
											end 
											  as expected_balanceT1_200
                 ,case when  (total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense > 0) then 
					 ( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense ) *( cast((Round(cassette_capacity_percentage_500,2)) as float)/100)
											end
											  as expected_balanceT1_500
                 , case when  ( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense > 0) then 
							( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_2000,2)) as float)/100)
											  end 
											  as expected_balanceT1_2000
				, case when  ( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 - avgdispense > 0) then 				
				(( total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense ) * (cast((Round(applied_vaulting_percentage,2)) as float)/100)) 
											  end 
									as total_vault_amount
				, case when  (total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 - avgdispense > 0) then 
				(( total_morning_balance +( loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 ) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_50,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_50		
											   
				, case when  (total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense > 0) then 
				(( total_morning_balance +(loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000) - avgdispense ) *
											   ((Round(cassette_capacity_percentage_100,2)) /100)
											   *(cast(applied_vaulting_percentage as float)/100))	
											   end 
											   as vaultingamount_100
 
				, case when  (total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 - avgdispense > 0) then 
				(( total_morning_balance +( loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 ) - avgdispense ) *
											   ((Round(cassette_capacity_percentage_200,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_200	

			    , case when  (total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000  - avgdispense > 0) then 
					(( total_morning_balance +( loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 ) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_500,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))
											   end 
											   as vaultingamount_500	

             , case when  (total_morning_balance + loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000 - avgdispense > 0) then 
					(( total_morning_balance +( loading_amount_100   --take loading amount
										      + loading_amount_200
											  + loading_amount_500
											  + loading_amount_2000) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_2000,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_2000	
				INTO #pre_vault_amounting
				FROM #pre_vault_amounting_1


			--  select * FROM #pre_vault_amounting
--select * FROM #pre_vault_amounting
	--PENDING 23 Dec 2018: Amount for total_expected_balanceT1 is getting in negative. Ned to check.

	--****************************************Rounding of Vaulting amount*************************************************************/
	--select * FROM #pre_vault_amounting
	 drop table if exists #vault_amounting
		
		/* For testing dont remove below commented code */
		--DECLARE @MaxDestinationDate DATE
		--DECLARE @dispenseformula NVARCHAR(50)
		--DECLARE @confidence_factor NVARCHAR(50)
		--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		--DECLARE @denomination_wise_round_off_100 INT			 -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_200 int      
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100_currency_chest int      
		--DECLARE @denomination_wise_round_off_200_currency_chest int
		--DECLARE @denomination_wise_round_off_500_currency_chest int
		--DECLARE @denomination_wise_round_off_2000_currency_chest INT
		--declare @default_avg_dispense int
		--declare @vaulting_for_normal_weekday_percentage float,
		--		@vaulting_for_normal_weekend_percentage float,
		--		@vaulting_for_extended_weekend_percentage float
		
		
		--Select	@confidence_factor = confidence_factor
		--, @dispenseformula = dispenseformula
		--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		--, @denomination_wise_round_off_100_currency_chest = denomination_wise_round_off_100_currency_chest
		--, @denomination_wise_round_off_200_currency_chest = denomination_wise_round_off_200_currency_chest
		--, @denomination_wise_round_off_500_currency_chest = denomination_wise_round_off_500_currency_chest
		--, @denomination_wise_round_off_2000_currency_chest = denomination_wise_round_off_2000_currency_chest
		--,@default_avg_dispense = default_average_dispense
		--, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
		--, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
		--, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
		-- from system_settings where record_status = 'Active'
		
			
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Insert into vault_amounting Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


					select	*
					,vaultingamount_50 as rounded_vaultingamount_50
					,
						--CASE WHEN 
						--	is_currency_chest = 'Yes'
						--	THEN
						--		CASE WHEN 
						--			(cast(vaultingamount_100 as bigint)  % @denomination_wise_round_off_100_currency_chest ) > 0  
						--		THEN
						--			  (cast( (vaultingamount_100 /@denomination_wise_round_off_100_currency_chest) as int) +1) * @denomination_wise_round_off_100_currency_chest
						--		ELSE 
						--		0
						--		END
						--	ELSE
							case when (cast(vaultingamount_100 as BIGINT)  % @denomination_wise_round_off_100 ) > 0  
							then  
						  (cast( (vaultingamount_100 /@denomination_wise_round_off_100) as BIGINT) +1) * @denomination_wise_round_off_100  
							else    0 
						--END
					end  rounded_vaultingamount_100
			
					,
							
						--CASE WHEN 
						--	is_currency_chest = 'Yes'
						--	THEN
						--		CASE WHEN 
						--			(cast(vaultingamount_200 as bigint)  % @denomination_wise_round_off_200_currency_chest ) > 0  
						--		THEN
						--			  (cast( (vaultingamount_200 /@denomination_wise_round_off_200_currency_chest) as int) +1) * @denomination_wise_round_off_200_currency_chest
						--		ELSE 
						--		0
						--		END
						--	ELSE
							case when (cast(vaultingamount_200 as BIGINT)  % @denomination_wise_round_off_200 ) > 0  
							   then  
							  (cast( (vaultingamount_200 /@denomination_wise_round_off_200) as BIGINT) +1) * @denomination_wise_round_off_200  
								else    0 
						--	END
						end  rounded_vaultingamount_200
				 
					,	
						--CASE WHEN 
						--	is_currency_chest = 'Yes'
						--	THEN
						--		CASE WHEN 
						--			(cast(vaultingamount_500 as bigint)  % @denomination_wise_round_off_500_currency_chest ) > 0  
						--		THEN
						--			  (cast( (vaultingamount_500 /@denomination_wise_round_off_500_currency_chest) as int) +1) * @denomination_wise_round_off_500_currency_chest
						--		ELSE 
						--		0
						--		END
						--	ELSE
								case when (cast(vaultingamount_500 as BIGINT)  % @denomination_wise_round_off_500 ) > 0  
									then  
									(cast( (vaultingamount_500 /@denomination_wise_round_off_500) as BIGINT) +1) * @denomination_wise_round_off_500  
									else    0 
							--END
						end  rounded_vaultingamount_500, 

						--CASE WHEN 
						--	is_currency_chest = 'Yes'
						--	THEN
						--		CASE WHEN 
						--			(cast(vaultingamount_2000 as bigint)  % @denomination_wise_round_off_2000_currency_chest ) > 0  
						--		THEN
						--			  (cast( (vaultingamount_2000 /@denomination_wise_round_off_2000_currency_chest) as int) +1) * @denomination_wise_round_off_2000_currency_chest
						--		ELSE 
						--		0
						--		END
						--	ELSE
								case when (cast(vaultingamount_2000 as BIGINT)  % @denomination_wise_round_off_2000 ) > 0  
						   then  
						  (cast( (vaultingamount_2000 /@denomination_wise_round_off_2000) as BIGINT) +1) * @denomination_wise_round_off_2000  
							else    0 
						--END
					end  rounded_vaultingamount_2000

					--select * from system_settings

		into #vault_amounting
		from #pre_vault_amounting

		--Select * from #vault_amounting		where feeder_branch_code = 'PIMPRI'

		-- Select sum(rounded_vaultingamount_100),
		--		sum(rounded_vaultingamount_200),
		--		sum(rounded_vaultingamount_500),
		--		sum(rounded_vaultingamount_2000) from #vault_amounting		where feeder_branch_code = 'PIMPRI'

	
		--3900000	7400000	22100000	0
		--3900000	7400000	30500000	0
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	    values ('Insert into vault_amounting Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		-- Select * from #vault_amounting		where feeder_branch_code = 'KARAD'
		--   select * from #pre_vault_amounting
		---  drop table  #deno_rounded_loading_amount
		

		--DECLARE @dateT DATETIME
		--SET @dateT ='2018-11-05'

		--DECLARE @MaxDestinationDate DATE
		--DECLARE @dispenseformula NVARCHAR(50)
		--DECLARE @confidence_factor NVARCHAR(50)
		--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT
		--Select	@confidence_factor = confidence_factor
		--, @dispenseformula = dispenseformula
		--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		-- from system_settings where record_status = 'Active'			
	--		DECLARE @MaxDestinationDate DATE
	--	DECLARE @dispenseformula NVARCHAR(50)
	--	DECLARE @confidence_factor NVARCHAR(50)
	--	DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
	--	DECLARE @denomination_wise_round_off_100 INT			 -- for rounding, replace with 100000
	--	DECLARE @denomination_wise_round_off_200 int      
	--	DECLARE @denomination_wise_round_off_500 int
	--	DECLARE @denomination_wise_round_off_2000 int
	--	DECLARE @denomination_wise_round_off_100_currency_chest int      
	--	DECLARE @denomination_wise_round_off_200_currency_chest int
	--	DECLARE @denomination_wise_round_off_500_currency_chest int
	--	DECLARE @denomination_wise_round_off_2000_currency_chest INT
	--	declare @default_avg_dispense int
	--	declare @vaulting_for_normal_weekday_percentage float,
	--			@vaulting_for_normal_weekend_percentage float,
	--			@vaulting_for_extended_weekend_percentage float
		
		
	--	Select	@confidence_factor = confidence_factor
	--	, @dispenseformula = dispenseformula
	--	, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
	--	, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
	--	, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
	--	, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
	--	, @denomination_wise_round_off_100_currency_chest = denomination_wise_round_off_100_currency_chest
	--	, @denomination_wise_round_off_200_currency_chest = denomination_wise_round_off_200_currency_chest
	--	, @denomination_wise_round_off_500_currency_chest = denomination_wise_round_off_500_currency_chest
	--	, @denomination_wise_round_off_2000_currency_chest = denomination_wise_round_off_2000_currency_chest
	--	,@default_avg_dispense = default_average_dispense
	--	, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
	--	, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
	--	, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
	--	 from system_settings where record_status = 'Active'
	--	 	declare @Execution_log_str nvarchar(max)

	--		DECLARE @dateT DATETIME = '2019-05-14'
	--DECLARE @cur_user varchar(50) = 'SA'
	--DECLARE @created_reference_id varchar(40) = 'C10058407185420'
	-----------------DECLARE @for_project_id varchar(50) = 'MOF_MAH'
	-----------------DECLARE @for_bank_code varchar(40) = 'SBI'
	-----------------DECLARE @for_feeder_branch nvarchar(50) = 'PIMPRI, PUNE_00575'
	-----------------DECLARE @is_revised_forecasting varchar(10) = 'Yes'
	-----------------DECLARE @active_indent_code	NVARCHAR(max) = 'SBI/MOF_MAH/PIMPRI, PUNE_00575/Logicash/20190422175115'

	--DECLARE @for_project_id varchar(50) = 'All'
	--DECLARE @for_bank_code varchar(40) = 'SBI'
	--DECLARE @for_feeder_branch nvarchar(50) = 'All'
	--DECLARE @is_revised_forecasting varchar(10) = 'No'
	--DECLARE @active_indent_code	NVARCHAR(max) = ''
		/***************************************Roundig of calcualted vault amount*********************************/
		WHILE 1=1
		BEGIN	

		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Insert into #vault_gap_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		DROP TABLE IF exists #vault_gap_amount		 
		
		SELECT *
		, ROUND((rounded_vaultingamount_100 - vaultingamount_100),2) AS rounding_gap_100
		, ROUND((rounded_vaultingamount_200 - vaultingamount_200),2) AS rounding_gap_200
		, ROUND((rounded_vaultingamount_500 - vaultingamount_500),2) AS rounding_gap_500
		, ROUND((rounded_vaultingamount_2000 - vaultingamount_2000),2) AS rounding_gap_2000
		, (rounded_vaultingamount_100 + rounded_vaultingamount_200 + rounded_vaultingamount_500 + rounded_vaultingamount_2000) AS total_rounded_vault_amount
		, (rounded_vaultingamount_100 + rounded_vaultingamount_200 + rounded_vaultingamount_500 + rounded_vaultingamount_2000) - total_vault_amount  AS total_rounded_vault_amount_gap
		 INTO   #vault_gap_amount
		 FROM   #vault_amounting
		
		  -- Select * from #vault_gap_amount
		 -- total rounded vaulting amnt> original vaulting amt

		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into #vault_gap_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

		IF exists ( SELECT * FROM #vault_gap_amount
		 WHERE (total_rounded_vault_amount ) > limit_amount ) 
		
		BEGIN
			-- find max gap
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #max_vault_gap Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

			DROP TABLE IF exists #max_vault_gap 	
			SELECT *, 
				(
					SELECT MAX(v) FROM (VALUES (rounding_gap_100), 
					(rounding_gap_200), 
					(rounding_gap_500),
					(rounding_gap_2000)) AS value(v)
		    	) 
					AS max_gap
					INTO #max_vault_gap
				    FROM #vault_gap_amount

				
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #max_vault_gap Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	
			/* For testing dont remove below commented code */	

		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT

		--Select	
		-- @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		-- from system_settings where record_status = 'Active'	
		

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #vault_amounting Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			TRUNCATE TABLE #vault_amounting		

			INSERT INTO #vault_amounting
			SELECT  atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	atm_priority
					,	atm_band
					,	reason_for_priority
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	denomination_100_max_capacity_percentage
					,	denomination_200_max_capacity_percentage
					,	denomination_500_max_capacity_percentage
					,	denomination_2000_max_capacity_percentage
					,	cassette_50_count_original  
					,	cassette_100_count_original 
					,	cassette_200_count_original 
					,	cassette_500_count_original 
					,	cassette_2000_count_original
					,	deno_100_priority
					,	deno_200_priority
					,	deno_500_priority
					,	deno_2000_priority
					,	is_100_available
					,	is_200_available
					,	is_500_available
					,	is_2000_available
					,	is_zero_dispense
					,	feasibility_frequency
					,	is_feasible_today
					,   bank_decide_limit_days
					,   bank_loading_limit_days
					,   feeder_decide_limit_days
					,   feeder_loading_limit_days
					,   feasibility_decide_limit_days
					,   feasibility_loading_limit_days
					,	is_currency_chest
					,	holiday_code
					,	is_indent_required
					,	withdrawal_type
					--,	cassette_100_capacity_amount
					--,	cassette_200_capacity_amount
					--,	cassette_500_capacity_amount
					--,	cassette_2000_capacity_amount
					--,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					--,	tentative_loading_amount_50
					,	max_loading_capacity_amount_100
					,	max_loading_capacity_amount_200
					,	max_loading_capacity_amount_500
					,	max_loading_capacity_amount_2000
					,	remaining_capacity_amount_100
					,	remaining_capacity_amount_200
					,	remaining_capacity_amount_500
					,	remaining_capacity_amount_2000
					,	total_remaining_capacity_amount
					--,   rounded_amount_50
					,   loading_amount_100
					,   loading_amount_200
					,   loading_amount_500
					,   loading_amount_2000
					,	total_expected_balanceT1
					,	expected_balanceT1_50
					,	expected_balanceT1_100
					,	expected_balanceT1_200
					,	expected_balanceT1_500
					,	expected_balanceT1_2000
					,	total_vault_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	rounded_vaultingamount_50
					, CASE  
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_100 = max_gap 
							THEN   rounded_vaultingamount_100 - @denomination_wise_round_off_100 
							ELSE rounded_vaultingamount_100 
						END AS rounded_Vault_amt_100_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_200 = max_gap 
							THEN   rounded_vaultingamount_200 - @denomination_wise_round_off_200 
							ELSE rounded_vaultingamount_200 
						END AS rounded_Vault_amt_200_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_500 = max_gap 
							THEN  ( rounded_vaultingamount_500 - @denomination_wise_round_off_500 )
							ELSE rounded_vaultingamount_500 
						END AS rounded_Vault_amt_500_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_2000 = max_gap 
							THEN   rounded_vaultingamount_2000 - @denomination_wise_round_off_2000 
							ELSE rounded_vaultingamount_2000 
						END AS rounded_Vault_amt_2000_updated
						--,denomination_100_max_capacity_percentage
						--,denomination_200_max_capacity_percentage
						--,denomination_500_max_capacity_percentage
						--,denomination_2000_max_capacity_percentage
						--,cassette_50_count_original  
						--,cassette_100_count_original 
						--,cassette_200_count_original 
						--,cassette_500_count_original 
						--,cassette_2000_count_original
						--,deno_100_priority
						--,deno_200_priority
						--,deno_500_priority
						--,deno_2000_priority
				FROM 
					 #max_vault_gap  

			   -- SELECT * FROM #vault_amounting

			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #vault_amounting Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
						
		END
	ELSE
		BEGIN
		
			BREAK
		END

END

		--	select * from #vault_amounting 	    
  ----------------------------------Inserting into distribution planning detail----------------------
  
	--	select * from #vault_amounting 
 
 
            DROP table if exists #distribution

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #distribution Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				
			Select * into #distribution
				from(
				Select atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	atm_priority
					,	atm_band
					,	reason_for_priority
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	denomination_100_max_capacity_percentage
					,	denomination_200_max_capacity_percentage
					,	denomination_500_max_capacity_percentage
					,	denomination_2000_max_capacity_percentage
					,	cassette_50_count_original  
					,	cassette_100_count_original 
					,	cassette_200_count_original 
					,	cassette_500_count_original 
					,	cassette_2000_count_original
					,	deno_100_priority
					,	deno_200_priority
					,	deno_500_priority
					,	deno_2000_priority
					,	is_zero_dispense
					,	feasibility_frequency
					,	is_feasible_today
					,   bank_decide_limit_days
					,   bank_loading_limit_days
					,   feeder_decide_limit_days
					,   feeder_loading_limit_days
					,   feasibility_decide_limit_days
					,   feasibility_loading_limit_days
					,	is_currency_chest
					,	holiday_code
					,	is_indent_required
					,	withdrawal_type
					,	is_100_available
					,	is_200_available
					,	is_500_available
					,	is_2000_available
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	max_loading_capacity_amount_100
					,	max_loading_capacity_amount_200
					,	max_loading_capacity_amount_500
					,	max_loading_capacity_amount_2000
					,	remaining_capacity_amount_100
					,	remaining_capacity_amount_200
					,	remaining_capacity_amount_500
					,	remaining_capacity_amount_2000
					,	total_remaining_capacity_amount
					--,	tentative_loading_amount_2000
					--,   loading_amount_50	as forecasted_amt_50 
					,   loading_amount_100	as forecasted_amt_100
					,   loading_amount_200	as forecasted_amt_200
					,   loading_amount_500	as forecasted_amt_500
					,   loading_amount_2000	as forecasted_amt_2000
					,  (loading_amount_100 + loading_amount_200 + loading_amount_500 + loading_amount_2000) as total_forecasted_amt
					,	total_expected_balanceT1
					,	expected_balanceT1_50
					,	expected_balanceT1_100
					,	expected_balanceT1_200
					,	expected_balanceT1_500
					,	expected_balanceT1_2000
					,	total_vault_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	rounded_vaultingamount_50
					,	rounded_vaultingamount_100
					,	rounded_vaultingamount_200
					,	rounded_vaultingamount_500
					,	rounded_vaultingamount_2000
					,	(rounded_vaultingamount_50 + rounded_vaultingamount_100	+ rounded_vaultingamount_200
					+    rounded_vaultingamount_500+ rounded_vaultingamount_2000 )
					     as total_rounded_vault_amt
							from  #vault_amounting

					)a	
					
			--select * from #distribution		 where feeder_branch_code = 'ACHALPUR' and bank_code = 'BOMH'

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #distribution Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #distribution Completed'
		
			  -- generate indent code.
			  
				drop table if exists #dist_with_cra	
				DROP TABLE IF EXISTS #final_feeder_level_indent
			DROP TABLE IF EXISTS #feeder_level_forecast
			DROP TABLE IF EXISTS #feeder_level_dataset
			--DECLARE @is_revised_forecasting varchar(10) = 'No'

				SELECT dist.* ,
					cra.new_cra as cra
					,CASE WHEN 
					@is_revised_forecasting = 'No' THEN concat(dist.bank_code,'/',dist.project_id,'/',dist.feeder_branch_code,'/',cra.new_cra,'/',format(DATEADD(MI,330,GETUTCDATE()),'yyyyMMddHHmmss'))
					ELSE concat(dist.bank_code,'/',dist.project_id,'/',dist.feeder_branch_code,'/',cra.new_cra,'/R',format(DATEADD(MI,330,GETUTCDATE()),'yyyyMMddHHmmss'))
					END AS indentcode
				INTO #dist_with_cra
				FROM #distribution dist 
				JOIN cra_feasibility cra
				ON dist.atm_id = cra.atm_id 
				AND dist.site_code = cra.site_code 				
				AND cra.record_status = 'Active'

			--SELECT * FROM #dist_with_cra
		
		--

		

		-- Calculate vaulting in case of no cash pre availability
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #feeder_level_forecast Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		drop table if exists #feeder_level_forecast_original_1

		SELECT 
		indentcode,
		indentdate,
		project_id, 
		bank_code, 
		feeder_branch_code
		,sol_id
		,cra
		,is_currency_chest
		,is_indent_required
		,withdrawal_type
		,SUM(max_loading_amount) max_loading_amount
		--,SUM(cassette_50_capacity_amount)	AS max_loading_amount_50
		,SUM(max_loading_capacity_amount_100)	AS max_loading_amount_100
		,SUM(max_loading_capacity_amount_200)	AS max_loading_amount_200
		,SUM(max_loading_capacity_amount_500)	AS max_loading_amount_500
		,SUM(max_loading_capacity_amount_2000) AS max_loading_amount_2000
		,SUM(min_loading_amount)			AS min_loading_amount
		,SUM(forecast_loading_amount)		AS forecast_loading_amount
		--,SUM(forecast_loading_amount_50)	AS forecast_loading_amount_50
		,SUM(forecast_loading_amount_100)	AS forecast_loading_amount_100
		,SUM(forecast_loading_amount_200)	AS forecast_loading_amount_200
		,SUM(forecast_loading_amount_500)	AS forecast_loading_amount_500
		,SUM(forecast_loading_amount_2000)	AS forecast_loading_amount_2000
		,SUM(total_cassette_capacity)		AS total_cassette_capacity
		,SUM(remaining_capacity_amount_100	) AS remaining_capacity_amount_100
		,SUM(remaining_capacity_amount_200	) AS remaining_capacity_amount_200
		,SUM(remaining_capacity_amount_500	) AS remaining_capacity_amount_500
		,SUM(remaining_capacity_amount_2000	) AS remaining_capacity_amount_2000
		,SUM(total_remaining_capacity_amount) AS total_remaining_capacity_amount
		--,SUM(cassette_50_capacity_amount)	AS cassette_50_capacity_amount
		--,SUM(cassette_100_capacity_amount)	AS cassette_100_capacity_amount
		--,SUM(cassette_200_capacity_amount)	AS cassette_200_capacity_amount
		--,SUM(cassette_500_capacity_amount)	AS cassette_500_capacity_amount
		--,SUM(cassette_2000_capacity_amount) AS cassette_2000_capacity_amount

	INTO #feeder_level_forecast_original_1
	FROM
		(
			SELECT 
				indentcode,
				indentdate,
				atm_id,
				sol_id, 
				project_id, 
				bank_code, 
				feeder_branch_code,
				cra,
				--forecasted_amt_50 AS forecast_loading_amount_50, 
				forecasted_amt_100 AS forecast_loading_amount_100,
				forecasted_amt_200 AS forecast_loading_amount_200,
				forecasted_amt_500 AS forecast_loading_amount_500,
				forecasted_amt_2000 AS forecast_loading_amount_2000,
				(COALESCE(forecasted_amt_100,0) + 
				COALESCE(forecasted_amt_200,0) + COALESCE(forecasted_amt_500,0) + 
				COALESCE(forecasted_amt_2000,0)
				) AS forecast_loading_amount
				,total_cassette_capacity
				,(
					SELECT MIN(v) FROM (VALUES (bank_cash_limit), 
					(insurance_limit),
					 (total_cassette_capacity)) AS value(v)) 
					 AS max_loading_amount
				,
				(	 CASE  
						WHEN curbal_div_avgDisp>= decidelimitdays 
							THEN 0         
						WHEN decidelimitdays=1 
							THEN 1.5*avgdispense
						WHEN cashout=1 
							THEN (CASE 
									WHEN  loadingGap_cur_bal_avg_disp >100000 
										THEN loadingGap_cur_bal_avg_disp
										ELSE loadingGap_cur_bal_avg_disp 
									END)
							ELSE  loadingGap_cur_bal_avg_disp  
						END	) 
						AS min_loading_amount	
						
				,  max_loading_capacity_amount_100
				,  max_loading_capacity_amount_200
				,  max_loading_capacity_amount_500
				,  max_loading_capacity_amount_2000
				,  is_currency_chest
				,	is_indent_required
				,	withdrawal_type
				,	remaining_capacity_amount_100
				,	remaining_capacity_amount_200
				,	remaining_capacity_amount_500
				,	remaining_capacity_amount_2000
				,	total_remaining_capacity_amount

				FROM #dist_with_cra

				)atm
				 GROUP by project_id, bank_code, feeder_branch_code,indentdate,indentcode,cra,sol_id,is_currency_chest,is_indent_required,withdrawal_type


				--select * from #dist_with_cra
				--select * from #feeder_level_forecast_original_1 where feeder_branch_code = 'KARAD'

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #feeder_level_forecast Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			

			
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #feeder_level_forecast Completed'

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Generate indent short code started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Generate indent short code started'
			-----------------------------------------------------------------START--------------------------------------------
			drop table if exists #feeder_level_forecast_short_code

			select *, dbo.fn_generate_shortcode(6) as indent_short_code
			into #feeder_level_forecast_short_code
			from #feeder_level_forecast_original_1

			drop table if exists #temp_duplicate_feeder_level_forecast

			select * into #temp_duplicate_feeder_level_forecast from #feeder_level_forecast_short_code
				where 
				indent_short_code in
					(
						select indent_short_code from #feeder_level_forecast_short_code group by indent_short_code having count(*) > 1
							union
						select indent_short_code from distribution_planning_master_V2 where record_status = 'Active' and indent_short_code is not null
					)
				--or indent_short_code is null
				--or len(LTRIM(rtrim(indent_short_code))) = 0

			drop table if exists #temp_unique_feeder_level_forecast
			
			select * into #temp_unique_feeder_level_forecast from #feeder_level_forecast_short_code
			where indent_short_code not in(select indent_short_code from #temp_duplicate_feeder_level_forecast);

			while (1!=2)
			begin 
				if exists(select 1 from #temp_duplicate_feeder_level_forecast)
				begin
					
					drop table if exists #refreshed_feeder_level_forecast
					
					select	*
							, dbo.fn_generate_shortcode(6) as indent_short_code  
						into #refreshed_feeder_level_forecast
					from #feeder_level_forecast_original_1 
					where indentcode in (select indentcode from #temp_duplicate_feeder_level_forecast)
					
					drop table if exists #refreshed_unique_feeder_level_forecast

					select * into #refreshed_unique_feeder_level_forecast from #refreshed_feeder_level_forecast where indent_short_code not in 
									(
										select indent_short_code from #refreshed_feeder_level_forecast group by indent_short_code having count(*) > 1
											union
										select indent_short_code from #temp_unique_feeder_level_forecast 
											union 
										select indent_short_code from distribution_planning_master_V2 where record_status = 'Active'
									)

					delete from #temp_duplicate_feeder_level_forecast where indentcode in(select indentcode from #refreshed_unique_feeder_level_forecast )

					insert into #temp_unique_feeder_level_forecast 
					select 
						* 
					from #refreshed_unique_feeder_level_forecast

				end
				else
				begin
					-- There is no duplicate short_code so break this loop.
					break
				end	
			end

			drop table if exists #feeder_level_forecast
			SELECT * into #feeder_level_forecast FROM #temp_unique_feeder_level_forecast
	
			---CLEANUP TEMP TABLES
			drop table if exists #feeder_level_forecast_original_1
			drop table if exists #feeder_level_forecast_short_code
			drop table if exists #temp_duplicate_feeder_level_forecast
			drop table if exists #temp_unique_feeder_level_forecast			
			drop table if exists #refreshed_feeder_level_forecast
			drop table if exists #refreshed_unique_feeder_level_forecast
			
			
			-----------------------------------------------------------------COMPLETED--------------------------------------------
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Generate indent short code Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Generate indent short code Completed'
			
			--select * from #feeder_level_forecast where feeder_branch_code = 'KARAD'
			--select * from #final_feeder_level_indent

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #final_feeder_level_indent Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			
			--SELECT 22100000-760000

		--DECLARE @MaxDestinationDate DATE
		--DECLARE @dispenseformula NVARCHAR(50)
		--DECLARE @confidence_factor NVARCHAR(50)
		--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		--DECLARE @denomination_wise_round_off_100 INT			 -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_200 int      
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100_currency_chest int      
		--DECLARE @denomination_wise_round_off_200_currency_chest int
		--DECLARE @denomination_wise_round_off_500_currency_chest int
		--DECLARE @denomination_wise_round_off_2000_currency_chest INT
		--declare @default_avg_dispense int
		--declare @vaulting_for_normal_weekday_percentage float,
		--		@vaulting_for_normal_weekend_percentage float,
		--		@vaulting_for_extended_weekend_percentage float
		
		
		--Select	@confidence_factor = confidence_factor
		--, @dispenseformula = dispenseformula
		--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		--, @denomination_wise_round_off_100_currency_chest = denomination_wise_round_off_100_currency_chest
		--, @denomination_wise_round_off_200_currency_chest = denomination_wise_round_off_200_currency_chest
		--, @denomination_wise_round_off_500_currency_chest = denomination_wise_round_off_500_currency_chest
		--, @denomination_wise_round_off_2000_currency_chest = denomination_wise_round_off_2000_currency_chest
		--,@default_avg_dispense = default_average_dispense
		--, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
		--, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
		--, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
		-- from system_settings where record_status = 'Active'

			DROP TABLE IF EXISTS #final_feeder_level_indent
		 --  DECLARE @dateT DATETIME = '2019-05-10'
			  SELECT * 
					,	vault_balance_100 + indent_100 - forecast_loading_amount_100 as vault_closing_balance_100
					,	vault_balance_200 + indent_200 - forecast_loading_amount_200 as vault_closing_balance_200
					,	vault_balance_500 + indent_500 - forecast_loading_amount_500 as vault_closing_balance_500
					,	vault_balance_2000 + indent_2000 - forecast_loading_amount_2000 as vault_closing_balance_2000
					,	total_vault_balance + (indent_100 + indent_200 + indent_500 + indent_2000) - forecast_loading_amount as total_vault_closing_balance
			  INTO #final_feeder_level_indent
			  FROM
			  (
			  
			  Select FL.*  ,
					a.vaultingamount_50,
					a.vaultingamount_100,
					a.vaultingamount_200,
					a.vaultingamount_500,
					a.vaultingamount_2000,
					a.total_rounded_vault_amt,
					ISNULL(VCB.vault_balance_100,0) as vault_balance_100 ,
					ISNULL(VCB.vault_balance_200,0) as vault_balance_200,
					ISNULL(VCB.vault_balance_500,0) as vault_balance_500,
					ISNULL(VCB.vault_balance_2000,0) as vault_balance_2000,
					ISNULL(VCB.total_vault_balance,0) as total_vault_balance,
					CASE WHEN ISNULL(cAST(a.vaultingamount_100 as BIGINT),0) >= ISNULL(CAST(VCB.vault_balance_100 as BIGINT),0)
						THEN 
							ISNULL(a.vaultingamount_100,0) - ISNULL(VCB.vault_balance_100,0) 
						ELSE
							ISNULL(VCB.vault_balance_100,0) - ISNULL(a.vaultingamount_100,0) 
					END
						as final_vaulting_amount_100,
					--ISNULL(a.vaultingamount_200,0) - ISNULL(VCB.vault_balance_200,0) as final_vaulting_amount_200,
					--ISNULL(a.vaultingamount_500,0) - ISNULL(VCB.vault_balance_500,0) as final_vaulting_amount_500,
					--ISNULL(a.vaultingamount_2000,0) - ISNULL(VCB.vault_balance_2000,0) as final_vaulting_amount_2000,
					--ISNULL(a.total_rounded_vault_amt,0) - ISNULL(VCB.total_vault_balance,0) as final_vaulting_amount_total,
					CASE WHEN ISNULL(a.vaultingamount_200,0) >= ISNULL(VCB.vault_balance_200,0)
						THEN 
							ISNULL(a.vaultingamount_200,0) - ISNULL(VCB.vault_balance_200,0) 
						ELSE
							ISNULL(VCB.vault_balance_200,0) - ISNULL(a.vaultingamount_200,0) 
					END
						as final_vaulting_amount_200,
					CASE WHEN CAST(ISNULL(a.vaultingamount_500,0) AS BIGINT) >= CAST(ISNULL(VCB.vault_balance_500,0) AS BIGINT)
						THEN 
							CAST(ISNULL(a.vaultingamount_500,0) AS BIGINT)  - CAST(ISNULL(VCB.vault_balance_500,0) AS BIGINT)
						ELSE
							CAST(ISNULL(VCB.vault_balance_500,0) AS BIGINT) - CAST(ISNULL(a.vaultingamount_500,0) AS BIGINT)
					END
						as final_vaulting_amount_500, 
					--VCB.vault_balance_500 - a.vaultingamount_500 as final_vaulting_amount_500,
					CASE WHEN ISNULL(a.vaultingamount_2000,0) >= ISNULL(VCB.vault_balance_2000,0)
						THEN 
							ISNULL(a.vaultingamount_2000,0) - ISNULL(VCB.vault_balance_2000,0) 
						ELSE
							ISNULL(VCB.vault_balance_2000,0) - ISNULL(a.vaultingamount_2000,0) 
					END
						as final_vaulting_amount_2000,
					CASE WHEN ISNULL(a.total_rounded_vault_amt,0) >= ISNULL(VCB.total_vault_balance,0)
						THEN 
							ISNULL(a.total_rounded_vault_amt,0) - ISNULL(VCB.total_vault_balance,0) 
						ELSE
							ISNULL(VCB.total_vault_balance,0) - ISNULL(a.total_rounded_vault_amt,0) 
					END
						as final_vaulting_amount_total,



					--FL.forecast_loading_amount_50 as indent_50,
					CASE 
					WHEN (
							FL.forecast_loading_amount_100 - 
								(
								CASE 
									WHEN floor(ISNULL(vault_balance_100,0)  / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 < 0
										THEN 
											0
										ELSE
											floor(ISNULL(vault_balance_100,0)  / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
									END
									)
								+ ISNULL(a.vaultingamount_100 ,0)
							) < 0 
						THEN 0
						ELSE 
							FL.forecast_loading_amount_100 - 
								(
								CASE 
									WHEN floor(ISNULL(vault_balance_100,0) / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 < 0
										THEN 
											0
										ELSE
											floor(ISNULL(vault_balance_100,0)  / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
									END
									)
								+ ISNULL(a.vaultingamount_100 ,0)
					END as indent_100,
					CASE 
					WHEN (
							FL.forecast_loading_amount_200 - 
								(
								CASE 
									WHEN floor(ISNULL(vault_balance_200,0) / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 < 0
										THEN 
											0
										ELSE
											floor(ISNULL(vault_balance_200,0) / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
									END
									)
								+ ISNULL(a.vaultingamount_200 ,0)
							) < 0
						THEN 0
						ELSE 
							FL.forecast_loading_amount_200 - 
								(
								CASE 
									WHEN floor(ISNULL(vault_balance_200,0) / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 < 0
										THEN 
											0
										ELSE
											floor(ISNULL(vault_balance_200,0) / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
									END
									)
								+ ISNULL(a.vaultingamount_200 ,0)
					END as indent_200,
					CASE 
					WHEN (
							FL.forecast_loading_amount_500 - 
								(
								CASE 
									WHEN floor(ISNULL(vault_balance_500,0) / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 < 0
										THEN 
											0
										ELSE
											floor(ISNULL(vault_balance_500,0) / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
									END
									)
								+ ISNULL(a.vaultingamount_500 ,0)
							) < 0
						THEN 0
						ELSE 
							FL.forecast_loading_amount_500 - 
								(
								CASE 
									WHEN floor(ISNULL(vault_balance_500,0) / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 < 0
										THEN 
											0
										ELSE
											floor(ISNULL(vault_balance_500,0) / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
									END
									)
								+ ISNULL(a.vaultingamount_500 ,0)
					END as indent_500,
					CASE 
					WHEN (
							FL.forecast_loading_amount_2000 - 
								(
								CASE 
									WHEN floor(ISNULL(vault_balance_2000,0) / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 < 0
										THEN 
											0
										ELSE
											floor(ISNULL(vault_balance_2000,0) / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
									END
									)
								+ ISNULL(a.vaultingamount_2000 ,0)
							) < 0
						THEN 0
						ELSE 
							FL.forecast_loading_amount_2000 - 
								(
								CASE 
									WHEN floor(ISNULL(vault_balance_2000,0) / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 < 0
										THEN 
											0
										ELSE
											floor(ISNULL(vault_balance_2000,0) / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
									END
									)
								+ ISNULL(a.vaultingamount_2000 ,0)
					END as indent_2000
          from #feeder_level_forecast FL
		   left join (
			Select 
				bank_code,project_id,feeder_branch_code,indentdate,
				SUM(rounded_vaultingamount_50)   as		vaultingamount_50,
				SUM(rounded_vaultingamount_100)  as		vaultingamount_100,
				SUM(rounded_vaultingamount_200)  as		vaultingamount_200,
				SUM(rounded_vaultingamount_500)  as		vaultingamount_500,   
				SUM(rounded_vaultingamount_2000) as		vaultingamount_2000,
				SUM(total_rounded_vault_amt)     as		total_rounded_vault_amt			      
			   from #dist_with_cra 			
					group by  bank_code,feeder_branch_code,project_id,indentdate-- add cra and indent_code
					)a
					on FL.bank_code=a.bank_code
					and FL.project_id=a.project_id
					and FL.feeder_branch_code=a.feeder_branch_code

					LEFT JOIN vault_cash_balance VCB
					on FL.bank_code=VCB.bank_name
					AND vcb.cra_name = fl.cra
					and FL.feeder_branch_code=VCB.feeder_branch_name
					and cast(VCB.Date as date) = cast(@dateT-1 as date)
					and VCB.record_status ='Active'
					and len(ltrim(rtrim(isnull(atm_id,'')))) < 4 
		)a

		--SELECT * from #final_feeder_level_indent where feeder_branch_code='PIMPRI'
		--e
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #final_feeder_level_indent Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			  
			DROP TABLE IF EXISTS #availability_dataset
			CREATE TABLE #availability_dataset
			(
				project_id						nvarchar(50)  COLLATE DATABASE_DEFAULT
			,	bank_code						nvarchar(50)  COLLATE DATABASE_DEFAULT
			,	feeder_branch_code				nvarchar(50)  COLLATE DATABASE_DEFAULT
			,	cra								nvarchar(50)  COLLATE DATABASE_DEFAULT
			,	Total_Amount_Available			BIGINT NULL
			,	available_100_amount          	BIGINT NULL
			,   available_200_amount			BIGINT NULL
			,   available_500_amount			BIGINT NULL
			,   available_2000_amount			BIGINT NULL
			,   is_deno_wise_cash_available			INT NULL
			
			) 


			  --select * from #final_feeder_level_indent
			  --SELECT * FROM #feeder_level_forecast WHERE bank_code = 'DENA'

			  -- SELECT * FROM distribution_planning_detail

			  ------------------------------------------ Cash Pre Availability Code ---------------------------------------------------------

			--SELECT * FROM #dist_with_cra where feeder_branch_code = 'KOLHAPUR'
			 -- SELECT * from #feeder_level_forecast
			--  SELECT * FROM Cash_pre_availability
				--DECLARE @dateT DATETIME = '2018-11-05'
			-- select f.*,a.Total_Amount_Available,a.available_100_amount, a.available_200_amount, a.available_500_amount, a.available_2000_amount  
			--,case when (a.available_100_amount + a.available_200_amount + a.available_500_amount + a.available_2000_amount) = a.Total_Amount_Available then
			--1 else 0 end as is_deno_wise_cash_available 
			--into #feeder_level_dataset
			--from #feeder_level_forecast f 
			--LEFT join Cash_pre_availability a
			--on f.project_id = a.project_id 
			--	and f.bank_code=a.bank_code 
			--	and a.Applied_to_level = 'Feeder'
			--	and f.feeder_branch_code = a.Feeder_Branch_Code
			--	and ( @dateT between a.from_date and a.to_date )
			--	and a.record_status = 'Active'
			
			--DECLARE @dateT DATETIME = '2019-02-14'

			IF (@is_revised_forecasting = 'No')
			BEGIN
			INSERT INTO #availability_dataset
			select	project_id,
					bank_code,
					feeder_branch_code,
					cra,
					COALESCE(a.Total_Amount_Available,0) as Total_Amount_Available,
					COALESCE(a.available_100_amount,0) as  available_100_amount,
					COALESCE(a.available_200_amount, 0) as available_200_amount,
					COALESCE(a.available_500_amount, 0) as available_500_amount,
					COALESCE(a.available_2000_amount,0) as available_2000_amount
			,
				case 
				when (COALESCE(a.available_100_amount,0) + 
					  COALESCE(a.available_200_amount,0) + 
					    COALESCE(a.available_500_amount,0) + 
						 COALESCE(a.available_2000_amount,0)) =  COALESCE(a.Total_Amount_Available,0) then
			1 else 0 end as is_deno_wise_cash_available 
			from  Cash_pre_availability a
			WHERE a.Applied_to_level = 'Feeder'
				and ( @dateT between a.from_date and a.to_date )
				and a.record_status = 'Active'
			END

			IF (@is_revised_forecasting = 'Yes')
			BEGIN
			INSERT INTO #availability_dataset
			select	project_id,
					bank_code,
					feeder_branch_code,
					cra,
					COALESCE(req.Total_Amount_Available,0) as Total_Amount_Available,
					COALESCE(req.available_100_amount,0) as  available_100_amount,
					COALESCE(req.available_200_amount, 0) as available_200_amount,
					COALESCE(req.available_500_amount, 0) as available_500_amount,
					COALESCE(req.available_2000_amount,0) as available_2000_amount
			,
				case 
				when (	COALESCE(available_100_amount,0) + 
						COALESCE(available_200_amount,0) + 
						COALESCE(available_500_amount,0) + 
						COALESCE(available_2000_amount,0)) =  COALESCE(Total_Amount_Available,0) 
				then
			1 else 0 end as is_deno_wise_cash_available  
			from indent_revision_request req 
			where indent_order_number = @active_indent_code and record_status = 'Approved'

			--IF EXISTS
			--	(
			--		SELECT 1 FROM atm_list_for_revision
			--		WHERE project_id = @for_project_id
			--		AND bank_code = @for_bank_code
			--		AND feeder_branch_code = @for_feeder_branch
			--		AND cast(for_date as date)=  cast(@dateT as date)
			--		AND record_status = 'Active'
			--	)
			--	BEGIN
			--		SET @is_atm_list_available = 'Yes'
			--	END
			--	ELSE
			--	BEGIN 
			--		SET @is_atm_list_available = 'No'
			--	END

			END
			--SELECT @is_atm_list_available
			--select * from #feeder_level_dataset
			IF EXISTS 
				(	
					SELECT 1 FROM #final_feeder_level_indent
					WHERE is_indent_required = 'Yes' and withdrawal_type = 'Vault'
				)
			BEGIN 
				INSERT INTO #availability_dataset
					select	project_id,
							bank_code,
							feeder_branch_code,
							cra,
							COALESCE(a.total_vault_balance ,0) as Total_Amount_Available,
							COALESCE(a.vault_balance_100,0) as  available_100_amount,
							COALESCE(a.vault_balance_200, 0) as available_200_amount,
							COALESCE(a.vault_balance_500, 0) as available_500_amount,
							COALESCE(a.vault_balance_2000,0) as available_2000_amount
					,
						case 
						when (COALESCE(a.vault_balance_100,0) + 
							  COALESCE(a.vault_balance_200,0) + 
								COALESCE(a.vault_balance_500,0) + 
								 COALESCE(a.vault_balance_2000,0)) =  COALESCE(a.total_vault_balance,0) then
					1 else 0 end as is_deno_wise_cash_available 
					from  #final_feeder_level_indent a
					WHERE is_indent_required = 'Yes' and withdrawal_type = 'Vault'
			END

			
			--select * from #availability_dataset
		
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into ##feeder_level_dataset Completed'

			DROP TABLE IF EXISTS #feeder_level_dataset

			SELECT feeder.*,
				   avail.Total_Amount_Available,
				   avail.available_100_amount,
				   avail.available_200_amount,
				   avail.available_500_amount,
				   available_2000_amount,
				   is_deno_wise_cash_available
			INTO #feeder_level_dataset
			FROM #final_feeder_level_indent feeder
			LEFT JOIN #availability_dataset	avail
			on feeder.bank_code = avail.bank_code
			AND feeder.project_id = avail.project_id
			AND feeder.feeder_branch_code = avail.feeder_branch_code

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Started curosr to allocate amount', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		--	select * from #dist_with_cra
		--	select * from #feeder_level_dataset where feeder_branch_code = 'KOLHAPUR'
 --DECLARE @Execution_log_str varchar(max)
 --DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
	--	DECLARE @denomination_wise_round_off_500 int
	--	DECLARE @denomination_wise_round_off_2000 int
	--	DECLARE @denomination_wise_round_off_100 INT

	--	Select	
	--	 @denomination_wise_round_off_100 = denomination_wise_round_off_100 
	--	, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
	--	, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
	--	, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
	--	 from system_settings where record_status = 'Active'	
 
	--DECLARE @dateT DATETIME = '2019-04-29'
	--DECLARE @cur_user varchar(50) = 'SA'
	--DECLARE @created_reference_id varchar(40) = 'C10060367031559'
	-----------------DECLARE @for_project_id varchar(50) = 'MOF_MAH'
	---------------------DECLARE @for_bank_code varchar(40) = 'SBI'
	---------------------DECLARE @for_feeder_branch nvarchar(50) = 'PIMPRI, PUNE_00575'
	---------------------DECLARE @is_revised_forecasting varchar(10) = 'Yes'
	---------------------DECLARE @active_indent_code	NVARCHAR(max) = 'SBI/MOF_MAH/PIMPRI, PUNE_00575/Logicash/20190422175115'

	--DECLARE @for_project_id varchar(50) = 'MOF_MAH'
	--DECLARE @for_bank_code varchar(40) = 'BOMH'
	--DECLARE @for_feeder_branch nvarchar(50) = 'AMRAVATI'
	--DECLARE @is_revised_forecasting varchar(10) = 'Yes'
	--DECLARE @active_indent_code	NVARCHAR(max) = 'BOMH/MOF_MAH/AMRAVATI/LOGICASH/20190517131231'
	--DECLARE @is_atm_list_available varchar(10)
	--declare @timestamp_date datetime =  DATEADD(MI,330,GETUTCDATE())
	--DECLARE @out varchar(50) 
	--DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
	--declare @Execution_log_str nvarchar(max)
	----DECLARE @outputVal varchar(100)
	--declare @feederbranchcode varchar(max)
	--DECLARE @indentcounter INT 
	--DECLARE @shortcode NVARCHAR(50)
	--DECLARE @MaxDestinationDate DATE
	--	DECLARE @dispenseformula NVARCHAR(50)
	--	DECLARE @confidence_factor NVARCHAR(50)
	--	DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
	--	DECLARE @denomination_wise_round_off_100 INT			 -- for rounding, replace with 100000
	--	DECLARE @denomination_wise_round_off_200 int      
	--	DECLARE @denomination_wise_round_off_500 int
	--	DECLARE @denomination_wise_round_off_2000 int
	--	DECLARE @denomination_wise_round_off_100_currency_chest int      
	--	DECLARE @denomination_wise_round_off_200_currency_chest int
	--	DECLARE @denomination_wise_round_off_500_currency_chest int
	--	DECLARE @denomination_wise_round_off_2000_currency_chest INT
	--	declare @default_avg_dispense int
	--	declare @vaulting_for_normal_weekday_percentage float,
	--			@vaulting_for_normal_weekend_percentage float,
	--			@vaulting_for_extended_weekend_percentage float
		
		
		--Select	@confidence_factor = confidence_factor
		--, @dispenseformula = dispenseformula
		--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		--, @denomination_wise_round_off_100_currency_chest = denomination_wise_round_off_100_currency_chest
		--, @denomination_wise_round_off_200_currency_chest = denomination_wise_round_off_200_currency_chest
		--, @denomination_wise_round_off_500_currency_chest = denomination_wise_round_off_500_currency_chest
		--, @denomination_wise_round_off_2000_currency_chest = denomination_wise_round_off_2000_currency_chest
		--,@default_avg_dispense = default_average_dispense
		--, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
		--, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
		--, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
		-- from system_settings where record_status = 'Active'


 DROP TABLE IF EXISTS #temp_cash_pre_availability

CREATE TABLE #temp_cash_pre_availability
(
	project_id		nvarchar(50)
,	bank_code		nvarchar(50)
,   feeder_branch_code nvarchar(50)
,	atmid		nvarchar(50)
,	total_opening_remaining_available_amount		bigint
,	opening_remaining_available_amount_100		bigint
,	opening_remaining_available_amount_200		bigint
,	opening_remaining_available_amount_500		bigint
,	opening_remaining_available_amount_2000		bigint
,	original_total_forecasted_amt		bigint
,	original_forecasted_amt_100		bigint
,	original_forecasted_amt_200		bigint
,	original_forecasted_amt_500		bigint
,	original_forecasted_amt_2000		bigint
,	morning_balance_100		bigint
,	morning_balance_200		bigint
,	morning_balance_500		bigint
,	morning_balance_2000		bigint
,	total_morning_balance		bigint
,	cassette_100_count_original		bigint
,	cassette_200_count_original		bigint
,	cassette_500_count_original		bigint
,	cassette_2000_count_original		bigint
,	cassette_100_brand_capacity		bigint
,	cassette_200_brand_capacity		bigint
,	cassette_500_brand_capacity		bigint
,	cassette_2000_brand_capacity		bigint
,	total_capacity_amount_100		bigint
,	total_capacity_amount_200		bigint
,	total_capacity_amount_500		bigint
,	total_capacity_amount_2000		bigint
,	denomination_100_max_capacity_percentage		bigint
,	denomination_200_max_capacity_percentage		bigint
,	denomination_500_max_capacity_percentage		bigint
,	denomination_2000_max_capacity_percentage		bigint
,	max_amt_allowed_100		bigint
,	max_amt_allowed_200		bigint
,	max_amt_allowed_500		bigint
,	max_amt_allowed_2000		bigint
,	denomination_wise_round_off_100		bigint
,	denomination_wise_round_off_200		bigint
,	denomination_wise_round_off_500		bigint
,	denomination_wise_round_off_2000		bigint
,	tentative_loading_100		bigint
,	tentative_loading_200		bigint
,	tentative_loading_500		bigint
,	tentative_loading_2000		bigint
,	rounded_tentative_loading_100		bigint
,	rounded_tentative_loading_200		bigint
,	rounded_tentative_loading_500		bigint
,	rounded_tentative_loading_2000		bigint
,	deno_100_priority		bigint
,	deno_200_priority		bigint
,	deno_500_priority		bigint
,	deno_2000_priority		bigint
,	is_deno_wise_cash_available		int
,	priority_1_is_denomination_100		int
,	priority_1_is_denomination_200		int
,	priority_1_is_denomination_500		int
,	priority_1_is_denomination_2000		int
,	priority_1_is_remaining_amount_available_100		int
,	priority_1_is_remaining_amount_available_200		int
,	priority_1_is_remaining_amount_available_500		int
,	priority_1_is_remaining_amount_available_2000		int
,	priority_1_is_remaining_capacity_available_100		int
,	priority_1_is_remaining_capacity_available_200		int
,	priority_1_is_remaining_capacity_available_500		int
,	priority_1_is_remaining_capacity_available_2000		int
,	priority_1_loading_amount_100		bigint
,	priority_1_loading_amount_200		bigint
,	priority_1_loading_amount_500		bigint
,	priority_1_loading_amount_2000		bigint
,	priority_2_is_denomination_100		int
,	priority_2_is_denomination_200		int
,	priority_2_is_denomination_500		int
,	priority_2_is_denomination_2000		int
,	priority_2_is_remaining_amount_available_100		int
,	priority_2_is_remaining_amount_available_200		int
,	priority_2_is_remaining_amount_available_500		int
,	priority_2_is_remaining_amount_available_2000		int
,	priority_2_is_remaining_capacity_available_100		int
,	priority_2_is_remaining_capacity_available_200		int
,	priority_2_is_remaining_capacity_available_500		int
,	priority_2_is_remaining_capacity_available_2000		int
,	priority_2_loading_amount_100		bigint
,	priority_2_loading_amount_200		bigint
,	priority_2_loading_amount_500		bigint
,	priority_2_loading_amount_2000		bigint
,	priority_3_is_denomination_100		int
,	priority_3_is_denomination_200		int
,	priority_3_is_denomination_500		int
,	priority_3_is_denomination_2000		int
,	priority_3_is_remaining_amount_available_100		int
,	priority_3_is_remaining_amount_available_200		int
,	priority_3_is_remaining_amount_available_500		int
,	priority_3_is_remaining_amount_available_2000		int
,	priority_3_is_remaining_capacity_available_100		int
,	priority_3_is_remaining_capacity_available_200		int
,	priority_3_is_remaining_capacity_available_500		int
,	priority_3_is_remaining_capacity_available_2000		int
,	priority_3_loading_amount_100		bigint
,	priority_3_loading_amount_200		bigint
,	priority_3_loading_amount_500		bigint
,	priority_3_loading_amount_2000		bigint
,	priority_4_is_denomination_100		int
,	priority_4_is_denomination_200		int
,	priority_4_is_denomination_500		int
,	priority_4_is_denomination_2000		int
,	priority_4_is_remaining_amount_available_100		int
,	priority_4_is_remaining_amount_available_200		int
,	priority_4_is_remaining_amount_available_500		int
,	priority_4_is_remaining_amount_available_2000		int
,	priority_4_is_remaining_capacity_available_100		int
,	priority_4_is_remaining_capacity_available_200		int
,	priority_4_is_remaining_capacity_available_500		int
,	priority_4_is_remaining_capacity_available_2000		int
,	priority_4_loading_amount_100		bigint
,	priority_4_loading_amount_200		bigint
,	priority_4_loading_amount_500		bigint
,	priority_4_loading_amount_2000		bigint
,	loading_amount_100		bigint
,	loading_amount_200		bigint
,	loading_amount_500		bigint
,	loading_amount_2000		bigint
,	total_loading_amount		bigint
,	remaining_capacity_amount_100		bigint
,	remaining_capacity_amount_200		bigint
,	remaining_capacity_amount_500		bigint
,	remaining_capacity_amount_2000		bigint
,   closing_remaining_available_amount_100      bigint
,   closing_remaining_available_amount_200		bigint
,   closing_remaining_available_amount_500		bigint
,   closing_remaining_available_amount_2000		bigint
,   total_closing_remaining_available_amount	bigint
,	total_forecasted_remaining_amt		bigint
)

DROP TABLE IF EXISTS #temp_feeder_forecast
CREATE TABLE #temp_feeder_forecast
(
	project_id							nvarchar(50)
,	bank_code							nvarchar(50)
,	feeder_branch_code					nvarchar(50)
,	is_deno_wise_cash_available			INT    NULL
,	total_remaining_available_amount	BIGINT NULL
,   remaining_avail_100					BIGINT NULL
,   remaining_avail_200					BIGINT NULL
,   remaining_avail_500					BIGINT NULL
,   remaining_avail_2000				BIGINT NULL

)


--declare @feederbranchcode varchar(max)
--declare @atmid varchar(50)
declare @availableAmount BIGINT
DECLARE @avail_100 BIGINT
DECLARE @avail_200 BIGINT
DECLARE @avail_500 BIGINT
DECLARE @avail_2000 BIGINT
--DECLARE @forecast_amt BIGINT

		
		
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Cursor strated for allocating amount'


 DECLARE cursor1 CURSOR READ_ONLY
   FOR
    SELECT project_id
, bank_code
, feeder_branch_code
,total_amount_available
,available_100_amount
,available_200_amount
,available_500_amount
,available_2000_amount
,is_deno_wise_cash_available
	FROM #feeder_level_dataset  where Total_Amount_Available > 0
	ORDER BY feeder_branch_code


	declare @total_remaining_available_amount BIGINT
	, @remaining_avail_100 BIGINT
	, @remaining_avail_200 BIGINT
	, @remaining_avail_500 BIGINT
	, @remaining_avail_2000 BIGINT
	, @total_original_available_amount BIGINT
	, @original_avail_100 BIGINT
	, @original_avail_200 BIGINT
	, @original_avail_500 BIGINT
	, @original_avail_2000 BIGINT
	, @loading_amount_100 BIGINT
	, @loading_amount_200 BIGINT
	, @loading_amount_500 BIGINT
	, @loading_amount_2000 BIGINT
	, @is_deno_wise_cash_available INT
	, @project_id nvarchar(50)
	, @bank_code nvarchar(50)
	OPEN cursor1

	FETCH NEXT FROM cursor1 INTO 
@project_id
,@bank_code
, @feederbranchcode
, @total_original_available_amount
, @original_avail_100
, @original_avail_200
, @original_avail_500
, @original_avail_2000
, @is_deno_wise_cash_available
    
    WHILE @@FETCH_STATUS = 0  
    BEGIN		
				SET @total_remaining_available_amount	= @total_original_available_amount
				SET @remaining_avail_100				= @original_avail_100
				SET @remaining_avail_200				= @original_avail_200
				SET @remaining_avail_500				= @original_avail_500
				SET @remaining_avail_2000				= @original_avail_2000
				
				--IF(@is_atm_list_available = 'No')
				--BEGIN

				--	IF CURSOR_STATUS('golbal','cursor2')>=-1
				--	BEGIN
				--		DEALLOCATE cursor2
				--	END
					DECLARE cursor2 cursor Read_Only
					For
					
					-- New 
					SELECT	
							dist.atm_id,					
							 CASE 
								WHEN CashOut = 1
								THEN 1
								WHEN atm.atm_band = 'Platinum'
								THEN 2
								WHEN atm.atm_band = 'Gold'
								THEN 3	
								WHEN atm.atm_band = 'Silver'
								THEN 4
								ELSE 5
								END as atm_priority
							, COALESCE(dist.denomination_100_max_capacity_percentage ,sa.deno_100_max_capacity_percentage)   as denomination_100_max_capacity_percentage
							, COALESCE(dist.denomination_200_max_capacity_percentage ,sa.deno_200_max_capacity_percentage)  as denomination_200_max_capacity_percentage
							, COALESCE(dist.denomination_500_max_capacity_percentage ,sa.deno_500_max_capacity_percentage)  as denomination_500_max_capacity_percentage
							, COALESCE(dist.denomination_2000_max_capacity_percentage,sa.deno_2000_max_capacity_percentage) as denomination_2000_max_capacity_percentage
							, morning_balance_100
							, morning_balance_200
							, morning_balance_500
							, morning_balance_2000
							, total_morning_balance
							, forecasted_amt_100
							, forecasted_amt_200
							, forecasted_amt_500
							, forecasted_amt_2000	
							, total_forecasted_amt
							--, cassette_50_count_original					
							, cassette_100_count_original 
							, cassette_200_count_original 
							, cassette_500_count_original 
							, cassette_2000_count_original
							, COALESCE(dist.deno_100_priority ,sa.deno_100_priority)   as deno_100_priority
							, COALESCE(dist.deno_200_priority ,sa.deno_200_priority)   as deno_200_priority
							, COALESCE(dist.deno_500_priority ,sa.deno_500_priority)   as deno_500_priority
							, COALESCE(dist.deno_2000_priority,sa.deno_2000_priority)  as deno_2000_priority
							, cassette_100_brand_capacity
							, cassette_200_brand_capacity
							, cassette_500_brand_capacity
							, cassette_2000_brand_capacity
							, limit_amount
					from #dist_with_cra  dist
					LEFT JOIN atm_master atm
					on atm.atm_id =  dist.atm_id and atm.site_code = dist.site_code 
					and atm.record_status = 'Active' and atm.site_status = 'Active'
					CROSS JOIN system_settings sa
					WHERE dist.project_id = @project_id and dist.bank_code = @bank_code and feeder_branch_code = @feederbranchcode
					AND sa.record_status = 'Active'
					ORDER BY atm_priority
				
				--ELSE
				--BEGIN
				--	IF CURSOR_STATUS('local','cursor2')>=-1
				--	BEGIN
				--		DEALLOCATE cursor2
				--	END
				--	DECLARE cursor2 cursor Read_Only
				--	For
					
				--	-- New 
				--	SELECT	
				--			dist.atm_id,					
				--			 CASE 
				--				WHEN CashOut = 1
				--				THEN 1
				--				WHEN atm.atm_band = 'Platinum'
				--				THEN 2
				--				WHEN atm.atm_band = 'Gold'
				--				THEN 3	
				--				WHEN atm.atm_band = 'Silver'
				--				THEN 4
				--				ELSE 5
				--				END as atm_priority
				--			, COALESCE(dist.denomination_100_max_capacity_percentage ,sa.deno_100_max_capacity_percentage)   as denomination_100_max_capacity_percentage
				--			, COALESCE(dist.denomination_200_max_capacity_percentage ,sa.deno_200_max_capacity_percentage)  as denomination_200_max_capacity_percentage
				--			, COALESCE(dist.denomination_500_max_capacity_percentage ,sa.deno_500_max_capacity_percentage)  as denomination_500_max_capacity_percentage
				--			, COALESCE(dist.denomination_2000_max_capacity_percentage,sa.deno_2000_max_capacity_percentage) as denomination_2000_max_capacity_percentage
				--			, morning_balance_100
				--			, morning_balance_200
				--			, morning_balance_500
				--			, morning_balance_2000
				--			, total_morning_balance
				--			, forecasted_amt_100
				--			, forecasted_amt_200
				--			, forecasted_amt_500
				--			, forecasted_amt_2000	
				--			, total_forecasted_amt
				--			--, cassette_50_count_original					
				--			, cassette_100_count_original 
				--			, cassette_200_count_original 
				--			, cassette_500_count_original 
				--			, cassette_2000_count_original
				--			, COALESCE(dist.deno_100_priority ,sa.deno_100_priority)   as deno_100_priority
				--			, COALESCE(dist.deno_200_priority ,sa.deno_200_priority)   as deno_200_priority
				--			, COALESCE(dist.deno_500_priority ,sa.deno_500_priority)   as deno_500_priority
				--			, COALESCE(dist.deno_2000_priority,sa.deno_2000_priority)  as deno_2000_priority
				--			, cassette_100_brand_capacity
				--			, cassette_200_brand_capacity
				--			, cassette_500_brand_capacity
				--			, cassette_2000_brand_capacity
				--			, limit_amount
				--	from #dist_with_cra  dist
				--	LEFT JOIN atm_master atm
				--	on atm.atm_id =  dist.atm_id and atm.site_code = dist.site_code 
				--	and atm.record_status = 'Active' and atm.site_status = 'Active'
				--	INNER JOIN atm_list_for_revision list
				--	on list.atm_id = dist.atm_id AND list.site_code = dist.site_code
				--	AND list.record_status = 'Active' 
				--	AND CAST(list.for_date as date) = '2019-04-29'					
				--	CROSS JOIN system_settings sa
				--	WHERE dist.project_id = 'MOF_MAH' and dist.bank_code = 'BOMH' and dist.feeder_branch_code = 'AMRAVATI'
				--	AND sa.record_status = 'Active'
				--	ORDER BY atm_priority
				--END

					declare @atmid varchar(50)												
							, @atm_priority INT												
							, @denomination_100_max_capacity_percentage bigint
							, @denomination_200_max_capacity_percentage bigint
							, @denomination_500_max_capacity_percentage bigint
							, @denomination_2000_max_capacity_percentage bigint
							, @morning_balance_100 bigint
							, @morning_balance_200 bigint
							, @morning_balance_500 bigint
							, @morning_balance_2000 bigint
							, @total_morning_balance BIGINT
							, @original_forecasted_amt_100  bigint
							, @original_forecasted_amt_200  bigint
							, @original_forecasted_amt_500  bigint
							, @original_forecasted_amt_2000 bigint
							, @original_total_forecasted_amt BIGINT
							--, @cassette_50_count_original   BIGINT							
							, @cassette_100_count_original 	bigint
							, @cassette_200_count_original 	bigint
							, @cassette_500_count_original 	bigint
							, @cassette_2000_count_original	bigint		
							, @deno_100_priority   BIGINT
							, @deno_200_priority	  BIGINT
							, @deno_500_priority	  BIGINT
							, @deno_2000_priority  BIGINT
							, @cassette_100_brand_capacity 	bigint		
							, @cassette_200_brand_capacity	bigint		
							, @cassette_500_brand_capacity	bigint		
							, @cassette_2000_brand_capacity	bigint		
							, @limit_amount bigint
				    OPEN cursor2
					FETCH NEXT FROM cursor2 
						  INTO  							
							 @atmid							
							 ,@atm_priority						
							, @denomination_100_max_capacity_percentage
							, @denomination_200_max_capacity_percentage
							, @denomination_500_max_capacity_percentage
							, @denomination_2000_max_capacity_percentage
							, @morning_balance_100
							, @morning_balance_200
							, @morning_balance_500
							, @morning_balance_2000
							, @total_morning_balance
							, @original_forecasted_amt_100
							, @original_forecasted_amt_200
							, @original_forecasted_amt_500
							, @original_forecasted_amt_2000
							, @original_total_forecasted_amt
							--, @cassette_50_count_original  
							, @cassette_100_count_original 
							, @cassette_200_count_original 
							, @cassette_500_count_original 
							, @cassette_2000_count_original
							, @deno_100_priority
							, @deno_200_priority
							, @deno_500_priority
							, @deno_2000_priority
							, @cassette_100_brand_capacity
							, @cassette_200_brand_capacity
							, @cassette_500_brand_capacity
							, @cassette_2000_brand_capacity
							, @limit_amount
					WHILE @@FETCH_STATUS=0
					BEGIN			---- Cursor 2 Begin
						DECLARE @forecasted_remaining_amt_100  bigint
							, @forecasted_remaining_amt_200  bigint
							, @forecasted_remaining_amt_500  bigint
							, @forecasted_remaining_amt_2000 bigint

							, @total_forecasted_remaining_amt BIGINT
							, @remaining_capacity_amount_100  bigint
							, @remaining_capacity_amount_200  bigint
							, @remaining_capacity_amount_500  bigint
							, @remaining_capacity_amount_2000 bigint
							, @total_remaining_capacity_amount BIGINT

							, @total_opening_remaining_available_amount BIGINT
							, @opening_remaining_available_amount_100 BIGINT
							, @opening_remaining_available_amount_200 BIGINT
							, @opening_remaining_available_amount_500 BIGINT
							, @opening_remaining_available_amount_2000 BIGINT


						SET  @total_opening_remaining_available_amount = @total_remaining_available_amount
						SET	 @opening_remaining_available_amount_100 =  @remaining_avail_100
						SET	 @opening_remaining_available_amount_200 =  @remaining_avail_200
						SET	 @opening_remaining_available_amount_500 = @remaining_avail_500
						SET	 @opening_remaining_available_amount_2000 = @remaining_avail_2000

						set @forecasted_remaining_amt_100 = @original_forecasted_amt_100
						SET	@forecasted_remaining_amt_200 = @original_forecasted_amt_200
						SET	@forecasted_remaining_amt_500 = @original_forecasted_amt_500
						SET	@forecasted_remaining_amt_2000 = @original_forecasted_amt_2000
						SET @total_forecasted_remaining_amt = @original_total_forecasted_amt
						SET @remaining_capacity_amount_100   = @remaining_capacity_amount_100
						SET @remaining_capacity_amount_200   = @remaining_capacity_amount_200
						SET @remaining_capacity_amount_500  = @remaining_capacity_amount_500
						SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000
						SET @total_remaining_capacity_amount = @total_remaining_capacity_amount
						

						-- GET TOTAL CAPACITY AMOUNT
						DECLARE @total_capacity_amount_100 bigint
							, @total_capacity_amount_200 bigint
							, @total_capacity_amount_500 bigint 
							, @total_capacity_amount_2000 bigint

						SET @total_capacity_amount_100 = @cassette_100_count_original * @cassette_100_brand_capacity * 100
						SET @total_capacity_amount_200 = @cassette_200_count_original * @cassette_200_brand_capacity * 200
						SET @total_capacity_amount_500 = @cassette_500_count_original * @cassette_500_brand_capacity * 500
						SET @total_capacity_amount_2000 = @cassette_2000_count_original * @cassette_2000_brand_capacity * 2000
						
						SET @remaining_capacity_amount_100   = @total_capacity_amount_100
						SET @remaining_capacity_amount_200   = @total_capacity_amount_200 
						SET @remaining_capacity_amount_500   = @total_capacity_amount_500 
						SET @remaining_capacity_amount_2000  = @total_capacity_amount_2000
						SET @total_remaining_capacity_amount = @total_capacity_amount_100 + @total_capacity_amount_200  +  @total_capacity_amount_500 + @total_capacity_amount_2000
						
						-- GET TOTAL CAPACITY AMOUNT LIMIT TO CASSETTE CAPACITY PERCENTAGE

						DECLARE @max_amt_allowed_100 BIGINT,
								@max_amt_allowed_200 BIGINT,
								@max_amt_allowed_500 BIGINT,
								@max_amt_allowed_2000 BIGINT

						SET @max_amt_allowed_100 =  @total_capacity_amount_100 * @denomination_100_max_capacity_percentage / 100
						SET @max_amt_allowed_200 =  @total_capacity_amount_200 * @denomination_200_max_capacity_percentage / 100
						SET @max_amt_allowed_500 =  @total_capacity_amount_500 * @denomination_500_max_capacity_percentage / 100
						SET @max_amt_allowed_2000 =  @total_capacity_amount_2000 * @denomination_2000_max_capacity_percentage / 100

						-- Calculate  Deduct Morning balance
						DECLARE @tentative_loading_100 BIGINT,
								@tentative_loading_200 BIGINT,
								@tentative_loading_500 BIGINT,
								@tentative_loading_2000 BIGINT

						SET @tentative_loading_100   =  ABS(@max_amt_allowed_100 -  CASE  WHEN  (@max_amt_allowed_100 = 0) 
																				THEN 0 
																				ELSE @morning_balance_100
																				END)
						SET @tentative_loading_200   = ABS( @max_amt_allowed_200 - CASE  WHEN  (@max_amt_allowed_200 = 0) 
																				THEN 0 
																				ELSE @morning_balance_200
																				END)
						SET @tentative_loading_500   =  ABS(@max_amt_allowed_500 - CASE  WHEN  (@max_amt_allowed_500 = 0) 
																				THEN 0 
																				ELSE @morning_balance_500
																				END)
						SET @tentative_loading_2000  =  ABS(@max_amt_allowed_2000 - CASE  WHEN  (@max_amt_allowed_2000 = 0) 
																				THEN 0 
																				ELSE @morning_balance_2000
																				END)
						--- Calculate rounded tentative loading

						DECLARE @rounded_tentative_loading_100 BIGINT,
								@rounded_tentative_loading_200 BIGINT,
								@rounded_tentative_loading_500 BIGINT,
								@rounded_tentative_loading_2000 BIGINT

						SET @rounded_tentative_loading_100   =   @tentative_loading_100 - (@tentative_loading_100 % @denomination_wise_round_off_100 )
						SET @rounded_tentative_loading_200   =   @tentative_loading_200 - (@tentative_loading_200 % @denomination_wise_round_off_200 )
						SET @rounded_tentative_loading_500   =   @tentative_loading_500 - (@tentative_loading_500 % @denomination_wise_round_off_500 )
						SET @rounded_tentative_loading_2000   =   @tentative_loading_2000 - (@tentative_loading_2000 % @denomination_wise_round_off_2000 )

						DECLARE @priority_1_is_denomination_100  BIGINT = 0,
									@priority_1_is_denomination_200 BIGINT = 0,
									@priority_1_is_denomination_500 BIGINT = 0,
									@priority_1_is_denomination_2000 BIGINT = 0,
									@priority_1_is_remaining_amount_available_100 BIGINT = 0,
									@priority_1_is_remaining_amount_available_200 BIGINT = 0,
									@priority_1_is_remaining_amount_available_500 BIGINT = 0,
									@priority_1_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_1_loading_amount_100 BIGINT = 0,
									@priority_1_loading_amount_200 BIGINT = 0,
									@priority_1_loading_amount_500 BIGINT = 0,
									@priority_1_loading_amount_2000 BIGINT = 0,
									@priority_2_is_denomination_100  BIGINT = 0,
									@priority_2_is_denomination_200 BIGINT = 0,
									@priority_2_is_denomination_500 BIGINT = 0,
									@priority_2_is_denomination_2000 BIGINT = 0,
									@priority_2_is_remaining_amount_available_100 BIGINT = 0,
									@priority_2_is_remaining_amount_available_200 BIGINT = 0,
									@priority_2_is_remaining_amount_available_500 BIGINT = 0,
									@priority_2_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_2_loading_amount_100 BIGINT = 0,
									@priority_2_loading_amount_200 BIGINT = 0,
									@priority_2_loading_amount_500 BIGINT = 0,
									@priority_2_loading_amount_2000 BIGINT = 0,
									@priority_3_is_denomination_100  BIGINT = 0,
									@priority_3_is_denomination_200 BIGINT = 0,
									@priority_3_is_denomination_500 BIGINT = 0,
									@priority_3_is_denomination_2000 BIGINT = 0,
									@priority_3_is_remaining_amount_available_100 BIGINT = 0,
									@priority_3_is_remaining_amount_available_200 BIGINT = 0,
									@priority_3_is_remaining_amount_available_500 BIGINT = 0,
									@priority_3_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_3_loading_amount_100 BIGINT = 0,
									@priority_3_loading_amount_200 BIGINT = 0,
									@priority_3_loading_amount_500 BIGINT = 0,
									@priority_3_loading_amount_2000 BIGINT = 0,
									@priority_4_is_denomination_100  BIGINT = 0,
									@priority_4_is_denomination_200 BIGINT = 0,
									@priority_4_is_denomination_500 BIGINT = 0,
									@priority_4_is_denomination_2000 BIGINT = 0,
									@priority_4_is_remaining_amount_available_100 BIGINT = 0,
									@priority_4_is_remaining_amount_available_200 BIGINT = 0,
									@priority_4_is_remaining_amount_available_500 BIGINT = 0,
									@priority_4_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_4_loading_amount_100 BIGINT = 0,
									@priority_4_loading_amount_200 BIGINT = 0,
									@priority_4_loading_amount_500 BIGINT = 0,
									@priority_4_loading_amount_2000 BIGINT = 0
							
							SET @loading_amount_100 = 0
							SET @loading_amount_200 = 0
							SET @loading_amount_500 = 0
							SET @loading_amount_2000 = 0
		

						IF(@is_deno_wise_cash_available = 1)
						BEGIN
							-- CONTINUE FOR ALLOCATION LOGIC
							IF (@total_forecasted_remaining_amt > 0)
							BEGIN
								IF (@deno_100_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_100 = 1
											SET @priority_1_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											-- Round @priority_max_loading_capacity_amount_100 to minimum
											IF (floor(@priority_1_max_loading_capacity_amount_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100) < 0
											BEGIN 
												set @priority_1_max_loading_capacity_amount_100 = 0 
											END
											ELSE
											BEGIN
												set @priority_1_max_loading_capacity_amount_100 = floor(@priority_1_max_loading_capacity_amount_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
											END
											IF @remaining_avail_100 > @priority_1_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_100 = @priority_1_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 												
												if @remaining_avail_100 % @denomination_wise_round_off_100 = 0 
												begin
													SET @priority_1_loading_amount_100 = @remaining_avail_100
												end
												else 
												begin
													if (CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100) < @priority_1_max_loading_capacity_amount_100
														and
														 (CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_1_loading_amount_100 = CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
													end
													else
													begin
														if floor(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 < 0
														begin
															SET @priority_1_loading_amount_100 = 0
														end
														else
														begin
															SET @priority_1_loading_amount_100 = floor(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
														end
													end
												end												
											END

											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_100 = @priority_1_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_1_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_1_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 1)

								ELSE IF (@deno_200_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_200 = 1
											SET @priority_1_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											-- Round @priority_max_loading_capacity_amount to minimum
											IF (floor(@priority_1_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 ) < 0
											BEGIN
												set @priority_1_max_loading_capacity_amount_200 = 0 
											END -- END (floor(@priority_1_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 ) < 0
											ELSE
											BEGIN
												set @priority_1_max_loading_capacity_amount_200 = floor(@priority_1_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200	
											END	-- END of ELSE when (floor(@priority_1_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 ) < 0
											
											IF @remaining_avail_200 > @priority_1_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_200 = @priority_1_max_loading_capacity_amount_200
												--SELECT @priority_1_loading_amount_200
											END	-- END of @remaining_avail_200 > @priority_1_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												--------------------------------------
												--	When available amount is less than capacity then allocate only available amount 												
												if @remaining_avail_200 % @denomination_wise_round_off_200 = 0 
												begin
													SET @priority_1_loading_amount_200 = @remaining_avail_200
												end
												else 
												begin
													if (CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200) < @priority_1_max_loading_capacity_amount_200
														and
														 (CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_1_loading_amount_200 = CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
													end
													else
													begin
														if floor(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 < 0
														begin
															SET @priority_1_loading_amount_200 = 0
														end
														else
														begin
															SET @priority_1_loading_amount_200 = floor(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
														end
													end
												end												


												-------------------------------------
												--SET @priority_1_loading_amount_200 = @remaining_avail_200


												--SELECT @priority_1_loading_amount_200
											END		-- END of else
											--SELECT @priority_1_loading_amount_200
											--SELECT @priority_2_loading_amount_200
											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_200 = @priority_1_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_1_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_1_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 1)
									ELSE IF (@deno_500_priority = 1)
									BEGIN
										SET @priority_1_is_denomination_500 = 1
										IF (@remaining_avail_500 > 0)
										BEGIN
											SET @priority_1_is_remaining_amount_available_500 = 1
											IF (@rounded_tentative_loading_500 > 0 )
											BEGIN
												SET @priority_1_is_remaining_capacity_available_500 = 1
												SET @priority_1_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
												
												-- Round @priority_max_loading_capacity_amount to minimum
											IF (floor(@priority_1_max_loading_capacity_amount_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 ) < 0
											BEGIN
												set @priority_1_max_loading_capacity_amount_500 = 0 
											END
											ELSE
											BEGIN
												set @priority_1_max_loading_capacity_amount_500 = floor(@priority_1_max_loading_capacity_amount_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
											END
												
												IF @remaining_avail_500 > @priority_1_max_loading_capacity_amount_500
												BEGIN
													-- assigining amount as per available capacity
													SET @priority_1_loading_amount_500 = @priority_1_max_loading_capacity_amount_500
												END
												ELSE
												BEGIN
													--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_500 % @denomination_wise_round_off_500 = 0 
												begin
													SET @priority_1_loading_amount_500 = @remaining_avail_500
												end
												else 
												begin
													if (CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500) < @priority_1_max_loading_capacity_amount_500
														and
														 (CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_1_loading_amount_500 = CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
													end
													else
													begin
														if floor(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 < 0
														begin
															SET @priority_1_loading_amount_500 = 0
														end
														else
														begin
															SET @priority_1_loading_amount_500 = floor(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
														end
													end
												end		
													
													
													--SET @priority_1_loading_amount_500 = @remaining_avail_500
												END

												--Loading for priority 1 done
												--deduct from available amount
												SET @loading_amount_500 = @priority_1_loading_amount_500
												SET @remaining_avail_500 = @remaining_avail_500 - @priority_1_loading_amount_500
												SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_500
												SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_500
												SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_500
												SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_1_loading_amount_500
											
											END	-- END OF  (@rounded_tentative_loading_500 > 0 )
										END		-- END of (@remaining_avail_500 > 0)
										END		-- END OF (@deno_500_priority = 1)
								ELSE IF (@deno_2000_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_2000 = 1
											SET @priority_1_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											-- Round @priority_max_loading_capacity_amount to minimum
											IF (floor(@priority_1_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 ) < 0
											BEGIN
												set @priority_1_max_loading_capacity_amount_2000 = 0 
											END -- END (floor(@priority_1_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 ) < 0
											ELSE
											BEGIN
												set @priority_1_max_loading_capacity_amount_2000 = floor(@priority_1_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
											END ---- END for else part of (floor(@priority_1_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 ) < 0
											
											IF @remaining_avail_2000 > @priority_1_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_2000 = @priority_1_max_loading_capacity_amount_2000
											END -- END of @remaining_avail_2000 > @priority_1_max_loading_capacity_amount_2000
											ELSE
											BEGIN
											if @remaining_avail_2000 % @denomination_wise_round_off_2000 = 0 
												begin
													SET @priority_1_loading_amount_2000 = @remaining_avail_2000
												end
												else 
												BEGIN
												--	When available amount is less than capacity then allocate only available amount 
													if (CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000) < @priority_1_max_loading_capacity_amount_2000
															and
													   (CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000)  + @total_morning_balance < @limit_amount 														 
													begin
															SET @priority_1_loading_amount_2000 = CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
													end -- END when ceiling amount is less than capacity of cassette and limit amount
													else -- ELSE of when ceiling amount is less than capacity of cassette and limit amount
													begin
														if floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 < 0
															begin
																SET @priority_1_loading_amount_2000 = 0
															end -- END floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 < 0
														else
															begin
																SET @priority_1_loading_amount_2000 = floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
															end -- END for else part of floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 < 0
													end -- END for else part of when ceiling amount is less than capacity of cassette and limit amount
												END -- END of else part of @remaining_avail_2000 > @priority_1_max_loading_capacity_amount_2000
												--SET @priority_1_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_2000 = @priority_1_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_1_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_1_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 1)
													
		  -------------------------------------- Checking for 2nd priority denomination --------------------------------- 
								IF (@deno_100_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_100 = 1
											SET @priority_2_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											-- Round @priority_max_loading_capacity_amount to minimum
											IF (floor(@priority_2_max_loading_capacity_amount_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 ) < 0
											BEGIN
												 set @priority_2_max_loading_capacity_amount_100 = 0 
											END
											ELSE
											BEGIN
												set @priority_2_max_loading_capacity_amount_100 = floor(@priority_2_max_loading_capacity_amount_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
											END
											IF @remaining_avail_100 > @priority_2_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_100 = @priority_2_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												------------------------------------------------
												--	When available amount is less than capacity then allocate only available amount 												
												if @remaining_avail_100 % @denomination_wise_round_off_100 = 0 
												begin
													SET @priority_2_loading_amount_100 = @remaining_avail_100
												end
												else 
												begin
													if (CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100) < @priority_2_max_loading_capacity_amount_100
														and
														 (CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_2_loading_amount_100 = CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
													end
													else
													begin
														if floor(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 < 0
														begin
															SET @priority_2_loading_amount_100 = 0
														end
														else
														begin
															SET @priority_2_loading_amount_100 = floor(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
														end
													end
												END
												--------------------------------------------------
												--SET @priority_2_loading_amount_100 = @remaining_avail_100
											 END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_2_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_2_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_2_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 2)

								ELSE IF (@deno_200_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_200 = 1
											SET @priority_2_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF (floor(@priority_2_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 ) < 0
											BEGIN
												 set @priority_2_max_loading_capacity_amount_200 = 0 
											END
											ELSE
											BEGIN
												set @priority_2_max_loading_capacity_amount_200 = floor(@priority_2_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
											END
											
											IF @remaining_avail_200 > @priority_2_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_200 = @priority_2_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_2_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												
												if @remaining_avail_200 % @denomination_wise_round_off_200 = 0 
												begin
													SET @priority_2_loading_amount_200 = @remaining_avail_200
												end
												else 
												begin
													if (CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200) < @priority_2_max_loading_capacity_amount_200
														and
														 (CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_2_loading_amount_200 = CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
													end
													else
													begin
														if floor(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 < 0
														begin
															SET @priority_2_loading_amount_200 = 0
														end
														else
														begin
															SET @priority_2_loading_amount_200 = floor(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
														end
													end
													
												END								
												--SET @priority_2_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_2_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_2_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_2_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 2)
								ELSE IF (@deno_500_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_500 = 1
											SET @priority_2_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF (floor(@priority_2_max_loading_capacity_amount_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 ) < 0
											BEGIN
												 set @priority_2_max_loading_capacity_amount_500 = 0 
											END
											ELSE
											BEGIN
												set @priority_2_max_loading_capacity_amount_500 = floor(@priority_2_max_loading_capacity_amount_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
											END
											
											
											IF @remaining_avail_500 > @priority_2_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_500 = @priority_2_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												
												if @remaining_avail_500 % @denomination_wise_round_off_500 = 0 
												begin
													SET @priority_2_loading_amount_500 = @remaining_avail_500
												end
												else 
												begin
													if (CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500) < @priority_2_max_loading_capacity_amount_500
														and
														 (CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_2_loading_amount_500 = CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
													end
													else
													begin
														if floor(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 < 0
														begin
															SET @priority_2_loading_amount_500 = 0
														end
														else
														begin
															SET @priority_2_loading_amount_500 = floor(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
														end
													end
													END
												--SET @priority_2_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_2_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_2_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_2_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 2)
								ELSE IF (@deno_2000_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_2000 = 1
											SET @priority_2_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF (floor(@priority_2_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 ) < 0
											BEGIN
												 set @priority_2_max_loading_capacity_amount_2000 = 0 
											END
											ELSE
											BEGIN
												set @priority_2_max_loading_capacity_amount_2000 = floor(@priority_2_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
											END
											IF @remaining_avail_2000 > @priority_2_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_2000 = @priority_2_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_2000 % @denomination_wise_round_off_2000 = 0 
												begin
													SET @priority_2_loading_amount_2000 = @remaining_avail_2000
												end
												else 
												begin
													if (CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000) < @priority_2_max_loading_capacity_amount_2000
														and
														 (CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_2_loading_amount_2000 = CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
													end
													else
													begin
														if floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 < 0
														begin
															SET @priority_2_loading_amount_2000 = 0
														end
														else
														begin
															SET @priority_2_loading_amount_2000 = floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
														end
													end					
												END									
												--SET @priority_2_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_2_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_2_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_2_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 2)
			
			
			--------------------------------- Checking for 3rd priority denomination --------------------------------- 
								IF (@deno_100_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_100 = 1
											SET @priority_3_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF (floor(@priority_3_max_loading_capacity_amount_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 ) < 0
											BEGIN
												 set @priority_3_max_loading_capacity_amount_100 = 0 
											END
											ELSE
											BEGIN
												set @priority_3_max_loading_capacity_amount_100 = floor(@priority_3_max_loading_capacity_amount_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
											END
											
											IF @remaining_avail_100 > @priority_3_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_100 = @priority_3_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_100 % @denomination_wise_round_off_100 = 0 
												begin
													SET @priority_3_loading_amount_100 = @remaining_avail_100
												end
												else 
												begin
													if (CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100) < @priority_3_max_loading_capacity_amount_100
														and
														 (CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_3_loading_amount_100 = CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
													end
													else
													begin
														if floor(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 < 0
														begin
															SET @priority_3_loading_amount_100 = 0
														end
														else
														begin
															SET @priority_3_loading_amount_100 = floor(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
														end
													end
												END	
												--SET @priority_3_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 3 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_3_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_3_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_3_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 3)

								ELSE IF (@deno_200_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_200 = 1
											SET @priority_3_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											
											IF (floor(@priority_3_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 ) < 0
											BEGIN
												 set @priority_3_max_loading_capacity_amount_200 = 0 
											END
											ELSE
											BEGIN
												set @priority_3_max_loading_capacity_amount_200 = floor(@priority_3_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
											END
											
											IF @remaining_avail_200 > @priority_3_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_200 = @priority_3_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_3_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_200 % @denomination_wise_round_off_200 = 0 
												begin
													SET @priority_3_loading_amount_200 = @remaining_avail_200
												end
												else 
												begin
													if (CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200) < @priority_3_max_loading_capacity_amount_200
														and
														 (CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_3_loading_amount_200 = CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
													end
													else
													begin
														if floor(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 < 0
														begin
															SET @priority_3_loading_amount_200 = 0
														end
														else
														begin
															SET @priority_3_loading_amount_200 = floor(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
														end
													end
												
												END
												
												--SET @priority_3_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_3_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_3_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_3_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 3)
								ELSE IF (@deno_500_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_500 = 1
											SET @priority_3_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											
											IF (floor(@priority_3_max_loading_capacity_amount_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 ) < 0
											BEGIN
												 set @priority_3_max_loading_capacity_amount_500 = 0 
											END
											ELSE
											BEGIN
												set @priority_3_max_loading_capacity_amount_500 = floor(@priority_3_max_loading_capacity_amount_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
											END
											
											IF @remaining_avail_500 > @priority_3_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_500 = @priority_3_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_500 % @denomination_wise_round_off_500 = 0 
												begin
													SET @priority_3_loading_amount_500 = @remaining_avail_500
												end
												else 
												begin
													if (CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500) < @priority_3_max_loading_capacity_amount_500
														and
														 (CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_3_loading_amount_500 = CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
													end
													else
													begin
														if floor(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 < 0
														begin
															SET @priority_3_loading_amount_500 = 0
														end
														else
														begin
															SET @priority_3_loading_amount_500 = floor(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
														end
													end
												
												END
												
												--  SET @priority_3_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 3 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_3_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_3_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_3_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 3)
								ELSE IF (@deno_2000_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_2000 = 1
											SET @priority_3_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF (floor(@priority_3_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 ) < 0
											BEGIN
												 set @priority_3_max_loading_capacity_amount_2000 = 0 
											END
											ELSE
											BEGIN
												set @priority_3_max_loading_capacity_amount_2000 = floor(@priority_3_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
											END
											IF @remaining_avail_2000 > @priority_3_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_2000 = @priority_3_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_2000 % @denomination_wise_round_off_2000 = 0 
												begin
													SET @priority_3_loading_amount_2000 = @remaining_avail_2000
												end
												else 
												begin
													if (CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000) < @priority_3_max_loading_capacity_amount_2000
														and
														 (CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_3_loading_amount_2000 = CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
													end
													else
													begin
														if floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 < 0
														begin
															SET @priority_3_loading_amount_2000 = 0
														end
														else
														begin
															SET @priority_3_loading_amount_2000 = floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
														end
													end
												END
																												
												-- SET @priority_3_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_3_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_3_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_3_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 3)
							
							
				---------------------------------------- Checking for 4th priority denomination --------------------------------- 
								IF (@deno_100_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_100 = 1
											SET @priority_4_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											
											IF (floor(@priority_4_max_loading_capacity_amount_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 ) < 0
											BEGIN
												 set @priority_4_max_loading_capacity_amount_100 = 0 
											END
											ELSE
											BEGIN
												set @priority_4_max_loading_capacity_amount_100 = floor(@priority_4_max_loading_capacity_amount_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
											END
											IF @remaining_avail_100 > @priority_4_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_100 = @priority_4_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_100 % @denomination_wise_round_off_100 = 0 
												begin
													SET @priority_4_loading_amount_100 = @remaining_avail_100
												end
												else 
												begin
													if (CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100) < @priority_4_max_loading_capacity_amount_100
														and
														 (CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_4_loading_amount_100 = CEILING(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
													end
													else
													begin
														if floor(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100 < 0
														begin
															SET @priority_4_loading_amount_100 = 0
														end
														else
														begin
															SET @priority_4_loading_amount_100 = floor(@remaining_avail_100 / @denomination_wise_round_off_100 * 1.0) * @denomination_wise_round_off_100
														end
													end							
												END
												
												-- SET @priority_4_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 4 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_4_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_4_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_4_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 4)

								ELSE IF (@deno_200_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_200 = 1
											SET @priority_4_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF (floor(@priority_4_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 ) < 0
											BEGIN
												 set @priority_4_max_loading_capacity_amount_200 = 0 
											END
											ELSE
											BEGIN
												set @priority_4_max_loading_capacity_amount_200 = floor(@priority_4_max_loading_capacity_amount_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
											END
											
											IF @remaining_avail_200 > @priority_4_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_200 = @priority_4_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_4_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_100 % @denomination_wise_round_off_100 = 0 
												begin
													SET @priority_4_loading_amount_200 = @remaining_avail_100
												end
												else 
												begin
													if (CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200) < @priority_4_max_loading_capacity_amount_200
														and
														 (CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_4_loading_amount_200 = CEILING(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
													end
													else
													begin
														if floor(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200 < 0
														begin
															SET @priority_4_loading_amount_200 = 0
														end
														else
														begin
															SET @priority_4_loading_amount_200 = floor(@remaining_avail_200 / @denomination_wise_round_off_200 * 1.0) * @denomination_wise_round_off_200
														end
													end
																								
												END
												
												-- SET @priority_4_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 4 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_4_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_4_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_4_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 4)
								ELSE IF (@deno_500_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_500 = 1
											SET @priority_4_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF (floor(@priority_4_max_loading_capacity_amount_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 ) < 0
											BEGIN
												 set @priority_4_max_loading_capacity_amount_500 = 0 
											END
											ELSE
											BEGIN
												set @priority_4_max_loading_capacity_amount_500 = floor(@priority_4_max_loading_capacity_amount_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
											END
											
											IF @remaining_avail_500 > @priority_4_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_500 = @priority_4_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_500 % @denomination_wise_round_off_500 = 0 
												begin
													SET @priority_4_loading_amount_500 = @remaining_avail_500
												end
												else 
												begin
													if (CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500) < @priority_4_max_loading_capacity_amount_500
														and
														 (CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_4_loading_amount_500 = CEILING(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
													end
													else
													begin
														if floor(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500 < 0
														begin
															SET @priority_4_loading_amount_500 = 0
														end
														else
														begin
															SET @priority_4_loading_amount_500 = floor(@remaining_avail_500 / @denomination_wise_round_off_500 * 1.0) * @denomination_wise_round_off_500
														end
													end
																								
											 END
												
												-- SET @priority_4_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_4_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_4_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_4_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 4)
								ELSE IF (@deno_2000_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_2000 = 1
											SET @priority_4_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											
											IF (floor(@priority_4_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 ) < 0
											BEGIN
												 set @priority_4_max_loading_capacity_amount_2000 = 0 
											END
											ELSE
											BEGIN
												set @priority_4_max_loading_capacity_amount_2000 = floor(@priority_4_max_loading_capacity_amount_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
											END
											IF @remaining_avail_2000 > @priority_4_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_2000 = @priority_4_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												if @remaining_avail_2000 % @denomination_wise_round_off_2000 = 0 
												begin
													SET @priority_4_loading_amount_2000 = @remaining_avail_2000
												end
												else 
												begin
													if (CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000) < @priority_4_max_loading_capacity_amount_2000
														and
														 (CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000) + (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)  + @total_morning_balance < @limit_amount 														 
													begin
														SET @priority_4_loading_amount_2000 = CEILING(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
													end
													else
													begin
														if floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000 < 0
														begin
															SET @priority_4_loading_amount_2000 = 0
														end
														else
														begin
															SET @priority_4_loading_amount_2000 = floor(@remaining_avail_2000 / @denomination_wise_round_off_2000 * 1.0) * @denomination_wise_round_off_2000
														end
													end
																						
												
												END
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_4_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_4_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_4_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 4)


							INSERT INTO #temp_cash_pre_availability
							(
										project_id
									,	bank_code
									,   feeder_branch_code
									,	atmid
									,	total_opening_remaining_available_amount
									,	opening_remaining_available_amount_100
									,	opening_remaining_available_amount_200
									,	opening_remaining_available_amount_500
									,	opening_remaining_available_amount_2000
									,	original_total_forecasted_amt
									,	original_forecasted_amt_100
									,	original_forecasted_amt_200
									,	original_forecasted_amt_500
									,	original_forecasted_amt_2000
									,	morning_balance_100
									,	morning_balance_200
									,	morning_balance_500
									,	morning_balance_2000
									,	total_morning_balance
									,	cassette_100_count_original
									,	cassette_200_count_original
									,	cassette_500_count_original
									,	cassette_2000_count_original
									,	cassette_100_brand_capacity
									,	cassette_200_brand_capacity
									,	cassette_500_brand_capacity
									,	cassette_2000_brand_capacity
									,	total_capacity_amount_100
									,	total_capacity_amount_200
									,	total_capacity_amount_500
									,	total_capacity_amount_2000
									,	denomination_100_max_capacity_percentage
									,	denomination_200_max_capacity_percentage
									,	denomination_500_max_capacity_percentage
									,	denomination_2000_max_capacity_percentage
									,	max_amt_allowed_100
									,	max_amt_allowed_200
									,	max_amt_allowed_500
									,	max_amt_allowed_2000
									,	denomination_wise_round_off_100
									,	denomination_wise_round_off_200
									,	denomination_wise_round_off_500
									,	denomination_wise_round_off_2000
									,	tentative_loading_100
									,	tentative_loading_200
									,	tentative_loading_500
									,	tentative_loading_2000
									,	rounded_tentative_loading_100
									,	rounded_tentative_loading_200
									,	rounded_tentative_loading_500
									,	rounded_tentative_loading_2000
									,	deno_100_priority
									,	deno_200_priority
									,	deno_500_priority
									,	deno_2000_priority
									,	is_deno_wise_cash_available
									,	priority_1_is_denomination_100
									,	priority_1_is_denomination_200
									,	priority_1_is_denomination_500
									,	priority_1_is_denomination_2000
									,	priority_1_is_remaining_amount_available_100
									,	priority_1_is_remaining_amount_available_200
									,	priority_1_is_remaining_amount_available_500
									,	priority_1_is_remaining_amount_available_2000
									,	priority_1_is_remaining_capacity_available_100
									,	priority_1_is_remaining_capacity_available_200
									,	priority_1_is_remaining_capacity_available_500
									,	priority_1_is_remaining_capacity_available_2000
									,	priority_1_loading_amount_100
									,	priority_1_loading_amount_200
									,	priority_1_loading_amount_500
									,	priority_1_loading_amount_2000
									,	priority_2_is_denomination_100
									,	priority_2_is_denomination_200
									,	priority_2_is_denomination_500
									,	priority_2_is_denomination_2000
									,	priority_2_is_remaining_amount_available_100
									,	priority_2_is_remaining_amount_available_200
									,	priority_2_is_remaining_amount_available_500
									,	priority_2_is_remaining_amount_available_2000
									,	priority_2_is_remaining_capacity_available_100
									,	priority_2_is_remaining_capacity_available_200
									,	priority_2_is_remaining_capacity_available_500
									,	priority_2_is_remaining_capacity_available_2000
									,	priority_2_loading_amount_100
									,	priority_2_loading_amount_200
									,	priority_2_loading_amount_500
									,	priority_2_loading_amount_2000
									,	priority_3_is_denomination_100
									,	priority_3_is_denomination_200
									,	priority_3_is_denomination_500
									,	priority_3_is_denomination_2000
									,	priority_3_is_remaining_amount_available_100
									,	priority_3_is_remaining_amount_available_200
									,	priority_3_is_remaining_amount_available_500
									,	priority_3_is_remaining_amount_available_2000
									,	priority_3_is_remaining_capacity_available_100
									,	priority_3_is_remaining_capacity_available_200
									,	priority_3_is_remaining_capacity_available_500
									,	priority_3_is_remaining_capacity_available_2000
									,	priority_3_loading_amount_100
									,	priority_3_loading_amount_200
									,	priority_3_loading_amount_500
									,	priority_3_loading_amount_2000
									,	priority_4_is_denomination_100
									,	priority_4_is_denomination_200
									,	priority_4_is_denomination_500
									,	priority_4_is_denomination_2000
									,	priority_4_is_remaining_amount_available_100
									,	priority_4_is_remaining_amount_available_200
									,	priority_4_is_remaining_amount_available_500
									,	priority_4_is_remaining_amount_available_2000
									,	priority_4_is_remaining_capacity_available_100
									,	priority_4_is_remaining_capacity_available_200
									,	priority_4_is_remaining_capacity_available_500
									,	priority_4_is_remaining_capacity_available_2000
									,	priority_4_loading_amount_100
									,	priority_4_loading_amount_200
									,	priority_4_loading_amount_500
									,	priority_4_loading_amount_2000
									,	loading_amount_100
									,	loading_amount_200
									,	loading_amount_500
									,	loading_amount_2000
									,	total_loading_amount
									,	remaining_capacity_amount_100
									,	remaining_capacity_amount_200
									,	remaining_capacity_amount_500
									,	remaining_capacity_amount_2000
									,   closing_remaining_available_amount_100
									,   closing_remaining_available_amount_200
									,   closing_remaining_available_amount_500
									,   closing_remaining_available_amount_2000
									,   total_closing_remaining_available_amount									
									,	total_forecasted_remaining_amt
							)
							SELECT  
									    @project_id								                                                            as project_id								
								   , @bank_code									                                                            as bank_code									
								   , @feederbranchcode																						as feeder_branch_code
								   , @atmid										                                                            as atmid										
									, @total_opening_remaining_available_amount																as total_opening_remaining_available_amount
								   , COALESCE(@opening_remaining_available_amount_100	,0)													as opening_remaining_available_amount_100	
								   , COALESCE(@opening_remaining_available_amount_200	,0)                                                 as opening_remaining_available_amount_200	
								   , COALESCE(@opening_remaining_available_amount_500	,0)                                                 as opening_remaining_available_amount_500	
								   , COALESCE(@opening_remaining_available_amount_2000	,0)                                                 as opening_remaining_available_amount_2000	
								
								   , @original_total_forecasted_amt							                                                as original_total_forecasted_amt							
								   , @original_forecasted_amt_100				                                                            as original_forecasted_amt_100				
								   , @original_forecasted_amt_200                                                                           as original_forecasted_amt_200
								   , @original_forecasted_amt_500                                                                           as original_forecasted_amt_500
								   , @original_forecasted_amt_2000                                                                          as original_forecasted_amt_2000
								   , @morning_balance_100                                                                                   as morning_balance_100
								   , @morning_balance_200                                                                                   as morning_balance_200
								   , @morning_balance_500                                                                                   as morning_balance_500
								   , @morning_balance_2000                                                                                  as morning_balance_2000
								   , @total_morning_balance                                                                                 as total_morning_balance
								   , @cassette_100_count_original                                                                           as cassette_100_count_original
								   , @cassette_200_count_original                                                                           as cassette_200_count_original
								   , @cassette_500_count_original                                                                           as cassette_500_count_original
								   , @cassette_2000_count_original                                                                          as cassette_2000_count_original
								   , @cassette_100_brand_capacity                                                                           as cassette_100_brand_capacity
								   , @cassette_200_brand_capacity                                                                           as cassette_200_brand_capacity
								   , @cassette_500_brand_capacity                                                                           as cassette_500_brand_capacity
								   , @cassette_2000_brand_capacity                                                                          as cassette_2000_brand_capacity
								   , @total_capacity_amount_100                                                                             as total_capacity_amount_100
								   , @total_capacity_amount_200                                                                             as total_capacity_amount_200
								   , @total_capacity_amount_500                                                                             as total_capacity_amount_500
								   , @total_capacity_amount_2000                                                                            as total_capacity_amount_2000
								   , @denomination_100_max_capacity_percentage                                                              as denomination_100_max_capacity_percentage
								   , @denomination_200_max_capacity_percentage                                                              as denomination_200_max_capacity_percentage
								   , @denomination_500_max_capacity_percentage                                                              as denomination_500_max_capacity_percentage
								   , @denomination_2000_max_capacity_percentage                                                             as denomination_2000_max_capacity_percentage
								   , @max_amt_allowed_100                                                                                   as max_amt_allowed_100
								   , @max_amt_allowed_200                                                                                   as max_amt_allowed_200
								   , @max_amt_allowed_500                                                                                   as max_amt_allowed_500
								   , @max_amt_allowed_2000                                                                                  as max_amt_allowed_2000
								   , @denomination_wise_round_off_100                                                                       as denomination_wise_round_off_100
								   , @denomination_wise_round_off_200                                                                       as denomination_wise_round_off_200
								   , @denomination_wise_round_off_500                                                                       as denomination_wise_round_off_500
								   , @denomination_wise_round_off_2000                                                                      as denomination_wise_round_off_2000
								   , @tentative_loading_100                                                                                 as tentative_loading_100 
								   , @tentative_loading_200                                                                                 as tentative_loading_200 
								   , @tentative_loading_500                                                                                 as tentative_loading_500 
								   , @tentative_loading_2000                                                                                as tentative_loading_2000
								   -- Max Capacity                                                                                          
								   , @rounded_tentative_loading_100		                                                                    as rounded_tentative_loading_100		
								   , @rounded_tentative_loading_200	                                                                        as rounded_tentative_loading_200	    
								   , @rounded_tentative_loading_500	                                                                        as rounded_tentative_loading_500	    
								   , @rounded_tentative_loading_2000	                                                                    as rounded_tentative_loading_2000	
								   , @deno_100_priority                                                                                     as deno_100_priority
								   , @deno_200_priority                                                                                     as deno_200_priority
								   , @deno_500_priority                                                                                     as deno_500_priority
								   , @deno_2000_priority                                                                                    as deno_2000_priority
								   , @is_deno_wise_cash_available                                                                           as is_deno_wise_cash_available
									---- If denomination wise is availabble                                                                 
									-- Priority of denomination                                                                             
								   , @priority_1_is_denomination_100                                                                        as priority_1_is_denomination_100
								   , @priority_1_is_denomination_200                                                                        as priority_1_is_denomination_200
								   , @priority_1_is_denomination_500                                                                        as priority_1_is_denomination_500
								   , @priority_1_is_denomination_2000 								   	                                    as priority_1_is_denomination_2000 								   	
								   , @priority_1_is_remaining_amount_available_100                                                          as priority_1_is_remaining_amount_available_100
								   , @priority_1_is_remaining_amount_available_200                                                          as priority_1_is_remaining_amount_available_200
								   , @priority_1_is_remaining_amount_available_500                                                          as priority_1_is_remaining_amount_available_500
								   , @priority_1_is_remaining_amount_available_2000                                                         as priority_1_is_remaining_amount_available_2000
								   , @priority_1_is_remaining_capacity_available_100                                                        as priority_1_is_remaining_capacity_available_100
								   , @priority_1_is_remaining_capacity_available_200                                                        as priority_1_is_remaining_capacity_available_200
								   , @priority_1_is_remaining_capacity_available_500                                                        as priority_1_is_remaining_capacity_available_500
								   , @priority_1_is_remaining_capacity_available_2000								                        as priority_1_is_remaining_capacity_available_2000								
								   , @priority_1_loading_amount_100                                                                         as priority_1_loading_amount_100
								   , @priority_1_loading_amount_200                                                                         as priority_1_loading_amount_200
								   , @priority_1_loading_amount_500                                                                         as priority_1_loading_amount_500
								   , @priority_1_loading_amount_2000								                                        as priority_1_loading_amount_2000								   
								   , @priority_2_is_denomination_100                                                                        as priority_2_is_denomination_100
								   , @priority_2_is_denomination_200                                                                        as priority_2_is_denomination_200
								   , @priority_2_is_denomination_500                                                                        as priority_2_is_denomination_500
								   , @priority_2_is_denomination_2000 								                                        as priority_2_is_denomination_2000 								   
								   , @priority_2_is_remaining_amount_available_100                                                          as priority_2_is_remaining_amount_available_100
								   , @priority_2_is_remaining_amount_available_200                                                          as priority_2_is_remaining_amount_available_200
								   , @priority_2_is_remaining_amount_available_500                                                          as priority_2_is_remaining_amount_available_500
								   , @priority_2_is_remaining_amount_available_2000                                                         as priority_2_is_remaining_amount_available_2000
								   , @priority_2_is_remaining_capacity_available_100                                                        as priority_2_is_remaining_capacity_available_100
								   , @priority_2_is_remaining_capacity_available_200                                                        as priority_2_is_remaining_capacity_available_200
								   , @priority_2_is_remaining_capacity_available_500                                                        as priority_2_is_remaining_capacity_available_500
								   , @priority_2_is_remaining_capacity_available_2000  								                        as priority_2_is_remaining_capacity_available_2000  								
								   , @priority_2_loading_amount_100                                                                         as priority_2_loading_amount_100
								   , @priority_2_loading_amount_200                                                                         as priority_2_loading_amount_200
								   , @priority_2_loading_amount_500                                                                         as priority_2_loading_amount_500
								   , @priority_2_loading_amount_2000								                                        as priority_2_loading_amount_2000								   
								   , @priority_3_is_denomination_100                                                                        as priority_3_is_denomination_100
								   , @priority_3_is_denomination_200                                                                        as priority_3_is_denomination_200
								   , @priority_3_is_denomination_500                                                                        as priority_3_is_denomination_500
								   , @priority_3_is_denomination_2000 								                                        as priority_3_is_denomination_2000 								   
								   , @priority_3_is_remaining_amount_available_100                                                          as priority_3_is_remaining_amount_available_100
								   , @priority_3_is_remaining_amount_available_200                                                          as priority_3_is_remaining_amount_available_200
								   , @priority_3_is_remaining_amount_available_500                                                          as priority_3_is_remaining_amount_available_500
								   , @priority_3_is_remaining_amount_available_2000								                            as priority_3_is_remaining_amount_available_2000								   
								   , @priority_3_is_remaining_capacity_available_100                                                        as priority_3_is_remaining_capacity_available_100
								   , @priority_3_is_remaining_capacity_available_200                                                        as priority_3_is_remaining_capacity_available_200
								   , @priority_3_is_remaining_capacity_available_500                                                        as priority_3_is_remaining_capacity_available_500
								   , @priority_3_is_remaining_capacity_available_2000  						                                as priority_3_is_remaining_capacity_available_2000  						
								   , @priority_3_loading_amount_100                                                                         as priority_3_loading_amount_100
								   , @priority_3_loading_amount_200                                                                         as priority_3_loading_amount_200
								   , @priority_3_loading_amount_500                                                                         as priority_3_loading_amount_500
								   , @priority_3_loading_amount_2000								                                        as priority_3_loading_amount_2000								   
								   , @priority_4_is_denomination_100                                                                        as priority_4_is_denomination_100
								   , @priority_4_is_denomination_200                                                                        as priority_4_is_denomination_200
								   , @priority_4_is_denomination_500                                                                        as priority_4_is_denomination_500
								   , @priority_4_is_denomination_2000 								                                        as priority_4_is_denomination_2000 								   
								   , @priority_4_is_remaining_amount_available_100                                                          as priority_4_is_remaining_amount_available_100
								   , @priority_4_is_remaining_amount_available_200                                                          as priority_4_is_remaining_amount_available_200
								   , @priority_4_is_remaining_amount_available_500                                                          as priority_4_is_remaining_amount_available_500
								   , @priority_4_is_remaining_amount_available_2000								                            as priority_4_is_remaining_amount_available_2000								   
								   , @priority_4_is_remaining_capacity_available_100                                                        as priority_4_is_remaining_capacity_available_100
								   , @priority_4_is_remaining_capacity_available_200                                                        as priority_4_is_remaining_capacity_available_200
								   , @priority_4_is_remaining_capacity_available_500                                                        as priority_4_is_remaining_capacity_available_500
								   , @priority_4_is_remaining_capacity_available_2000		                                                as priority_4_is_remaining_capacity_available_2000		
								   , @priority_4_loading_amount_100                                                                         as priority_4_loading_amount_100
								   , @priority_4_loading_amount_200                                                                         as priority_4_loading_amount_200
								   , @priority_4_loading_amount_500                                                                         as priority_4_loading_amount_500
								   , @priority_4_loading_amount_2000								                                        as priority_4_loading_amount_2000								   
								   , @loading_amount_100                                                                                    as loading_amount_100
								   , @loading_amount_200                                                                                    as loading_amount_200
								   , @loading_amount_500                                                                                    as loading_amount_500
								   , @loading_amount_2000                                                                                   as loading_amount_2000
									-- Total loading amount								                                                    
								   , @loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000                 as total_loading_amount
								   , @remaining_capacity_amount_100                                                                         as remaining_capacity_amount_100
								   , @remaining_capacity_amount_200                                                                         as remaining_capacity_amount_200
								   , @remaining_capacity_amount_500                                                                         as remaining_capacity_amount_500
								   , @remaining_capacity_amount_2000								                                        as remaining_capacity_amount_2000								   
								   --closing available amount	                                                                           
								   , @remaining_avail_100			                                                                        as closing_avail_100			
								   , @remaining_avail_200			                                                                        as closing_avail_200			
								   , @remaining_avail_500			                                                                        as closing_avail_500			
								   , @remaining_avail_2000                                                                                  as closing_avail_2000
								   , @total_remaining_available_amount                                                                      as total_closing_available_amount
								   , @total_forecasted_remaining_amt                                                                        as total_forecasted_remaining_amt

							END -- END OF (@total_forecasted_remaining_amt > 0)						

						 
										
									
							  
						END -- end of If denomination wise cash is available
						
						ELSE --If denomination wise cash is not available.
						BEGIN							-- Denomination wise not available BEGIN
							IF (@total_remaining_available_amount > @original_total_forecasted_amt)
							BEGIN						--
								SET @loading_amount_100 = @original_forecasted_amt_100
								SET @loading_amount_200 = @original_forecasted_amt_200
								SET @loading_amount_500 = @original_forecasted_amt_500
								SET @loading_amount_2000 = @original_forecasted_amt_2000								

								SET @total_remaining_available_amount = @total_remaining_available_amount - (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)
							END
							ELSE -- IF REMAINING AMOUNT IS LESS THAN FORCASTED AMOUNT
							BEGIN
								
								IF (@deno_100_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 1
										SET @priority_1_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 0
										SET @priority_1_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_100 = @priority_1_loading_amount_100
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_1_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_1_loading_amount_100
								END		-- end of (@deno_100_priority = 1 )

								ELSE IF (@deno_200_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 1
										SET @priority_1_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 0
										SET @priority_1_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_200 = @priority_1_loading_amount_200
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_1_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_1_loading_amount_200
								END		-- end of (@deno_200_priority = 1 )

								ELSE IF (@deno_500_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_500 = 1
										SET @priority_1_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_500 = 0
										SET @priority_1_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_500 = @priority_1_loading_amount_500
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_1_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_1_loading_amount_500
								END		-- end of (@deno_500_priority = 1 )

								ELSE IF (@deno_2000_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 1
										SET @priority_1_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 0
										SET @priority_1_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_2000 = @priority_1_loading_amount_2000
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_1_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_1_loading_amount_2000
								END		-- end of (@deno_2000_priority = 1 )
								----------------------------------Priority 2 start -------------------------------------------
								IF (@deno_100_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 1
										SET @priority_2_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 0
										SET @priority_2_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_100 = @priority_2_loading_amount_100
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_2_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_2_loading_amount_100
								END		-- end of (@deno_100_priority = 2 )

								ELSE IF (@deno_200_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 1
										SET @priority_2_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 0
										SET @priority_2_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_200 = @priority_2_loading_amount_200
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_2_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_2_loading_amount_200
								END		-- end of (@deno_200_priority = 2 )

								ELSE IF (@deno_500_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 1
										SET @priority_2_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 0
										SET @priority_2_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_500 = @priority_2_loading_amount_500
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_2_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_2_loading_amount_500
								END		-- end of (@deno_500_priority = 2 )

								ELSE IF (@deno_2000_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 1
										SET @priority_2_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 0
										SET @priority_2_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_2000 = @priority_2_loading_amount_2000
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_2_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_2_loading_amount_2000
								END		-- end of (@deno_2000_priority = 2 )

								---------------------------------Priority 3 start -------------------------------
								IF (@deno_100_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 1
										SET @priority_3_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 0
										SET @priority_3_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_100 = @priority_3_loading_amount_100
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_3_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_3_loading_amount_100
								END		-- end of (@deno_100_priority = 3 )

								ELSE IF (@deno_200_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 1
										SET @priority_3_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 0
										SET @priority_3_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_200 = @priority_3_loading_amount_200
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_3_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_3_loading_amount_200
								END		-- end of (@deno_200_priority = 1 )

								ELSE IF (@deno_500_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 1
										SET @priority_3_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 0
										SET @priority_3_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_500 = @priority_3_loading_amount_500
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_3_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_3_loading_amount_500
								END		-- end of (@deno_500_priority = 3 )

								ELSE IF (@deno_2000_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 1
										SET @priority_3_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 0
										SET @priority_3_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_2000 = @priority_3_loading_amount_2000
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_3_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_3_loading_amount_2000
								END		-- end of (@deno_2000_priority = 3 )

								----------------------------------priority 4 start------------------------------
								IF (@deno_100_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 1
										SET @priority_4_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 0
										SET @priority_4_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_100 = @priority_4_loading_amount_100
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_4_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_4_loading_amount_100
								END		-- end of (@deno_100_priority = 4 )

								ELSE IF (@deno_200_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 1
										SET @priority_4_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 0
										SET @priority_4_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_200 = @priority_4_loading_amount_200
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_4_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_4_loading_amount_200
								END		-- end of (@deno_200_priority = 4 )

								ELSE IF (@deno_500_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 1
										SET @priority_4_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 0
										SET @priority_4_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_500 = @priority_4_loading_amount_500
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_4_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_4_loading_amount_500
								END		-- end of (@deno_500_priority = 4 )

								ELSE IF (@deno_2000_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 1
										SET @priority_4_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 0
										SET @priority_4_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_2000 = @priority_4_loading_amount_2000
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_4_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_4_loading_amount_2000
								END		-- end of (@deno_2000_priority = 4 )
							END	-- END OF ELSE -- IF REMAINING AMOUNT IS LESS THAN FORCASTED AMOUNT

							-- insert into temp table
							
							INSERT INTO #temp_cash_pre_availability 
							(
													project_id
												,	bank_code
												,	feeder_branch_code
												,	atmid
												,	total_opening_remaining_available_amount
												,	opening_remaining_available_amount_100
												,	opening_remaining_available_amount_200
												,	opening_remaining_available_amount_500
												,	opening_remaining_available_amount_2000
												,	original_total_forecasted_amt
												,	original_forecasted_amt_100
												,	original_forecasted_amt_200
												,	original_forecasted_amt_500
												,	original_forecasted_amt_2000
												,	morning_balance_100
												,	morning_balance_200
												,	morning_balance_500
												,	morning_balance_2000
												,	total_morning_balance
												,	cassette_100_count_original
												,	cassette_200_count_original
												,	cassette_500_count_original
												,	cassette_2000_count_original
												,	cassette_100_brand_capacity
												,	cassette_200_brand_capacity
												,	cassette_500_brand_capacity
												,	cassette_2000_brand_capacity
												,	total_capacity_amount_100
												,	total_capacity_amount_200
												,	total_capacity_amount_500
												,	total_capacity_amount_2000
												,	denomination_100_max_capacity_percentage
												,	denomination_200_max_capacity_percentage
												,	denomination_500_max_capacity_percentage
												,	denomination_2000_max_capacity_percentage
												,	max_amt_allowed_100
												,	max_amt_allowed_200
												,	max_amt_allowed_500
												,	max_amt_allowed_2000
												,	denomination_wise_round_off_100
												,	denomination_wise_round_off_200
												,	denomination_wise_round_off_500
												,	denomination_wise_round_off_2000
												,	tentative_loading_100
												,	tentative_loading_200
												,	tentative_loading_500
												,	tentative_loading_2000
												,	rounded_tentative_loading_100
												,	rounded_tentative_loading_200
												,	rounded_tentative_loading_500
												,	rounded_tentative_loading_2000
												,	deno_100_priority
												,	deno_200_priority
												,	deno_500_priority
												,	deno_2000_priority
												,	is_deno_wise_cash_available
												,	priority_1_is_denomination_100
												,	priority_1_is_denomination_200
												,	priority_1_is_denomination_500
												,	priority_1_is_denomination_2000
												,	priority_1_is_remaining_amount_available_100
												,	priority_1_is_remaining_amount_available_200
												,	priority_1_is_remaining_amount_available_500
												,	priority_1_is_remaining_amount_available_2000
												,	priority_1_loading_amount_100
												,	priority_1_loading_amount_200
												,	priority_1_loading_amount_500
												,	priority_1_loading_amount_2000
												,	priority_2_is_denomination_100
												,	priority_2_is_denomination_200
												,	priority_2_is_denomination_500
												,	priority_2_is_denomination_2000
												,	priority_2_is_remaining_amount_available_100
												,	priority_2_is_remaining_amount_available_200
												,	priority_2_is_remaining_amount_available_500
												,	priority_2_is_remaining_amount_available_2000
												,	priority_2_loading_amount_100
												,	priority_2_loading_amount_200
												,	priority_2_loading_amount_500
												,	priority_2_loading_amount_2000
												,	priority_3_is_denomination_100
												,	priority_3_is_denomination_200
												,	priority_3_is_denomination_500
												,	priority_3_is_denomination_2000
												,	priority_3_is_remaining_amount_available_100
												,	priority_3_is_remaining_amount_available_200
												,	priority_3_is_remaining_amount_available_500
												,	priority_3_is_remaining_amount_available_2000
												,	priority_3_loading_amount_100
												,	priority_3_loading_amount_200
												,	priority_3_loading_amount_500
												,	priority_3_loading_amount_2000
												,	priority_4_is_denomination_100
												,	priority_4_is_denomination_200
												,	priority_4_is_denomination_500
												,	priority_4_is_denomination_2000
												,	priority_4_is_remaining_amount_available_100
												,	priority_4_is_remaining_amount_available_200
												,	priority_4_is_remaining_amount_available_500
												,	priority_4_is_remaining_amount_available_2000
												,	priority_4_loading_amount_100
												,	priority_4_loading_amount_200
												,	priority_4_loading_amount_500
												,	priority_4_loading_amount_2000
												,	loading_amount_100
												,	loading_amount_200
												,	loading_amount_500
												,	loading_amount_2000
												,	total_loading_amount
												,	total_closing_remaining_available_amount
												,	total_forecasted_remaining_amt
			)
			SELECT
									 @project_id								                                                            as project_id								
								   , @bank_code									                                                            as bank_code									
								   , @feederbranchcode																						as feeder_branch_code
								   , @atmid										                                                            as atmid
								   , @total_opening_remaining_available_amount																as total_opening_remaining_available_amount
								   , COALESCE(@opening_remaining_available_amount_100		,0)												as opening_remaining_available_amount_100	
								   , COALESCE(@opening_remaining_available_amount_200	    ,0)                                             as opening_remaining_available_amount_200	
								   , COALESCE(@opening_remaining_available_amount_500	    ,0)                                             as opening_remaining_available_amount_500	
								   , COALESCE(@opening_remaining_available_amount_2000	    ,0)                                             as opening_remaining_available_amount_2000											
								   , @original_total_forecasted_amt							                                                as original_total_forecasted_amt							
								   , @original_forecasted_amt_100				                                                            as original_forecasted_amt_100				
								   , @original_forecasted_amt_200                                                                           as original_forecasted_amt_200
								   , @original_forecasted_amt_500                                                                           as original_forecasted_amt_500
								   , @original_forecasted_amt_2000                                                                          as original_forecasted_amt_2000
								   , @morning_balance_100                                                                                   as morning_balance_100
								   , @morning_balance_200                                                                                   as morning_balance_200
								   , @morning_balance_500                                                                                   as morning_balance_500
								   , @morning_balance_2000                                                                                  as morning_balance_2000
								   , @total_morning_balance                                                                                 as total_morning_balance
								   , @cassette_100_count_original                                                                           as cassette_100_count_original
								   , @cassette_200_count_original                                                                           as cassette_200_count_original
								   , @cassette_500_count_original                                                                           as cassette_500_count_original
								   , @cassette_2000_count_original                                                                          as cassette_2000_count_original
								   , @cassette_100_brand_capacity                                                                           as cassette_100_brand_capacity
								   , @cassette_200_brand_capacity                                                                           as cassette_200_brand_capacity
								   , @cassette_500_brand_capacity                                                                           as cassette_500_brand_capacity
								   , @cassette_2000_brand_capacity                                                                          as cassette_2000_brand_capacity
								   , @total_capacity_amount_100                                                                             as total_capacity_amount_100
								   , @total_capacity_amount_200                                                                             as total_capacity_amount_200
								   , @total_capacity_amount_500                                                                             as total_capacity_amount_500
								   , @total_capacity_amount_2000                                                                            as total_capacity_amount_2000
								   , @denomination_100_max_capacity_percentage                                                              as denomination_100_max_capacity_percentage
								   , @denomination_200_max_capacity_percentage                                                              as denomination_200_max_capacity_percentage
								   , @denomination_500_max_capacity_percentage                                                              as denomination_500_max_capacity_percentage
								   , @denomination_2000_max_capacity_percentage                                                             as denomination_2000_max_capacity_percentage
								   , @max_amt_allowed_100                                                                                   as max_amt_allowed_100
								   , @max_amt_allowed_200                                                                                   as max_amt_allowed_200
								   , @max_amt_allowed_500                                                                                   as max_amt_allowed_500
								   , @max_amt_allowed_2000                                                                                  as max_amt_allowed_2000
								   , @denomination_wise_round_off_100                                                                       as denomination_wise_round_off_100
								   , @denomination_wise_round_off_200                                                                       as denomination_wise_round_off_200
								   , @denomination_wise_round_off_500                                                                       as denomination_wise_round_off_500
								   , @denomination_wise_round_off_2000                                                                      as denomination_wise_round_off_2000
								   , @tentative_loading_100                                                                                 as tentative_loading_100 
								   , @tentative_loading_200                                                                                 as tentative_loading_200 
								   , @tentative_loading_500                                                                                 as tentative_loading_500 
								   , @tentative_loading_2000                                                                                as tentative_loading_2000
								   -- Max Capacity                                                                                          
								   , @rounded_tentative_loading_100		                                                                    as rounded_tentative_loading_100		
								   , @rounded_tentative_loading_200	                                                                        as rounded_tentative_loading_200	    
								   , @rounded_tentative_loading_500	                                                                        as rounded_tentative_loading_500	    
								   , @rounded_tentative_loading_2000	                                                                    as rounded_tentative_loading_2000	
								   , @deno_100_priority                                                                                     as deno_100_priority
								   , @deno_200_priority                                                                                     as deno_200_priority
								   , @deno_500_priority                                                                                     as deno_500_priority
								   , @deno_2000_priority                                                                                    as deno_2000_priority
								   , @is_deno_wise_cash_available                                                                           as is_deno_wise_cash_available
									---- If denomination wise is availabble                                                                 
									-- Priority of denomination                                                                             
								   , @priority_1_is_denomination_100                                                                        as priority_1_is_denomination_100
								   , @priority_1_is_denomination_200                                                                        as priority_1_is_denomination_200
								   , @priority_1_is_denomination_500                                                                        as priority_1_is_denomination_500
								   , @priority_1_is_denomination_2000 								   	                                    as priority_1_is_denomination_2000 								   	
								   , @priority_1_is_remaining_amount_available_100                                                          as priority_1_is_remaining_amount_available_100
								   , @priority_1_is_remaining_amount_available_200                                                          as priority_1_is_remaining_amount_available_200
								   , @priority_1_is_remaining_amount_available_500                                                          as priority_1_is_remaining_amount_available_500
								   , @priority_1_is_remaining_amount_available_2000                                                         as priority_1_is_remaining_amount_available_2000
								   , @priority_1_loading_amount_100                                                                         as priority_1_loading_amount_100
								   , @priority_1_loading_amount_200                                                                         as priority_1_loading_amount_200
								   , @priority_1_loading_amount_500                                                                         as priority_1_loading_amount_500
								   , @priority_1_loading_amount_2000								                                        as priority_1_loading_amount_2000								   
								   , @priority_2_is_denomination_100                                                                        as priority_2_is_denomination_100
								   , @priority_2_is_denomination_200                                                                        as priority_2_is_denomination_200
								   , @priority_2_is_denomination_500                                                                        as priority_2_is_denomination_500
								   , @priority_2_is_denomination_2000 								                                        as priority_2_is_denomination_2000 								   
								   , @priority_2_is_remaining_amount_available_100                                                          as priority_2_is_remaining_amount_available_100
								   , @priority_2_is_remaining_amount_available_200                                                          as priority_2_is_remaining_amount_available_200
								   , @priority_2_is_remaining_amount_available_500                                                          as priority_2_is_remaining_amount_available_500
								   , @priority_2_is_remaining_amount_available_2000                                                         as priority_2_is_remaining_amount_available_2000
								   , @priority_2_loading_amount_100                                                                         as priority_2_loading_amount_100
								   , @priority_2_loading_amount_200                                                                         as priority_2_loading_amount_200
								   , @priority_2_loading_amount_500                                                                         as priority_2_loading_amount_500
								   , @priority_2_loading_amount_2000								                                        as priority_2_loading_amount_2000								   
								   , @priority_3_is_denomination_100                                                                        as priority_3_is_denomination_100
								   , @priority_3_is_denomination_200                                                                        as priority_3_is_denomination_200
								   , @priority_3_is_denomination_500                                                                        as priority_3_is_denomination_500
								   , @priority_3_is_denomination_2000 								                                        as priority_3_is_denomination_2000 								   
								   , @priority_3_is_remaining_amount_available_100                                                          as priority_3_is_remaining_amount_available_100
								   , @priority_3_is_remaining_amount_available_200                                                          as priority_3_is_remaining_amount_available_200
								   , @priority_3_is_remaining_amount_available_500                                                          as priority_3_is_remaining_amount_available_500
								   , @priority_3_is_remaining_amount_available_2000								                            as priority_3_is_remaining_amount_available_2000								   
								   , @priority_3_loading_amount_100                                                                         as priority_3_loading_amount_100
								   , @priority_3_loading_amount_200                                                                         as priority_3_loading_amount_200
								   , @priority_3_loading_amount_500                                                                         as priority_3_loading_amount_500
								   , @priority_3_loading_amount_2000								                                        as priority_3_loading_amount_2000								   
								   , @priority_4_is_denomination_100                                                                        as priority_4_is_denomination_100
								   , @priority_4_is_denomination_200                                                                        as priority_4_is_denomination_200
								   , @priority_4_is_denomination_500                                                                        as priority_4_is_denomination_500
								   , @priority_4_is_denomination_2000 								                                        as priority_4_is_denomination_2000 								   
								   , @priority_4_is_remaining_amount_available_100                                                          as priority_4_is_remaining_amount_available_100
								   , @priority_4_is_remaining_amount_available_200                                                          as priority_4_is_remaining_amount_available_200
								   , @priority_4_is_remaining_amount_available_500                                                          as priority_4_is_remaining_amount_available_500
								   , @priority_4_is_remaining_amount_available_2000								                            as priority_4_is_remaining_amount_available_2000								   
								   , @priority_4_loading_amount_100                                                                         as priority_4_loading_amount_100
								   , @priority_4_loading_amount_200                                                                         as priority_4_loading_amount_200
								   , @priority_4_loading_amount_500                                                                         as priority_4_loading_amount_500
								   , @priority_4_loading_amount_2000								                                        as priority_4_loading_amount_2000								   
								   , @loading_amount_100                                                                                    as loading_amount_100
								   , @loading_amount_200                                                                                    as loading_amount_200
								   , @loading_amount_500                                                                                    as loading_amount_500
								   , @loading_amount_2000                                                                                   as loading_amount_2000
									-- Total loading amount								                                                    
								   , @loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000                 as total_loading_amount
								   --closing available amount	                                                                           
								   , @total_remaining_available_amount                                                                      as total_closing_remaining_available_amount
								   , @total_forecasted_remaining_amt                                                                        as total_forecasted_remaining_amt
				
						END	--ELSE --If denoination wise cash is not available.						
			
						FETCH NEXT FROM cursor2 
									 INTO  @atmid							
									 , @atm_priority							
									, @denomination_100_max_capacity_percentage
									, @denomination_200_max_capacity_percentage
									, @denomination_500_max_capacity_percentage
									, @denomination_2000_max_capacity_percentage
									, @morning_balance_100
									, @morning_balance_200
									, @morning_balance_500
									, @morning_balance_2000
									, @total_morning_balance
									, @original_forecasted_amt_100
									, @original_forecasted_amt_200
									, @original_forecasted_amt_500
									, @original_forecasted_amt_2000
									, @original_total_forecasted_amt
									--, @cassette_50_count_original  
									, @cassette_100_count_original 
									, @cassette_200_count_original 
									, @cassette_500_count_original 
									, @cassette_2000_count_original
									, @deno_100_priority
									, @deno_200_priority
									, @deno_500_priority
									, @deno_2000_priority
									, @cassette_100_brand_capacity
									, @cassette_200_brand_capacity
									, @cassette_500_brand_capacity
									, @cassette_2000_brand_capacity
									, @limit_amount
					END			-- While End (cusror2)
				CLOSE cursor2 
				DEALLOCATE cursor2
		
			--- Insert in temp table for feeder 

				
				INSERT INTO #temp_feeder_forecast
				(
						project_id						
					,	bank_code						
					,	feeder_branch_code							
					,	is_deno_wise_cash_available		
					,	total_remaining_available_amount
					,   remaining_avail_100				
					,   remaining_avail_200				
					,   remaining_avail_500				
					,   remaining_avail_2000			
				)

				SELECT	  @project_id
						, @bank_code
						, @feederbranchcode
						, @is_deno_wise_cash_available
						, @total_remaining_available_amount
						, @remaining_avail_100			
						, @remaining_avail_200			
						, @remaining_avail_500			
						, @remaining_avail_2000			

	FETCH NEXT FROM cursor1 INTO 
@project_id
,@bank_code
,@feederbranchcode
, @total_original_available_amount
, @Original_avail_100
, @Original_avail_200
, @Original_avail_500
, @Original_avail_2000
,@is_deno_wise_cash_available
	END

   CLOSE cursor1 
 DEALLOCATE cursor1
 --select * from #temp_cash_pre_availability
 
 --select * from #temp_feeder_forecast where feeder_branch_code = 'KOLHAPUR'
 
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Completed curosr to allocate amount', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Cursor completed for allocating amount'
 
	drop table if exists #forecasted_dataset

	-- Create final dataset for atm level here

	-- Add flag indicating if cash pre availability is there
	select distinct a.*
			, case when b.Total_Amount_Available > 0 then 1 else 0 end as is_cash_pre_available
			, b.available_100_amount as		feeder_available_100_amount
			, b.available_200_amount as		feeder_available_200_amount
			, b.available_500_amount as		feeder_available_500_amount
			, b.available_2000_amount as	feeder_available_2000_amount
			, b.Total_Amount_Available as	feeder_total_amount_available	
			, b.vault_closing_balance_100
			, b.vault_closing_balance_200
			, b.vault_closing_balance_500
			, b.vault_closing_balance_2000
			, b.total_vault_closing_balance		
	into #forecasted_dataset
	from #dist_with_cra  a
	left join #feeder_level_dataset b
	on a.project_id COLLATE DATABASE_DEFAULT= b.project_id COLLATE DATABASE_DEFAULT
	and a.bank_code  COLLATE DATABASE_DEFAULT= b.bank_code  COLLATE DATABASE_DEFAULT
	and a.feeder_branch_code  COLLATE DATABASE_DEFAULT= b.feeder_branch_code COLLATE DATABASE_DEFAULT
	
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Inserting data into #forecasted_dataset completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	--		select * from #feeder_level_dataset where feeder_branch_code = 'KOLHAPUR'
	-- select * from #forecasted_dataset where feeder_branch_code = 'KOLHAPUR'
	--select * from #dist_with_cra
	--Prepare final dataset to insert into database
	drop table if exists #final_atm_level_allocated_dataset_temp


	select	a.project_id
			,a.bank_code
			,a.feeder_branch_code
			,a.is_currency_chest
			,a.atm_priority
			,b.total_opening_remaining_available_amount
			,a.atm_id
			,insurance_limit
			,bank_cash_limit
			,limit_amount
			,a.is_cash_pre_available
			,a.total_forecasted_amt
			,b.total_loading_amount
			,case when a.is_cash_pre_available = 1 then coalesce(b.total_loading_amount,0) else coalesce(a.total_forecasted_amt,0) end 	as final_total_loading_amount ---total
			,case when a.is_cash_pre_available = 1 then coalesce(b.loading_amount_100,0) else coalesce(a.forecasted_amt_100,0) end 	as final_total_loading_amount_100		
			,case when a.is_cash_pre_available = 1 then coalesce(b.loading_amount_200,0) else coalesce(a.forecasted_amt_200,0) end 	as final_total_loading_amount_200
			,case when a.is_cash_pre_available = 1 then coalesce(b.loading_amount_500,0) else coalesce(a.forecasted_amt_500,0) end 	as final_total_loading_amount_500
			,case when a.is_cash_pre_available = 1 then coalesce(b.loading_amount_2000,0) else coalesce(a.forecasted_amt_2000,0) end 	as final_total_loading_amount_2000
			,case when a.is_cash_pre_available = 1 then 0 else a.total_rounded_vault_amt end as total_atm_vaulting_amount_after_pre_availability --total
			,case when a.is_cash_pre_available = 1 then 0 else a.rounded_vaultingamount_100 end as atm_vaulting_amount_after_pre_availability_100
			,case when a.is_cash_pre_available = 1 then 0 else a.rounded_vaultingamount_200 end as atm_vaulting_amount_after_pre_availability_200
			,case when a.is_cash_pre_available = 1 then 0 else a.rounded_vaultingamount_500 end as atm_vaulting_amount_after_pre_availability_500
			,case when a.is_cash_pre_available = 1 then 0 else a.rounded_vaultingamount_2000 end as atm_vaulting_amount_after_pre_availability_2000
			,COALESCE(a.remaining_capacity_amount_100,b.remaining_capacity_amount_100	)  as remaining_capacity_amount_100
			,COALESCE(a.remaining_capacity_amount_200,b.remaining_capacity_amount_200	)  as remaining_capacity_amount_200
			,COALESCE(a.remaining_capacity_amount_500,b.remaining_capacity_amount_500	)  as remaining_capacity_amount_500
			,COALESCE(a.remaining_capacity_amount_2000,b.remaining_capacity_amount_2000	)  as remaining_capacity_amount_2000
			,withdrawal_type

			--Add dennomination wise
	into #final_atm_level_allocated_dataset_temp
	from #forecasted_dataset a
	left join 
		#temp_cash_pre_availability b
			on 
				a.project_id COLLATE DATABASE_DEFAULT = b.project_id COLLATE DATABASE_DEFAULT
				and a.bank_code COLLATE DATABASE_DEFAULT = b.bank_code COLLATE DATABASE_DEFAULT
				and a.feeder_branch_code COLLATE DATABASE_DEFAULT = b.feeder_branch_code COLLATE DATABASE_DEFAULT
				and a.atm_id COLLATE DATABASE_DEFAULT= b.atmid  COLLATE DATABASE_DEFAULT	
	
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Inserting data into #final_atm_level_allocated_dataset_temp completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	--Feeder level
	--select sum(final_total_loading_amount_100),
	--		sum(final_total_loading_amount_200),
	--		SUM(final_total_loading_amount_500),
	--		SUM(final_total_loading_amount_2000) from #final_atm_level_allocated_dataset_temp where feeder_branch_code = 'SSI THANE'
	
	--select * from #final_atm_level_allocated_dataset_temp
	
--		select sum(rounded_vaultingamount_500) from #forecasted_dataset where feeder_branch_code = 'PIMPRI'
--select sum) from #final_atm_level_allocated_dataset_temp where feeder_branch_code = 'PIMPRI'


	--select sum(final_total_loading_amount)  from #final_atm_level_allocated_dataset_temp 
	--	select sum) from #final_atm_level_allocated_dataset_temp
		--SELECT * FROM #forecasted_dataset
	--	select * from #temp_feeder_forecast where feeder_branch_code = 'SSI THANE'

		drop table if exists #final_feeder_level_allocated_dataset


				--DECLARE @MaxDestinationDate DATE
				--DECLARE @dispenseformula NVARCHAR(50)
				--DECLARE @confidence_factor NVARCHAR(50)
				--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
				--DECLARE @denomination_wise_round_off_100 INT			 -- for rounding, replace with 100000
				--DECLARE @denomination_wise_round_off_200 int      
				--DECLARE @denomination_wise_round_off_500 int
				--DECLARE @denomination_wise_round_off_2000 int
				--DECLARE @denomination_wise_round_off_100_currency_chest int      
				--DECLARE @denomination_wise_round_off_200_currency_chest int
				--DECLARE @denomination_wise_round_off_500_currency_chest int
				--DECLARE @denomination_wise_round_off_2000_currency_chest INT
				--declare @default_avg_dispense int
				--declare @vaulting_for_normal_weekday_percentage float,
				--		@vaulting_for_normal_weekend_percentage float,
				--		@vaulting_for_extended_weekend_percentage float
		
		
				--Select	@confidence_factor = confidence_factor
				--, @dispenseformula = dispenseformula
				--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
				--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
				--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
				--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
				--, @denomination_wise_round_off_100_currency_chest = denomination_wise_round_off_100_currency_chest
				--, @denomination_wise_round_off_200_currency_chest = denomination_wise_round_off_200_currency_chest
				--, @denomination_wise_round_off_500_currency_chest = denomination_wise_round_off_500_currency_chest
				--, @denomination_wise_round_off_2000_currency_chest = denomination_wise_round_off_2000_currency_chest
				--,@default_avg_dispense = default_average_dispense
				--, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
				--, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
				--, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
				-- from system_settings where record_status = 'Active'

				-- DECLARE @is_revised_forecasting varchar(10) = 'No'

		SELECT *,   (CASE 
					WHEN withdrawal_type = 'Vault' 
					THEN (
							--vaulting_after_pre_availabiliy_allocation_100 - final_total_atm_loading_after_pre_availability_allocation_amount_100
							vault_balance_100 - final_total_atm_loading_after_pre_availability_allocation_amount_100
						 )
					ELSE 
					--vaulting_after_pre_availabiliy_allocation_100
					vault_balance_100
					END) + indent_after_pre_availabiliy_allocation_100 - final_total_atm_loading_after_pre_availability_allocation_amount_100 as vault_closing_balance_after_pre_availability_100
				, (CASE 
					WHEN withdrawal_type = 'Vault' 
					THEN (
							--vaulting_after_pre_availabiliy_allocation_200 - final_total_atm_loading_after_pre_availability_allocation_amount_200
							vault_balance_200 - final_total_atm_loading_after_pre_availability_allocation_amount_200
						 )
					ELSE 
					--vaulting_after_pre_availabiliy_allocation_200
					vault_balance_200
					END) + indent_after_pre_availabiliy_allocation_200 - final_total_atm_loading_after_pre_availability_allocation_amount_200 as vault_closing_balance_after_pre_availability_200
				, (CASE 
					WHEN withdrawal_type = 'Vault' 
					THEN (
							--vaulting_after_pre_availabiliy_allocation_500 - final_total_atm_loading_after_pre_availability_allocation_amount_500
							vault_balance_500 - final_total_atm_loading_after_pre_availability_allocation_amount_500
							)
					ELSE 
					--vaulting_after_pre_availabiliy_allocation_500
					vault_balance_500
					END) + indent_after_pre_availabiliy_allocation_500 - final_total_atm_loading_after_pre_availability_allocation_amount_500 as vault_closing_balance_after_pre_availability_500
				, (CASE 
					WHEN withdrawal_type = 'Vault' 
					THEN (
							--vaulting_after_pre_availabiliy_allocation_2000 - final_total_atm_loading_after_pre_availability_allocation_amount_2000
							vault_balance_2000 - final_total_atm_loading_after_pre_availability_allocation_amount_2000
						)
					ELSE 
					--vaulting_after_pre_availabiliy_allocation_2000
					vault_balance_2000
					END) + indent_after_pre_availabiliy_allocation_2000 - final_total_atm_loading_after_pre_availability_allocation_amount_2000 as vault_closing_balance_after_pre_availability_2000
				, total_vault_balance + (
																	  indent_after_pre_availabiliy_allocation_100 
																	+ indent_after_pre_availabiliy_allocation_200 
																	+ indent_after_pre_availabiliy_allocation_500
																	+ indent_after_pre_availabiliy_allocation_2000
																	) - 
																	(
																	final_total_atm_loading_after_pre_availability_allocation_amount_100 +
																	final_total_atm_loading_after_pre_availability_allocation_amount_200 + 
																	final_total_atm_loading_after_pre_availability_allocation_amount_500 + 
																	final_total_atm_loading_after_pre_availability_allocation_amount_2000 
																	 ) as total_vault_closing_balance_after_pre_availability
				, CASE WHEN 
						ISNULL(Total_Amount_Available,0) > 0
						THEN
							 final_total_atm_loading_after_pre_availability_allocation_amount_100 - final_total_loading_amount_100 
					ELSE
					CASE WHEN 
						withdrawal_type = 'Vault'
						THEN
							0
						ELSE 
						final_total_atm_loading_after_pre_availability_allocation_amount_100 - forecast_loading_amount_100
					END
					END as currency_chest_adjustment_100
				, CASE WHEN 
							ISNULL(Total_Amount_Available,0) > 0
							THEN
					 final_total_atm_loading_after_pre_availability_allocation_amount_200 - final_total_loading_amount_200
					ELSE 
						CASE WHEN 
								withdrawal_type = 'Vault'
								THEN
									0
								ELSE
						final_total_atm_loading_after_pre_availability_allocation_amount_200 - forecast_loading_amount_200	
						END
					END as currency_chest_adjustment_200
				, CASE WHEN 
							ISNULL(Total_Amount_Available,0) > 0
							THEN
						final_total_atm_loading_after_pre_availability_allocation_amount_500 - final_total_loading_amount_500
					ELSE 
						CASE WHEN 
								withdrawal_type = 'Vault'
								THEN
									0
								ELSE 
						final_total_atm_loading_after_pre_availability_allocation_amount_500 - forecast_loading_amount_500
						END
					END as currency_chest_adjustment_500
				, CASE WHEN 
							ISNULL(Total_Amount_Available,0) > 0
							THEN
					 final_total_atm_loading_after_pre_availability_allocation_amount_2000 - final_total_loading_amount_2000
					ELSE 
					CASE WHEN 
								withdrawal_type = 'Vault'
								THEN
									0
								ELSE
						final_total_atm_loading_after_pre_availability_allocation_amount_2000 - forecast_loading_amount_2000
					END
					END as currency_chest_adjustment_2000
				--, final_total_atm_loading_after_pre_availability_allocation_amount_200 - forecast_loading_amount_200 as currency_chest_adjustment_200
				--, final_total_atm_loading_after_pre_availability_allocation_amount_500 - forecast_loading_amount_500 as currency_chest_adjustment_500
				--, final_total_atm_loading_after_pre_availability_allocation_amount_2000 - forecast_loading_amount_2000 as currency_chest_adjustment_2000
				, indent_after_pre_availabiliy_allocation_100 
					+ indent_after_pre_availabiliy_allocation_200 
					+ indent_after_pre_availabiliy_allocation_500
					+ indent_after_pre_availabiliy_allocation_2000
					as total_indent_after_pre_availabiliy_allocation
				, final_total_atm_loading_after_pre_availability_allocation_amount_100 +
				  final_total_atm_loading_after_pre_availability_allocation_amount_200 + 
				  final_total_atm_loading_after_pre_availability_allocation_amount_500 + 
				  final_total_atm_loading_after_pre_availability_allocation_amount_2000  as final_total_atm_loading_after_pre_availability_allocation_amount
		INTO #final_feeder_level_allocated_dataset
		FROM
		(
			select a.*,
				c.final_total_loading_amount,
				c.final_total_loading_amount_100,
				c.final_total_loading_amount_200,
				c.final_total_loading_amount_500,
				c.final_total_loading_amount_2000
				,case when  a.Total_Amount_Available > 0 
					then 
					CASE WHEN 
						feeder_branch.is_vaulting_enabled = 'Yes' 
					THEN
						isnull(b.total_remaining_available_amount,0)
					ELSE
						CASE WHEN total_vault_balance > 0 
								THEN 
									ISNULL(total_vault_balance,0)
								ELSE
								0
							END 
					END
				else 
					CASE WHEN total_vault_balance > 0 
								THEN 
									ISNULL(total_vault_balance,0)
								ELSE
								isnull(a.total_rounded_vault_amt,0) 
							END 
				end as total_vaulting_after_pre_availabiliy_allocation
				,CASE WHEN  a.Total_Amount_Available > 0   
					THEN
						CASE WHEN a.available_100_amount > 0 and
							feeder_branch.is_vaulting_enabled = 'Yes' 
						THEN
							isnull(b.remaining_avail_100,0)
						ELSE
							CASE WHEN vault_balance_100 > 0 
								THEN 
									ISNULL(vault_balance_100,0)
								ELSE
								0
							END 
						END
					else 
						CASE WHEN vault_balance_100 > 0 
							THEN 
								--ISNULL(vault_balance_100,0) + isnull(a.vaultingamount_100,0)
								ISNULL(vault_balance_100,0)
							ELSE
								isnull(a.vaultingamount_100,0)
						END 
				end as vaulting_after_pre_availabiliy_allocation_100
			
				,CASE WHEN  a.Total_Amount_Available > 0   THEN			
					CASE WHEN a.available_200_amount > 0 and 
						feeder_branch.is_vaulting_enabled = 'Yes' 
						THEN
						isnull(b.remaining_avail_200,0) 
					ELSE 
						CASE WHEN vault_balance_200 > 0 
								THEN 
									ISNULL(vault_balance_200,0)
								ELSE
								0
							END 
					END
					else 
						CASE WHEN vault_balance_200 > 0 
							THEN 
								ISNULL(vault_balance_200,0) 
							ELSE
								isnull(a.vaultingamount_200,0)
					 END  
				end as vaulting_after_pre_availabiliy_allocation_200
			
				,CASE WHEN  a.Total_Amount_Available > 0   THEN
					CASE WHEN a.available_500_amount > 0 and
						feeder_branch.is_vaulting_enabled = 'Yes' 
						THEN
							isnull(b.remaining_avail_500,0) 
						else 
						CASE WHEN vault_balance_500 > 0 
								THEN 
									ISNULL(vault_balance_500,0)
								ELSE
								0
							END 
					END
					else
						CASE WHEN vault_balance_500 > 0 
								THEN 
									ISNULL(vault_balance_500,0) 
								ELSE 
								isnull(a.vaultingamount_500,0)
							END  
				end as vaulting_after_pre_availabiliy_allocation_500
			
				,CASE WHEN  a.Total_Amount_Available > 0   THEN
					CASE WHEN a.available_2000_amount > 0 and 
						feeder_branch.is_vaulting_enabled = 'Yes' 
						THEN
						isnull(b.remaining_avail_2000,0) 
						else
							CASE WHEN vault_balance_2000 > 0 
								THEN 
									ISNULL(vault_balance_2000,0)
								ELSE
								0
							END
						END
					else 
						CASE WHEN vault_balance_2000 > 0 
								THEN 
									ISNULL(vault_balance_2000,0) 
								ELSE
									isnull(a.vaultingamount_2000,0)
							END
				end as vaulting_after_pre_availabiliy_allocation_2000
				--Add for all denomination
				--, ISNULL(c.final_total_loading_amount,0) as final_total_atm_loading_after_pre_availability_allocation_amount
				, CASE WHEN  a.Total_Amount_Available > 0   
					THEN
					CASE WHEN a.available_100_amount > 0 and 
						feeder_branch.is_vaulting_enabled = 'Yes' 
						THEN
							CASE WHEN 	
								a.is_currency_chest = 'Yes'
									THEN
										CASE
											WHEN 
												cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint) > final_total_loading_amount_100
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest )) * @denomination_wise_round_off_100_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
										END
									ELSE
							ISNULL(c.final_total_loading_amount_100,0) 
							END
						ELSE
							CASE
								WHEN a.is_currency_chest = 'Yes'
								THEN
									CASE
											WHEN 
												cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint) > final_total_loading_amount_100
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest )) * @denomination_wise_round_off_100_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
											END
								ELSE
									ISNULL(c.final_total_loading_amount_100,0)
							END
						
						END
						ELSE
							CASE WHEN
								withdrawal_type = 'Vault'
								THEN 
								0
								ELSE
								CASE
									WHEN a.is_currency_chest = 'Yes'
									THEN
										CASE
												WHEN 
													(cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
													-
													c.final_total_loading_amount_100
													)
													> c.remaining_capacity_amount_100
												THEN
													cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest )) * @denomination_wise_round_off_100_currency_chest as bigint)
												ELSE
													cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
												END
									ELSE
										ISNULL(c.final_total_loading_amount_100,0)
								END
							END
							
							--ISNULL(c.final_total_loading_amount_100,0)
					END as final_total_atm_loading_after_pre_availability_allocation_amount_100
				,
					CASE WHEN  a.Total_Amount_Available > 0   
					THEN
					CASE WHEN a.available_200_amount > 0 and 
						feeder_branch.is_vaulting_enabled = 'Yes' 
						THEN
					CASE WHEN 	
					a.is_currency_chest = 'Yes'
						THEN
							CASE 
								WHEN
									cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint) > final_total_loading_amount_200
								THEN
									cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest )) * @denomination_wise_round_off_200_currency_chest as bigint)
								ELSE
									cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
							END
						ELSE
							ISNULL(c.final_total_loading_amount_200,0)
						END
					ELSE
						--ISNULL(c.final_total_loading_amount_200,0)
						CASE WHEN a.is_currency_chest = 'Yes'
						THEN
							CASE WHEN
									cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint) > final_total_loading_amount_200
								THEN
									cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest )) * @denomination_wise_round_off_200_currency_chest as bigint)
								ELSE
									cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
							END
						ELSE
							ISNULL(c.final_total_loading_amount_200,0)
							
						END
					END 
					ELSE
						--ISNULL(c.final_total_loading_amount_200,0)
						CASE WHEN
								withdrawal_type = 'Vault'
								THEN 
								0
								ELSE
						CASE WHEN 	
					a.is_currency_chest = 'Yes'
						THEN
							CASE 
								WHEN
									(cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
									-
									c.final_total_loading_amount_200
									)
									> c.remaining_capacity_amount_200
								THEN
									cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest )) * @denomination_wise_round_off_200_currency_chest as bigint)
								ELSE
									cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
							END
						ELSE
							ISNULL(c.final_total_loading_amount_200,0)
						END
					END
					END as final_total_atm_loading_after_pre_availability_allocation_amount_200	
				,
					CASE WHEN  a.Total_Amount_Available > 0   
					THEN
					CASE WHEN a.available_500_amount > 0 and 
							  feeder_branch.is_vaulting_enabled = 'Yes' and 
							  withdrawal_type <> 'Vault'
							THEN 
								CASE WHEN 	
									a.is_currency_chest = 'Yes'
										THEN
											CASE 
												WHEN 
													cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint) > final_total_loading_amount_500
												THEN
													cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest )) * @denomination_wise_round_off_500_currency_chest as bigint)
												ELSE
													cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
											END
										ELSE
											ISNULL(c.final_total_loading_amount_500,0) 
								END
							ELSE
						CASE WHEN 	
							a.is_currency_chest = 'Yes'
							THEN
								CASE 
									WHEN 
										cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint) > final_total_loading_amount_500
									THEN
										cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest )) * @denomination_wise_round_off_500_currency_chest as bigint)
									ELSE
										cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
								END
						ELSE
								ISNULL(c.final_total_loading_amount_500,0) 
						END
						--ISNULL(c.final_total_loading_amount_500,0) 
					END
					ELSE
						--ISNULL(c.final_total_loading_amount_500,0)
						CASE WHEN
								withdrawal_type = 'Vault'
								THEN 
								0
								ELSE
						CASE WHEN 	
						a.is_currency_chest = 'Yes'
						THEN
							CASE 
								WHEN 
									(cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
									-
									c.final_total_loading_amount_500
									)
									> c.remaining_capacity_amount_500
								THEN
									cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest )) * @denomination_wise_round_off_500_currency_chest as bigint)
								ELSE
									cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
							END
						ELSE
							ISNULL(c.final_total_loading_amount_500,0) 
						END
						END
					END as final_total_atm_loading_after_pre_availability_allocation_amount_500	
				,
					CASE WHEN  a.Total_Amount_Available > 0   
					THEN
					CASE WHEN a.available_2000_amount > 0 and 
						feeder_branch.is_vaulting_enabled = 'Yes' 
						THEN
						CASE WHEN 	
						a.is_currency_chest = 'Yes'
						THEN
							CASE
								WHEN
									cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint) > final_total_loading_amount_2000
								THEN
									cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest )) * @denomination_wise_round_off_2000_currency_chest as bigint)
								ELSE
									cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
							END
						ELSE
							ISNULL(c.final_total_loading_amount_2000,0) 
						END
					ELSE
						--ISNULL(c.final_total_loading_amount_2000,0)
						CASE WHEN 	
						a.is_currency_chest = 'Yes'
						THEN
							CASE
								WHEN
									cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint) > final_total_loading_amount_2000
								THEN
									cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest )) * @denomination_wise_round_off_2000_currency_chest as bigint)
								ELSE
									cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
							END
						ELSE
							ISNULL(c.final_total_loading_amount_2000,0) 
						END
					END
					ELSE
						--ISNULL(c.final_total_loading_amount_2000,0)
						CASE WHEN
								withdrawal_type = 'Vault'
								THEN 
								0
								ELSE
						CASE WHEN 	
						a.is_currency_chest = 'Yes'
						THEN
							CASE
								WHEN
									(cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
									-	
									c.final_total_loading_amount_2000
									)  > c.remaining_capacity_amount_2000
								THEN
									cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest )) * @denomination_wise_round_off_2000_currency_chest as bigint)
								ELSE
									cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
							END
						ELSE
							ISNULL(c.final_total_loading_amount_2000,0) 
						END
						END
					END as final_total_atm_loading_after_pre_availability_allocation_amount_2000	
		
				, CASE WHEN  a.Total_Amount_Available > 0   THEN
					CASE WHEN a.available_100_amount > 0  and feeder_branch.is_vaulting_enabled = 'Yes'  and withdrawal_type <> 'Vault'
						then
							(CASE WHEN 	
								a.is_currency_chest = 'Yes'
									THEN
										CASE
											WHEN
												cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint) >final_total_loading_amount_100
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest )) * @denomination_wise_round_off_100_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
										END
									ELSE
								isnull(c.final_total_loading_amount_100,0)
							END) + isnull(b.remaining_avail_100,0)
						ELSE
							CASE WHEN 	
								a.is_currency_chest = 'Yes'
									THEN
										CASE
											WHEN
												cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint) >final_total_loading_amount_100
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest )) * @denomination_wise_round_off_100_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
										END
									ELSE
								isnull(c.final_total_loading_amount_100,0)
							END
						END
					else	
						CASE WHEN
								withdrawal_type = 'Vault'
								THEN 
								0
								ELSE
								CASE WHEN 
										a.is_currency_chest = 'Yes'
											THEN 
												CASE WHEN
													(
														CASE
															WHEN 
																(cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
																-
																c.final_total_loading_amount_100
																)
																> c.remaining_capacity_amount_100
															THEN
																cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest )) * @denomination_wise_round_off_100_currency_chest as bigint)
															ELSE
																cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
															END
														--Cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
													   + isnull(a.vaultingamount_100,0)
													   - CASE WHEN 
															 vault_balance_100 > 0 
														 THEN 
															isnull(vault_balance_100,0) 
														 else
															0
														 end
													)< 0
													THEN 
														0
													ELSE
													(
														CASE
															WHEN 
																(cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
																-
																c.final_total_loading_amount_100
																)
																> c.remaining_capacity_amount_100
															THEN
																cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest )) * @denomination_wise_round_off_100_currency_chest as bigint)
															ELSE
																cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
															END
														--Cast(round((1.0 *isnull(c.final_total_loading_amount_100,0) / @denomination_wise_round_off_100_currency_chest ),0) * @denomination_wise_round_off_100_currency_chest as bigint)
													   + isnull(a.vaultingamount_100,0)
													   - CASE WHEN 
														 vault_balance_100 > 0 
														 THEN 
														 isnull(vault_balance_100,0) 
														 else
														 0
														 end
													)
													END
											ELSE
												CASE WHEN 
														(
														   isnull(c.final_total_loading_amount_100,0)
															 + isnull(a.vaultingamount_100,0) 
															 -	CASE WHEN 
																vault_balance_100 > 0 THEN isnull(vault_balance_100,0) 
																else 
																0 
																end
														 ) < 0
													THEN 
														0
													ELSE
														(
														   isnull(c.final_total_loading_amount_100,0)
															 + isnull(a.vaultingamount_100,0) 
															 -	CASE WHEN 
																vault_balance_100 > 0 THEN isnull(vault_balance_100,0) 
																else 
																0 
																end
														 )
													END		
								END
					END		
				 end as indent_after_pre_availabiliy_allocation_100
				, CASE WHEN  a.Total_Amount_Available > 0   THEN
					 CASE WHEN a.available_200_amount > 0  and feeder_branch.is_vaulting_enabled = 'Yes'  and withdrawal_type <> 'Vault'
						then
							(CASE WHEN 	
								a.is_currency_chest = 'Yes'
									THEN
										CASE 
											WHEN
												cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint) > final_total_loading_amount_200
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest )) * @denomination_wise_round_off_200_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
										END
									ELSE
								isnull(c.final_total_loading_amount_200,0)
							END) + isnull(b.remaining_avail_200,0)
							ELSE 
								CASE WHEN 	
								a.is_currency_chest = 'Yes'
									THEN
										CASE 
											WHEN
												cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint) > final_total_loading_amount_200
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest )) * @denomination_wise_round_off_200_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
										END
										--cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
									ELSE
								isnull(c.final_total_loading_amount_200,0)
							END 
						END
						else 
						CASE WHEN
								withdrawal_type = 'Vault'
								THEN 
								0
								ELSE
								CASE WHEN 
										a.is_currency_chest = 'Yes'
											THEN 
												CASE WHEN
													(
														CASE
															WHEN 
																(cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
																-
																c.final_total_loading_amount_200
																)
																> c.remaining_capacity_amount_200
															THEN
																cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest )) * @denomination_wise_round_off_200_currency_chest as bigint)
															ELSE
																cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
															END
														--Cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
													   + isnull(a.vaultingamount_200,0)
													   - CASE WHEN 
															 vault_balance_200 > 0 
														 THEN 
															isnull(vault_balance_200,0) 
														 else
															0
														 end
													)< 0
													THEN 
														0
													ELSE
													(
														CASE
															WHEN 
																(cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
																-
																c.final_total_loading_amount_200
																)
																> c.remaining_capacity_amount_200
															THEN
																cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest )) * @denomination_wise_round_off_200_currency_chest as bigint)
															ELSE
																cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
															END
														--Cast(round((1.0 *isnull(c.final_total_loading_amount_200,0) / @denomination_wise_round_off_200_currency_chest ),0) * @denomination_wise_round_off_200_currency_chest as bigint)
													   + isnull(a.vaultingamount_200,0)
													   - CASE WHEN 
														 vault_balance_200 > 0 
														 THEN 
														 isnull(vault_balance_200,0) 
														 else
														 0
														 end
													)
													END
											ELSE
												CASE WHEN 
														(
														   isnull(c.final_total_loading_amount_200,0)
															 + isnull(a.vaultingamount_200,0) 
															 -	CASE WHEN 
																vault_balance_200 > 0 THEN isnull(vault_balance_200,0) 
																else 
																0 
																end
														 ) < 0
													THEN 
														0
													ELSE
														(
														   isnull(c.final_total_loading_amount_200,0)
															 + isnull(a.vaultingamount_200,0) 
															 -	CASE WHEN 
																vault_balance_200 > 0 THEN isnull(vault_balance_200,0) 
																else 
																0 
																end
														 )
													END		
								END
					END		
				  end as indent_after_pre_availabiliy_allocation_200
				, CASE 
					WHEN  a.Total_Amount_Available > 0   
					THEN
						CASE 
							WHEN a.available_500_amount > 0  AND feeder_branch.is_vaulting_enabled = 'Yes' and withdrawal_type <> 'Vault'
							then
								(
								CASE 
									WHEN 	
										a.is_currency_chest = 'Yes' 
											THEN
												CASE 
													WHEN 
														cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint) > final_total_loading_amount_500
													THEN
														cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest )) * @denomination_wise_round_off_500_currency_chest as bigint)
													ELSE
														cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
												END
											ELSE
									isnull(c.final_total_loading_amount_500,0)
								END) + 
								isnull(b.remaining_avail_500,0)
							ELSE
							CASE 
								WHEN 	
								a.is_currency_chest = 'Yes'
									THEN
										CASE 
											WHEN 
												cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint) > final_total_loading_amount_500
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest )) * @denomination_wise_round_off_500_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
										END
										--cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
									ELSE
								isnull(c.final_total_loading_amount_500,0)
							END
						END
					else 
						--	CASE WHEN
						--		withdrawal_type = 'Vault'
						--		THEN 
						--		0
						--		ELSE
						--		(CASE WHEN 	
						--		a.is_currency_chest = 'Yes'
						--			THEN
						--				cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
						--			ELSE
						--		isnull(c.final_total_loading_amount_500,0)
						--	END)+ isnull(a.vaultingamount_500,0) 
						--		- 
						--		CASE WHEN 
						--		 vault_balance_500 > 0 THEN isnull(vault_balance_500,0) else 0 end
						--END
							CASE WHEN
								withdrawal_type = 'Vault'
							THEN 
								0
							ELSE
								CASE WHEN 
										a.is_currency_chest = 'Yes'
											THEN 
												CASE WHEN
													(
														CASE
															WHEN 
																(cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
																-
																c.final_total_loading_amount_500
																)
																> c.remaining_capacity_amount_500
															THEN
																cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest )) * @denomination_wise_round_off_500_currency_chest as bigint)
															ELSE
																cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
															END
														--Cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
													   + isnull(a.vaultingamount_500,0)
													   - CASE WHEN 
															 vault_balance_500 > 0 
														 THEN 
															isnull(vault_balance_500,0) 
														 else
															0
														 end
													)< 0
													THEN 
														0
													ELSE
													(
														CASE
															WHEN 
																(cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
																-
																c.final_total_loading_amount_500
																)
																> c.remaining_capacity_amount_500
															THEN
																cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest )) * @denomination_wise_round_off_500_currency_chest as bigint)
															ELSE
																cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
															END
														--Cast(round((1.0 *isnull(c.final_total_loading_amount_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint)
													   + isnull(a.vaultingamount_500,0)
													   - CASE WHEN 
														 vault_balance_500 > 0 
														 THEN 
														 isnull(vault_balance_500,0) 
														 else
														 0
														 end
													)
													END
											ELSE
												CASE WHEN 
														(
														   isnull(c.final_total_loading_amount_500,0)
															 + isnull(a.vaultingamount_500,0) 
															 -	CASE WHEN 
																vault_balance_500 > 0 THEN isnull(vault_balance_500,0) 
																else 
																0 
																end
														 ) < 0
													THEN 
														0
													ELSE
														(
														   isnull(c.final_total_loading_amount_500,0)
															 + isnull(a.vaultingamount_500,0) 
															 -	CASE WHEN 
																vault_balance_500 > 0 THEN isnull(vault_balance_500,0) 
																else 
																0 
																end
														 )
													END		
								END
					END							
				end as indent_after_pre_availabiliy_allocation_500
				, CASE WHEN  a.Total_Amount_Available > 0   THEN
					CASE WHEN a.available_2000_amount > 0 and feeder_branch.is_vaulting_enabled = 'Yes'  and withdrawal_type <> 'Vault'
						then
							(CASE WHEN 	
								a.is_currency_chest = 'Yes'
									THEN
										CASE
											WHEN
												cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint) > final_total_loading_amount_2000
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest )) * @denomination_wise_round_off_2000_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
										END
									ELSE
								isnull(c.final_total_loading_amount_2000,0)
							END) + isnull(b.remaining_avail_2000,0)
						ELSE
							CASE WHEN 	
								a.is_currency_chest = 'Yes'
									THEN
										CASE
											WHEN
												cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint) > final_total_loading_amount_2000
											THEN
												cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest )) * @denomination_wise_round_off_2000_currency_chest as bigint)
											ELSE
												cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
										END
										--cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
									ELSE
								isnull(c.final_total_loading_amount_2000,0)
							END
						END
					else 
					CASE WHEN
								withdrawal_type = 'Vault'
								THEN 
								0
								ELSE
								CASE WHEN 
										a.is_currency_chest = 'Yes'
											THEN 
												CASE WHEN
													(
														CASE
															WHEN 
																(cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
																-
																c.final_total_loading_amount_2000
																)
																> c.remaining_capacity_amount_2000
															THEN
																cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest )) * @denomination_wise_round_off_2000_currency_chest as bigint)
															ELSE
																cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
															END
														--Cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
													   + isnull(a.vaultingamount_2000,0)
													   - CASE WHEN 
															 vault_balance_2000 > 0 
														 THEN 
															isnull(vault_balance_2000,0) 
														 else
															0
														 end
													)< 0
													THEN 
														0
													ELSE
													(
														CASE
															WHEN 
																(cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
																-
																c.final_total_loading_amount_2000
																)
																> c.remaining_capacity_amount_2000
															THEN
																cast(FLOOR((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest )) * @denomination_wise_round_off_2000_currency_chest as bigint)
															ELSE
																cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
															END
														--Cast(round((1.0 *isnull(c.final_total_loading_amount_2000,0) / @denomination_wise_round_off_2000_currency_chest ),0) * @denomination_wise_round_off_2000_currency_chest as bigint)
													   + isnull(a.vaultingamount_2000,0)
													   - CASE WHEN 
														 vault_balance_2000 > 0 
														 THEN 
														 isnull(vault_balance_2000,0) 
														 else
														 0
														 end
													)
													END
											ELSE
												CASE WHEN 
														(
														   isnull(c.final_total_loading_amount_2000,0)
															 + isnull(a.vaultingamount_2000,0) 
															 -	CASE WHEN 
																vault_balance_2000 > 0 THEN isnull(vault_balance_2000,0) 
																else 
																0 
																end
														 ) < 0
													THEN 
														0
													ELSE
														(
														   isnull(c.final_total_loading_amount_2000,0)
															 + isnull(a.vaultingamount_2000,0) 
															 -	CASE WHEN 
																vault_balance_2000 > 0 THEN isnull(vault_balance_2000,0) 
																else 
																0 
																end
														 )
													END		
								END
					END		
				end as indent_after_pre_availabiliy_allocation_2000
				-- add for all allocation
		from
		#feeder_level_dataset a	
		left join #temp_feeder_forecast b
		on a.project_id COLLATE DATABASE_DEFAULT= b.project_id COLLATE DATABASE_DEFAULT
		and a.bank_code COLLATE DATABASE_DEFAULT= b.bank_code COLLATE DATABASE_DEFAULT
		and a.feeder_branch_code COLLATE DATABASE_DEFAULT= b.feeder_branch_code COLLATE DATABASE_DEFAULT
		left join feeder_branch_master feeder_branch
		on a.feeder_branch_code COLLATE DATABASE_DEFAULT = feeder_branch.feeder_branch COLLATE DATABASE_DEFAULT
		AND a.bank_code COLLATE DATABASE_DEFAULT = feeder_branch.bank_code COLLATE DATABASE_DEFAULT
		AND a.project_id COLLATE DATABASE_DEFAULT = feeder_branch.project_id COLLATE DATABASE_DEFAULT
		AND feeder_branch.record_status = 'Active'
		left join 
		(
		select project_id
			 , bank_code
			 , feeder_branch_code
			 , sum(final_total_loading_amount) as final_total_loading_amount
			 , SUM(final_total_loading_amount_100) as final_total_loading_amount_100
			 , SUM(final_total_loading_amount_200) as final_total_loading_amount_200
			 , SUM(final_total_loading_amount_500) as final_total_loading_amount_500
			 , SUM(final_total_loading_amount_2000) as final_total_loading_amount_2000
			 , SUM(atm_vaulting_amount_after_pre_availability_100) as atm_vaulting_amount_after_pre_availability_100
			 , SUM(atm_vaulting_amount_after_pre_availability_200) as atm_vaulting_amount_after_pre_availability_200
			 , SUM(atm_vaulting_amount_after_pre_availability_500) as atm_vaulting_amount_after_pre_availability_500
			 , SUM(atm_vaulting_amount_after_pre_availability_2000) as atm_vaulting_amount_after_pre_availability_2000
			 , SUM(total_atm_vaulting_amount_after_pre_availability) as total_atm_vaulting_amount_after_pre_availability
			 , SUM(remaining_capacity_amount_100) as remaining_capacity_amount_100
			 , SUM(remaining_capacity_amount_200) as remaining_capacity_amount_200
			 , SUM(remaining_capacity_amount_500) as remaining_capacity_amount_500
			 , SUM(remaining_capacity_amount_2000) as remaining_capacity_amount_2000
				--Add for all denomination
		  from #final_atm_level_allocated_dataset_temp
		  WHERE final_total_loading_amount > 0
		  group by project_id, bank_code, feeder_branch_code
		  ) c
		  on c.project_id = a.project_id COLLATE DATABASE_DEFAULT
		  and c.bank_code = a.bank_code COLLATE DATABASE_DEFAULT
		  and c.feeder_branch_code = a.feeder_branch_code COLLATE DATABASE_DEFAULT
		)d	


		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
		values ('Inserting data into #final_feeder_level_allocated_dataset completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		
	--	select * from #final_feeder_level_allocated_dataset   --where feeder_branch_code = 'PIMPRI'





		
					


		  -- where c.feeder_branch_code = 'DHAWADA'

		  --SELECT atm.*
				-- ,  feeder.currency_chest_adjustment_100
				-- ,	feeder.currency_chest_adjustment_200
				-- ,	feeder.currency_chest_adjustment_500
				-- ,	feeder.currency_chest_adjustment_2000
				-- ,	final_total_atm_loading_after_pre_availability_allocation_amount_100
				-- ,	final_total_atm_loading_after_pre_availability_allocation_amount_200	  
				-- ,	final_total_atm_loading_after_pre_availability_allocation_amount_500
				-- ,	final_total_atm_loading_after_pre_availability_allocation_amount_2000
				-- ,	total_indent_after_pre_availabiliy_allocation
				-- ,  forecast_loading_amount_100
				-- ,	forecast_loading_amount_200
				-- ,	forecast_loading_amount_500
				-- ,	forecast_loading_amount_2000
				-- ,	total_forecasted_amt 
		  --FROM #final_feeder_level_allocated_dataset feeder
		  --LEFT JOIN #final_atm_level_allocated_dataset atm
		  --on atm.feeder_branch_code = feeder.feeder_branch_code
		  --AND atm.bank_code = feeder.bank_code 
		  --AND atm.project_id = feeder.project_id
	


		-- select * from #final_feeder_level_allocated_dataset where feeder_branch_code = 'KOLHAPUR'

		 -- select * from #final_atm_level_allocated_dataset where project_id = 'MOF_MAH'
			--AND bank_code = 'BOMH' and feeder_branch_code = 'PIMPRI'
		--	select * from #final_feeder_level_allocated_dataset where feeder_branch_code = 'KOLHAPUR'
			--AND bank_code = 'BOMH' and feeder_branch_code = 'PIMPRI'

		--DECLARE @is_revised_forecasting varchar(100) = 'No'

			DROP TABLE IF EXISTS #currency_chest_feeder_branch1
				select project_id
					 , bank_code
					 , feeder_branch_code
					 , is_currency_chest
					 , CASE 
						WHEN 
							SUM(Total_Amount_Available) > 0
						THEN
							SUM(final_total_loading_amount_100 )
						ELSE 
							SUM(forecast_loading_amount_100)
						END as forecast_loading_amount_100  
					, CASE WHEN 
							SUM(Total_Amount_Available) > 0
						THEN
							SUM(final_total_loading_amount_200 )
						ELSE 
							SUM(forecast_loading_amount_200)
						END as forecast_loading_amount_200 
					, CASE WHEN 
							SUM(Total_Amount_Available) > 0
						THEN
							SUM(final_total_loading_amount_500 )
						ELSE 
							SUM(forecast_loading_amount_500)
						END as forecast_loading_amount_500 
					, CASE WHEN 
							SUM(Total_Amount_Available) > 0
						THEN
							SUM(final_total_loading_amount_2000 )
						ELSE 
							SUM(forecast_loading_amount_2000)
						END as forecast_loading_amount_2000 
					 --, SUM(forecast_loading_amount_200 ) as forecast_loading_amount_200
					 --, SUM(forecast_loading_amount_500 ) as forecast_loading_amount_500
					 --, SUM(forecast_loading_amount_2000) as forecast_loading_amount_2000
					 , SUM(currency_chest_adjustment_100	) as currency_chest_adjustment_100
					 , SUM(currency_chest_adjustment_200	) as currency_chest_adjustment_200
					 , SUM(currency_chest_adjustment_500	) as currency_chest_adjustment_500
					 , SUM(currency_chest_adjustment_2000	) as currency_chest_adjustment_2000
					 , SUM(currency_chest_adjustment_100	) as currency_chest_left_amount_100
					 , SUM(currency_chest_adjustment_200	) as currency_chest_left_amount_200
					 , SUM(currency_chest_adjustment_500	) as currency_chest_left_amount_500
					 , SUM(currency_chest_adjustment_2000	) as currency_chest_left_amount_2000
					 , CASE WHEN 
							SUM(Total_Amount_Available) > 0
						THEN
							SUM(final_total_loading_amount_100)
						ELSE 
							SUM(forecast_loading_amount_100)
						END as forecast_loading_amount_100_after_currency_chest  
					, CASE WHEN 
							SUM(Total_Amount_Available) > 0
						THEN
							SUM(final_total_loading_amount_200 )
						ELSE 
							SUM(forecast_loading_amount_200)
						END as forecast_loading_amount_200_after_currency_chest  
					, CASE WHEN 
							SUM(Total_Amount_Available) > 0
						THEN
							SUM(final_total_loading_amount_500 )
						ELSE 
							SUM(forecast_loading_amount_500)
						END as forecast_loading_amount_500_after_currency_chest  
					, CASE WHEN 
							SUM(Total_Amount_Available) > 0
						THEN
							SUM(final_total_loading_amount_2000 )
						ELSE 
							SUM(forecast_loading_amount_2000)
						END as forecast_loading_amount_2000_after_currency_chest  
					 --, SUM(forecast_loading_amount_100 ) as forecast_loading_amount_100_after_currency_chest
					 --, SUM(forecast_loading_amount_200 ) as forecast_loading_amount_200_after_currency_chest
					 --, SUM(forecast_loading_amount_500 ) as forecast_loading_amount_500_after_currency_chest
					 --, SUM(forecast_loading_amount_2000) as forecast_loading_amount_2000_after_currency_chest
					 , SUM(remaining_capacity_amount_100 ) as remaining_capacity_100_feeder_level
					 , SUM(remaining_capacity_amount_200 ) as remaining_capacity_200_feeder_level
					 , SUM(remaining_capacity_amount_500 ) as remaining_capacity_500_feeder_level
					 , SUM(remaining_capacity_amount_2000) as remaining_capacity_2000_feeder_level
					 , SUM(final_vaulting_amount_100) as final_vaulting_amount_100
					 , SUM(final_vaulting_amount_200) as final_vaulting_amount_200
					 , SUM(final_vaulting_amount_500) as final_vaulting_amount_500
					 , SUM(final_vaulting_amount_2000) as final_vaulting_amount_2000
					 , SUM(final_vaulting_amount_total) as final_vaulting_amount_total
					 , FLOOR(SUM(final_vaulting_amount_500)/@denomination_wise_round_off_500_currency_chest)*@denomination_wise_round_off_500_currency_chest as forecasted_vaulting_amount_500
				  INTO #currency_chest_feeder_branch1
				  from #final_feeder_level_allocated_dataset
				  WHERE is_currency_chest = 'Yes'
				  AND (currency_chest_adjustment_100 <> 0 
						OR currency_chest_adjustment_200 <> 0 
						OR currency_chest_adjustment_500 <> 0 
						OR currency_chest_adjustment_2000 <> 0
					)
				  group by project_id, bank_code, feeder_branch_code,is_currency_chest


			--	 select * from #final_feeder_level_allocated_dataset where feeder_branch_code = 'PIMPRI'
				  DROP TABLE IF EXISTS #currency_chest_feeder_branch

				  SELECT * ,
							final_vaulting_amount_500	- forecasted_vaulting_amount_500 as currency_chest_vault_difference		  
				  INTO #currency_chest_feeder_branch
				  FROM #currency_chest_feeder_branch1


				 -- SELECT * FROM #currency_chest_feeder_branch
			--	  DROP TABLE IF EXISTS #final_feeder_level_allocated_dataset

		--	SELECT * FROm #final_feeder_level_allocated_dataset  where feeder_branch_code = 'BHANDARA MAIN_0328'
		--	 select SUM(final_total_loading_amount_currency_chest) from #final_atm_level_allocated_dataset 
				--select * from #final_feeder_level_allocated_dataset where  feeder_branch_code='Araria'
			 
				  DROP TABLE IF EXISTS #final_atm_level_allocated_dataset --where  feeder_branch_code='Araria'

				  SELECT  *
						, ROW_NUMBER() OVER (PARTITION BY feeder_branch_code ORDER BY final_total_loading_amount asc) as allocation_rank
						, 
						  CASE	
							WHEN withdrawal_type = 'Vault' and final_total_loading_amount = 0
							THEN 0
							ELSE
							  final_total_loading_amount_100 
							END as final_total_loading_amount_100_currency_chest
						--, final_total_loading_amount_200 as final_total_loading_amount_200_currency_chest
						--, final_total_loading_amount_500 as final_total_loading_amount_500_currency_chest
						--, final_total_loading_amount_2000 as final_total_loading_amount_2000_currency_chest
						,
							CASE WHEN withdrawal_type = 'Vault' and final_total_loading_amount = 0
							THEN 0
							ELSE
							  final_total_loading_amount_200 
						END AS final_total_loading_amount_200_currency_chest
						,
							CASE WHEN withdrawal_type = 'Vault' and final_total_loading_amount = 0
							THEN 0
							ELSE
							  final_total_loading_amount_500 
						END AS final_total_loading_amount_500_currency_chest
						,	CASE WHEN withdrawal_type = 'Vault' and final_total_loading_amount = 0
							THEN 0
							ELSE
							  final_total_loading_amount_2000 
						END AS final_total_loading_amount_2000_currency_chest
						--,	CASE WHEN withdrawal_type = 'Vault' and final_total_loading_amount <> 0
						--	THEN 0
						--	ELSE
						--	  final_total_loading_amount_2000 
						--END AS final_total_loading_amount_2000_currency_chest
						, CASE	
							WHEN withdrawal_type = 'Vault' and final_total_loading_amount = 0
							THEN 0
							ELSE
							  final_total_loading_amount 
							END as final_total_loading_amount_currency_chest
						--, final_total_loading_amount as final_total_loading_amount_currency_chest
						, remaining_capacity_amount_100 as remaining_capacity_amount_100_after_currency_chest
						, remaining_capacity_amount_200 as remaining_capacity_amount_200_after_currency_chest
						, remaining_capacity_amount_500 as remaining_capacity_amount_500_after_currency_chest
						, remaining_capacity_amount_2000 as remaining_capacity_amount_2000_after_currency_chest
						, atm_vaulting_amount_after_pre_availability_100 as atm_vaulting_amount_after_pre_availability_100_cc
						, atm_vaulting_amount_after_pre_availability_200 as atm_vaulting_amount_after_pre_availability_200_cc
						, atm_vaulting_amount_after_pre_availability_500 as atm_vaulting_amount_after_pre_availability_500_cc
						, atm_vaulting_amount_after_pre_availability_2000 as atm_vaulting_amount_after_pre_availability_2000_cc
						, total_atm_vaulting_amount_after_pre_availability as total_atm_vaulting_amount_after_pre_availability_Cc
				  INTO #final_atm_level_allocated_dataset
				  from #final_atm_level_allocated_dataset_temp

			  	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('Inserting data into #final_atm_level_allocated_dataset completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


				--select * from #final_atm_level_allocated_dataset where feeder_branch_code = 'PIMPRI'
				--select * from #final_feeder_level_allocated_dataset where total_indent_after_pre_availabiliy_allocation < 0 
		--	  select* from #currency_chest_feeder_branch where  feeder_branch_code = 'PIMPRI'
				
		--		select SUM(final_total_loading_amount_500),
		--		SUM(final_total_loading_amount_500_currency_chest) from #final_atm_level_allocated_dataset
		
		--SELECT * FROM #currency_chest_feeder_branch
		
		--		update #final_atm_level_allocated_dataset
		--		set remaining_capacity_amount_500 = 0
		--		,	remaining_capacity_amount_500_after_currency_chest = 0
		--		where feeder_branch_code = 'AMRAVATI' 

		--		UPDATE #currency_chest_feeder_branch
		--		SET remaining_capacity_500_feeder_level = 0
		--		where feeder_branch_code = 'AMRAVATI' 
		
		--select SUM(final_total_loading_amount_500),
		--	   SUM(final_total_loading_amount_500_currency_chest)
		--	 from #final_atm_level_allocated_dataset  where feeder_branch_code = 'KOLHAPUR'

		--SELECT * FROM #final_atm_level_allocated_dataset 
		--						WHERE final_total_loading_amount_500 > 0 
		--	order by final_total_loading_amount desc
		-- select SUM(atm_vaulting_amount_after_pre_availability_500_cc) from #final_atm_level_allocated_dataset where feeder_branch_code = 'PIMPRI' 
		
		--select SUM(final_total_loading_amount),SUM(final_total_loading_amount_currency_chest) from #final_atm_level_allocated_dataset  
		
		--SELECT 36000000%500000
		--DECLARE @MaxDestinationDate DATE
		--DECLARE @dispenseformula NVARCHAR(50)
		--DECLARE @confidence_factor NVARCHAR(50)
		--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		--DECLARE @denomination_wise_round_off_100 INT			 -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_200 int      
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100_currency_chest int      
		--DECLARE @denomination_wise_round_off_200_currency_chest int
		--DECLARE @denomination_wise_round_off_500_currency_chest int
		--DECLARE @denomination_wise_round_off_2000_currency_chest INT
		--declare @default_avg_dispense int
		--declare @vaulting_for_normal_weekday_percentage float,
		--		@vaulting_for_normal_weekend_percentage float,
		--		@vaulting_for_extended_weekend_percentage float
		
		
		--Select	@confidence_factor = confidence_factor
		--, @dispenseformula = dispenseformula
		--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		--, @denomination_wise_round_off_100_currency_chest = denomination_wise_round_off_100_currency_chest
		--, @denomination_wise_round_off_200_currency_chest = denomination_wise_round_off_200_currency_chest
		--, @denomination_wise_round_off_500_currency_chest = denomination_wise_round_off_500_currency_chest
		--, @denomination_wise_round_off_2000_currency_chest = denomination_wise_round_off_2000_currency_chest
		--,@default_avg_dispense = default_average_dispense
		--, @vaulting_for_normal_weekday_percentage = vaulting_for_normal_weekday_percentage
		--, @vaulting_for_normal_weekend_percentage = vaulting_for_extended_weekend_percentage
		--, @vaulting_for_extended_weekend_percentage = vaulting_for_extended_weekend_percentage
		-- from system_settings where record_status = 'Active'

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			 values ('Cursor started for currency chest logic', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
 DECLARE currencyCusror CURSOR READ_ONLY LOCAL 
   FOR
    SELECT project_id
		  , bank_code
		  , feeder_branch_code
		  , forecast_loading_amount_100_after_currency_chest
		  , forecast_loading_amount_200_after_currency_chest
		  , forecast_loading_amount_500_after_currency_chest
		  , forecast_loading_amount_2000_after_currency_chest
		  , currency_chest_adjustment_100
		  , currency_chest_adjustment_200
		  , currency_chest_adjustment_500
		  , currency_chest_adjustment_2000
		  , remaining_capacity_100_feeder_level
		  , remaining_capacity_200_feeder_level
		  , remaining_capacity_500_feeder_level
		  , remaining_capacity_2000_feeder_level
		  , currency_chest_vault_difference
		FROM  #currency_chest_feeder_branch
	--WHERE feeder_branch_code='PIMPRI'
	ORDER BY feeder_branch_code



	declare 
		  @project_id_cc						nvarchar(50)
		, @bank_code_cc							nvarchar(50)
		, @feeder_branch_code_cc				nvarchar(50)
		, @forecast_loading_100_after_cc		BIGINT 
		, @forecast_loading_200_after_cc		BIGINT 
		, @forecast_loading_500_after_cc		BIGINT 
		, @forecast_loading_2000_after_cc		BIGINT 
		, @currency_adjustment_100				BIGINT
		, @currency_adjustment_200				BIGINT
		, @currency_adjustment_500				BIGINT
		, @currency_adjustment_2000				BIGINT
		, @remaining_capacity_feeder_level_100  BIGINT
		, @remaining_capacity_feeder_level_200  BIGINT
		, @remaining_capacity_feeder_level_500  BIGINT
		, @remaining_capacity_feeder_level_2000 BIGINT
		, @currency_chest_vault_difference      BIGINT


	OPEN currencyCusror

	FETCH NEXT FROM currencyCusror INTO 
					  @project_id_cc		    
					, @bank_code_cc			    
					, @feeder_branch_code_cc 
					, @forecast_loading_100_after_cc
					, @forecast_loading_200_after_cc
					, @forecast_loading_500_after_cc
					, @forecast_loading_2000_after_cc   
					, @currency_adjustment_100  
					, @currency_adjustment_200  
					, @currency_adjustment_500  
					, @currency_adjustment_2000 
					, @remaining_capacity_feeder_level_100 
					, @remaining_capacity_feeder_level_200 
					, @remaining_capacity_feeder_level_500 
    				, @remaining_capacity_feeder_level_2000
					, @currency_chest_vault_difference

    WHILE @@FETCH_STATUS = 0 
    BEGIN		
			
		WHILE(@currency_adjustment_500 <> 0)	
		BEGIN
			IF CURSOR_STATUS('local','currency_chest_2')>=-1
			BEGIN
				DEALLOCATE currency_chest_2
			END
			DECLARE currency_chest_2 cursor Read_Only LOCAL
			FOR
			--DECLARE @currency_adjustment_500 BIGINT = 100000
			--DECLARE @remaining_capacity_feeder_level_500 BIGINT = 1000000
				SELECT   atm_id
						,bank_code
						,feeder_branch_code
						,project_id
						,insurance_limit
						,limit_amount
						,final_total_loading_amount_100
						,final_total_loading_amount_200
						,final_total_loading_amount_500
						,final_total_loading_amount_2000
						,final_total_loading_amount
						,final_total_loading_amount_100_currency_chest
						,final_total_loading_amount_200_currency_chest
						,final_total_loading_amount_500_currency_chest
						,final_total_loading_amount_2000_currency_chest
						,final_total_loading_amount_currency_chest
						,CASE 
							WHEN (@currency_adjustment_500 > @remaining_capacity_feeder_level_500)
								THEN 
									ROW_NUMBER() OVER (PARTITION BY feeder_branch_code ORDER BY final_total_loading_amount desc)
							ELSE
								CASE
									WHEN @currency_adjustment_500 > 0
									THEN	
										ROW_NUMBER() OVER (PARTITION BY feeder_branch_code ORDER BY final_total_loading_amount asc) 
									ELSE
										ROW_NUMBER() OVER (PARTITION BY feeder_branch_code ORDER BY final_total_loading_amount desc) 
							END
						END as allocation_rank
						,remaining_capacity_amount_100
						,remaining_capacity_amount_200
						,remaining_capacity_amount_500
						,remaining_capacity_amount_2000
						,remaining_capacity_amount_100_after_currency_chest
						,remaining_capacity_amount_200_after_currency_chest
						,remaining_capacity_amount_500_after_currency_chest
						,remaining_capacity_amount_2000_after_currency_chest
						,atm_vaulting_amount_after_pre_availability_500_cc
						FROM #final_atm_level_allocated_dataset WHERE  feeder_branch_code=@feeder_branch_code_cc
						and bank_code = @bank_code_cc and project_id =@project_id_cc AND final_total_loading_amount_500 > 0
						ORDER BY allocation_rank asc

					DECLARE   @atmid_cc										    NVARCHAR(100)
							, @bank_cc										    NVARCHAR(100)
							, @feeder_cc										NVARCHAR(100)
							, @project_cc										NVARCHAR(100)
							, @insurance_limit_cc                               BIGINT
							, @limit_amount_cc									BIGINT
							, @final_total_loading_amount_100					BIGINT
							, @final_total_loading_amount_200					BIGINT
							, @final_total_loading_amount_500					BIGINT
							, @final_total_loading_amount_2000					BIGINT
							, @final_total_loading_amount						BIGINT
							, @final_total_loading_amount_100_currency_chest    BIGINT
							, @final_total_loading_amount_200_currency_chest	BIGINT
							, @final_total_loading_amount_500_currency_chest	BIGINT
							, @final_total_loading_amount_2000_currency_chest	BIGINT
							, @final_total_loading_amount_currency_chest		BIGINT
							, @allocation_rank									INT
							, @remaining_Capacity_100							BIGINT
							, @remaining_Capacity_200							BIGINT
							, @remaining_Capacity_500							BIGINT
							, @remaining_Capacity_2000							BIGINT
							, @remaining_Capacity_after_currency_chest_100		BIGINT
							, @remaining_Capacity_after_currency_chest_200		BIGINT
							, @remaining_Capacity_after_currency_chest_500		BIGINT
							, @remaining_Capacity_after_currency_chest_2000		BIGINT
							, @atm_vault_after_cc_500							BIGINT

						DECLARE @grouping_capacity_500 BIGINT = (SELECT SUM(remaining_capacity_amount_500) FROM #final_atm_level_allocated_dataset 
								WHERE final_total_loading_amount_500 > 0 
								and feeder_branch_code = @feeder_branch_code_cc and bank_code = @bank_code_cc AND project_id=@project_id_cc)
						

				
					
						OPEN currency_chest_2
					
							FETCH NEXT FROM currency_chest_2 INTO 
											  @atmid_cc		
											, @bank_cc
											, @feeder_cc
											, @project_cc							     
											, @insurance_limit_cc                                
											, @limit_amount_cc
											, @final_total_loading_amount_100	
											, @final_total_loading_amount_200	
											, @final_total_loading_amount_500	
											, @final_total_loading_amount_2000	
											, @final_total_loading_amount																					 
											, @final_total_loading_amount_100_currency_chest     
											, @final_total_loading_amount_200_currency_chest	 
											, @final_total_loading_amount_500_currency_chest	 
											, @final_total_loading_amount_2000_currency_chest	 
											, @final_total_loading_amount_currency_chest		
											, @allocation_rank
											, @remaining_Capacity_100						
											, @remaining_Capacity_200						
											, @remaining_Capacity_500						
											, @remaining_Capacity_2000						
											, @remaining_Capacity_after_currency_chest_100	
											, @remaining_Capacity_after_currency_chest_200	
											, @remaining_Capacity_after_currency_chest_500	
											, @remaining_Capacity_after_currency_chest_2000
											, @atm_vault_after_cc_500	
										
							WHILE @@FETCH_STATUS = 0  
							BEGIN	
								--SELECT @feeder_cc
								--Select @atmid_cc
							--WHILE ( @currency_adjustment_500 <> 0)
							--BEGIN
									IF(@currency_adjustment_500 <> 0)
									BEGIN
									--SELECT @currency_adjustment_500
									--SELECT @remaining_capacity_feeder_level_500
									--SELECT @grouping_capacity_500
									--SELECT @remaining_Capacity_after_currency_chest_500
									--SELECT @grouping_capacity_500
									--SELECT @currency_chest_vault_difference
										IF ((@currency_adjustment_500 > @remaining_capacity_feeder_level_500) OR (@currency_adjustment_500 > @grouping_capacity_500))
										BEGIN 
											SET @currency_adjustment_500 = (SELECT FLOOR(@forecast_loading_500_after_cc/@denomination_wise_round_off_500_currency_chest)*@denomination_wise_round_off_500_currency_chest - @forecast_loading_500_after_cc)

											UPDATE #currency_chest_feeder_branch
											SET currency_chest_adjustment_500 = @currency_adjustment_500,
												currency_chest_left_amount_500 = @currency_adjustment_500
											WHERE feeder_branch_code = @feeder_cc and bank_code= @bank_cc
											and project_id = @project_cc

										--	SELECT * FROM #currency_chest_feeder_branch
												
										END
											SET @final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest
											SET @final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest		

											IF (@currency_adjustment_500 / @denomination_wise_round_off_500 = 1)
												BEGIN
								
													IF (@remaining_Capacity_after_currency_chest_500>=@denomination_wise_round_off_500)
													BEGIN
														IF( @currency_adjustment_500 = @grouping_capacity_500)
														BEGIN
															SET @final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest 
															SET @final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest 
															SET @remaining_Capacity_after_currency_chest_500 = @remaining_Capacity_after_currency_chest_500 
															SET @atm_vault_after_cc_500 = @atm_vault_after_cc_500


															UPDATE #final_atm_level_allocated_dataset
															SET final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest + @currency_adjustment_500,
																final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest + @currency_adjustment_500,
																remaining_capacity_amount_500_after_currency_chest = @remaining_Capacity_after_currency_chest_500 - @currency_adjustment_500
															WHERE atm_id = @atmid_cc

															IF ( @currency_chest_vault_difference <> 0 AND @atm_vault_after_cc_500 <> 0 )
															BEGIN
																UPDATE #final_atm_level_allocated_dataset
																SET atm_vaulting_amount_after_pre_availability_500_cc = atm_vaulting_amount_after_pre_availability_500_cc - @currency_chest_vault_difference
																WHERE atm_id = @atmid_cc

																SET @currency_chest_vault_difference = 0
															END
															
															--SELECT * FROM #final_atm_level_allocated_dataset WHERE atm_id = @atmid_cc

															
															UPDATE #currency_chest_feeder_branch
															SET forecast_loading_amount_500_after_currency_chest = forecast_loading_amount_500_after_currency_chest + @currency_adjustment_500 
																,currency_chest_left_amount_500 = currency_chest_left_amount_500 - @currency_adjustment_500
															WHERE feeder_branch_code = @feeder_cc and bank_code= @bank_cc
															and project_id = @project_cc
																
															SET @currency_adjustment_500 = @currency_adjustment_500 - @currency_adjustment_500
															

														END
														ELSE
														BEGIN
															SET @final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest + @denomination_wise_round_off_500
															SET @final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest + @denomination_wise_round_off_500
															SET @remaining_Capacity_after_currency_chest_500 = @remaining_Capacity_after_currency_chest_500 + (@final_total_loading_amount_500 -@final_total_loading_amount_500_currency_chest) 

															UPDATE #final_atm_level_allocated_dataset
															SET final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest,
															final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest,
															remaining_capacity_amount_500_after_currency_chest = @remaining_Capacity_after_currency_chest_500 
															WHERE atm_id = @atmid_cc

															
															IF ( @currency_chest_vault_difference <> 0 AND @atm_vault_after_cc_500 <> 0 )
															BEGIN
																UPDATE #final_atm_level_allocated_dataset
																SET atm_vaulting_amount_after_pre_availability_500_cc = atm_vaulting_amount_after_pre_availability_500_cc - @currency_chest_vault_difference
																WHERE atm_id = @atmid_cc

																SET @currency_chest_vault_difference = 0
															END
															--SELECT * FROM #final_atm_level_allocated_dataset WHERE atm_id = @atmid_cc


															UPDATE #currency_chest_feeder_branch
															SET forecast_loading_amount_500_after_currency_chest = forecast_loading_amount_500_after_currency_chest + @currency_adjustment_500
																,currency_chest_left_amount_500 = currency_chest_left_amount_500 - @currency_adjustment_500
															WHERE feeder_branch_code = @feeder_cc and bank_code= @bank_cc
															and project_id = @project_cc

															SET @currency_adjustment_500 = @currency_adjustment_500 - @denomination_wise_round_off_500
															SET @currency_chest_vault_difference = 0
														END
												END
													


													--ELSE
													----BEGIN
													----	FETCH NEXT FROM currency_chest_2 INTO 
													----		  @atmid_cc		
													----		, @bank_cc
													----		, @feeder_cc
													----		, @project_cc							     
													----		, @insurance_limit_cc                                   
													----		, @limit_amount_cc	 
													----		, @final_total_loading_amount_100	    
													----		, @final_total_loading_amount_200		 
													----		, @final_total_loading_amount_500		 
													----		, @final_total_loading_amount_2000		 
													----		, @final_total_loading_amount							
													----		, @final_total_loading_amount_100_currency_chest    
													----		, @final_total_loading_amount_200_currency_chest	
													----		, @final_total_loading_amount_500_currency_chest	
													----		, @final_total_loading_amount_2000_currency_chest	
													----		, @final_total_loading_amount_currency_chest		
													----		, @allocation_rank
													----		, @remaining_Capacity_100						
													----		, @remaining_Capacity_200						
													----		, @remaining_Capacity_500						
													----		, @remaining_Capacity_2000						
													----		, @remaining_Capacity_after_currency_chest_100	
													----		, @remaining_Capacity_after_currency_chest_200	
													----		, @remaining_Capacity_after_currency_chest_500	
													----		, @remaining_Capacity_after_currency_chest_2000	
													----		, @atm_vault_after_cc_500	
													----END

												END
											ELSE IF (@currency_adjustment_500 / @denomination_wise_round_off_500 > 1)
												BEGIN
												--	SELECT @remaining_Capacity_500
													IF (@remaining_Capacity_500>=@denomination_wise_round_off_500)
													BEGIN
														IF( @currency_adjustment_500 = @grouping_capacity_500)
															BEGIN
																SET @final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest 
																SET @final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest 
																SET @remaining_Capacity_after_currency_chest_500 = @remaining_Capacity_after_currency_chest_500 
																
																UPDATE #final_atm_level_allocated_dataset
																SET final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest + @currency_adjustment_500,
																	final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest + @currency_adjustment_500,
																	remaining_capacity_amount_500_after_currency_chest = @remaining_Capacity_after_currency_chest_500 - @currency_adjustment_500
																WHERE atm_id = @atmid_cc
															
																--SELECT * FROM #final_atm_level_allocated_dataset WHERE atm_id = @atmid_cc
																IF ( @currency_chest_vault_difference <> 0 AND @atm_vault_after_cc_500 <> 0 )
																BEGIN
																UPDATE #final_atm_level_allocated_dataset
																SET atm_vaulting_amount_after_pre_availability_500_cc = atm_vaulting_amount_after_pre_availability_500_cc - @currency_chest_vault_difference
																WHERE atm_id = @atmid_cc

																SET @currency_chest_vault_difference = 0
																END
																
																--SELECT * FROM #final_atm_level_allocated_dataset WHERE atm_id = @atmid_cc

																UPDATE #currency_chest_feeder_branch
																SET forecast_loading_amount_500_after_currency_chest = forecast_loading_amount_500_after_currency_chest + @currency_adjustment_500 
																	,currency_chest_left_amount_500 = currency_chest_left_amount_500 - @currency_adjustment_500
																WHERE feeder_branch_code = @feeder_cc and bank_code= @bank_cc
																and project_id = @project_cc
																
																SET @currency_adjustment_500 = @currency_adjustment_500 - @currency_adjustment_500
																
															END
															--SELECT 'In remaining'
														ELSE
															BEGIN
																SET @final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest +  @denomination_wise_round_off_500
																SET @final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest +  @denomination_wise_round_off_500
																SET @remaining_Capacity_after_currency_chest_500 = @remaining_Capacity_after_currency_chest_500 + (@final_total_loading_amount_500 -@final_total_loading_amount_500_currency_chest) 
																
																UPDATE #final_atm_level_allocated_dataset
																SET final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest,
																	final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest,
																	remaining_capacity_amount_500_after_currency_chest = @remaining_Capacity_after_currency_chest_500
																WHERE atm_id = @atmid_cc
															
															 	IF ( @currency_chest_vault_difference <> 0 AND @atm_vault_after_cc_500 <> 0)
																BEGIN
																UPDATE #final_atm_level_allocated_dataset
																SET atm_vaulting_amount_after_pre_availability_500_cc = atm_vaulting_amount_after_pre_availability_500_cc - @currency_chest_vault_difference
																WHERE atm_id = @atmid_cc

																SET @currency_chest_vault_difference = 0
																END
																
																--SELECT * FROM #final_atm_level_allocated_dataset WHERE atm_id = @atmid_cc
															
																SET @currency_adjustment_500 = @currency_adjustment_500 - @denomination_wise_round_off_500
																

																UPDATE #currency_chest_feeder_branch
																SET forecast_loading_amount_500_after_currency_chest = forecast_loading_amount_500_after_currency_chest + @denomination_wise_round_off_500 
																	,currency_chest_left_amount_500 = currency_chest_left_amount_500 - @denomination_wise_round_off_500
																WHERE feeder_branch_code = @feeder_cc and bank_code= @bank_cc
																and project_id = @project_cc
															END
													END
													--ELSE
													--BEGIN
													--	FETCH NEXT FROM currency_chest_2 INTO 
													--		  @atmid_cc		
													--		, @bank_cc
													--		, @feeder_cc
													--		, @project_cc							     
													--		, @insurance_limit_cc                                   
													--		, @limit_amount_cc	 
													--		, @final_total_loading_amount_100	    
													--		, @final_total_loading_amount_200		 
													--		, @final_total_loading_amount_500		 
													--		, @final_total_loading_amount_2000		 
													--		, @final_total_loading_amount							
													--		, @final_total_loading_amount_100_currency_chest    
													--		, @final_total_loading_amount_200_currency_chest	
													--		, @final_total_loading_amount_500_currency_chest	
													--		, @final_total_loading_amount_2000_currency_chest	
													--		, @final_total_loading_amount_currency_chest		
													--		, @allocation_rank
													--		, @remaining_Capacity_100						
													--		, @remaining_Capacity_200						
													--		, @remaining_Capacity_500						
													--		, @remaining_Capacity_2000						
													--		, @remaining_Capacity_after_currency_chest_100	
													--		, @remaining_Capacity_after_currency_chest_200	
													--		, @remaining_Capacity_after_currency_chest_500	
													--		, @remaining_Capacity_after_currency_chest_2000	
													--		, @atm_vault_after_cc_500	
													--END
												END
											ELSE IF (@currency_adjustment_500 / @denomination_wise_round_off_500 = -1)
												BEGIN

													SET @final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest - @denomination_wise_round_off_500
													SET @final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest - @denomination_wise_round_off_500
													SET @remaining_Capacity_after_currency_chest_500 = @remaining_Capacity_after_currency_chest_500 + (@final_total_loading_amount_500 -@final_total_loading_amount_500_currency_chest) 
											
													IF (@remaining_Capacity_after_currency_chest_500>(@final_total_loading_amount_500_currency_chest -@final_total_loading_amount_500))
													BEGIN
															UPDATE #final_atm_level_allocated_dataset
														SET final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest,
															final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest,
															remaining_capacity_amount_500_after_currency_chest = @remaining_Capacity_after_currency_chest_500
														WHERE atm_id = @atmid_cc
														
														IF ( @currency_chest_vault_difference <> 0 AND @atm_vault_after_cc_500 <> 0 )
														BEGIN
															UPDATE #final_atm_level_allocated_dataset
															SET atm_vaulting_amount_after_pre_availability_500_cc = atm_vaulting_amount_after_pre_availability_500_cc - @currency_chest_vault_difference
															WHERE atm_id = @atmid_cc

															SET @currency_chest_vault_difference = 0
														END
														
														--SELECT * FROM #final_atm_level_allocated_dataset WHERE atm_id = @atmid_cc

														UPDATE #currency_chest_feeder_branch
														SET forecast_loading_amount_500_after_currency_chest = forecast_loading_amount_500_after_currency_chest - @denomination_wise_round_off_500,
															currency_chest_left_amount_500 = currency_chest_left_amount_500 + @denomination_wise_round_off_500 												
														WHERE feeder_branch_code = @feeder_cc and bank_code= @bank_cc
														and project_id = @project_cc

														SET @currency_adjustment_500 = @currency_adjustment_500 + @denomination_wise_round_off_500
														
													END
													--ELSE
													--BEGIN
													--	FETCH NEXT FROM currency_chest_2 INTO 
													--		  @atmid_cc		
													--		, @bank_cc
													--		, @feeder_cc
													--		, @project_cc							     
													--		, @insurance_limit_cc                                   
													--		, @limit_amount_cc	 
													--		, @final_total_loading_amount_100	    
													--		, @final_total_loading_amount_200		 
													--		, @final_total_loading_amount_500		 
													--		, @final_total_loading_amount_2000		 
													--		, @final_total_loading_amount							
													--		, @final_total_loading_amount_100_currency_chest    
													--		, @final_total_loading_amount_200_currency_chest	
													--		, @final_total_loading_amount_500_currency_chest	
													--		, @final_total_loading_amount_2000_currency_chest	
													--		, @final_total_loading_amount_currency_chest		
													--		, @allocation_rank
													--		, @remaining_Capacity_100						
													--		, @remaining_Capacity_200						
													--		, @remaining_Capacity_500						
													--		, @remaining_Capacity_2000						
													--		, @remaining_Capacity_after_currency_chest_100	
													--		, @remaining_Capacity_after_currency_chest_200	
													--		, @remaining_Capacity_after_currency_chest_500	
													--		, @remaining_Capacity_after_currency_chest_2000	
													--		, @atm_vault_after_cc_500	
													--END
												END
											ELSE IF (@currency_adjustment_500 / @denomination_wise_round_off_500 < 1)
												BEGIN
											
													SET @final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest  - @denomination_wise_round_off_500
													SET @final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest  - @denomination_wise_round_off_500
													SET @remaining_Capacity_after_currency_chest_500 = @remaining_Capacity_after_currency_chest_500 + (@final_total_loading_amount_500 -@final_total_loading_amount_500_currency_chest) 
													
													IF (@remaining_Capacity_after_currency_chest_500>=(@final_total_loading_amount_500_currency_chest -@final_total_loading_amount_500))
													BEGIN
														UPDATE #final_atm_level_allocated_dataset
														SET final_total_loading_amount_500_currency_chest = @final_total_loading_amount_500_currency_chest,
															final_total_loading_amount_currency_chest = @final_total_loading_amount_currency_chest,
															remaining_capacity_amount_500_after_currency_chest = @remaining_Capacity_after_currency_chest_500
														WHERE atm_id = @atmid_cc
														
														
														IF ( @currency_chest_vault_difference <> 0 AND @atm_vault_after_cc_500 <> 0 )
														BEGIN
														UPDATE #final_atm_level_allocated_dataset
														SET atm_vaulting_amount_after_pre_availability_500_cc = atm_vaulting_amount_after_pre_availability_500_cc - @currency_chest_vault_difference
														WHERE atm_id = @atmid_cc
														
														SET @currency_chest_vault_difference = 0

														END

														--SELECT * FROM #final_atm_level_allocated_dataset WHERE atm_id = @atmid_cc

														SET @currency_adjustment_500 = @currency_adjustment_500 + @denomination_wise_round_off_500
														
														--SELECT * FROM #final_atm_level_allocated_dataset WHERE atm_id = @atmid_cc
														UPDATE #currency_chest_feeder_branch
														SET forecast_loading_amount_500_after_currency_chest = forecast_loading_amount_500_after_currency_chest - @denomination_wise_round_off_500,
															currency_chest_left_amount_500 = currency_chest_left_amount_500 + @denomination_wise_round_off_500
														WHERE feeder_branch_code = @feeder_cc and bank_code= @bank_cc
														and project_id = @project_cc

													END
													--ELSE
													--BEGIN
													--	FETCH NEXT FROM currency_chest_2 INTO 
													--		  @atmid_cc		
													--		, @bank_cc
													--		, @feeder_cc
													--		, @project_cc							     
													--		, @insurance_limit_cc                                   
													--		, @limit_amount_cc	 
													--		, @final_total_loading_amount_100	    
													--		, @final_total_loading_amount_200		 
													--		, @final_total_loading_amount_500		 
													--		, @final_total_loading_amount_2000		 
													--		, @final_total_loading_amount							
													--		, @final_total_loading_amount_100_currency_chest    
													--		, @final_total_loading_amount_200_currency_chest	
													--		, @final_total_loading_amount_500_currency_chest	
													--		, @final_total_loading_amount_2000_currency_chest	
													--		, @final_total_loading_amount_currency_chest		
													--		, @allocation_rank
													--		, @remaining_Capacity_100						
													--		, @remaining_Capacity_200						
													--		, @remaining_Capacity_500						
													--		, @remaining_Capacity_2000						
													--		, @remaining_Capacity_after_currency_chest_100	
													--		, @remaining_Capacity_after_currency_chest_200	
													--		, @remaining_Capacity_after_currency_chest_500	
													--		, @remaining_Capacity_after_currency_chest_2000	
													--		, @atm_vault_after_cc_500	
													--END
												END

										END
										
							FETCH NEXT FROM currency_chest_2 INTO 
											  @atmid_cc		
											, @bank_cc
											, @feeder_cc
											, @project_cc							     
											, @insurance_limit_cc                                   
											, @limit_amount_cc	 
											, @final_total_loading_amount_100	    
											, @final_total_loading_amount_200		 
											, @final_total_loading_amount_500		 
											, @final_total_loading_amount_2000		 
											, @final_total_loading_amount							
											, @final_total_loading_amount_100_currency_chest    
											, @final_total_loading_amount_200_currency_chest	
											, @final_total_loading_amount_500_currency_chest	
											, @final_total_loading_amount_2000_currency_chest	
											, @final_total_loading_amount_currency_chest		
											, @allocation_rank
											, @remaining_Capacity_100						
											, @remaining_Capacity_200						
											, @remaining_Capacity_500						
											, @remaining_Capacity_2000						
											, @remaining_Capacity_after_currency_chest_100	
											, @remaining_Capacity_after_currency_chest_200	
											, @remaining_Capacity_after_currency_chest_500	
											, @remaining_Capacity_after_currency_chest_2000
											, @atm_vault_after_cc_500													
							END
						
						--FETCH NEXT FROM currency_chest_2 INTO 
						--					  @atmid_cc		
						--					, @bank_cc
						--					, @feeder_cc
						--					, @project_cc							     
						--					, @insurance_limit_cc                                   
						--					, @limit_amount_cc	 
						--					, @final_total_loading_amount_100	    
						--					, @final_total_loading_amount_200		 
						--					, @final_total_loading_amount_500		 
						--					, @final_total_loading_amount_2000		 
						--					, @final_total_loading_amount							
						--					, @final_total_loading_amount_100_currency_chest    
						--					, @final_total_loading_amount_200_currency_chest	
						--					, @final_total_loading_amount_500_currency_chest	
						--					, @final_total_loading_amount_2000_currency_chest	
						--					, @final_total_loading_amount_currency_chest		
						--					, @allocation_rank
						--					, @remaining_Capacity_100						
						--					, @remaining_Capacity_200						
						--					, @remaining_Capacity_500						
						--					, @remaining_Capacity_2000						
						--					, @remaining_Capacity_after_currency_chest_100	
						--					, @remaining_Capacity_after_currency_chest_200	
						--					, @remaining_Capacity_after_currency_chest_500	
						--					, @remaining_Capacity_after_currency_chest_2000
						--					, @atm_vault_after_cc_500													
						--	END
							CLOSE currency_chest_2 
							DEALLOCATE currency_chest_2	
						END

FETCH NEXT FROM currencyCusror INTO 
					  @project_id_cc		    
					, @bank_code_cc			    
					, @feeder_branch_code_cc   
					, @forecast_loading_100_after_cc
					, @forecast_loading_200_after_cc
					, @forecast_loading_500_after_cc
					, @forecast_loading_2000_after_cc 
					, @currency_adjustment_100  
					, @currency_adjustment_200  
					, @currency_adjustment_500  
					, @currency_adjustment_2000 
					, @remaining_capacity_feeder_level_100 
					, @remaining_capacity_feeder_level_200 
					, @remaining_capacity_feeder_level_500 
    				, @remaining_capacity_feeder_level_2000
					, @currency_chest_vault_difference
END
CLOSE currencyCusror 
DEALLOCATE currencyCusror	
--	NA0260C2 -- wrong


UPDATE #final_feeder_level_allocated_dataset
		SET 
			--vault_closing_balance_after_pre_availability_100 = 
			--	CASE WHEN final_total_loading_amount_100 > final_total_atm_loading_after_pre_availability_allocation_amount_100
			--	THEN 
			--		vault_closing_balance_after_pre_availability_100 + ABS(currency_chest_adjustment_100)
			--	ELSE
			--		vault_closing_balance_after_pre_availability_100
			--	END,
			--vault_closing_balance_after_pre_availability_200 = 
			--	CASE WHEN final_total_loading_amount_200 > final_total_atm_loading_after_pre_availability_allocation_amount_200
			--	THEN 
			--		vault_closing_balance_after_pre_availability_200 + ABS(currency_chest_adjustment_200)
			--	ELSE
			--		vault_closing_balance_after_pre_availability_200
			--	END,
			--vault_closing_balance_after_pre_availability_500 = 
			--	CASE WHEN final_total_loading_amount_500 > final_total_atm_loading_after_pre_availability_allocation_amount_500
			--	THEN 
			--		vault_closing_balance_after_pre_availability_500 + ABS(currency_chest_adjustment_500)
			--	ELSE
			--		vault_closing_balance_after_pre_availability_500
			--	END,
			--vault_closing_balance_after_pre_availability_2000 = 
			--	CASE WHEN final_total_loading_amount_2000 > final_total_atm_loading_after_pre_availability_allocation_amount_2000
			--	THEN 
			--		vault_closing_balance_after_pre_availability_2000 + ABS(currency_chest_adjustment_2000)
			--	ELSE
			--		vault_closing_balance_after_pre_availability_2000
			--	END,
			remaining_capacity_amount_100 = 
				CASE 
				WHEN final_total_loading_amount_100 > final_total_atm_loading_after_pre_availability_allocation_amount_100
				THEN 
					remaining_capacity_amount_100 + (final_total_loading_amount_100 - final_total_atm_loading_after_pre_availability_allocation_amount_100)
				WHEN final_total_loading_amount_100 < final_total_atm_loading_after_pre_availability_allocation_amount_100
				THEN
					remaining_capacity_amount_100 - (final_total_atm_loading_after_pre_availability_allocation_amount_100 - final_total_loading_amount_100)
				ELSE
					remaining_capacity_amount_100
				END,
			remaining_capacity_amount_200 = 
				CASE 
				WHEN final_total_loading_amount_200 > final_total_atm_loading_after_pre_availability_allocation_amount_200
				THEN 
					remaining_capacity_amount_200 + (final_total_loading_amount_200 - final_total_atm_loading_after_pre_availability_allocation_amount_200)
				WHEN final_total_loading_amount_200 < final_total_atm_loading_after_pre_availability_allocation_amount_200
				THEN
					remaining_capacity_amount_200 - (final_total_atm_loading_after_pre_availability_allocation_amount_200 - final_total_loading_amount_200)
				ELSE
					remaining_capacity_amount_200
				END,

			remaining_capacity_amount_500 = 
				CASE 
				WHEN final_total_loading_amount_500 > final_total_atm_loading_after_pre_availability_allocation_amount_500
				THEN 
					remaining_capacity_amount_500 + (final_total_loading_amount_500 - final_total_atm_loading_after_pre_availability_allocation_amount_500)
				WHEN final_total_loading_amount_500 < final_total_atm_loading_after_pre_availability_allocation_amount_500
				THEN
					remaining_capacity_amount_500 - (final_total_atm_loading_after_pre_availability_allocation_amount_500 - final_total_loading_amount_500)
				ELSE
					remaining_capacity_amount_500
				END,

				remaining_capacity_amount_2000 = 
				CASE 
				WHEN final_total_loading_amount_2000 > final_total_atm_loading_after_pre_availability_allocation_amount_2000
				THEN 
					remaining_capacity_amount_2000 + (final_total_loading_amount_2000 - final_total_atm_loading_after_pre_availability_allocation_amount_2000)
				WHEN final_total_loading_amount_2000 < final_total_atm_loading_after_pre_availability_allocation_amount_2000
				THEN
					remaining_capacity_amount_2000 - (final_total_atm_loading_after_pre_availability_allocation_amount_2000 - final_total_loading_amount_2000)
				ELSE
					remaining_capacity_amount_2000
				END

		--UPDATE #final_feeder_level_allocated_dataset
		--SET vault_closing_balance_after_pre_availability_100 =
		--			CASE WHEN withdrawal_type = 'Vault' and vault_closing_balance_after_pre_availability_100 > 0
		--				 THEN vault_closing_balance_after_pre_availability_100 - final_total_atm_loading_after_pre_availability_allocation_amount_100
		--			ELSE 
		--				vault_closing_balance_after_pre_availability_100
		--			END,

		--	vault_closing_balance_after_pre_availability_200 =
		--			CASE WHEN withdrawal_type = 'Vault' and vault_closing_balance_after_pre_availability_200 > 0
		--			THEN vault_closing_balance_after_pre_availability_200 - final_total_atm_loading_after_pre_availability_allocation_amount_200
		--			ELSE
		--			vault_closing_balance_after_pre_availability_200
		--			END,
		--	vault_closing_balance_after_pre_availability_500 =
		--			CASE WHEN withdrawal_type = 'Vault' and vault_closing_balance_after_pre_availability_500> 0 
		--			THEN vault_closing_balance_after_pre_availability_500 - final_total_atm_loading_after_pre_availability_allocation_amount_500
		--			ELSE
		--			vault_closing_balance_after_pre_availability_500
		--			END,
		--	vault_closing_balance_after_pre_availability_2000 =
		--			CASE WHEN withdrawal_type = 'Vault'  and vault_closing_balance_after_pre_availability_2000 > 0
		--			THEN vault_closing_balance_after_pre_availability_2000 - final_total_atm_loading_after_pre_availability_allocation_amount_2000
		--			ELSE
		--			vault_closing_balance_after_pre_availability_2000
		--			END


			UPDATE #final_feeder_level_allocated_dataset
			SET		total_vault_closing_balance_after_pre_availability = 
					ISNULL(vault_closing_balance_after_pre_availability_100,0) +  
					ISNULL(vault_closing_balance_after_pre_availability_200,0) + 
					ISNULL(vault_closing_balance_after_pre_availability_500,0) + 
					ISNULL(vault_closing_balance_after_pre_availability_2000,0),
					total_indent_after_pre_availabiliy_allocation = 
					indent_after_pre_availabiliy_allocation_100 +
					indent_after_pre_availabiliy_allocation_200 + 
					indent_after_pre_availabiliy_allocation_500 + 
					indent_after_pre_availabiliy_allocation_2000

					--CASE WHEN 
					--	total_vault_balance > 0
					--	THEN
					--		(ISNULL(vault_balance_100,0) - ISNULL(vault_closing_balance_after_pre_availability_100,0)) +
					--		(ISNULL(vault_balance_200,0) - ISNULL(vault_closing_balance_after_pre_availability_200,0)) +
					--		(ISNULL(vault_balance_500,0) - ISNULL(vault_closing_balance_after_pre_availability_500,0)) +
					--		(ISNULL(vault_balance_2000,0) - ISNULL(vault_closing_balance_after_pre_availability_2000,0)) 
					--	ELSE
					--ISNUll(final_total_atm_loading_after_pre_availability_allocation_amount,0)+
					--ISNULL(vault_closing_balance_after_pre_availability_100,0) +  
					--ISNULL(vault_closing_balance_after_pre_availability_200,0) + 
					--ISNULL(vault_closing_balance_after_pre_availability_500,0) + 
					--ISNULL(vault_closing_balance_after_pre_availability_2000,0)
					,
					total_remaining_capacity_amount = 
					ISNUll(remaining_capacity_amount_100,0) + 
					ISNULL(remaining_capacity_amount_200,0) + 
					ISNULL(remaining_capacity_amount_500,0) + 
					ISNULL(remaining_capacity_amount_2000,0)


			  --select SUM(final_total_loading_amount_500),
					--SUM(final_total_loading_amount_500_currency_chest)
					--,SUm(final_total_loading_amount)
					--, SUM(final_total_loading_amount_currency_chest)
					--from #final_atm_level_allocated_dataset
			  

			  --select * from #final_feeder_level_allocated_dataset 


--Ernakulam SBI
--Kannur SBI
--Kollam SBI
--Mangalapuram
--PIMPRI - NA1154C1
--	WA1897C1 -- right
--	GFBU000010021
	--select sum(final_total_loading_amount_currency_chest),
	--		sum(final_total_loading_amount) 
	--		,sum(final_total_loading_amount_100_currency_chest),
	--		sum(final_total_loading_amount_100) ,
	--		sum(final_total_loading_amount_200_currency_chest),
	--		sum(final_total_loading_amount_200) ,
	--		sum(final_total_loading_amount_500_currency_chest),
	--		sum(final_total_loading_amount_500) ,
	--		sum(final_total_loading_amount_2000_currency_chest),
	--		sum(final_total_loading_amount_2000) 
	--		from #final_atm_level_allocated_dataset where feeder_branch_code = 'KOLHAPUR'
	
	
	--select * from #currency_chest_feeder_branch where feeder_branch_code = 'Treasury Branch Pune'


	--select * from #final_feeder_level_allocated_dataset where feeder_branch_code = 'Treasury Branch Pune'


	
	--select sum(atm_vaulting_amount_after_pre_availability_500),sum(atm_vaulting_amount_after_pre_availability_500_cc) from #final_atm_level_allocated_dataset where feeder_branch_code = 'PIMPRI'
	

	--select atm_vaulting_amount_after_pre_availability_500,
	--atm_vaulting_amount_after_pre_availability_500_cc, * from #final_atm_level_allocated_dataset where atm_vaulting_amount_after_pre_availability_500 <> atm_vaulting_amount_after_pre_availability_500_cc
	
	
	 --where feeder_branch_code='MUMBAI MAIN_00300'
			
			
			--UPDATE #final_feeder_level_allocated_dataset
			--SET indent_after_pre_availabiliy_allocation_500 =
			--	CASE WHEN 
			--	cast(round((1.0 *isnull(indent_after_pre_availabiliy_allocation_500,0) / @denomination_wise_round_off_500_currency_chest ),0) * @denomination_wise_round_off_500_currency_chest as bigint) > final_total_loading_amount_2000
					
			
	--select * from #final_feeder_level_allocated_dataset where feeder_branch_code = 'PIMPRI'
			
	--select * from #final_feeder_level_allocated_dataset where total_indent_after_pre_availabiliy_allocation < 0 	
			




	----atm_id='GFBU000009028'
	--select SUM(final_total_loading_amount_500)
	--		,SUM(final_total_loading_amount_500_currency_chest) from #final_atm_level_allocated_dataset where is_currency_chest = 'Yes'

	--and atm_id =  'CFNA000326004'
	--select * from #currency_chest_feeder_branch where feeder_branch_code='MUMBAI MAIN_00300'


			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			 values ('Cursor completed for currency chest logic', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		--select * from #currency_chest_feeder_branch where is_currency_chest = 'Yes'

		--select * FROM #final_atm_level_allocated_dataset where withdrawal_type = 'Vault' 


	 --************************************Cypher Code********************************************************
	  	-- Generate cypher code and amount to work here.(Pending)
		drop table if exists #cypher_code

	    Select *into #cypher_code
		from
		(
		  Select 
		  FL.bank_code,
		  FL.project_id,
		  FL.feeder_branch_code,
		  FL.indentdate,
		  FL.sol_id,
		  dbo.fn_sum_of_digits( ISNULL(FL.sol_id,0))  as  sol_id_cypher_code, --As per req :Function to get sum of all digits in Sol_id
		  CC_date.value  as date_value,
		  CC_date.cypher_code   as cypher_code_date,		
		  CC_day.value  as day_value,
		  CC_day.cypher_code as cypher_code_day,
		  CC_month.value as month_value,
		  CC_month.cypher_code as cypher_code_month, 
		  CC_year.value as year_value,
		  CC_year.cypher_code as cypher_code_year,
		  FL.total_indent_after_pre_availabiliy_allocation,
		  CC_amount.value as feeder_amount_value,
		  CC_amount.cypher_code as cypher_code_amount,
		  CC_date.cypher_code+dbo.fn_sum_of_digits( ISNULL(FL.sol_id,0)) +CC_month.cypher_code+CC_year.cypher_code +CC_day.cypher_code+ CC_amount.cypher_code 
		        as final_cypher_Code	  
	 
		from #final_feeder_level_allocated_dataset   FL

		  --Select  dbo.fn_sum_of_digits( ISNULL(FL.sol_id,0)) from #final_feeder_level_allocated_dataset   FL

		 join cypher_code CC_date
		on
		    FL.bank_code COLLATE DATABASE_DEFAULT =CC_date.bank_code
		and FL.project_id COLLATE DATABASE_DEFAULT =CC_date.project_id
		and CC_date.record_status='Active'
		and CC_date.category='Date' 
		and CC_date.value=datepart(dd,indentdate)


		 join cypher_code CC_day
		on
		    FL.bank_code COLLATE DATABASE_DEFAULT =CC_day.bank_code
		and FL.project_id COLLATE DATABASE_DEFAULT =CC_day.project_id
		and CC_day.record_status='Active'
		and CC_day.category='Day' 
		and CC_day.value= DATENAME(DW,indentdate)


		join cypher_code CC_month
		on
		    FL.bank_code COLLATE DATABASE_DEFAULT =CC_month.bank_code
		and FL.project_id COLLATE DATABASE_DEFAULT =CC_month.project_id
		and CC_month.record_status='Active'
		and CC_month.category='Month' 
		and CC_month.value= datename(month,indentdate)

		join cypher_code CC_year
		on
		    FL.bank_code COLLATE DATABASE_DEFAULT =CC_year.bank_code
		and FL.project_id COLLATE DATABASE_DEFAULT =CC_year.project_id
		and CC_year.record_status='Active'
		and CC_year.category='Year' 
		and CC_year.value= datename(YEAR,indentdate)

		join cypher_code CC_amount
		on
		    FL.bank_code COLLATE DATABASE_DEFAULT =CC_amount.bank_code
		and FL.project_id COLLATE DATABASE_DEFAULT =CC_amount.project_id
		and CC_amount.record_status='Active'
		and CC_amount.category='Amount' 
		and CC_amount.value=  FL.total_indent_after_pre_availabiliy_allocation

		)a


		--select * from #final_atm_level_allocated_dataset
		--Select * from #cypher_code
		   
		--***************************************************************************************
      
		IF @is_revised_forecasting = 'Yes'
		BEGIN
			SELECT @indentcounter = indent_counter,
				   @shortcode = indent_short_code
				   FROM distribution_planning_master_V2  
				   where  indent_code = @active_indent_code 
				   AND record_status = 'Active' 
		END	
 
 


			  --------------------------INSERT INTO DATABASE-----------------------------
			  begin transaction

			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_detail Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			DECLARE @ForStatus VARCHAR(100) = '''Active'''+','+'''Review Pending'''+','+'''Approval Pending'''
		--	SELECT @ForStatus
			--DECLARE @ForStatus VARCHAR(15) = 'Active'
			DECLARE @ToStatus VARCHAR(15) = 'History'
			

			/*
			IF EXISTS(
						SELECT 1 
						FROM data_update_log
						WHERE record_status = 'Active'
						AND data_for_type = 'Indent'
						AND @is_revised_forecasting= 'No'
						AND datafor_date_time = @dateT
					)

					BEGIN
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old data update log entries for indent ' 
	
						DECLARE @SetWhere_data_update VARCHAR(MAX) = ' data_for_type = ''' + 'Indent' + ''' and datafor_date_time =  ''' + cast(@dateT as nvarchar(50))+ ''''

						EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_data_update,@out OUTPUT

						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for indent' 

					END	*/

                    /*
                    and 
					(project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
					and 
					(bank_code = case when ( @for_bank_code = 'All') then bank_code else  @for_bank_code   end)
					and 
					(feeder_branch_code = case when ( @for_feeder_branch = 'All') then feeder_branch_code else  @for_feeder_branch   end)
                    */
			IF EXISTS(
						SELECT 1 
						FROM distribution_planning_detail_V2
						WHERE record_status = 'Active'
						AND CAST(indentdate as date) = cast(@dateT as date)
						AND @is_revised_forecasting= 'No'
						AND indent_counter = 1
                        and 
                        (project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
                        and 
						( bank_code IN 
						(SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank_code else  @for_bank_code end ,',')))
                        and 
                        (feeder_branch_code = case when ( @for_feeder_branch = 'All') then feeder_branch_code else  @for_feeder_branch   end)
					)

					BEGIN
							set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in distribution_planning_detail ' 

                            DECLARE @SetWhere_distribution_detail VARCHAR(MAX) = ' indentdate = ''' + cast(@dateT as nvarchar(50))+ '''  
																			and 
																			(project_id = case when ( '''+  @for_project_id  + '''= ''All'') then project_id else ''' + @for_project_id +'''  end)
																			and 
																			(bank_code IN ( SELECT *  FROM splitstring( case when ( '''+ @for_bank_code + '''= ''All'') then bank_code else '''+ @for_bank_code + ''' end ,'','')))
																			and 
																			(feeder_branch_code = case when ('''+ @for_feeder_branch + '''= ''All'') then feeder_branch_code else''' +  @for_feeder_branch +'''  end)'

							EXEC dbo.[uspSetStatus] 'dbo.distribution_planning_detail_V2',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_distribution_detail,@out OUTPUT
						

						 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
						values ('Found old entries in distribution_planning_detail', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in distribution_planning_detail table for old entries for indent ' 
					
					END	
                    
			IF EXISTS(
						SELECT 1 
						FROM distribution_planning_master_V2
						WHERE record_status = 'Active'
						AND @is_revised_forecasting= 'No'
						AND CAST(indentdate as date) = cast(@dateT as date)
						AND indent_counter = 1
                        and 
                        (project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
                        and 
                       ( bank_code IN 
						( SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank_code else  @for_bank_code end ,',')))
                        and
                        (feeder_branch_code = case when ( @for_feeder_branch = 'All') then feeder_branch_code else  @for_feeder_branch   end)
					)


					BEGIN
					
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in distribution_planning_master ' 
                       
						DECLARE @SetWhere_distribution_master VARCHAR(MAX) =  ' indentdate = ''' + cast(@dateT as nvarchar(50))+ '''	and 
																			(project_id = case when ( '''+  @for_project_id  + '''= ''All'') then project_id else ''' + @for_project_id +'''  end)
																			and 
																			(bank_code IN ( SELECT *  FROM splitstring( case when ( '''+ @for_bank_code + '''= ''All'') then bank_code else '''+ @for_bank_code + ''' end ,'','')))
																			and 
																			(feeder_branch_code = case when ('''+ @for_feeder_branch  + '''= ''All'') then feeder_branch_code else''' +  @for_feeder_branch +'''  end)'
							
						EXEC dbo.[uspSetStatus] 'dbo.distribution_planning_master_V2',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_distribution_master,@out OUTPUT

						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in distribution_planning_master table for old entries for indent  ' 
					
					END
		
			IF EXISTS(
						SELECT 1
						FROM dbo.indent_master
						WHERE record_status = 'Active'
						AND CAST(order_date as date) = cast(@dateT as date)
						AND @is_revised_forecasting= 'No'
						AND indent_counter = 1
						and 
                        (project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
                        and 
                       ( bank IN 
						( SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank else  @for_bank_code end ,',')))
                        and 
                        (feeder_branch = case when ( @for_feeder_branch = 'All') then feeder_branch else  @for_feeder_branch   end)
                        
					)
					BEGIN
						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in indent master ' 
						
						DECLARE @SetWhere_indent_master VARCHAR(MAX) = ' order_date = ''' + cast(@dateT as nvarchar(50))+ ''' and 
																			(project_id = case when ( '''+  @for_project_id  + '''= ''All'') then project_id else ''' + @for_project_id +'''  end)
																			and 
																			(bank IN ( SELECT *  FROM splitstring( case when ( '''+ @for_bank_code + '''= ''All'') then bank else '''+ @for_bank_code + ''' end ,'','')))
																			and 
																			(feeder_branch = case when ('''+ @for_feeder_branch  + '''= ''All'') then feeder_branch else''' +  @for_feeder_branch +'''  end)'
							
						EXEC dbo.[uspSetStatus] 'dbo.indent_master',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_indent_master,@out OUTPUT
                        
												
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in indent master table for old entries for indent '
					
					END
					
			IF EXISTS(
						SELECT 1
						FROM dbo.indent_detail id
						JOIN indent_master im
						on im.indent_order_number = id.indent_order_number
						WHERE id.record_status = 'Active'
						AND id.indent_counter = 1
						AND CAST(order_date as date) = cast(@dateT as date)
						AND @is_revised_forecasting= 'No'
						and 
                        (project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
                        and 
                        (bank IN 
						( SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank else  @for_bank_code end ,',')))
                        and 
                        (feeder_branch = case when ( @for_feeder_branch = 'All') then feeder_branch else  @for_feeder_branch   end)
                        
					)

					BEGIN	
						UPDATE 	id
						SET id.record_status = 'History',
						id.deleted_on = @timestamp_date,
						deleted_by = @cur_user,
						deleted_reference_id = @created_reference_id
						FROM indent_detail id
						JOIN indent_master im
						on im.indent_order_number = id.indent_order_number
						WHERE id.record_status = 'Active'
						AND id.indent_counter = 1	
						AND	CAST(order_date as date) = cast(@dateT as date)			
						and 
                        (project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
                        and 
                       ( bank IN 
						( SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank else  @for_bank_code end ,',')))
                        and 
                        (feeder_branch = case when ( @for_feeder_branch = 'All') then feeder_branch else  @for_feeder_branch   end)
					END
			
			IF EXISTS(
						SELECT 1
						FROM calculated_avg_dispense
						WHERE record_status = 'Active'
						AND CAST(datafor_date_time as date) =  cast(@dateT as date)
						and 
                        (project_id = case when ( @for_project_id = 'All') then project_id else  @for_project_id   end)
                        and 
                       ( bank_code IN 
						( SELECT *  FROM splitstring ( case when  (@for_bank_code = 'All') then bank_code else  @for_bank_code end ,',')))
					)

					BEGIN
 
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in calculated_dispense ' 
					
							DECLARE @SetWhere_calculate_dispense VARCHAR(MAX) = ' datafor_date_time = ''' + cast(@dateT as nvarchar(50)) 
																				+ ''' and (project_id = case when ( '''+  @for_project_id  + '''= ''All'') then project_id else ''' + @for_project_id +'''  end)
																					and 
																					(bank_code IN ( SELECT *  FROM splitstring( case when ( '''+ @for_bank_code + '''= ''All'') then bank_code else '''+ @for_bank_code + ''' end ,'','')))'

							EXEC dbo.[uspSetStatus] 'dbo.[calculated_avg_dispense]',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_calculate_dispense,@out OUTPUT
					
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in calculated_dispense table ' 
				
					END	
			--select * from #dist_with_cra
			--select * from #final_feeder_level_allocated_dataset where is_currency_chest = 'Yes'
			  --select SUM(final_total_loading_amount_500),
					--SUM(final_total_loading_amount_500_currency_chest)
					--,SUm(final_total_loading_amount)
					--, SUM(final_total_loading_amount_currency_chest)
					--from #final_atm_level_allocated_dataset
			  

			  --select * from #final_feeder_level_allocated_dataset 
			  -- where is_currency_chest = 'Yes'
			   

  INSERT INTO  [distribution_planning_detail_V2]
				(
				[indent_code]
				,[atm_id]
				,[site_code]
				,[sol_id]
				,[bank_code]
				,[project_id]
				,[indentdate]
				,[bank_cash_limit]
				,[insurance_limit]
				,[feeder_branch_code]
				,[cra]
				,[base_limit]
				,[dispenseformula]
				,[decidelimitdays]
				,[loadinglimitdays]
				,[avgdispense]
				,[loading_amount]
				,[morning_balance_50]
				,[morning_balance_100]
				,[morning_balance_200]
				,[morning_balance_500]
				,[morning_balance_2000]
				,[total_morning_balance]
				,[cassette_50_brand_capacity]
				,[cassette_100_brand_capacity]
				,[cassette_200_brand_capacity]
				,[cassette_500_brand_capacity]
				,[cassette_2000_brand_capacity]
				,[cassette_50_count]
				,[cassette_100_count]
				,[cassette_200_count]
				,[cassette_500_count]
				,[cassette_2000_count]
				,[limit_amount]
				,[total_capacity_amount_50]
				,[total_capacity_amount_100]
				,[total_capacity_amount_200]
				,[total_capacity_amount_500]
				,[total_capacity_amount_2000]
				,[total_cassette_capacity]
				,[avgdecidelimit]
				,[DecideLimit]
				,[threshold_limit]
				,[loadinglimit]
				,[applied_vaulting_percentage]
				,[ignore_code]
				,[ignore_description]
				,[dist_Purpose]
				,[defaultamt]
				,[default_flag]
				,[curbal_div_avgDisp]
				,[loadingGap_cur_bal_avg_disp]
				,[CashOut]
				,[remaining_balance_50]
				,[remaining_balance_100]
				,[remaining_balance_200]
				,[remaining_balance_500]
				,[remaining_balance_2000]
				,[cassette_capacity_percentage_50]
				,[cassette_capacity_percentage_100]
				,[cassette_capacity_percentage_200]
				,[cassette_capacity_percentage_500]
				,[cassette_capacity_percentage_2000]
				,max_loading_capacity_amount_100  
				,max_loading_capacity_amount_200	 
				,max_loading_capacity_amount_500	 
				,max_loading_capacity_amount_2000 
				--,[rounded_amount_50]
				,[rounded_amount_100]
				,[rounded_amount_200]
				,[rounded_amount_500]
				,[rounded_amount_2000]
				,[total_expected_balanceT1]
				,[expected_balanceT1_50]
				,[expected_balanceT1_100]
				,[expected_balanceT1_200]
				,[expected_balanceT1_500]
				,[expected_balanceT1_2000]
				,[total_vault_amount]
				,[vaultingamount_50]
				,[vaultingamount_100]
				,[vaultingamount_200]
				,[vaultingamount_500]
				,[vaultingamount_2000]
				,[rounded_vaultingamount_50]
				,[rounded_vaultingamount_100]
				,[rounded_vaultingamount_200]
				,[rounded_vaultingamount_500]
				,[rounded_vaultingamount_2000]
				,[total_rounded_vault_amt]
				, total_opening_remaining_available_amount	
				, opening_remaining_available_amount_100	
				, opening_remaining_available_amount_200	
				, opening_remaining_available_amount_500	
				, opening_remaining_available_amount_2000	
				, cassette_100_count_original				
				, cassette_200_count_original				
				, cassette_500_count_original				
				, cassette_2000_count_original				
				, denomination_100_max_capacity_percentage	
				, denomination_200_max_capacity_percentage	
				, denomination_500_max_capacity_percentage	
				, denomination_2000_max_capacity_percentage	
				, max_amt_allowed_100						
				, max_amt_allowed_200						
				, max_amt_allowed_500						
				, max_amt_allowed_2000						
				, denomination_wise_round_off_100			
				, denomination_wise_round_off_200			
				, denomination_wise_round_off_500			
				, denomination_wise_round_off_2000							
				, rounded_tentative_loading_100				
				, rounded_tentative_loading_200				
				, rounded_tentative_loading_500				
				, rounded_tentative_loading_2000			
				, deno_100_priority							
				, deno_200_priority							
				, deno_500_priority							
				, deno_2000_priority							
				, is_deno_wise_cash_available						
				, priority_1_is_denomination_100					
				, priority_1_is_denomination_200					
				, priority_1_is_denomination_500					
				, priority_1_is_denomination_2000					
				, priority_1_is_remaining_amount_available_100		
				, priority_1_is_remaining_amount_available_200		
				, priority_1_is_remaining_amount_available_500		
				, priority_1_is_remaining_amount_available_2000		
				, priority_1_is_remaining_capacity_available_100	
				, priority_1_is_remaining_capacity_available_200	
				, priority_1_is_remaining_capacity_available_500	
				, priority_1_is_remaining_capacity_available_2000	
				, priority_1_loading_amount_100						
				, priority_1_loading_amount_200						
				, priority_1_loading_amount_500						
				, priority_1_loading_amount_2000					
				, priority_2_is_denomination_100					
				, priority_2_is_denomination_200					
				, priority_2_is_denomination_500					
				, priority_2_is_denomination_2000					
				, priority_2_is_remaining_amount_available_100		
				, priority_2_is_remaining_amount_available_200		
				, priority_2_is_remaining_amount_available_500		
				, priority_2_is_remaining_amount_available_2000		
				, priority_2_is_remaining_capacity_available_100	
				, priority_2_is_remaining_capacity_available_200	
				, priority_2_is_remaining_capacity_available_500	
				, priority_2_is_remaining_capacity_available_2000	
				, priority_2_loading_amount_100						
				, priority_2_loading_amount_200						
				, priority_2_loading_amount_500						
				, priority_2_loading_amount_2000					
				, priority_3_is_denomination_100					
				, priority_3_is_denomination_200					
				, priority_3_is_denomination_500					
				, priority_3_is_denomination_2000					
				, priority_3_is_remaining_amount_available_100		
				, priority_3_is_remaining_amount_available_200		
				, priority_3_is_remaining_amount_available_500		
				, priority_3_is_remaining_amount_available_2000		
				, priority_3_is_remaining_capacity_available_100	
				, priority_3_is_remaining_capacity_available_200	
				, priority_3_is_remaining_capacity_available_500	
				, priority_3_is_remaining_capacity_available_2000	
				, priority_3_loading_amount_100						
				, priority_3_loading_amount_200						
				, priority_3_loading_amount_500						
				, priority_3_loading_amount_2000					
				, priority_4_is_denomination_100					
				, priority_4_is_denomination_200					
				, priority_4_is_denomination_500					
				, priority_4_is_denomination_2000					
				, priority_4_is_remaining_amount_available_100		
				, priority_4_is_remaining_amount_available_200		
				, priority_4_is_remaining_amount_available_500		
				, priority_4_is_remaining_amount_available_2000		
				, priority_4_is_remaining_capacity_available_100	
				, priority_4_is_remaining_capacity_available_200	
				, priority_4_is_remaining_capacity_available_500	
				, priority_4_is_remaining_capacity_available_2000	
				, priority_4_loading_amount_100						
				, priority_4_loading_amount_200						
				, priority_4_loading_amount_500						
				, priority_4_loading_amount_2000					
				, pre_avail_loading_amount_100								
				, pre_avail_loading_amount_200								
				, pre_avail_loading_amount_500								
				, pre_avail_loading_amount_2000								
				, pre_avail_total_loading_amount								
				, remaining_capacity_amount_100						
				, remaining_capacity_amount_200						
				, remaining_capacity_amount_500						
				, remaining_capacity_amount_2000					
				, closing_remaining_available_amount_100			
				, closing_remaining_available_amount_200			
				, closing_remaining_available_amount_500			
				, closing_remaining_available_amount_2000			
				, total_closing_remaining_available_amount			
				, total_forecasted_remaining_amt					
				, indent_counter 									
				, record_status
				,indent_type
				,created_on
				,created_by
				,created_reference_id
				,final_total_loading_amount                         
				,final_total_loading_amount_100						
				,final_total_loading_amount_200						
				,final_total_loading_amount_500						
				,final_total_loading_amount_2000					
				,total_atm_vaulting_amount_after_pre_availability	
				,atm_vaulting_amount_after_pre_availability_100		
				,atm_vaulting_amount_after_pre_availability_200		
				,atm_vaulting_amount_after_pre_availability_500		
				,atm_vaulting_amount_after_pre_availability_2000	
				,atm_priority									
				,is_cash_pre_available
				,atm_band
				,reason_for_priority
				,is_zero_dispense
				,feasibility_frequency
				,is_feasible_today	
				,bank_decide_limit_days
				,bank_loading_limit_days
				,feeder_decide_limit_days
				,feeder_loading_limit_days
				,feasibility_decide_limit_days
				,feasibility_loading_limit_days		
				,previous_indent_code
									
				)
			  SELECT	indentcode
					,	dist.atm_id
					,   dist.site_code
					,	dist.sol_id
					,	dist.bank_code
					,	dist.project_id
					,	dist.indentdate
					,	dist.bank_cash_limit
					,	dist.insurance_limit
					,	dist.feeder_branch_code
					,	dist.cra
					,   dist.base_limit
					,	dist.dispenseformula
					,	dist.decidelimitdays
					,	dist.loadinglimitdays
					,	dist.avgdispense
					,	dist.loading_amount
					,	dist.morning_balance_50
					,	dist.morning_balance_100
					,	dist.morning_balance_200
					,	dist.morning_balance_500
					,	dist.morning_balance_2000
					,	dist.total_morning_balance
					,	cassette_50_brand_capacity
					,	COALESCE(temp.cassette_100_brand_capacity,dist.cassette_100_brand_capacity) as cassette_100_brand_capacity
					,	COALESCE(temp.cassette_200_brand_capacity,dist.cassette_200_brand_capacity) as cassette_200_brand_capacity
					,	COALESCE(temp.cassette_500_brand_capacity,dist.cassette_500_brand_capacity) as cassette_500_brand_capacity
					,	COALESCE(temp.cassette_2000_brand_capacity,dist.cassette_2000_brand_capacity) as cassette_2000_brand_capacity
					,	dist.cassette_50_count
					,	dist.cassette_100_count
					,	dist.cassette_200_count
					,	dist.cassette_500_count
					,	dist.cassette_2000_count
					,	dist.limit_amount
					,	total_capacity_amount_50
					,	COALESCE(temp.total_capacity_amount_100,dist.total_capacity_amount_100) as total_capacity_amount_100
					,	COALESCE(temp.total_capacity_amount_200,dist.total_capacity_amount_200) as total_capacity_amount_200
					,	COALESCE(temp.total_capacity_amount_500,dist.total_capacity_amount_500) as total_capacity_amount_500
					,	COALESCE(temp.total_capacity_amount_2000,dist.total_capacity_amount_2000) as total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					--,	cassette_50_capacity_amount
					--,	cassette_100_capacity_amount
					--,	cassette_200_capacity_amount
					--,	cassette_500_capacity_amount
					--,	cassette_2000_capacity_amount
					--,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	max_loading_capacity_amount_100
					,	max_loading_capacity_amount_200
					,	max_loading_capacity_amount_500
					,	max_loading_capacity_amount_2000
					---,	tentative_loading_amount_2000
					--,   forecasted_amt_50
					,   forecasted_amt_100
					,   forecasted_amt_200
					,   forecasted_amt_500
					,   forecasted_amt_2000
					,	total_expected_balanceT1
					,	expected_balanceT1_50
					,	expected_balanceT1_100
					,	expected_balanceT1_200
					,	expected_balanceT1_500
					,	expected_balanceT1_2000
					,	total_vault_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	rounded_vaultingamount_50
					,	rounded_vaultingamount_100
					,	rounded_vaultingamount_200
					,	rounded_vaultingamount_500
					,	rounded_vaultingamount_2000
					,	total_rounded_vault_amt
					, atm.total_opening_remaining_available_amount	
					, opening_remaining_available_amount_100	
					, opening_remaining_available_amount_200	
					, opening_remaining_available_amount_500	
					, opening_remaining_available_amount_2000	
					, COALESCE(temp.cassette_100_count_original,dist.cassette_100_count)  as	 cassette_100_count_original
					, COALESCE(temp.cassette_200_count_original,dist.cassette_200_count)  as	 cassette_200_count_original			
					, COALESCE(temp.cassette_500_count_original,dist.cassette_500_count)  as	 cassette_500_count_original			
					, COALESCE(temp.cassette_2000_count_original,dist.cassette_2000_count)  as	 cassette_2000_count_original				
					, COALESCE(temp.denomination_100_max_capacity_percentage,dist.denomination_100_max_capacity_percentage) as denomination_100_max_capacity_percentage
					, COALESCE(temp.denomination_200_max_capacity_percentage ,dist.denomination_200_max_capacity_percentage) as denomination_200_max_capacity_percentage
					, COALESCE(temp.denomination_500_max_capacity_percentage ,dist.denomination_500_max_capacity_percentage) as denomination_500_max_capacity_percentage
					, COALESCE(temp.denomination_2000_max_capacity_percentage,dist.denomination_2000_max_capacity_percentage) as denomination_2000_max_capacity_percentage
					, max_amt_allowed_100						
					, max_amt_allowed_200						
					, max_amt_allowed_500						
					, max_amt_allowed_2000						
					, COALESCE(temp.denomination_wise_round_off_100,	 sa.denomination_wise_round_off_100)			as denomination_wise_round_off_100
					, COALESCE(temp.denomination_wise_round_off_200	,	 sa.denomination_wise_round_off_200)		as denomination_wise_round_off_200
					, COALESCE(temp.denomination_wise_round_off_500	,	 sa.denomination_wise_round_off_500)		as denomination_wise_round_off_500
					, COALESCE(temp.denomination_wise_round_off_2000	,sa.denomination_wise_round_off_2000)	as denomination_wise_round_off_2000	
					--, COALESCE(temp.tentative_loading_100	, dist.	tentative_loading_amount_100)		 as tentative_loading_100		
					--, COALESCE(temp.tentative_loading_200	, dist.	tentative_loading_amount_200)		 as tentative_loading_200							
					--, COALESCE(temp.tentative_loading_500	, dist.	tentative_loading_amount_500)		 as tentative_loading_500							
					--, COALESCE(temp.tentative_loading_2000	, dist.	tentative_loading_amount_2000)		 as tentative_loading_2000						
					, rounded_tentative_loading_100				
					, rounded_tentative_loading_200				
					, rounded_tentative_loading_500				
					, rounded_tentative_loading_2000			
					, COALESCE(temp.deno_100_priority  , dist.deno_100_priority) as deno_100_priority								
					, COALESCE(temp.deno_200_priority  , dist.deno_200_priority) as deno_200_priority 										
					, COALESCE(temp.deno_500_priority  , dist.deno_500_priority) as deno_500_priority										
					, COALESCE(temp.deno_2000_priority , dist.deno_2000_priority) as deno_2000_priority									
					, is_deno_wise_cash_available							
					, priority_1_is_denomination_100						
					, priority_1_is_denomination_200						
					, priority_1_is_denomination_500						
					, priority_1_is_denomination_2000						
					, priority_1_is_remaining_amount_available_100			
					, priority_1_is_remaining_amount_available_200			
					, priority_1_is_remaining_amount_available_500			
					, priority_1_is_remaining_amount_available_2000			
					, priority_1_is_remaining_capacity_available_100		
					, priority_1_is_remaining_capacity_available_200		
					, priority_1_is_remaining_capacity_available_500		
					, priority_1_is_remaining_capacity_available_2000		
					, priority_1_loading_amount_100							
					, priority_1_loading_amount_200							
					, priority_1_loading_amount_500							
					, priority_1_loading_amount_2000						
					, priority_2_is_denomination_100						
					, priority_2_is_denomination_200						
					, priority_2_is_denomination_500						
					, priority_2_is_denomination_2000						
					, priority_2_is_remaining_amount_available_100			
					, priority_2_is_remaining_amount_available_200			
					, priority_2_is_remaining_amount_available_500			
					, priority_2_is_remaining_amount_available_2000			
					, priority_2_is_remaining_capacity_available_100		
					, priority_2_is_remaining_capacity_available_200		
					, priority_2_is_remaining_capacity_available_500		
					, priority_2_is_remaining_capacity_available_2000		
					, priority_2_loading_amount_100							
					, priority_2_loading_amount_200							
					, priority_2_loading_amount_500							
					, priority_2_loading_amount_2000						
					, priority_3_is_denomination_100						
					, priority_3_is_denomination_200						
					, priority_3_is_denomination_500						
					, priority_3_is_denomination_2000						
					, priority_3_is_remaining_amount_available_100			
					, priority_3_is_remaining_amount_available_200			
					, priority_3_is_remaining_amount_available_500			
					, priority_3_is_remaining_amount_available_2000			
					, priority_3_is_remaining_capacity_available_100		
					, priority_3_is_remaining_capacity_available_200		
					, priority_3_is_remaining_capacity_available_500		
					, priority_3_is_remaining_capacity_available_2000		
					, priority_3_loading_amount_100							
					, priority_3_loading_amount_200							
					, priority_3_loading_amount_500							
					, priority_3_loading_amount_2000						
					, priority_4_is_denomination_100						
					, priority_4_is_denomination_200						
					, priority_4_is_denomination_500						
					, priority_4_is_denomination_2000						
					, priority_4_is_remaining_amount_available_100			
					, priority_4_is_remaining_amount_available_200			
					, priority_4_is_remaining_amount_available_500			
					, priority_4_is_remaining_amount_available_2000			
					, priority_4_is_remaining_capacity_available_100		
					, priority_4_is_remaining_capacity_available_200		
					, priority_4_is_remaining_capacity_available_500		
					, priority_4_is_remaining_capacity_available_2000		
					, priority_4_loading_amount_100							
					, priority_4_loading_amount_200							
					, priority_4_loading_amount_500							
					, priority_4_loading_amount_2000						
					, COALESCE(loading_amount_100   ,  forecasted_amt_100,0) as pre_avail_loading_amount_100				
					, COALESCE(loading_amount_200	,  forecasted_amt_200,0) as pre_avail_loading_amount_200								
					, COALESCE(loading_amount_500	,  forecasted_amt_500,0) as pre_avail_loading_amount_500								
					, COALESCE(loading_amount_2000	,  forecasted_amt_2000,0) as pre_avail_loading_amount_2000								
					, COALESCE(loading_amount_100   ,  forecasted_amt_100,0) + COALESCE(loading_amount_200	, forecasted_amt_200,0) + COALESCE(loading_amount_500	, forecasted_amt_500,0) + COALESCE(loading_amount_2000	, forecasted_amt_2000,0) as pre_avail_total_loading_amount					
					, atm.remaining_capacity_amount_100							
					, atm.remaining_capacity_amount_200							
					, atm.remaining_capacity_amount_500							
					, atm.remaining_capacity_amount_2000					
					, closing_remaining_available_amount_100				
					, closing_remaining_available_amount_200				
					, closing_remaining_available_amount_500				
					, closing_remaining_available_amount_2000				
					, total_closing_remaining_available_amount				
					, total_forecasted_remaining_amt						
					--, 1
					,CASE WHEN @is_revised_forecasting = 'Yes'
						THEN @indentcounter + 1 
						ELSE
						1
						END										 
					,CASE WHEN @is_revised_forecasting = 'Yes'
						THEN 'Review Pending' 
						ELSE 'Active'
						END as record_status
					,CASE WHEN @is_revised_forecasting = 'Yes'
						THEN 'Revised Indent'	
						ELSE 'Indent'
						END as indent_type
					,	@timestamp_date
					,	@cur_user
					,	@created_reference_id
					,	
						CASE WHEN 
							atm.is_currency_chest = 'Yes'
						Then 
							final_total_loading_amount_currency_chest
						ELSE         
							final_total_loading_amount
						END      
					,	
						CASE WHEN 
							atm.is_currency_chest = 'Yes'
						Then 
							final_total_loading_amount_100_currency_chest
						ELSE         
							final_total_loading_amount_100
						END  
					,	
						CASE WHEN 
							atm.is_currency_chest = 'Yes'
						Then 
							final_total_loading_amount_200_currency_chest
						ELSE         
							final_total_loading_amount_200
						END   
					,	
						CASE WHEN 
							atm.is_currency_chest = 'Yes'
						Then 
							final_total_loading_amount_500_currency_chest
						ELSE         
							final_total_loading_amount_500
						END         
					,	
						CASE WHEN 
							atm.is_currency_chest = 'Yes'
						Then 
							final_total_loading_amount_2000_currency_chest
						ELSE         
							final_total_loading_amount_2000
						END           
					--,	final_total_loading_amount_100						
					--,	final_total_loading_amount_200						
					--,	final_total_loading_amount_500						
					--,	final_total_loading_amount_2000					
					,CASE WHEN	
						atm.is_currency_chest = 'Yes'
						THEN
							atm_vaulting_amount_after_pre_availability_100 + 
							atm_vaulting_amount_after_pre_availability_200 + 
							atm_vaulting_amount_after_pre_availability_500_cc + 
							atm_vaulting_amount_after_pre_availability_2000
						ELSE
							atm_vaulting_amount_after_pre_availability_100+
							atm_vaulting_amount_after_pre_availability_200+
							atm_vaulting_amount_after_pre_availability_500+
							atm_vaulting_amount_after_pre_availability_2000
						END
					,atm_vaulting_amount_after_pre_availability_100		
					,atm_vaulting_amount_after_pre_availability_200		
					,CASE WHEN	
						atm.is_currency_chest = 'Yes'
						Then 
						atm_vaulting_amount_after_pre_availability_500_cc
						ELSE         
						atm_vaulting_amount_after_pre_availability_500
						END  		
					,atm_vaulting_amount_after_pre_availability_2000	
					,dist.atm_priority										
					,is_cash_pre_available
					,atm_band
					,reason_for_priority
					,is_zero_dispense	
					,feasibility_frequency
					,is_feasible_today		
					,bank_decide_limit_days
					,bank_loading_limit_days
					,feeder_decide_limit_days
					,feeder_loading_limit_days
					,feasibility_decide_limit_days
					,feasibility_loading_limit_days	
					,CASE WHEN @is_revised_forecasting = 'Yes'
							THEN @active_indent_code
						END as previous_indent_code
						
					FROM #dist_with_cra dist
					CROSS JOIN system_settings sa
					LEFT JOIN #final_atm_level_allocated_dataset atm
					on atm.atm_id  = dist.atm_id
					AND atm.bank_code = dist.bank_code
					and atm.project_id = dist.project_id
					LEFT JOIN #temp_cash_pre_availability temp
					on		temp.atmid COLLATE DATABASE_DEFAULT = dist.atm_id COLLATE DATABASE_DEFAULT
					AND		temp.project_id  COLLATE DATABASE_DEFAULT= dist.project_id COLLATE DATABASE_DEFAULT
					AND		temp.bank_code COLLATE DATABASE_DEFAULT = dist.bank_code COLLATE DATABASE_DEFAULT

					
					--select * from [distribution_planning_detail_18Feb]
					-- truncate table [distribution_planning_detail_18Feb]
					--delete from #dist_with_cra 
					--where cast(#dist_with_cra.indentdate as date) >(Select max(cast(indentdate as date)) 
					--												from distribution_planning_detail)

				DECLARE @row INT
				SELECT @row	= @@ROWCOUNT

			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_detail Completed'
 
		    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_detail Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				INSERT INTO [distribution_planning_master_V2]
					( 
					[indent_code]
					, indent_short_code
				   ,[indentdate]
				   ,[project_id]
				   ,[bank_code]
				   ,[feeder_branch_code]
				   ,[cra]
				   ,cypher_code_sol_id
				   ,cypher_code_date         -- added new column
				   ,cypher_code_day          -- added new column
				   ,cypher_code_month        -- added new column
				   ,cypher_code_year         -- added new column
				   ,cypher_code_amount 		 -- added new column	 
				   ,cypher_code              -- added new column
				   ,[max_loading_amount]
				   --,[max_loading_amount_50]
				   ,[max_loading_amount_100]
				   ,[max_loading_amount_200]
				   ,[max_loading_amount_500]
				   ,[max_loading_amount_2000]
				   ,[min_loading_amount]
				   ,[forecast_loading_amount]
				   --,[forecast_loading_amount_50]
				   ,[forecast_loading_amount_100]
				   ,[forecast_loading_amount_200]
				   ,[forecast_loading_amount_500]
				   ,[forecast_loading_amount_2000]
				   ,[total_cassette_capacity]
				   ,[vaultingamount_50]
				   ,[vaultingamount_100]
				   ,[vaultingamount_200]
				   ,[vaultingamount_500]
				   ,[vaultingamount_2000]
				   ,[total_rounded_vault_amt]
				   ,[vault_balance_100]
				   ,[vault_balance_200]
				   ,[vault_balance_500]
				   ,[vault_balance_2000]
				   ,[total_vault_balance]
				   ,is_deno_wise_cash_available		
				   ,Total_Amount_Available
				   ,available_100_amount				
				   ,available_200_amount				
				   ,available_500_amount				
				   ,available_2000_amount			
				  -- ,[indent_50]
				   ,[indent_100]
				   ,[indent_200]
				   ,[indent_500]
				   ,[indent_2000]
				   ,[record_status]
				   ,indent_type
				   ,[created_on]
				   ,[created_by]
				   ,[created_reference_id]
				   ,[indent_counter]
				   ,[total_vaulting_after_pre_availabiliy_allocation]   
				   ,[vaulting_after_pre_availabiliy_allocation_100]	    
				   ,[vaulting_after_pre_availabiliy_allocation_200]	    
				   ,[vaulting_after_pre_availabiliy_allocation_500]	    
				   ,[vaulting_after_pre_availabiliy_allocation_2000]	
				   ,[final_total_loading_amount]						
				   ,[total_indent_after_pre_availabiliy_allocation]		
				   ,[indent_after_pre_availabiliy_allocation_100]		
				   ,[indent_after_pre_availabiliy_allocation_200]		
				   ,[indent_after_pre_availabiliy_allocation_500]		
				   ,[indent_after_pre_availabiliy_allocation_2000]
				   ,previous_indent_code
				   ,total_closing_vault_balance 
				   ,closing_vault_balance_50
				   ,closing_vault_balance_100
				   ,closing_vault_balance_200
				   ,closing_vault_balance_500
				   ,closing_vault_balance_2000	
				   ,is_currency_chest				   
				   )
				SELECT 	
					 indentcode
					, CASE WHEN 
								@is_revised_forecasting = 'No' THEN indent_short_code
							ELSE
								'R' +  CAST ((@indentcounter + 1) AS nvarchar(15)) + '-'+  RIGHT(@shortcode,6)
							END
					as indent_short_code
					,feeder.indentdate
					,feeder.project_id
					,feeder.bank_code
					,feeder.feeder_branch_code
					,cra
					,sol_id_cypher_code
					,cypher_code_date
					,cypher_code_day
					,cypher_code_month
					,cypher_code_year
					,cypher_code_amount		
					,final_cypher_Code
					,max_loading_amount
					--,max_loading_amount_50
					,max_loading_amount_100
					,max_loading_amount_200
					,max_loading_amount_500
					,max_loading_amount_2000
					,min_loading_amount
					--,forecast_loading_amount
					,CASE 
						WHEN feeder.is_currency_chest = 'No'
							THEN feeder.forecast_loading_amount
						WHEN feeder.is_currency_chest = 'Yes'
							THEN final_total_atm_loading_after_pre_availability_allocation_amount_100 + 
								 final_total_atm_loading_after_pre_availability_allocation_amount_200 +
								 final_total_atm_loading_after_pre_availability_allocation_amount_500 +
								 final_total_atm_loading_after_pre_availability_allocation_amount_2000
						ELSE 
							feeder.forecast_loading_amount
					END
					--,forecast_loading_amount_50
					,CASE 
						WHEN feeder.is_currency_chest = 'No'
							THEN feeder.forecast_loading_amount_100
						WHEN feeder.is_currency_chest = 'Yes'
							THEN final_total_atm_loading_after_pre_availability_allocation_amount_100
						ELSE 
							feeder.forecast_loading_amount_100
					END
					,CASE 
						WHEN feeder.is_currency_chest = 'No'
							THEN feeder.forecast_loading_amount_200
						WHEN feeder.is_currency_chest = 'Yes'
							THEN  final_total_atm_loading_after_pre_availability_allocation_amount_200

						ELSE 
							feeder.forecast_loading_amount_200
					END
					,CASE 
						WHEN feeder.is_currency_chest = 'No'
							THEN feeder.forecast_loading_amount_500
						WHEN feeder.is_currency_chest = 'Yes'
							THEN  final_total_atm_loading_after_pre_availability_allocation_amount_500

						ELSE 
							feeder.forecast_loading_amount_500
					END
					,CASE 
						WHEN feeder.is_currency_chest = 'No'
							THEN feeder.forecast_loading_amount_2000
						WHEN feeder.is_currency_chest = 'Yes'
							THEN final_total_atm_loading_after_pre_availability_allocation_amount_2000
						ELSE 
							feeder.forecast_loading_amount_2000
					END
					,total_cassette_capacity
					--,cassette_50_capacity_amount
					--,cassette_100_capacity_amount
					--,cassette_200_capacity_amount
					--,cassette_500_capacity_amount
					--,cassette_2000_capacity_amount
					,vaultingamount_50
					,vaultingamount_100
					,vaultingamount_200
					,vaultingamount_500
					,vaultingamount_2000
					,total_rounded_vault_amt
					,vault_balance_100
					,vault_balance_200
					,vault_balance_500
					,vault_balance_2000
					,total_vault_balance
					,feeder.is_deno_wise_cash_available		
					,total_remaining_available_amount
					,remaining_avail_100				
					,remaining_avail_200				
					,remaining_avail_500				
					,remaining_avail_2000			
					,feeder.[indent_after_pre_availabiliy_allocation_100]
					,feeder.[indent_after_pre_availabiliy_allocation_200]	
					,feeder.[indent_after_pre_availabiliy_allocation_500]	
					,feeder.[indent_after_pre_availabiliy_allocation_2000]	
					,	CASE WHEN @is_revised_forecasting = 'Yes'
							THEN 'Review Pending' 
							ELSE 'Active'
						END as record_status
					,	CASE WHEN @is_revised_forecasting = 'Yes'
						THEN 'Revised Indent'	
						ELSE 'Indent'
						END as indent_type
					,	@timestamp_date
					,	@cur_user
					,   @created_reference_id
					--, 1
					,CASE WHEN @is_revised_forecasting = 'Yes'
						THEN @indentcounter + 1 
						ELSE
						1
						END	
					,feeder.[total_vaulting_after_pre_availabiliy_allocation]
					,feeder.[vaulting_after_pre_availabiliy_allocation_100]	
					,feeder.[vaulting_after_pre_availabiliy_allocation_200]	
					,feeder.[vaulting_after_pre_availabiliy_allocation_500]	
					,feeder.[vaulting_after_pre_availabiliy_allocation_2000]
					,feeder.final_total_atm_loading_after_pre_availability_allocation_amount					
					,feeder.[total_indent_after_pre_availabiliy_allocation]	
					,feeder.[indent_after_pre_availabiliy_allocation_100]	
					,feeder.[indent_after_pre_availabiliy_allocation_200]	
					, CASE WHEN
						feeder.is_currency_chest	= 'Yes'
						THEN
							ISNULL(final_total_atm_loading_after_pre_availability_allocation_amount_500,0) + ISNULL(currency_chest.forecasted_vaulting_amount_500,0)
						ELSE
							indent_after_pre_availabiliy_allocation_500
						END
					,feeder.[indent_after_pre_availabiliy_allocation_2000]	
					,CASE WHEN @is_revised_forecasting = 'Yes'
							THEN @active_indent_code
						END as previous_indent_code
					,feeder.total_vault_closing_balance_after_pre_availability
					,0
					,feeder.vault_closing_balance_after_pre_availability_100
					,feeder.vault_closing_balance_after_pre_availability_200
					,feeder.vault_closing_balance_after_pre_availability_500
					,feeder.vault_closing_balance_after_pre_availability_2000
					,feeder.is_currency_chest
				FROM #final_feeder_level_allocated_dataset feeder
				LEFT JOIN #currency_chest_feeder_branch currency_chest
				ON feeder.bank_code  COLLATE DATABASE_DEFAULT = currency_chest.bank_code
				AND feeder.feeder_branch_code COLLATE DATABASE_DEFAULT= currency_chest.feeder_branch_code COLLATE DATABASE_DEFAULT
				AND feeder.project_id   COLLATE DATABASE_DEFAULT      = currency_chest.project_id COLLATE DATABASE_DEFAULT
				LEFT JOIN #temp_feeder_forecast temp
				on feeder.bank_code COLLATE DATABASE_DEFAULT= temp.bank_code COLLATE DATABASE_DEFAULT
				AND feeder.feeder_branch_code COLLATE DATABASE_DEFAULT= temp.feeder_branch_code COLLATE DATABASE_DEFAULT
				AND feeder.project_id COLLATE DATABASE_DEFAULT = temp.project_id COLLATE DATABASE_DEFAULT

				left join #cypher_code CC          -- Join to get cypher code collums 
				on  feeder.bank_code=CC.bank_code
				and feeder.project_id=CC.project_id
				and  feeder.feeder_branch_code=CC.feeder_branch_code


				--SELECT SUM(final_total_loading_amount_500)
				--		,SUM(final_total_loading_amount_500_currency_chest) FROM #final_atm_level_allocated_dataset
            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
	


			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_master Completed'
--------- ----------------------------------feeder level indent amount generation---------------------------------------------------

	--SELECT * FROM #final_feeder_level_allocated_dataset where feeder_branch_code = 'KOLHAPUR'

	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
	INSERT INTO indent_master
		(  [indent_order_number]
		,indent_short_code
	      ,[order_date]
	      ,[collection_date]
	      ,[replenishment_date]
	      ,[cypher_code]
	      ,[bank]
	      ,[feeder_branch]
	      ,[cra]
	      --,[total_atm_loading_amount_50]
	      ,[total_atm_loading_amount_100]
	      ,[total_atm_loading_amount_200]
	      ,[total_atm_loading_amount_500]
	      ,[total_atm_loading_amount_2000]
	      ,[total_atm_loading_amount]
	      ,[cra_opening_vault_balance_50]
	      ,[cra_opening_vault_balance_100]
	      ,[cra_opening_vault_balance_200]
	      ,[cra_opening_vault_balance_500]
	      ,[cra_opening_vault_balance_2000]
	      ,[cra_opening_vault_balance]
	     -- ,[total_bank_withdrawal_amount_50]
	      ,[total_bank_withdrawal_amount_100]
	      ,[total_bank_withdrawal_amount_200]
	      ,[total_bank_withdrawal_amount_500]
	      ,[total_bank_withdrawal_amount_2000]
	      ,[total_bank_withdrawal_amount]
	      ,[authorized_by_1]
	      ,[authorized_by_2]
	      ,[notes]
	      ,[amount_in_words]
	      ,[indent_type]
	      ,[project_id]
	      ,[record_status]
	      ,[created_on]
		  ,[indent_counter]
		  ,created_reference_id
		  ,created_by
		  ,email_indent_mail_master_id
		  ,signature_authorized_by_1_auth_signatories_signatures_id
		  ,signature_authorized_by_2_auth_signatories_signatures_id
		  ,previous_indent_code
		  ,total_closing_vault_balance 
		  ,closing_vault_balance_50
		  ,closing_vault_balance_100
		  ,closing_vault_balance_200
		  ,closing_vault_balance_500
		  ,closing_vault_balance_2000	
		  )
		  SELECT  indentcode
				 , CASE WHEN 
								@is_revised_forecasting = 'No' THEN indent_short_code
							ELSE
								'R' +  CAST ((@indentcounter + 1	 ) AS nvarchar(15)) + '-'+  RIGHT(@shortcode,6)
							END
					as indent_short_code
				 ,feeder.indentdate AS [order_date]
				 ,feeder.indentdate AS [collection_date]
				 ,feeder.indentdate AS [replenishment_date]
				 ,final_cypher_Code as [cypher_code] 
				 ,feeder.[bank_code]
				 ,feeder.[feeder_branch_code]
				 ,feeder.[cra]
				 --,indent_50
				 ,FEEDER.final_total_atm_loading_after_pre_availability_allocation_amount_100
				 ,FEEDER.final_total_atm_loading_after_pre_availability_allocation_amount_200
				 ,FEEDER.final_total_atm_loading_after_pre_availability_allocation_amount_500
				 ,FEEDER.final_total_atm_loading_after_pre_availability_allocation_amount_2000
				 ,FEEDER.final_total_atm_loading_after_pre_availability_allocation_amount
				 , 0 AS [cra_opening_vault_balance_50]
				 , vault_balance_100   AS [cra_opening_vault_balance_100]
				 , vault_balance_200   AS [cra_opening_vault_balance_200]
				 , vault_balance_500   AS [cra_opening_vault_balance_500]
				 , vault_balance_2000  AS [cra_opening_vault_balance_2000]
				 , total_vault_balance AS [cra_opening_vault_balance]
				 --, indent_50 AS [total_bank_withdrawal_amount_50]
				 , CASE WHEN 
					withdrawal_type = 'Vault' and Total_Amount_Available > 0
					THEN
						0 
					ELSE
						feeder.indent_after_pre_availabiliy_allocation_100 
					END AS [total_bank_withdrawal_amount_100]
				 --, feeder.indent_after_pre_availabiliy_allocation_200 AS [total_bank_withdrawal_amount_200]
				 --, feeder.indent_after_pre_availabiliy_allocation_500 [total_bank_withdrawal_amount_500]
				 --, feeder.indent_after_pre_availabiliy_allocation_2000 AS [total_bank_withdrawal_amount_2000]
				 , CASE WHEN 
					withdrawal_type = 'Vault' and Total_Amount_Available > 0
					THEN
						0 
					ELSE
						feeder.indent_after_pre_availabiliy_allocation_200 
					END AS [total_bank_withdrawal_amount_200]
				, CASE WHEN 
					withdrawal_type = 'Vault' and Total_Amount_Available > 0
					THEN
						0 
					ELSE
						CASE WHEN feeder.is_currency_chest = 'Yes'
						THEN
							ISNULL(final_total_atm_loading_after_pre_availability_allocation_amount_500,0) + ISNULL(currency_chest.forecasted_vaulting_amount_500,0)
						ELSE
							ISNULL(feeder.indent_after_pre_availabiliy_allocation_500,0)
						END
					END AS [total_bank_withdrawal_amount_500]
				, CASE WHEN 
					withdrawal_type = 'Vault' and Total_Amount_Available > 0
					THEN
						0 
					ELSE
						feeder.indent_after_pre_availabiliy_allocation_2000 
					END AS [total_bank_withdrawal_amount_2000]
				 , CASE WHEN 
					withdrawal_type = 'Vault' and Total_Amount_Available > 0
					THEN
						0 
					ELSE
						CASE WHEN feeder.is_currency_chest = 'Yes'
						THEN
							ISNULL(indent_after_pre_availabiliy_allocation_100,0) + 
							ISNULL(indent_after_pre_availabiliy_allocation_200,0) +
							ISNUll(final_total_atm_loading_after_pre_availability_allocation_amount_500,0) + ISNUll(currency_chest.forecasted_vaulting_amount_500,0) +
							ISNULL(indent_after_pre_availabiliy_allocation_2000,0)
						ELSE
							feeder.total_indent_after_pre_availabiliy_allocation 
						END
					END AS [total_bank_withdrawal_amount]
				 , sign_executive.signatory_name AS [authorized_by_1]
				 , sign_supervisor.signatory_name AS [authorized_by_2]
				 , '' AS [notes]
				 , CASE WHEN 
					withdrawal_type = 'Vault' and Total_Amount_Available > 0
					THEN
					dbo.fnNumberToWords(
										ISNULL(indent_after_pre_availabiliy_allocation_100,0)+
										ISNULL(indent_after_pre_availabiliy_allocation_200,0)+
										ISNULL(indent_after_pre_availabiliy_allocation_500,0)+
										ISNULL(indent_after_pre_availabiliy_allocation_2000,0)
									  ) 
			       ELSE
					CASE WHEN feeder.is_currency_chest = 'Yes'
						THEN	
						dbo.fnNumberToWords(
						ISNULL(indent_after_pre_availabiliy_allocation_100,0) + 
							ISNULL(indent_after_pre_availabiliy_allocation_200,0) +
							ISNULL(final_total_atm_loading_after_pre_availability_allocation_amount_500,0) + ISNULL(currency_chest.forecasted_vaulting_amount_500,0) +
							ISNULL(indent_after_pre_availabiliy_allocation_2000,0)
							)
						ELSE
						dbo.fnNumberToWords(
										ISNULL(indent_after_pre_availabiliy_allocation_100,0)+
										ISNULL(indent_after_pre_availabiliy_allocation_200,0)+
										ISNULL(indent_after_pre_availabiliy_allocation_500,0)+
										ISNULL(indent_after_pre_availabiliy_allocation_2000,0)
									  ) 
					END
				END AS [amount_in_words]
				 , CASE WHEN @is_revised_forecasting = 'Yes'
						THEN 'Revised Indent'	
						ELSE 'Indent'
						END as indent_type
				 , feeder.[project_id]
				 ,	CASE WHEN @is_revised_forecasting = 'Yes'
							THEN 'Review Pending' 
							ELSE 'Active'
						END as record_status
				 , @timestamp_date  AS [created_on]
				 --, 1
				 ,CASE WHEN @is_revised_forecasting = 'Yes'
						THEN @indentcounter + 1 
						ELSE
						1
						END		
				 ,@created_reference_id
				 ,@cur_user			 
				 , m.id -- id for indent_mail_master
				 , sign_executive.Id -- id for Auth_Signatories_Signatures for designation = 'Cash Executive'
				 , sign_supervisor.Id -- id for Auth_Signatories_Signatures for designation = 'Cash Supervisor'
				 ,CASE WHEN @is_revised_forecasting = 'Yes'
							THEN @active_indent_code
						END as previous_indent_code
				,feeder.total_vault_closing_balance_after_pre_availability
				,0
				,feeder.vault_closing_balance_after_pre_availability_100
				,feeder.vault_closing_balance_after_pre_availability_200
				,feeder.vault_closing_balance_after_pre_availability_500
				,feeder.vault_closing_balance_after_pre_availability_2000

				from #final_feeder_level_allocated_dataset feeder
				LEFT JOIN #currency_chest_feeder_branch currency_chest
				ON feeder.bank_code  COLLATE DATABASE_DEFAULT = currency_chest.bank_code
				AND feeder.feeder_branch_code COLLATE DATABASE_DEFAULT= currency_chest.feeder_branch_code COLLATE DATABASE_DEFAULT
				AND feeder.project_id   COLLATE DATABASE_DEFAULT      = currency_chest.project_id COLLATE DATABASE_DEFAULT
				--where feeder.feeder_branch_code = 'BHANDARA MAIN_0328'
				--where forecast_loading_amount_500_after_currency_chest <> final_total_atm_loading_after_pre_availability_allocation_amount_500
				LEFT JOIN Mail_Master m
				on m.project_id = feeder.project_id COLLATE DATABASE_DEFAULT
				and m.bank_code = feeder.bank_code COLLATE DATABASE_DEFAULT
				and m.feeder_branch = feeder.feeder_branch_code COLLATE DATABASE_DEFAULT
				and m.record_status = 'Active'
				left join Auth_Signatories_Signatures sign_executive
					on sign_executive.project_id = feeder.project_id COLLATE DATABASE_DEFAULT
					and sign_executive.bank_code = feeder.bank_code  COLLATE DATABASE_DEFAULT
					and sign_executive.designation in ( 'Cash Ops Manager' )
					and sign_executive.record_status = 'Active'
				left join Auth_Signatories_Signatures sign_supervisor
					 on sign_supervisor.project_id = feeder.project_id COLLATE DATABASE_DEFAULT
					 and sign_supervisor.bank_code = feeder.bank_code COLLATE DATABASE_DEFAULT
					 and sign_supervisor.designation in ( 'Cash Ops Head' )
					 and sign_supervisor.record_status = 'Active'

				left join #cypher_code CC
				on feeder.project_id COLLATE DATABASE_DEFAULT=CC.project_id
				and feeder.feeder_branch_code COLLATE DATABASE_DEFAULT=CC.feeder_branch_code
				and feeder.bank_code COLLATE DATABASE_DEFAULT = CC.bank_code

                 DECLARE @rowcount INT	
				 DECLARE @ErrorCode INT		 
                 SELECT @rowcount=@@ROWCOUNT,@ErrorCode = @@error


	  	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		 
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into indent_master Completed'
--------------------------------Inserting into Indent Detail table on atm level------------------------------------------

--SELECT * FROM #final_feeder_level_allocated_dataset
			  
		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_detail Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	  
	
INSERT INTO indent_detail
	(  
	       indent_order_number,
	       [atm_id]
	      ,[location]
	      ,[purpose]
	      ,[loading_amount_50]
	      ,[loading_amount_100]
	      ,[loading_amount_200]
	      ,[loading_amount_500]
	      ,[loading_amount_2000]
	      ,[total]
	      ,[record_status]
		  ,indent_type
	      ,[created_on]
		  ,created_by
		  ,created_reference_id
		  ,indent_counter
		  ,previous_indent_code
	)
	SELECT
		     A.indentcode ,
			 A.atm_id,
			 M.location_name, 
			 --A.dist_Purpose,
			  CASE WHEN
				--(@is_revised_forecasting = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				(@is_revised_forecasting = 'Yes') OR (temp.is_currency_chest = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				THEN
					CASE WHEN  isnull(temp.final_total_loading_amount_100_currency_chest,0) +	 isnull(temp.final_total_loading_amount_200_currency_chest,0) + isnull(temp.final_total_loading_amount_500_currency_chest,0) +isnull( temp.final_total_loading_amount_2000_currency_chest,0) = 0
					THEN ''
					ELSE 'Add Cash'
					END
				ELSE 
					CASE WHEN isnull(temp.final_total_loading_amount_100,0) +	 isnull(temp.final_total_loading_amount_200,0) + isnull(temp.final_total_loading_amount_500,0) +isnull( temp.final_total_loading_amount_2000,0) = 0
					THEN ''
					ELSE 'Add Cash'
				END
			 END ,
			 NULL,
			 CASE WHEN	
			--	(@is_revised_forecasting = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				(@is_revised_forecasting = 'Yes') OR (temp.is_currency_chest = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				THEN temp.final_total_loading_amount_100_currency_chest
				ELSE
					temp.final_total_loading_amount_100
				END,
			 CASE WHEN	
				--(@is_revised_forecasting = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				(@is_revised_forecasting = 'Yes') OR (temp.is_currency_chest = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				THEN temp.final_total_loading_amount_200_currency_chest
				ELSE
					temp.final_total_loading_amount_200
				END,
			 CASE WHEN	
				--(@is_revised_forecasting = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				(@is_revised_forecasting = 'Yes') OR (temp.is_currency_chest = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				THEN temp.final_total_loading_amount_500_currency_chest
				ELSE
					temp.final_total_loading_amount_500
				END,
			 CASE WHEN	
				--(@is_revised_forecasting = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				(@is_revised_forecasting = 'Yes') OR (temp.is_currency_chest = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				THEN temp.final_total_loading_amount_2000_currency_chest
				ELSE
					temp.final_total_loading_amount_2000
				END,
			 --temp.final_total_loading_amount_200,
			 --temp.final_total_loading_amount_500,
			 --temp.final_total_loading_amount_2000,
			 CASE WHEN
				--(@is_revised_forecasting = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				(@is_revised_forecasting = 'Yes') OR (temp.is_currency_chest = 'Yes') OR (temp.withdrawal_type = 'Vault' and final_total_loading_amount > 0)
				THEN  isnull(temp.final_total_loading_amount_100_currency_chest,0) +	 isnull(temp.final_total_loading_amount_200_currency_chest,0) + isnull(temp.final_total_loading_amount_500_currency_chest,0) +isnull( temp.final_total_loading_amount_2000_currency_chest,0)
				ELSE isnull(temp.final_total_loading_amount_100,0) +	 isnull(temp.final_total_loading_amount_200,0) + isnull(temp.final_total_loading_amount_500,0) +isnull( temp.final_total_loading_amount_2000,0)
			 END AS [total] 
			 ,CASE WHEN @is_revised_forecasting = 'Yes'
			   THEN 'Review Pending' 
			   ELSE 'Active'
			   END as record_status
			,CASE WHEN @is_revised_forecasting = 'Yes'
			  THEN 'Revised Indent'	
			  ELSE 'Indent'
			  END as indent_type,
			 @timestamp_date  AS [created_on],
			 @cur_user,
			 @created_reference_id	
			 --,1
			 ,CASE WHEN @is_revised_forecasting = 'Yes'
						THEN @indentcounter + 1 
						ELSE
						1
						END	
			 ,CASE WHEN @is_revised_forecasting = 'Yes'
							THEN @active_indent_code
						END as previous_indent_code		
		 FROM #dist_with_cra   A
		 LEFT JOIN atm_master M
		 ON A.site_code  COLLATE DATABASE_DEFAULT= M.site_code COLLATE DATABASE_DEFAULT
		 AND A.atm_id   COLLATE DATABASE_DEFAULT= M.atm_id COLLATE DATABASE_DEFAULT
		 AND M.record_status ='Active' and M.site_status = 'Active'
		 LEFT JOIN  #final_atm_level_allocated_dataset temp
		 on temp.atm_id COLLATE DATABASE_DEFAULT= A.atm_id COLLATE DATABASE_DEFAULT
		 AND temp.bank_code COLLATE DATABASE_DEFAULT= A.bank_code COLLATE DATABASE_DEFAULT
		 AND temp.project_id COLLATE DATABASE_DEFAULT= A.project_id COLLATE DATABASE_DEFAULT
		

		--select * from #final_atm_level_allocated_dataset_temp
		
		
		
		 
			INSERT INTO calculated_avg_dispense
			(
			atm_id						,
			bank_code					,
			project_id					,
			T_M3_minus_3				,
			T_M3_minus_2				,
			T_M3_minus_1				,
			T_M3_minus_0				,
			T_M3_plus_1					,
			T_M3_plus_2					,
			T_M3_plus_3					,
			T_M2_minus_3				,
			T_M2_minus_2				,
			T_M2_minus_1				,
			T_M2_minus_0				,
			T_M2_plus_1					,
			T_M2_plus_2					,
			T_M2_plus_3					,
			T_M1_minus_3				,
			T_M1_minus_2				,
			T_M1_minus_1				,
			T_M1_minus_0				,
			T_M1_plus_1					,
			T_M1_plus_2					,
			T_M1_plus_3					,
			T_minus_7					,
			T_minus_6					,
			T_minus_5					,
			T_minus_4					,
			T_minus_3					,
			T_minus_2					,
			T_minus_1					,
			avg_of_5days_Tminus5		,
			avg_of_2days_Tminus2		,
			avg_month_minus_3			,
			avg_month_minus_2			,
			avg_month_minus_1			,
			avg_month_minus_0			,
			max_month_minus_3			,
			max_month_minus_2			,
			max_month_minus_1			,
			max_month_minus_0			,
			second_max_month_minus_0	,
			third_max_month_minus_0		,
			second_max_month_minus_1	,
			third_max_month_minus_1		,
			second_max_month_minus_2	,
			third_max_month_minus_2		,
			second_max_month_minus_3	,
			third_max_month_minus_3		,
			avg_28_days					,
			max_of_four_months_avg		,
			avg_of_four_months_avg		,
			default_dispense_amount		,
			max_of_four_months_max			,
			avg_of_four_months_max			,
			max_of_four_months_second_max	,
			avg_of_four_months_second_max	,
			max_of_four_months_third_max	,
			avg_of_four_months_third_max	,
			max_28_days_or_2_days		,
			max_of_current				,
			datafor_date_time			,
			record_status				,
			created_on					,
			created_by					,
			created_reference_id				
			)
			SELECT	atm_id						,
					bank_code					,
					project_id					,
					T_M3_minus_3				,
					T_M3_minus_2				,
					T_M3_minus_1				,
					T_M3_minus_0				,
					T_M3_plus_1					,
					T_M3_plus_2					,
					T_M3_plus_3					,
					T_M2_minus_3				,
					T_M2_minus_2				,
					T_M2_minus_1				,
					T_M2_minus_0				,
					T_M2_plus_1					,
					T_M2_plus_2					,
					T_M2_plus_3					,
					T_M1_minus_3				,
					T_M1_minus_2				,
					T_M1_minus_1				,
					T_M1_minus_0				,
					T_M1_plus_1					,
					T_M1_plus_2					,
					T_M1_plus_3					,
					T_minus_7					,
					T_minus_6					,
					T_minus_5					,
					T_minus_4					,
					T_minus_3					,
					T_minus_2					,
					T_minus_1					,
					avg_of_5days_Tminus5		,
					avg_of_2days_Tminus2		,
					avg_month_minus_3			,
					avg_month_minus_2			,
					avg_month_minus_1			,
					avg_month_minus_0			,
					max_month_minus_3			,
					max_month_minus_2			,
					max_month_minus_1			,
					max_month_minus_0			,
					second_max_month_minus_0	,
					third_max_month_minus_0		,
					second_max_month_minus_1	,
					third_max_month_minus_1		,
					second_max_month_minus_2	,
					third_max_month_minus_2		,
					second_max_month_minus_3	,
					third_max_month_minus_3		,
					avg_28_days					,
					max_of_four_months_avg		,
					avg_of_four_months_avg		,
					default_dispense_amount		,
					max_of_four_months_max			,
					avg_of_four_months_max			,
					max_of_four_months_second_max	,
					avg_of_four_months_second_max	,
					max_of_four_months_third_max	,
					avg_of_four_months_third_max	,
					max_28_days_or_2_days		,
					max_of_current				,
					CAST(@dateT as date)
					,'Active'
					,@timestamp_date
					,@cur_user
					,@created_reference_id
			FROM #calculated_avg_dispense

			--select * from #calculated_avg_dispense
		-- select * from #final_atm_level_allocated_dataset
		 --TRUNCATE TABLE indent_detail
		 --TRUNCATE TABLE  indent_master

						DECLARE @row_dul INT			 
                        SET @row_dul=@@ROWCOUNT
	
			set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into indent_detail Completed'

	
                        INSERT into data_update_log 
									(bank_code,
									project_id,
									operation_type,
									datafor_date_time,
									data_for_type,			 
									status_info_json,			 
									record_status,
									date_created,
									created_by,
									created_reference_id
									)
                                values
									(NULL,
									NULL,
									'Indent',
									@dateT,
									'Indent',			 
									'Indent calculated for date '+cast(@dateT as varchar(30)) + ' and no. of rows inserted : '+ cast(@row_dul as varchar(10)),
									'Active',
									@timestamp_date,
									@cur_user,
									@created_reference_id			 			
									)
			

			IF (@is_revised_forecasting = 'Yes')
			BEGIN
				UPDATE irr
					 SET irr.indent_revision_order_number = ir.indent_order_number,
						 irr.record_status = 'Review Pending',
						 modified_by = @cur_user,
						 modified_on = @timestamp_date,
						 modified_reference_id = @created_reference_id
					 FROM indent_revision_request irr
					 JOIN indent_master ir
					 on irr.indent_order_number = ir.previous_indent_code
					 AND irr.record_status = 'Approved'
			END

			 IF (@ErrorCode = 0)
                        BEGIN
                            IF(@row > 0)
                            BEGIN
                                    SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'success') 
                                  --  SELECT @outputval
                            END
							ELSE
							BEGIN
									SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'no_records') 
                                    --SELECT @outputval
							END
						END
	  commit transaction
 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Execution of [dbo].[usp_indent_calculation_V2] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	

END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
                     --   SELECT @outputval	
							
						 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
						  values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))				
			 END CATCH
END
