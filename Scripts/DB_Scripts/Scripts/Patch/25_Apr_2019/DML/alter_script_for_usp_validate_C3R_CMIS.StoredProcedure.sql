
USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[usp_validate_C3R_CMIS]    Script Date: 4/25/2019 5:38:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_validate_C3R_CMIS]
( 
	@datafor_date_time varchar(50) ,@ForRecordstatus varchar(50),@referenceid varchar(50),@systemUser varchar(50),@outputVal varchar(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @errorcode nvarchar(max)
	--DECLARE @outputVal varchar(max) 
	DECLARE @datafor nvarchar(5) = 'C3R'
	DECLARE @pendingstatus nvarchar(20) = 'Approval Pending'
	DECLARE @uploadedstatus nvarchar(20) = 'Uploaded'


	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[usp_validate_C3R_CMIS] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

------------------ Check if file is present in Data Update Log---------------

	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[data_update_log] WITH (NOLOCK)
				WHERE [datafor_date_time] = @datafor_date_time AND 
                      data_for_type = @datafor and 
                      record_status = @uploadedstatus AND 
					created_reference_id = @referenceid
				)
				
		BEGIN

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			
------------------ Check if file is present in Bank Wise File Table ----------------------

			IF EXISTS( Select 1 as ColumnName
						FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
						WHERE [datafor_date_time] = @datafor_date_time AND 
							  record_status = @ForRecordstatus 
							  AND created_reference_id = @referenceid
				)
				
				BEGIN
					
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in [dbo].[c3r_CMIS]', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				------- Select total count of records present inb file for particular date and status------------
				
				SET @CountTotal = (	
									SELECT count(1) 
									FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
						            WHERE [datafor_date_time] = @datafor_date_time 
										  and record_status = @ForRecordstatus 
										  AND created_reference_id = @referenceid					
								   ) 

				------------- Creating temporary table to process multiple update operations ---------------------------
	
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Creating Temporary Table and storing data', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				DROP TABLE IF EXISTS #temp_C3R
				SELECT * 
				INTO #temp_C3R 
				FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
				WHERE datafor_date_time= @datafor_date_time
					  AND record_status = @ForRecordstatus  
					  AND created_reference_id = @referenceid
					 
				
				DROP TABLE IF EXISTS #temp_C3R_VMIS
				SELECT * 
				INTO #temp_C3R_VMIS 
				FROM [dbo].[c3r_VMIS] WITH (NOLOCK)
				WHERE datafor_date_time= @datafor_date_time
					  AND record_status = @ForRecordstatus  
					  AND created_reference_id = @referenceid
					  
				update #temp_C3R_VMIS
				SET error_code = NULL, is_valid_record = 'Yes'
				
				UPDATE C3R_VMIS_TEMP 
				SET C3R_VMIS_TEMP.is_valid_record =  b.is_valid_record,
					C3R_VMIS_TEMP.error_code = b.error_code
					
				FROM [c3r_VMIS] C3R_VMIS_TEMP
					INNER JOIN #temp_C3R_VMIS b
					on b.bank = C3R_VMIS_TEMP.bank
					and b.cra_name = C3R_VMIS_TEMP.cra_name
					and b.feeder_branch_name = C3R_VMIS_TEMP.feeder_branch_name
					AND C3R_VMIS_TEMP.created_reference_id = @referenceid


				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


				UPDATE #temp_C3R 
				SET    error_code =  
						CASE 
							WHEN atm_id  in (Select atm_id from atm_master where record_status ='Active' and site_status in ('Active', 'Live'))
								  AND
								  bank  in (Select DISTINCT bank_code from atm_master where record_status ='Active' and site_status in ('Active', 'Live'))
							THEN NULL
							ELSE
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'master_validation_failed')
						END	
				
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


				-------------------------------------Validation 1------------------------------------------------------------------		
				--			Loading as per ATM Replenishment Details = Loading as per Switch Counter						   	--
				-------------------------------------------------------------------------------------------------------------------

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Loading as per ATM Replenishment Details Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				--BEGIN TRAN
				UPDATE #temp_C3R 
					SET     error_code =  
								CASE 
									WHEN (atm_repl_total = sw_loading_total)
									THEN NULL
									ELSE (
											SELECT cast(sequence as varchar(50)) 
											FROM app_config_param where 
											sub_category = 'atm_repl_total_not_matching' 
											and category = 'C3R validation'
										 )
								END		
				
				--COMMIT TRAN	
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Loading as per ATM Replenishment Details Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
									  
				------------------------------Validation 2------------------------------------		
				 --      ATM Counter Balance = Switch Counter Balance						  --
				------------------------------------------------------------------------------	
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for ATM Counter Balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				--BEGIN TRAN
				UPDATE #temp_C3R 
				SET     error_code =  
					  CASE  
					          WHEN	atmcount_op_bal_total <> sw_count_openbal_total 
					          THEN				
					          CASE
					               WHEN (error_code IS NULL)
					               THEN    
					                   (SELECT cast(sequence as varchar(50)) FROM app_config_param with (nolock) where category = 'C3R validation' and sub_category = 'atm_counter_mismatch')
					                ELSE 
					                   CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param with (nolock) where category = 'C3R validation' and sub_category = 'atm_counter_mismatch')) as varchar(max))
					              END
					          ELSE error_code 
					END
				--COMMIT TRAN
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for ATM Counter Balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			--	----------------------------------------Validation 3--------------------------------------------------------------	
			--	--					Sum of amount replenished should match with the total amount								--
			--	------------------------------------------------------------------------------------------------------------------
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for amount replenished Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				--BEGIN TRAN
					UPDATE #temp_C3R 
						SET     error_code =  
						CASE  
								WHEN	   
									(atm_repl_100 + atm_repl_200 + atm_repl_500 + atm_repl_2000) <> atm_repl_total 
								THEN				
								CASE
									WHEN (error_code IS NULL)
									THEN    
										(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'amount_replenished')
									ELSE 
										CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'amount_replenished')) as varchar(max))
									END
								ELSE error_code 
						END

				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for amount replenished Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
			--	----------------------------------------Validation 4--------------------------------------------------------------	
			--	--					Sum of closing balance should match with the total closing balance							--
			--	------------------------------------------------------------------------------------------------------------------
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for closing balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				--BEGIN TRAN

				UPDATE #temp_C3R 
				SET     error_code =  
				CASE  
						WHEN	
							(sw_closing_100 + sw_closing_200 + sw_closing_500 + sw_closing_2000) <> sw_closing_total
						THEN				
						CASE
							WHEN (error_code IS NULL)
							THEN    
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'closing_balance')
							ELSE 
								CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'closing_balance')) as varchar(max))
							END
						ELSE error_code 
				END
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for closing balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)						
		
				----------------------------------------Validation 5--------------------------------------------------------------	
				--					Sum of opening balance should match with the total opening balance							--
				------------------------------------------------------------------------------------------------------------------
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for opening balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--BEGIN TRAN
				UPDATE #temp_C3R 
				SET     error_code =  
				CASE  
						WHEN	 
						 (atmcount_op__bal_100 + atmcount_op__bal_200 + atmcount_op__bal_500 + atmcount_op__bal_2000) <> atmcount_op_bal_total
						THEN				
						CASE
							WHEN (error_code IS NULL)
							THEN    
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_balance')
							ELSE 
								CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_balance')) as varchar(max))
							END
						ELSE error_code 
				END
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for opening balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				----------------------------------------Validation 6--------------------------------------------------------------	
				--					Sum of remianing cash should match with the total remianing cash 						--
				------------------------------------------------------------------------------------------------------------------
		
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for remianing cash Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--BEGIN TRAN
					UPDATE #temp_C3R 
					SET     error_code =  
					CASE  
							WHEN	 
								(atmphysical_total_remainingcash_100 + atmphysical_total_remainingcash_200 + atmphysical_total_remainingcash_500 + atmphysical_total_remainingcash_2000) <> atmphysical_total_remainingcash_total
							THEN				
							CASE
								WHEN (error_code IS NULL)
								THEN    
									(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'remaining_cash')
								ELSE 
									CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'remaining_cash')) as varchar(max))
								END
							ELSE error_code 
					END
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for remianing cash Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		--		select 'Validation 7'
			----------------------------------------Validation 7-------------------------------------------------------------	
			--					Sum of closing balance in atm should match with the total closing balance in atm					--
			------------------------------------------------------------------------------------------------------------------
			
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for closing balance in atm Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--BEGIN TRAN
					UPDATE #temp_C3R 
					SET     error_code =  
					CASE  
							WHEN	   
							 (atm_closingbal_100 + atm_closingbal_200 + atm_closingbal_500 + atm_closingbal_2000) <> atm_closingbal_total 
							THEN				
							CASE
								WHEN (error_code IS NULL)
								THEN    
									(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'atm_closing_balance')
								ELSE 
									CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'atm_closing_balance')) as varchar(max))
								END
							ELSE error_code 
					END
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for closing balance in atm Comleted', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--select 'Validation 8'

			--------------------------------------Validation 8-------------------------------------------------------------	
				--				Previous day closing amount should match with the current day opening amount				--
			----------------------------------------------------------------------------------------------------------------
			IF EXISTS ( SELECT 1 FROM c3r_CMIS old with (nolock) 
						JOIN #temp_C3R new with (nolock) 
						on old.atm_id = new.atm_id
						AND old.bank = new.bank
						where old.date = dateadd(day,-1,new.date)
						AND old.record_status =	'Active'
						)
				BEGIN
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Previous day closing amount should match with the current day opening amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						--BEGIN TRAN
						UPDATE #temp_C3R 
						SET     error_code = 
						CASE  
								WHEN	   
									atmcount_op_bal_total <> (
															SELECT sw_closing_total
															FROM c3r_CMIS c WHERE 
															c.date = dateadd(day,-1,#temp_C3R.date) 
															AND c.record_status			=	'Active'
															AND #temp_C3R.atm_id		=	c.atm_id
															AND #temp_C3R.bank			=	c.bank
															AND #temp_C3R.feeder_branch =	c.feeder_branch
															)
								THEN				
								CASE
									WHEN (error_code IS NULL)
									THEN    
										(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_closing_amount')
									ELSE 
										CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_closing_amount')) as varchar(max))
									END
								ELSE error_code 
						END
						INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Previous day closing amount should match with the current day opening amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						--COMMIT TRAN
					END
				
					--select 'Validation 9'
			----------------------------------------Validation 7-------------------------------------------------------------	
			--					Total Loading as per CMIS= Total loading as per VMIS										--
			------------------------------------------------------------------------------------------------------------------
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Total Loading as per CMIS = Total loading as per VMIS Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--BEGIN TRAN
					
					DROP TABLE IF EXISTS #temp
					DROP TABLE IF EXISTS #validrecords
					;with #temp
					as
					(
						select sum(sw_loading_total) as c3r_amount,bank,feeder_branch 
						from #temp_C3R
						group by bank,feeder_branch
					)
						
					select vmis.*, 
					case 
					when 
					temp.c3r_amount = vmis.total_replenishto_atm
					then 'Valid'
					else 'Invalid'
					END
					as record_flag
					into #validrecords 
					from c3r_VMIS vmis
					join #temp temp 
					on vmis.bank = temp.bank
					and vmis.feeder_branch_name = temp.feeder_branch
					AND vmis.created_reference_id = @referenceid

					--select * from #temp_C3R
					--select * from #validrecords
					--SELECT  record_flag
					--from #validrecords valid  JOIN #temp_C3R on
					-- valid.feeder_branch_name = #temp_C3R.feeder_branch
					--AND valid.bank = #temp_C3R.bank


					UPDATE c 
					SET     error_code = 
					CASE  
							WHEN	   
							 v.record_flag = 'Invalid'
							THEN				
							CASE
								WHEN (c.error_code IS NULL)
								THEN    
									(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'cmis_vmis_mismatch')
								ELSE 
									CAST(CONCAT(c.error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'cmis_vmis_mismatch')) as varchar(max))
								END
							ELSE c.error_code 
					END 
					FROM #temp_C3R c
					JOIN #validrecords v
					on c.bank = v.bank 
					AND c.feeder_branch = v.feeder_branch_name

						
						
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Total Loading as per CMIS = Total loading as per VMIS completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			--	select 'Validation 10'
				
				
 ---------Updating is_valid_record column as Yes or No-------------
			--BEGIN TRAN
			UPDATE #temp_C3R
			SET    is_valid_record = 
					CASE 
						WHEN error_code IS NULL
						THEN 'Yes'
						ELSE 'No'
					END		
			--COMMIT TRAN
			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Is Valid Record Field Updated';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Is Valid Record Field Updated', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
-----------------Checking count for valid records in updated temporary table -----------------------------

			SET @countCalculated = (
									SELECT count(1) 
									FROM #temp_C3R  WITH (NOLOCK)
									WHERE is_valid_record = 'Yes' 
									)

-----------------Comparing both the counts ---------------------------------------------------------

			IF(@countCalculated != @CountTotal AND @countCalculated > 0)
				BEGIN
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Count Mismatch (Partial Valid File)';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
			
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'Exception' and sub_category = 'Partial Valid'
									)	
							
				END
			ELSE IF (@countCalculated = @CountTotal)
				BEGIN 
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Count Matched (Valid File)';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				SET @outputVal = (
								SELECT sequence from  [dbo].[app_config_param]
								where  category = 'File Operation' and sub_category = 'Data Validation Successful'
							)												
				END

			ELSE
				BEGIN
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- No valid record found in table';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
									)				
			END

------------------------Inserting distinct error code in new temporary table. Also splitting with , ------------------------

			IF OBJECT_ID('tempdb..#temp_distinct_codes') IS NOT NULL
			BEGIN
				DROP TABLE #temp_distinct_codes
			END
			ELSE
			BEGIN

				--SELECT Distinct error_code from 
				--SELECT DISTINCT VALUE 
				--INTO #tempbankfile 
				--FROM  #temp_C3R WITH(NOLOCK)
				--CROSS APPLY STRING_SPLIT(#temp_C3R.error_code, ',')
				
					DECLARE @Names VARCHAR(max) 
					;with distinct_error_codes
					as
					(
						select distinct error_code from #temp_C3R
						where error_code is not null
					)
					SELECT @Names = COALESCE(@Names + ',','') +  TRIM(error_code)
					FROM  distinct_error_codes

					SELECT DISTINCT VALUE INTO #temp_distinct_codes FROM string_split (@Names,',') 
				
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Temporary Table Created for storing error code';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Temporary Table Created for storing error code', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

--------------------- Creating one single error code string with , seperated delimiter---------------------------------------				
				SET @errorcode = (
								SELECT 
									Stuff((
										SELECT N', ' + VALUE FROM #temp_distinct_codes WITH(NOLOCK) FOR XML PATH(''),TYPE)
										.value('text()[1]','nvarchar(max)'),1,2,N''
										)
								)
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Error code String Created';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;			
			END			
			
--			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Started.... Updating columns in C3R file and data update log', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
--			BEGIN TRAN

			--IF (@Debug = 1)
			--	BEGIN
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--	    PRINT '-- Transaction Started.... Updating columns in bank wise file';
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--	END;	


------------------------- Updating columns in bank wise file using temporary table------------------------------------------
				BEGIN TRAN
				UPDATE C3R 
				SET C3R.is_valid_record =  b.is_valid_record,
					C3R.error_code = b.error_code,
					record_status = 
						CASE 
							WHEN b.is_valid_record = 'Yes' 
							THEN @pendingstatus
							WHEN b.is_valid_record = 'No' 
							THEN @uploadedstatus
						END
				FROM [c3r_CMIS] C3R
					INNER JOIN #temp_C3R b
					on b.atm_id = C3R.atm_id
					AND C3R.record_status = @ForRecordstatus
					AND C3R.created_reference_id = @referenceid


				UPDATE C3R 
				SET record_status = @pendingstatus,
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				FROM [c3r_VMIS] C3R
					WHERE C3R.[datafor_date_time] = @datafor_date_time 
					AND C3R.record_status = @ForRecordstatus
					AND C3R.created_reference_id = @referenceid
			
		--	COMMIT TRAN
			--IF (@Debug = 1)
			--	BEGIN
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--	    PRINT '-- Columns has been updated in bank wise file';
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--	END;
		

			--IF (@Debug = 1)
			--	BEGIN
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--	    PRINT '-- Updating columns in data update log ';
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--	END;

------------------------- Updating columns in Data Update Log table------------------------------------------
			--	BEGIN TRAN
				
				UPDATE dbo.data_update_log
				SET is_valid_file =
					CASE	
							WHEN @outputVal = 50009 
							THEN  1
							WHEN @outputVal = 50001
							THEN 0
							WHEN @outputVal = 10001
							THEN 0
					END,
					validation_code = (SELECT @errorcode),
					record_status = 
					CASE	
							WHEN @outputVal = 50009 
							THEN @pendingstatus
							WHEN @outputVal = 50001
							THEN @pendingstatus
							WHEN @outputVal = 10001
							THEN @uploadedstatus
					END,	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				WHERE [datafor_date_time] =  @datafor_date_time
					AND record_status = @ForRecordstatus 
					AND	data_for_type = @datafor
					and created_reference_id = @referenceid

			COMMIT TRAN;

			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Transaction Completed....Column has been updated in data update log ';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Completed....Column has been updated in data update log and bank wise file', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Execution of [dbo].[[sp_validate_C3R_CMIS] Completed.';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[usp_validate_C3R_CMIS] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			    --DECLARE @tableName VARCHAR(30) = 'dbo.[c3r_CMIS]'
       --         DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
       --         DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
       --         DECLARE @SetWhereClause VARCHAR(MAX) = '  datafor_date_time = ''' + 
       --                                         cast(@datafor_date_time as varchar(50)) + ''''


       --         EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT


       --         DECLARE @tableName_DUL VARCHAR(30) = 'dbo.data_update_log'
       --         DECLARE @ForStatus_DUL VARCHAR(30) = 'Uploaded'
       --         DECLARE @ToStatus_DUL VARCHAR(30) = 'Approval Pending'
       --         DECLARE @SetWhereClause_DUL VARCHAR(MAX) ='datafor_date_time = ''' + 
       --                                             cast(@datafor_date_time as varchar(50)) +  ''' and  data_for_type = ''C3R'''

       --         EXEC  dbo.[uspSetStatus] @tableName_DUL,@ForStatus_DUL,@ToStatus_DUL,@timestamp_date,@systemUser,@referenceid,@SetWhereClause_DUL,@out OUTPUT
                    --  SELECT @outputVal

	
		END
			ELSE
			BEGIN
				  RAISERROR (50002, 16,1);
			END
		END
	ELSE
		BEGIN
		--SELECT SESSION_ID from sys.dm_exec_sessions;  
			RAISERROR (	50004, 16,1);
		END
END

