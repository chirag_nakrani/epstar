delete from menu_master where menu_id in ('loading_operation','vaulting_operation','withdrawal_operation')

update menu_master set display_name = 'Withdrawal Status' where menu_id = 'withdrawal'

update menu_master set display_name = 'Diversion Status' where menu_id = 'diversion_operation'

update menu_master set mapped_url = 'operations/loadingstatus' where menu_id = 'loadingstatus'

update menu_master set mapped_url = 'operations/withdrawalstatus' where menu_id = 'withdrawal'