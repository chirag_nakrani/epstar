/****** Object:  Table [dbo].[gl_account_number]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gl_account_number]
(
[id] [bigint] IDENTITY(1,1) PRIMARY KEY NOT NULL,
atm_id nvarchar(50) NULL,
site_code nvarchar(50) NULL,
bank_code nvarchar(50) NULL,
gl_acc_number nvarchar(50) NULL,
[record_status] [nvarchar](50) NULL,
[created_on] [datetime] NULL,
[created_by] [nvarchar](50) NULL,
[created_reference_id] [nvarchar](50) NULL,
[approved_on] [datetime] NULL,
[approved_by] [nvarchar](50) NULL,
[approved_reference_id] [nvarchar](50) NULL,
[approve_reject_comment] [nvarchar](50) NULL,
[rejected_on] [datetime] NULL,
[rejected_by] [nvarchar](50) NULL,
[reject_reference_id] [nvarchar](50) NULL,
[modified_on] [datetime] NULL,
[modified_by] [nvarchar](50) NULL,
[modified_reference_id] [nvarchar](50) NULL,
[is_valid_record] [nvarchar](20) NULL,
[error_code] [nvarchar](50) NULL
)
GO