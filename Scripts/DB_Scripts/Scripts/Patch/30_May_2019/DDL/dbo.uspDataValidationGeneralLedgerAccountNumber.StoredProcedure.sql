USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspDataValidationGeneralLedgerAccountNumber]    Script Date: 5/30/2019 1:47:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


CREATE PROCEDURE [dbo].[uspDataValidationGeneralLedgerAccountNumber]
( 
	@api_flag    VARCHAR(50),
	@systemUser  VARCHAR(50),
	@referenceid VARCHAR(50),
	@outputVal   VARCHAR(50) OUTPUT
)
AS
BEGIN
	BEGIN TRY
	BEGIN TRAN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar            (max)
	DECLARE @brand_code nvarchar     (max)
	DECLARE @sitecode nvarchar       (max)
	DECLARE @procedurename nvarchar(100) = (SELECT OBJECT_NAME(@@PROCID))


 
	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	 values ('Execution of [dbo].[uspDataValidationGeneralLedgerAccountNumber] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	--DECLARE @CountTotal  int 
	--DECLARE @countCalculated  int
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].data_update_log_master
				WHERE record_status = 'Uploaded'
				AND created_reference_id = @referenceid
			 )
		BEGIN
			IF EXISTS (select 1 as columnname from gl_account_number where record_status = 'Uploaded' and created_reference_id = @referenceid)
				BEGIN
				--checking if inserted record is valid or not
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
					values ('checking if inserted record is valid or not', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
					update a
					set a.record_status= 'Approval Pending',
					a.modified_on = @current_datetime_stmp,
					a.modified_by = @systemUser,
					a.modified_reference_id = @referenceid
					from gl_account_number a
					inner join cra_feasibility c
					on a.atm_id = c.atm_id and
					a.bank_code = c.bank_code and 
					a.site_code = c.site_code
					where a.record_status = 'Uploaded'
					and a.created_reference_id = @referenceid


					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
					values ('Updating error code for invalid data started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
					
					update gl_account_number
					SET error_code = 
						 isnull(case when atm_id not in (select distinct(atm_id) from cra_feasibility where record_status = 'Active') then 'atm_id not found,' end, '')+
						 isnull(case when bank_code not in (select distinct(bank_code) from cra_feasibility where record_status = 'Active') then 'bank_code not found,' end, '')+
						 isnull(case when site_code not in (select distinct(site_code) from cra_feasibility where record_status = 'Active') then 'site_code not found,' end, ''),
						is_valid_record = (case when error_code is NULL then 'No' end)
					where created_reference_id = @referenceid
					and record_status = 'Uploaded'

					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
					values ('Updating same old record to history started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
					--updating old same record to History
					update a
					set a.record_status = 'History',
					a.modified_on = @current_datetime_stmp,
					a.modified_by = @systemUser,
					a.modified_reference_id = @referenceid
					from gl_account_number a
					inner join gl_account_number c
					on a.atm_id = c.atm_id and
					a.bank_code = c.bank_code and 
					a.site_code = c.site_code 
					and c.record_status = 'Approval Pending'
					where a.record_status = 'Active'

					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
					values ('Updating valid records to Active started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
					--updating inserted record to Active
					update a
					set a.record_status = 'Active',
					a.modified_on = @current_datetime_stmp,
					a.modified_by = @systemUser,
					a.modified_reference_id = @referenceid,
					a.is_valid_record = 'Yes'
					from gl_account_number a
					inner join gl_account_number c
					on a.atm_id = c.atm_id and
					a.bank_code = c.bank_code and 
					a.site_code = c.site_code 
					where a.record_status = 'Approval Pending'
					and a.created_reference_id = @referenceid

					declare @totalrecords int
					declare @total_valid_records int

					SET @totalrecords = (select count(1) from gl_account_number where created_reference_id = @referenceid)
					SET @total_valid_records = (select count(1) from gl_account_number where created_reference_id = @referenceid and record_status = 'Active')
					if @total_valid_records > 0
						BEGIN
							update data_update_log_master 
							set record_status = 'Active',
							is_valid_file = 'Yes',
							modified_on = @current_datetime_stmp,
							modified_by = @systemUser,
							modified_reference_id = @referenceid
							where record_status = 'Uploaded'
							and created_reference_id = @referenceid
							SET @outputVal = 'S101'
						END
					ELSE
						BEGIN
							update data_update_log_master 
							set is_valid_file = 'No'
							where created_reference_id = @referenceid

							update gl_account_number
							set is_valid_record = 'No'
							where created_reference_id = @referenceid

							SET @outputVal = 'E112'
						END
					--updating data_update_log_master
					
				END
			ELSE
				BEGIN
					update data_update_log_master 
					set is_valid_file = 'No'
					where created_reference_id = @referenceid

					update gl_account_number
					set is_valid_record = 'No'
					where created_reference_id = @referenceid

					RAISERROR(90002,16,1)
				END
		END

	ELSE
		BEGIN
			RAISERROR(90002,16,1)
		END

COMMIT;
END TRY
BEGIN CATCH
IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedurename
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SELECT @ErrorNumber;				
			 END CATCH
	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	values ('execution of [uspDataValidationGeneralLedgerAccountNumber] completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
END
