USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspValidateReferenceFiles]    Script Date: 5/30/2019 1:48:22 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspValidateReferenceFiles] 
@file_type varchar(50) ,@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
			DECLARE @Cmd NVARCHAR(MAX);
			--DECLARE @file_type varchar(50) = 'ATM'
			--DECLARE @api_flag varchar(50) = 'U'
			--DECLARE @systemUser varchar(50) = 'as'
			--DECLARE @referenceid varchar(50) = 'asasf'
			DECLARE @Result varchar(50)			
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = GETDATE();
			DECLARE @out1 varchar(100);

			SET @Cmd = CASE 
							WHEN @file_type = 'CRAVAULTMASTER' 
							THEN 'EXEC [uspDataValidationCRAVaultMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'MAILMASTER' 
							THEN 'EXEC [uspDataValidationMailMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'BANKESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationBANKEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'EPSESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationEPSEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'CRAESCALATIONMATRIX' 
							THEN 'EXEC [uspDataValidationCRAEscalationEmails] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'CRAEMPANELED' 
							THEN 'EXEC [uspDataValidationCRAEmpaneled] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'DEFAULTLOADING' 
							THEN 'EXEC [uspDataValidationDefaultLoading] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'BRANDBILLCAPACITY' 
							THEN 'EXEC [uspDataValidationBrandBillCapacity] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'SIGNATURE' 
							THEN 'EXEC [uspDataValidationSignature] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'CYPHERCODE' 
							THEN 'EXEC [uspDataValidationCypherCode] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'ATM_LIST' 
							THEN 'EXEC [uspDataValidationAtmListForRevision] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'ATM_REVISION_CASH_AVAILABILITY' 
							THEN 'EXEC [uspDataValidationAtmRevisionCashAvailability] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							WHEN @file_type = 'GENERAL_LEDGER_ACCOUNT_NUMBER' 
							THEN 'EXEC [uspDataValidationGeneralLedgerAccountNumber] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							--WHEN @file_type = 'CRAFEASIBILITY' 
							--THEN 'EXEC [uspDataValidationCRAFeasibility] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'
							--WHEN @file_type = 'AMS_ATM' 
							--THEN 'EXEC [uspDataValidationAtmMaster] ''' +  @api_flag+ ''''+ ','+ ''''+@systemUser+''''+ ','+''''+@referenceid+''''+ ', @Result OUTPUT ;'

							--WHEN @file_type = 'BOMH' 
							--THEN 'EXEC uspDataValidationBOMH ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							--WHEN @file_type = 'BOI' 
							--THEN 'EXEC uspDataValidationBOI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'BOB' 																															   
							--THEN 'EXEC uspDataValidationBOB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'CBI' 																															
							--THEN 'EXEC uspDataValidationCBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'CAB' 																															 
							--THEN 'EXEC uspDataValidationCAB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'CORP' 																															 
							--THEN 'EXEC uspDataValidationCORP ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'DENA' 																														  
							--THEN 'EXEC uspDataValidationDENA ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							--WHEN @file_type = 'IDBI' 																														  
							--THEN 'EXEC uspDataValidationIDBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							--WHEN @file_type = 'IOB' 																															   
							--THEN 'EXEC uspDataValidationIOB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'LVB' 																															   
							--THEN 'EXEC uspDataValidationLVB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'PSB' 																															   
							--THEN 'EXEC uspDataValidationPSB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'RSBL' 																															   
							--THEN 'EXEC uspDataValidationRSBL ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'SBI' 																															   
							--THEN 'EXEC uspDataValidationSBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'UCO' 																															   
							--THEN 'EXEC uspDataValidationUCO ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'UBI' 																															   
							--THEN 'EXEC uspDataValidationUBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
							--WHEN @file_type = 'VJB' 																															   
							--THEN 'EXEC uspDataValidationVJB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+  ''''+@projectid+'''' + ','+''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+', @Result OUTPUT ;'
					
							END
							print(@cmd)
			EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 

			IF(@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence from app_config_param where category = 'CBR_Validation' and sub_category = 'Error')
			END

------ Fetching sequence from table for success and error
			--SET @successVal =(
			--				SELECT sequence from  [dbo].[app_config_param]
			--				where category = 'File Operation' and sub_category = 'Data Validation Successful'
			--				)
                            
   --         SET @failedVal =(
			--				SELECT sequence from  [dbo].[app_config_param]
			--				where  category = 'File Operation' and sub_category = 'Data Validation Failed'
			--				)
------	If validation is successfull then proceed with the status update change
				
------ Calling Procedure to Update the status 
		            
            IF(@Result = 'S101')
				BEGIN	
					DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log_master'
					DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
					DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
					DECLARE @SetWhereClause VARCHAR(MAX) =' data_for_type = ''' + 
													@file_type+''' and created_reference_id = '''+@referenceid+''''

					EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT

					--DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log_master'
					--DECLARE @ForStatus1 VARCHAR(30) = 'Active'
					--DECLARE @ToStatus1 VARCHAR(30) =  'Deleted'
					--DECLARE @SetWhereClause1 VARCHAR(MAX) ='';

					--EXEC  dbo.[uspSetStatus] @tableName,@ForStatus1,@ToStatus1,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT

		
					SELECT @Result;	
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
 		
		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						DECLARE @code varchar(100)
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						IF @ErrorNumber = 90001
						BEGIN
							SET @code = 'E101'
						END
						ELSE IF @ErrorNumber = 90002
						BEGIN
							SET @code = 'E102'
						END
						SELECT @code
						
				END
			 END CATCH
END





--select * from app_config_param

--select *from data_update_log

--update data_update_log
--set project_id = 1,
--record_status = 'Uploaded',
--datafor_date_time = '2018-10-10 23:00:00.000'
--where id =23


--select * from cash_balance_file_alb where isValidRecord is not null



--where created_on = '2018-10-08 20:20:43.000'

--update cash_balance_file_alb
--set record_status = 'Uploaded'

--DECLARE @out varchar(50)
--EXEC [uspDataValidationALB] '2018-10-10 23:00:00.000','1','Uploaded','afasf','sa',@out OUTPUT
--SELECT @out

--select * from app_config_param where sequence = 50001