

	ALTER TABLE distribution_planning_detail
    ADD sol_id_cypher_code int NULL;

	ALTER TABLE distribution_planning_detail
    ADD cypher_code_date int NULL;

	ALTER TABLE distribution_planning_detail
    ADD cypher_code_day int NULL;

	ALTER TABLE distribution_planning_detail
    ADD cypher_code_month int NULL;

	ALTER TABLE distribution_planning_detail
    ADD cypher_code_year int NULL;

	ALTER TABLE distribution_planning_detail
    ADD cypher_code_amount int NULL;

	ALTER TABLE distribution_planning_detail
    ADD final_cypher_Code int NULL;

 