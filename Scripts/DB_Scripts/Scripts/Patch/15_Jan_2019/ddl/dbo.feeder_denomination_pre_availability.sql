/****** Object:  Table [dbo].[feeder_denomination_pre_availability]    Script Date: 15-01-2019 18:18:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[feeder_denomination_pre_availability](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[project_id] [nvarchar](15) NULL,
	[bank_code] [nvarchar](20) NULL,
	[feeder_branch_code] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[is_50_available] [nvarchar](10) NULL,
	[is_100_available] [nvarchar](10) NULL,
	[is_200_available] [nvarchar](10) NULL,
	[is_500_available] [nvarchar](10) NULL,
	[is_2000_available] [nvarchar](10) NULL,
	[from_date] [datetime] NULL,
	[to_date] [datetime] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[modified_by] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
	)

