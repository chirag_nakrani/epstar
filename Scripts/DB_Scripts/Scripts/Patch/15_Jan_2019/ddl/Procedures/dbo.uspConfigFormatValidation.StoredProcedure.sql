/****** Object:  StoredProcedure [dbo].[uspConfigFormatValidation]    Script Date: 15-01-2019 18:37:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspConfigFormatValidation]
	@file_type varchar(50),@ColumnNames varchar(max)
AS
----------Declaring variables to capture results------------------
BEGIN
	DECLARE @MetadataHeaders nvarchar(max)
	DECLARE @FileHeader nvarchar(max)
	DECLARE @IsRequired nvarchar(max)
	DECLARE @splitcolumncount int
	DECLARE @requiredcolcount int = 0
	DECLARE @Counter int = 0
	DECLARE @output varchar(50)
	DECLARE @MetaDataColumnCount int --to store no of columns in metadatatablemaster for particular file_type and storing the count into variable
	
	SET @MetaDataColumnCount = (SELECT count(1) FROM MetaDataTableMaster where file_type = @file_type and IsRequired = 'True')
	SET @splitcolumncount = (SELECT count(1) FROM fn_split(@ColumnNames, '+')) --Finding the total count of columns from input columnstring 
	--print (@MetaDataColumnCount)
	if (@MetaDataColumnCount > 0) --checking if there is anyrecord present for that particular file_type
		BEGIN
			DECLARE cursor1 CURSOR READ_ONLY --it is used for fetching the data to loop through it
				FOR
					SELECT columnHeaders, IsRequired FROM MetaDataTableMaster WHERE file_type = @file_type
				OPEN cursor1 --looping through the data
					FETCH NEXT FROM cursor1 into @MetadataHeaders, @IsRequired --Fetching the first record
						WHILE @@FETCH_STATUS = 0
							BEGIN
								DECLARE cursor2 cursor READ_ONLY  --this cursor is used for looping through the input data 
									FOR
										SELECT ColumnHeaders FROM fn_split(@ColumnNames,'+') OPTION (MAXRECURSION 1000)
										OPEN cursor2 
											FETCH NEXT FROM cursor2 INTO @FileHeader
												WHILE @@FETCH_STATUS = 0
													BEGIN
														--select @FileHeader
														--select @MetaDataHeaders
														IF(ltrim(rtrim(@FileHeader)) = ltrim(rtrim(@MetaDataHeaders)) and @IsRequired = 'True')
															BEGIN
																SET @Counter = @Counter + 1
															END
														FETCH NEXT FROM cursor2 into @FileHeader
													END
										CLOSE cursor2
										DEALLOCATE cursor2
					FETCH NEXT FROM cursor1 INTO @MetaDataHeaders,@IsRequired
							END
				CLOSE cursor1
			DEALLOCATE cursor1

		
		------ Comparing output from both variables-----------------
		IF( @Counter = @MetaDataColumnCount)
			BEGIN
			DECLARE cursor1 CURSOR READ_ONLY
			FOR
				SELECT columnHeaders 
				FROM MetaDataTableMaster
				WHERE file_type=@file_type
		
				OPEN cursor1
		------------fetching first record------------------
				FETCH NEXT FROM cursor1 INTO @MetaDataHeaders
					WHILE @@FETCH_STATUS = 0 
					BEGIN
							DECLARE cursor2 cursor Read_Only
							For
								SELECT ColumnHeaders FROM fn_split(@ColumnNames,'+') OPTION (MAXRECURSION 1000)
								OPEN cursor2
									FETCH NEXT FROM cursor2 INTO @FileHeader
									
										WHILE @@FETCH_STATUS=0
											BEGIN
											
											IF(charindex(',',@MetaDataHeaders)>1)
											BEGIN
										
												IF(charindex(@FileHeader,@MetaDataHeaders)>=1 )
												BEGIN
													SET @requiredcolcount = @requiredcolcount + 1
													
												END
											END
											
											IF(ltrim(rtrim(@FileHeader)) = ltrim(rtrim(@MetaDataHeaders)))
												BEGIN
												
												SET @requiredcolcount = @requiredcolcount + 1
												
												END
											FETCH NEXT FROM cursor2 INTO @FileHeader
								END
								CLOSE cursor2 
								DEALLOCATE cursor2
								FETCH NEXT FROM cursor1 INTO @MetaDataHeaders	
					END
			CLOSE cursor1 
		DEALLOCATE cursor1--SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Success')

			IF(@requiredcolcount = @splitcolumncount)
			BEGIN
			SET @output = ('S100')
			SELECT @output
			END
			Else
			BEGIN
				SET @output = ('E100')
				SELECT @output
			END

			END
		Else
			BEGIN
				SET @output = ( 'E100')
				SELECT @output
			END
	END
ELSE
		BEGIN
		INSERT INTO error_log (errorMessage,errorProcedure,errorSeverity) values ('Column information not defined in Meta Data Table','uspMasterFormatValidation',16)

		SET @output = ('E100')
		SELECT @output
		END
END

