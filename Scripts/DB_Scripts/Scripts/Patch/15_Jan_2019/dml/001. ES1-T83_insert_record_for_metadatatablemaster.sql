INSERT [dbo].[MetaDataTableMaster] ([file_type], [columnHeaders], [IsRequired], [table_column], [display_column_name], [table_name]) 
VALUES (N'CYPHERCODE', N'Category', N'True', N'category', N'Category', N'cypher_code')

INSERT [dbo].[MetaDataTableMaster] ([file_type], [columnHeaders], [IsRequired], [table_column], [display_column_name], [table_name]) 
VALUES (N'CYPHERCODE', N'Value', N'True', N'value', N'Value', N'cypher_code')

INSERT [dbo].[MetaDataTableMaster] ([file_type], [columnHeaders], [IsRequired], [table_column], [display_column_name], [table_name]) 
VALUES (N'CYPHERCODE', N'Cypher_Code', N'True', N'cypher_code', N'Cypher Code', N'cypher_code')

INSERT [dbo].[MetaDataTableMaster] ([file_type], [columnHeaders], [IsRequired], [table_column], [display_column_name], [table_name]) 
VALUES (N'CYPHERCODE', N'Project_ID', N'True', N'project_id', N'Project ID', N'cypher_code')

INSERT [dbo].[MetaDataTableMaster] ([file_type], [columnHeaders], [IsRequired], [table_column], [display_column_name], [table_name]) 
VALUES (N'CYPHERCODE', N'Bank_Code', N'True', N'bank_code', N'Bank Code', N'cypher_code')