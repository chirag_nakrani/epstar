/****** Object:  StoredProcedure [dbo].[usp_indent_pre_qualify_atm]    Script Date: 3/27/2019 5:15:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_indent_pre_qualify_atm]
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @pendingstatus varchar(50) = 'Approval Pending'
	DECLARE @out varchar(50)
	DECLARE @errorcode nvarchar(max)
	DECLARE @consideredfault varchar(100)= 'Any Cassette Faulty'
	DECLARE @date varchar(50) = (SELECT CONVERT(VARCHAR(8), GETDATE(), 8) 'hh:mm:ss')
	DECLARE @date_str varchar(50) = (SELECT SUBSTRING(REPLACE(@date,':',''),2,6))
	DECLARE @nextval varchar(50) = cast(next value for generate_ref_seq as varchar(50))
	DECLARE @referenceid varchar(100) = (select 'C' + @nextval + @date_str)

	drop table if exists #tempNewRecords

	SELECT qa.atmid,qa.site_code,qa.bankname,qa.fault_description,qa.record_status--,ipq.from_date,ipq.to_date
	INTO #tempNewRecords
	from ims_master qa 		
	where qa.fault_description IN ( @consideredfault) 
	AND CAST(datafor_date_time as date)  =  (SELECT CAST(MAX(datafor_date_time) as DATE) FROM ims_master where record_status = 'Active')
	and record_status = 'Active'	
	--select * from #tempNewRecords
    insert into #tempNewRecords(atmid,site_code,bankname,fault_description,record_status)
    select  atm_id,site_code,bank_code,'Tech Live but no Cash Live' as fault_description, record_status from atm_master where record_status = 'Active' and tech_live_date is not null and cash_live_date is null

	IF EXISTS (	
				SELECT 1 
				FROM #tempNewRecords 				
			  )
				BEGIN
					UPDATE ipq
					SET record_status = 'Deleted'			
					FROM indent_pre_qualify_atm ipq
					JOIN #tempNewRecords ar
					ON ar.atmid = ipq.atm_id AND ar.fault_description = ipq.category
					AND ipq.record_status = @pendingstatus AND (@timestamp_date BETWEEN ipq.from_date AND ipq.to_date)

					IF not exists (
									SELECT 1 FROM indent_pre_qualify_atm 
									WHERE record_status = 'Approval Pending'
									AND created_reference_id <> @referenceid
									)
								begin 
								
									UPDATE data_update_log_master
									SET record_status = 'Deleted'
									WHERE record_status = @pendingstatus
									and data_for_type = 'INDENT_PRE_QUALIFY_ATM'
									AND created_reference_id <> @referenceid
								end
				END
	
	IF	EXISTS (
					SELECT 1 FROM #tempNewRecords pr
					JOIN indent_pre_qualify_atm ipq  
					ON pr.atmid = ipq.atm_id
					AND pr.site_code = ipq.site_code
					and pr.fault_description = ipq.category		
					WHERE ipq.record_status = 'Active'
					AND (@timestamp_date BETWEEN from_date AND to_date)
				)
				BEGIN

					select * into #PreviouslyActiveRecords
					from ims_master 
					where atmid 
					not in (
								select atm_id from indent_pre_qualify_atm where record_status = 'Active' AND (@timestamp_date BETWEEN from_date AND to_date)
							)
					AND fault_description = @consideredfault
					AND CAST(datafor_date_time as date)  =  CAST(DATEADD(dd,-1,GETDATE()) as date)
					and record_status = 'Active'	
				
					INSERT INTO 
					indent_pre_qualify_atm (atm_id,site_code,is_qualified,category,from_date,to_date,record_status,created_reference_id)
					SELECT atmid,site_code,NULL,fault_description,@timestamp_date,@timestamp_date+1,@pendingstatus,@referenceid
					FROM #PreviouslyActiveRecords 
					WHERE record_status = 'Active' 
					AND fault_description = @consideredfault

				END			
		ELSE
			BEGIN
				select * into #newRecords
				from ims_master 
				where atmid 
				not in (
							select atm_id from indent_pre_qualify_atm where record_status <> 'Active' AND (@timestamp_date NOT BETWEEN from_date AND to_date)
						)
				AND fault_description = @consideredfault
				AND CAST(datafor_date_time as date)  =  CAST(DATEADD(dd,-1,GETDATE()) as date)
				and record_status = 'Active'	
			
				INSERT INTO 
				indent_pre_qualify_atm (atm_id,site_code,is_qualified,category,from_date,to_date,record_status,created_reference_id)
				SELECT atmid,site_code,NULL,fault_description,@timestamp_date,@timestamp_date+1,@pendingstatus,@referenceid
				FROM #newRecords 
				WHERE record_status = 'Active' 
				AND fault_description = @consideredfault
			END

			IF EXISTS (	
						SELECT 1 FROM indent_pre_qualify_atm
						WHERE created_reference_id = @referenceid
					  )
			BEGIN
				

				INSERT INTO data_update_log_master (data_for_type,record_status,created_reference_id,date_created)
				VALUES ('INDENT_PRE_QUALIFY_ATM',@pendingstatus,@referenceid,@timestamp_date)


			END


		END

