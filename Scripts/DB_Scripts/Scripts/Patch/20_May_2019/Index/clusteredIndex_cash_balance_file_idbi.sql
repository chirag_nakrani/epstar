CREATE CLUSTERED INDEX [ClusteredIndex_cash_balance_file_idbi] ON [dbo].[cash_balance_file_idbi]
(
	[id] ASC,
	[termid] ASC,
	[record_status] ASC,
	[created_reference_id] ASC,
	[datafor_date_time] ASC
)

