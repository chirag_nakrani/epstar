/****** Object:  StoredProcedure [dbo].[usp_indent_calculation_update]    Script Date: 25-12-2018 12:23:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC [usp_indent_revision_request] '2018-11-22','CAB/GOREGAON/Securevalue India Ltd/20190103135022','MOF','CAB',NULL,'Malay','abc223'


--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: Calculation of Indent for the indentdate @dateT
 --- =========================================================================

 -- On failure: E109
 -- On Success: S105
 -- Pick up status for 'Approved'

CREATE PROCEDURE [dbo].[usp_indent_revision_request]
 (		
        @dateT DATETIME,		
		@active_indent_code VARCHAR(max),
		@project_id  VARCHAR(50),
		@bank_code VARCHAR(50),
		@feeder_branch_code VARCHAR(50),
		@cur_user nvarchar(50),
		@created_reference_id nvarchar(50)
 )

AS 
BEGIN
	BEGIN TRY
	-- Temp_variable to calculate indent
	--DECLARE @dateT DATETIME = '2018-12-26'

	--DECLARE @dateT DATETIME		= '2018-11-22'							
	--DECLARE @active_indent_code VARCHAR(max) = 'ALB/MOF/DEORH/CMS Info Systems Ltd./20190103161225'
	--DECLARE @project_id  VARCHAR(50) = 'MOF'
	--DECLARE @bank_code VARCHAR(50) = 'CBI'
	--DECLARE @feeder_branch_code VARCHAR(50) = null
	--DECLARE @cur_user nvarchar(50) = 'SA'
	--DECLARE @created_reference_id nvarchar(50) = 'abc'
	declare @timestamp_date datetime =  DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50) 
	DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
	declare @Execution_log_str nvarchar(max)
	DECLARE @outputVal varchar(100)
	declare @feederbranchcode varchar(max)
	
	
	DECLARE @forecast_amt BIGINT
	
	set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@dateT as nvarchar(50)) 
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Execution of [dbo].[usp_indent_calculation_SIT] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		--DECLARE @dateT DATETIME
		--SET     @dateT ='2018-11-22'

		DECLARE @MaxDestinationDate DATE
		DECLARE @dispenseformula NVARCHAR(50)
		DECLARE @confidence_factor NVARCHAR(50)
		DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		DECLARE @denomination_wise_round_off_500 int
		DECLARE @denomination_wise_round_off_2000 int
		DECLARE @denomination_wise_round_off_100 INT
		declare @default_avg_dispense int
		
		
		Select	@confidence_factor = confidence_factor
		, @dispenseformula = dispenseformula
		, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		,@default_avg_dispense = default_average_dispense
		 from system_settings where record_status = 'Active'


		 DROP TABLE IF EXISTS #dist_with_cra
		 select * into #dist_with_cra from distribution_planning_detail where indent_code = @active_indent_code

		-- select * from distribution_planning_detail
		 --CAB/GOREGAON/Securevalue India Ltd/20190103135022

		 DROP TABLE IF EXISTS #feeder_level_forecast
		 select * into #feeder_level_forecast from distribution_planning_master where indent_code = @active_indent_code
		-- SELECT * FROM #feeder_level_forecast
		
		-- SELECT * FROM distribution_planning_master where indent_code = 'CAB/GOREGAON/Securevalue India Ltd/20190103135022'
		 
		 --project_id, bank_code, feeder_branch_code,total_amount_available,available_100_amount
		 --,available_200_amount,available_500_amount,available_2000_amount,is_deno_wise_cash_available
		 DROP TABLE IF EXISTS #cash_pre_availability
		 select	  project_id
				, bank_code
				, feeder_branch_code
				, total_amount_available
				, available_100_amount
				, available_200_amount
				, available_500_amount
				, available_2000_amount
				, case when (available_100_amount + available_200_amount + available_500_amount + available_2000_amount) = total_amount_available then
			1 else 0 end as is_deno_wise_cash_available  
		 into #cash_pre_availability 
		 from indent_revision_request where indent_order_number = @active_indent_code and record_status = 'Approved'
		 
		 --select * from #cash_pre_availability
		 
			DROP TABLE IF EXISTS #temp_cash_pre_availability
			
			CREATE TABLE #temp_cash_pre_availability
			(
				project_id		nvarchar(50)
			,	bank_code		nvarchar(50)
			,	atmid		nvarchar(50)
			,	total_opening_remaining_available_amount		bigint
			,	opening_remaining_available_amount_100		bigint
			,	opening_remaining_available_amount_200		bigint
			,	opening_remaining_available_amount_500		bigint
			,	opening_remaining_available_amount_2000		bigint
			,	original_total_forecasted_amt		bigint
			,	original_forecasted_amt_100		bigint
			,	original_forecasted_amt_200		bigint
			,	original_forecasted_amt_500		bigint
			,	original_forecasted_amt_2000		bigint
			,	morning_balance_100		bigint
			,	morning_balance_200		bigint
			,	morning_balance_500		bigint
			,	morning_balance_2000		bigint
			,	total_morning_balance		bigint
			,	cassette_100_count_original		bigint
			,	cassette_200_count_original		bigint
			,	cassette_500_count_original		bigint
			,	cassette_2000_count_original		bigint
			,	cassette_100_brand_capacity		bigint
			,	cassette_200_brand_capacity		bigint
			,	cassette_500_brand_capacity		bigint
			,	cassette_2000_brand_capacity		bigint
			,	total_capacity_amount_100		bigint
			,	total_capacity_amount_200		bigint
			,	total_capacity_amount_500		bigint
			,	total_capacity_amount_2000		bigint
			,	denomination_100_max_capacity_percentage		bigint
			,	denomination_200_max_capacity_percentage		bigint
			,	denomination_500_max_capacity_percentage		bigint
			,	denomination_2000_max_capacity_percentage		bigint
			,	max_amt_allowed_100		bigint
			,	max_amt_allowed_200		bigint
			,	max_amt_allowed_500		bigint
			,	max_amt_allowed_2000		bigint
			,	denomination_wise_round_off_100		bigint
			,	denomination_wise_round_off_200		bigint
			,	denomination_wise_round_off_500		bigint
			,	denomination_wise_round_off_2000		bigint
			,	tentative_loading_100		bigint
			,	tentative_loading_200		bigint
			,	tentative_loading_500		bigint
			,	tentative_loading_2000		bigint
			,	rounded_tentative_loading_100		bigint
			,	rounded_tentative_loading_200		bigint
			,	rounded_tentative_loading_500		bigint
			,	rounded_tentative_loading_2000		bigint
			,	deno_100_priority		bigint
			,	deno_200_priority		bigint
			,	deno_500_priority		bigint
			,	deno_2000_priority		bigint
			,	is_deno_wise_cash_available		int
			,	priority_1_is_denomination_100		int
			,	priority_1_is_denomination_200		int
			,	priority_1_is_denomination_500		int
			,	priority_1_is_denomination_2000		int
			,	priority_1_is_remaining_amount_available_100		int
			,	priority_1_is_remaining_amount_available_200		int
			,	priority_1_is_remaining_amount_available_500		int
			,	priority_1_is_remaining_amount_available_2000		int
			,	priority_1_is_remaining_capacity_available_100		int
			,	priority_1_is_remaining_capacity_available_200		int
			,	priority_1_is_remaining_capacity_available_500		int
			,	priority_1_is_remaining_capacity_available_2000		int
			,	priority_1_loading_amount_100		bigint
			,	priority_1_loading_amount_200		bigint
			,	priority_1_loading_amount_500		bigint
			,	priority_1_loading_amount_2000		bigint
			,	priority_2_is_denomination_100		int
			,	priority_2_is_denomination_200		int
			,	priority_2_is_denomination_500		int
			,	priority_2_is_denomination_2000		int
			,	priority_2_is_remaining_amount_available_100		int
			,	priority_2_is_remaining_amount_available_200		int
			,	priority_2_is_remaining_amount_available_500		int
			,	priority_2_is_remaining_amount_available_2000		int
			,	priority_2_is_remaining_capacity_available_100		int
			,	priority_2_is_remaining_capacity_available_200		int
			,	priority_2_is_remaining_capacity_available_500		int
			,	priority_2_is_remaining_capacity_available_2000		int
			,	priority_2_loading_amount_100		bigint
			,	priority_2_loading_amount_200		bigint
			,	priority_2_loading_amount_500		bigint
			,	priority_2_loading_amount_2000		bigint
			,	priority_3_is_denomination_100		int
			,	priority_3_is_denomination_200		int
			,	priority_3_is_denomination_500		int
			,	priority_3_is_denomination_2000		int
			,	priority_3_is_remaining_amount_available_100		int
			,	priority_3_is_remaining_amount_available_200		int
			,	priority_3_is_remaining_amount_available_500		int
			,	priority_3_is_remaining_amount_available_2000		int
			,	priority_3_is_remaining_capacity_available_100		int
			,	priority_3_is_remaining_capacity_available_200		int
			,	priority_3_is_remaining_capacity_available_500		int
			,	priority_3_is_remaining_capacity_available_2000		int
			,	priority_3_loading_amount_100		bigint
			,	priority_3_loading_amount_200		bigint
			,	priority_3_loading_amount_500		bigint
			,	priority_3_loading_amount_2000		bigint
			,	priority_4_is_denomination_100		int
			,	priority_4_is_denomination_200		int
			,	priority_4_is_denomination_500		int
			,	priority_4_is_denomination_2000		int
			,	priority_4_is_remaining_amount_available_100		int
			,	priority_4_is_remaining_amount_available_200		int
			,	priority_4_is_remaining_amount_available_500		int
			,	priority_4_is_remaining_amount_available_2000		int
			,	priority_4_is_remaining_capacity_available_100		int
			,	priority_4_is_remaining_capacity_available_200		int
			,	priority_4_is_remaining_capacity_available_500		int
			,	priority_4_is_remaining_capacity_available_2000		int
			,	priority_4_loading_amount_100		bigint
			,	priority_4_loading_amount_200		bigint
			,	priority_4_loading_amount_500		bigint
			,	priority_4_loading_amount_2000		bigint
			,	loading_amount_100		bigint
			,	loading_amount_200		bigint
			,	loading_amount_500		bigint
			,	loading_amount_2000		bigint
			,	total_loading_amount		bigint
			,	remaining_capacity_amount_100		bigint
			,	remaining_capacity_amount_200		bigint
			,	remaining_capacity_amount_500		bigint
			,	remaining_capacity_amount_2000		bigint
			,   closing_remaining_available_amount_100      bigint
			,   closing_remaining_available_amount_200		bigint
			,   closing_remaining_available_amount_500		bigint
			,   closing_remaining_available_amount_2000		bigint
			,   total_closing_remaining_available_amount	bigint
			
			,	total_forecasted_remaining_amt		bigint
			)
			
			DROP TABLE IF EXISTS #temp_feeder_forecast
			CREATE TABLE #temp_feeder_forecast
			(
				project_id							nvarchar(50)
			,	bank_code							nvarchar(50)
			,	feeder_branch_code					nvarchar(50)
			,	is_deno_wise_cash_available			INT    NULL
			,	total_remaining_available_amount	BIGINT NULL
			,   remaining_avail_100					BIGINT NULL
			,   remaining_avail_200					BIGINT NULL
			,   remaining_avail_500					BIGINT NULL
			,   remaining_avail_2000				BIGINT NULL
			
			)
			
		 -- Run cursor for feeder level from #cash_pre_availability
		 DECLARE cursor1 CURSOR READ_ONLY
		 FOR
		 SELECT project_id, bank_code, feeder_branch_code,total_amount_available,available_100_amount,available_200_amount,available_500_amount,available_2000_amount,is_deno_wise_cash_available
		 FROM #cash_pre_availability 
		 ORDER BY feeder_branch_code

		-- SELECT * FROM #cash_pre_availability

	declare @total_remaining_available_amount BIGINT
	, @remaining_avail_100 BIGINT
	, @remaining_avail_200 BIGINT
	, @remaining_avail_500 BIGINT
	, @remaining_avail_2000 BIGINT
	, @total_original_available_amount BIGINT
	, @original_avail_100 BIGINT
	, @original_avail_200 BIGINT
	, @original_avail_500 BIGINT
	, @original_avail_2000 BIGINT
	, @loading_amount_100 BIGINT
	, @loading_amount_200 BIGINT
	, @loading_amount_500 BIGINT
	, @loading_amount_2000 BIGINT
	, @is_deno_wise_cash_available INT
	, @projectid nvarchar(50)
	, @bankcode nvarchar(50)
	OPEN cursor1

	FETCH NEXT FROM cursor1 INTO @projectid,@bankcode, @feederbranchcode, @total_original_available_amount, @original_avail_100, @original_avail_200, @original_avail_500, @original_avail_2000, @is_deno_wise_cash_available
    
    WHILE @@FETCH_STATUS = 0  
    BEGIN		
				SET @total_remaining_available_amount	= @total_original_available_amount
				SET @remaining_avail_100				= @original_avail_100
				SET @remaining_avail_200				= @original_avail_200
				SET @remaining_avail_500				= @original_avail_500
				SET @remaining_avail_2000				= @original_avail_2000
				

				--SELECT @total_remaining_available_amount	 
				--SELECT @remaining_avail_100				
				--SELECT @remaining_avail_200				
				--SELECT @remaining_avail_500				
				--SELECT @remaining_avail_2000				
				--SELECT 
				--SELECT 

						------ While Begin (cursor1 )]
				--DECLARE @availableAmount,@avail_100,@avail_200,@avail_500,@avail_2000

				--IF (@total_remaining_available_amount > 0  AND  @total_remaining_available_amount = @remaining_avail_100 + @remaining_avail_200 + @remaining_avail_500 + @remaining_avail_2000)
				--BEGIN		
						---- Available amount > 0 Begin
					DECLARE cursor2 cursor Read_Only
					For
					
					-- New
					SELECT	
							 dist.atm_id,
							 CASE 
								WHEN atm_band = 'Platinum'
								THEN 1
								WHEN atm_band = 'Gold'
								THEN 2
								WHEN CashOut = 1
								THEN 3
								WHEN atm_band = 'Silver'
								THEN 4
								ELSE 5
							END as atm_priority 						
							, denomination_100_max_capacity_percentage
							, denomination_200_max_capacity_percentage
							, denomination_500_max_capacity_percentage
							, denomination_2000_max_capacity_percentage
							, morning_balance_100
							, morning_balance_200
							, morning_balance_500
							, morning_balance_2000
							, total_morning_balance
							, rounded_amount_100  as forecasted_amt_100		 
							, rounded_amount_200  as forecasted_amt_200	
							, rounded_amount_500  as forecasted_amt_500	
							, rounded_amount_2000 as forecasted_amt_2000	
							, (rounded_amount_100 + rounded_amount_200 + rounded_amount_500 + rounded_amount_2000) as total_forecasted_amt
							--, cassette_50_count_original					
							, cassette_100_count_original 
							, cassette_200_count_original 
							, cassette_500_count_original 
							, cassette_2000_count_original
							, deno_100_priority
							, deno_200_priority
							, deno_500_priority
							, deno_2000_priority
							, cassette_100_brand_capacity
							, cassette_200_brand_capacity
							, cassette_500_brand_capacity
							, cassette_2000_brand_capacity
							
					 from #dist_with_cra dist
					LEFT JOIN atm_master atm
					on atm.atm_id =  dist.atm_id and atm.site_code = dist.site_code 
					and dist.project_id = @projectid and dist.bank_code = @bankcode and feeder_branch_code = @feederbranchcode
					ORDER BY atm_priority
					
					--SELECT * FROM #dist_with_cra where feeder_branch_code = 'KHADAKI'
					declare @atmid varchar(50)
							, @atm_priority INT												
							, @denomination_100_max_capacity_percentage bigint
							, @denomination_200_max_capacity_percentage bigint
							, @denomination_500_max_capacity_percentage bigint
							, @denomination_2000_max_capacity_percentage bigint
							, @morning_balance_100 bigint
							, @morning_balance_200 bigint
							, @morning_balance_500 bigint
							, @morning_balance_2000 bigint
							, @total_morning_balance BIGINT
							, @original_forecasted_amt_100  bigint
							, @original_forecasted_amt_200  bigint
							, @original_forecasted_amt_500  bigint
							, @original_forecasted_amt_2000 bigint
							, @original_total_forecasted_amt BIGINT
							--, @cassette_50_count_original   BIGINT							
							, @cassette_100_count_original 	bigint
							, @cassette_200_count_original 	bigint
							, @cassette_500_count_original 	bigint
							, @cassette_2000_count_original	bigint		
							, @deno_100_priority   BIGINT
							, @deno_200_priority	  BIGINT
							, @deno_500_priority	  BIGINT
							, @deno_2000_priority  BIGINT
							, @cassette_100_brand_capacity 	bigint		
							, @cassette_200_brand_capacity	bigint		
							, @cassette_500_brand_capacity	bigint		
							, @cassette_2000_brand_capacity	bigint		
				    OPEN cursor2
					FETCH NEXT FROM cursor2 
						  INTO  							
							 @atmid	
							 ,@atm_priority						
							, @denomination_100_max_capacity_percentage
							, @denomination_200_max_capacity_percentage
							, @denomination_500_max_capacity_percentage
							, @denomination_2000_max_capacity_percentage
							, @morning_balance_100
							, @morning_balance_200
							, @morning_balance_500
							, @morning_balance_2000
							, @total_morning_balance
							, @original_forecasted_amt_100
							, @original_forecasted_amt_200
							, @original_forecasted_amt_500
							, @original_forecasted_amt_2000
							, @original_total_forecasted_amt
							--, @cassette_50_count_original  
							, @cassette_100_count_original 
							, @cassette_200_count_original 
							, @cassette_500_count_original 
							, @cassette_2000_count_original
							, @deno_100_priority
							, @deno_200_priority
							, @deno_500_priority
							, @deno_2000_priority
							, @cassette_100_brand_capacity
							, @cassette_200_brand_capacity
							, @cassette_500_brand_capacity
							, @cassette_2000_brand_capacity
					WHILE @@FETCH_STATUS=0
					BEGIN			---- Cursor 2 Begin
							--SELECT 'Hi'
						DECLARE @forecasted_remaining_amt_100  bigint
							, @forecasted_remaining_amt_200  bigint
							, @forecasted_remaining_amt_500  bigint
							, @forecasted_remaining_amt_2000 bigint

							, @total_forecasted_remaining_amt BIGINT
							, @remaining_capacity_amount_100  bigint
							, @remaining_capacity_amount_200  bigint
							, @remaining_capacity_amount_500  bigint
							, @remaining_capacity_amount_2000 bigint
							, @total_remaining_capacity_amount BIGINT

							, @total_opening_remaining_available_amount BIGINT
							, @opening_remaining_available_amount_100 BIGINT
							, @opening_remaining_available_amount_200 BIGINT
							, @opening_remaining_available_amount_500 BIGINT
							, @opening_remaining_available_amount_2000 BIGINT


						SET  @total_opening_remaining_available_amount = @total_remaining_available_amount
						SET	 @opening_remaining_available_amount_100 =  @remaining_avail_100
						SET	 @opening_remaining_available_amount_200 =  @remaining_avail_200
						SET	 @opening_remaining_available_amount_500 = @remaining_avail_500
						SET	 @opening_remaining_available_amount_2000 = @remaining_avail_2000

						set @forecasted_remaining_amt_100 = @original_forecasted_amt_100
						SET	@forecasted_remaining_amt_200 = @original_forecasted_amt_200
						SET	@forecasted_remaining_amt_500 = @original_forecasted_amt_500
						SET	@forecasted_remaining_amt_2000 = @original_forecasted_amt_2000
						SET @total_forecasted_remaining_amt = @original_total_forecasted_amt
						SET @remaining_capacity_amount_100   = @remaining_capacity_amount_100
						SET @remaining_capacity_amount_200   = @remaining_capacity_amount_200
						SET @remaining_capacity_amount_500  = @remaining_capacity_amount_500
						SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000
						SET @total_remaining_capacity_amount = @total_remaining_capacity_amount

						-- GET TOTAL CAPACITY AMOUNT
						DECLARE @total_capacity_amount_100 bigint
							, @total_capacity_amount_200 bigint
							, @total_capacity_amount_500 bigint 
							, @total_capacity_amount_2000 bigint

						SET @total_capacity_amount_100 = @cassette_100_count_original * @cassette_100_brand_capacity * 100
						SET @total_capacity_amount_200 = @cassette_200_count_original * @cassette_200_brand_capacity * 200
						SET @total_capacity_amount_500 = @cassette_500_count_original * @cassette_500_brand_capacity * 500
						SET @total_capacity_amount_2000 = @cassette_2000_count_original * @cassette_2000_brand_capacity * 2000
						
						
						-- GET TOTAL CAPACITY AMOUNT LIMIT TO CASSETTE CAPACITY PERCENTAGE

						DECLARE @max_amt_allowed_100 BIGINT,
								@max_amt_allowed_200 BIGINT,
								@max_amt_allowed_500 BIGINT,
								@max_amt_allowed_2000 BIGINT

						SET @max_amt_allowed_100 =  @total_capacity_amount_100 * @denomination_100_max_capacity_percentage / 100
						SET @max_amt_allowed_200 =  @total_capacity_amount_200 * @denomination_200_max_capacity_percentage / 100
						SET @max_amt_allowed_500 =  @total_capacity_amount_500 * @denomination_500_max_capacity_percentage / 100
						SET @max_amt_allowed_2000 =  @total_capacity_amount_2000 * @denomination_2000_max_capacity_percentage / 100

						-- 
						--DECLARE @max_amt_capacity_100 BIGINT,
						--		@max_amt_capacity_200 BIGINT,
						--		@max_amt_capacity_500 BIGINT,
						--		@max_amt_capacity_2000 BIGINT

						---- Calculate Maximum capacity amount
						--set @max_amt_capacity_100 = @total_capacity_amount_100  *  @denomination_100_max_capacity_percentage / 100
						--set @max_amt_capacity_200 = @total_capacity_amount_200  *  @denomination_200_max_capacity_percentage / 100
						--set @max_amt_capacity_500 = @total_capacity_amount_500  *  @denomination_500_max_capacity_percentage / 100
						--set @max_amt_capacity_2000 = @total_capacity_amount_2000  *  @denomination_2000_max_capacity_percentage / 100						

						-- Calculate  Deduct Morning balance
						DECLARE @tentative_loading_100 BIGINT,
								@tentative_loading_200 BIGINT,
								@tentative_loading_500 BIGINT,
								@tentative_loading_2000 BIGINT

						SET @tentative_loading_100   =  @max_amt_allowed_100 - @morning_balance_100
						SET @tentative_loading_200   =  @max_amt_allowed_200 - @morning_balance_200
						SET @tentative_loading_500   =  @max_amt_allowed_500 - @morning_balance_500
						SET @tentative_loading_2000  =  @max_amt_allowed_2000 - @morning_balance_2000

						--- Calculate rounded tentative loading

						DECLARE @rounded_tentative_loading_100 BIGINT,
								@rounded_tentative_loading_200 BIGINT,
								@rounded_tentative_loading_500 BIGINT,
								@rounded_tentative_loading_2000 BIGINT

						SET @rounded_tentative_loading_100   =   @tentative_loading_100 - (@tentative_loading_100 % @denomination_wise_round_off_100 )
						SET @rounded_tentative_loading_200   =   @tentative_loading_200 - (@tentative_loading_200 % @denomination_wise_round_off_200 )
						SET @rounded_tentative_loading_500   =   @tentative_loading_500 - (@tentative_loading_500 % @denomination_wise_round_off_500 )
						SET @rounded_tentative_loading_2000   =   @tentative_loading_2000 - (@tentative_loading_2000 % @denomination_wise_round_off_2000 )

						DECLARE     @priority_1_is_denomination_100  BIGINT = 0,
									@priority_1_is_denomination_200 BIGINT = 0,
									@priority_1_is_denomination_500 BIGINT = 0,
									@priority_1_is_denomination_2000 BIGINT = 0,
									@priority_1_is_remaining_amount_available_100 BIGINT = 0,
									@priority_1_is_remaining_amount_available_200 BIGINT = 0,
									@priority_1_is_remaining_amount_available_500 BIGINT = 0,
									@priority_1_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_1_loading_amount_100 BIGINT = 0,
									@priority_1_loading_amount_200 BIGINT = 0,
									@priority_1_loading_amount_500 BIGINT = 0,
									@priority_1_loading_amount_2000 BIGINT = 0,
									@priority_2_is_denomination_100  BIGINT = 0,
									@priority_2_is_denomination_200 BIGINT = 0,
									@priority_2_is_denomination_500 BIGINT = 0,
									@priority_2_is_denomination_2000 BIGINT = 0,
									@priority_2_is_remaining_amount_available_100 BIGINT = 0,
									@priority_2_is_remaining_amount_available_200 BIGINT = 0,
									@priority_2_is_remaining_amount_available_500 BIGINT = 0,
									@priority_2_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_2_loading_amount_100 BIGINT = 0,
									@priority_2_loading_amount_200 BIGINT = 0,
									@priority_2_loading_amount_500 BIGINT = 0,
									@priority_2_loading_amount_2000 BIGINT = 0,
									@priority_3_is_denomination_100  BIGINT = 0,
									@priority_3_is_denomination_200 BIGINT = 0,
									@priority_3_is_denomination_500 BIGINT = 0,
									@priority_3_is_denomination_2000 BIGINT = 0,
									@priority_3_is_remaining_amount_available_100 BIGINT = 0,
									@priority_3_is_remaining_amount_available_200 BIGINT = 0,
									@priority_3_is_remaining_amount_available_500 BIGINT = 0,
									@priority_3_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_3_loading_amount_100 BIGINT = 0,
									@priority_3_loading_amount_200 BIGINT = 0,
									@priority_3_loading_amount_500 BIGINT = 0,
									@priority_3_loading_amount_2000 BIGINT = 0,
									@priority_4_is_denomination_100  BIGINT = 0,
									@priority_4_is_denomination_200 BIGINT = 0,
									@priority_4_is_denomination_500 BIGINT = 0,
									@priority_4_is_denomination_2000 BIGINT = 0,
									@priority_4_is_remaining_amount_available_100 BIGINT = 0,
									@priority_4_is_remaining_amount_available_200 BIGINT = 0,
									@priority_4_is_remaining_amount_available_500 BIGINT = 0,
									@priority_4_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_4_loading_amount_100 BIGINT = 0,
									@priority_4_loading_amount_200 BIGINT = 0,
									@priority_4_loading_amount_500 BIGINT = 0,
									@priority_4_loading_amount_2000 BIGINT = 0
							

						IF(@is_deno_wise_cash_available = 1)
						BEGIN
							-- CONTINUE FOR ALLOCATION LOGIC
							
							--SELECT 'total forecasted remaining amt' + cast(@total_forecasted_remaining_amt as nvarchar(max))
							select @deno_100_priority
							SELECT @deno_200_priority
							SELECT @deno_500_priority
							SELECT @deno_2000_priority
							--SELECT @total_forecasted_remaining_amt
							--SELECT @remaining_avail_200
							--SELECT @rounded_tentative_loading_200
							--SELECT @priority_1_max_loading_capacity_amount_200
							--SELECT @total_forecasted_remaining_amt
							select @total_forecasted_remaining_amt
							select @remaining_avail_100
							select @rounded_tentative_loading_100
							select @priority_1_max_loading_capacity_amount_100

							IF (@total_forecasted_remaining_amt > 0)
							BEGIN
								IF (@deno_100_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_100 = 1
											SET @priority_1_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_1_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_100 = @priority_1_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_100 = @priority_1_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_1_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_1_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 1)

								ELSE IF (@deno_200_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_200 = 1
											SET @priority_1_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_1_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_200 = @priority_1_max_loading_capacity_amount_200
												--SELECT @priority_1_loading_amount_200
											END	-- END of @remaining_avail_200 > @priority_1_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_200 = @remaining_avail_200
												--SELECT @priority_1_loading_amount_200
											END		-- END of else
											--SELECT @priority_1_loading_amount_200
											--SELECT @priority_2_loading_amount_200
											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_200 = @priority_1_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_1_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_1_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 1)
									ELSE IF (@deno_500_priority = 1)
									BEGIN
										SET @priority_1_is_denomination_500 = 1
										IF (@remaining_avail_500 > 0)
										BEGIN
											SET @priority_1_is_remaining_amount_available_500 = 1
											IF (@rounded_tentative_loading_500 > 0 )
											BEGIN
												SET @priority_1_is_remaining_capacity_available_500 = 1
												SET @priority_1_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
												IF @remaining_avail_500 > @priority_1_max_loading_capacity_amount_500
												BEGIN
													-- assigining amount as per available capacity
													SET @priority_1_loading_amount_500 = @priority_1_max_loading_capacity_amount_500
												END
												ELSE
												BEGIN
													--	When available amount is less than capacity then allocate only available amount 
													SET @priority_1_loading_amount_500 = @remaining_avail_500
												END

												--Loading for priority 1 done
												--deduct from available amount
												SET @loading_amount_500 = @priority_1_loading_amount_500
												SET @remaining_avail_500 = @remaining_avail_500 - @priority_1_loading_amount_500
												SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_500
												SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_500
												SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_500
												SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_1_loading_amount_500
											
											END	-- END OF  (@rounded_tentative_loading_500 > 0 )
										END		-- END of (@remaining_avail_500 > 0)
										END		-- END OF (@deno_500_priority = 1)
								ELSE IF (@deno_2000_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_2000 = 1
											SET @priority_1_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_1_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_2000 = @priority_1_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_2000 = @priority_1_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_1_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_1_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 1)
					
		  -------------------------------------- Checking for 2nd priority denomination --------------------------------- 
								IF (@deno_100_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_100 = 1
											SET @priority_2_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_2_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_100 = @priority_2_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_2_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_2_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_2_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 2)

								ELSE IF (@deno_200_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_200 = 1
											SET @priority_2_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_2_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_200 = @priority_2_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_2_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_2_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_2_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_2_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 2)
								ELSE IF (@deno_500_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_500 = 1
											SET @priority_2_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF @remaining_avail_500 > @priority_2_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_500 = @priority_2_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_2_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_2_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_2_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 2)
								ELSE IF (@deno_2000_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_2000 = 1
											SET @priority_2_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_2_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_2000 = @priority_2_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_2_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_2_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_2_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 2)
			
			
			--------------------------------- Checking for 3rd priority denomination --------------------------------- 
								IF (@deno_100_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_100 = 1
											SET @priority_3_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_3_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_100 = @priority_3_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 3 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_3_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_3_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_3_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 3)

								ELSE IF (@deno_200_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_200 = 1
											SET @priority_3_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_3_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_200 = @priority_3_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_3_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_3_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_3_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_3_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 3)
								ELSE IF (@deno_500_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_500 = 1
											SET @priority_3_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF @remaining_avail_500 > @priority_3_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_500 = @priority_3_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 3 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_3_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_3_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_3_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 3)
								ELSE IF (@deno_2000_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_2000 = 1
											SET @priority_3_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_3_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_2000 = @priority_3_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_3_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_3_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_3_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 3)
							
							
				---------------------------------------- Checking for 4th priority denomination --------------------------------- 
								IF (@deno_100_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_100 = 1
											SET @priority_4_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_4_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_100 = @priority_4_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 4 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_4_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_4_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_4_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 4)

								ELSE IF (@deno_200_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_200 = 1
											SET @priority_4_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_4_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_200 = @priority_4_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_4_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 4 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_4_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_4_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_4_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 4)
								ELSE IF (@deno_500_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_500 = 1
											SET @priority_4_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF @remaining_avail_500 > @priority_4_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_500 = @priority_4_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_4_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_4_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_4_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 4)
								ELSE IF (@deno_2000_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_2000 = 1
											SET @priority_4_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_4_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_2000 = @priority_4_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_4_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_4_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_4_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 4)
		
		
		
		
						INSERT INTO #temp_cash_pre_availability
							(
										project_id
									,	bank_code
									,	atmid
									,	total_opening_remaining_available_amount
									,	opening_remaining_available_amount_100
									,	opening_remaining_available_amount_200
									,	opening_remaining_available_amount_500
									,	opening_remaining_available_amount_2000
									,	original_total_forecasted_amt
									,	original_forecasted_amt_100
									,	original_forecasted_amt_200
									,	original_forecasted_amt_500
									,	original_forecasted_amt_2000
									,	morning_balance_100
									,	morning_balance_200
									,	morning_balance_500
									,	morning_balance_2000
									,	total_morning_balance
									,	cassette_100_count_original
									,	cassette_200_count_original
									,	cassette_500_count_original
									,	cassette_2000_count_original
									,	cassette_100_brand_capacity
									,	cassette_200_brand_capacity
									,	cassette_500_brand_capacity
									,	cassette_2000_brand_capacity
									,	total_capacity_amount_100
									,	total_capacity_amount_200
									,	total_capacity_amount_500
									,	total_capacity_amount_2000
									,	denomination_100_max_capacity_percentage
									,	denomination_200_max_capacity_percentage
									,	denomination_500_max_capacity_percentage
									,	denomination_2000_max_capacity_percentage
									,	max_amt_allowed_100
									,	max_amt_allowed_200
									,	max_amt_allowed_500
									,	max_amt_allowed_2000
									,	denomination_wise_round_off_100
									,	denomination_wise_round_off_200
									,	denomination_wise_round_off_500
									,	denomination_wise_round_off_2000
									,	tentative_loading_100
									,	tentative_loading_200
									,	tentative_loading_500
									,	tentative_loading_2000
									,	rounded_tentative_loading_100
									,	rounded_tentative_loading_200
									,	rounded_tentative_loading_500
									,	rounded_tentative_loading_2000
									,	deno_100_priority
									,	deno_200_priority
									,	deno_500_priority
									,	deno_2000_priority
									,	is_deno_wise_cash_available
									,	priority_1_is_denomination_100
									,	priority_1_is_denomination_200
									,	priority_1_is_denomination_500
									,	priority_1_is_denomination_2000
									,	priority_1_is_remaining_amount_available_100
									,	priority_1_is_remaining_amount_available_200
									,	priority_1_is_remaining_amount_available_500
									,	priority_1_is_remaining_amount_available_2000
									,	priority_1_is_remaining_capacity_available_100
									,	priority_1_is_remaining_capacity_available_200
									,	priority_1_is_remaining_capacity_available_500
									,	priority_1_is_remaining_capacity_available_2000
									,	priority_1_loading_amount_100
									,	priority_1_loading_amount_200
									,	priority_1_loading_amount_500
									,	priority_1_loading_amount_2000
									,	priority_2_is_denomination_100
									,	priority_2_is_denomination_200
									,	priority_2_is_denomination_500
									,	priority_2_is_denomination_2000
									,	priority_2_is_remaining_amount_available_100
									,	priority_2_is_remaining_amount_available_200
									,	priority_2_is_remaining_amount_available_500
									,	priority_2_is_remaining_amount_available_2000
									,	priority_2_is_remaining_capacity_available_100
									,	priority_2_is_remaining_capacity_available_200
									,	priority_2_is_remaining_capacity_available_500
									,	priority_2_is_remaining_capacity_available_2000
									,	priority_2_loading_amount_100
									,	priority_2_loading_amount_200
									,	priority_2_loading_amount_500
									,	priority_2_loading_amount_2000
									,	priority_3_is_denomination_100
									,	priority_3_is_denomination_200
									,	priority_3_is_denomination_500
									,	priority_3_is_denomination_2000
									,	priority_3_is_remaining_amount_available_100
									,	priority_3_is_remaining_amount_available_200
									,	priority_3_is_remaining_amount_available_500
									,	priority_3_is_remaining_amount_available_2000
									,	priority_3_is_remaining_capacity_available_100
									,	priority_3_is_remaining_capacity_available_200
									,	priority_3_is_remaining_capacity_available_500
									,	priority_3_is_remaining_capacity_available_2000
									,	priority_3_loading_amount_100
									,	priority_3_loading_amount_200
									,	priority_3_loading_amount_500
									,	priority_3_loading_amount_2000
									,	priority_4_is_denomination_100
									,	priority_4_is_denomination_200
									,	priority_4_is_denomination_500
									,	priority_4_is_denomination_2000
									,	priority_4_is_remaining_amount_available_100
									,	priority_4_is_remaining_amount_available_200
									,	priority_4_is_remaining_amount_available_500
									,	priority_4_is_remaining_amount_available_2000
									,	priority_4_is_remaining_capacity_available_100
									,	priority_4_is_remaining_capacity_available_200
									,	priority_4_is_remaining_capacity_available_500
									,	priority_4_is_remaining_capacity_available_2000
									,	priority_4_loading_amount_100
									,	priority_4_loading_amount_200
									,	priority_4_loading_amount_500
									,	priority_4_loading_amount_2000
									,	loading_amount_100
									,	loading_amount_200
									,	loading_amount_500
									,	loading_amount_2000
									,	total_loading_amount
									,	remaining_capacity_amount_100
									,	remaining_capacity_amount_200
									,	remaining_capacity_amount_500
									,	remaining_capacity_amount_2000
									,   closing_remaining_available_amount_100
									,   closing_remaining_available_amount_200
									,   closing_remaining_available_amount_500
									,   closing_remaining_available_amount_2000
									,   total_closing_remaining_available_amount									
									,	total_forecasted_remaining_amt
							)
							SELECT  
									    @project_id								                                                            as project_id								
								   , @bank_code									                                                            as bank_code									
								   , @atmid										                                                            as atmid										
									, @total_opening_remaining_available_amount																as total_opening_remaining_available_amount
								   , @opening_remaining_available_amount_100																as opening_remaining_available_amount_100	
								   , @opening_remaining_available_amount_200	                                                            as opening_remaining_available_amount_200	
								   , @opening_remaining_available_amount_500	                                                            as opening_remaining_available_amount_500	
								   , @opening_remaining_available_amount_2000	                                                            as opening_remaining_available_amount_2000	
								
								   , @original_total_forecasted_amt							                                                as original_total_forecasted_amt							
								   , @original_forecasted_amt_100				                                                            as original_forecasted_amt_100				
								   , @original_forecasted_amt_200                                                                           as original_forecasted_amt_200
								   , @original_forecasted_amt_500                                                                           as original_forecasted_amt_500
								   , @original_forecasted_amt_2000                                                                          as original_forecasted_amt_2000
								   , @morning_balance_100                                                                                   as morning_balance_100
								   , @morning_balance_200                                                                                   as morning_balance_200
								   , @morning_balance_500                                                                                   as morning_balance_500
								   , @morning_balance_2000                                                                                  as morning_balance_2000
								   , @total_morning_balance                                                                                 as total_morning_balance
								   , @cassette_100_count_original                                                                           as cassette_100_count_original
								   , @cassette_200_count_original                                                                           as cassette_200_count_original
								   , @cassette_500_count_original                                                                           as cassette_500_count_original
								   , @cassette_2000_count_original                                                                          as cassette_2000_count_original
								   , @cassette_100_brand_capacity                                                                           as cassette_100_brand_capacity
								   , @cassette_200_brand_capacity                                                                           as cassette_200_brand_capacity
								   , @cassette_500_brand_capacity                                                                           as cassette_500_brand_capacity
								   , @cassette_2000_brand_capacity                                                                          as cassette_2000_brand_capacity
								   , @total_capacity_amount_100                                                                             as total_capacity_amount_100
								   , @total_capacity_amount_200                                                                             as total_capacity_amount_200
								   , @total_capacity_amount_500                                                                             as total_capacity_amount_500
								   , @total_capacity_amount_2000                                                                            as total_capacity_amount_2000
								   , @denomination_100_max_capacity_percentage                                                              as denomination_100_max_capacity_percentage
								   , @denomination_200_max_capacity_percentage                                                              as denomination_200_max_capacity_percentage
								   , @denomination_500_max_capacity_percentage                                                              as denomination_500_max_capacity_percentage
								   , @denomination_2000_max_capacity_percentage                                                             as denomination_2000_max_capacity_percentage
								   , @max_amt_allowed_100                                                                                   as max_amt_allowed_100
								   , @max_amt_allowed_200                                                                                   as max_amt_allowed_200
								   , @max_amt_allowed_500                                                                                   as max_amt_allowed_500
								   , @max_amt_allowed_2000                                                                                  as max_amt_allowed_2000
								   , @denomination_wise_round_off_100                                                                       as denomination_wise_round_off_100
								   , @denomination_wise_round_off_200                                                                       as denomination_wise_round_off_200
								   , @denomination_wise_round_off_500                                                                       as denomination_wise_round_off_500
								   , @denomination_wise_round_off_2000                                                                      as denomination_wise_round_off_2000
								   , @tentative_loading_100                                                                                 as tentative_loading_100 
								   , @tentative_loading_200                                                                                 as tentative_loading_200 
								   , @tentative_loading_500                                                                                 as tentative_loading_500 
								   , @tentative_loading_2000                                                                                as tentative_loading_2000
								   -- Max Capacity                                                                                          
								   , @rounded_tentative_loading_100		                                                                    as rounded_tentative_loading_100		
								   , @rounded_tentative_loading_200	                                                                        as rounded_tentative_loading_200	    
								   , @rounded_tentative_loading_500	                                                                        as rounded_tentative_loading_500	    
								   , @rounded_tentative_loading_2000	                                                                    as rounded_tentative_loading_2000	
								   , @deno_100_priority                                                                                     as deno_100_priority
								   , @deno_200_priority                                                                                     as deno_200_priority
								   , @deno_500_priority                                                                                     as deno_500_priority
								   , @deno_2000_priority                                                                                    as deno_2000_priority
								   , @is_deno_wise_cash_available                                                                           as is_deno_wise_cash_available
									---- If denomination wise is availabble                                                                 
									-- Priority of denomination                                                                             
								   , @priority_1_is_denomination_100                                                                        as priority_1_is_denomination_100
								   , @priority_1_is_denomination_200                                                                        as priority_1_is_denomination_200
								   , @priority_1_is_denomination_500                                                                        as priority_1_is_denomination_500
								   , @priority_1_is_denomination_2000 								   	                                    as priority_1_is_denomination_2000 								   	
								   , @priority_1_is_remaining_amount_available_100                                                          as priority_1_is_remaining_amount_available_100
								   , @priority_1_is_remaining_amount_available_200                                                          as priority_1_is_remaining_amount_available_200
								   , @priority_1_is_remaining_amount_available_500                                                          as priority_1_is_remaining_amount_available_500
								   , @priority_1_is_remaining_amount_available_2000                                                         as priority_1_is_remaining_amount_available_2000
								   , @priority_1_is_remaining_capacity_available_100                                                        as priority_1_is_remaining_capacity_available_100
								   , @priority_1_is_remaining_capacity_available_200                                                        as priority_1_is_remaining_capacity_available_200
								   , @priority_1_is_remaining_capacity_available_500                                                        as priority_1_is_remaining_capacity_available_500
								   , @priority_1_is_remaining_capacity_available_2000								                        as priority_1_is_remaining_capacity_available_2000								
								   , @priority_1_loading_amount_100                                                                         as priority_1_loading_amount_100
								   , @priority_1_loading_amount_200                                                                         as priority_1_loading_amount_200
								   , @priority_1_loading_amount_500                                                                         as priority_1_loading_amount_500
								   , @priority_1_loading_amount_2000								                                        as priority_1_loading_amount_2000								   
								   , @priority_2_is_denomination_100                                                                        as priority_2_is_denomination_100
								   , @priority_2_is_denomination_200                                                                        as priority_2_is_denomination_200
								   , @priority_2_is_denomination_500                                                                        as priority_2_is_denomination_500
								   , @priority_2_is_denomination_2000 								                                        as priority_2_is_denomination_2000 								   
								   , @priority_2_is_remaining_amount_available_100                                                          as priority_2_is_remaining_amount_available_100
								   , @priority_2_is_remaining_amount_available_200                                                          as priority_2_is_remaining_amount_available_200
								   , @priority_2_is_remaining_amount_available_500                                                          as priority_2_is_remaining_amount_available_500
								   , @priority_2_is_remaining_amount_available_2000                                                         as priority_2_is_remaining_amount_available_2000
								   , @priority_2_is_remaining_capacity_available_100                                                        as priority_2_is_remaining_capacity_available_100
								   , @priority_2_is_remaining_capacity_available_200                                                        as priority_2_is_remaining_capacity_available_200
								   , @priority_2_is_remaining_capacity_available_500                                                        as priority_2_is_remaining_capacity_available_500
								   , @priority_2_is_remaining_capacity_available_2000  								                        as priority_2_is_remaining_capacity_available_2000  								
								   , @priority_2_loading_amount_100                                                                         as priority_2_loading_amount_100
								   , @priority_2_loading_amount_200                                                                         as priority_2_loading_amount_200
								   , @priority_2_loading_amount_500                                                                         as priority_2_loading_amount_500
								   , @priority_2_loading_amount_2000								                                        as priority_2_loading_amount_2000								   
								   , @priority_3_is_denomination_100                                                                        as priority_3_is_denomination_100
								   , @priority_3_is_denomination_200                                                                        as priority_3_is_denomination_200
								   , @priority_3_is_denomination_500                                                                        as priority_3_is_denomination_500
								   , @priority_3_is_denomination_2000 								                                        as priority_3_is_denomination_2000 								   
								   , @priority_3_is_remaining_amount_available_100                                                          as priority_3_is_remaining_amount_available_100
								   , @priority_3_is_remaining_amount_available_200                                                          as priority_3_is_remaining_amount_available_200
								   , @priority_3_is_remaining_amount_available_500                                                          as priority_3_is_remaining_amount_available_500
								   , @priority_3_is_remaining_amount_available_2000								                            as priority_3_is_remaining_amount_available_2000								   
								   , @priority_3_is_remaining_capacity_available_100                                                        as priority_3_is_remaining_capacity_available_100
								   , @priority_3_is_remaining_capacity_available_200                                                        as priority_3_is_remaining_capacity_available_200
								   , @priority_3_is_remaining_capacity_available_500                                                        as priority_3_is_remaining_capacity_available_500
								   , @priority_3_is_remaining_capacity_available_2000  						                                as priority_3_is_remaining_capacity_available_2000  						
								   , @priority_3_loading_amount_100                                                                         as priority_3_loading_amount_100
								   , @priority_3_loading_amount_200                                                                         as priority_3_loading_amount_200
								   , @priority_3_loading_amount_500                                                                         as priority_3_loading_amount_500
								   , @priority_3_loading_amount_2000								                                        as priority_3_loading_amount_2000								   
								   , @priority_4_is_denomination_100                                                                        as priority_4_is_denomination_100
								   , @priority_4_is_denomination_200                                                                        as priority_4_is_denomination_200
								   , @priority_4_is_denomination_500                                                                        as priority_4_is_denomination_500
								   , @priority_4_is_denomination_2000 								                                        as priority_4_is_denomination_2000 								   
								   , @priority_4_is_remaining_amount_available_100                                                          as priority_4_is_remaining_amount_available_100
								   , @priority_4_is_remaining_amount_available_200                                                          as priority_4_is_remaining_amount_available_200
								   , @priority_4_is_remaining_amount_available_500                                                          as priority_4_is_remaining_amount_available_500
								   , @priority_4_is_remaining_amount_available_2000								                            as priority_4_is_remaining_amount_available_2000								   
								   , @priority_4_is_remaining_capacity_available_100                                                        as priority_4_is_remaining_capacity_available_100
								   , @priority_4_is_remaining_capacity_available_200                                                        as priority_4_is_remaining_capacity_available_200
								   , @priority_4_is_remaining_capacity_available_500                                                        as priority_4_is_remaining_capacity_available_500
								   , @priority_4_is_remaining_capacity_available_2000		                                                as priority_4_is_remaining_capacity_available_2000		
								   , @priority_4_loading_amount_100                                                                         as priority_4_loading_amount_100
								   , @priority_4_loading_amount_200                                                                         as priority_4_loading_amount_200
								   , @priority_4_loading_amount_500                                                                         as priority_4_loading_amount_500
								   , @priority_4_loading_amount_2000								                                        as priority_4_loading_amount_2000								   
								   , @loading_amount_100                                                                                    as loading_amount_100
								   , @loading_amount_200                                                                                    as loading_amount_200
								   , @loading_amount_500                                                                                    as loading_amount_500
								   , @loading_amount_2000                                                                                   as loading_amount_2000
									-- Total loading amount								                                                    
								   , @loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000                 as total_loading_amount
								   , @remaining_capacity_amount_100                                                                         as remaining_capacity_amount_100
								   , @remaining_capacity_amount_200                                                                         as remaining_capacity_amount_200
								   , @remaining_capacity_amount_500                                                                         as remaining_capacity_amount_500
								   , @remaining_capacity_amount_2000								                                        as remaining_capacity_amount_2000								   
								   --closing available amount	                                                                           
								   , @remaining_avail_100			                                                                        as closing_avail_100			
								   , @remaining_avail_200			                                                                        as closing_avail_200			
								   , @remaining_avail_500			                                                                        as closing_avail_500			
								   , @remaining_avail_2000                                                                                  as closing_avail_2000
								   , @total_remaining_available_amount                                                                      as total_closing_available_amount
								   , @total_forecasted_remaining_amt                                                                        as total_forecasted_remaining_amt

							END -- END OF (@total_forecasted_remaining_amt > 0)						

						 
										
									
							  
						END
						ELSE --If denoination wise cash is not available.
						BEGIN							-- Denomination wise not available BEGIN
							IF (@total_remaining_available_amount > @original_total_forecasted_amt)
							BEGIN						--
								SET @loading_amount_100 = @original_forecasted_amt_100
								SET @loading_amount_200 = @original_forecasted_amt_200
								SET @loading_amount_500 = @original_forecasted_amt_500
								SET @loading_amount_2000 = @original_forecasted_amt_2000								

								SET @total_remaining_available_amount = @total_remaining_available_amount - (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)
							END
							ELSE -- IF REMAINING AMOUNT IS LESS THAN FORCASTED AMOUNT
							BEGIN
								
								IF (@deno_100_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 1
										SET @priority_1_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 0
										SET @priority_1_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_1_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_1_loading_amount_100
								END		-- end of (@deno_100_priority = 1 )

								ELSE IF (@deno_200_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 1
										SET @priority_1_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 0
										SET @priority_1_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_1_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_1_loading_amount_200
								END		-- end of (@deno_200_priority = 1 )

								ELSE IF (@deno_500_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_500 = 1
										SET @priority_1_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_500 = 0
										SET @priority_1_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_1_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_1_loading_amount_500
								END		-- end of (@deno_500_priority = 1 )

								ELSE IF (@deno_2000_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 1
										SET @priority_1_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 0
										SET @priority_1_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_1_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_1_loading_amount_2000
								END		-- end of (@deno_2000_priority = 1 )
								----------------------------------Priority 2 start -------------------------------------------
								IF (@deno_100_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 1
										SET @priority_2_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 0
										SET @priority_2_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_2_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_2_loading_amount_100
								END		-- end of (@deno_100_priority = 2 )

								ELSE IF (@deno_200_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 1
										SET @priority_2_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 0
										SET @priority_2_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_2_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_2_loading_amount_200
								END		-- end of (@deno_200_priority = 2 )

								ELSE IF (@deno_500_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 1
										SET @priority_2_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 0
										SET @priority_2_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_2_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_2_loading_amount_500
								END		-- end of (@deno_500_priority = 2 )

								ELSE IF (@deno_2000_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 1
										SET @priority_2_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 0
										SET @priority_2_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_2_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_2_loading_amount_2000
								END		-- end of (@deno_2000_priority = 2 )

								---------------------------------Priority 3 start -------------------------------
								IF (@deno_100_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 1
										SET @priority_3_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 0
										SET @priority_3_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_3_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_3_loading_amount_100
								END		-- end of (@deno_100_priority = 3 )

								ELSE IF (@deno_200_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 1
										SET @priority_3_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 0
										SET @priority_3_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_3_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_3_loading_amount_200
								END		-- end of (@deno_200_priority = 1 )

								ELSE IF (@deno_500_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 1
										SET @priority_3_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 0
										SET @priority_3_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_3_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_3_loading_amount_500
								END		-- end of (@deno_500_priority = 3 )

								ELSE IF (@deno_2000_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 1
										SET @priority_3_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 0
										SET @priority_3_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_3_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_3_loading_amount_2000
								END		-- end of (@deno_2000_priority = 3 )

								----------------------------------priority 4 start------------------------------
								IF (@deno_100_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 1
										SET @priority_4_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 0
										SET @priority_4_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_4_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_4_loading_amount_100
								END		-- end of (@deno_100_priority = 4 )

								ELSE IF (@deno_200_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 1
										SET @priority_4_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 0
										SET @priority_4_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_4_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_4_loading_amount_200
								END		-- end of (@deno_200_priority = 4 )

								ELSE IF (@deno_500_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 1
										SET @priority_4_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 0
										SET @priority_4_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_4_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_4_loading_amount_500
								END		-- end of (@deno_500_priority = 4 )

								ELSE IF (@deno_2000_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 1
										SET @priority_4_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 0
										SET @priority_4_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_4_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_4_loading_amount_2000
								END		-- end of (@deno_2000_priority = 4 )
							END	-- END OF ELSE -- IF REMAINING AMOUNT IS LESS THAN FORCASTED AMOUNT

							-- insert into temp table
							
							INSERT INTO #temp_cash_pre_availability 
							(
													project_id
												,	bank_code
												,	atmid
												,	total_opening_remaining_available_amount
												,	opening_remaining_available_amount_100
												,	opening_remaining_available_amount_200
												,	opening_remaining_available_amount_500
												,	opening_remaining_available_amount_2000
												,	original_total_forecasted_amt
												,	original_forecasted_amt_100
												,	original_forecasted_amt_200
												,	original_forecasted_amt_500
												,	original_forecasted_amt_2000
												,	morning_balance_100
												,	morning_balance_200
												,	morning_balance_500
												,	morning_balance_2000
												,	total_morning_balance
												,	cassette_100_count_original
												,	cassette_200_count_original
												,	cassette_500_count_original
												,	cassette_2000_count_original
												,	cassette_100_brand_capacity
												,	cassette_200_brand_capacity
												,	cassette_500_brand_capacity
												,	cassette_2000_brand_capacity
												,	total_capacity_amount_100
												,	total_capacity_amount_200
												,	total_capacity_amount_500
												,	total_capacity_amount_2000
												,	denomination_100_max_capacity_percentage
												,	denomination_200_max_capacity_percentage
												,	denomination_500_max_capacity_percentage
												,	denomination_2000_max_capacity_percentage
												,	max_amt_allowed_100
												,	max_amt_allowed_200
												,	max_amt_allowed_500
												,	max_amt_allowed_2000
												,	denomination_wise_round_off_100
												,	denomination_wise_round_off_200
												,	denomination_wise_round_off_500
												,	denomination_wise_round_off_2000
												,	tentative_loading_100
												,	tentative_loading_200
												,	tentative_loading_500
												,	tentative_loading_2000
												,	rounded_tentative_loading_100
												,	rounded_tentative_loading_200
												,	rounded_tentative_loading_500
												,	rounded_tentative_loading_2000
												,	deno_100_priority
												,	deno_200_priority
												,	deno_500_priority
												,	deno_2000_priority
												,	is_deno_wise_cash_available
												,	priority_1_is_denomination_100
												,	priority_1_is_denomination_200
												,	priority_1_is_denomination_500
												,	priority_1_is_denomination_2000
												,	priority_1_is_remaining_amount_available_100
												,	priority_1_is_remaining_amount_available_200
												,	priority_1_is_remaining_amount_available_500
												,	priority_1_is_remaining_amount_available_2000
												,	priority_1_loading_amount_100
												,	priority_1_loading_amount_200
												,	priority_1_loading_amount_500
												,	priority_1_loading_amount_2000
												,	priority_2_is_denomination_100
												,	priority_2_is_denomination_200
												,	priority_2_is_denomination_500
												,	priority_2_is_denomination_2000
												,	priority_2_is_remaining_amount_available_100
												,	priority_2_is_remaining_amount_available_200
												,	priority_2_is_remaining_amount_available_500
												,	priority_2_is_remaining_amount_available_2000
												,	priority_2_loading_amount_100
												,	priority_2_loading_amount_200
												,	priority_2_loading_amount_500
												,	priority_2_loading_amount_2000
												,	priority_3_is_denomination_100
												,	priority_3_is_denomination_200
												,	priority_3_is_denomination_500
												,	priority_3_is_denomination_2000
												,	priority_3_is_remaining_amount_available_100
												,	priority_3_is_remaining_amount_available_200
												,	priority_3_is_remaining_amount_available_500
												,	priority_3_is_remaining_amount_available_2000
												,	priority_3_loading_amount_100
												,	priority_3_loading_amount_200
												,	priority_3_loading_amount_500
												,	priority_3_loading_amount_2000
												,	priority_4_is_denomination_100
												,	priority_4_is_denomination_200
												,	priority_4_is_denomination_500
												,	priority_4_is_denomination_2000
												,	priority_4_is_remaining_amount_available_100
												,	priority_4_is_remaining_amount_available_200
												,	priority_4_is_remaining_amount_available_500
												,	priority_4_is_remaining_amount_available_2000
												,	priority_4_loading_amount_100
												,	priority_4_loading_amount_200
												,	priority_4_loading_amount_500
												,	priority_4_loading_amount_2000
												,	loading_amount_100
												,	loading_amount_200
												,	loading_amount_500
												,	loading_amount_2000
												,	total_loading_amount
												,	total_closing_remaining_available_amount
												,	total_forecasted_remaining_amt
			)
			SELECT
									 @project_id								                                                            as project_id								
								   , @bank_code									                                                            as bank_code									
								   , @atmid										                                                            as atmid
								   , @total_opening_remaining_available_amount																as total_opening_remaining_available_amount
								   , @opening_remaining_available_amount_100																as opening_remaining_available_amount_100	
								   , @opening_remaining_available_amount_200	                                                            as opening_remaining_available_amount_200	
								   , @opening_remaining_available_amount_500	                                                            as opening_remaining_available_amount_500	
								   , @opening_remaining_available_amount_2000	                                                            as opening_remaining_available_amount_2000											
								   , @original_total_forecasted_amt							                                                as original_total_forecasted_amt							
								   , @original_forecasted_amt_100				                                                            as original_forecasted_amt_100				
								   , @original_forecasted_amt_200                                                                           as original_forecasted_amt_200
								   , @original_forecasted_amt_500                                                                           as original_forecasted_amt_500
								   , @original_forecasted_amt_2000                                                                          as original_forecasted_amt_2000
								   , @morning_balance_100                                                                                   as morning_balance_100
								   , @morning_balance_200                                                                                   as morning_balance_200
								   , @morning_balance_500                                                                                   as morning_balance_500
								   , @morning_balance_2000                                                                                  as morning_balance_2000
								   , @total_morning_balance                                                                                 as total_morning_balance
								   , @cassette_100_count_original                                                                           as cassette_100_count_original
								   , @cassette_200_count_original                                                                           as cassette_200_count_original
								   , @cassette_500_count_original                                                                           as cassette_500_count_original
								   , @cassette_2000_count_original                                                                          as cassette_2000_count_original
								   , @cassette_100_brand_capacity                                                                           as cassette_100_brand_capacity
								   , @cassette_200_brand_capacity                                                                           as cassette_200_brand_capacity
								   , @cassette_500_brand_capacity                                                                           as cassette_500_brand_capacity
								   , @cassette_2000_brand_capacity                                                                          as cassette_2000_brand_capacity
								   , @total_capacity_amount_100                                                                             as total_capacity_amount_100
								   , @total_capacity_amount_200                                                                             as total_capacity_amount_200
								   , @total_capacity_amount_500                                                                             as total_capacity_amount_500
								   , @total_capacity_amount_2000                                                                            as total_capacity_amount_2000
								   , @denomination_100_max_capacity_percentage                                                              as denomination_100_max_capacity_percentage
								   , @denomination_200_max_capacity_percentage                                                              as denomination_200_max_capacity_percentage
								   , @denomination_500_max_capacity_percentage                                                              as denomination_500_max_capacity_percentage
								   , @denomination_2000_max_capacity_percentage                                                             as denomination_2000_max_capacity_percentage
								   , @max_amt_allowed_100                                                                                   as max_amt_allowed_100
								   , @max_amt_allowed_200                                                                                   as max_amt_allowed_200
								   , @max_amt_allowed_500                                                                                   as max_amt_allowed_500
								   , @max_amt_allowed_2000                                                                                  as max_amt_allowed_2000
								   , @denomination_wise_round_off_100                                                                       as denomination_wise_round_off_100
								   , @denomination_wise_round_off_200                                                                       as denomination_wise_round_off_200
								   , @denomination_wise_round_off_500                                                                       as denomination_wise_round_off_500
								   , @denomination_wise_round_off_2000                                                                      as denomination_wise_round_off_2000
								   , @tentative_loading_100                                                                                 as tentative_loading_100 
								   , @tentative_loading_200                                                                                 as tentative_loading_200 
								   , @tentative_loading_500                                                                                 as tentative_loading_500 
								   , @tentative_loading_2000                                                                                as tentative_loading_2000
								   -- Max Capacity                                                                                          
								   , @rounded_tentative_loading_100		                                                                    as rounded_tentative_loading_100		
								   , @rounded_tentative_loading_200	                                                                        as rounded_tentative_loading_200	    
								   , @rounded_tentative_loading_500	                                                                        as rounded_tentative_loading_500	    
								   , @rounded_tentative_loading_2000	                                                                    as rounded_tentative_loading_2000	
								   , @deno_100_priority                                                                                     as deno_100_priority
								   , @deno_200_priority                                                                                     as deno_200_priority
								   , @deno_500_priority                                                                                     as deno_500_priority
								   , @deno_2000_priority                                                                                    as deno_2000_priority
								   , @is_deno_wise_cash_available                                                                           as is_deno_wise_cash_available
									---- If denomination wise is availabble                                                                 
									-- Priority of denomination                                                                             
								   , @priority_1_is_denomination_100                                                                        as priority_1_is_denomination_100
								   , @priority_1_is_denomination_200                                                                        as priority_1_is_denomination_200
								   , @priority_1_is_denomination_500                                                                        as priority_1_is_denomination_500
								   , @priority_1_is_denomination_2000 								   	                                    as priority_1_is_denomination_2000 								   	
								   , @priority_1_is_remaining_amount_available_100                                                          as priority_1_is_remaining_amount_available_100
								   , @priority_1_is_remaining_amount_available_200                                                          as priority_1_is_remaining_amount_available_200
								   , @priority_1_is_remaining_amount_available_500                                                          as priority_1_is_remaining_amount_available_500
								   , @priority_1_is_remaining_amount_available_2000                                                         as priority_1_is_remaining_amount_available_2000
								   , @priority_1_loading_amount_100                                                                         as priority_1_loading_amount_100
								   , @priority_1_loading_amount_200                                                                         as priority_1_loading_amount_200
								   , @priority_1_loading_amount_500                                                                         as priority_1_loading_amount_500
								   , @priority_1_loading_amount_2000								                                        as priority_1_loading_amount_2000								   
								   , @priority_2_is_denomination_100                                                                        as priority_2_is_denomination_100
								   , @priority_2_is_denomination_200                                                                        as priority_2_is_denomination_200
								   , @priority_2_is_denomination_500                                                                        as priority_2_is_denomination_500
								   , @priority_2_is_denomination_2000 								                                        as priority_2_is_denomination_2000 								   
								   , @priority_2_is_remaining_amount_available_100                                                          as priority_2_is_remaining_amount_available_100
								   , @priority_2_is_remaining_amount_available_200                                                          as priority_2_is_remaining_amount_available_200
								   , @priority_2_is_remaining_amount_available_500                                                          as priority_2_is_remaining_amount_available_500
								   , @priority_2_is_remaining_amount_available_2000                                                         as priority_2_is_remaining_amount_available_2000
								   , @priority_2_loading_amount_100                                                                         as priority_2_loading_amount_100
								   , @priority_2_loading_amount_200                                                                         as priority_2_loading_amount_200
								   , @priority_2_loading_amount_500                                                                         as priority_2_loading_amount_500
								   , @priority_2_loading_amount_2000								                                        as priority_2_loading_amount_2000								   
								   , @priority_3_is_denomination_100                                                                        as priority_3_is_denomination_100
								   , @priority_3_is_denomination_200                                                                        as priority_3_is_denomination_200
								   , @priority_3_is_denomination_500                                                                        as priority_3_is_denomination_500
								   , @priority_3_is_denomination_2000 								                                        as priority_3_is_denomination_2000 								   
								   , @priority_3_is_remaining_amount_available_100                                                          as priority_3_is_remaining_amount_available_100
								   , @priority_3_is_remaining_amount_available_200                                                          as priority_3_is_remaining_amount_available_200
								   , @priority_3_is_remaining_amount_available_500                                                          as priority_3_is_remaining_amount_available_500
								   , @priority_3_is_remaining_amount_available_2000								                            as priority_3_is_remaining_amount_available_2000								   
								   , @priority_3_loading_amount_100                                                                         as priority_3_loading_amount_100
								   , @priority_3_loading_amount_200                                                                         as priority_3_loading_amount_200
								   , @priority_3_loading_amount_500                                                                         as priority_3_loading_amount_500
								   , @priority_3_loading_amount_2000								                                        as priority_3_loading_amount_2000								   
								   , @priority_4_is_denomination_100                                                                        as priority_4_is_denomination_100
								   , @priority_4_is_denomination_200                                                                        as priority_4_is_denomination_200
								   , @priority_4_is_denomination_500                                                                        as priority_4_is_denomination_500
								   , @priority_4_is_denomination_2000 								                                        as priority_4_is_denomination_2000 								   
								   , @priority_4_is_remaining_amount_available_100                                                          as priority_4_is_remaining_amount_available_100
								   , @priority_4_is_remaining_amount_available_200                                                          as priority_4_is_remaining_amount_available_200
								   , @priority_4_is_remaining_amount_available_500                                                          as priority_4_is_remaining_amount_available_500
								   , @priority_4_is_remaining_amount_available_2000								                            as priority_4_is_remaining_amount_available_2000								   
								   , @priority_4_loading_amount_100                                                                         as priority_4_loading_amount_100
								   , @priority_4_loading_amount_200                                                                         as priority_4_loading_amount_200
								   , @priority_4_loading_amount_500                                                                         as priority_4_loading_amount_500
								   , @priority_4_loading_amount_2000								                                        as priority_4_loading_amount_2000								   
								   , @loading_amount_100                                                                                    as loading_amount_100
								   , @loading_amount_200                                                                                    as loading_amount_200
								   , @loading_amount_500                                                                                    as loading_amount_500
								   , @loading_amount_2000                                                                                   as loading_amount_2000
									-- Total loading amount								                                                    
								   , @loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000                 as total_loading_amount
								   --closing available amount	                                                                           
								   , @total_remaining_available_amount                                                                      as total_closing_remaining_available_amount
								   , @total_forecasted_remaining_amt                                                                        as total_forecasted_remaining_amt



				

						END	--ELSE --If denoination wise cash is not available.						
						
						FETCH NEXT FROM cursor2 
									 INTO  @atmid
									 , @atm_priority							
									, @denomination_100_max_capacity_percentage
									, @denomination_200_max_capacity_percentage
									, @denomination_500_max_capacity_percentage
									, @denomination_2000_max_capacity_percentage
									, @morning_balance_100
									, @morning_balance_200
									, @morning_balance_500
									, @morning_balance_2000
									, @total_morning_balance
									, @original_forecasted_amt_100
									, @original_forecasted_amt_200
									, @original_forecasted_amt_500
									, @original_forecasted_amt_2000
									, @original_total_forecasted_amt
									--, @cassette_50_count_original  
									, @cassette_100_count_original 
									, @cassette_200_count_original 
									, @cassette_500_count_original 
									, @cassette_2000_count_original
									, @deno_100_priority
									, @deno_200_priority
									, @deno_500_priority
									, @deno_2000_priority
									, @cassette_100_brand_capacity
									, @cassette_200_brand_capacity
									, @cassette_500_brand_capacity
									, @cassette_2000_brand_capacity
					END			-- While End (cusror2)
				CLOSE cursor2 
				DEALLOCATE cursor2
			END

			--- Insert in temp table for feeder 

				
				INSERT INTO #temp_feeder_forecast
				(
						project_id						
					,	bank_code						
					,	feeder_branch_code							
					,	is_deno_wise_cash_available		
					,	total_remaining_available_amount
					,   remaining_avail_100				
					,   remaining_avail_200				
					,   remaining_avail_500				
					,   remaining_avail_2000			
				)

				SELECT	  @project_id
						, @bank_code
						, @feederbranchcode
						, @is_deno_wise_cash_available
						, @total_remaining_available_amount
						, @remaining_avail_100			
						, @remaining_avail_200			
						, @remaining_avail_500			
						, @remaining_avail_2000			


	FETCH NEXT FROM cursor1 INTO @project_id,@bank_code,@feederbranchcode, @total_original_available_amount, @Original_avail_100, @Original_avail_200, @Original_avail_500, @Original_avail_2000,@is_deno_wise_cash_available
	
   CLOSE cursor1 
 DEALLOCATE cursor1


 --RETURN;
 
 --SELECT * FROM #temp_cash_pre_availability
 --SELECT * FROM #temp_feeder_forecast

 --RETURN;
	 -- Run cursor for atm level from #dist_with_cra

	  --SELECT * FROM #temp_feeder_forecast

	   --begin transaction

			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_detail Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			DECLARE @ForStatus VARCHAR(15) = 'Active'
			DECLARE @ToStatus VARCHAR(15) = 'Deleted'
			

			
			--IF EXISTS(
			--			SELECT 1 
			--			FROM data_update_log
			--			WHERE record_status = 'Active'
			--			AND data_for_type = 'Revised Indent'
			--			AND datafor_date_time = @dateT
			--		)

			--		BEGIN
			--			set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old data update log entries for indent ' 
	
			--			DECLARE @SetWhere_data_update VARCHAR(MAX) = ' data_for_type = ''' + 'Indent' + ''' and datafor_date_time =  ''' + cast(@dateT as nvarchar(50))+ ''''

			--			EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_data_update,@out OUTPUT

			--			set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for indent' 

			--		END	

			IF EXISTS(
						SELECT 1 
						FROM distribution_planning_detail
						WHERE record_status = 'Active'
						AND indent_code = @active_indent_code
					)

					BEGIN
							set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in distribution_planning_detail ' 
	
							DECLARE @SetWhere_distribution_detail VARCHAR(MAX) = ' indent_code = ''' + @active_indent_code + ''''

							EXEC dbo.[uspSetStatus] 'dbo.distribution_planning_detail',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_distribution_detail,@out OUTPUT
						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in distribution_planning_detail table for old entries for indent ' 
					
					END	
			IF EXISTS(
						SELECT 1 
						FROM distribution_planning_master
						WHERE record_status = 'Active'
						AND indent_code = @active_indent_code
					)

					BEGIN
					
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in distribution_planning_master ' 
	
						DECLARE @SetWhere_distribution_master VARCHAR(MAX) = ' indent_code = ''' + @active_indent_code + ''''
							
						EXEC dbo.[uspSetStatus] 'dbo.distribution_planning_master',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_distribution_master,@out OUTPUT

						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in distribution_planning_master table for old entries for indent  ' 
					
					END
		
			IF EXISTS(
						SELECT 1 
						FROM dbo.indent_master
						WHERE record_status = 'Active'
						AND indent_order_number = @active_indent_code
					
					)
					BEGIN
						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in indent master ' 
						
						DECLARE @SetWhere_indent_master VARCHAR(MAX) = ' indent_order_number = ''' + @active_indent_code + ''''
							
						EXEC dbo.[uspSetStatus] 'dbo.indent_master',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_indent_master,@out OUTPUT
												
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in indent master table for old entries for indent '
					
					END
					
			IF EXISTS(
							SELECT *
							FROM dbo.indent_detail id
							WHERE id.record_status = 'Active'
							AND indent_order_number = @active_indent_code
						)

					BEGIN	
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in indent master ' 
						
						DECLARE @SetWhere_indent_detail VARCHAR(MAX) = ' indent_order_number = ''' + @active_indent_code + ''''
							
						EXEC dbo.[uspSetStatus] 'dbo.indent_detail',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_indent_master,@out OUTPUT
												
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in indent master table for old entries for indent '
										
					END





 		DECLARE @row INT
		DECLARE @errorcode int
			   INSERT INTO  distribution_planning_detail
				(
				[indent_code]
				,[atm_id]
				,[site_code]
				,[sol_id]
				,[bank_code]
				,[project_id]
				,[indentdate]
				,[bank_cash_limit]
				,[insurance_limit]
				,[feeder_branch_code]
				,[cra]
				,[base_limit]
				,[dispenseformula]
				,[decidelimitdays]
				,[loadinglimitdays]
				,[avgdispense]
				,[loading_amount]
				,[morning_balance_50]
				,[morning_balance_100]
				,[morning_balance_200]
				,[morning_balance_500]
				,[morning_balance_2000]
				,[total_morning_balance]
				,[cassette_50_brand_capacity]
				,[cassette_100_brand_capacity]
				,[cassette_200_brand_capacity]
				,[cassette_500_brand_capacity]
				,[cassette_2000_brand_capacity]
				,[cassette_50_count]
				,[cassette_100_count]
				,[cassette_200_count]
				,[cassette_500_count]
				,[cassette_2000_count]
				,[limit_amount]
				,[total_capacity_amount_50]
				,[total_capacity_amount_100]
				,[total_capacity_amount_200]
				,[total_capacity_amount_500]
				,[total_capacity_amount_2000]
				,[total_cassette_capacity]
				,[avgdecidelimit]
				,[DecideLimit]
				,[threshold_limit]
				,[loadinglimit]
				,[applied_vaulting_percentage]
				,[ignore_code]
				,[ignore_description]
				,[dist_Purpose]
				,[defaultamt]
				,[default_flag]
				,[curbal_div_avgDisp]
				,[loadingGap_cur_bal_avg_disp]
				,[CashOut]
				,[remaining_balance_50]
				,[remaining_balance_100]
				,[remaining_balance_200]
				,[remaining_balance_500]
				,[remaining_balance_2000]
				,[cassette_50_capacity_amount]
				,[cassette_100_capacity_amount]
				,[cassette_200_capacity_amount]
				,[cassette_500_capacity_amount]
				,[cassette_2000_capacity_amount]
				,[total_cassette_capacity_amount]
				,[cassette_capacity_percentage_50]
				,[cassette_capacity_percentage_100]
				,[cassette_capacity_percentage_200]
				,[cassette_capacity_percentage_500]
				,[cassette_capacity_percentage_2000]
				,[tentative_loading_amount_50]
				,[tentative_loading_amount_100]
				,[tentative_loading_amount_200]
				,[tentative_loading_amount_500]
				,[tentative_loading_amount_2000]
				,[rounded_amount_50]
				,[rounded_amount_100]
				,[rounded_amount_200]
				,[rounded_amount_500]
				,[rounded_amount_2000]
				,[total_expected_balanceT1]
				,[expected_balanceT1_50]
				,[expected_balanceT1_100]
				,[expected_balanceT1_200]
				,[expected_balanceT1_500]
				,[expected_balanceT1_2000]
				,[total_vault_amount]
				,[vaultingamount_50]
				,[vaultingamount_100]
				,[vaultingamount_200]
				,[vaultingamount_500]
				,[vaultingamount_2000]
				,[rounded_vaultingamount_50]
				,[rounded_vaultingamount_100]
				,[rounded_vaultingamount_200]
				,[rounded_vaultingamount_500]
				,[rounded_vaultingamount_2000]
				,[total_rounded_vault_amt]
				, total_opening_remaining_available_amount	
				, opening_remaining_available_amount_100	
				, opening_remaining_available_amount_200	
				, opening_remaining_available_amount_500	
				, opening_remaining_available_amount_2000	
				, cassette_100_count_original				
				, cassette_200_count_original				
				, cassette_500_count_original				
				, cassette_2000_count_original				
				, denomination_100_max_capacity_percentage	
				, denomination_200_max_capacity_percentage	
				, denomination_500_max_capacity_percentage	
				, denomination_2000_max_capacity_percentage	
				, max_amt_allowed_100						
				, max_amt_allowed_200						
				, max_amt_allowed_500						
				, max_amt_allowed_2000						
				, denomination_wise_round_off_100			
				, denomination_wise_round_off_200			
				, denomination_wise_round_off_500			
				, denomination_wise_round_off_2000			
				, tentative_loading_100						
				, tentative_loading_200						
				, tentative_loading_500						
				, tentative_loading_2000					
				, rounded_tentative_loading_100				
				, rounded_tentative_loading_200				
				, rounded_tentative_loading_500				
				, rounded_tentative_loading_2000			
				, deno_100_priority							
				, deno_200_priority							
				, deno_500_priority							
				, deno_2000_priority							
				, is_deno_wise_cash_available						
				, priority_1_is_denomination_100					
				, priority_1_is_denomination_200					
				, priority_1_is_denomination_500					
				, priority_1_is_denomination_2000					
				, priority_1_is_remaining_amount_available_100		
				, priority_1_is_remaining_amount_available_200		
				, priority_1_is_remaining_amount_available_500		
				, priority_1_is_remaining_amount_available_2000		
				, priority_1_is_remaining_capacity_available_100	
				, priority_1_is_remaining_capacity_available_200	
				, priority_1_is_remaining_capacity_available_500	
				, priority_1_is_remaining_capacity_available_2000	
				, priority_1_loading_amount_100						
				, priority_1_loading_amount_200						
				, priority_1_loading_amount_500						
				, priority_1_loading_amount_2000					
				, priority_2_is_denomination_100					
				, priority_2_is_denomination_200					
				, priority_2_is_denomination_500					
				, priority_2_is_denomination_2000					
				, priority_2_is_remaining_amount_available_100		
				, priority_2_is_remaining_amount_available_200		
				, priority_2_is_remaining_amount_available_500		
				, priority_2_is_remaining_amount_available_2000		
				, priority_2_is_remaining_capacity_available_100	
				, priority_2_is_remaining_capacity_available_200	
				, priority_2_is_remaining_capacity_available_500	
				, priority_2_is_remaining_capacity_available_2000	
				, priority_2_loading_amount_100						
				, priority_2_loading_amount_200						
				, priority_2_loading_amount_500						
				, priority_2_loading_amount_2000					
				, priority_3_is_denomination_100					
				, priority_3_is_denomination_200					
				, priority_3_is_denomination_500					
				, priority_3_is_denomination_2000					
				, priority_3_is_remaining_amount_available_100		
				, priority_3_is_remaining_amount_available_200		
				, priority_3_is_remaining_amount_available_500		
				, priority_3_is_remaining_amount_available_2000		
				, priority_3_is_remaining_capacity_available_100	
				, priority_3_is_remaining_capacity_available_200	
				, priority_3_is_remaining_capacity_available_500	
				, priority_3_is_remaining_capacity_available_2000	
				, priority_3_loading_amount_100						
				, priority_3_loading_amount_200						
				, priority_3_loading_amount_500						
				, priority_3_loading_amount_2000					
				, priority_4_is_denomination_100					
				, priority_4_is_denomination_200					
				, priority_4_is_denomination_500					
				, priority_4_is_denomination_2000					
				, priority_4_is_remaining_amount_available_100		
				, priority_4_is_remaining_amount_available_200		
				, priority_4_is_remaining_amount_available_500		
				, priority_4_is_remaining_amount_available_2000		
				, priority_4_is_remaining_capacity_available_100	
				, priority_4_is_remaining_capacity_available_200	
				, priority_4_is_remaining_capacity_available_500	
				, priority_4_is_remaining_capacity_available_2000	
				, priority_4_loading_amount_100						
				, priority_4_loading_amount_200						
				, priority_4_loading_amount_500						
				, priority_4_loading_amount_2000					
				, loading_amount_100								
				, loading_amount_200								
				, loading_amount_500								
				, loading_amount_2000								
				, total_loading_amount								
				, remaining_capacity_amount_100						
				, remaining_capacity_amount_200						
				, remaining_capacity_amount_500						
				, remaining_capacity_amount_2000					
				, closing_remaining_available_amount_100			
				, closing_remaining_available_amount_200			
				, closing_remaining_available_amount_500			
				, closing_remaining_available_amount_2000			
				, total_closing_remaining_available_amount			
				, total_forecasted_remaining_amt					
				, indent_counter 									
				, record_status
				,created_on
				,created_by
				,created_reference_id
				)
			  SELECT	indent_code
					,	atm_id
					,   site_code
					,	sol_id
					,	temp.bank_code
					,	temp.project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	cra
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	dist.morning_balance_100
					,	dist.morning_balance_200
					,	dist.morning_balance_500
					,	dist.morning_balance_2000
					,	dist.total_morning_balance
					,	cassette_50_brand_capacity
					,	temp.cassette_100_brand_capacity
					,	temp.cassette_200_brand_capacity
					,	temp.cassette_500_brand_capacity
					,	temp.cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	temp.total_capacity_amount_100
					,	temp.total_capacity_amount_200
					,	temp.total_capacity_amount_500
					,	temp.total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   NULL
					,   rounded_amount_100
					,   rounded_amount_200
					,   rounded_amount_500
					,   rounded_amount_2000
					,	total_expected_balanceT1
					,	expected_balanceT1_50
					,	expected_balanceT1_100
					,	expected_balanceT1_200
					,	expected_balanceT1_500
					,	expected_balanceT1_2000
					,	total_vault_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	rounded_vaultingamount_50
					,	rounded_vaultingamount_100
					,	rounded_vaultingamount_200
					,	rounded_vaultingamount_500
					,	rounded_vaultingamount_2000
					,	total_rounded_vault_amt
					, temp.total_opening_remaining_available_amount	
					, temp.opening_remaining_available_amount_100	
					, temp.opening_remaining_available_amount_200	
					, temp.opening_remaining_available_amount_500	
					, temp.opening_remaining_available_amount_2000	
					, temp.cassette_100_count_original				
					, temp.cassette_200_count_original				
					, temp.cassette_500_count_original				
					, temp.cassette_2000_count_original				
					, temp.denomination_100_max_capacity_percentage	
					, temp.denomination_200_max_capacity_percentage	
					, temp.denomination_500_max_capacity_percentage	
					, temp.denomination_2000_max_capacity_percentage	
					, temp.max_amt_allowed_100						
					, temp.max_amt_allowed_200						
					, temp.max_amt_allowed_500						
					, temp.max_amt_allowed_2000						
					, temp.denomination_wise_round_off_100			
					, temp.denomination_wise_round_off_200			
					, temp.denomination_wise_round_off_500			
					, temp.denomination_wise_round_off_2000			
					, temp.tentative_loading_100						
					, temp.tentative_loading_200						
					, temp.tentative_loading_500						
					, temp.tentative_loading_2000					
					, temp.rounded_tentative_loading_100				
					, temp.rounded_tentative_loading_200				
					, temp.rounded_tentative_loading_500				
					, temp.rounded_tentative_loading_2000			
					, temp.deno_100_priority										
					, temp.deno_200_priority										
					, temp.deno_500_priority										
					, temp.deno_2000_priority									
					, temp.is_deno_wise_cash_available							
					, temp.priority_1_is_denomination_100						
					, temp.priority_1_is_denomination_200						
					, temp.priority_1_is_denomination_500						
					, temp.priority_1_is_denomination_2000						
					, temp.priority_1_is_remaining_amount_available_100			
					, temp.priority_1_is_remaining_amount_available_200			
					, temp.priority_1_is_remaining_amount_available_500			
					, temp.priority_1_is_remaining_amount_available_2000			
					, temp.priority_1_is_remaining_capacity_available_100		
					, temp.priority_1_is_remaining_capacity_available_200		
					, temp.priority_1_is_remaining_capacity_available_500		
					, temp.priority_1_is_remaining_capacity_available_2000		
					, temp.priority_1_loading_amount_100							
					, temp.priority_1_loading_amount_200							
					, temp.priority_1_loading_amount_500							
					, temp.priority_1_loading_amount_2000						
					, temp.priority_2_is_denomination_100						
					, temp.priority_2_is_denomination_200						
					, temp.priority_2_is_denomination_500						
					, temp.priority_2_is_denomination_2000						
					, temp.priority_2_is_remaining_amount_available_100			
					, temp.priority_2_is_remaining_amount_available_200			
					, temp.priority_2_is_remaining_amount_available_500			
					, temp.priority_2_is_remaining_amount_available_2000			
					, temp.priority_2_is_remaining_capacity_available_100		
					, temp.priority_2_is_remaining_capacity_available_200		
					, temp.priority_2_is_remaining_capacity_available_500		
					, temp.priority_2_is_remaining_capacity_available_2000		
					, temp.priority_2_loading_amount_100							
					, temp.priority_2_loading_amount_200							
					, temp.priority_2_loading_amount_500							
					, temp.priority_2_loading_amount_2000						
					, temp.priority_3_is_denomination_100						
					, temp.priority_3_is_denomination_200						
					, temp.priority_3_is_denomination_500						
					, temp.priority_3_is_denomination_2000						
					, temp.priority_3_is_remaining_amount_available_100			
					, temp.priority_3_is_remaining_amount_available_200			
					, temp.priority_3_is_remaining_amount_available_500			
					, temp.priority_3_is_remaining_amount_available_2000			
					, temp.priority_3_is_remaining_capacity_available_100		
					, temp.priority_3_is_remaining_capacity_available_200		
					, temp.priority_3_is_remaining_capacity_available_500		
					, temp.priority_3_is_remaining_capacity_available_2000		
					, temp.priority_3_loading_amount_100							
					, temp.priority_3_loading_amount_200							
					, temp.priority_3_loading_amount_500							
					, temp.priority_3_loading_amount_2000						
					, temp.priority_4_is_denomination_100						
					, temp.priority_4_is_denomination_200						
					, temp.priority_4_is_denomination_500						
					, temp.priority_4_is_denomination_2000						
					, temp.priority_4_is_remaining_amount_available_100			
					, temp.priority_4_is_remaining_amount_available_200			
					, temp.priority_4_is_remaining_amount_available_500			
					, temp.priority_4_is_remaining_amount_available_2000			
					, temp.priority_4_is_remaining_capacity_available_100		
					, temp.priority_4_is_remaining_capacity_available_200		
					, temp.priority_4_is_remaining_capacity_available_500		
					, temp.priority_4_is_remaining_capacity_available_2000		
					, temp.priority_4_loading_amount_100							
					, temp.priority_4_loading_amount_200							
					, temp.priority_4_loading_amount_500							
					, temp.priority_4_loading_amount_2000						
					, temp.loading_amount_100									
					, temp.loading_amount_200									
					, temp.loading_amount_500									
					, temp.loading_amount_2000									
					, temp.total_loading_amount									
					, temp.remaining_capacity_amount_100							
					, temp.remaining_capacity_amount_200							
					, temp.remaining_capacity_amount_500							
					, temp.remaining_capacity_amount_2000						
					, temp.closing_remaining_available_amount_100				
					, temp.closing_remaining_available_amount_200				
					, temp.closing_remaining_available_amount_500				
					, temp.closing_remaining_available_amount_2000				
					, temp.total_closing_remaining_available_amount				
					, temp.total_forecasted_remaining_amt						
					, dist.is_deno_wise_cash_available + 1 										 
					,	'Approval Pending'
					,	@timestamp_date
					,	@cur_user
					,	@created_reference_id
					FROM #dist_with_cra dist
					LEFT JOIN #temp_cash_pre_availability temp
					on		temp.atmid COLLATE DATABASE_DEFAULT = dist.atm_id COLLATE DATABASE_DEFAULT
					AND		temp.project_id  COLLATE DATABASE_DEFAULT= dist.project_id COLLATE DATABASE_DEFAULT
					AND		temp.bank_code COLLATE DATABASE_DEFAULT = dist.bank_code COLLATE DATABASE_DEFAULT
				
				SELECT @row	= @@ROWCOUNT,@ErrorCode = @@error

					INSERT INTO distribution_planning_master
					( 
					[indent_code]
				   ,[indentdate]
				   ,[project_id]
				   ,[bank_code]
				   ,[feeder_branch_code]
				   ,[cra]
				   ,[max_loading_amount]
				   ,[max_loading_amount_50]
				   ,[max_loading_amount_100]
				   ,[max_loading_amount_200]
				   ,[max_loading_amount_500]
				   ,[max_loading_amount_2000]
				   ,[min_loading_amount]
				   ,[forecast_loading_amount]
				   ,[forecast_loading_amount_50]
				   ,[forecast_loading_amount_100]
				   ,[forecast_loading_amount_200]
				   ,[forecast_loading_amount_500]
				   ,[forecast_loading_amount_2000]
				   ,[total_cassette_capacity]
				   ,[cassette_50_capacity_amount]
				   ,[cassette_100_capacity_amount]
				   ,[cassette_200_capacity_amount]
				   ,[cassette_500_capacity_amount]
				   ,[cassette_2000_capacity_amount]
				   ,[vaultingamount_50]
				   ,[vaultingamount_100]
				   ,[vaultingamount_200]
				   ,[vaultingamount_500]
				   ,[vaultingamount_2000]
				   ,[total_rounded_vault_amt]
				   ,[vault_balance_100]
				   ,[vault_balance_200]
				   ,[vault_balance_500]
				   ,[vault_balance_2000]
				   ,[total_vault_balance]
				   ,is_deno_wise_cash_available		
				   ,total_remaining_available_amount
				   ,remaining_avail_100				
				   ,remaining_avail_200				
				   ,remaining_avail_500				
				   ,remaining_avail_2000			
				   ,[indent_50]
				   ,[indent_100]
				   ,[indent_200]
				   ,[indent_500]
				   ,[indent_2000]
				   ,[record_status]
				   ,[created_on]
				   ,[created_by]
				   ,[created_reference_id]
				   ,indent_counter				   
				   )
				SELECT 	
						indent_code
					,	indentdate
					,	temp.project_id
					,	temp.bank_code
					,	temp.feeder_branch_code
					,	cra
					,	max_loading_amount
					,	max_loading_amount_50
					,	max_loading_amount_100
					,	max_loading_amount_200
					,	max_loading_amount_500
					,	max_loading_amount_2000
					,	min_loading_amount
					,	forecast_loading_amount
					,	forecast_loading_amount_50
					,	forecast_loading_amount_100
					,	forecast_loading_amount_200
					,	forecast_loading_amount_500
					,	forecast_loading_amount_2000
					,	total_cassette_capacity
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	total_rounded_vault_amt
					,	vault_balance_100
					,	vault_balance_200
					,	vault_balance_500
					,	vault_balance_2000
					,	total_vault_balance
					,	temp.is_deno_wise_cash_available		
					,	temp.total_remaining_available_amount
					,	temp.remaining_avail_100				
					,	temp.remaining_avail_200				
					,	temp.remaining_avail_500				
					,	temp.remaining_avail_2000			
					,	indent_50
					,	indent_100
					,	indent_200
					,	indent_500
					,	indent_2000
					,	'Approval Pending'
					,	@timestamp_date
					,	@cur_user
					,	@created_reference_id
					,   indent_counter + 1
				FROM #feeder_level_forecast feeder
				 LEFT JOIN #temp_feeder_forecast temp
				on feeder.bank_code COLLATE DATABASE_DEFAULT= temp.bank_code COLLATE DATABASE_DEFAULT
				AND feeder.feeder_branch_code COLLATE DATABASE_DEFAULT= temp.feeder_branch_code COLLATE DATABASE_DEFAULT
				AND feeder.project_id COLLATE DATABASE_DEFAULT = temp.project_id COLLATE DATABASE_DEFAULT


				INSERT INTO indent_master
		(  [indent_order_number]
	      ,[order_date]
	      ,[collection_date]
	      ,[replenishment_date]
	      ,[cypher_code]
	      ,[bank]
	      ,[feeder_branch]
	      ,[cra]
	      ,[total_atm_loading_amount_50]
	      ,[total_atm_loading_amount_100]
	      ,[total_atm_loading_amount_200]
	      ,[total_atm_loading_amount_500]
	      ,[total_atm_loading_amount_2000]
	      ,[total_atm_loading_amount]
	      ,[cra_opening_vault_balance_50]
	      ,[cra_opening_vault_balance_100]
	      ,[cra_opening_vault_balance_200]
	      ,[cra_opening_vault_balance_500]
	      ,[cra_opening_vault_balance_2000]
	      ,[cra_opening_vault_balance]
	      ,[total_bank_withdrawal_amount_50]
	      ,[total_bank_withdrawal_amount_100]
	      ,[total_bank_withdrawal_amount_200]
	      ,[total_bank_withdrawal_amount_500]
	      ,[total_bank_withdrawal_amount_2000]
	      ,[total_bank_withdrawal_amount]
	      ,[authorized_by_1]
	      ,[authorized_by_2]
	      ,[notes]
	      ,[amount_in_words]
	      ,[indent_type]
	      ,[project_id]
	      ,[record_status]
	      ,[created_on]
		  ,indent_counter
		  )
		  SELECT  indent_code
				 ,indentdate AS [order_date]
				 ,indentdate AS [collection_date]
				 ,indentdate AS [replenishment_date]
				 ,'PENDING_INTEGRATION' as [cypher_code] 
				 ,temp.[bank_code]
				 ,feeder.[feeder_branch_code]
				 ,[cra]
				 ,indent_50
				 ,indent_100
				 ,indent_200
				 ,indent_500
				 ,indent_2000
				 ,indent_50+indent_100+indent_200+indent_500+indent_2000 
				 , 0 AS [cra_opening_vault_balance_50]
				 , vault_balance_100   AS [cra_opening_vault_balance_100]
				 , vault_balance_200   AS [cra_opening_vault_balance_200]
				 , vault_balance_500   AS [cra_opening_vault_balance_500]
				 , vault_balance_2000  AS [cra_opening_vault_balance_2000]
				 , total_vault_balance AS [cra_opening_vault_balance]
				 , indent_50 AS [total_bank_withdrawal_amount_50]
				 , indent_100 AS [total_bank_withdrawal_amount_100]
				 , indent_200 AS [total_bank_withdrawal_amount_200]
				 , indent_500 [total_bank_withdrawal_amount_500]
				 , indent_2000 AS [total_bank_withdrawal_amount_2000]
				 , (indent_50+indent_100+indent_200+indent_500+indent_2000 ) AS [total_bank_withdrawal_amount]
				 ,'' AS [authorized_by_1]
				 ,'' AS [authorized_by_2]
				 ,'' AS [notes]
				 ,'PENDING' --dbo.fnNumberToWords((indent_50+indent_100+indent_200+indent_500+indent_2000 )) AS [amount_in_words]
				 ,'Revised Indent' AS [indent_type]
				 ,feeder.[project_id]
				 ,'Approval Pending' [record_status]
				 , @timestamp_date  AS [created_on]
				 , indent_counter + 1			 
				 from #feeder_level_forecast feeder
				 LEFT join  #temp_feeder_forecast temp 
				 on temp.bank_code  COLLATE DATABASE_DEFAULT= feeder.bank_code  COLLATE DATABASE_DEFAULT
				 and temp.project_id  COLLATE DATABASE_DEFAULT= feeder.project_id  COLLATE DATABASE_DEFAULT
				 and temp.feeder_branch_code  COLLATE DATABASE_DEFAULT= feeder.feeder_branch_code  COLLATE DATABASE_DEFAULT



				INSERT INTO indent_detail
					(  
					       indent_order_number,
					       [atm_id]
					      ,[location]
					      ,[purpose]
					      ,[loading_amount_50]
					      ,[loading_amount_100]
					      ,[loading_amount_200]
					      ,[loading_amount_500]
					      ,[loading_amount_2000]
					      ,[total]
					      ,[record_status]
					      ,[created_on]
						  ,created_by
						  ,created_reference_id
						  ,indent_counter
					)
					SELECT
						     A.indent_code ,
							 A.atm_id,
							 M.location_name, 
							 A.dist_Purpose,
							 NULL,
							 temp.loading_amount_100,
							 temp.loading_amount_200,
							 temp.loading_amount_500,
							 temp.loading_amount_2000,
							 isnull(temp.loading_amount_100,0) +	
							 isnull(temp.loading_amount_200,0) + 
							 isnull(temp.loading_amount_500,0)	   +
							 isnull(temp.loading_amount_2000,0) AS [total] ,
							 'Approval Pending' AS [record_status],
							 @timestamp_date  AS [created_on],
							 @cur_user,
							 @created_reference_id	
							 ,a.is_deno_wise_cash_available + 1		
						 FROM #dist_with_cra   A
						 LEFT JOIN atm_master M
						 ON A.site_code = M.site_code
						 AND A.atm_id  = M.atm_id
						 AND M.record_status ='Active'
						 LEFT JOIN  #temp_cash_pre_availability temp
						 on temp.atmid COLLATE DATABASE_DEFAULT = a.atm_id COLLATE DATABASE_DEFAULT
						 AND temp.bank_code COLLATE DATABASE_DEFAULT = a.bank_code COLLATE DATABASE_DEFAULT
						 AND temp.project_id COLLATE DATABASE_DEFAULT = a.project_id COLLATE DATABASE_DEFAULT



					
		  Update indent_revision_request 
		  set record_status = 'Review Pending', 
		      modified_on = @timestamp_date,
			  modified_by = @cur_user,
			  modified_reference_id = @created_reference_id
		  where indent_order_number = @active_indent_code and record_status = 'Approved'
		  
		  
		   IF (@ErrorCode = 0)
                        BEGIN
                            IF(@row > 0)
                            BEGIN
                                    SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'success') 
                                    SELECT @outputval
                            END
							ELSE
							BEGIN
									SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'no_records') 
                                    SELECT @outputval
							END
						END



END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(100) = @procedure_name
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
                        SELECT @outputval	
										
			 END CATCH
END


		 -- insert into 4 tables with record status 'Approval_Pending' and indent_counter = old + 1
		 -- distribution_planning_detail
		 -- distribution_planning_master
		 -- indent_master : If case when pre_avail_loading_amount
		 -- indent_detail



		 -- Update indent_revision_request 
		 -- set record_status = 'Review_Pending', modified_on = '', modified_by = '',modified_reference_id = ''
		 -- where indent_order_number = @active_indent_code and record_status = 'Approved'

--END TRY
--		BEGIN CATCH
--			IF(@@TRANCOUNT > 0 )
--				BEGIN
--					ROLLBACK TRAN;
--				END
--						DECLARE @ErrorNumber INT = ERROR_NUMBER();
--						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
--						DECLARE @ErrorState INT = ERROR_STATE();
--						DECLARE @ErrorProcedure varchar(50) = @procedure_name
--						DECLARE @ErrorLine INT = ERROR_LINE();
--						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
--						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
--						INSERT INTO dbo.error_log values
--							(
--								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
--							)
--						SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
--                        SELECT @outputval	
										
--			 END CATCH
--END
