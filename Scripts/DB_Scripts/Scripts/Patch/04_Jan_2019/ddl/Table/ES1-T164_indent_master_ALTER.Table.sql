DROP TABLE IF EXISTS [indent_master]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[indent_master](
	[indent_order_number] [nvarchar](255) PRIMARY KEY,
	[order_date] [date] NULL,
	[collection_date] [date] NULL,
	[replenishment_date] [date] NULL,
	[cypher_code] [nvarchar](50) NULL,
	[bank] [nvarchar](50) NULL,
	[feeder_branch] [nvarchar](50) NULL,
	[cra] [nvarchar](50) NULL,
	[total_atm_loading_amount_50] [int] NULL,
	[total_atm_loading_amount_100] [int] NULL,
	[total_atm_loading_amount_200] [int] NULL,
	[total_atm_loading_amount_500] [int] NULL,
	[total_atm_loading_amount_2000] [int] NULL,
	[total_atm_loading_amount] [int] NULL,
	[cra_opening_vault_balance_50] [int] NULL,
	[cra_opening_vault_balance_100] [int] NULL,
	[cra_opening_vault_balance_200] [int] NULL,
	[cra_opening_vault_balance_500] [int] NULL,
	[cra_opening_vault_balance_2000] [int] NULL,
	[cra_opening_vault_balance] [int] NULL,
	[total_bank_withdrawal_amount_50] [int] NULL,
	[total_bank_withdrawal_amount_100] [int] NULL,
	[total_bank_withdrawal_amount_200] [int] NULL,
	[total_bank_withdrawal_amount_500] [int] NULL,
	[total_bank_withdrawal_amount_2000] [int] NULL,
	[total_bank_withdrawal_amount] [int] NULL,
	[authorized_by_1] [nvarchar](50) NULL,
	[authorized_by_2] [nvarchar](50) NULL,
	[notes] [nvarchar](max) NULL,
	[amount_in_words] [nvarchar](500) NULL,
	[indent_type] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [datetime] NULL,
	[approved_trigger_reference_id] [datetime] NULL,
	[indent_counter] BIGINT NULL,	
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	previous_indent_code nvarchar(255) NULL
)
GO