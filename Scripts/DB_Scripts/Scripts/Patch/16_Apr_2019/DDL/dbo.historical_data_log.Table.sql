use epstar
CREATE TABLE [dbo].[historical_data_log](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[data_for_type] [nvarchar](50) NULL,
	[data_for_datetime] [datetime] NULL,
	[file_name] [nvarchar](100) NULL,
	[status] [nvarchar](500) NULL,
	[level] [nvarchar](200) NULL,
	[file_status] [nvarchar](200) NULL,
	[bank_code] [nvarchar](100) NULL,
	[project_id] [nvarchar](100) NULL,
	[region] [nvarchar](50) NULL,
	[is_valid_file] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[validation_code] [nvarchar](50) NULL
)
