 
/****** Object:  StoredProcedure [dbo].[usp_DispenseValidationUBI]    Script Date: 06-02-2019 15:19:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_DispenseValidationUBI]
( 
	@datafor_date_time varchar(50) ,@projectid varchar(50),@region varchar(50),@ForRecordstatus varchar(50),@referenceid varchar(50),@systemUser varchar(50),@Debug BIT = 0,@outputVal VARCHAR(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime =  DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50)
	DECLARE @errorcode nvarchar(max)
	DECLARE @bankcode nvarchar(10) = 'UBI'
    DECLARE @datafor nvarchar(10) = 'Dispense'
    DECLARE @activestatus nvarchar(20) = 'Active'
	DECLARE @approvedstatus nvarchar(20) = 'Approved'
	DECLARE @uploadedstatus nvarchar(20) = 'Uploaded'


	IF (@Debug = 1)
    BEGIN
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
        PRINT '-- Execution of [dbo].[usp_DispenseValidationUBI] Started.';
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';
    END;

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	 values ('Execution of [dbo].[usp_DispenseValidationUBI] Started',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


------------------ Check if file is present in Data Update Log---------------

	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[data_update_log] WITH (NOLOCK)
				WHERE [datafor_date_time] = @datafor_date_time AND 
					  bank_code = @bankcode AND 
					  data_for_type = @datafor and 
					  record_status = @ForRecordstatus AND
					  project_id = @projectid
					  AND created_reference_id = @referenceid
					  AND region = @region
				)
				
		BEGIN

			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Checking in Data Update Log Table ';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in Data Update Log Table ',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			
------------------ Check if file is present in Bank Wise File Table ----------------------

			IF EXISTS( Select 1 as ColumnName
						FROM [dbo].[cash_dispense_file_ubi] WITH (NOLOCK)
						WHERE [datafor_date_time] = @datafor_date_time AND 
							  record_status = @ForRecordstatus  
					          --project_id = @projectid 
							  AND created_reference_id = @referenceid
							 
				)
				
				BEGIN
					IF (@Debug = 1)
					BEGIN
					    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
					    PRINT '-- Checking in [dbo].[cash_dispense_file_ubi]  ';
					    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
					END;
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in [dbo].[cash_dispense_file_ubi]',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
---------------- Select total count of records present in UBI file for particular date and status------------
				
				SET @CountTotal = (	
									SELECT count(1) 
									FROM [dbo].[cash_dispense_file_ubi] WITH (NOLOCK)
						            WHERE [datafor_date_time] = @datafor_date_time 
										  and record_status = @ForRecordstatus 
										  --AND project_id = @projectid 
										  AND created_reference_id = @referenceid
										 
					
								   ) 

--------------------- Creating temporary table to process multiple update operations ---------------------------
	
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Creating Temporary Table and storing data ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Creating Temporary Table and storing data',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				SELECT * 
				INTO #temp_cash_dispense_file_ubi 
				FROM [dbo].[cash_dispense_file_ubi] WITH (NOLOCK)
				WHERE datafor_date_time= @datafor_date_time
					  AND record_status = @ForRecordstatus  
					  --AND project_id = @projectid
					  AND created_reference_id = @referenceid
					   
			
----------------------------------------Validation 1--------------------------		
--							Check For ATM status in Master Data				--
------------------------------------------------------------------------------
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for AMS master Started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Started',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


				UPDATE #temp_cash_dispense_file_ubi 
				SET     error_code =  
					CASE 
						WHEN (	atm_id IN (
												SELECT atm_id from 
												atm_master where record_status = @activestatus
												and bank_code = @bankcode 
											 
											 )
							 )
						THEN NULL
						ELSE
							(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CDR_Validaiton' and sub_category = 'atm_not_found')
					END	
												  

		
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for AMS master Completed ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Completed',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
										  
------------------------------------------Validation 2------------------------------------		
----		           Validation for Dispense amount is not in divisible of 100'		  --
------------------------------------------------------------------------------------------	
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Dispense amount is not in divisible of 100/ Negative';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Dispense amount is not in divisible of 100 / Negative started',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				UPDATE #temp_cash_dispense_file_ubi  
				SET     error_code =  
					CASE  
						WHEN (type_01+type_02+type_03+type_04)%100 <> 0 OR (type_01+type_02+type_03+type_04)<0
						THEN
							CASE
								WHEN (error_code IS NULL)
								THEN	
									(SELECT cast(sequence as varchar(50)) FROM app_config_param
									 where category = 'CDR_Validaiton' 
									 and sub_category = 'amount_dispensed')
			 					ELSE 
									CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CDR_Validaiton' and sub_category = 'amount_dispensed')) as varchar(max))
							END
						ELSE error_code
					END								 

				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Dispense amount is not in divisible of 100 / Negative'
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Dispense amount is not in divisible of 100 / Negative Completed',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

-----------Updating is_valid_record column as Yes or No-------------

			UPDATE #temp_cash_dispense_file_ubi
			SET    is_valid_record = 
					CASE 
						WHEN error_code IS NULL
						THEN 'Yes'
						ELSE 'No'
					END		
			
			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Is Valid Record Field Updated';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Is Valid Record Field Updated',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
-----------------Checking count for valid records in updated temporary table -----------------------------

			SET @countCalculated = (
									SELECT count(1) 
									FROM #temp_cash_dispense_file_ubi  
									WHERE is_valid_record = 'Yes' 
									)

-----------------Comparing both the counts ---------------------------------------------------------

			IF(@countCalculated != @CountTotal AND @countCalculated > 0)
				BEGIN
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Count Mismatch (Partial Valid File)';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
			
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'Exception'
										 and sub_category = 'Partial Valid'
									 )				
				END
			ELSE IF (@countCalculated = @CountTotal)
				BEGIN 
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Count Matched (Valid File)';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				SET @outputVal = (
								SELECT sequence from  [dbo].[app_config_param]
								where  category = 'File Operation' and sub_category = 'Data Validation Successful'
							)												
				END

			ELSE
				BEGIN
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- No valid record found in table';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
									 )				
			END

------------------------Inserting distinct error code in new temporary table. Also splitting with , ------------------------

			IF OBJECT_ID('tempdb..#temp_distinct_codes') IS NOT NULL
			BEGIN
				DROP TABLE #temp_distinct_codes
			END
			ELSE
			BEGIN
				DECLARE @Names VARCHAR(max) 
				;with distinct_error_codes
				as
				(
					select distinct error_code from #temp_cash_dispense_file_ubi
					where error_code is not null
				)
				SELECT @Names = COALESCE(@Names + ',','') +  TRIM(error_code)
				FROM  distinct_error_codes

				SELECT DISTINCT VALUE INTO #temp_distinct_codes FROM string_split (@Names,',') 
				
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Temporary Table Created for storing error code';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Temporary Table Created for storing error code', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

--------------------- Creating one single error code string with , seperated delimiter---------------------------------------				
				SET @errorcode = (
								SELECT 
									Stuff((
										SELECT N', ' + VALUE FROM #temp_distinct_codes FOR XML PATH(''),TYPE)
										.value('text()[1]','nvarchar(max)'),1,2,N''
										)
								)
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Error code String Created';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;			
			END			
			

------------------------- Updating columns in bank wise file using temporary table------------------------------------------

				UPDATE UBI 
				SET UBI.is_valid_record =  b.is_valid_record,
					UBI.error_code = b.error_code,
					record_status = @approvedstatus
						
				FROM [cash_dispense_file_ubi] UBI
					INNER JOIN #temp_cash_dispense_file_ubi b
					on b.atm_id = UBI.atm_id				  
					AND UBI.record_status = @ForRecordstatus
					AND UBI.created_reference_id = @referenceid

			
			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Columns has been updated in bank wise file';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
		

			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Updating columns in data update log ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;

------------------------- Updating columns in Data Update Log table------------------------------------------
				
				
				UPDATE dbo.data_update_log
				SET is_valid_file =
					CASE	
							WHEN @outputVal = 50009 
							THEN  1
							WHEN @outputVal = 50001
							THEN 0
							WHEN @outputVal = 10001
							THEN 0
					END,
					validation_code = (SELECT @errorcode),
					record_status = 
					CASE	
							WHEN @outputVal = 50009 
							THEN @approvedstatus
							WHEN @outputVal = 50001
							THEN @approvedstatus
							WHEN @outputVal = 10001
							THEN @uploadedstatus
					END,	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				WHERE [datafor_date_time] =  @datafor_date_time
					AND record_status = @ForRecordstatus 
				 
					AND bank_code = @bankcode 
					AND	data_for_type = @datafor
					AND project_id = @projectid
					AND created_reference_id = @referenceid

			--COMMIT TRAN;

			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Transaction Completed....Column has been updated in data update log ';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Completed....Column has been updated in data update log and bank wise file',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Execution of [dbo].[usp_DispenseValidationUBI] Completed.';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[usp_DispenseValidationUBI] Completed',  DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


		END
			ELSE
			BEGIN
				  RAISERROR (50002, 16,1);
			END
		END
	ELSE
		BEGIN
			RAISERROR (	50004, 16,1);
		END
END

