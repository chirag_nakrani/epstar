/****** Object:  StoredProcedure [dbo].[uspDataValidationAMSAtmMaster]    Script Date: 28-12-2018 20:39:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 


ALTER PROCEDURE [dbo].[uspDataValidationAMSAtmMaster]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime =  DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @procedurename varchar(max)= (SELECT OBJECT_NAME(@@PROCID))


 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	 values ('Execution of [dbo].[uspDataValidationAMSAtmMaster] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

	-- Checking for any record that is present in uploaded status in table
	
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].atm_master
				WHERE record_status = 'Uploaded'
			 )
				
			BEGIN



IF EXISTS(SELECT 1 FROM dbo.atm_master WHERE record_status = 'Active')
	BEGIN
	IF (@api_flag = 'F')

	-- Check for API Flag if it is for File Upload or screen edit
	-- 'F' - File Upload ,  else - Screen Edit

		BEGIN
		
		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	   	values ('Update in ATM Master started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


		---- Updating each column field in case of null and not null

			UPDATE a
			set a.old_atm_id								=						COALESCE(a.old_atm_id							, b.old_atm_id							),
				a.atm_band									=						COALESCE(a.atm_band								, b.atm_band							),				
				a.ej_docket_no								=						COALESCE(a.ej_docket_no							, b.ej_docket_no						),
				a.grouting_status							=						COALESCE(a.grouting_status						, b.grouting_status						),
				a.brand										=						COALESCE(a.brand								, b.brand								),
				a.vsat_id									=						COALESCE(a.vsat_id								, b.vsat_id								),
				a.vendor_name								=						COALESCE(a.vendor_name							, b.vendor_name							),
				a.serial_no									=						COALESCE(a.serial_no							, b.serial_no							),
				a.current_deployment_status					=						COALESCE(a.current_deployment_status			, b.current_deployment_status			),
				a.tech_live_date							=						COALESCE(a.tech_live_date						, b.tech_live_date						),
				a.cash_live_date							=						COALESCE(a.cash_live_date						, b.cash_live_date						),
				a.insurance_limits							=						COALESCE(a.insurance_limits						, b.insurance_limits					),
				a.project_id								=						COALESCE(a.project_id							, b.project_id							),
				a.bank_name									=						COALESCE(a.bank_name							, b.bank_name							),
				a.bank_code									=						COALESCE(a.bank_code							, b.bank_code							),
				a.circle_zone_region						=						COALESCE(a.circle_zone_region					, b.circle_zone_region					),
				a.site_code_2015_16							=						COALESCE(a.site_code_2015_16					, b.site_code_2015_16					),
				a.site_code_2016_17							=						COALESCE(a.site_code_2016_17					, b.site_code_2016_17					),
				a.state										=						COALESCE(a.state								, b.state								),
				a.district									=						COALESCE(a.district								, b.district							),
				a.city										=						COALESCE(a.city									, b.city								),
				a.site_address_line_1						=						COALESCE(a.site_address_line_1					, b.site_address_line_1					),
				a.site_address_line_2						=						COALESCE(a.site_address_line_2					, b.site_address_line_2					),
				a.pincode									=						COALESCE(a.pincode								, b.pincode								),
				a.site_category								=						COALESCE(a.site_category						, b.site_category						),
				a.site_type									=						COALESCE(a.site_type							, b.site_type							),
				a.installation_type							=						COALESCE(a.installation_type					, b.installation_type					),
				a.site_status								=						COALESCE(a.site_status							, b.site_status							),
				a.channel_manager_name						=						COALESCE(a.channel_manager_name					, b.channel_manager_name				),
				a.channel_manager_contact_no				=						COALESCE(a.channel_manager_contact_no			, b.channel_manager_contact_no			),
				a.location_name								=						COALESCE(a.location_name						, b.location_name						),
				a.atm_cash_removal_date						=						COALESCE(a.atm_cash_removal_date				, b.atm_cash_removal_date				),
				a.atm_ip									=						COALESCE(a.atm_ip								, b.atm_ip								),
				a.switch_ip									=						COALESCE(a.switch_ip							, b.switch_ip							),
				a.external_camera_installation_status		=						COALESCE(a.external_camera_installation_status	, b.external_camera_installation_status	),
				a.atm_owner									=						COALESCE(a.atm_owner							, b.atm_owner							),
				a.stabilizer_status							=						COALESCE(a.stabilizer_status					, b.stabilizer_status					),
				a.ups_capacity								=						COALESCE(a.ups_capacity							, b.ups_capacity						),
				a.no_of_batteries							=						COALESCE(a.no_of_batteries						, b.no_of_batteries						),
				a.ups_battery_backup						=						COALESCE(a.ups_battery_backup					, b.ups_battery_backup					),
				a.load_shedding_status						=						COALESCE(a.load_shedding_status					, b.load_shedding_status				),
				a.solar_dg									=						COALESCE(a.solar_dg								, b.solar_dg							),
				a.date_created								=						COALESCE(a.date_created							, b.date_created						),
				a.date_updated								=						COALESCE(a.date_updated							, b.date_updated						),
				a.date_deleted								=						COALESCE(a.date_deleted							, b.date_deleted						),
				a.is_deleted								=						COALESCE(a.is_deleted							, b.is_deleted							),
				a.record_status = 'Approval Pending',
				a.modified_on = @current_datetime_stmp,
				a.modified_by = @systemUser,
				a.modified_reference_id = @referenceid
			 from  atm_master a inner join atm_master b on a.atm_id = b.atm_id and a.site_code = b.site_code and 
			  a.record_status = 'Uploaded' and b.record_status = 'Active'

			   INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			 values ('Update in ATM Master completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			END
	ELSE
		BEGIN
		
			-- Screen Edit Scenario --
			-- For screen edit we have to directly update the record status from uploaded to approval pending

			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			values ('Update for screen edit scenario started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			DECLARE @tableName VARCHAR(30) = 'dbo.atm_master'
			DECLARE @ForStatus1 VARCHAR(30) = 'Uploaded'
			DECLARE @ToStatus1 VARCHAR(30) =  'Approval Pending'
			DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''

			EXEC  dbo.[uspSetStatus] @tableName,@ForStatus1,@ToStatus1,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
			UPDATE data_update_log_master 
			SET pending_count =	
					(
						SELECT COUNT(*) FROM dbo.atm_master WHERE created_reference_id = @referenceid
						and record_status = 'Approval Pending'
					)	
			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			 values ('Update for screen edit scenario Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


		END
	END
	ELSE 
		BEGIN
				-- If there are no records in active status then directly update record status to approval pending 
				 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
				 values ('Update in atm master started without active records', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
				
				DECLARE @tableName2 VARCHAR(30) = 'dbo.atm_master'
				DECLARE @ForStatus2 VARCHAR(30) = 'Uploaded'
				DECLARE @ToStatus2 VARCHAR(30) =  'Approval Pending'
				DECLARE @SetWhereClause2 VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''''

				EXEC  dbo.[uspSetStatus] @tableName2,@ForStatus2,@ToStatus2,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause2,@out OUTPUT
				UPDATE data_update_log_master 
				SET pending_count =	
						(
							SELECT COUNT(*) FROM dbo.atm_master WHERE created_reference_id = @referenceid
							and record_status = 'Approval Pending'
						)

				 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
				 values ('Update in atm master Completed without active records', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
				
			END
			

			SET @outputVal = 'S101'
				
	END
						
						ELSE
						
						BEGIN
							--SELECT 'In rasierror 90002'
							RAISERROR(90002,16,1)
						END
	END
						
			--END

