USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspDataValidationBOB]    Script Date: 5/22/2019 12:29:48 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspDataValidationBOB]
( 
	@datafor_date_time varchar(50) ,@projectid varchar(50),@region varchar(50),@ForRecordstatus varchar(50),@referenceid varchar(50),@systemUser varchar(50),@Debug BIT = 0,@outputVal VARCHAR(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50)
	DECLARE @errorcode nvarchar(max)
	DECLARE @bankcode nvarchar(10) = 'BOB'
	DECLARE @datafor nvarchar(10) = 'CBR'
    DECLARE @activestatus nvarchar(20) = 'Active'
	DECLARE @approvedstatus nvarchar(20) = 'Approved'
	DECLARE @uploadedstatus nvarchar(20) = 'Uploaded'

	IF (@Debug = 1)
    BEGIN
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
        PRINT '-- Execution of [dbo].[uspDataValidationBOB] Started.';
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';
    END;

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
	 values ('Execution of [dbo].[uspDataValidationBOB] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


------------------ Check if file is present in Data Update Log---------------

    IF EXISTS( Select 1 as ColumnName
                FROM [dbo].[data_update_log] WITH (NOLOCK)
                WHERE [datafor_date_time] = @datafor_date_time AND 
                      bank_code = @bankcode AND 
                      data_for_type = @datafor and 
                      record_status = @ForRecordstatus AND
                      project_id = @projectid AND 
					  created_reference_id = @referenceid 
					  AND region = @region
                )
                
        BEGIN

			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Checking in Data Update Log Table ';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
			 values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			
------------------ Check if file is present in Bank Wise File Table ----------------------

			IF EXISTS( 
					Select 1 as ColumnName
					FROM [dbo].[cash_balance_file_bob] WITH (NOLOCK)
					WHERE [datafor_date_time] = @datafor_date_time AND 
					record_status = @ForRecordstatus and
                              		--AND project_id = @projectid AND
					created_reference_id = @referenceid 
					--AND region = @region
	                 	 )
                
                BEGIN
                    IF (@Debug = 1)
                    BEGIN
                        PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                        PRINT '-- Checking in [dbo].[cash_balance_file_bob]  ';
                        PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                    END;
                    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
                    values ('Checking in [dbo].[cash_balance_file_bob]', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
---------------- Select total count of records present in bob file for particular date and status------------
                
                SET @CountTotal = (    
                                    SELECT count(1) 
                                    FROM [dbo].[cash_balance_file_bob] WITH (NOLOCK)
                                    WHERE [datafor_date_time] = @datafor_date_time 
                                          and record_status = @ForRecordstatus 
                                          --AND project_id = @projectid 
					  AND created_reference_id = @referenceid 
					  --AND region = @region
                                   )		

--------------------- Creating temporary table to process multiple update operations ---------------------------
	
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Creating Temporary Table and storing data ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Creating Temporary Table and storing data', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				SELECT * 
				INTO #temp_cash_balance_file_bob 
				FROM [dbo].[cash_balance_file_bob] WITH (NOLOCK)
				WHERE datafor_date_time= @datafor_date_time
					  AND record_status = @ForRecordstatus  
					  --AND project_id = @projectid
					  AND created_reference_id = @referenceid
					  --AND region = @region

            
----------------------------------------Validation 1--------------------------        
--                            Check For ATM status in Master Data                --
------------------------------------------------------------------------------
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for AMS master Started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


				UPDATE #temp_cash_balance_file_bob 
				SET   error_code =  
						CASE 
							WHEN (	atmid IN (
												SELECT atm_id FROM 
												atm_master WITH(NOLOCK) WHERE site_status IN ('Live','Active')
												AND record_status = @activestatus
												AND bank_code = @bankcode 
												--AND project_id = @projectid
											)
								 )
							THEN NULL
							ELSE
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'atm_not_live')
						END	


		
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for AMS master Completed ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for AMS master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
										  
----------------------------------------Validation 2------------------------------------		
--		           Hopper wise balance should match with the ending balance			  --
----------------------------------------------------------------------------------------	
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Hopper wise balance should match with the ending balance Started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Hopper wise balance should match with the ending balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				UPDATE #temp_cash_balance_file_bob 
				SET   error_code =  
				CASE  
					WHEN hop1endcash+hop2endcash+hop3endcash+hop4endcash <> endcash 
					THEN	
						CASE
							WHEN (error_code IS NULL)
							THEN	
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Amount Mismatch')
				 			ELSE 
								CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Amount Mismatch')) as varchar(max))
						END
					ELSE error_code
				END


				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Hopper wise balance should match with the ending balance Completed';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Hopper wise balance should match with the ending balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
----------------------------------------Validation 3----------------------------	
------			1.switch balance = 0			
------			2.switch balance < 0
------			3.switch balance is in decimal values
------			4.denomination wise value does not match with the available data
--------------------------------------------------------------------------------
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Switch balance check started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Switch balance check started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	
				UPDATE #temp_cash_balance_file_bob
				SET   error_code =  
				CASE 
					WHEN	endcash = 0
						OR  endcash < 0
						OR	endcash <> FLOOR(endcash)
						OR	( hop1endcash % 100 <> 0) 
						OR 
							(
								CASE WHEN hop2bill = 200 
										THEN hop2endcash%200 
									 ELSE hop2endcash%100
									 END
							)<>0
						 OR (hop3endcash % 500 <> 0) 
						 OR (hop4endcash % 2000 <> 0) 
					THEN	
						CASE
							WHEN (error_code IS NULL)
							THEN	
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'numeric_validation')
				 			ELSE 
								CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'numeric_validation')) as varchar(max))
						END
					ELSE error_code
				   END		

										
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Switch banalnce check Completed';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;	
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Switch balance check Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	
-------------------------------------------Validation 4 ---------------------------------		
------			1.switch balance > cassette capacity			
------			2.switch balance > Bank Limit (In case of UBI and CBI only)
------			3.switch balance > Insurance Limit
-----------------------------------------------------------------------------------------
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Cassette Capacity, Insurance Limit, Bank Limit check started';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Cassette Capacity, Insurance Limit, Bank Limit check started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
					

				UPDATE #temp_cash_balance_file_bob						
				SET     error_code =  
					CASE 
						WHEN	endcash >
								( 
                                        select (COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) *	COALESCE(bc.capacity_50,  0)  *50)  +
                                         (COALESCE(cc.cassette_100_count, clm.cassette_100_count,0) *		COALESCE(bc.capacity_100, sa.deno_100_bill_capacity ) *100) +
                                         (COALESCE(cc.cassette_200_count, clm.cassette_200_count,0) *		COALESCE(bc.capacity_200, sa.deno_200_bill_capacity ) *200) +       
                                         (COALESCE(cc.cassette_500_count, clm.cassette_500_count,0) *		COALESCE(bc.capacity_500, sa.deno_500_bill_capacity ) *500) +
                                         (COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0) *		COALESCE(bc.capacity_2000,sa.deno_2000_bill_capacity)*2000) 
                                         AS total_cassette_capacity
                                         FROM atm_master AS  mast
                                              INNER JOIN     ATM_Config_limits clm
                                              ON            mast.atm_id = clm.atm_id  AND
                                                            mast.site_code = clm.site_code AND
                                                            clm.record_status = @activestatus                              
                                              LEFT JOIN      
                                                   (
                                                   SELECT 
                                                        project_id,
                                                        bank_code, 
                                                        site_code,
                                                        atm_id, 
                                                        cassette_50_count, 
                                                        cassette_100_count, 
                                                        cassette_200_count,
                                                        cassette_500_count,
                                                        cassette_2000_count 
                                                    FROM Modify_cassette_pre_config 
                                                    WHERE @timestamp_date between from_date AND to_date
                                                    AND record_status = @activestatus
                                                   )cc
                                              ON clm.site_code = cc.site_code AND 
                                                 clm.atm_id = cc.atm_id
												 CROSS JOIN system_settings sa
                                                 LEFT JOIN Brand_Bill_Capacity bc ON 
                                                           bc.brand_code = mast.brand 
                                                           AND bc.record_status = @activestatus
                                                  WHERE mast.record_status = @activestatus
                                                  and mast.bank_code = @bankcode
                                                  --AND mast.project_id = @projectid 
												  AND mast.atm_id = #temp_cash_balance_file_bob.atmid
												  AND sa.record_status = @activestatus 
								)																					
							OR  endcash >
								(
									CASE 
                                        WHEN 
										( 
											SELECT insurance_limit 
											from ATM_Config_limits atm 
											where atm.atm_id = #temp_cash_balance_file_bob.atmid 
											AND atm.record_status = @activestatus
											AND bank_code = @bankcode
											--and project_id = @projectid 
											) IS NOT NULL 
										OR 
										( 
											SELECT insurance_limit 
											from ATM_Config_limits atm 
											where atm.atm_id = #temp_cash_balance_file_bob.atmid 
											AND atm.record_status = @activestatus
											AND bank_code = @bankcode 
											--and project_id = @projectid 
										) <> 0
                                        THEN
                                        (
											SELECT insurance_limit 
											from ATM_Config_limits atm 
											where atm.atm_id = #temp_cash_balance_file_bob.atmid 
											AND atm.record_status = @activestatus
											AND bank_code = @bankcode
											--and project_id = @projectid
										)
                                        END
								)	 
						THEN	
							CASE
								WHEN (error_code IS NULL)
								THEN	
									(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'exceeding_limits')
				 				ELSE 
									CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'exceeding_limits')) as varchar(max))
							END
						ELSE error_code
				   END		

				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Validation for Cassette Capacity, Insurance Limit, Bank Limit check Completed';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Cassette Capacity, Insurance Limit, Bank Limit check Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
 -----------Updating is_valid_record column as Yes or No-------------

			UPDATE #temp_cash_balance_file_bob
			SET    is_valid_record = 
					CASE 
						WHEN error_code IS NULL
						THEN 'Yes'
						ELSE 'No'
					END		
			
			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Is Valid Record Field Updated';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Is Valid Record Field Updated', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
-----------------Checking count for valid records in updated temporary table -----------------------------

			SET @countCalculated = (
									SELECT count(1) 
									FROM #temp_cash_balance_file_bob  
									WHERE is_valid_record = 'Yes' 
									)

-----------------Comparing both the counts ---------------------------------------------------------

			IF(@countCalculated != @CountTotal AND @countCalculated > 0)
				BEGIN
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Count Mismatch (Partial Valid File)';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
			
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]																						
										where category = 'Exception' 
										and sub_category = 'Partial Valid'
									 )				
				END
			ELSE IF (@countCalculated = @CountTotal)
				BEGIN 
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Count Matched (Valid File)';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				SET @outputVal = (
								SELECT sequence from  [dbo].[app_config_param]
								where  category = 'File Operation' 
								and sub_category = 'Data Validation Successful'
							)												
				END

			ELSE
				BEGIN
				IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- No valid record found in table';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
				
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
									 )				
			END

------------------------Inserting distinct error code in new temporary table. Also splitting with , ------------------------

            IF OBJECT_ID('tempdb..#temp_distinct_codes') IS NOT NULL
            BEGIN
                DROP TABLE #temp_distinct_codes
            END
            ELSE
            BEGIN
                DECLARE @Names VARCHAR(max) 
				;with distinct_error_codes
				as
				(
					select distinct error_code from #temp_cash_balance_file_bob
					where error_code is not null
				)
				SELECT @Names = COALESCE(@Names + ',','') +  TRIM(error_code)
				FROM  distinct_error_codes

				SELECT DISTINCT VALUE INTO #temp_distinct_codes FROM string_split (@Names,',')
               
			    IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Temporary Table Created for storing error code';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
                 values ('Temporary Table Created for storing error code', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

--------------------- Creating one single error code string with , seperated delimiter---------------------------------------                
                SET @errorcode = (
                                SELECT 
                                    Stuff((
                                        SELECT N', ' + VALUE FROM #temp_distinct_codes FOR XML PATH(''),TYPE)
                                        .value('text()[1]','nvarchar(max)'),1,2,N''
                                        )
                                )
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Error code String Created';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;            
            END            
            
            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
             values ('Transaction Started.... Updating columns in bank wise file and data update log', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)        
            BEGIN TRAN

			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Transaction Started.... Updating columns in bank wise file';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;	


------------------------- Updating columns in bank wise file using temporary table------------------------------------------

				UPDATE bob 
				SET bob.is_valid_record =  b.is_valid_record,
					bob.error_code = b.error_code,
					record_status = @approvedstatus
				FROM [cash_balance_file_bob] bob
					INNER JOIN #temp_cash_balance_file_bob b
					on b.atmid = bob.atmid
					--AND bob.region = @region
					--AND bob.project_id = @projectid
					AND bob.record_status = @ForRecordstatus
					AND bob.created_reference_id = @referenceid

			
			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Columns has been updated in bank wise file';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;
		

			IF (@Debug = 1)
				BEGIN
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				    PRINT '-- Updating columns in data update log ';
				    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				END;

------------------------- Updating columns in Data Update Log table------------------------------------------
				
				
				UPDATE dbo.data_update_log
				SET is_valid_file =
					CASE	
							WHEN @outputVal = 50009 
							THEN  1
							WHEN @outputVal = 50001
							THEN 0
							WHEN @outputVal = 10001
							THEN 0
					END,
					validation_code = (SELECT @errorcode),
					record_status = 
					CASE	
							WHEN @outputVal = 50009 
							THEN @approvedstatus
							WHEN @outputVal = 50001
							THEN @approvedstatus
							WHEN @outputVal = 10001
							THEN @uploadedstatus
					END,	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				WHERE [datafor_date_time] =  @datafor_date_time
					AND record_status = @ForRecordstatus
					--AND region = @region 
					AND bank_code = @bankcode 
					AND	data_for_type = @datafor
					AND project_id = @projectid
					AND created_reference_id = @referenceid


			COMMIT TRAN;

			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Transaction Completed....Column has been updated in data update log ';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Completed....Column has been updated in data update log and bank wise file', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			IF (@Debug = 1)
			BEGIN
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			    PRINT '-- Execution of [dbo].[uspDataValidationBOB] Completed.';
			    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[uspDataValidationBOB] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


		END
			ELSE
			BEGIN
				  RAISERROR (50002, 16,1);
			END
		END
	ELSE
		BEGIN
			RAISERROR (	50004, 16,1);
		END
END

