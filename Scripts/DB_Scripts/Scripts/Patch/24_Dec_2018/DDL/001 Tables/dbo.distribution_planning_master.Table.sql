/****** Object:  Table [dbo].[distribution_planning_master]    Script Date: 2018-12-24 12:52:54 ******/
DROP TABLE IF EXISTS [distribution_planning_master]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[distribution_planning_master](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[indent_code] [nvarchar](255) NULL,
	[indentdate] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[feeder_branch_code] [nvarchar](50) NULL,
	[cra] [nvarchar](50) NULL,
	[max_loading_amount] [float] NULL,
	[max_loading_amount_50] [float] NULL,
	[max_loading_amount_100] [float] NULL,
	[max_loading_amount_200] [float] NULL,
	[max_loading_amount_500] [float] NULL,
	[max_loading_amount_2000] [float] NULL,
	[min_loading_amount] [numeric](38, 1) NULL,
	[forecast_loading_amount] [float] NULL,
	[forecast_loading_amount_50] [float] NULL,
	[forecast_loading_amount_100] [int] NULL,
	[forecast_loading_amount_200] [int] NULL,
	[forecast_loading_amount_500] [int] NULL,
	[forecast_loading_amount_2000] [int] NULL,
	[total_cassette_capacity] [float] NULL,
	[cassette_50_capacity_amount] [float] NULL,
	[cassette_100_capacity_amount] [float] NULL,
	[cassette_200_capacity_amount] [float] NULL,
	[cassette_500_capacity_amount] [float] NULL,
	[cassette_2000_capacity_amount] [float] NULL,
	[vaultingamount_50] [float] NULL,
	[vaultingamount_100] [int] NULL,
	[vaultingamount_200] [int] NULL,
	[vaultingamount_500] [int] NULL,
	[vaultingamount_2000] [int] NULL,
	[total_rounded_vault_amt] [float] NULL,
	[vault_balance_100] [int] NULL,
	[vault_balance_200] [int] NULL,
	[vault_balance_500] [int] NULL,
	[vault_balance_2000] [int] NULL,
	[total_vault_balance] [int] NULL,
	[indent_50] [float] NULL,
	[indent_100] [int] NULL,
	[indent_200] [int] NULL,
	[indent_500] [int] NULL,
	[indent_2000] [int] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	indent_counter BIGINT NULL
)
GO


