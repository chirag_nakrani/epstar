
DROP TABLE IF EXISTS [indent_detail]

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[indent_detail](
	[indent_order_number] [nvarchar](255) NOT NULL,
	[sr_no] [int] NULL,
	[atm_id] [nvarchar](50) NOT NULL,
	[general_ledger_account_number] [nvarchar](50) NULL,
	[location] [nvarchar](255) NULL,
	[purpose] [nvarchar](255) NULL,
	[loading_amount_50] [bigint] NULL,
	[loading_amount_100] [bigint] NULL,
	[loading_amount_200] [bigint] NULL,
	[loading_amount_500] [bigint] NULL,
	[loading_amount_2000] [bigint] NULL,
	[total] [bigint] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	indent_counter BIGINT NULL
 CONSTRAINT [PK_indent_detail] PRIMARY KEY CLUSTERED 
(
	[indent_order_number] ASC,
	[atm_id] ASC
)WITH ( IGNORE_DUP_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


