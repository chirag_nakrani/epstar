USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspSetStatus]    Script Date: 25-12-2018 12:32:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uspSetStatus]
			(
			 @tablename  varchar(100),			 
			 @ForStatus   varchar (30),
             @ToStatus    varchar(30) ,
			 @date        varchar(50)    ,
			 @statusby    varchar(50) , -- e.g approvedby,deletedby
			 @referenceid nvarchar(50),			
			 @whereclause nvarchar(max),
			 @outputVal varchar(100) OUTPUT
			 )

AS

 DECLARE             @sql_set_p2  varchar(max)
 DECLARE             @sqls_p1 varchar(max)
 DECLARE             @sql_set_p3 varchar(max)
 DECLARE             @sql_where varchar(max)
 DECLARE             @strQuery varchar(max)
 DECLARE			 @NumRowsChanged int
 DECLARE			 @errorcode int
-- DECLARE		@tablename  varchar(30) = 'ATM_Config_limits'			 
--DECLARE			 @ForStatus   varchar (30)='Approval Pending'
--DECLARE             @ToStatus    varchar(30) = 'Active' 
--DECLARE			 @date        varchar(50)  ='2018-10-24 18:02:48' 
--DECLARE			 @statusby    varchar(50)  = 'cashops1'-- e.g approvedby,deletedby
--DECLARE			 @referenceid nvarchar(50)	='C10000032800756'
--DECLARE @whereclause varchar(max) = =''' created_reference_id = ''' + @referenceid +'';

--DECLARE			 @whereclause nvarchar(max) = ' bank_code = ''BOMH'' and datafor_date_time = ''2018-10-16 08:00:00'' and data_for_type = ''CBR'' and project_id = ''MOF'' '


		BEGIN
		SET NOCOUNT ON
	--	insert into error_log (errorMessage) values (@tablename)

			 	SET @sqls_p1 = 'update ' +  @tablename + ' set record_status =''' + @ToStatus
			    SET @sql_where = ' where record_status = ''' + @ForStatus 
				               + ''' and ' + @whereclause
		--	insert into error_log (errorMessage) values (@sqls_p1)
				--insert into error_log (errorMessage) values (@sql_where)
				IF  @ToStatus='Approved'
				begin
					SET @sql_set_p2 = @sqls_p1 + ''', approved_on = ''' + cast(@date as varchar(30))
					SET @sql_set_p2 = @sql_set_p2 + ''', approved_by = ''' + @statusby
					SET @sql_set_p2 = @sql_set_p2 + ''', approved_reference_id = ''' + @referenceid +''		

				END
				
				ELSE IF  @ToStatus='Approval Pending'
				begin
					IF @tablename = 'dbo.data_update_log'
					BEGIN
					SET @sql_set_p2 = @sqls_p1 + ''', validated_on = ''' + cast(@date as varchar(30))
					SET @sql_set_p2 = @sql_set_p2 + ''', created_by = ''' + @statusby
					SET @sql_set_p2 = @sql_set_p2 + ''', created_reference_id = ''' + @referenceid +''	
					END
					ELSE
					BEGIN
					SET @sql_set_p2 = @sqls_p1 + ''', created_by = ''' + @statusby
					SET @sql_set_p2 = @sql_set_p2 + ''', created_reference_id = ''' + @referenceid +''	
					END	
		--	insert into error_log (errorMessage) values (@sql_set_p2)
				END

				ELSE IF  @ToStatus='Validated'
				begin        
			       SET @sql_set_p2 = @sqls_p1
				end
			
				ELSE IF  @ToStatus='Consolidated'
				begin	
			        SET @sql_set_p2 = @sqls_p1
				end
				ELSE IF  @ToStatus='Active'
				begin	
			        SET @sql_set_p2 = @sqls_p1 
				end
				ELSE IF  @ToStatus='Rejected' 
				begin
					SET @sql_set_p2 = @sqls_p1 + ''', rejected_on = ''' + cast(@date as varchar(30))
					SET @sql_set_p2 = @sql_set_p2 + ''', rejected_by = ''' + @statusby
					SET @sql_set_p2 = @sql_set_p2 + ''', reject_reference_id = ''' + @referenceid +''		
				end

			--insert into error_log (errorMessage) values (@sql_set_p2)
			 IF  @ToStatus='Deleted'
				begin
					SET @sql_set_p2 = @sqls_p1 + ''', deleted_on = ''' + cast(@date as varchar(30))
					SET @sql_set_p2 = @sql_set_p2 + ''', deleted_by = ''' + @statusby
					SET @sql_set_p2 = @sql_set_p2 + ''', deleted_reference_id = ''' + @referenceid +''	
				end
			
			        SET @sql_set_p3 = @sql_set_p2 + ''', modified_on= ''' + cast(@date as varchar(30))
					SET @sql_set_p3 = @sql_set_p3 + ''', modified_by = ''' + @statusby
					SET @sql_set_p3 = @sql_set_p3 + ''', modified_reference_id = ''' + @referenceid +''''		
			    
			    SET @strQuery =  @sql_set_p3 + @sql_where 
				--print(@strQuery)
				
				--END
				--insert into error_log (errorMessage) values (@strQuery)

				EXEC  (@strQuery)
				
				SELECT @NumRowsChanged = @@ROWCOUNT, @ErrorCode = @@ERROR

				IF (@ErrorCode = 0)
					BEGIN
					IF(@NumRowsChanged = 0)
						BEGIN
						SET @outputVal = (
										SELECT sequence from app_config_param
										where category = 'status_update' and sub_category = 'No_records'
										)
						END
					ELSE
						BEGIN
						SET @outputVal = (
										SELECT sequence from app_config_param
										where value = 'Status Updated Successfully'
										)
						END
					END
					
				ELSE
					BEGIN
						RAISERROR (50005,16,1)		
				END
				--SELECT @outputVal
	 END
