SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC [usp_indent_calculation] '2018-11-05',NULL,NULL,NULL,NULL,NULL,NULL,'Malay','abc223'


--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: Calculation of Indent for the indentdate @dateT
 --- =========================================================================

ALTER PROCEDURE [dbo].[usp_indent_calculation]
 (		
        @dateT DATETIME,
		@is_Indent INT, 
		@is_Revision INT,
		@Active_Indent_Code INT,
		@project_Id_for_Rev  VARCHAR(50),
		@Rev_bankcode VARCHAR(50),
		@Feederbranch VARCHAR(50),
		@cur_user nvarchar(50),
		@created_reference_id nvarchar(50)
 )

AS 
BEGIN
	
BEGIN TRY
	declare @timestamp_date datetime =  DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50) 
	DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
	declare @Execution_log_str nvarchar(max)
	DECLARE @outputVal varchar(100)
	set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@dateT as nvarchar(50)) 
--BEGIN TRANSACTION
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Execution of [dbo].[usp_indent_calculation_SIT] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		--DECLARE @dateT DATETIME
		--SET     @dateT ='2018-11-05'

		DECLARE @MaxDestinationDate DATE
		DECLARE @dispenseformula NVARCHAR(50)
		DECLARE @confidence_factor NVARCHAR(50)
		DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		DECLARE @denomination_wise_round_off_500 int
		DECLARE @denomination_wise_round_off_2000 int
		DECLARE @denomination_wise_round_off_100 INT
		declare @default_avg_dispense int
		Select	@confidence_factor = confidence_factor
		, @dispenseformula = dispenseformula
		, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		,@default_avg_dispense = default_average_dispense
		 from system_settings where record_status = 'Active'

   /*******************************************Get Vaulting Percentage*************************************************/

 
 --IF @is_Indent=1
 --BEGIN
 --declare @dateT date
 --SET @dateT ='2018-11-22'
 --Set @MaxDestinationDate=(Select ISNULL(max(for_date),'1900-01-01') from Applied_Vaulting)
  
	/*********************************Calculate Applied vaulting******************************************************/														 
 

		drop table if exists #Applied_Vaulting
		
		Select * 
		into #Applied_Vaulting 
	    from(  
			SELECT 
			 @dateT as indent_date,
			am.project_id,am.bank_code,am.atm_id,
			case when cra.vaulting ='Yes'
				 then 
					case 
						when fvpc.is_vaulting_allowed = 'Yes' 
						then fvpc.vaulting_percentage 
				   else (
						case 
							when DATEPART(dw, @dateT)  NOT IN (1,7)   
							then  50
						    when  DATEPART(dw, @dateT) IN (1,7)   
							then   60
					        else     70 
						end
						)
				end 
			 end applied_vaulting_percentage
		    from atm_master am
				 left join cra_feasibility cra
				 on cra.site_code=am.site_code
					and cra.project_id=am.project_id
					and cra.bank_code=am.bank_code
					AND cra.record_status = 'Active'
		
				 left join  Feeder_Vaulting_pre_config fvpc
				 on cra.feeder_branch_code=fvpc.feeder_branch_code
					and cra.project_id=fvpc.project_id
					and cra.bank_code=fvpc.bank_code
				 and fvpc.record_status='Active'
				 
				 )d where d.applied_vaulting_percentage is not null
  
	
  /******************************************Calculate Loading amount************************************************************************************/
        INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
         values ('Insert into #pre_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

     DROP TABLE IF exists #pre_loading_amount 	
	
		SELECT 
			atm_id,
			site_code,
			sol_id,
			bank_code,
			project_id,
			indentdate,
			bank_cash_limit,
			insurance_limit	,
			feeder_branch_code,
			CASE 
				WHEN  bank_cash_limit< insurance_limit 
					THEN bank_cash_limit
				ELSE insurance_limit 
			END	AS limit_amount,
			max_of_bankcash_insurancelimit
			base_limit,
			dispenseformula,
			decidelimitdays,
			loadinglimitdays,
			avgdispense,
			cassette_50_count,
			cassette_100_count,
			cassette_200_count,
			cassette_500_count,
			cassette_2000_count,
			capacity_50,
			capacity_100,
			capacity_200,
			capacity_500,
			capacity_2000,
			total_capacity_amount_50,
			total_capacity_amount_100,
			total_capacity_amount_200,
			total_capacity_amount_500,
			total_capacity_amount_2000,
			total_cassette_capacity,
			avgdecidelimit,
			DecideLimit,
			threshold_limit,			
			loadinglimit, 
			applied_vaulting_percentage,
			ignore_code,
			ignore_description, 
			dist_Purpose,
			defaultamt,	
			default_flag,		
			remaining_balance_50,
			remaining_balance_100,
			remaining_balance_200,
			remaining_balance_500,
			remaining_balance_2000,
			Morning_balance_T_minus_1,
			curbal_div_avgDisp,
			loadingGap_cur_bal_avg_disp,
			CashOut,		               
             loadingamount    
		 
		 --drop table if exists #pre_loading_amount 
			INTO #pre_loading_amount
			FROM
			(
			Select 
			atm_id
			,site_code
			,sol_id
			,bank_code
			,project_id
			,indentdate
			,bank_cash_limit
			,insurance_limit
			,feeder_branch_code
			,max_of_bankcash_insurancelimit
			,base_limit
			,dispenseformula
			,decidelimitdays
			,loadinglimitdays
			,avgdispense
			,cassette_50_count
			,cassette_100_count
			,cassette_200_count
			,cassette_500_count
			,cassette_2000_count
			,capacity_50
			,capacity_100
			,capacity_200
			,capacity_500
			,capacity_2000
			,total_capacity_amount_50
			,total_capacity_amount_100
			,total_capacity_amount_200
			,total_capacity_amount_500
			,total_capacity_amount_2000
			,total_cassette_capacity
			,avgdecidelimit
			,DecideLimit
			,threshold_limit
			,loadinglimit
			,applied_vaulting_percentage
			,ignore_code
			,ignore_description
			,dist_Purpose
			,defaultamt
			,case when Morning_balance_T_minus_1 is NULL then 1 
										else 0 end 
										as default_flag  -- Flag for default loading
			,remaining_balance_50
			,remaining_balance_100
			,remaining_balance_200
			,remaining_balance_500
			,remaining_balance_2000
			,Morning_balance_T_minus_1
			,curbal_div_avgDisp
			,loadingGap_cur_bal_avg_disp
			,CashOut			
            ,CASE when ignore_code  IS NOT NULL THEN 0   -- calculating loading amount on ATM level 
               ELSE		     
			      CASE WHEN Morning_balance_T_minus_1 is NULL then defaultamt
						  ELSE
							(CASE 
								WHEN insurance_limit  < 
										CASE 
											WHEN  Morning_balance_T_minus_1 < DecideLimit
												THEN (loadinglimit-Morning_balance_T_minus_1) + 	
												 (  cast(@confidence_factor as float)/cast(100 as float))
																					 * avgdispense 	
												ELSE  0 --DecideLimit
								 END 
								THEN insurance_limit 
							ELSE 
								CASE 
									WHEN  Morning_balance_T_minus_1 < DecideLimit
										THEN (loadinglimit- Morning_balance_T_minus_1)  + 
											 (  cast(@confidence_factor as float)/cast(100 as float))* avgdispense 													
						                ELSE  0 --DecideLimit 
										END 
						END ) END END  loadingamount    
	 
            FROM 
              (
					SELECT b.*,
					AV.applied_vaulting_percentage,
				case    when qa.is_qualified = 'Disqualified'  then 20001  
						when fault_description is NOT NULL then 20003
						when OPS.feeder_branch_code is NOT NULL then 20002 						 
						else NULL
						end ignore_code,  -- putting code for disqualified, faulty,start/stop  cases

				case when qa.is_qualified = 'Disqualified'  then 'Disqualified'  
				when fault_description is not NULL then  fault_description 
				 else NULL
				end ignore_description,   

				case when eod.atm_id is not null then  'EOD' 
				      else NULL
					  end dist_Purpose,     

					  --Case when mb.Morning_balance_T_minus_1 is NULL then DLL.amount 														
					  --else 0 end  defaultamt,
					DLL.amount  as defaultamt,				
					mb.remaining_balance_50,
					mb.remaining_balance_100,
					mb.remaining_balance_200,
					mb.remaining_balance_500,
					mb.remaining_balance_2000,
					mb.Morning_balance_T_minus_1,
					ROUND((CAST(Morning_balance_T_minus_1 AS FLOAT)/ CAST(avgdispense  AS FLOAT) ),1) AS curbal_div_avgDisp,
					(decidelimitdays- (Morning_balance_T_minus_1/avgdispense)) * avgdispense AS loadingGap_cur_bal_avg_disp,
					CASE 
						WHEN Morning_balance_T_minus_1 <10000 
						THEN 1
						ELSE 0 
					END CashOut

					FROM 
					(
						SELECT a.*,avgdispense*decidelimitdays AS avgdecidelimit, 
						CASE 
							WHEN (
								CASE 
									WHEN base_limit > (avgdispense*decidelimitdays) 
										THEN base_limit
										ELSE  avgdispense*decidelimitdays END) > total_cassette_capacity 
								THEN  total_cassette_capacity
								ELSE 
								( CASE 
									WHEN base_limit > (avgdispense*decidelimitdays) 
									THEN base_limit
									ELSE  avgdispense*decidelimitdays END)
							END DecideLimit, 
					
					 avgdispense*loadinglimitdays  AS threshold_limit ,
					 CASE 
						WHEN (
							 CASE 
								WHEN  base_limit >  (avgdispense*loadinglimitdays)    
									THEN  base_limit 
									ELSE  avgdispense*loadinglimitdays   
								END ) > total_cassette_capacity 
							THEN total_cassette_capacity
								ELSE  
									( CASE 
										WHEN  base_limit >  (avgdispense*loadinglimitdays)    
											THEN  base_limit 
											ELSE  avgdispense*loadinglimitdays   
										END )
					 	  END loadinglimit						 
 					  FROM  
					  (    
					  SELECT mast.atm_id,mast.site_code,
							 sol_id,
							 mast.bank_code,
							 mast.project_id,
							 @dateT AS indentdate,
							 clm.bank_cash_limit,
							 clm.insurance_limit,
							 cra.feeder_branch_code as feeder_branch_code,
							 CASE 
								WHEN clm.bank_cash_limit <  clm.insurance_limit 
									THEN clm.bank_cash_limit
									ELSE clm.insurance_limit 
								END AS  max_of_bankcash_insurancelimit,
							 clm.base_limit,
							 @dispenseformula AS dispenseformula ,
							 dl.[decidelimitdays],
							 dl.loadinglimitdays,
							COALESCE( CASE 
								WHEN @dispenseformula='max_of_max'    
									THEN cd.max_of_max		
								WHEN @dispenseformula='avg_of_max'    
									THEN cd.avg_of_max  
								WHEN @dispenseformula='max_of_avg'    
									THEN cd.max_of_avg  
								WHEN @dispenseformula='avg_of_avg'    
									THEN cd.avg_of_avg  
								WHEN @dispenseformula='max_of_secondmax'    
									THEN cd.max_of_secondmax
								WHEN @dispenseformula='avg_of_secondmax'    
									THEN cd.avg_of_secondmax
								WHEN @dispenseformula='max_of_thirdmax'    
									THEN cd.max_of_thirdmax
								WHEN @dispenseformula='avg_of_thirdmax'    
									THEN cd.avg_of_thirdmax
								WHEN @dispenseformula='_28days_or_T2dispense'    
									THEN  cd._28days_or_T2dispense
							END,  @default_avg_dispense) avgdispense
					          , COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) AS cassette_50_count
					         , COALESCE(cc.cassette_100_count, clm.cassette_100_count,0) AS cassette_100_count
					         , COALESCE(cc.cassette_200_count, clm.cassette_200_count,0) AS cassette_200_count
					         , COALESCE(cc.cassette_500_count, clm.cassette_500_count,0) AS cassette_500_count
					         , COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0) AS cassette_2000_count  
						     ,COALESCE(bc.capacity_50,2000) as capacity_50
						     ,COALESCE(bc.capacity_100,2000) as capacity_100
						     ,COALESCE(bc.capacity_200,2000) as capacity_200
						     ,COALESCE(bc.capacity_500,2000) as capacity_500
						     ,COALESCE(bc.capacity_2000,2000) as capacity_2000
						     ,COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) * COALESCE(bc.capacity_50,2000) *50 AS total_capacity_amount_50
						     ,COALESCE(cc.cassette_50_count, clm.cassette_100_count) * COALESCE(bc.capacity_100,2000) *100 AS total_capacity_amount_100
						     ,COALESCE(cc.cassette_50_count, clm.cassette_200_count) * COALESCE(bc.capacity_200,2000) *200 AS total_capacity_amount_200
						     ,COALESCE(cc.cassette_50_count, clm.cassette_500_count) *COALESCE(bc.capacity_500,2000)*500 AS total_capacity_amount_500
						     ,COALESCE(cc.cassette_50_count, clm.cassette_2000_count) * COALESCE(bc.capacity_2000,2000) *2000 AS total_capacity_amount_2000
						     
						     ,(COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) * COALESCE(bc.capacity_50,2000) *50) +
					          (COALESCE(cc.cassette_50_count, clm.cassette_100_count,0) *COALESCE(bc.capacity_100,2000)  *100) +
						      (COALESCE(cc.cassette_50_count, clm.cassette_200_count,0) *COALESCE(bc.capacity_200,2000) *200)+	   
						      (COALESCE(cc.cassette_50_count, clm.cassette_500_count,0) * COALESCE(bc.capacity_500,2000) *500) +
						      (COALESCE(cc.cassette_50_count, clm.cassette_2000_count,0) * COALESCE(bc.capacity_2000,2000) *2000)
							  AS total_cassette_capacity
							  --DECLARE @dateT DATETIME
							  --SET @dateT ='2018-11-11'
							  
							  --SELECT mast.* 
							  FROM  atm_master AS  mast
							  inner JOIN		ATM_Config_limits clm
							  ON			mast.atm_id = clm.atm_id  AND
											mast.site_code = clm.site_code AND
											clm.record_status = 'Active'
							  
							  inner JOIN cra_feasibility cra on
								mast.site_code = cra.site_code
								AND mast.atm_id = cra.atm_id AND
								cra.record_status = 'Active'

							  LEFT JOIN 	calculated_dispense cd
								ON mast.atm_id=cd.atm_id AND 
								mast.project_id = cd.project_id AND
								mast.bank_code = cd.bank AND
								CAST(cd.datafor_date_time as date) = cast(@dateT as date)
								and cd.record_status  = 'Active'
							  
							  LEFT JOIN		limitdays dl
									  ON mast.atm_id=dl.atm_id AND 
									mast.bank_code=dl.bank_code AND 
									mast.project_id=dl.project_id AND
									CAST(dl.indent_date as date) = cast(@dateT as date) AND
									dl.record_status = 'Active'	
							  
							  LEFT JOIN   feeder_branch_master fb on
											cra.bank_code = fb.bank_code
											AND cra.feeder_branch_code = fb.feeder_branch 
											AND fb.project_id = cra.project_id 
											AND fb.record_status = 'Active'
							  LEFT JOIN      
							       (
							       SELECT 
										project_id,
										bank_code, 
										site_code,
										atm_id, 
										cassette_50_count, 
										cassette_100_count, 
										cassette_200_count,
										cassette_500_count,
										cassette_2000_count 
									FROM Modify_cassette_pre_config 
									WHERE @dateT between from_date AND to_date
							        AND record_status = 'Active'
							       )cc
							  ON clm.site_code = cc.site_code AND 
								 clm.atm_id = cc.atm_id
								 LEFT JOIN Brand_Bill_Capacity bc ON 
										   bc.brand_code = mast.brand 
								           AND bc.record_status = 'Active'
										WHERE mast.record_status = 'Active'
							       )a
					 )b 

					 	LEFT JOIN 
					morningbalance mb
					ON b.atm_id=mb.atm_id
					and b.bank_code = mb.bank_name
					and mb.record_status = 'Active'
					and mb.datafor_date_time = @dateT

					LEFT Join #Applied_Vaulting AV
					on  b.atm_id=AV.atm_id
					and b.project_id=AV.project_id
					and b.bank_code= AV.bank_code

					LEFT join Indent_Pre_Qualify_ATM  QA
					 on   b.atm_id=QA.atm_id
					 and b.site_code = QA.site_code					 
					 and   QA.record_status='Active'					 
					 and @dateT between QA.from_date and QA.to_date

					LEFT join	(
								SELECT atmid,site_code,
										STUFF((
											  SELECT ',' + T.fault_description
											  FROM dbo.ims_master T
											  WHERE A.atmid = T.atmid
											  FOR XML PATH('')), 1, 1, '') as fault_description 
										 from ims_master A
								where fault_description not in ('Any Cassette Faulty')
								and record_status='Active'
								group by atmid,site_code
								
								) im
					on b.atm_id=im.atmid  and b.site_code = im.atm_id					

				    LEFT join [indent_eod_pre_activity] EOD
					on b.atm_id=EOD.atm_id
					and b.bank_code=EOD.bank_code
					and b.project_id=EOD.project_id
					and EOD.record_status='Active'

					left join ops_stop_batch OPS
					on b.feeder_branch_code=OPS.feeder_branch_code
					and  b.project_id=OPS.project_id
					and b.bank_code=OPS.bank_code
					and OPS.record_status='Active'
					and @dateT between cast(OPS.from_date as date) and cast(OPS.to_date as date)

					left join Default_loading DLL
					on b.project_id=DLL.project_id
					and b.bank_code=DLL.bank_code
					and DLL.record_status='Active'
			
				) c
								 
		            ) d


      INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Fetch data from tables and insert into #pre_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

 /****************************  Rounding of loading amount /negativebalance  ************************************/



	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Start prepare dataset  to handle negative amount, Insert into #loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	DROP TABLE IF exists #loading_amount   
	SELECT [atm_id],
			site_code,
			sol_id,
			bank_code,
			project_id,
			indentdate,
			bank_cash_limit,
			insurance_limit	,
			feeder_branch_code, 
			base_limit,
			dispenseformula,
			 decidelimitdays,
			 loadinglimitdays,
			 avgdispense
			,COALESCE([loadingamount],0)				AS loading_amount
			,COALESCE(remaining_balance_50,0)			AS [morning_balance_50]
			,COALESCE(remaining_balance_100,0)			AS morning_balance_100
			,COALESCE(remaining_balance_200,0)			AS morning_balance_200
			,COALESCE(remaining_balance_500,0)			AS morning_balance_500
			,COALESCE(remaining_balance_2000,0)			AS morning_balance_2000
			,COALESCE(morning_balance_T_minus_1,0)		AS total_morning_balance
			,COALESCE(capacity_50,0)					AS cassette_50_brand_capacity
			,COALESCE(capacity_100,0)					AS cassette_100_brand_capacity
			,COALESCE(capacity_200,0)					AS cassette_200_brand_capacity
			,COALESCE(capacity_500,0)					AS cassette_500_brand_capacity
			,COALESCE(capacity_2000,0)					AS cassette_2000_brand_capacity
			,COALESCE(cassette_50_count ,0)				AS cassette_50_count
			,COALESCE(cassette_100_count,0)				AS cassette_100_count
			,COALESCE(cassette_200_count,0)				AS cassette_200_count
			,COALESCE(cassette_500_count,0)				AS cassette_500_count
			,COALESCE(cassette_2000_count,0)			AS cassette_2000_count 
			,COALESCE(limit_amount,0)					AS limit_amount 
			,total_capacity_amount_50
			,total_capacity_amount_100
			,total_capacity_amount_200
			,total_capacity_amount_500
			,total_capacity_amount_2000
			,total_cassette_capacity
			,avgdecidelimit
			,DecideLimit
			,threshold_limit
			,loadinglimit
			,applied_vaulting_percentage
			,ignore_code
			,ignore_description 
			,dist_Purpose
			,defaultamt
			,default_flag
			,curbal_div_avgDisp
			,loadingGap_cur_bal_avg_disp
			,CashOut
			,remaining_balance_50
			,remaining_balance_100
			,remaining_balance_200
			,remaining_balance_500
			,remaining_balance_2000

			INTO #loading_amount
			FROM #pre_loading_amount

      INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Insert into #loading_amount completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		
		DECLARE @ITERATION AS INT
		SET @ITERATION = 0
		WHILE 1=1
			BEGIN
				
		
				SET @ITERATION = @ITERATION + 1
				-- Calculate Cassette capacity amount
	
				DROP TABLE IF exists #cassette_capacity_amount

				
      INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
      values ('Insert into #cassette_capacity_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

				SELECT * 
					,[cassette_50_brand_capacity]*[cassette_50_count]*50       AS cassette_50_capacity_amount
					,[cassette_100_brand_capacity]*[cassette_100_count]*100    AS cassette_100_capacity_amount
					,[cassette_200_brand_capacity]*[cassette_200_count]*200    AS cassette_200_capacity_amount
					,[cassette_500_brand_capacity]*[cassette_500_count]*500    AS cassette_500_capacity_amount
					,[cassette_2000_brand_capacity]*[cassette_2000_count]*2000 AS cassette_2000_capacity_amount
					,([cassette_50_brand_capacity]*[cassette_50_count]*50) + 
					 ([cassette_100_brand_capacity]*[cassette_100_count]*100) +
					 ([cassette_200_brand_capacity]*[cassette_200_count]*200) +
					 ([cassette_500_brand_capacity]*[cassette_500_count]*500) +
					 ([cassette_2000_brand_capacity]*[cassette_2000_count]*2000)AS total_cassette_capacity_amount
				 INTO #cassette_capacity_amount	  
				 FROM #loading_amount
	
	--SELECT * FROM #cassette_capacity_amount


			   INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #cassette_capacity_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
			DROP TABLE IF EXISTS #cassette_capacity_percentage
	
			-- Calculate Cassette capacity percentage
			SELECT *,
				COALESCE(cassette_50_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)     AS cassette_capacity_percentage_50
				,COALESCE(cassette_100_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)   AS cassette_capacity_percentage_100
				,COALESCE(cassette_200_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)   AS cassette_capacity_percentage_200
				,COALESCE(cassette_500_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)   AS cassette_capacity_percentage_500
				,COALESCE(cassette_2000_capacity_amount * 100.0 / NULLIF(total_cassette_capacity_amount,0),0)  AS cassette_capacity_percentage_2000
			INTO #cassette_capacity_percentage
			FROM #cassette_capacity_amount
			
			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #cassette_capacity_percentage Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
			--SELECT * FROM #cassette_capacity_percentage

			DROP TABLE IF exists #cassette_capacity_amount
			DROP TABLE IF exists #tentative_loading_amount			


			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #tentative_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
			SELECT *
				,CASE WHEN cassette_capacity_percentage_50 > 0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_50/100 ) -morning_balance_50 ELSE 0 END AS tentative_loading_amount_50
				,CASE WHEN cassette_capacity_percentage_100 > 0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_100/100 ) -morning_balance_100 ELSE 0 END AS tentative_loading_amount_100
				,CASE WHEN cassette_capacity_percentage_200 > 0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_200/100 ) -morning_balance_200 ELSE 0 END AS tentative_loading_amount_200
				,CASE WHEN cassette_capacity_percentage_500 > 0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_500/100 ) -morning_balance_500 ELSE 0 END AS tentative_loading_amount_500
				,CASE WHEN cassette_capacity_percentage_2000 > 0 THEN ( (total_morning_balance + loading_amount) * cassette_capacity_percentage_2000/100 ) -morning_balance_2000 ELSE 0 END AS tentative_loading_amount_2000
			--,@ITERATION AS iterate_counter
			INTO #tentative_loading_amount
			FROM #cassette_capacity_percentage


			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #tentative_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

			DROP TABLE IF EXISTS #cassette_capacity_percentage			

			IF exists(
				SELECT 1 FROM #tentative_loading_amount 
				WHERE tentative_loading_amount_50 < 0 OR
				 tentative_loading_amount_100 <0 OR 
				 tentative_loading_amount_200 <0 OR 
				 tentative_loading_amount_500 <0 OR 
				 tentative_loading_amount_2000 <0
				)
			BEGIN

				TRUNCATE TABLE #loading_amount				
				
		   INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Insert into #loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

				INSERT INTO #loading_amount
				SELECT
					[atm_id],
					site_code,					
					sol_id,
					bank_code,
					project_id,
					indentdate,
					bank_cash_limit,
					insurance_limit	,
					feeder_branch_code, 
					base_limit,
					dispenseformula,
					 decidelimitdays,
					 loadinglimitdays,
					 avgdispense
					,loading_amount
					,morning_balance_50
					,morning_balance_100
					,morning_balance_200
					,morning_balance_500	
					,morning_balance_2000
					,total_morning_balance
					,cassette_50_brand_capacity
					,cassette_100_brand_capacity
					,cassette_200_brand_capacity
					,cassette_500_brand_capacity
					,cassette_2000_brand_capacity
				  ,CASE 
					WHEN tentative_loading_amount_50 <0 
						THEN 0 
						ELSE  cassette_50_count 
					END AS cassette_50_count
				  ,CASE 
					WHEN tentative_loading_amount_100 <0 
						THEN 0 
						ELSE  cassette_100_count 
					END AS cassette_100_count
				  ,CASE 
					WHEN tentative_loading_amount_200 <0 
						THEN 0 
						ELSE  cassette_200_count 
					END AS cassette_200_count
				  ,CASE 
					WHEN tentative_loading_amount_500 <0 
						THEN 0 
						ELSE  cassette_500_count 
					END AS cassette_500_count
				  ,CASE 
					WHEN tentative_loading_amount_2000 <0 
						THEN 0 
						ELSE  cassette_2000_count 
					END AS cassette_2000_count    				
				  ,limit_amount
				  ,total_capacity_amount_50
				  ,total_capacity_amount_100
				  ,total_capacity_amount_200
				  ,total_capacity_amount_500
				  ,total_capacity_amount_2000
				  ,total_cassette_capacity
				  ,avgdecidelimit
				  ,DecideLimit
				  ,threshold_limit
				  ,loadinglimit
				  ,applied_vaulting_percentage
				  ,ignore_code
			      ,ignore_description 
				  ,dist_Purpose
				  ,defaultamt
				  ,default_flag
				  ,curbal_div_avgDisp
				  ,loadingGap_cur_bal_avg_disp
				  ,CashOut		
				  ,remaining_balance_50
				  ,remaining_balance_100
				  ,remaining_balance_200
				  ,remaining_balance_500
				  ,remaining_balance_2000			
				 FROM #tentative_loading_amount
						
				-- Continue until negative amount
				 --SELECT 'Iteration' , @ITERATION
				 --BREAK
			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Insert into #loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

			-- Logic goes here: The loop can be broken with the BREAK command.
			END -- END 
			ELSE
			BEGIN -- BEGIN of ELSE

			
			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Update into #loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
							

	

				-- PENDING: Add 20% more cash for 100 AND 200 denomination IF capacity is allowed

			update #tentative_loading_amount
			Set tentative_loading_amount_100= case when (morning_balance_100+tentative_loading_amount_100
																						+((cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_100)) < cassette_100_capacity_amount
																						    then  ( tentative_loading_amount_100 + (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_100)
																							else (cassette_100_capacity_amount-morning_balance_100)
																							end
			, tentative_loading_amount_200= case when (morning_balance_200+tentative_loading_amount_200
																						+( (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_200)) < cassette_200_capacity_amount
																						    then  ( tentative_loading_amount_200 +  (cast(@confidence_factor as float)/cast(100 as float))*tentative_loading_amount_200)
																							else (cassette_200_capacity_amount-morning_balance_200)
																							end
			

				break

			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
              values ('Update into #loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				
			
			END -- END of ELSE
		
		END -- END of WHILE loop


		--Round amount		 
		 
		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
          values ('Insert into #deno_rounded_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


									
		  drop table if exists #deno_rounded_loading_amount	
		


			select	*
					,tentative_loading_amount_50 as rounded_amount_50,
			
					case when (cast(tentative_loading_amount_100 as bigint)  % @denomination_wise_round_off_100 ) > 0  
						then  
						case when (cast( (tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) +1) * @denomination_wise_round_off_100 > cassette_100_capacity_amount
						then  (cast(( tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) -1) * @denomination_wise_round_off_100
							else (cast( (tentative_loading_amount_100 /@denomination_wise_round_off_100) as int) +1) * @denomination_wise_round_off_100
						end
					 else    0 end  rounded_amount_100,

					 

				 case when (cast( tentative_loading_amount_200 as bigint)  % @denomination_wise_round_off_200 ) > 0  then 
						case when (cast( (tentative_loading_amount_100 /@denomination_wise_round_off_200) as int) +1) * @denomination_wise_round_off_200 > cassette_200_capacity_amount then
							(cast((tentative_loading_amount_100 /@denomination_wise_round_off_200)  as int) -1) * @denomination_wise_round_off_200
						else
							(cast( (tentative_loading_amount_100 /@denomination_wise_round_off_200)  as int) +1) * @denomination_wise_round_off_200
						end
					else  
						0 
					end  rounded_amount_200,
					

				 case when (cast( (tentative_loading_amount_500 ) as bigint)  % @denomination_wise_round_off_500 ) > 0  then 
						case when (cast(  (tentative_loading_amount_500 /@denomination_wise_round_off_500)  as int) +1) * @denomination_wise_round_off_500 > cassette_500_capacity_amount then 
							(cast(  (tentative_loading_amount_500 /@denomination_wise_round_off_500) as int) -1) * @denomination_wise_round_off_500
						else
							(cast(  (tentative_loading_amount_500 /@denomination_wise_round_off_500) as int) +1) * @denomination_wise_round_off_500
						end
					else    
						0 
					end  rounded_amount_500,

				 case when (cast((tentative_loading_amount_2000 ) as bigint)  % @denomination_wise_round_off_2000 ) > 0  	then 
						case when (cast( (tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) +1) * @denomination_wise_round_off_2000 > cassette_2000_capacity_amount then
							(cast((tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) -1) * @denomination_wise_round_off_2000
						else
							(cast((tentative_loading_amount_2000 /@denomination_wise_round_off_2000) as int) +1) * @denomination_wise_round_off_2000
						end 
					else    
						0 
					end  rounded_amount_2000
		into #deno_rounded_loading_amount
		from #tentative_loading_amount
		
		-- select * from #deno_rounded_loading_amount
		-- Select * from  #tentative_loading_amount
		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
          values ('Insert into #deno_rounded_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
					
		---  drop table  #deno_rounded_loading_amount
		WHILE 1=1
		BEGIN	
		DROP TABLE IF exists #gap_amount

		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
          values ('Insert into #gap_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		

		
		SELECT *
		, rounded_amount_100 - tentative_loading_amount_100 AS rounding_gap_100
		, rounded_amount_200 - tentative_loading_amount_200 AS rounding_gap_200
		, rounded_amount_500 - tentative_loading_amount_500 AS rounding_gap_500
		, rounded_amount_2000 - tentative_loading_amount_2000 AS rounding_gap_2000
		, (rounded_amount_100 + rounded_amount_200 + rounded_amount_500 + rounded_amount_2000) AS total_rounded_loading_amount
		, (rounded_amount_100 + rounded_amount_200 + rounded_amount_500 + rounded_amount_2000) - loading_amount  AS total_loading_amount_rounding_gap
		 INTO #gap_amount
		 FROM #deno_rounded_loading_amount
		
		  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
          values ('Insert into #gap_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		

		--SELECT * FROM execution_log order by id desc	
		
		IF exists ( SELECT 1 FROM #gap_amount WHERE (total_rounded_loading_amount + total_morning_balance) > limit_amount ) 
		BEGIN
			-- find max gap
			
			DROP TABLE IF exists #max_gap 

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			values ('Insert into #max_gap Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		

			SELECT *, 
				(
					SELECT MAX(v) FROM (VALUES (rounding_gap_100),
					 (rounding_gap_200), 
					 (rounding_gap_500),
					 (rounding_gap_2000)
				 )
					  AS value(v)) AS max_gap
					INTO #max_gap
				    FROM #gap_amount

			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
             values ('Insert into #max_gap Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		

			-- Re-distribute max gap amount to lower

			TRUNCATE TABLE #deno_rounded_loading_amount			

			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #deno_rounded_loading_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		

			INSERT INTO #deno_rounded_loading_amount
			SELECT  atm_id
					, site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50
					, CASE -- total rounded vaulting amnt> original vaulting amt
						WHEN (total_rounded_loading_amount + total_morning_balance) > limit_amount AND rounding_gap_100 = max_gap 
							THEN   rounded_amount_100 - @denomination_wise_round_off_100 
							ELSE rounded_amount_100 
						END AS rounded_amount_100_updated
					, CASE 
						WHEN (total_rounded_loading_amount + total_morning_balance) > limit_amount AND rounding_gap_200 = max_gap 
							THEN   rounded_amount_200 - @denomination_wise_round_off_200 
							ELSE rounded_amount_200 
						END AS rounded_amount_200_updated
					, CASE 
						WHEN (total_rounded_loading_amount + total_morning_balance) > limit_amount AND rounding_gap_500 = max_gap 
							THEN   rounded_amount_500 - @denomination_wise_round_off_500 
							ELSE rounded_amount_500 
						END AS rounded_amount_500_updated
					, CASE 
						WHEN (total_rounded_loading_amount + total_morning_balance) > limit_amount AND rounding_gap_2000 = max_gap 
							THEN   rounded_amount_2000 - @denomination_wise_round_off_2000 
							ELSE rounded_amount_2000 
						END AS rounded_amount_2000_updated
				FROM #max_gap
			
			  INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #deno_rounded_loading_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
		END
		ELSE
		BEGIN
					BREAK
		END
	END -- END of WHILE loop for rounding
--END--END of procedure
	
		DROP TABLE IF exists  #pre_vault_amounting


		Select * into #pre_vault_amounting
		from
		(
			SELECT      atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
				    ,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   case when  (rounded_amount_50 +
									rounded_amount_100+
									rounded_amount_200+
									rounded_amount_500+
									rounded_amount_2000) = 0
							 and dist_Purpose='EOD'
							 then 'EOD' 
							 else 'Add Cash' end as dist_Purpose
					,   defaultamt
					,  default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50
					,   rounded_amount_100
					,   rounded_amount_200
					,   rounded_amount_500
					,   rounded_amount_2000
					,   case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
						( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) 
											  end
											  as total_expected_balanceT1
					, case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
					( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_50,2)) as float)/100)
											  end 
											  as expected_balanceT1_50
                ,case when  ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then  
				( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_100,2)) as float)/100)
											  end 
											  as expected_balanceT1_100
                ,case when  ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then  
				( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_200,2)) as float)/100)
											end 
											  as expected_balanceT1_200
                 ,case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
					 ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) *( cast((Round(cassette_capacity_percentage_500,2)) as float)/100)
											end
											  as expected_balanceT1_500
                 , case when  ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
							( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(cassette_capacity_percentage_2000,2)) as float)/100)
											  end 
											  as expected_balanceT1_2000
				, case when  ( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 				
				(( total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense ) * (cast((Round(applied_vaulting_percentage,2)) as float)/100)) 
											  end 
									as total_vault_amount
				, case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
				(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_50,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_50		
											   
				, case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
				(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											   ((Round(cassette_capacity_percentage_100,2)) /100)
											   *(cast(applied_vaulting_percentage as float)/100))	
											   end 
											   as vaultingamount_100
 
				, case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
				(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											   ((Round(cassette_capacity_percentage_200,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_200	

			    , case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
					(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_500,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))
											   end 
											   as vaultingamount_500	

             , case when  (total_morning_balance + rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000  - avgdispense > 0) then 
					(( total_morning_balance +( rounded_amount_50 
										      + rounded_amount_100
											  + rounded_amount_200
											  + rounded_amount_500
											  + rounded_amount_2000 ) - avgdispense ) *
											  ( (Round(cassette_capacity_percentage_2000,2)) /100)
											   *(cast((Round(applied_vaulting_percentage,2)) as float)/100))	
											   end 
											   as vaultingamount_2000	
					FROM 
               #deno_rounded_loading_amount 
			   
			   )a

	--PENDING 23 Dec 2018: Amount for total_expected_balanceT1 is getting in negative. Ned to check.

	--****************************************Rounding of Vaulting amount*************************************************************/

	 drop table if exists #vault_amounting
		
		/* For testing dont remove below commented code */
		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT

		-- Select	
		--  @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		-- from system_settings where record_status = 'Active'	
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Insert into vault_amounting Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


					select	*
					,vaultingamount_50 as rounded_vaultingamount_50
					,
						case when (cast(vaultingamount_100 as bigint)  % @denomination_wise_round_off_100 ) > 0  
						   then  
						  (cast( (vaultingamount_100 /@denomination_wise_round_off_100) as int) +1) * @denomination_wise_round_off_100  
							else    0 
							end  rounded_vaultingamount_100
			
					,
						case when (cast(vaultingamount_200 as bigint)  % @denomination_wise_round_off_200 ) > 0  
						   then  
						  (cast( (vaultingamount_200 /@denomination_wise_round_off_200) as int) +1) * @denomination_wise_round_off_200  
							else    0 
							end  rounded_vaultingamount_200
				 
					,
						case when (cast(vaultingamount_500 as bigint)  % @denomination_wise_round_off_500 ) > 0  
						   then  
						  (cast( (vaultingamount_500 /@denomination_wise_round_off_500) as int) +1) * @denomination_wise_round_off_500  
							else    0 
							end  rounded_vaultingamount_500, 

					
						case when (cast(vaultingamount_2000 as bigint)  % @denomination_wise_round_off_2000 ) > 0  
						   then  
						  (cast( (vaultingamount_2000 /@denomination_wise_round_off_2000) as int) +1) * @denomination_wise_round_off_2000  
							else    0 
							end  rounded_vaultingamount_2000


		into #vault_amounting
		from #pre_vault_amounting


		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	    values ('Insert into vault_amounting Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		--   Select * from #vault_amounting		
		--   select * from #pre_vault_amounting
		---  drop table  #deno_rounded_loading_amount
		

		--DECLARE @dateT DATETIME
		--SET @dateT ='2018-11-05'

		--DECLARE @MaxDestinationDate DATE
		--DECLARE @dispenseformula NVARCHAR(50)
		--DECLARE @confidence_factor NVARCHAR(50)
		--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT
		--Select	@confidence_factor = confidence_factor
		--, @dispenseformula = dispenseformula
		--, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		-- from system_settings where record_status = 'Active'			



		/***************************************Roundig of calcualted vault amount*********************************/
		WHILE 1=1
		BEGIN	

		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	values ('Insert into #vault_gap_amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

		DROP TABLE IF exists #vault_gap_amount		 
		
		SELECT *
		, ROUND((rounded_vaultingamount_100 - vaultingamount_100),2) AS rounding_gap_100
		, ROUND((rounded_vaultingamount_200 - vaultingamount_200),2) AS rounding_gap_200
		, ROUND((rounded_vaultingamount_500 - vaultingamount_500),2) AS rounding_gap_500
		, ROUND((rounded_vaultingamount_2000 - vaultingamount_2000),2) AS rounding_gap_2000
		, (rounded_vaultingamount_100 + rounded_vaultingamount_200 + rounded_vaultingamount_500 + rounded_vaultingamount_2000) AS total_rounded_vault_amount
		, (rounded_vaultingamount_100 + rounded_vaultingamount_200 + rounded_vaultingamount_500 + rounded_vaultingamount_2000) - total_vault_amount  AS total_rounded_vault_amount_gap
		 INTO   #vault_gap_amount
		 FROM   #vault_amounting
		
		  -- Select * from #vault_gap_amount
		 -- total rounded vaulting amnt> original vaulting amt

		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into #vault_gap_amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

		IF exists ( SELECT * FROM #vault_gap_amount
		 WHERE (total_rounded_vault_amount ) > limit_amount ) 
		
		BEGIN
			-- find max gap
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #max_vault_gap Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

			DROP TABLE IF exists #max_vault_gap 	
			SELECT *, 
				(
					SELECT MAX(v) FROM (VALUES (rounding_gap_100), 
					(rounding_gap_200), 
					(rounding_gap_500),
					(rounding_gap_2000)) AS value(v)
		    	) 
					AS max_gap
					INTO #max_vault_gap
				    FROM #vault_gap_amount

				
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #max_vault_gap Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	
			/* For testing dont remove below commented code */	

		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT

		--Select	
		-- @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		-- from system_settings where record_status = 'Active'	
		

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #vault_amounting Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			TRUNCATE TABLE #vault_amounting		

			INSERT INTO #vault_amounting
			SELECT  atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,    defaultamt
					,    default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50
					,   rounded_amount_100
					,   rounded_amount_200
					,   rounded_amount_500
					,   rounded_amount_2000
					, total_expected_balanceT1
					,expected_balanceT1_50
					,expected_balanceT1_100
					,expected_balanceT1_200
					,expected_balanceT1_500
					,expected_balanceT1_2000
					,total_vault_amount
					,vaultingamount_50
					,vaultingamount_100
					,vaultingamount_200
					,vaultingamount_500
					,vaultingamount_2000
					,rounded_vaultingamount_50
					, CASE  
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_100 = max_gap 
							THEN   rounded_vaultingamount_100 - @denomination_wise_round_off_100 
							ELSE rounded_vaultingamount_100 
						END AS rounded_Vault_amt_100_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_200 = max_gap 
							THEN   rounded_vaultingamount_200 - @denomination_wise_round_off_200 
							ELSE rounded_vaultingamount_200 
						END AS rounded_Vault_amt_200_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_500 = max_gap 
							THEN  ( rounded_vaultingamount_500 - @denomination_wise_round_off_500 )
							ELSE rounded_vaultingamount_500 
						END AS rounded_Vault_amt_500_updated
					, CASE 
						WHEN (total_rounded_vault_amount ) > total_vault_amount AND rounding_gap_2000 = max_gap 
							THEN   rounded_vaultingamount_2000 - @denomination_wise_round_off_2000 
							ELSE rounded_vaultingamount_2000 
						END AS rounded_Vault_amt_2000_updated
				FROM 
					 #max_vault_gap  

			   -- SELECT * FROM #vault_amounting
			   -- select * from  #max_vault_gap
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #vault_amounting Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
						
		END
	ELSE
		BEGIN
		
			BREAK
		END

END
  ----------------------------------Inserting into distribution planning detail----------------------
  
		--select * from #vault_amounting 
 

            DROP table if exists #distribution

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #distribution Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				
			Select * into #distribution
				from(
				Select atm_id
					,	site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50
					,   rounded_amount_100
					,   rounded_amount_200
					,   rounded_amount_500
					,   rounded_amount_2000
					, total_expected_balanceT1
					, expected_balanceT1_50
					, expected_balanceT1_100
					, expected_balanceT1_200
					, expected_balanceT1_500
					, expected_balanceT1_2000
					, total_vault_amount
					, vaultingamount_50
					, vaultingamount_100
					, vaultingamount_200
					, vaultingamount_500
					, vaultingamount_2000
					, rounded_vaultingamount_50
					, rounded_vaultingamount_100
					, rounded_vaultingamount_200
					, rounded_vaultingamount_500
					, rounded_vaultingamount_2000
					,(rounded_vaultingamount_50 + rounded_vaultingamount_100	+ rounded_vaultingamount_200
					+ rounded_vaultingamount_500+ rounded_vaultingamount_2000 )
					as total_rounded_vault_amt

					from  #vault_amounting

					)a		   

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #distribution Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	   
			  -- generate indent code.
			  
				drop table if exists #dist_with_cra

				SELECT dist.* ,
					cra.new_cra as cra
					,concat(dist.bank_code,'/',dist.feeder_branch_code,'/',cra.new_cra,'/',format(DATEADD(MI,330,GETUTCDATE()),'yyyyMMddHHmmss')) AS indentcode
				INTO #dist_with_cra
				FROM #distribution dist 
				JOIN cra_feasibility cra
				ON dist.atm_id = cra.atm_id 
				AND dist.site_code = cra.site_code 				
				AND cra.record_status = 'Active'


			
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #feeder_level_forecast Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	

		SELECT 
		indentcode,
		indentdate,
		project_id, 
		bank_code, 
		feeder_branch_code
		,cra
		,SUM(max_loading_amount) max_loading_amount
		,SUM(cassette_50_capacity_amount)	AS max_loading_amount_50
		,SUM(cassette_100_capacity_amount)	AS max_loading_amount_100
		,SUM(cassette_200_capacity_amount)	AS max_loading_amount_200
		,SUM(cassette_500_capacity_amount)	AS max_loading_amount_500
		,SUM(cassette_2000_capacity_amount) AS max_loading_amount_2000
		,SUM(min_loading_amount)			AS min_loading_amount
		,SUM(forecast_loading_amount)		AS forecast_loading_amount
		,SUM(forecast_loading_amount_50)	AS forecast_loading_amount_50
		,SUM(forecast_loading_amount_100)	AS forecast_loading_amount_100
		,SUM(forecast_loading_amount_200)	AS forecast_loading_amount_200
		,SUM(forecast_loading_amount_500)	AS forecast_loading_amount_500
		,SUM(forecast_loading_amount_2000)	AS forecast_loading_amount_2000
		,SUM(total_cassette_capacity)		AS total_cassette_capacity
		,SUM(cassette_50_capacity_amount)	AS cassette_50_capacity_amount
		,SUM(cassette_100_capacity_amount)	AS cassette_100_capacity_amount
		,SUM(cassette_200_capacity_amount)	AS cassette_200_capacity_amount
		,SUM(cassette_500_capacity_amount)	AS cassette_500_capacity_amount
		,SUM(cassette_2000_capacity_amount) AS cassette_2000_capacity_amount

	INTO #feeder_level_forecast
	FROM
		(
			SELECT 
				indentcode,
				indentdate,
				atm_id, 
				project_id, 
				bank_code, 
				feeder_branch_code,
				cra,
				rounded_amount_50 AS forecast_loading_amount_50, 
				rounded_amount_100 AS forecast_loading_amount_100,
				rounded_amount_200 AS forecast_loading_amount_200,
				rounded_amount_500 AS forecast_loading_amount_500,
				rounded_amount_2000 AS forecast_loading_amount_2000,
				(rounded_amount_50 + rounded_amount_100 + 
				rounded_amount_200 + rounded_amount_500 + 
				rounded_amount_2000) AS forecast_loading_amount
				,total_cassette_capacity
				,(
					SELECT MIN(v) FROM (VALUES (bank_cash_limit), 
					(insurance_limit),
					 (total_cassette_capacity)) AS value(v)) 
					 AS max_loading_amount
				,
				(	 CASE  
						WHEN curbal_div_avgDisp>= decidelimitdays 
							THEN 0         
						WHEN decidelimitdays=1 
							THEN 1.5*avgdispense
						WHEN cashout=1 
							THEN (CASE 
									WHEN  loadingGap_cur_bal_avg_disp >100000 
										THEN loadingGap_cur_bal_avg_disp
										ELSE loadingGap_cur_bal_avg_disp 
									END)
							ELSE  loadingGap_cur_bal_avg_disp  
						END	) 
						AS min_loading_amount	
						
				,  cassette_50_capacity_amount - morning_balance_50		AS cassette_50_capacity_amount,
				  cassette_100_capacity_amount - morning_balance_100	AS cassette_100_capacity_amount,
				  cassette_200_capacity_amount - morning_balance_200	AS cassette_200_capacity_amount,
				  cassette_500_capacity_amount - morning_balance_500	AS cassette_500_capacity_amount,
				  cassette_2000_capacity_amount - morning_balance_2000	AS cassette_2000_capacity_amount

				FROM #dist_with_cra

				)atm
				 GROUP by project_id, bank_code, feeder_branch_code,indentdate,indentcode,cra


			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #feeder_level_forecast Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			--select * from #feeder_level_forecast
			--select * from #final_feeder_level_indent

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #final_feeder_level_indent Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			  Select FL.*  ,
					a.vaultingamount_50,
					a.vaultingamount_100,
					a.vaultingamount_200,
					a.vaultingamount_500,
					a.vaultingamount_2000,
					a.total_rounded_vault_amt,
					VCB.vault_balance_100 ,
					VCB.vault_balance_200,
					VCB.vault_balance_500,
					VCB.vault_balance_2000,
					VCB.total_vault_balance,
					FL.forecast_loading_amount_50 as indent_50,
					FL.forecast_loading_amount_100 -ISNULL(VCB.vault_balance_100,0)+ ISNULL(a.vaultingamount_100 ,0)   as indent_100,
					FL.forecast_loading_amount_200 -ISNULL(VCB.vault_balance_200,0)+ ISNULL(a.vaultingamount_200 ,0)   as indent_200,
					FL.forecast_loading_amount_500 -ISNULL(VCB.vault_balance_500,0)+ ISNULL(a.vaultingamount_500 ,0)   as indent_500,
					FL.forecast_loading_amount_2000 -ISNULL(VCB.vault_balance_2000,0)+ ISNULL(a.vaultingamount_2000,0) as indent_2000

          into #final_feeder_level_indent
		  from #feeder_level_forecast FL

		   left join (

			Select 
			bank_code,project_id,feeder_branch_code,indentdate,
			 
			SUM(rounded_vaultingamount_50)   as vaultingamount_50,
			SUM(rounded_vaultingamount_100)  as vaultingamount_100,
			SUM(rounded_vaultingamount_200)  as vaultingamount_200,
			SUM(rounded_vaultingamount_500)  as vaultingamount_500,   
			SUM(rounded_vaultingamount_2000) as vaultingamount_2000,
			SUM(total_rounded_vault_amt)     as total_rounded_vault_amt			      
			   from #dist_with_cra 			
					group by  bank_code,feeder_branch_code,project_id,indentdate-- add cra and indent_code
					)a
					on FL.bank_code=a.bank_code
					and FL.project_id=a.project_id
					and FL.feeder_branch_code=a.feeder_branch_code

					LEFT JOIN vault_cash_balance VCB
					on FL.bank_code=VCB.bank_name
					and FL.project_id=VCB.project_id
					and FL.feeder_branch_code=VCB.feeder_branch_name
					and cast(VCB.Date as date) = cast(@dateT as date)
					and VCB.record_status ='Active'
					and len(ltrim(rtrim(isnull(atm_id,'')))) < 4 

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into #final_feeder_level_indent Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			  

			  --SELECT * FROM #feeder_level_forecast WHERE bank_code = 'DENA'

			  -- SELECT * FROM distribution_planning_detail
			  --------------------------INSERT INTO DATABASE-----------------------------
			  begin transaction

			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_detail Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
			DECLARE @ForStatus VARCHAR(15) = 'Active'
			DECLARE @ToStatus VARCHAR(15) = 'Deleted'
			

			
			IF EXISTS(
						SELECT 1 
						FROM data_update_log
						WHERE record_status = 'Active'
						AND data_for_type = 'Indent'
						AND datafor_date_time = @dateT
					)

					BEGIN
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old data update log entries for indent ' 
	
						DECLARE @SetWhere_data_update VARCHAR(MAX) = ' data_for_type = ''' + 'Indent' + ''' and datafor_date_time =  ''' + cast(@dateT as nvarchar(50))+ ''''

						EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_data_update,@out OUTPUT

						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for indent' 

					END	

			IF EXISTS(
						SELECT 1 
						FROM distribution_planning_detail
						WHERE record_status = 'Active'
						AND indentdate = @dateT
						AND indent_counter = 1
					)

					BEGIN
							set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in distribution_planning_detail ' 
	
							DECLARE @SetWhere_distribution_detail VARCHAR(MAX) = ' indentdate = ''' + cast(@dateT as nvarchar(50))+ ''' and indent_counter =  1 '

							EXEC dbo.[uspSetStatus] 'dbo.distribution_planning_detail',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_distribution_detail,@out OUTPUT
						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in distribution_planning_detail table for old entries for indent ' 
					
					END	
			IF EXISTS(
						SELECT 1 
						FROM distribution_planning_master
						WHERE record_status = 'Active'
						AND indentdate = @dateT
						AND indent_counter = 1
					)

					BEGIN
					
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in distribution_planning_master ' 
	
						DECLARE @SetWhere_distribution_master VARCHAR(MAX) =  ' indentdate = ''' + cast(@dateT as nvarchar(50))+ ''' and indent_counter =  1 '
							
						EXEC dbo.[uspSetStatus] 'dbo.distribution_planning_master',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_distribution_master,@out OUTPUT

						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in distribution_planning_master table for old entries for indent  ' 
					
					END
		
			IF EXISTS(
						SELECT 1 
						FROM dbo.indent_master
						WHERE record_status = 'Active'
						AND order_date = @dateT
						AND indent_counter = 1
					
					)
					BEGIN
						
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in indent master ' 
						
						DECLARE @SetWhere_indent_master VARCHAR(MAX) = ' order_date = ''' + cast(@dateT as nvarchar(50))+ ''' and indent_counter =  1 '
							
						EXEC dbo.[uspSetStatus] 'dbo.indent_master',@ForStatus,@ToStatus,@timestamp_date,@cur_user,@created_reference_id,@SetWhere_indent_master,@out OUTPUT
												
						set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in indent master table for old entries for indent '
					
					END
					
			IF EXISTS(
							SELECT 1
							FROM dbo.indent_detail id
							JOIN indent_master im
							on im.indent_order_number = id.indent_order_number
							WHERE id.record_status = 'Active'
							AND id.indent_counter = 1
						)

					BEGIN	
						UPDATE 	id
						SET id.record_status = 'Deleted'
						FROM indent_detail id
						JOIN indent_master im
						on im.indent_order_number = id.indent_order_number
						WHERE id.record_status = 'Active'
						AND id.indent_counter = 1					
					END
			
					
			  INSERT INTO  distribution_planning_detail
				(
				[indent_code]
				,[atm_id]
				,[site_code]
				,[sol_id]
				,[bank_code]
				,[project_id]
				,[indentdate]
				,[bank_cash_limit]
				,[insurance_limit]
				,[feeder_branch_code]
				,[cra]
				,[base_limit]
				,[dispenseformula]
				,[decidelimitdays]
				,[loadinglimitdays]
				,[avgdispense]
				,[loading_amount]
				,[morning_balance_50]
				,[morning_balance_100]
				,[morning_balance_200]
				,[morning_balance_500]
				,[morning_balance_2000]
				,[total_morning_balance]
				,[cassette_50_brand_capacity]
				,[cassette_100_brand_capacity]
				,[cassette_200_brand_capacity]
				,[cassette_500_brand_capacity]
				,[cassette_2000_brand_capacity]
				,[cassette_50_count]
				,[cassette_100_count]
				,[cassette_200_count]
				,[cassette_500_count]
				,[cassette_2000_count]
				,[limit_amount]
				,[total_capacity_amount_50]
				,[total_capacity_amount_100]
				,[total_capacity_amount_200]
				,[total_capacity_amount_500]
				,[total_capacity_amount_2000]
				,[total_cassette_capacity]
				,[avgdecidelimit]
				,[DecideLimit]
				,[threshold_limit]
				,[loadinglimit]
				,[applied_vaulting_percentage]
				,[ignore_code]
				,[ignore_description]
				,[dist_Purpose]
				,[defaultamt]
				,[default_flag]
				,[curbal_div_avgDisp]
				,[loadingGap_cur_bal_avg_disp]
				,[CashOut]
				,[remaining_balance_50]
				,[remaining_balance_100]
				,[remaining_balance_200]
				,[remaining_balance_500]
				,[remaining_balance_2000]
				,[cassette_50_capacity_amount]
				,[cassette_100_capacity_amount]
				,[cassette_200_capacity_amount]
				,[cassette_500_capacity_amount]
				,[cassette_2000_capacity_amount]
				,[total_cassette_capacity_amount]
				,[cassette_capacity_percentage_50]
				,[cassette_capacity_percentage_100]
				,[cassette_capacity_percentage_200]
				,[cassette_capacity_percentage_500]
				,[cassette_capacity_percentage_2000]
				,[tentative_loading_amount_50]
				,[tentative_loading_amount_100]
				,[tentative_loading_amount_200]
				,[tentative_loading_amount_500]
				,[tentative_loading_amount_2000]
				,[rounded_amount_50]
				,[rounded_amount_100]
				,[rounded_amount_200]
				,[rounded_amount_500]
				,[rounded_amount_2000]
				,[total_expected_balanceT1]
				,[expected_balanceT1_50]
				,[expected_balanceT1_100]
				,[expected_balanceT1_200]
				,[expected_balanceT1_500]
				,[expected_balanceT1_2000]
				,[total_vault_amount]
				,[vaultingamount_50]
				,[vaultingamount_100]
				,[vaultingamount_200]
				,[vaultingamount_500]
				,[vaultingamount_2000]
				,[rounded_vaultingamount_50]
				,[rounded_vaultingamount_100]
				,[rounded_vaultingamount_200]
				,[rounded_vaultingamount_500]
				,[rounded_vaultingamount_2000]
				,[total_rounded_vault_amt]
				, record_status
				,created_on
				,created_by
				,created_reference_id
				)
			  SELECT	indentcode
					,	atm_id
					,   site_code
					,	sol_id
					,	bank_code
					,	project_id
					,	indentdate
					,	bank_cash_limit
					,	insurance_limit
					,	feeder_branch_code
					,	cra
					,   base_limit
					,	dispenseformula
					,	decidelimitdays
					,	loadinglimitdays
					,	avgdispense
					,	loading_amount
					,	morning_balance_50
					,	morning_balance_100
					,	morning_balance_200
					,	morning_balance_500
					,	morning_balance_2000
					,	total_morning_balance
					,	cassette_50_brand_capacity
					,	cassette_100_brand_capacity
					,	cassette_200_brand_capacity
					,	cassette_500_brand_capacity
					,	cassette_2000_brand_capacity
					,	cassette_50_count
					,	cassette_100_count
					,	cassette_200_count
					,	cassette_500_count
					,	cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	total_capacity_amount_100
					,	total_capacity_amount_200
					,	total_capacity_amount_500
					,	total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					,	cassette_50_capacity_amount
					,	cassette_100_capacity_amount
					,	cassette_200_capacity_amount
					,	cassette_500_capacity_amount
					,	cassette_2000_capacity_amount
					,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	tentative_loading_amount_50
					,	tentative_loading_amount_100
					,	tentative_loading_amount_200
					,	tentative_loading_amount_500
					,	tentative_loading_amount_2000
					,   rounded_amount_50
					,   rounded_amount_100
					,   rounded_amount_200
					,   rounded_amount_500
					,   rounded_amount_2000
					, total_expected_balanceT1
					, expected_balanceT1_50
					, expected_balanceT1_100
					, expected_balanceT1_200
					, expected_balanceT1_500
					, expected_balanceT1_2000
					, total_vault_amount
					, vaultingamount_50
					, vaultingamount_100
					, vaultingamount_200
					, vaultingamount_500
					, vaultingamount_2000
					, rounded_vaultingamount_50
					, rounded_vaultingamount_100
					, rounded_vaultingamount_200
					, rounded_vaultingamount_500
					, rounded_vaultingamount_2000
					, total_rounded_vault_amt 
					, 'Active'
					, @timestamp_date
					, @cur_user
					, @created_reference_id
					FROM #dist_with_cra 
					--delete from #dist_with_cra 
					--where cast(#dist_with_cra.indentdate as date) >(Select max(cast(indentdate as date)) 
					--												from distribution_planning_detail)

				DECLARE @row INT
				SELECT @row	= @@ROWCOUNT
		    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_detail Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


			 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
					
					INSERT INTO distribution_planning_master
					( 
					[indent_code]
				   ,[indentdate]
				   ,[project_id]
				   ,[bank_code]
				   ,[feeder_branch_code]
				   ,[cra]
				   ,[max_loading_amount]
				   ,[max_loading_amount_50]
				   ,[max_loading_amount_100]
				   ,[max_loading_amount_200]
				   ,[max_loading_amount_500]
				   ,[max_loading_amount_2000]
				   ,[min_loading_amount]
				   ,[forecast_loading_amount]
				   ,[forecast_loading_amount_50]
				   ,[forecast_loading_amount_100]
				   ,[forecast_loading_amount_200]
				   ,[forecast_loading_amount_500]
				   ,[forecast_loading_amount_2000]
				   ,[total_cassette_capacity]
				   ,[cassette_50_capacity_amount]
				   ,[cassette_100_capacity_amount]
				   ,[cassette_200_capacity_amount]
				   ,[cassette_500_capacity_amount]
				   ,[cassette_2000_capacity_amount]
				   ,[vaultingamount_50]
				   ,[vaultingamount_100]
				   ,[vaultingamount_200]
				   ,[vaultingamount_500]
				   ,[vaultingamount_2000]
				   ,[total_rounded_vault_amt]
				   ,[vault_balance_100]
				   ,[vault_balance_200]
				   ,[vault_balance_500]
				   ,[vault_balance_2000]
				   ,[total_vault_balance]
				   ,[indent_50]
				   ,[indent_100]
				   ,[indent_200]
				   ,[indent_500]
				   ,[indent_2000]
				   ,[record_status]
				   ,[created_on]
				   ,[created_by]
				   ,[created_reference_id]				   
				   )
				SELECT 	
					 indentcode
					,indentdate
					,project_id
					,bank_code
					,feeder_branch_code
					,cra
					,max_loading_amount
					,max_loading_amount_50
					,max_loading_amount_100
					,max_loading_amount_200
					,max_loading_amount_500
					,max_loading_amount_2000
					,min_loading_amount
					,forecast_loading_amount
					,forecast_loading_amount_50
					,forecast_loading_amount_100
					,forecast_loading_amount_200
					,forecast_loading_amount_500
					,forecast_loading_amount_2000
					,total_cassette_capacity
					,cassette_50_capacity_amount
					,cassette_100_capacity_amount
					,cassette_200_capacity_amount
					,cassette_500_capacity_amount
					,cassette_2000_capacity_amount
					,vaultingamount_50
					,vaultingamount_100
					,vaultingamount_200
					,vaultingamount_500
					,vaultingamount_2000
					,total_rounded_vault_amt
					,vault_balance_100
					,vault_balance_200
					,vault_balance_500
					,vault_balance_2000
					,total_vault_balance
					,indent_50
					,indent_100
					,indent_200
					,indent_500
					,indent_2000
					, 'Active'
					, @timestamp_date
					, @cur_user
					, @created_reference_id
				FROM #final_feeder_level_indent


            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	        values ('Insert into distribution_planning_master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			
--------- ----------------------------------feeder level indent amount generation---------------------------------------------------
	
 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #pre_Cipher_details_rev Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	 
	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	 values ('Insert into #pre_Cipher_details_rev Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		

	------------------------------------------------- Indent Code generation--------------------------------------------------
	--DECLARE @dateT DATE 
	--SET @dateT='2018-11-22'
		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			  values ('Insert into #indent_code Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))



	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		


	INSERT INTO indent_master
		(  [indent_order_number]
	      ,[order_date]
	      ,[collection_date]
	      ,[replenishment_date]
	      ,[cypher_code]
	      ,[bank]
	      ,[feeder_branch]
	      ,[cra]
	      ,[total_atm_loading_amount_50]
	      ,[total_atm_loading_amount_100]
	      ,[total_atm_loading_amount_200]
	      ,[total_atm_loading_amount_500]
	      ,[total_atm_loading_amount_2000]
	      ,[total_atm_loading_amount]
	      ,[cra_opening_vault_balance_50]
	      ,[cra_opening_vault_balance_100]
	      ,[cra_opening_vault_balance_200]
	      ,[cra_opening_vault_balance_500]
	      ,[cra_opening_vault_balance_2000]
	      ,[cra_opening_vault_balance]
	      ,[total_bank_withdrawal_amount_50]
	      ,[total_bank_withdrawal_amount_100]
	      ,[total_bank_withdrawal_amount_200]
	      ,[total_bank_withdrawal_amount_500]
	      ,[total_bank_withdrawal_amount_2000]
	      ,[total_bank_withdrawal_amount]
	      ,[authorized_by_1]
	      ,[authorized_by_2]
	      ,[notes]
	      ,[amount_in_words]
	      ,[indent_type]
	      ,[project_id]
	      ,[record_status]
	      ,[created_on]
		  )
		  SELECT indentcode
				 ,indentdate AS [order_date]
				 ,indentdate AS [collection_date]
				 ,indentdate AS [replenishment_date]
				 ,'PENDING_INTEGRATION' as [cypher_code] 
				 ,[bank_code]
				 ,[feeder_branch_code]
				 ,[cra]
				 ,indent_50
				 ,indent_100
				 ,indent_200
				 ,indent_500
				 ,indent_2000
				 ,indent_50+indent_100+indent_200+indent_500+indent_2000 
				 , 0 AS [cra_opening_vault_balance_50]
				 , vault_balance_100   AS [cra_opening_vault_balance_100]
				 , vault_balance_200   AS [cra_opening_vault_balance_200]
				 , vault_balance_500   AS [cra_opening_vault_balance_500]
				 , vault_balance_2000  AS [cra_opening_vault_balance_2000]
				 , total_vault_balance AS [cra_opening_vault_balance]
				 , indent_50 AS [total_bank_withdrawal_amount_50]
				 , indent_100 AS [total_bank_withdrawal_amount_100]
				 , indent_200 AS [total_bank_withdrawal_amount_200]
				 , indent_500 [total_bank_withdrawal_amount_500]
				 , indent_2000 AS [total_bank_withdrawal_amount_2000]
				 , (indent_50+indent_100+indent_200+indent_500+indent_2000 ) AS [total_bank_withdrawal_amount]
				 ,'' AS [authorized_by_1]
				 ,'' AS [authorized_by_2]
				 ,'' AS [notes]
				 ,'PENDING' --dbo.fnNumberToWords((indent_50+indent_100+indent_200+indent_500+indent_2000 )) AS [amount_in_words]
				 ,'Indent' AS [indent_type]
				 ,[project_id]
				 ,'Active' [record_status]
				 , getdate()  AS [created_on]				 
				 from #final_feeder_level_indent 

				 
                 DECLARE @rowcount INT	
				 DECLARE @ErrorCode INT		 
                 SELECT @rowcount=@@ROWCOUNT,@ErrorCode = @@error


	  	 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


--------------------------------Inserting into Indent Detail table on atm level------------------------------------------

			  
		 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_detail Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	  
	INSERT INTO indent_detail
	(  
	       indent_order_number,
	       [atm_id]
	      ,[location]
	      ,[purpose]
	      ,[loading_amount_50]
	      ,[loading_amount_100]
	      ,[loading_amount_200]
	      ,[loading_amount_500]
	      ,[loading_amount_2000]
	      ,[total]
	      ,[record_status]
	      ,[created_on]
		  ,created_by
		  ,created_reference_id
	)
	SELECT
		     A.indentcode ,
			 A.atm_id,
			 M.location_name, 
			 A.dist_Purpose,
			 A.rounded_amount_50,
			 A.rounded_amount_100,
			 A.rounded_amount_200,
			 A.rounded_amount_500,
			 A.rounded_amount_2000,
			 isnull(A.rounded_amount_50,0)   + isnull(A.rounded_amount_100,0) +	 isnull(A.rounded_amount_200,0) + isnull(A.rounded_amount_500,0) +isnull( A.rounded_amount_2000,0) AS [total] ,
			 'Active' AS [record_status],
			 @timestamp_date  AS [created_on],
			 @cur_user,
			 @created_reference_id			
		 FROM #dist_with_cra   A
		 LEFT JOIN atm_master M
		 ON A.site_code = M.site_code
		 AND A.atm_id  = M.atm_id
		 AND M.record_status ='Active'


						DECLARE @row_dul INT			 
                        SET @row_dul=@@ROWCOUNT

                        INSERT into data_update_log 
									(bank_code,
									project_id,
									operation_type,
									datafor_date_time,
									data_for_type,			 
									status_info_json,			 
									record_status,
									date_created,
									created_by,
									created_reference_id
									)
                                values
									(NULL,
									NULL,
									'Indent',
									@dateT,
									'Indent',			 
									'Indent calculated for date '+cast(@dateT as varchar(30)) + ' and no. of rows inserted : '+ cast(@row_dul as varchar(10)),
									'Active',
									@timestamp_date,
									@cur_user,
									@created_reference_id			 			
									)

		
			 IF (@ErrorCode = 0)
                        BEGIN
                            IF(@row > 0)
                            BEGIN
                                    SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'success') 
                                    SELECT @outputval
                            END
							ELSE
							BEGIN
									SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'no_records') 
                                    SELECT @outputval
							END
						END
	  commit transaction
 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Insert into indent_detail Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
	     values ('Execution of [dbo].[usp_indent_calculation_SIT] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
	

END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
                        SELECT @outputval	
										
			 END CATCH
END