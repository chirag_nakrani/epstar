 
/****** Object:  StoredProcedure [dbo].[usp_DecideLimitDays]    Script Date: 19-12-2018 12:49:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER Procedure [dbo].[usp_DecideLimitDays_test] (@dateT date,@systemuser nvarchar(50),@referenceid nvarchar(100))
as
BEGIN

BEGIN TRY

		declare @ForStatus_active nvarchar(50)
		declare @ToStatus_deleted nvarchar(50)
		DECLARE @timestamp_date varchar(50) =convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120);
		DECLARE @operation_type  varchar(40)
		SET @operation_type='Decide_Limit_days'
		DECLARE @out nvarchar(50)
		DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
		declare @Execution_log_str nvarchar(max)
		set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@dateT as nvarchar(50)) 
		DECLARE @outputVal varchar(50)


	 --declare @dateT datetime
	 --set @dateT ='2018-11-05'
	 --EXEC [usp_DecideLimitDays_test] '2018-11-30'


	--************************************Logging*******************************************************
         INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
         values ('Execution of [dbo].[usp_DecideLimitDays] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		
      --------------- Calculating decide limit days based on the CRA_feasilibilty matrix --------------------------

      --------------- Creating calender of seven days for the indent date @dateT-----------------------------------

		;with calendar
		 as
		 (
		  select @dateT startDt,dateadd(day,7,@dateT) endDt, 1 rn
		  union all
		  select dateadd(day,1,startDt) , endDt,rn +1
		  from calendar
		  WHERE  dateadd(day,1,startDt)<endDt
          )
		  
      ------------- Creating pivot of of the weekdays with those of cra_feasibility collumns--------------------------------
		
		,cte 
		 as 
		 (
			select 	atm_id,
					bank_code,
					site_code,
					project_id,
					v.*
		    from CRA_feasibility 
			     cross apply (values ('monday'   ,is_feasible_mon), 
									 ('tuesday'  ,is_feasible_tue),
									 ('wednesday',is_feasible_wed),
							         ('thursday' ,is_feasible_thu),
					                 ('friday'   ,is_feasible_fri),
						             ('saturday' ,is_feasible_sat)
							  ) v(name,flag)
		 )


			-------------calculating the next feasible day and date from 
			---          above two datasets checking 2nd and 4th Sat also for the same

                    Select * into #temp
					from
					(
					select  atm_id,
							bank_code,
							site_code,
							project_id,
						    @dateT today,
						--case when t2.flag='N' then null else
						    min(case when flag = 'Yes' 
									 and (DATENAME(dw,startDt)<> 'Saturday'
									 or (DATENAME(dw,startDt) = 'Saturday' 
	                                 and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, startDt), 0), startDt) +1) 
									             in (1,3,5) )) 
									 then startDt end )      nextYDay,

                           datename(dw,min(case when flag = 'Yes' 
						           and (DATENAME(dw,startDt)<> 'Saturday'
								    or (DATENAME(dw,startDt) = 'Saturday' 
	                               and (DATEDIFF(WEEK, DATEADD(MONTH, DATEDIFF(MONTH, 0, startDt), 0), startDt) +1) 
									             in (1,3,5) ))
										        then startDt end ) ) nextYname

										from calendar t1 
										join cte t2 
										on datename(dw,startDt) = t2.name
										and  startDt>@dateT
										group by atm_id,
												bank_code,
												site_code,
												project_id
										)a


				--Select * from #temp
				--select * from CRA_feasibility

				-----************************************Logging******************************************************

           INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
            values ('Insert into limitdays Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
				---Finally calculating decidelimit  and loadinglimit days and inserting it into the table
	BEGIN TRANSACTION
			
			set @ForStatus_active = 'Active'
			set @ToStatus_deleted = 'Deleted'
			IF EXISTS   (						
									SELECT 1
									FROM data_update_log
									WHERE 
						                datafor_date_time = @dateT
						                AND 
						                data_for_type = @operation_type
						                and 
						                record_status = 'Active'
									)	
					BEGIN
					
					
					set @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old data update log entries for ' + @operation_type
										
					DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' datafor_date_time = ''' + CAST(@dateT  as varchar(50))+ 
					                                            ''' and data_for_type = ''' + @operation_type +''''
	
					--SELECT @SetWhere_DataUpdateLog

					EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus_active,@ToStatus_deleted,@timestamp_date,@systemuser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
					--select @out
					
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in data_update_log table for old entries for ' + @operation_type
				
					END
				
			IF EXISTS   (						
					SELECT 1
					FROM limitdays
					WHERE 
					    indent_date = @dateT
					)	
				 BEGIN
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Found old entries in decide limit days ' 
										
					DECLARE @SetWhere_limitdays VARCHAR(MAX) =  ' indent_date = ''' + CAST(@dateT  as varchar(20))+ ''''
	

					EXEC dbo.[uspSetStatus] 'dbo.limitdays',@ForStatus_active,@ToStatus_deleted,@timestamp_date,@systemuser,@referenceid,@SetWhere_limitdays,@out OUTPUT
					
					SET @Execution_log_str = @Execution_log_str + CHAR(13) + CHAR(10) + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ': Status updated in limit days table for old entries ' 
				

				 END
									
							Insert  into limitdays 
							(atm_id,
							bank_code,
							eps_site_code,
							project_id,
							indent_date,
							next_feasible_date,
							decidelimitdays,
							loadinglimitdays)
							select 
							atm_id,
							bank_code,
							site_code,
							project_id,
							today, 
							nextYDay , 
							DATEDIFF(day, today,nextYDay) as [decidelimitdays]
							, case when  datediff(day, today,nextYDay) >3
								   then  datediff(day, today,nextYDay) +0.5 
							       else datediff(day, today,nextYDay) +1 
							       end as loadinglimitdays
							from #temp
	
	

			--*************************************Data_update_log entry*****************************************

 
		        	DECLARE @row INT			 
					DECLARE @ErrorCode INT
			 		SELECT @row=@@ROWCOUNT,@ErrorCode = @@error

					INSERT into data_update_log 
					(bank_code,
					project_id,
					operation_type,
					datafor_date_time,
					data_for_type,			 
					status_info_json,			 
					record_status,
					date_created
					)

					values
					(NULL,
					NULL,
					'Decide_Limit_days', --
					@dateT,
					'Decide_Limit_days',			 
					'Decide Limit days calculated and no. of rows inserted : '+cast(@row as varchar(10)),
					'Active',
					DATEADD(MI,330,GETUTCDATE())			
					)
			---****************************************Logging****************************************************
		 
		 IF (@ErrorCode = 0)
                        BEGIN
                            IF(@row > 0)
                            BEGIN
                                    SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'success') 
                                    SELECT @outputval
                            END
							ELSE
							BEGIN
									SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'no_records') 
                                    SELECT @outputval
							END
						END

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
			 values ('Insert into limitdays Completed and Execution of [dbo].[usp_DecideLimitDays] Completed with recordcount ' + cast(@row as nvarchar(10)), DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	 

COMMIT;
END TRY

BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name;
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
                        SELECT @outputval	
			 END CATCH
 END