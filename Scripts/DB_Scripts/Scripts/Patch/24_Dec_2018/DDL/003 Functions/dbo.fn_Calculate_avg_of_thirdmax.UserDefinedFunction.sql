 
/****** Object:  UserDefinedFunction [dbo].[fn_Calculate_avg_of_thirdmax]    Script Date: 20-12-2018 19:37:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --- ========================================================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: calculated average of the average dispense  of third maximum dispense amount for each of three months
 --- ========================================================================================================

 
 ALTER FUNCTION [dbo].[fn_Calculate_avg_of_thirdmax]
(@atm_id varchar(20),@datetoday date,@bank varchar(50), @project varchar(30))
RETURNS int
AS
BEGIN
  

/*

declare @datetoday as date
set @datetoday='2018-10-15'  --15 coctober
Select  CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@datetoday)  )  , 105)	

*/
--Select * from cash_dispense_dataset1

declare @max1 int,@max2 int, @max3 int,@max4 int,@max_of_thirdmax int,@avg_of_thirdmax int
 
 -- Get third maximum dispense amount for three month ago
SET @max1=
(
Select min(total_dispense_amount)
from 
( select distinct top 3 total_dispense_amount from [cash_dispense_month_minus_3] 
where   atm_id=@atm_id and bank=@bank and project_id=@project
order by total_dispense_amount desc

) as a
)

-- Get third maximum dispense amount for two month ago
SET @max2=
(
Select min(total_dispense_amount)
from 
( select distinct top 3 total_dispense_amount from [cash_dispense_month_minus_2] 
where   atm_id=@atm_id and bank=@bank and project_id=@project
order by total_dispense_amount desc

) as a
)

-- Get third maximum dispense amount for one month ago
SET @max3=
(
Select min(total_dispense_amount)
from 
( select distinct top 3 total_dispense_amount from [cash_dispense_month_minus_1]
where   atm_id=@atm_id and bank=@bank and project_id=@project
order by total_dispense_amount desc

) as a
)

-- Get third maximum dispense amount for current month
SET @max4=
(
Select min(total_dispense_amount)
from 
( select distinct top 3 total_dispense_amount from [cash_dispense_month_minus_0]
where   atm_id=@atm_id and bank=@bank and project_id=@project
order by total_dispense_amount desc

) as a
)

 


--SET @avg_of_thirdmax= 
--(
--SELECT  (ISNULL(@max1,0)+ISNULL(@max2,0)+ISNULL(@max3,0)+ISNULL(@max4,0))/4
--)

SET @avg_of_thirdmax= 
(
SELECT avg(n)
FROM (VALUES (@max1), (@max2), (@max3), (@max4)) AS i(n)
)  
 
 return @avg_of_thirdmax
  
 END

 
