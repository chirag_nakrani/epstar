USE [epstar]
GO

SET ANSI_PADDING ON
GO


CREATE NONCLUSTERED INDEX [NonClusteredIndex_C3R_CMIS] ON [dbo].[c3r_CMIS]
(
	[bank] ASC,
	[feeder_branch] ASC,
	[atm_id] ASC,
	[cra_name] ASC,
	[project_id] ASC,
	[record_status] ASC,
	[created_reference_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)


CREATE NONCLUSTERED INDEX [NonClusteredIndex_C3R_VMIS] ON [dbo].[c3r_VMIS]
(
	[bank] ASC,
	[feeder_branch_name] ASC,
	[cra_name] ASC,
	[project_id] ASC,
	[record_status] ASC,
	[created_reference_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)