Create PROCEDURE [dbo].[usp_validate_C3R_CMIS]
( 
	@datafor_date_time varchar(50) 
	,@ForRecordstatus varchar(50)
	,@referenceid varchar(50)
	,@systemUser varchar(50)
	,@outputVal varchar(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @errorcode nvarchar(max)
	--DECLARE @outputVal varchar(max) 
	DECLARE @datafor nvarchar(5) = 'C3R'
	DECLARE @pendingstatus nvarchar(20) = 'Approval Pending'
	DECLARE @uploadedstatus nvarchar(20) = 'Uploaded'
	DECLARE @deletedstatus nvarchar(20) = 'Deleted'
	Declare @EODStatus nvarchar(20) = 'EOD'
	Declare @LoadingStatus nvarchar(20) = 'Loading'


	--declare @datafor_date_time varchar(50) = '2019-04-29 00:00:00.000'
	--declare @ForRecordstatus varchar(50) = 'Uploaded'
	--declare @referenceid varchar(50) = 'C10057490224439'
	--declare @systemUser varchar(50) = 'SA'
	


	


	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[usp_validate_C3R_CMIS] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

------------------ Check if file is present in Data Update Log---------------

	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[data_update_log] WITH (NOLOCK)
				WHERE [datafor_date_time] = @datafor_date_time AND 
                      data_for_type = @datafor and 
                      record_status = @uploadedstatus AND 
					created_reference_id = @referenceid
				)
				
		BEGIN





			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			
------------------ Check if file is present in Bank Wise File Table ----------------------

			IF EXISTS( Select 1 as ColumnName
						FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
						WHERE [datafor_date_time] = @datafor_date_time AND 
							  record_status = @ForRecordstatus 
							  AND created_reference_id = @referenceid
				)
				
				BEGIN
					
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Checking in [dbo].[c3r_CMIS]', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				------- Select total count of records present inb file for particular date and status------------
				
				SET @CountTotal = (	
									SELECT count(1) 
									FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
						            WHERE [datafor_date_time] = @datafor_date_time 
										  and record_status = @ForRecordstatus 
										  AND created_reference_id = @referenceid					
								   ) 

				------------- Creating temporary table to process multiple update operations ---------------------------
	
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Creating Temporary Table and storing data', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


				

				update C3R_CMIS 
				set bank = case
							when bank = 'BOB MS' then 'BOB'
							when bank = 'CANARA' then 'CAB'
							when bank = 'CANARA MS' then 'CAB'
							when bank = 'DENA MS' then 'DENA'
							when bank = 'UBI MS' then 'UBI'
							else bank
							end
				where created_reference_id = @referenceid
			    
				
				DROP TABLE IF EXISTS #temp_C3R_CMIS
				SELECT * 
				INTO #temp_C3R_CMIS 
				FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
				WHERE datafor_date_time= @datafor_date_time
					  AND record_status = @ForRecordstatus  
					  AND created_reference_id = @referenceid
				
				CREATE CLUSTERED INDEX IX_Temp_C3R_CMIS  
				ON #temp_C3R_CMIS (id, bank,atm_id asc,feeder_branch asc, record_status asc, datafor_date_time desc)


				DROP TABLE IF EXISTS #temp_C3R_VMIS
				SELECT * 
				INTO #temp_C3R_VMIS 
				FROM [dbo].[c3r_VMIS] WITH (NOLOCK)
				WHERE datafor_date_time= @datafor_date_time
					  AND record_status = @ForRecordstatus  
					  AND created_reference_id = @referenceid
					  
				update #temp_C3R_VMIS
				SET error_code = NULL, is_valid_record = 'Yes'


				UPDATE C3RVMIS 
				SET C3RVMIS.is_valid_record =  temp.is_valid_record,          
					C3RVMIS.error_code = temp.error_code

					FROM [c3r_VMIS] C3RVMIS
					INNER JOIN #temp_C3R_VMIS temp
					on temp.bank = C3RVMIS.bank
					and temp.cra_name = C3RVMIS.cra_name
					and temp.feeder_branch_name = C3RVMIS.feeder_branch_name
					AND C3RVMIS.created_reference_id = @referenceid
					
				
				

					
				
			    
				-- atm with multiple row with status_of_loading as 'LOADING' will be marked as deleted except latest recored with latest eod_loading_time
				-- also if "Loading" and "EOD" are present then for a record then "Loading" will have preference and other will get deleted
				-- so there will one record active for 'LOADING' status
				;with cte1 as
				(
					select  ROW_NUMBER() over (partition by atm_id, feeder_branch, bank order by eod_loading_time desc) as rownum, *
					from #temp_C3R_CMIS 
					where status_of_loading = @LoadingStatus
					and record_status <> @deletedstatus
	
				)
				update cte1 set record_status = @deletedstatus, is_valid_record = 'No', deleted_on = getdate(), deleted_by=@systemUser, deleted_reference_id = @referenceid
				where rownum <> '1'


				--Marking records with status_of_loading with  EOD+Loading
				--Loading status will have preference and EOD record will be marked as deleted
				;with cte2 as
				(
					select  ROW_NUMBER() over (partition by atm_id, feeder_branch, bank order by status_of_loading desc) as rownum, *
					from #temp_C3R_CMIS 
					where (status_of_loading = @LoadingStatus or status_of_loading = @EODStatus)
					and record_status <> @deletedstatus
				)
				update cte2 set record_status = @deletedstatus, is_valid_record = 'No', deleted_on = getdate(), deleted_by=@systemUser, deleted_reference_id = @referenceid
				where rownum <> '1'

				--Record with multiple EOD --select latest one
				;with cte3 as
				(
					select  ROW_NUMBER() over (partition by atm_id, feeder_branch, bank order by status_of_loading desc) as rownum, *
					from #temp_C3R_CMIS 
					where status_of_loading = @EODStatus
					and record_status <> @deletedstatus
				)
				update cte3 set record_status = @deletedstatus, is_valid_record = 'No', deleted_on = getdate(), deleted_by=@systemUser, deleted_reference_id = @referenceid
				where rownum <> '1'



				----------------AMS Master Validation : Checking record available in ATM Master---------------------------
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Validation for AMS master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				UPDATE #temp_C3R_CMIS 
				SET    error_code =  
						CASE 
							WHEN atm_id  in (Select atm_id from atm_master where record_status ='Active' and site_status in ('Active', 'Live'))
								  AND
								  bank  in (Select DISTINCT bank_code from atm_master where record_status ='Active' and site_status in ('Active', 'Live'))
							THEN NULL
							ELSE
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'master_validation_failed')
						END	
				where record_status = @uploadedstatus
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Validation for AMS master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)



			

				-------------------------------------Validation 1------------------------------------------------------------------		
				--			Loading as per ATM Replenishment Details = Loading as per Switch Counter						   	--
				-------------------------------------------------------------------------------------------------------------------

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Validation for Loading as per ATM Replenishment Details Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				--BEGIN TRAN
				UPDATE #temp_C3R_CMIS 
					SET     error_code =  
								CASE 
									WHEN (atm_repl_total = sw_loading_total)
									THEN NULL
									ELSE (
											SELECT cast(sequence as varchar(50)) 
											FROM app_config_param where 
											sub_category = 'atm_repl_total_not_matching' 
											and category = 'C3R validation'
										 )
								END		
					where record_status = @uploadedstatus
				
				--COMMIT TRAN	
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Loading as per ATM Replenishment Details Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				

				
					-------------------------------------Validation 1.1------------------------------------------------------------------		
				--			Validation : If for 1 ATM Id with status = "No EOD and No Loading" is available as a status then it will never have a second entry with loading or EOD on the same day						   	--
				-------------------------------------------------------------------------------------------------------------------
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Validation for - NO EOD NO LOADING - Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				update #temp_C3R_CMIS  set error_code = case when error_code is null
							then (SELECT cast(sequence as varchar(50)) FROM app_config_param with (nolock) where category = 'C3R validation' and sub_category = 'NO_EOD_NO_LOADING_Status_Check')
							else CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param with (nolock) where category = 'C3R validation' and sub_category = 'NO_EOD_NO_LOADING_Status_Check')) as varchar(max))
							end
				 			
				where atm_id in 
				(
				select t1.atm_id from #temp_C3R_CMIS t1
				inner join #temp_C3R_CMIS t2
				on t1.atm_id = t2.atm_id
				and t1.bank = t2.bank
				and t1.cra_name = t2.cra_name
				and t1.feeder_branch = t2.feeder_branch
				where    
				t2.status_of_loading = 'NO EOD NO LOADING'   --it will skip "NO EOD NO LOADING" record from t1 so other records get filtered
				and t1.status_of_loading <> t2.status_of_loading    --will skip single "NO EOD NO LOADING" record 
				and t1.record_status = @uploadedstatus
				and t2.record_status = @uploadedstatus
				)
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Validation for - NO EOD NO LOADING - Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)



				-------------------------------------Validation 1.2------------------------------------------------------------------		
				--			Validation : checking balances(closing and opening) are available 			   	--
				-------------------------------------------------------------------------------------------------------------------
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Validation checking balances available - Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				update #temp_C3R_CMIS 
					   set error_code = 
					   case when error_code is null
							then (SELECT cast(sequence as varchar(50)) FROM app_config_param with (nolock) where category = 'C3R validation' and sub_category = 'NO_EOD_NO_LOADING_BalanceCheck')
							else CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param with (nolock) where category = 'C3R validation' and sub_category = 'NO_EOD_NO_LOADING_BalanceCheck')) as varchar(max))
						end
				  
				where (	isnull(atmcount_op_bal_total, 0) = 0       or       isnull(atm_closingbal_total, 0) = 0      )
				and status_of_loading = 'NO EOD NO LOADING'
				and record_status = @uploadedstatus

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
				values ('Validation checking balances available - completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)



									  
				------------------------------Validation 2------------------------------------		
				 --      ATM Counter Balance = Switch Counter Balance						  --
				------------------------------------------------------------------------------	
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for ATM Counter Balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				--BEGIN TRAN
				UPDATE #temp_C3R_CMIS 
				SET     error_code =  
					  CASE  
					          WHEN	atmcount_op_bal_total <> sw_count_openbal_total 
					          THEN				
					          CASE
					               WHEN (error_code IS NULL)
					               THEN    
					                   (SELECT cast(sequence as varchar(50)) FROM app_config_param with (nolock) where category = 'C3R validation' and sub_category = 'atm_counter_mismatch')
					                ELSE 
					                   CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param with (nolock) where category = 'C3R validation' and sub_category = 'atm_counter_mismatch')) as varchar(max))
					              END
					          ELSE error_code 
					END
				where record_status = @uploadedstatus
				--COMMIT TRAN
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for ATM Counter Balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			--	----------------------------------------Validation 3--------------------------------------------------------------	
			--	--					Sum of amount replenished should match with the total amount								--
			--	------------------------------------------------------------------------------------------------------------------
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for amount replenished Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				--BEGIN TRAN
					UPDATE #temp_C3R_CMIS 
						SET     error_code =  
						CASE  
								WHEN	   
									(atm_repl_100 + atm_repl_200 + atm_repl_500 + atm_repl_2000) <> atm_repl_total 
								THEN				
								CASE
									WHEN (error_code IS NULL)
									THEN    
										(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'amount_replenished')
									ELSE 
										CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'amount_replenished')) as varchar(max))
									END
								ELSE error_code 
						END
						where record_status = @uploadedstatus
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for amount replenished Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
			--	----------------------------------------Validation 4--------------------------------------------------------------	
			--	--					Sum of closing balance should match with the total closing balance							--
			--	------------------------------------------------------------------------------------------------------------------
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for closing balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				--BEGIN TRAN

				UPDATE #temp_C3R_CMIS 
				SET     error_code =  
				CASE  
						WHEN	
							(sw_closing_100 + sw_closing_200 + sw_closing_500 + sw_closing_2000) <> sw_closing_total
						THEN				
						CASE
							WHEN (error_code IS NULL)
							THEN    
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'closing_balance')
							ELSE 
								CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'closing_balance')) as varchar(max))
							END
						ELSE error_code 
				END
				where record_status = @uploadedstatus
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for closing balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)						
		
				----------------------------------------Validation 5--------------------------------------------------------------	
				--					Sum of opening balance should match with the total opening balance							--
				------------------------------------------------------------------------------------------------------------------
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for opening balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--BEGIN TRAN
				UPDATE #temp_C3R_CMIS 
				SET     error_code =  
				CASE  
						WHEN	 
						 (atmcount_op__bal_100 + atmcount_op__bal_200 + atmcount_op__bal_500 + atmcount_op__bal_2000) <> atmcount_op_bal_total
						THEN				
						CASE
							WHEN (error_code IS NULL)
							THEN    
								(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_balance')
							ELSE 
								CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_balance')) as varchar(max))
							END
						ELSE error_code 
				END
				where record_status = @uploadedstatus
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for opening balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				----------------------------------------Validation 6--------------------------------------------------------------	
				--					Sum of remianing cash should match with the total remianing cash 						--
				------------------------------------------------------------------------------------------------------------------
		
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for remianing cash Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--BEGIN TRAN
					UPDATE #temp_C3R_CMIS 
					SET     error_code =  
					CASE  
							WHEN	 
								(atmphysical_total_remainingcash_100 + atmphysical_total_remainingcash_200 + atmphysical_total_remainingcash_500 + atmphysical_total_remainingcash_2000) <> atmphysical_total_remainingcash_total
							THEN				
							CASE
								WHEN (error_code IS NULL)
								THEN    
									(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'remaining_cash')
								ELSE 
									CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'remaining_cash')) as varchar(max))
								END
							ELSE error_code 
					END
					where record_status = @uploadedstatus
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for remianing cash Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		--		select 'Validation 7'
			----------------------------------------Validation 7-------------------------------------------------------------	
			--					Sum of closing balance in atm should match with the total closing balance in atm					--
			------------------------------------------------------------------------------------------------------------------
			
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for closing balance in atm Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--BEGIN TRAN
					UPDATE #temp_C3R_CMIS 
					SET     error_code =  
					CASE  
							WHEN	   
							 (atm_closingbal_100 + atm_closingbal_200 + atm_closingbal_500 + atm_closingbal_2000) <> atm_closingbal_total 
							THEN				
							CASE
								WHEN (error_code IS NULL)
								THEN    
									(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'atm_closing_balance')
								ELSE 
									CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'atm_closing_balance')) as varchar(max))
								END
							ELSE error_code 
					END
					where record_status = @uploadedstatus
				--COMMIT TRAN
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for closing balance in atm Comleted', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--select 'Validation 8'

			--------------------------------------Validation 8-------------------------------------------------------------	
				--				Previous day closing amount should match with the current day opening amount				--
			----------------------------------------------------------------------------------------------------------------
			IF EXISTS ( SELECT 1 FROM c3r_CMIS old with (nolock) 
						JOIN #temp_C3R_CMIS new with (nolock) 
						on old.atm_id = new.atm_id
						AND old.bank = new.bank
						where old.datafor_date_time = dateadd(day,-1,new.datafor_date_time)
						AND old.record_status =	'Active'
						and old.status_of_loading = new.status_of_loading
						and new.record_status = @uploadedstatus
						)
				BEGIN
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
					values ('Validation for Previous day closing amount should match with the current day opening amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						--BEGIN TRAN
						UPDATE #temp_C3R_CMIS 
						SET     error_code = 
						CASE  
								WHEN	   
									atmcount_op_bal_total <> (
															SELECT sw_closing_total
															FROM c3r_CMIS c WHERE 
															c.datafor_date_time = dateadd(day,-1,#temp_C3R_CMIS.datafor_date_time) 
															AND c.record_status			=	'Active'
															AND #temp_C3R_CMIS.record_status = @uploadedstatus
															AND #temp_C3R_CMIS.atm_id		=	c.atm_id
															AND #temp_C3R_CMIS.bank			=	c.bank
															AND #temp_C3R_CMIS.feeder_branch =	c.feeder_branch
															--And #temp_C3R_CMIS.status_of_loading = c.status_of_loading   
															)
								THEN				
								CASE
									WHEN (error_code IS NULL)
									THEN    
										(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_closing_amount')
									ELSE 
										CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_closing_amount')) as varchar(max))
									END
								ELSE error_code 
						END
						INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Previous day closing amount should match with the current day opening amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						--COMMIT TRAN
					END
				

	
				
				--------------------------------------Validation 8.1-------------------------------------------------------------	
				--				Previous day closing amount should match with the current day opening amount				--
			----------------------------------------------------------------------------------------------------------------
			IF EXISTS ( SELECT 1 FROM c3r_CMIS old with (nolock) 
						JOIN #temp_C3R_CMIS new with (nolock) 
						on old.atm_id = new.atm_id
						AND old.bank = new.bank
						where old.datafor_date_time = dateadd(day,-1,new.datafor_date_time)
						AND old.record_status =	'Active'
						and old.status_of_loading = new.status_of_loading
						and new.record_status = @uploadedstatus
						)
				BEGIN
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
					values ('Validation for Previous day closing amount should match with the current day opening amount Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						--BEGIN TRAN
						UPDATE #temp_C3R_CMIS 
						SET     error_code = 
						CASE  
								WHEN	   
									atmcount_op_bal_total <> (
															SELECT atm_closingbal_total
															FROM c3r_CMIS c WHERE 
															c.datafor_date_time = dateadd(day,-1,#temp_C3R_CMIS.datafor_date_time) 
															AND c.record_status			=	'Active'
															AND #temp_C3R_CMIS.record_status = @uploadedstatus
															AND #temp_C3R_CMIS.atm_id		=	c.atm_id
															AND #temp_C3R_CMIS.bank			=	c.bank
															AND #temp_C3R_CMIS.feeder_branch =	c.feeder_branch
															--And #temp_C3R_CMIS.status_of_loading = c.status_of_loading   
															)
								THEN				
								CASE
									WHEN (error_code IS NULL)
									THEN    
										(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_atmclosing_amount')
									ELSE 
										CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'opening_atmclosing_amount')) as varchar(max))
									END
								ELSE error_code 
						END
						INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Previous day closing amount should match with the current day opening amount Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						--COMMIT TRAN
					END

					


				
					--select 'Validation 9'
			----------------------------------------Validation 7-------------------------------------------------------------	
			--					Total Loading as per CMIS= Total loading as per VMIS										--
			------------------------------------------------------------------------------------------------------------------
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Total Loading as per CMIS = Total loading as per VMIS Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				--BEGIN TRAN
					
					DROP TABLE IF EXISTS #temp
					DROP TABLE IF EXISTS #validrecords

					;with #temp
					as
					(
						select sum(sw_loading_total) as c3r_amount,bank,feeder_branch 
						from #temp_C3R_CMIS
						where record_status = @uploadedstatus
						group by bank,feeder_branch
					)

					
						
					select vmis.*, 
					case 
					when 
					temp.c3r_amount = vmis.total_replenishto_atm
					then 'Valid'
					else 'Invalid'
					END
					as record_flag
					into #validrecords 
					from c3r_VMIS vmis
					join #temp temp 
					on vmis.bank = temp.bank
					and vmis.feeder_branch_name = temp.feeder_branch
					AND vmis.created_reference_id = @referenceid

					


					--compared sw_loading_total with VMIS and gathered valid records,  marking everyone invalid if one status is invalid in #validrecords
					UPDATE c 
					SET     error_code = 
						 CASE  
							WHEN	   
							 v.record_flag = 'Invalid'
							THEN				
							CASE
								WHEN (c.error_code IS NULL)
								THEN    
									(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'cmis_vmis_mismatch')
								ELSE 
									CAST(CONCAT(c.error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'C3R validation' and sub_category = 'cmis_vmis_mismatch')) as varchar(max))
								END
							ELSE c.error_code 
					END 
					FROM #temp_C3R_CMIS c
					JOIN #validrecords v
					on c.bank = v.bank 
					AND c.feeder_branch = v.feeder_branch_name
					where 
					c.record_status = @uploadedstatus

						
						
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Total Loading as per CMIS = Total loading as per VMIS completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			--	select 'Validation 10'
				
				
			---------Updating is_valid_record column as Yes or No-------------
			--BEGIN TRAN
			UPDATE #temp_C3R_CMIS
			SET    is_valid_record = 
					CASE 
						WHEN error_code IS NULL
						THEN 'Yes'
						When error_code = '70018'
						THEN 'YES'
						ELSE 'No'
					END		
			where record_status = @uploadedstatus
			--COMMIT TRAN
			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Is Valid Record Field Updated';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Is Valid Record Field Updated', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
			-----------------Checking count for valid records in updated temporary table -----------------------------

			SET @countCalculated = (
									SELECT count(1) 
									FROM #temp_C3R_CMIS  WITH (NOLOCK)
									WHERE is_valid_record = 'Yes' 
									and record_status = @uploadedstatus 
									
									)
			set @CountTotal = (
								SELECT count(1) 
								FROM #temp_C3R_CMIS  WITH (NOLOCK)
								WHERE record_status = @uploadedstatus 
									
							  )
-----------------Comparing both the counts ---------------------------------------------------------
			

			IF(@countCalculated != @CountTotal AND @countCalculated > 0)
				BEGIN
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Count Mismatch (Partial Valid File)';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
			
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'Exception' and sub_category = 'Partial Valid'
									)	
							
				END
			ELSE IF (@countCalculated = @CountTotal)
				BEGIN 
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Count Matched (Valid File)';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				SET @outputVal = (
								SELECT sequence from  [dbo].[app_config_param]
								where  category = 'File Operation' and sub_category = 'Data Validation Successful'
							)												
				END

			ELSE
				BEGIN
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- No valid record found in table';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
									)				
			END

------------------------Inserting distinct error code in new temporary table. Also splitting with , ------------------------

			IF OBJECT_ID('tempdb..#temp_distinct_codes') IS NOT NULL
			BEGIN
				DROP TABLE #temp_distinct_codes
			END
			ELSE
			BEGIN

				
					DECLARE @Names VARCHAR(max) 
					;with distinct_error_codes
					as
					(
						select distinct error_code from #temp_C3R_CMIS
						where error_code is not null
						and record_status = @uploadedstatus 
					)
					SELECT @Names = COALESCE(@Names + ',','') +  TRIM(error_code)
					FROM  distinct_error_codes

					SELECT DISTINCT VALUE INTO #temp_distinct_codes FROM string_split (@Names,',') 
		
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Temporary Table Created for storing error code', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

--------------------- Creating one single error code string with , seperated delimiter---------------------------------------				
				
				SET @errorcode = (
								SELECT 
									Stuff((
										SELECT N', ' + VALUE FROM #temp_distinct_codes WITH(NOLOCK) FOR XML PATH(''),TYPE)
										.value('text()[1]','nvarchar(max)'),1,2,N''
										)
								)
					
			END			
			
--			
------------------------- Updating columns in bank wise file using temporary table------------------------------------------
				BEGIN TRAN
				UPDATE C3R 
				SET C3R.is_valid_record =  b.is_valid_record,
					C3R.error_code = b.error_code,
					record_status = 
						CASE 
							WHEN b.is_valid_record = 'Yes' 
							THEN @pendingstatus
							WHEN b.is_valid_record = 'No' and b.record_status = @deletedstatus
							Then @deletedstatus
							WHEN b.is_valid_record = 'No' 
							THEN @uploadedstatus
							
						END
				FROM [c3r_CMIS] C3R
					INNER JOIN #temp_C3R_CMIS b
					on b.atm_id = C3R.atm_id
					and c3r.status_of_loading = b.status_of_loading            -- record_status = 'DELETED' also marked as 'UPLOADED' in main table
					and c3r.eod_loading_time = b.eod_loading_time
					AND C3R.record_status = @ForRecordstatus
					AND C3R.created_reference_id = @referenceid
					
				

				UPDATE VMIS 
				SET record_status = @pendingstatus,
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				FROM [c3r_VMIS] VMIS
					WHERE VMIS.[datafor_date_time] = @datafor_date_time 
					AND VMIS.record_status = @ForRecordstatus
					AND VMIS.created_reference_id = @referenceid
					--select record_status,* from [c3r_CMIS] where atm_id = 'DA0032C1'
			
		--	COMMIT TRAN
			--IF (@Debug = 1)
			--	BEGIN
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--	    PRINT '-- Columns has been updated in bank wise file';
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--	END;
		

			--IF (@Debug = 1)
			--	BEGIN
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--	    PRINT '-- Updating columns in data update log ';
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--	END;

------------------------- Updating columns in Data Update Log table------------------------------------------
			--	BEGIN TRAN
				
				UPDATE dbo.data_update_log
				SET is_valid_file =
					CASE	
							WHEN @outputVal = 50009 
							THEN  1
							WHEN @outputVal = 50001
							THEN 0
							WHEN @outputVal = 10001
							THEN 0
					END,
					validation_code = (SELECT @errorcode),
					record_status = 
					CASE	
							WHEN @outputVal = 50009 
							THEN @pendingstatus
							WHEN @outputVal = 50001
							THEN @pendingstatus
							WHEN @outputVal = 10001
							THEN @uploadedstatus
					END,	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				WHERE [datafor_date_time] =  @datafor_date_time
					AND record_status = @ForRecordstatus 
					AND	data_for_type = @datafor
					and created_reference_id = @referenceid
					--select record_status,* from [c3r_CMIS] where atm_id = 'DA0032C1'

			COMMIT TRAN;

			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Transaction Completed....Column has been updated in data update log ';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Completed....Column has been updated in data update log and bank wise file', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Execution of [dbo].[[sp_validate_C3R_CMIS] Completed.';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[usp_validate_C3R_CMIS] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

	
		END
			ELSE
			BEGIN
				  RAISERROR (50002, 16,1);
			END
		END
	ELSE
		BEGIN
		--SELECT SESSION_ID from sys.dm_exec_sessions;  
			RAISERROR (	50004, 16,1);
		END
END



