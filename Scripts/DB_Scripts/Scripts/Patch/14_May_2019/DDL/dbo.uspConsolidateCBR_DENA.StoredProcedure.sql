USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspConsolidateCBR_DENA]    Script Date: 5/14/2019 3:19:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[uspConsolidateCBR_DENA] 
   (@datafor_date_time varchar(100),@projectid varchar(50),@region varchar(50),@referenceid varchar(50),@systemUser varchar(50),@out varchar(200) OUTPUT)
AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;
DECLARE @bankcode nvarchar(10) = 'DENA'
DECLARE @datafor nvarchar(10) = 'CBR'
DECLARE @activestatus nvarchar(20) = 'Active'
DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
DECLARE @approvedstatus nvarchar(20) = 'Approved'

INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		IF EXISTS(
			Select 1 as ColumnName
			FROM [dbo].[data_update_log]
			WHERE datafor_date_time = @datafor_date_time AND 
					bank_code = @bankcode AND 
					data_for_type = @datafor AND 
					record_status = @approvedstatus AND
					project_id = @projectid AND 
					region =@region AND 
					created_reference_id = @referenceid

			) 
		BEGIN			--- Check for approved status in DUL

			IF EXISTS   (						
				SELECT 1
				FROM [dbo].[data_update_log]
				WHERE datafor_date_time = @datafor_date_time AND 
				bank_code = @bankcode AND 
				data_for_type = @datafor AND 
				record_status = @activestatus 
			 
				
				 
			 
				)	
				BEGIN				--- Check Active status in DUL

					--	BEGIN	--- For bank (START)
							IF EXISTS
							(
								Select 1 as ColumnName
								FROM [dbo].[cash_balance_file_dena]
								WHERE datafor_date_time =  @datafor_date_time AND  
										record_status = @activestatus 
									     
							) 
							BEGIN						-----Check active in bankwise file
								IF EXISTS(
										Select 1 as ColumnName
										FROM [dbo].[cash_balance_register]
										WHERE datafor_date_time = @datafor_date_time AND 
												bank_name = @bankcode  AND 
												record_status = @activestatus  
										)
										BEGIN			---- Check Cash Balance Register Table for Active Status (START)	

										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Active record found!! Changing status from Active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
										
										   	UPDATE history
											SET history.record_status = 'Deleted',
												history.deleted_on = @current_datetime_stmp,
												history.deleted_by = @systemUser,
												history.deleted_reference_id = @referenceid
											FROM cash_balance_file_dena history
											JOIN cash_balance_file_dena new
											on new.atm_id = history.atm_id
											AND ISNULL(new.project_id,0) = ISNULL(history.project_id,0)
											AND new.datafor_date_time = @datafor_date_time
											AND history.datafor_date_time = @datafor_date_time
											AND new.record_status = 'Approved'
											AND history.record_status = 'Active'

											 

											 UPDATE cash_balance_file_dena set record_status='Active',
													modified_on=@current_datetime_stmp,
													modified_by=@systemUser , 
													modified_reference_id=@referenceid

														where  datafor_date_time=@datafor_date_time 
														and created_reference_id=@referenceid
														and record_status='Approved'


											--DECLARE @SetWhere_CBR_bankWise1 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
											--		@datafor_date_time   + '''   and  created_reference_id = ''' + @referenceid + ''''
											
											--EXEC dbo.[uspSetStatus] 'dbo.cash_balance_file_dena',@approvedstatus,@activestatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CBR_bankWise1,@out OUTPUT

										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records successfully marked as Deleted. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

											---------inserting data after status update for revision
												INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id,
														error_code
													)
											
													Select CBH.datafor_date_time, 
															@bankcode,
															CBH.atm_id,
															CASE 
																WHEN hop1bill = 100 THEN hop1cash 
																 WHEN  hop2bill = 100  THEN hop2cash
																 WHEN hop3bill = 100 THEN hop3cash 
																 WHEN  hop4bill = 100  THEN hop4cash
																  WHEN  hop5bill = 100  THEN hop5cash
																  ELSE 0 
																  END as cash100,
															CASE 
																WHEN hop1bill = 200 THEN hop1cash 
																 WHEN  hop2bill = 200  THEN hop2cash
																 WHEN hop3bill = 200 THEN hop3cash 
																 WHEN  hop4bill = 200  THEN hop4cash
																  WHEN  hop5bill = 200  THEN hop5cash
																  ELSE 0 
																  END as cash200,
															CASE 
																WHEN hop1bill = 500 THEN hop1cash 
																 WHEN  hop2bill =500  THEN hop2cash
																 WHEN hop3bill = 500 THEN hop3cash 
																 WHEN  hop4bill = 500  THEN hop4cash
																  WHEN  hop5bill = 500  THEN hop5cash
																  ELSE 0
																  END as cash500,
															CASE 
																WHEN hop1bill = 2000 THEN hop1cash 
																 WHEN  hop2bill = 2000  THEN hop2cash
																 WHEN hop3bill = 2000 THEN hop3cash 
																 WHEN  hop4bill = 2000  THEN hop4cash
																  WHEN  hop5bill = 2000  THEN hop5cash
																  ELSE 0
																  END as cash2000,
													        hop1cash+hop2cash+hop3cash+hop4cash+hop5cash
															as closing_bal,
															AM.project_id,
															CBH.region,
															@activestatus,
															CBH.is_valid_record,
															CBH.created_on,
															CBH.created_by,
															CBH.created_reference_id,
															CBH.approved_on,
															CBH.approved_by,
															CBH.approved_reference_id,
															CBH.modified_on,
															CBH.modified_by,
															CBH.modified_reference_id  ,
															CBH.error_code 
														from  
														[dbo].[cash_balance_file_dena] CBH
														left join atm_master AM ON
														CBH.atm_id=AM.atm_id
														and AM.bank_code=@bankcode
														and AM.record_status='Active'
														and AM.site_status='Active'	

														where datafor_date_time= @datafor_date_time             --remove created
														and CBH.record_status = @activestatus
														--and CBH.project_id = @projectid   
														and CBH.created_reference_id = @referenceid

														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)



														UPDATE history
														SET history.record_status = 'Deleted',
															history.deleted_on = @current_datetime_stmp,
															history.deleted_by = @systemUser,
															history.deleted_reference_id = @referenceid
														FROM cash_balance_register history
														JOIN cash_balance_register new
														on new.atm_id = history.atm_id
														AND ISNULL(new.project_id,0) = ISNULL(history.project_id,0)
														AND new.bank_name = @bankcode
														AND history.bank_name = @bankcode
														AND new.datafor_date_time  = @datafor_date_time
														AND history.datafor_date_time = @datafor_date_time
														AND new.created_reference_id <> history.created_reference_id
														AND history.created_reference_id <> @referenceid
														and new.record_status = @activestatus
														AND history.record_status = @activestatus


														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
														values ('Start:updateing dataupdate log to mark active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


														DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' +
													    @datafor + ''' and project_id = ''' + @projectid + ''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
											

														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														IF EXISTS (
																		SELECT 1 from dbo.cash_balance_register where 
																				datafor_date_time = @datafor_date_time and  
																				bank_name = @bankcode and 
																				record_status = @activestatus and
																				created_reference_id=@referenceid
																				-- region=@region  and
																				--project_id = @projectid
																	)
																		BEGIN
																			SET @out = (
																						SELECT Sequence from  [dbo].[app_config_param] where 
																						    category = 'File Operation' 
																						AND sub_category = 'Consolidation Successful'
																						) 
																		END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END

								
								INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure  Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


										END				---- Check Cash Balance Register Table for Active Status (END)

							END
							ELSE		------	BOMH ELSE
							BEGIN
								RAISERROR(50050,16,1)			
										--ELSE			---- Check Cash Balance Register Table ELSE
										--BEGIN 
										--END			

									----- BOMH Active Check Start(END)
							--ELSE		------	BOMH ELSE
							--BEGIN
							--END
				
						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN
					
					
				--DECLARE @SetWhere_CBR_bankWise2 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
				--		@datafor_date_time   + '''  and  created_reference_id = ''' + @referenceid + ''''
				
				--EXEC dbo.[uspSetStatus] 'dbo.cash_balance_file_dena',@approvedstatus,@activestatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CBR_bankWise2,@out OUTPUT
				

				UPDATE cash_balance_file_dena set record_status='Active',
						modified_on=@current_datetime_stmp,
						modified_by=@systemUser , 
						modified_reference_id=@referenceid

							where  datafor_date_time=@datafor_date_time 
							and created_reference_id=@referenceid
							and record_status='Approved'
				
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records have been marked as Active status. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						
					---------inserting data after status update for revision
												INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id,
														error_code
													)
											
													Select CBH.datafor_date_time, 
															@bankcode,
															CBH.atm_id,
															CASE 
																WHEN hop1bill = 100 THEN hop1cash 
																 WHEN  hop2bill = 100  THEN hop2cash
																 WHEN hop3bill = 100 THEN hop3cash 
																 WHEN  hop4bill = 100  THEN hop4cash
																  WHEN  hop5bill = 100  THEN hop5cash
																  ELSE 0 
																  END as cash100,
															CASE 
																WHEN hop1bill = 200 THEN hop1cash 
																 WHEN  hop2bill = 200  THEN hop2cash
																 WHEN hop3bill = 200 THEN hop3cash 
																 WHEN  hop4bill = 200  THEN hop4cash
																  WHEN  hop5bill = 200  THEN hop5cash
																  ELSE 0 
																  END as cash200,
															CASE 
																WHEN hop1bill = 500 THEN hop1cash 
																 WHEN  hop2bill =500  THEN hop2cash
																 WHEN hop3bill = 500 THEN hop3cash 
																 WHEN  hop4bill = 500  THEN hop4cash
																  WHEN  hop5bill = 500  THEN hop5cash
																  ELSE 0
																  END as cash500,
															CASE 
																WHEN hop1bill = 2000 THEN hop1cash 
																 WHEN  hop2bill = 2000  THEN hop2cash
																 WHEN hop3bill = 2000 THEN hop3cash 
																 WHEN  hop4bill = 2000  THEN hop4cash
																  WHEN  hop5bill = 2000  THEN hop5cash
																  ELSE 0
																  END as cash2000,
													        hop1cash+hop2cash+hop3cash+hop4cash+hop5cash
															as closing_bal,
															AM.project_id,
															CBH.region,
															@activestatus,
															CBH.is_valid_record,
															CBH.created_on,
															CBH.created_by,
															CBH.created_reference_id,
															CBH.approved_on,
															CBH.approved_by,
															CBH.approved_reference_id,
															CBH.modified_on,
															CBH.modified_by,
															CBH.modified_reference_id  ,
															CBH.error_code 
														from  
														[dbo].[cash_balance_file_dena] CBH
														left join atm_master AM ON
														CBH.atm_id=AM.atm_id
														and AM.bank_code=@bankcode
														and AM.record_status='Active'
														and AM.site_status='Active'	

														where datafor_date_time= @datafor_date_time             --remove created
														and CBH.record_status = @activestatus
														--and CBH.project_id = @projectid   
														and CBH.created_reference_id = @referenceid



														DECLARE @SetWhere_DataUpdateLog_dul VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor 
																	 + ''' and project_id = ''' + @projectid +
																	  ''' and region= ''' +@region+''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',
																		@approvedstatus,@activestatus,
																		@current_datetime_stmp,@systemUser,
																		@referenceid,@SetWhere_DataUpdateLog_dul,@out OUTPUT
											


											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data Consolidated Successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														IF EXISTS (
																	SELECT 1 
																	from dbo.cash_balance_register 
																	where datafor_date_time = @datafor_date_time 
																	and  bank_name = @bankcode
																	and record_status = @activestatus
																	 
																	)
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END

										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
			BEGIN
				RAISERROR(50010,16,1)
			END
END
