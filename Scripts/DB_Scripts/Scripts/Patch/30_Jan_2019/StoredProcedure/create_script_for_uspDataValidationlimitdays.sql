GO
/****** Object:  StoredProcedure [dbo].[uspDataValidationlimitdays]    Script Date: 30-01-2019 18:08:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspDataValidationlimitdays]
( 
	@api_flag varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar(max)
	DECLARE @atmid nvarchar(max)
	DECLARE @sitecode nvarchar(max)
	-------- Updated Part START-------------------------
	DECLARE @errorcode varchar(max)
	--------Updated Part END-----------------------------

	
	-- Checking for any record that is present in uploaded status in table
	
	IF EXISTS(
			    SELECT 1 AS ColumnName
				FROM [dbo].limitdays
				WHERE record_status = 'Uploaded'
				and created_reference_id =  @referenceid
			)
				
			BEGIN
			-- Proceed if we found any record in uploaded status
				
				--- SELECT total count of records present in   file for particular date and status
				SET @CountTotal = (
									SELECT count(1) FROM limitdays
									WHERE record_status = 'Uploaded'
									and created_reference_id = @referenceid
								  )	   
	
				SET @countCalculated = (
										SELECT count(1) FROM limitdays acl
										INNER JOIN atm_master clm 
										ON acl.atm_id = clm.atm_id
									    and acl.eps_site_code = clm.site_code
										and acl.bank_code = clm.bank_code
										WHERE acl.record_status = 'Uploaded'
										and acl.created_reference_id = @referenceid
										and clm.record_status = 'Active'
										and clm.site_status in ('ACTIVE', 'LIVE')
									)
				--print (@CountTotal)   --5
				--print (@countCalculated)--7
----------------- Updating isValidRecord as Valid or Invalid based on validation ---------------------------
		
-------------- Check if all atm ids and site code are present in the master data -----------------
				UPDATE limitdays
				SET is_valid_record = 
				CASE WHEN atm_id in (
											SELECT acl.atm_id FROM limitdays acl
											INNER JOIN atm_master clm 
											ON acl.atm_id = clm.atm_id
											WHERE acl.record_status = 'Uploaded'
											and acl.created_reference_id = @referenceid
											and clm.record_status = 'Active'
											and clm.site_status in ('ACTIVE', 'LIVE')
											)--5
										AND

							eps_site_code in (
											SELECT acl.eps_site_code FROM limitdays acl
												INNER JOIN atm_master clm 
												ON acl.eps_site_code = clm.site_code
												WHERE acl.record_status = 'Uploaded'
												and acl.created_reference_id = @referenceid
												and clm.record_status = 'Active'
												and clm.site_status in ('ACTIVE', 'LIVE')
									      )
						THEN 'Yes'
							  ELSE
							'No'
					END 
					WHERE record_status = 'Uploaded'

					DECLARE @tableName VARCHAR(30) = 'dbo.limitdays'
					DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
					DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'

					--- If all records found in config limits and atm master table

					IF ((@CountTotal = @countCalculated) OR (@CountTotal <> @countCalculated AND @countCalculated > 0))
					BEGIN
						
						--- Update is_valid_file in data update log as YES

						UPDATE data_update_log_master 
						SET is_valid_file = 
								CASE
									WHEN @CountTotal = @countCalculated
									THEN 'Yes'
									ELSE
										'No'
									END
						WHERE record_status = 'Uploaded'
						AND data_for_type = 'LIMITDAYS'
						AND created_reference_id = @referenceid

						IF EXISTS( SELECT 1 FROM limitdays WHERE record_status = 'Active')
							BEGIN
							
							-- Check for API Flag if it is for File Upload or screen edit
							-- 'F' - File Upload ,  else - Screen Edit
							---Changes
							IF (@api_flag = 'F')
								BEGIN
						-- changes 04-01-2019
								 INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
								 values ('Update in limitdays started for all the fields', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


					 UPDATE a
                            set  
                            a.eps_site_code							=	COALESCE(a.eps_site_code,b.eps_site_code),
                            a.atm_id								=	COALESCE(a.atm_id,b.atm_id),
                            a.project_id							=	COALESCE(a.project_id,b.project_id),
                            a.bank_code								=	COALESCE(a.bank_code,b.bank_code),
                            a.indent_date							=	COALESCE(a.indent_date,b.indent_date),
                            a.next_feasible_date					=	COALESCE(a.next_feasible_date,b.next_feasible_date),
                            a.next_working_date						=	COALESCE(a.next_working_date,b.next_working_date),
                            a.default_decide_limit_days             =	COALESCE(a.default_decide_limit_days,b.default_decide_limit_days),
                            a.decidelimitdays						=	COALESCE(a.decidelimitdays,b.decidelimitdays),
                            a.default_loading_limit_days            =	COALESCE(a.default_loading_limit_days,b.default_loading_limit_days),
                            a.loadinglimitdays						=	COALESCE(a.loadinglimitdays,b.loadinglimitdays),
							a.record_status = 'Approval Pending',
							a.modified_on = @current_datetime_stmp,
							a.modified_by = @systemUser,
							a.modified_reference_id = @referenceid
							from  limitdays a 
							LEFT join limitdays b 
								on
							a.atm_id = b.atm_id
							and a.eps_site_code = b.eps_site_code
							where a.record_status = 'Uploaded' 
							and a.is_valid_record = 'Yes'
							and b.record_status = 'Active'
							and a.created_reference_id = @referenceid


					     INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
						 values ('Update in limitdays completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

						 END
									ELSE
									BEGIN
										-- Screen Edit Scenario --
										-- For screen edit we have to directly update the record status from uploaded to approval pending

										DECLARE @SetWhereClause VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''' and is_valid_record = ''Yes'''

										EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
										

									END
							END
							ELSE 
								BEGIN

									-- If there are no records in active status then directly update record status to approval pending 
										
										DECLARE @SetWhereClause2 VARCHAR(MAX) =' created_reference_id = '''+@referenceid+''' and is_valid_record = ''Yes'''

										EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause2,@out OUTPUT
		
									END
									
									UPDATE data_update_log_master 
										SET pending_count =	
												(
													SELECT COUNT(*) FROM dbo.limitdays WHERE created_reference_id = @referenceid
													and record_status = 'Approval Pending'
													),
											total_count = 
											(
												SELECT COUNT(*) FROM dbo.limitdays WHERE created_reference_id = @referenceid
													
											)
										WHERE record_status = 'Uploaded'
										AND data_for_type = 'LIMITDAYS' 
										AND created_reference_id = @referenceid
									
									SET @outputVal = 'S101'
										
							END

				ELSE 
					BEGIN
	
	-----------------------Updated Part START-----------------------------------------
						UPDATE data_update_log_master 
						SET is_valid_file = 'No'
						WHERE record_status = 'Uploaded'
						AND data_for_type = 'LIMITDAYS' 
						AND created_reference_id = @referenceid
						
						RAISERROR(90001,16,1)

					END

				END
						
	ELSE
						
	BEGIN
		--SELECT 'In rasierror 90002'
		RAISERROR(90002,16,1)
	END
						
			END