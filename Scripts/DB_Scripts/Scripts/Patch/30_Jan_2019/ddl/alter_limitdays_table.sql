/****** Object:  Table [dbo].[limitdays]    Script Date: 30-01-2019 17:45:30 ******/

CREATE TABLE [dbo].[limitdays_tmp](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[eps_site_code] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[indent_date] [datetime] NULL,
	[next_feasible_date] [datetime] NULL,
	[next_working_date] [datetime] NULL,
	[default_decide_limit_days] [float] NULL,
	[decidelimitdays] [float] NULL,
	[default_loading_limit_days] [float] NULL,
	[loadinglimitdays] [float] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](50) NULL,
	[error_code] [nvarchar](50) NULL
	
	)

SET IDENTITY_INSERT dbo.limitdays_tmp  ON

INSERT INTO dbo.limitdays_tmp (
	id,
	eps_site_code,
	atm_id,
	project_id,
	bank_code,
	indent_date,
	next_feasible_date,
	next_working_date,
	default_decide_limit_days,
	decidelimitdays,
	default_loading_limit_days,
	loadinglimitdays,
	record_status,
	created_on,
	created_by,
	created_reference_id,
	approved_on,
	approved_by,
	approved_reference_id,
	modified_on,
	modified_by,
	modified_reference_id,
	deleted_on,
	deleted_by,
	deleted_reference_id
)

select 
	id,
	eps_site_code,
	atm_id,
	project_id,
	bank_code,
	indent_date,
	next_feasible_date,
	next_working_date,
	default_decide_limit_days,
	decidelimitdays,
	default_loading_limit_days,
	loadinglimitdays,
	record_status,
	created_on,
	created_by,
	created_reference_id,
	approved_on,
	approved_by,
	approved_reference_id,
	modified_on,
	modified_by,
	modified_reference_id,
	deleted_on,
	deleted_by,
	deleted_reference_id
	from limitdays
	
SET IDENTITY_INSERT dbo.limitdays_tmp  OFF

EXEC sp_rename 'limitdays', 'limitdays_tmp_bckp_30012019_1802'

EXEC sp_rename 'limitdays_tmp', 'limitdays'

drop table limitdays_tmp_bckp_30012019_1802;