/****** Object:  StoredProcedure [dbo].[uspDataValidationfdr_casstt_max_capacity_percentage]    Script Date: 02-04-2019 20:02:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspDataValidationfdr_casstt_max_capacity_percentage]
( 
	@api_flag    VARCHAR(50),
	@systemUser  VARCHAR(50),
	@referenceid VARCHAR(50),
	@outputVal   VARCHAR(50) OUTPUT
)
AS
BEGIN
	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE());
	DECLARE @ColumnName varchar(255)
	DECLARE @out varchar(50)
	DECLARE @sql nvarchar       (max)
	DECLARE @bank_code nvarchar (max)
	DECLARE @project_id nvarchar(max)
	
	--DECLARE @tableName VARCHAR(50) = 'dbo.feeder_denomination_priority'
 --   DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
 --   DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'

	----DECLARE @referenceid varchar(10) = 'ssdfdsg'
 --   DECLARE @SetWhereClause VARCHAR(MAX) = '  created_reference_id = ''' + 
 --                                               @referenceid + ''''

	----SELECT @SetWhereClause
	--DECLARE @tableName_DUL VARCHAR(30) = 'dbo.data_update_log_master'



	--INSERT INTO dbo.execution_log values ('Execution of [dbo].[uspDataValidationFeederDenomination] Started', DATEADD(MI,330,GETUTCDATE()))

------------------ Check if file is present in Data Update Log---------------
DECLARE @count_rows_feeder_csstt_maxPer int = (select count(1) from [dbo].[feeder_cassette_max_capacity_percentage] where created_reference_id = @referenceid and 
					  record_status = 'Uploaded')

	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[feeder_cassette_max_capacity_percentage] WITH (NOLOCK)
				WHERE created_reference_id = @referenceid and 
					  record_status = 'Uploaded'
				)
	BEGIN
 
		--select *, 	
		--isnull(case when denomination_100 not between 0 and 4 then 'ERROR_100,' end,'') +
		--isnull(case when denomination_200 not between 0 and 4 then 'ERROR_200,' end,'') +
		--isnull(case when denomination_500 not between 0 and 4 then 'ERROR_500,' end,'') +
		--isnull(case when denomination_2000 not between 0 and 4 then 'ERROR_2000,' end,'') 
		--as denomination_error
		-- from feeder_denomination_priority  
		-- where  created_reference_id = @referenceid and 
		--				  record_status = 'Uploaded'


		update feeder_cassette_max_capacity_percentage
		set error_code =	isnull(case when denomination_100 not between 0 and 100 then 'ERROR_100,' end,'') +
		isnull(case when denomination_200 not between 0 and 100 then 'ERROR_200,' end,'') +
		isnull(case when denomination_500 not between 0 and 100 then 'ERROR_500,' end,'') +
		isnull(case when denomination_2000 not between 0 and 100 then 'ERROR_2000,' end,'')+
		isnull(case when project_id  not in ( select DISTINCT cf.project_id from cra_feasibility cf where cf.record_status = 'Active' )
				or bank_code not in  (select DISTINCT cf.bank_code from cra_feasibility cf where cf.record_status = 'Active')	
				or feeder_branch_code not in (select DISTINCT cf.feeder_branch_code from cra_feasibility cf Where cf.record_status = 'Active' )
				THEN 'record_not_present,' end, '')
		 where  created_reference_id = @referenceid and 
						  record_status = 'Uploaded'

		update feeder_cassette_max_capacity_percentage
		set is_valid_record = case when error_code = '' then 'Yes' else 'No' end
		where record_status = 'Uploaded' and created_reference_id = @referenceid
		DECLARE @count_valid_records int = (select count(1) from [dbo].[feeder_cassette_max_capacity_percentage] where created_reference_id = @referenceid and 
					  record_status = 'Uploaded' and is_valid_record = 'Yes')

		if (@count_rows_feeder_csstt_maxPer = @count_valid_records)
			BEGIN
				SET @outputVal = (	select sequence from [dbo].[app_config_param]
									where category = 'File Operation' and sub_category = 'Data Validation Successful')
				if (@outputVal = '50009')
					BEGIN
						SET @outputVal = 'S101' 
					END
				
				update data_update_log_master set record_status = 'Approval Pending' where created_reference_id = @referenceid
    
			END
		else if (@count_rows_feeder_csstt_maxPer != @count_valid_records and @count_valid_records > 0)
			BEGIN
				SET @outputVal = (	select sequence from [dbo].[app_config_param]
									where category = 'Exception' and sub_category = 'Partial Valid'
				)
				if (@outputVal = '50001')
					BEGIN
						SET @outputVal = 'S101' 
					END
				update data_update_log_master set record_status = 'Approval Pending' where created_reference_id = @referenceid
			END
		else 
			BEGIN
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
								)
			END

			UPDATE feeder_cassette_max_capacity_percentage
				SET record_status = 
						CASE 
							WHEN is_valid_record = 'Yes' 
							THEN 'Approval Pending'
							WHEN is_valid_record = 'No' 
							THEN 'Uploaded'
						END
			WHERE created_reference_id = @referenceid
					


				--EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT
    --            EXEC  dbo.[uspSetStatus] @tableName_DUL,@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT

	--SELECT @outputVal

	END
END