 
/****** Object:  StoredProcedure [dbo].[uspConsolidateCBR_BOMH]    Script Date: 07-02-2019 16:55:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


 --chg projectid parametere

ALTER PROCEDURE [dbo].[uspConsolidateCBR_BOMH] 

   ( @datafor_date_time varchar(100),@projectid varchar(50),@region varchar(50),@referenceid varchar(50),@systemUser varchar(50),@out varchar(200) OUTPUT)
AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;
DECLARE @bankcode nvarchar(10) = 'BOMH'
DECLARE @datafor nvarchar(10) = 'CBR'
DECLARE @activestatus nvarchar(20) = 'Active'
DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
DECLARE @approvedstatus nvarchar(20) = 'Approved'


	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		IF EXISTS(
			Select 1 as ColumnName
			FROM [dbo].[data_update_log]
			WHERE datafor_date_time = @datafor_date_time AND 
					bank_code = @bankcode AND 
					data_for_type = @datafor AND 
					record_status = @approvedstatus AND
					project_id = @projectid AND 
					region=@region AND
					created_reference_id = @referenceid
			) 
		BEGIN			--- Check for approved status in DUL

			IF EXISTS   (						
				SELECT 1
				FROM [dbo].[data_update_log]
				WHERE datafor_date_time = @datafor_date_time AND 
				bank_code = @bankcode AND 
					data_for_type = @datafor AND 
					record_status = @activestatus  
					--project_id = @projectid --chg
				)	
				BEGIN				--- Check Active status in DUL

					--	BEGIN	--- For bank (START)
							IF EXISTS
							(
								Select 1 as ColumnName
								FROM [dbo].[cash_balance_file_bomh]
								WHERE datafor_date_time =  @datafor_date_time AND    --remove created on
										record_status = @activestatus  
										--project_id = @projectid                        --chg
							) 
							BEGIN						-----Check active in bankwise file
								IF EXISTS(
										Select 1 as ColumnName
										FROM [dbo].[cash_balance_register]
										WHERE datafor_date_time = @datafor_date_time AND 
												bank_name = @bankcode  AND 
												record_status = @activestatus  
												--project_id = @projectid  --chg
										)
										BEGIN			---- Check Cash Balance Register Table for Active Status (START)	
										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Active record found!! Changing status from Active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
											
											UPDATE history
											SET history.record_status = 'Deleted',
												history.deleted_on = @current_datetime_stmp,
												history.deleted_by = @systemUser,
												history.deleted_reference_id = @referenceid
											FROM cash_balance_file_bomh history
											JOIN cash_balance_file_bomh new
											on new.term_id = history.term_id
											AND new.project_id = history.project_id
											AND new.datafor_date_time = history.datafor_date_time
											AND new.record_status = 'Approved'
											AND history.record_status = 'Active'
											
											UPDATE cash_balance_file_bomh set record_status='Active',
													modified_on=@current_datetime_stmp,
													modified_by=@systemUser , 
													modified_reference_id=@referenceid

														where  datafor_date_time=@datafor_date_time 
														and created_reference_id=@referenceid
														and record_status='Approved'
										 
											 
											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records successfully marked as Deleted. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

											---------inserting data after status update for revision
												INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,        --chg
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id,
														error_code
													)
											
													Select datafor_date_time, 
															@bankcode,
															term_id,
															CASE 
																WHEN LEFT(term_id,1) like 'D%'
																THEN cassette1
																ELSE
																	 cassette2
																END as Deno100,
															CASE 
																WHEN LEFT(term_id,1) like 'D%'
																THEN cassette4
																ELSE
																	 cassette1
																END as Deno200,
															CASE 
																WHEN LEFT(term_id,1) like 'D%'
																THEN cassette2
																ELSE
																	 cassette3
																END as Deno500,
															CASE 
																WHEN LEFT(term_id,1) like 'D%'
																THEN cassette3
																ELSE
																	 cassette4
																END as Deno2000,
															tot_cash as closing_bal,
															AM.project_id,          --chg
															region,
															@activestatus,
															CBH.is_valid_record,
															CBH.created_on,
															CBH.created_by,
															CBH.created_reference_id,
															CBH.approved_on,
															CBH.approved_by,
															CBH.approved_reference_id,
															CBH.modified_on,
															CBH.modified_by,
															CBH.modified_reference_id  ,
															CBH.error_code 
														from  
														[dbo].[cash_balance_file_bomh] CBH
														left join atm_master AM ON
														CBH.term_id=AM.atm_id
														and AM.bank_code=@bankcode
														and AM.record_status='Active'
														and AM.site_status='Active'

														where datafor_date_time= @datafor_date_time             --remove created
														and CBH.record_status = @activestatus
														--and CBH.project_id = @projectid   
														and CBH.created_reference_id = @referenceid
														
															INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)	
														
														
														UPDATE history
														SET history.record_status = 'Deleted',
															history.deleted_on = @current_datetime_stmp,
															history.deleted_by = @systemUser,
															history.deleted_reference_id = @referenceid
														FROM cash_balance_register history
														JOIN cash_balance_register new
														on new.atm_id = history.atm_id
														AND new.project_id = history.project_id
														AND new.bank_name = @bankcode
														AND history.bank_name = @bankcode
														AND new.datafor_date_time  = @datafor_date_time
														AND new.created_reference_id <> history.created_reference_id
														AND history.created_reference_id <> @referenceid
														and new.record_status = @activestatus
														AND history.record_status = @activestatus

														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
														values ('Start:updateing dataupdate log to mark active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


														DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor + ''' and project_id = ''' + @projectid + ''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
											
															--Set status in bank wise table as Approved
											
														---------inserting data after status update for revision
																							
										
														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														IF EXISTS (
																		SELECT 1 from dbo.cash_balance_register where 
																				datafor_date_time = @datafor_date_time and  
																				bank_name = @bankcode and 
																				record_status = @activestatus  and 
																				created_reference_id=@referenceid
																				--project_id = @projectid     --chg
																				--and region =@region
																	)
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'  --chg
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END

								INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
								


										END				---- Check Cash Balance Register Table for Active Status (END)

								END
							ELSE		------	BOMH ELSE
							BEGIN
								RAISERROR(50050,16,1)	
										--ELSE			---- Check Cash Balance Register Table ELSE
										--BEGIN 
										--END			

									----- BOMH Active Check Start(END)
							--ELSE		------	BOMH ELSE
							--BEGIN
							--END
				
						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN
					
				UPDATE cash_balance_file_bomh set record_status='Active',
						modified_on=@current_datetime_stmp,
						modified_by=@systemUser , 
						modified_reference_id=@referenceid

							where  datafor_date_time=@datafor_date_time 
							and created_reference_id=@referenceid
							and record_status='Approved'

					---------inserting data after status update for revision  --like above insert
											INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,        --chg
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id,
														error_code
													)
											
													Select datafor_date_time, 
															@bankcode,
															term_id,
															CASE 
																WHEN LEFT(term_id,1) like 'D%'
																THEN cassette1
																ELSE
																	 cassette2
																END as Deno100,
															CASE 
																WHEN LEFT(term_id,1) like 'D%'
																THEN cassette4
																ELSE
																	 cassette1
																END as Deno200,
															CASE 
																WHEN LEFT(term_id,1) like 'D%'
																THEN cassette2
																ELSE
																	 cassette3
																END as Deno500,
															CASE 
																WHEN LEFT(term_id,1) like 'D%'
																THEN cassette3
																ELSE
																	 cassette4
																END as Deno2000,
															tot_cash as closing_bal,
															AM.project_id,          --chg
															region,
															@activestatus,
															CBH.is_valid_record,
															CBH.created_on,
															CBH.created_by,
															CBH.created_reference_id,
															CBH.approved_on,
															CBH.approved_by,
															CBH.approved_reference_id,
															CBH.modified_on,
															CBH.modified_by,
															CBH.modified_reference_id  ,
															CBH.error_code 
														from  
														[dbo].[cash_balance_file_bomh] CBH
														left join atm_master AM ON
														CBH.term_id=AM.atm_id
														and AM.bank_code=@bankcode
														and AM.record_status='Active'
														and AM.site_status='Active'

														where datafor_date_time= @datafor_date_time             --remove created
														and CBH.record_status = @activestatus
														and CBH.project_id = @projectid   
														and CBH.created_reference_id = @referenceid



														DECLARE @SetWhere_DataUpdateLog_dul VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor 
																	 + ''' and project_id = ''' + @projectid +
																	  ''' and region= ''' +@region+''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',
																		@approvedstatus,@activestatus,
																		@current_datetime_stmp,@systemUser,
																		@referenceid,@SetWhere_DataUpdateLog_dul,@out OUTPUT
											

														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data Consolidated Successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														IF EXISTS (SELECT 1 from dbo.cash_balance_register 
														where datafor_date_time = @datafor_date_time and
																		  bank_name = 'BOMH' 
																		  and record_status = 'Active'
																		  --and created_reference_id=@referenceid
														                 --and project_id = @projectid 
																		 --and region=@region
														   )
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' 
																		AND sub_category = 'Consolidation Successful'      --chg
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END
											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
			BEGIN
				RAISERROR(50010,16,1)
			END
END
GO


