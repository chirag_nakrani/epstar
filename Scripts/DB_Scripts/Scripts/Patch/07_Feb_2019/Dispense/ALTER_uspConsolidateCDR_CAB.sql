 
/****** Object:  StoredProcedure [dbo].[uspConsolidateCDR_CAB]    Script Date: 07-02-2019 18:40:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

-- consolidation of Dispense 
ALTER PROCEDURE [dbo].[uspConsolidateCDR_CAB] 
   (@datafor_date_time varchar(100),
    @projectid varchar(50),
	@region varchar(50),
   @referenceid varchar(50),
   @systemUser varchar(50),
   @out varchar(200) OUTPUT)  -- added parameter @out

AS 
BEGIN
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
    values ('Execution of [dbo].[uspConsolidateCDR_CAB] Started. param @datafor_date_time = ' + @datafor_date_time  , DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

	declare @datafor_date  nvarchar(50) = cast(cast(@datafor_date_time as date) as nvarchar(50))
 

	DECLARE @recordStatus varchar(50);
	DECLARE @successmsg varchar(max);
	DECLARE @errorsmsg varchar(max);
	DECLARE @count1 int;
	DECLARE @errorNumber int;
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @rowcount int ;
	DECLARE @bankcode nvarchar(10) = 'CAB'
	DECLARE @datafor nvarchar(10) = 'Dispense'
	DECLARE @activestatus nvarchar(20) = 'Active'
	DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
	DECLARE @approvedstatus nvarchar(20) = 'Approved'
	--DECLARE @out varchar(200);

		IF EXISTS(
			Select 1 as ColumnName
			FROM data_update_log
			WHERE cast(datafor_date_time as date) = @datafor_date AND 
					bank_code = @bankcode AND 
					data_for_type = @datafor and 
					record_status = @approvedstatus and
					project_id = @projectid  
					AND created_reference_id = @referenceid
			       ) 
		BEGIN			--- Check for approved status in DUL
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('Found Record Status = Approved in data_update_log', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			IF EXISTS   (						
						SELECT 1
						FROM data_update_log
						WHERE cast(datafor_date_time as date) = @datafor_date AND 
						bank_code =     @bankcode AND 
						data_for_type = @datafor and 
						record_status = @activestatus and
					    project_id = @projectid          --chg
            )	
            BEGIN				--- Check Active status in DUL
                --Add
                -- update status of Data Update Log  for that bank /date 

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('Found Record Status = Active in data_update_log. Update from active to Deleted in data_update_log', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

                DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
                            @bankcode + ''' and cast(datafor_date_time as date) = ''' + 
                            @datafor_date  + ''' and data_for_type = ''' + 
							@datafor + ''' and project_id = ''' + 
							@projectid + -- ''' and region= ''' +@region+
                            ''''


                EXEC dbo.[uspSetStatus] 'dbo.data_update_log',
				         @activestatus,@deletedstatus,
						 @current_datetime_stmp,@systemUser,
						 @referenceid,@SetWhere_DataUpdateLog,
						 @out OUTPUT
            
            END
			
				--If found in cash_dispense_file_bomh, mark as deleted
            IF EXISTS
            (
                Select 1 as ColumnName
                FROM [cash_dispense_file_CAB]
                WHERE cast(datafor_date_time as date) = @datafor_date AND  
                             record_status = @activestatus 
                             --AND region=@region 
                             --and project_id = @projectid          --chg
            ) 
            BEGIN		
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('Found Record Status = Active in [cash_dispense_file_CAB]. Update from active to Deleted in [cash_dispense_file_bomh]', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
                
				-- Set status of bankwise dispense table  for that date
                DECLARE @SetWhere_CDR_bankWise VARCHAR(MAX) =    ' cast(datafor_date_time as date) = ''' +       --chgevery projectid
                        @datafor_date  + --''' and project_id = ''' + @projectid + 
                        --''' and region= ''' +@region +
                        ''''

                EXEC dbo.[uspSetStatus] 'dbo.cash_dispense_file_CAB',
					@activestatus,@deletedstatus,
					@current_datetime_stmp,
                    @systemUser,@referenceid,
					@SetWhere_CDR_bankWise,@out OUTPUT
            END

			--ELSE		------	BOMH ELSE
			--BEGIN
			--	RAISERROR(50050,16,1)				
			--END
            -----Check active in cash_dispense_register


            IF EXISTS(
                    Select 1 as ColumnName
                    FROM [cash_dispense_register]
                    WHERE
                     cast(datafor_date_time as date) =@datafor_date AND 
                        bank_name = @bankcode  AND 
                            record_status = @activestatus
                                                
                              --region=@region AND
                              -- project_id = @projectid          --chg
            )
            BEGIN			---- Check Cash Balance Register Table for Active Status (START)
                -- update status in  consolidated dispense table  cash_dispense_resgister

				    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				    values ('Found Record Status = Active in [cash_dispense_register]. Update from active to Deleted in [cash_dispense_register]', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
				    
                    DECLARE @SetWhere_CDR VARCHAR(MAX) = ' bank_name = ''' + 
                            @bankcode +
                             ''' and cast(datafor_date_time as date) = ''' + 
                            @datafor_date  +
                            --''' and project_id = ''' + @projectid + 
                            --''' and region= ''' +@region+
                            ''''

                   EXEC dbo.[uspSetStatus] 'dbo.cash_dispense_register',
											@activestatus,@deletedstatus,
											@current_datetime_stmp,
											@systemUser,@referenceid,
											@SetWhere_CDR,
											@out OUTPUT
            END
										 
							
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('Now, Set Record Status = Active in [cash_dispense_file_CAB]. Update from Approved to Active in [cash_dispense_file_bomh]', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
								
            -- Set status in bank wise table as Approved
            DECLARE @SetWhere_CDR_bankWise1 VARCHAR(MAX) =' cast(datafor_date_time as date) = ''' +									------ Changing status from approved to active
                    @datafor_date   +    -- ''' and project_id = ''' + @projectid + 
					''' and  created_reference_id = ''' + @referenceid + 
                                         --''' and region= ''' +@region+
                    ''''
            
            EXEC dbo.[uspSetStatus] 'dbo.cash_dispense_file_CAB',@approvedstatus,@activestatus,
                                       @current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CDR_bankWise1,@out OUTPUT

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('Now, insert into [cash_dispense_register].', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
            ---------inserting data after status update for revision
											
											INSERT INTO 
													cash_dispense_register ( 
														datafor_date_time,
														[atm_id] , 
														bank_name,
														[total_dispense_amount] ,
														record_status,
														project_id, 
														region,       --chg
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
															modified_on,
															modified_by,
															modified_reference_id,
															is_valid_record,
															error_code
													)
											
													Select 
											           datafor_date_time ,
													CBH.atm_id,
													@bankcode,	
													CBH.amount_dispensed ,
													@activestatus,
													AM.project_id, 
													CBH.region,
													CBH.created_on,
													CBH.created_by,
													CBH.created_reference_id,
													CBH.approved_on,
													CBH.approved_by,
													CBH.approved_reference_id,
													CBH.modified_on,
													CBH.modified_by,
													CBH.modified_reference_id  
													,CBH.is_valid_record,
													CBH.error_code
											    from  
												cash_dispense_file_cab CBH
												left join atm_master AM
												on CBH.atm_id=AM.atm_id
												and AM.bank_code=@bankcode
												and AM.site_status='Active'
												and AM.record_status='Active'

												where datafor_date_time= @datafor_date_time 
												and CBH.record_status = @activestatus
												--and project_id = @projectid  
												and CBH.created_reference_id = @referenceid     

                /*******Check for successful execution of inserting records*******/
                IF EXISTS (
                                 SELECT 1 from dbo.cash_dispense_register 
                                    where	cast(datafor_date_time as date) = @datafor_date 
                                            and bank_name = @bankcode
                                            --and region=@region 
                                            and record_status = @activestatus
                                              
                            )
                BEGIN
                    SET @out = (
                                SELECT Sequence from  [dbo].[app_config_param] where 
                                category = 'File Operation'
								 AND sub_category = 'Consolidation Successful'  --chg
                                ) 
                END
                ELSE
                BEGIN
                    RAISERROR (50003,16,1)
                END
 		END					--- Approve END
		ELSE				---- Approve ELSE
			RAISERROR(50010,16,1)
END
