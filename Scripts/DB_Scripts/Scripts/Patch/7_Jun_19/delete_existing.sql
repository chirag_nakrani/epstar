DECLARE @tablename varchar(100)
declare @contraint varchar(100)-- Here we create a variable that will contain the ID of each row.
declare @cmd varchar(max)
DECLARE ITEM_CURSOR CURSOR  -- Here we prepare the cursor and give the select statement to iterate through
FOR
select so.name, si.name from sys.indexes si
inner join sys.objects  so
on si.object_id = so.object_id
where so.name in ('cash_balance_register',
'c3r_CMIS',
'c3r_VMIS',
'cash_balance_register',
'action_reference_master_log',
'applied_vaulting',
'ams_api_token_data',
'app_config_param',
'cash_balance_file_alb',
'cash_balance_file_bob',
'cash_balance_file_boi',
'cash_balance_file_bomh',
'cash_balance_file_cab',
'cash_balance_file_cbi',
'cash_balance_file_corp',
'cash_balance_file_dena',
'cash_balance_file_idbi',
'cash_balance_file_iob',
'cash_balance_file_lvb',
'cash_balance_file_psb',
'cash_balance_file_rsbl',
'cash_balance_file_sbi',
'cash_balance_file_ubi',
'cash_balance_file_uco',
'cash_balance_file_vjb',
'cash_dispense_file_bomh',
'cash_dispense_file_cab',
'cash_dispense_file_rsbl',
'cash_dispense_file_sbi',
'cash_dispense_file_ubi',
'cash_dispense_register',
'cash_balance_file_sbi_staging',
'cash_dispense_file_sbi',
'dispense_amount_calc',
 'cash_dispense_register')

 
OPEN ITEM_CURSOR 
 
FETCH NEXT FROM ITEM_CURSOR INTO @tablename, @contraint
 
WHILE @@FETCH_STATUS = 0 
BEGIN
 

if (@contraint like 'PK%')
begin
	set @cmd = 'ALTER TABLE ' + @tablename +  ' DROP CONSTRAINT ' + @contraint + ';'
	print @cmd
end
else
begin
	set @cmd = 'drop index ' + @tablename+'.'+ @contraint+';'
	print @cmd
end


 
FETCH NEXT FROM ITEM_CURSOR INTO @tablename, @contraint
 
END

CLOSE ITEM_CURSOR  
DEALLOCATE ITEM_CURSOR 