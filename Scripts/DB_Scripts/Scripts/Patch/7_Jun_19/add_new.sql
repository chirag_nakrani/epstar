
---for ams_api_token_data
CREATE CLUSTERED INDEX [CLIX_ams_api_token_data_datafordate_recordstatus] ON [dbo].[ams_api_token_data]
(
	[datafor_date_time] ASC,
	[record_status] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)



--for CLIX_c3r_CMIS
CREATE CLUSTERED INDEX [CLIX_c3r_CMIS_dataForDate_recordstatus_cra_feeder] ON [dbo].[c3r_CMIS]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[cra_name] ASC,
	[feeder_branch] ASC,
	[is_valid_record] 
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


--for CLIX_c3r_VMIS
CREATE CLUSTERED INDEX [CLIX_c3r_VMIS_datafor_recordstatus_bank_cra_feeder] ON [dbo].[c3r_VMIS]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[bank] ASC,
	[cra_name] ASC,
	[feeder_branch_name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


-----------for cash_balance_register
CREATE CLUSTERED INDEX [CLIX_cash_balance_register_datafor_bank_rstatus_proj] ON [dbo].[cash_balance_register]
(
	[datafor_date_time] DESC,
	[bank_name] ASC,
	[record_status] ASC,
	[project_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


CREATE CLUSTERED INDEX [CLIX_cash_balance_file_alb_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_alb]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


CREATE CLUSTERED INDEX [CLIX_cash_balance_file_bob_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_bob]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


CREATE CLUSTERED INDEX [CLIX_cash_balance_file_boi_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_boi]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


CREATE CLUSTERED INDEX [CLIX_cash_balance_file_bomh_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_bomh]
(
	
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_balance_file_cab_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_cab]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_balance_file_cbi_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_cbi]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO




CREATE CLUSTERED INDEX [CLIX_cash_balance_file_bomh_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_corp]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_balance_file_dena_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_dena]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_balance_file_idbi_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_idbi]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO




CREATE CLUSTERED INDEX [CLIX_cash_balance_file_iob_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_iob]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_balance_file_lvb_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_lvb]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO




CREATE CLUSTERED INDEX [CLIX_cash_balance_file_psb_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_psb]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO




CREATE CLUSTERED INDEX [CLIX_cash_balance_file_rsbl_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_rsbl]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO




CREATE CLUSTERED INDEX [CLIX_cash_balance_file_sbi_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_sbi]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_balance_file_ubi_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_ubi]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_balance_file_uco_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_uco]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_balance_file_vjb_datafor_record_isvalid_errorcode] ON [dbo].[cash_balance_file_vjb]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC,
	[error_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_dispense_file_bomh_datafor_recordstatus_isvalid_proj] ON [dbo].[cash_dispense_file_bomh]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


CREATE CLUSTERED INDEX [CLIX_cash_dispense_file_cab_datafor_recordstatus_isvalid_proj] ON [dbo].[cash_dispense_file_cab]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


CREATE CLUSTERED INDEX [CLIX_cash_dispense_file_rsbl_datafor_recordstatus_isvalid_proj] ON [dbo].[cash_dispense_file_rsbl]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


CREATE CLUSTERED INDEX [CLIX_cash_dispense_file_sbi_datafor_recordstatus_isvalid_proj] ON [dbo].[cash_dispense_file_sbi]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


CREATE CLUSTERED INDEX [CLIX_cash_dispense_file_ubi_datafor_recordstatus_isvalid_proj] ON [dbo].[cash_dispense_file_ubi]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



CREATE CLUSTERED INDEX [CLIX_cash_dispense_register_datafor_record_bank_proj_damt] ON [dbo].[cash_dispense_register]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[bank_name] ASC,
	[total_dispense_amount] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



--for app_config_param
CREATE UNIQUE CLUSTERED INDEX [CLIX_app_config_param_sequence_value] ON [dbo].[app_config_param]
(
	[sequence] ASC,
	[value] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


CREATE UNIQUE NONCLUSTERED INDEX [NCIX_app_config_param_cat_subcat] ON [dbo].[app_config_param]
(
	[category] ASC,
	[sub_category] ASC
)



--for applied_vaulting
CREATE NONCLUSTERED INDEX [NCIX_Applied_vaulting_fordate_proj_bank_site_record] ON [dbo].[applied_vaulting]
(
	[for_date] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[bank_code] ASC,
	[site_code] ASC
)
INCLUDE ( 	[applied_vaulting_percentage]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


CREATE CLUSTERED INDEX [CLIX_applied_vaulting_fordate_proj_bank_record] ON [dbo].[applied_vaulting]
(
	[for_date] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[bank_code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


--for dispense_amount_calc
CREATE CLUSTERED INDEX [CLIX_dispense_amount_calc_datafor_proj_bank_site_record] ON [dbo].[dispense_amount_calc]
(
	[datafor_date] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[bank_code] ASC,
	[site_code] ASC
	
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO


--remove keys from error_log and execution_log

--for cash_balance_file_sbi_staging
CREATE CLUSTERED INDEX [CLIX_cash_balance_file_sbi_staging_datafor_proj_record_isvalid] ON [dbo].[cash_balance_file_sbi_staging]
(
	[datafor_date_time] ASC,
	[record_status] ASC,
	[project_id] ASC,
	[is_valid_record] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

GO



