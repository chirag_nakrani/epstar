  
CREATE TABLE [dbo].[cash_balance_file_uco_tmp]
   (
   [id] [bigint] IDENTITY(1,1) PRIMARY KEY , 
	 
	[atm_id] [nvarchar](15) NULL,
	[cassete1_start] [int] NULL,
	[cassete1_dispensed] [int] NULL,
	[cassete1_remaining] [int] NULL,
	[cassete2_start] [int] NULL,
	[cassete2_dispensed] [int] NULL,
	[cassete2_remaining] [int] NULL,
	[cassete3_start] [int] NULL,
	[cassete3_dispensed] [int] NULL,
	[cassete3_remaining] [int] NULL,
	[cassete4_start] [int] NULL,
	[cassete4_dispensed] [int] NULL,
	[cassete4_remaining] [int] NULL,
	[total_start] [int] NULL,
	[toatal_dispensed] [int] NULL,
	[total_remaining] [int] NULL,
	[vendor_name] [nvarchar](30) NULL,
	[region] [nvarchar](50) NULL,
	[mtloc] [nvarchar](255) NULL,
	[mtadr] [nvarchar](255) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_uco_tmp  ON


INSERT INTO dbo.cash_balance_file_uco_tmp (
	[id]
      ,[atm_id]
      ,[cassete1_start]
      ,[cassete1_dispensed]
      ,[cassete1_remaining]
      ,[cassete2_start]
      ,[cassete2_dispensed]
      ,[cassete2_remaining]
      ,[cassete3_start]
      ,[cassete3_dispensed]
      ,[cassete3_remaining]
      ,[cassete4_start]
      ,[cassete4_dispensed]
      ,[cassete4_remaining]
      ,[total_start]
      ,[toatal_dispensed]
      ,[total_remaining]
      ,[vendor_name]
      ,[region]
      ,[mtloc]
      ,[mtadr]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
  
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	)

SELECT 
 [id]
      ,[atm_id]
      ,[cassete1_start]
      ,[cassete1_dispensed]
      ,[cassete1_remaining]
      ,[cassete2_start]
      ,[cassete2_dispensed]
      ,[cassete2_remaining]
      ,[cassete3_start]
      ,[cassete3_dispensed]
      ,[cassete3_remaining]
      ,[cassete4_start]
      ,[cassete4_dispensed]
      ,[cassete4_remaining]
      ,[total_start]
      ,[toatal_dispensed]
      ,[total_remaining]
      ,[vendor_name]
      ,[region_file]
      ,[mtloc]
      ,[mtadr]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
      
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_uco;


SET IDENTITY_INSERT dbo.cash_balance_file_uco_tmp  OFF

EXEC sp_rename 'cash_balance_file_uco', 'cash_balance_file_uco_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_uco_tmp', 'cash_balance_file_uco'

drop table cash_balance_file_uco_tmp_bckp_21012019_1925;
