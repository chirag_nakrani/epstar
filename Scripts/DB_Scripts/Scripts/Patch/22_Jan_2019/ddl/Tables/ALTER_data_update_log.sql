  
CREATE TABLE [dbo].[data_update_log_tmp]
   (
    [id] [bigint] IDENTITY(1,1) PRIMARY KEY , 	 
	[bank_code] [nvarchar](15) NULL,
	[project_id] [nvarchar](50) NULL,
	[operation_type] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[data_for_type] [nvarchar](50) NULL,
	[source_info] [nvarchar](2000) NULL,
	[destination_info] [nvarchar](2000) NULL,
	[status_info_json] [nvarchar](max) NULL,
	[start_loading_on] [datetime] NULL,
	[end_loading_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[validated_on] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approver_comment] [nvarchar](2000) NULL,
	[consolidated_on] [datetime] NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_comment] [nvarchar](2000) NULL,
	[record_status] [nvarchar](50) NULL,
	[is_valid_file] [int] NULL,
	[date_created] [datetime] NULL,
	[date_updated] [datetime] NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[validation_code] [nvarchar](100) NULL
 

)	


SET IDENTITY_INSERT [data_update_log_tmp]  ON


INSERT INTO dbo.[data_update_log_tmp] (
	  [id]
      ,[bank_code]
      ,[project_id]
      ,[operation_type]
      ,[datafor_date_time]
      
      ,[data_for_type]
      ,[source_info]
      ,[destination_info]
      ,[status_info_json]
      ,[start_loading_on]
      ,[end_loading_on]
      ,[created_by]
      ,[created_reference_id]
      ,[validated_on]
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[approver_comment]
      ,[consolidated_on]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_comment]
      ,[record_status]
      ,[is_valid_file]
      ,[date_created]
      ,[date_updated]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[validation_code]
	)

SELECT 
 [id]
      ,[bank_code]
      ,[project_id]
      ,[operation_type]
      ,[datafor_date_time]
      
      ,[data_for_type]
      ,[source_info]
      ,[destination_info]
      ,[status_info_json]
      ,[start_loading_on]
      ,[end_loading_on]
      ,[created_by]
      ,[created_reference_id]
      ,[validated_on]
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[approver_comment]
      ,[consolidated_on]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_comment]
      ,[record_status]
      ,[is_valid_file]
      ,[date_created]
      ,[date_updated]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[validation_code]

	FROM data_update_log  ;


SET IDENTITY_INSERT dbo.[data_update_log_tmp]  OFF

EXEC sp_rename 'data_update_log', 'data_update_log_tmp_bckp_21012019_1925'

EXEC sp_rename 'data_update_log_tmp', 'data_update_log'

drop table data_update_log_tmp_bckp_21012019_1925;
