  
CREATE TABLE [dbo].[cash_balance_file_psb_tmp]
   (
   [id] [bigint] IDENTITY(1,1) PRIMARY KEY, 
	[atm_id] [nvarchar](15) NULL,
	[cash_out] [int] NULL,
	[endcash] [int] NULL,
	[hop1cash] [int] NULL,
	[hop1bill] [int] NULL,
	[hop2cash] [int] NULL,
	[hop2bill] [int] NULL,
	[hop3cash] [int] NULL,
	[hop3bill] [int] NULL,
	[hop4cash] [int] NULL,
	[hop4bill] [int] NULL,
	[ownr] [nvarchar](50) NULL,
	[state] [nvarchar](10) NULL,
	[timestamp] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_psb_tmp  ON


INSERT INTO dbo.cash_balance_file_psb_tmp (
	   [id]
      ,[atm_id]
      ,[cash_out]
      ,[endcash]
      ,[hop1cash]
      ,[hop1bill]
      ,[hop2cash]
      ,[hop2bill]
      ,[hop3cash]
      ,[hop3bill]
      ,[hop4cash]
      ,[hop4bill]
      ,[ownr]
      ,[state]
      ,[timestamp]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
   
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	)

SELECT 
    [id]
      ,[atm_id]
      ,[cash_out]
      ,[endcash]
      ,[hop1cash]
      ,[hop1bill]
      ,[hop2cash]
      ,[hop2bill]
      ,[hop3cash]
      ,[hop3bill]
      ,[hop4cash]
      ,[hop4bill]
      ,[ownr]
      ,[state]
      ,[timestamp]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
   
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_psb;


SET IDENTITY_INSERT dbo.cash_balance_file_psb_tmp  OFF

EXEC sp_rename 'cash_balance_file_psb', 'cash_balance_file_psb_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_psb_tmp', 'cash_balance_file_psb'

drop table cash_balance_file_psb_tmp_bckp_21012019_1925;
