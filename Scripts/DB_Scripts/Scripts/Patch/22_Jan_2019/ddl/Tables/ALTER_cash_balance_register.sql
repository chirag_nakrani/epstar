  
CREATE TABLE [dbo].[cash_balance_register_tmp]
   (
    [id] [bigint] IDENTITY(1,1) PRIMARY KEY,   
	 
	[datafor_date_time] [datetime] NULL,
	[bank_name] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[remaining_balance_50] [bigint] NULL,
	[remaining_balance_100] [bigint] NULL,
	[remaining_balance_200] [bigint] NULL,
	[remaining_balance_500] [bigint] NULL,
	[remaining_balance_2000] [bigint] NULL,
	[total_remaining_balance] [bigint] NULL,
	[calculated_remaining_balance_amount] [bigint] NULL,
	[is_total_remaining_matched_with_calculated] [bit] NULL,
	[exception_trigger_id] [nvarchar](255) NULL,
	[project_id] [nvarchar](50) NULL,
	[region] [nvarchar](50) NULL,
	[record_status] [nvarchar](20) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT [cash_balance_register_tmp]  ON


INSERT INTO dbo.[cash_balance_register_tmp] (
	[id]
      ,[datafor_date_time]
      ,[bank_name]
      ,[atm_id]
      ,[remaining_balance_50]
      ,[remaining_balance_100]
      ,[remaining_balance_200]
      ,[remaining_balance_500]
      ,[remaining_balance_2000]
      ,[total_remaining_balance]
      ,[calculated_remaining_balance_amount]
      ,[is_total_remaining_matched_with_calculated]
      ,[exception_trigger_id]
      ,[project_id]
       
      ,[record_status]
      ,[is_valid_record]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
	)

SELECT 
 [id]
      ,[datafor_date_time]
      ,[bank_name]
      ,[atm_id]
      ,[remaining_balance_50]
      ,[remaining_balance_100]
      ,[remaining_balance_200]
      ,[remaining_balance_500]
      ,[remaining_balance_2000]
      ,[total_remaining_balance]
      ,[calculated_remaining_balance_amount]
      ,[is_total_remaining_matched_with_calculated]
      ,[exception_trigger_id]
      ,[project_id]
       
      ,[record_status]
      ,[is_valid_record]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]

	FROM cash_balance_register ;


SET IDENTITY_INSERT dbo.cash_balance_register_tmp  OFF

EXEC sp_rename 'cash_balance_register', 'cash_balance_register_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_register_tmp', 'cash_balance_register'

drop table cash_balance_register_tmp_bckp_21012019_1925;
