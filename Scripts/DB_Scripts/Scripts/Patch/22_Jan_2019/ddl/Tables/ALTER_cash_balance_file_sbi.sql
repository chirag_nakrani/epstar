  
CREATE TABLE [dbo].[cash_balance_file_sbi_tmp]
   (
   [id] [bigint] IDENTITY(1,1) PRIMARY KEY , 
	[terminal_location] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[cash_dispensed_hopper_1] [int] NULL,
	[bills_hopr1] [int] NULL,
	[cash_dispensed_hopper_2] [int] NULL,
	[bills_hopr2] [int] NULL,
	[cash_dispensed_hopper_3] [int] NULL,
	[bills_hopr3] [int] NULL,
	[cash_dispensed_hopper_4] [int] NULL,
	[bills_hopr4] [int] NULL,
	[cash_dispensed_hopper_5] [int] NULL,
	[bills_hopr5] [int] NULL,
	[cash_dispensed_hopper_6] [int] NULL,
	[bills_hopr6] [int] NULL,
	[atm_id] [nvarchar](20) NULL,
	[total_remaining_cash_def_curr] [int] NULL,
	[remote_address] [nvarchar](255) NULL,
	[cash_increment_hopper_1] [int] NULL,
	[cash_increment_hopper_2] [int] NULL,
	[cash_increment_hopper_3] [int] NULL,
	[cash_increment_hopper_4] [int] NULL,
	[cash_increment_hopper_5] [int] NULL,
	[cash_increment_hopper_6] [int] NULL,
	[circle] [nvarchar](100) NULL,
	[sitetype] [nvarchar](15) NULL,
	[msvendor] [nvarchar](30) NULL,
	[lastwithdrawaltime] [datetime] NULL,
	[switch] [nvarchar](50) NULL,
	[district] [nvarchar](50) NULL,
	[module] [nvarchar](100) NULL,
	[currentstatus] [nvarchar](10) NULL,
	[branchmanagerphone] [nvarchar](max) NULL,
	[channelmanagercontact] [nvarchar](max) NULL,
	[jointcustodianphone] [nvarchar](max) NULL,
	[network] [nvarchar](10) NULL,
	[populationgroup] [nvarchar](20) NULL,
	[regionname] [int] NULL,
	[statename] [nvarchar](255) NULL,
	[jointcustodianname1] [nvarchar](50) NULL,
	[jointcustodianphone2] [nvarchar](15) NULL,
	[last_deposit_txn_time] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_sbi_tmp  ON


INSERT INTO dbo.cash_balance_file_sbi_tmp (
	  [id]
      ,[terminal_location]
      ,[city]
      ,[cash_dispensed_hopper_1]
      ,[bills_hopr1]
      ,[cash_dispensed_hopper_2]
      ,[bills_hopr2]
      ,[cash_dispensed_hopper_3]
      ,[bills_hopr3]
      ,[cash_dispensed_hopper_4]
      ,[bills_hopr4]
      ,[cash_dispensed_hopper_5]
      ,[bills_hopr5]
      ,[cash_dispensed_hopper_6]
      ,[bills_hopr6]
      ,[atm_id]
      ,[total_remaining_cash_def_curr]
      ,[remote_address]
      ,[cash_increment_hopper_1]
      ,[cash_increment_hopper_2]
      ,[cash_increment_hopper_3]
      ,[cash_increment_hopper_4]
      ,[cash_increment_hopper_5]
      ,[cash_increment_hopper_6]
      ,[circle]
      ,[sitetype]
      ,[msvendor]
      ,[lastwithdrawaltime]
      ,[switch]
      ,[district]
      ,[module]
      ,[currentstatus]
      ,[branchmanagerphone]
      ,[channelmanagercontact]
      ,[jointcustodianphone]
      ,[network]
      ,[populationgroup]
      ,[regionname]
      ,[statename]
      ,[jointcustodianname1]
      ,[jointcustodianphone2]
      ,[last_deposit_txn_time]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
     
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	)

SELECT 
   [id]
      ,[terminal_location]
      ,[city]
      ,[cash_dispensed_hopper_1]
      ,[bills_hopr1]
      ,[cash_dispensed_hopper_2]
      ,[bills_hopr2]
      ,[cash_dispensed_hopper_3]
      ,[bills_hopr3]
      ,[cash_dispensed_hopper_4]
      ,[bills_hopr4]
      ,[cash_dispensed_hopper_5]
      ,[bills_hopr5]
      ,[cash_dispensed_hopper_6]
      ,[bills_hopr6]
      ,[atm_id]
      ,[total_remaining_cash_def_curr]
      ,[remote_address]
      ,[cash_increment_hopper_1]
      ,[cash_increment_hopper_2]
      ,[cash_increment_hopper_3]
      ,[cash_increment_hopper_4]
      ,[cash_increment_hopper_5]
      ,[cash_increment_hopper_6]
      ,[circle]
      ,[sitetype]
      ,[msvendor]
      ,[lastwithdrawaltime]
      ,[switch]
      ,[district]
      ,[module]
      ,[currentstatus]
      ,[branchmanagerphone]
      ,[channelmanagercontact]
      ,[jointcustodianphone]
      ,[network]
      ,[populationgroup]
      ,[regionname]
      ,[statename]
      ,[jointcustodianname1]
      ,[jointcustodianphone2]
      ,[last_deposit_txn_time]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
     
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_sbi;


SET IDENTITY_INSERT dbo.cash_balance_file_sbi_tmp  OFF

EXEC sp_rename 'cash_balance_file_sbi', 'cash_balance_file_sbi_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_sbi_tmp', 'cash_balance_file_sbi'

drop table cash_balance_file_sbi_tmp_bckp_21012019_1925;
