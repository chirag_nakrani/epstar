 

CREATE TABLE [dbo].[cash_balance_file_alb_tmp](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[group_name] [nvarchar](50) NULL,
	[termid] [nvarchar](50) NULL,
	[location] [nvarchar](100) NULL,
	[currbalcass1] [int] NULL,
	[ccurrbalcass2] [int] NULL,
	[currbalcass3] [int] NULL,
	[currbalcass4] [int] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](50) NULL,
	[error_code] [nvarchar](50) NULL
)	


SET IDENTITY_INSERT dbo.cash_balance_file_alb_tmp  ON


INSERT INTO dbo.cash_balance_file_alb_tmp (
		[id]
      ,[group_name]
      ,[termid]
      ,[location]
      ,[currbalcass1]
      ,[ccurrbalcass2]
      ,[currbalcass3]
      ,[currbalcass4]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]   
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	
	)

SELECT 
        [id]
      ,[group_name]
      ,[termid]
      ,[location]
      ,[currbalcass1]
      ,[ccurrbalcass2]
      ,[currbalcass3]
      ,[currbalcass4]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]   
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_alb;


SET IDENTITY_INSERT dbo.cash_balance_file_alb_tmp  OFF

EXEC sp_rename 'cash_balance_file_alb', 'cash_balance_file_alb_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_alb_tmp', 'cash_balance_file_alb'

drop table cash_balance_file_alb_tmp_bckp_21012019_1925;
