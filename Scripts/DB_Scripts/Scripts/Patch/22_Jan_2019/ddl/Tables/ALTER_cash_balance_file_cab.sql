  
CREATE TABLE [dbo].[cash_balance_file_cab_tmp](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[atm_id] [nvarchar](15) NULL,
	[termloc] [nvarchar](100) NULL,
	[termcity] [nvarchar](50) NULL,
	[regn_id] [nvarchar](15) NULL,
	[network_link_status] [nvarchar](10) NULL,
	[atm_status] [nvarchar](10) NULL,
	[out_time] [nvarchar](15) NULL,
	[err_1] [nvarchar](max) NULL,
	[err_2] [nvarchar](max) NULL,
	[err_3] [nvarchar](max) NULL,
	[err_4] [nvarchar](max) NULL,
	[err_5] [nvarchar](max) NULL,
	[err_6] [nvarchar](max) NULL,
	[err_7] [nvarchar](max) NULL,
	[err_8] [nvarchar](max) NULL,
	[end_cash] [int] NULL,
	[last_tran] [nvarchar](max) NULL,
	[last_up] [nvarchar](max) NULL,
	[hop1cash] [int] NULL,
	[hop1bill] [int] NULL,
	[hop2cash] [int] NULL,
	[hop2bill] [int] NULL,
	[hop3cash] [int] NULL,
	[hop3bill] [int] NULL,
	[hop4cash] [int] NULL,
	[hop4bill] [int] NULL,
	[hop5cash] [nvarchar](max) NULL,
	[timestamp] [nvarchar](255) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](50) NULL,
	[modified_by] [varchar](50) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_cab_tmp  ON


INSERT INTO dbo.cash_balance_file_cab_tmp (
	 [id]
      ,[atm_id]
      ,[termloc]
      ,[termcity]
      ,[regn_id]
      ,[network_link_status]
      ,[atm_status]
      ,[out_time]
      ,[err_1]
      ,[err_2]
      ,[err_3]
      ,[err_4]
      ,[err_5]
      ,[err_6]
      ,[err_7]
      ,[err_8]
      ,[end_cash]
      ,[last_tran]
      ,[last_up]
      ,[hop1cash]
      ,[hop1bill]
      ,[hop2cash]
      ,[hop2bill]
      ,[hop3cash]
      ,[hop3bill]
      ,[hop4cash]
      ,[hop4bill]
      ,[hop5cash]
      ,[timestamp]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
   
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[modified_by]
      ,[error_code]
	)

SELECT 
       [id]
      ,[atm_id]
      ,[termloc]
      ,[termcity]
      ,[regn_id]
      ,[network_link_status]
      ,[atm_status]
      ,[out_time]
      ,[err_1]
      ,[err_2]
      ,[err_3]
      ,[err_4]
      ,[err_5]
      ,[err_6]
      ,[err_7]
      ,[err_8]
      ,[end_cash]
      ,[last_tran]
      ,[last_up]
      ,[hop1cash]
      ,[hop1bill]
      ,[hop2cash]
      ,[hop2bill]
      ,[hop3cash]
      ,[hop3bill]
      ,[hop4cash]
      ,[hop4bill]
      ,[hop5cash]
      ,[timestamp]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
      
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[modified_by]
      ,[error_code]
	FROM cash_balance_file_cab;


SET IDENTITY_INSERT dbo.cash_balance_file_cab_tmp  OFF

EXEC sp_rename 'cash_balance_file_cab', 'cash_balance_file_cab_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_cab_tmp', 'cash_balance_file_cab'

drop table cash_balance_file_cab_tmp_bckp_21012019_1925;
