  
CREATE TABLE [dbo].[cash_balance_file_dena_tmp](
    [id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[atm_id] [nvarchar](15) NULL,
	[stat] [nvarchar](50) NULL,
	[atm_stat] [nvarchar](50) NULL,
	[fiid] [nvarchar](100) NULL,
	[err_2] [nvarchar](255) NULL,
	[err_3] [nvarchar](255) NULL,
	[err_4] [nvarchar](255) NULL,
	[err_5] [nvarchar](255) NULL,
	[err_6] [nvarchar](255) NULL,
	[err_7] [nvarchar](255) NULL,
	[err_8] [nvarchar](255) NULL,
	[cashchng] [int] NULL,
	[cashout] [int] NULL,
	[cashrepl] [nvarchar](255) NULL,
	[cashsply] [nvarchar](255) NULL,
	[end_cash] [int] NULL,
	[hop4cash] [int] NULL,
	[hop4bill] [int] NULL,
	[hop5cash] [int] NULL,
	[hop5bill] [int] NULL,
	[amt_chk] [int] NULL,
	[num_chk] [int] NULL,
	[amt_dep] [int] NULL,
	[num_dep] [int] NULL,
	[intvdown] [nvarchar](255) NULL,
	[flt_cnt] [int] NULL,
	[hop1cash] [int] NULL,
	[hop1bill] [int] NULL,
	[hop2cash] [int] NULL,
	[hop2bill] [int] NULL,
	[hop3cash] [int] NULL,
	[hop3bill] [int] NULL,
	[linestat] [nvarchar](255) NULL,
	[termloc] [nvarchar](255) NULL,
	[termcity] [nvarchar](255) NULL,
	[term_st] [nvarchar](255) NULL,
	[termst_x] [nvarchar](255) NULL,
	[ownr] [nvarchar](255) NULL,
	[row_cnt] [int] NULL,
	[src_name] [nvarchar](255) NULL,
	[spvr_on] [nvarchar](255) NULL,
	[brch_id] [nvarchar](255) NULL,
	[regn_id] [nvarchar](255) NULL,
	[timeofst] [nvarchar](255) NULL,
	[atm_type] [nvarchar](255) NULL,
	[vendor_name] [nvarchar](255) NULL,
	[timestamp_data] [nvarchar](255) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_dena_tmp  ON


INSERT INTO dbo.cash_balance_file_dena_tmp (
	 [id]
      ,[atm_id]
      ,[stat]
      ,[atm_stat]
      ,[fiid]
      ,[err_2]
      ,[err_3]
      ,[err_4]
      ,[err_5]
      ,[err_6]
      ,[err_7]
      ,[err_8]
      ,[cashchng]
      ,[cashout]
      ,[cashrepl]
      ,[cashsply]
      ,[end_cash]
      ,[hop4cash]
      ,[hop4bill]
      ,[hop5cash]
      ,[hop5bill]
      ,[amt_chk]
      ,[num_chk]
      ,[amt_dep]
      ,[num_dep]
      ,[intvdown]
      ,[flt_cnt]
      ,[hop1cash]
      ,[hop1bill]
      ,[hop2cash]
      ,[hop2bill]
      ,[hop3cash]
      ,[hop3bill]
      ,[linestat]
      ,[termloc]
      ,[termcity]
      ,[term_st]
      ,[termst_x]
      ,[ownr]
      ,[row_cnt]
      ,[src_name]
      ,[spvr_on]
      ,[brch_id]
      ,[regn_id]
      ,[timeofst]
      ,[atm_type]
      ,[vendor_name]
      ,[timestamp_data]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
    
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	)

SELECT 
    [id]
      ,[atm_id]
      ,[stat]
      ,[atm_stat]
      ,[fiid]
      ,[err_2]
      ,[err_3]
      ,[err_4]
      ,[err_5]
      ,[err_6]
      ,[err_7]
      ,[err_8]
      ,[cashchng]
      ,[cashout]
      ,[cashrepl]
      ,[cashsply]
      ,[end_cash]
      ,[hop4cash]
      ,[hop4bill]
      ,[hop5cash]
      ,[hop5bill]
      ,[amt_chk]
      ,[num_chk]
      ,[amt_dep]
      ,[num_dep]
      ,[intvdown]
      ,[flt_cnt]
      ,[hop1cash]
      ,[hop1bill]
      ,[hop2cash]
      ,[hop2bill]
      ,[hop3cash]
      ,[hop3bill]
      ,[linestat]
      ,[termloc]
      ,[termcity]
      ,[term_st]
      ,[termst_x]
      ,[ownr]
      ,[row_cnt]
      ,[src_name]
      ,[spvr_on]
      ,[brch_id]
      ,[regn_id]
      ,[timeofst]
      ,[atm_type]
      ,[vendor_name]
      ,[timestamp_data]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
    
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_dena;


SET IDENTITY_INSERT dbo.cash_balance_file_dena_tmp  OFF

EXEC sp_rename 'cash_balance_file_dena', 'cash_balance_file_dena_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_dena_tmp', 'cash_balance_file_dena'

drop table cash_balance_file_dena_tmp_bckp_21012019_1925;
