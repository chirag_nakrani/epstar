 

 
	 

CREATE TABLE [dbo].[cash_balance_file_bob_tmp](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY  NOT NULL,
	[atmid] [nvarchar](15) NULL,
	[location] [nvarchar](100) NULL,
	[state] [nvarchar](10) NULL,
	[atmstatus] [nvarchar](10) NULL,
	[msvendor] [nvarchar](15) NULL,
	[currentstatus] [nvarchar](15) NULL,
	[regionname] [nvarchar](50) NULL,
	[zone] [nvarchar](50) NULL,
	[address] [nvarchar](max) NULL,
	[hop1bill] [int] NULL,
	[hop2bill] [int] NULL,
	[hop3bill] [int] NULL,
	[hop4bill] [int] NULL,
	[hop1begcash] [int] NULL,
	[hop2begcash] [int] NULL,
	[hop3begcash] [int] NULL,
	[hop4begcash] [int] NULL,
	[hop1endcash] [int] NULL,
	[hop2endcash] [int] NULL,
	[hop3endcash] [int] NULL,
	[hop4endcash] [int] NULL,
	[hop1cashdeposit] [int] NULL,
	[hop2cashdeposit] [int] NULL,
	[hop3cashdeposit] [int] NULL,
	[hop4cashdeposit] [int] NULL,
	[begcash] [int] NULL,
	[endcash] [int] NULL,
	[cashout] [int] NULL,
	[cashincr] [int] NULL,
	[timestamp] [datetime] NULL,
	[lastwithdrawaltime] [datetime] NULL,
	[lastsupervisorytime] [datetime] NULL,
	[lastdeposittranstime] [nvarchar](50) NULL,
	[lastadmintxntime] [datetime] NULL,
	[lastreversaltime] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL, 
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[bank_name] [nvarchar](100) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_bob_tmp  ON


INSERT INTO dbo.cash_balance_file_bob_tmp (
		 [id]
      ,[atmid]
      ,[location]
      ,[state]
      ,[atmstatus]
      ,[msvendor]
      ,[currentstatus]
      ,[regionname]
      ,[zone]
      ,[address]
      ,[hop1bill]
      ,[hop2bill]
      ,[hop3bill]
      ,[hop4bill]
      ,[hop1begcash]
      ,[hop2begcash]
      ,[hop3begcash]
      ,[hop4begcash]
      ,[hop1endcash]
      ,[hop2endcash]
      ,[hop3endcash]
      ,[hop4endcash]
      ,[hop1cashdeposit]
      ,[hop2cashdeposit]
      ,[hop3cashdeposit]
      ,[hop4cashdeposit]
      ,[begcash]
      ,[endcash]
      ,[cashout]
      ,[cashincr]
      ,[timestamp]
      ,[lastwithdrawaltime]
      ,[lastsupervisorytime]
      ,[lastdeposittranstime]
      ,[lastadmintxntime]
      ,[lastreversaltime]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]      
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[bank_name]
      ,[is_valid_record]
      ,[error_code]
	
	)

SELECT 
         [id]
      ,[atmid]
      ,[location]
      ,[state]
      ,[atmstatus]
      ,[msvendor]
      ,[currentstatus]
      ,[regionname]
      ,[zone]
      ,[address]
      ,[hop1bill]
      ,[hop2bill]
      ,[hop3bill]
      ,[hop4bill]
      ,[hop1begcash]
      ,[hop2begcash]
      ,[hop3begcash]
      ,[hop4begcash]
      ,[hop1endcash]
      ,[hop2endcash]
      ,[hop3endcash]
      ,[hop4endcash]
      ,[hop1cashdeposit]
      ,[hop2cashdeposit]
      ,[hop3cashdeposit]
      ,[hop4cashdeposit]
      ,[begcash]
      ,[endcash]
      ,[cashout]
      ,[cashincr]
      ,[timestamp]
      ,[lastwithdrawaltime]
      ,[lastsupervisorytime]
      ,[lastdeposittranstime]
      ,[lastadmintxntime]
      ,[lastreversaltime]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]     
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[bank_name]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_bob;


SET IDENTITY_INSERT dbo.cash_balance_file_bob_tmp  OFF

EXEC sp_rename 'cash_balance_file_bob', 'cash_balance_file_bob_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_bob_tmp', 'cash_balance_file_bob'

drop table cash_balance_file_bob_tmp_bckp_21012019_1925;
