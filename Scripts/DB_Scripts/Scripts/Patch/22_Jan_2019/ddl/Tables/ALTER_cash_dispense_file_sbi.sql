  
CREATE TABLE [dbo].[cash_dispense_file_sbi_tmp]
   (
    [id] [bigint] IDENTITY(1,1) PRIMARY KEY , 	 
	[atm_id] [nvarchar](20) NOT NULL,
	[cash_withdrawal] [int] NULL,
	[opening_cash] [int] NULL,
	[total_cash_replenished] [int] NULL,
	[cer] [float] NULL,
	[msvendor] [nvarchar](20) NULL,
	[circle_name] [nvarchar](30) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT [cash_dispense_file_sbi_tmp]  ON


INSERT INTO dbo.[cash_dispense_file_sbi_tmp] (
	 [id]
      ,[atm_id]
      ,[cash_withdrawal]
      ,[opening_cash]
      ,[total_cash_replenished]
      ,[cer]
      ,[msvendor]
      ,[circle_name]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
   
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[is_valid_record]
      ,[error_code]
	)

SELECT 
 [id]
      ,[atm_id]
      ,[cash_withdrawal]
      ,[opening_cash]
      ,[total_cash_replenished]
      ,[cer]
      ,[msvendor]
      ,[circle_name]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
  
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[is_valid_record]
      ,[error_code]

	FROM  [cash_dispense_file_sbi] ;


SET IDENTITY_INSERT dbo.[cash_dispense_file_sbi_tmp]  OFF

EXEC sp_rename 'cash_dispense_file_sbi', 'cash_dispense_file_sbi_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_dispense_file_sbi_tmp', 'cash_dispense_file_sbi'

drop table cash_dispense_file_sbi_tmp_bckp_21012019_1925;
