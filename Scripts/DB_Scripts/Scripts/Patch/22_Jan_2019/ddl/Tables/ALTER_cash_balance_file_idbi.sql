  
CREATE TABLE [dbo].[cash_balance_file_idbi_tmp]
   (
    [id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[TERMID] [nvarchar](255) NULL,
	[TiaDATETIME] [nvarchar](255) NULL,
	[Bill1] [bigint] NULL,
	[Bill2] [bigint] NULL,
	[Bill3] [bigint] NULL,
	[Bill4] [bigint] NULL,
	[STARTBALCASS1] [bigint] NULL,
	[STARTBALCASS2] [bigint] NULL,
	[STARTBALCASS3] [bigint] NULL,
	[STARTBALCASS4] [bigint] NULL,
	[CASHINCCASS1] [bigint] NULL,
	[CASHINCCASS2] [bigint] NULL,
	[CASHINCCASS3] [bigint] NULL,
	[CASHINCCASS4] [bigint] NULL,
	[CASHOUTCASS1] [bigint] NULL,
	[CASHOUTCASS2] [bigint] NULL,
	[CASHOUTCASS3] [bigint] NULL,
	[CASHOUTCASS4] [bigint] NULL,
	[CURRBALCASS1] [bigint] NULL,
	[CURRBALCASS2] [bigint] NULL,
	[CURRBALCASS3] [bigint] NULL,
	[CURRBALCASS4] [bigint] NULL,
	[LASTRESETDATE] [nvarchar](255) NULL,
	[LASTRESETTIME] [float] NULL,
	[LASTTXNDATE] [nvarchar](255) NULL,
	[LASTTXNTIME] [float] NULL,
	[LASTTXNSERIAL] [float] NULL,
	[LASTTXNSTATUS] [float] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_idbi_tmp  ON


INSERT INTO dbo.cash_balance_file_idbi_tmp (
	   [id]
      ,[TERMID]
      ,[TiaDATETIME]
      ,[Bill1]
      ,[Bill2]
      ,[Bill3]
      ,[Bill4]
      ,[STARTBALCASS1]
      ,[STARTBALCASS2]
      ,[STARTBALCASS3]
      ,[STARTBALCASS4]
      ,[CASHINCCASS1]
      ,[CASHINCCASS2]
      ,[CASHINCCASS3]
      ,[CASHINCCASS4]
      ,[CASHOUTCASS1]
      ,[CASHOUTCASS2]
      ,[CASHOUTCASS3]
      ,[CASHOUTCASS4]
      ,[CURRBALCASS1]
      ,[CURRBALCASS2]
      ,[CURRBALCASS3]
      ,[CURRBALCASS4]
      ,[LASTRESETDATE]
      ,[LASTRESETTIME]
      ,[LASTTXNDATE]
      ,[LASTTXNTIME]
      ,[LASTTXNSERIAL]
      ,[LASTTXNSTATUS]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
 
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	)

SELECT 
    [id]
      ,[TERMID]
      ,[TiaDATETIME]
      ,[Bill1]
      ,[Bill2]
      ,[Bill3]
      ,[Bill4]
      ,[STARTBALCASS1]
      ,[STARTBALCASS2]
      ,[STARTBALCASS3]
      ,[STARTBALCASS4]
      ,[CASHINCCASS1]
      ,[CASHINCCASS2]
      ,[CASHINCCASS3]
      ,[CASHINCCASS4]
      ,[CASHOUTCASS1]
      ,[CASHOUTCASS2]
      ,[CASHOUTCASS3]
      ,[CASHOUTCASS4]
      ,[CURRBALCASS1]
      ,[CURRBALCASS2]
      ,[CURRBALCASS3]
      ,[CURRBALCASS4]
      ,[LASTRESETDATE]
      ,[LASTRESETTIME]
      ,[LASTTXNDATE]
      ,[LASTTXNTIME]
      ,[LASTTXNSERIAL]
      ,[LASTTXNSTATUS]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
    
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_idbi;


SET IDENTITY_INSERT dbo.cash_balance_file_idbi_tmp  OFF

EXEC sp_rename 'cash_balance_file_idbi', 'cash_balance_file_idbi_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_idbi_tmp', 'cash_balance_file_idbi'

drop table cash_balance_file_idbi_tmp_bckp_21012019_1925;
