  
CREATE TABLE [dbo].[cash_dispense_register_tmp]
   (
    [id] [bigint] IDENTITY(1,1) PRIMARY KEY , 	 
	[date] [datetime] NULL,
	[atm_id] [nvarchar](20) NOT NULL,
	[bank_name] [nvarchar](50) NOT NULL,
	[Location] [nvarchar](50) NULL,
	[total_dispense_amount] [int] NULL,
	[reference_id] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[region] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[record_status] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[dispense_type] [nvarchar](20) NULL
 

)	


SET IDENTITY_INSERT [cash_dispense_register_tmp]  ON


INSERT INTO dbo.[cash_dispense_register_tmp] (
	  [id]
      ,[date]
      ,[atm_id]
      ,[bank_name]
      ,[Location]
      ,[total_dispense_amount]
      ,[reference_id]
      ,[project_id]
     
      ,[created_on]
      ,[created_by]
      ,[approved_on]
      ,[approved_by]
      ,[deleted_on]
      ,[deleted_by]
      ,[modified_by]
      ,[modified_on]
      ,[record_status]
      ,[is_valid_record]
      ,[approved_reference_id]
      ,[created_reference_id]
      ,[datafor_date_time]
      ,[modified_reference_id]
      ,[deleted_reference_id]
      ,[dispense_type]
	)

SELECT 
 [id]
      ,[date]
      ,[atm_id]
      ,[bank_name]
      ,[Location]
      ,[total_dispense_amount]
      ,[reference_id]
      ,[project_id]     
      ,[created_on]
      ,[created_by]
      ,[approved_on]
      ,[approved_by]
      ,[deleted_on]
      ,[deleted_by]
      ,[modified_by]
      ,[modified_on]
      ,[record_status]
      ,[is_valid_record]
      ,[approved_reference_id]
      ,[created_reference_id]
      ,[datafor_date_time]
      ,[modified_reference_id]
      ,[deleted_reference_id]
      ,[dispense_type]

	FROM [cash_dispense_register]  ;


SET IDENTITY_INSERT dbo.[cash_dispense_register_tmp]  OFF

EXEC sp_rename 'cash_dispense_register', 'cash_dispense_file_register_bckp_21012019_1925'

EXEC sp_rename 'cash_dispense_register_tmp', 'cash_dispense_register'

drop table cash_dispense_file_register_bckp_21012019_1925;
