 

 
	 

CREATE TABLE [dbo].[cash_balance_file_boi_tmp](
	[id] [bigint] IDENTITY(1,1)  PRIMARY KEY NOT NULL,
	[institutionid] [int] NULL,
	[termid] [nvarchar](15) NULL,
	[location] [nvarchar](100) NULL,
	[cash_start1] [int] NULL,
	[cash_start2] [int] NULL,
	[cash_start3] [int] NULL,
	[cash_start4] [int] NULL,
	[cash_inc1] [int] NULL,
	[cash_inc2] [int] NULL,
	[cash_inc3] [int] NULL,
	[cash_inc4] [int] NULL,
	[cash_dec1] [int] NULL,
	[cash_dec2] [int] NULL,
	[cash_dec3] [int] NULL,
	[cash_dec4] [int] NULL,
	[cash_out1] [int] NULL,
	[cash_out2] [int] NULL,
	[cash_out3] [int] NULL,
	[cash_out4] [int] NULL,
	[cash_currbal1] [int] NULL,
	[cash_currbal2] [int] NULL,
	[cash_currbal3] [int] NULL,
	[cash_currbal4] [int] NULL,
	[termid1] [nvarchar](15) NULL,
	[txn_date] [datetime] NULL,
	[txn_time] [nvarchar](10) NULL,
	[respcode] [nvarchar](50) NULL,
	[pcode] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](50) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_boi_tmp  ON


INSERT INTO dbo.cash_balance_file_boi_tmp (
	[id]
      ,[institutionid]
      ,[termid]
      ,[location]
      ,[cash_start1]
      ,[cash_start2]
      ,[cash_start3]
      ,[cash_start4]
      ,[cash_inc1]
      ,[cash_inc2]
      ,[cash_inc3]
      ,[cash_inc4]
      ,[cash_dec1]
      ,[cash_dec2]
      ,[cash_dec3]
      ,[cash_dec4]
      ,[cash_out1]
      ,[cash_out2]
      ,[cash_out3]
      ,[cash_out4]
      ,[cash_currbal1]
      ,[cash_currbal2]
      ,[cash_currbal3]
      ,[cash_currbal4]
      ,[termid1]
      ,[txn_date]
      ,[txn_time]
      ,[respcode]
      ,[pcode]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
    
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	
	)

SELECT 
        [id]
      ,[institutionid]
      ,[termid]
      ,[location]
      ,[cash_start1]
      ,[cash_start2]
      ,[cash_start3]
      ,[cash_start4]
      ,[cash_inc1]
      ,[cash_inc2]
      ,[cash_inc3]
      ,[cash_inc4]
      ,[cash_dec1]
      ,[cash_dec2]
      ,[cash_dec3]
      ,[cash_dec4]
      ,[cash_out1]
      ,[cash_out2]
      ,[cash_out3]
      ,[cash_out4]
      ,[cash_currbal1]
      ,[cash_currbal2]
      ,[cash_currbal3]
      ,[cash_currbal4]
      ,[termid1]
      ,[txn_date]
      ,[txn_time]
      ,[respcode]
      ,[pcode]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
 
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_boi;


SET IDENTITY_INSERT dbo.cash_balance_file_boi_tmp  OFF

EXEC sp_rename 'cash_balance_file_boi', 'cash_balance_file_boi_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_boi_tmp', 'cash_balance_file_boi'

drop table cash_balance_file_boi_tmp_bckp_21012019_1925;
