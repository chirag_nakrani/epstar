  
CREATE TABLE [dbo].[cash_balance_file_iob_tmp]
   (
    [id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[atm_id] [nvarchar](15) NULL,
	[remaining_1] [int] NULL,
	[remaining_2] [int] NULL,
	[remaining_3] [int] NULL,
	[remaining_4] [int] NULL,
	[total_of_remaining] [int] NULL,
	[opening_1] [int] NULL,
	[opening_2] [int] NULL,
	[opening_3] [int] NULL,
	[opening_4] [int] NULL,
	[total_of_opening] [int] NULL,
	[dispense_1] [int] NULL,
	[dispense_2] [int] NULL,
	[dispense_3] [int] NULL,
	[dispense_4] [int] NULL,
	[total_of_dispense] [int] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
 

)	


SET IDENTITY_INSERT dbo.cash_balance_file_iob_tmp  ON


INSERT INTO dbo.cash_balance_file_iob_tmp (
	   [id]
      ,[atm_id]
      ,[remaining_1]
      ,[remaining_2]
      ,[remaining_3]
      ,[remaining_4]
      ,[total_of_remaining]
      ,[opening_1]
      ,[opening_2]
      ,[opening_3]
      ,[opening_4]
      ,[total_of_opening]
      ,[dispense_1]
      ,[dispense_2]
      ,[dispense_3]
      ,[dispense_4]
      ,[total_of_dispense]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
    
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	)

SELECT 
    [id]
      ,[atm_id]
      ,[remaining_1]
      ,[remaining_2]
      ,[remaining_3]
      ,[remaining_4]
      ,[total_of_remaining]
      ,[opening_1]
      ,[opening_2]
      ,[opening_3]
      ,[opening_4]
      ,[total_of_opening]
      ,[dispense_1]
      ,[dispense_2]
      ,[dispense_3]
      ,[dispense_4]
      ,[total_of_dispense]
      ,[project_id]
      ,[record_status]
      ,[created_on]
      ,[created_by]
      ,[created_reference_id]
      ,[datafor_date_time]
  
      ,[approved_on]
      ,[approved_by]
      ,[approved_reference_id]
      ,[rejected_on]
      ,[rejected_by]
      ,[reject_reference_id]
      ,[reject_trigger_reference_id]
      ,[deleted_on]
      ,[deleted_by]
      ,[deleted_reference_id]
      ,[modified_on]
      ,[modified_by]
      ,[modified_reference_id]
      ,[is_valid_record]
      ,[error_code]
	FROM cash_balance_file_iob;


SET IDENTITY_INSERT dbo.cash_balance_file_iob_tmp  OFF

EXEC sp_rename 'cash_balance_file_iob', 'cash_balance_file_iob_tmp_bckp_21012019_1925'

EXEC sp_rename 'cash_balance_file_iob_tmp', 'cash_balance_file_iob'

drop table cash_balance_file_iob_tmp_bckp_21012019_1925;
