GO
/****** Object:  Table [dbo].[project_bank_feeder_cra_mapping]    Script Date: 22-01-2019 18:59:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[project_bank_feeder_cra_mapping_tmp](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[bank_id] [nvarchar](50) NOT NULL,
	[project_id] [nvarchar](50) NOT NULL,
	[region] [nvarchar](50) NULL,
	[Feeder] [nvarchar](50) NULL,
	[iscbr] [nvarchar](50) NULL,
	[isdispense] [nvarchar](50) NULL
)
GO


SET IDENTITY_INSERT dbo.project_bank_feeder_cra_mapping_tmp  ON


INSERT INTO dbo.project_bank_feeder_cra_mapping_tmp (
	id,
	bank_id,
	project_id,
	Feeder,
	iscbr,
	isdispense
)

SELECT 

id,
	bank_id,
	project_id,
	Feeder,
	iscbr,
	isdispense
	FROM project_bank_feeder_cra_mapping;


SET IDENTITY_INSERT dbo.project_bank_feeder_cra_mapping_tmp  OFF

EXEC sp_rename 'project_bank_feeder_cra_mapping', 'project_bank_feeder_cra_mapping_tmp_bckp_22012109_1814'

EXEC sp_rename 'project_bank_feeder_cra_mapping_tmp', 'project_bank_feeder_cra_mapping'

drop table project_bank_feeder_cra_mapping_tmp_bckp_22012109_1814;
