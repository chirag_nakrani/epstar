/****** Object:  StoredProcedure [dbo].[uspConsolidateCBR_PSB]    Script Date: 27-11-2018 15:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[uspConsolidateCBR_PSB] 
   (@datafor_date_time varchar(100),@projectid varchar(50), @region varchar(50) ,@referenceid varchar(50),@systemUser varchar(50),@out varchar(200) OUTPUT)
AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;
DECLARE @bankcode nvarchar(10) = 'PSB'
DECLARE @datafor nvarchar(10) = 'CBR'
DECLARE @activestatus nvarchar(20) = 'Active'
DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
DECLARE @approvedstatus nvarchar(20) = 'Approved'

INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		IF EXISTS(
			Select 1 as ColumnName
			FROM [dbo].[data_update_log]
			WHERE datafor_date_time = @datafor_date_time AND 
					bank_code = @bankcode AND 
					data_for_type = @datafor AND 
					record_status = @approvedstatus AND
					project_id = @projectid AND 
					region= @region   AND
					created_reference_id = @referenceid

			) 
		BEGIN			--- Check for approved status in DUL

			IF EXISTS   (						
				SELECT 1
				FROM [dbo].[data_update_log]
				WHERE datafor_date_time = @datafor_date_time AND 
				bank_code = @bankcode AND 
				data_for_type = @datafor AND 
				record_status = @activestatus AND
				region= @region   AND
				project_id = @projectid
				)	
				BEGIN				--- Check Active status in DUL


					--	BEGIN	--- For bank (START)
							IF EXISTS
							(
								Select 1 as ColumnName
								FROM [dbo].[cash_balance_file_psb]
								WHERE datafor_date_time =  @datafor_date_time AND  
										record_status = @activestatus AND
										region= @region   AND
										project_id = @projectid
							) 
							BEGIN						-----Check active in bankwise file
								IF EXISTS(
										Select 1 as ColumnName
										FROM [dbo].[cash_balance_register]
										WHERE datafor_date_time = @datafor_date_time AND 
												bank_name = @bankcode  AND 
												record_status = @activestatus AND
												region= @region   AND
												project_id = @projectid
										)
										BEGIN			---- Check Cash Balance Register Table for Active Status (START)	
										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Active record found!! Changing status from Active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

											DECLARE @SetWhere_CBR_bankWise VARCHAR(MAX) = ' datafor_date_time = ''' + 
													@datafor_date_time  + ''' and project_id = ''' + @projectid + ''' and region= ''' +@region+''''
											EXEC dbo.[uspSetStatus] 'dbo.cash_balance_file_psb',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CBR_bankWise,@out OUTPUT
											
											DECLARE @SetWhere_CBR VARCHAR(MAX) = ' bank_name = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and project_id = ''' + @projectid + ''' and region= ''' +@region+''''
							
											EXEC dbo.[uspSetStatus] 'dbo.cash_balance_register',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CBR,@out OUTPUT
										
											DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) = ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor + ''' and project_id = ''' + @projectid + ''' and region= ''' +@region+''''

											EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
											

											DECLARE @SetWhere_CBR_bankWise1 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
													@datafor_date_time   + ''' and project_id = ''' + @projectid + ''' and  created_reference_id = ''' + @referenceid + ''' and region= ''' +@region+''''
											
											EXEC dbo.[uspSetStatus] 'dbo.cash_balance_file_psb',@approvedstatus,@activestatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CBR_bankWise1,@out OUTPUT
										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records successfully marked as Deleted. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

											---------inserting data after status update for revision
												INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id
													)
											
													Select datafor_date_time, 
															@bankcode,
															atm_id,
															CASE 
																WHEN hop1bill = 100  THEN [hop1cash]
																WHEN hop2bill = 100  THEN [hop2cash] ELSE 0  END,
															0,
															CASE 
																WHEN hop2bill = 500 then [hop2cash]
																WHEN hop3bill = 500 then [hop3cash] ELSE 0  END,
															CASE 
																WHEN hop3bill = 2000 then [hop3cash]
																WHEN hop4bill = 2000 then [hop4cash] ELSE 0  END,
															[hop1cash] + [hop2cash]+[hop3cash]+[hop4cash] as closing_bal,
															project_id,
															region,
															@activestatus,
															is_valid_record,
															created_on,
															created_by,
															created_reference_id,
															approved_on,
															approved_by,
															approved_reference_id,
															modified_on,
															modified_by,
															modified_reference_id   
														from  
														[dbo].[cash_balance_file_psb]
														where datafor_date_time= @datafor_date_time 
														and record_status = @activestatus
														and project_id = @projectid
														and region=@region
														and created_reference_id = @referenceid

													INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														IF EXISTS (
																		SELECT 1 from dbo.cash_balance_register where 
																				datafor_date_time = @datafor_date_time and  
																				bank_name = @bankcode and 
																				 region=@region and
																				record_status = @activestatus and 
																				project_id = @projectid
																	)
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END

								

										INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure  Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
	

										END				---- Check Cash Balance Register Table for Active Status (END)

										
										--ELSE			---- Check Cash Balance Register Table ELSE
										--BEGIN 
										--END			

									----- BOMH Active Check Start(END)
							
							END
							ELSE		------	BOMH ELSE
							BEGIN
								RAISERROR(50050,16,1)
								--SELECT @out
				
						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN
					
					DECLARE @SetWhere_CBR_bankWise2 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
							@datafor_date_time   + ''' and project_id = ''' + @projectid + ''' and  created_reference_id = ''' + @referenceid + ''' and region= ''' +@region+''''
					
					EXEC dbo.[uspSetStatus] 'dbo.cash_balance_file_psb',@approvedstatus,@activestatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CBR_bankWise2,@out OUTPUT
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Records have been marked as Active status. Now consolidating Data',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
						
					---------inserting data after status update for revision
												INSERT INTO 
													dbo.cash_balance_register ( 
														datafor_date_time,
														[bank_name] ,
														[atm_id] , 
														[remaining_balance_100] ,
														[remaining_balance_200] ,
														[remaining_balance_500] ,
														[remaining_balance_2000],
														[total_remaining_balance],
														project_id,
														region,
														record_status,
														is_valid_record,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id
													)
											
													Select datafor_date_time, 
															@bankcode,
															atm_id,
															CASE 
																WHEN hop1bill = 100  THEN [hop1cash]
																WHEN hop2bill = 100  THEN [hop2cash] ELSE 0  END,
															0,
															CASE 
																WHEN hop2bill = 500 then [hop2cash]
																WHEN hop3bill = 500 then [hop3cash] ELSE 0  END,
															CASE 
																WHEN hop3bill = 2000 then [hop3cash]
																WHEN hop4bill = 2000 then [hop4cash] ELSE 0  END,
															[hop1cash] + [hop2cash]+[hop3cash]+[hop4cash] as closing_bal,
															project_id,
															region,
															@activestatus,
															is_valid_record,
															created_on,
															created_by,
															created_reference_id,
															approved_on,
															approved_by,
															approved_reference_id,
															modified_on,
															modified_by,
															modified_reference_id   
														from  
														[dbo].[cash_balance_file_psb]
														where datafor_date_time= @datafor_date_time 
														and region=@region
														and record_status = @activestatus
														and project_id = @projectid
														and created_reference_id = @referenceid


											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data Consolidated Successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														IF EXISTS (SELECT 1 from dbo.cash_balance_register where datafor_date_time = @datafor_date_time and  bank_name = @bankcode and record_status = @activestatus and project_id = @projectid)
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END

											INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
			BEGIN
				RAISERROR(50010,16,1)
			END
END


--select * from sys.messages

--USE master;  
--GO  
--EXEC sp_addmessage 50050, 16,   
--   N'Active status not found in bankwise file',@replace = 'replace';  
--GO  

--exec sp_addmessage ''
GO
