/****** Object:  StoredProcedure [dbo].[uspValidateDispenseFiles]    Script Date: 27-11-2018 15:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --     EXEC [uspValidateDispenseFiles]  '2018-10-05 14:10:31.000','BOMH','Uploaded','11212121', 'SA'
 
CREATE PROCEDURE [dbo].[uspValidateDispenseFiles]  

@datafor_date_time varchar(100) ,@bankcode nvarchar(10), @projectid varchar(50),@region varchar(50),@recordstatus varchar(50),@referenceid varchar(50),@systemUser varchar(50),@debug BIT = 0
AS
BEGIN
	BEGIN TRY
			DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @Result varchar(100);
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
			DECLARE @outputVal VARCHAR(50)

			SET @Cmd = CASE 

			                WHEN @bankcode = 'BOMH' 
							THEN 'EXEC usp_DispenseValidationBOMH ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+', @Result OUTPUT ;'
							 
							 WHEN @bankcode = 'SBI' 
							THEN 'EXEC usp_DispenseValidationSBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+', @Result OUTPUT ;'
					     	
							 WHEN @bankcode = 'CAB' 
							THEN 'EXEC usp_DispenseValidationCAB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' +','+ ''''+@region+'''' +  ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+', @Result OUTPUT ;'
					     	
							 WHEN @bankcode = 'RSBL' 
							THEN 'EXEC usp_DispenseValidationRSBL ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' +','+ ''''+@region+'''' +  ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+', @Result OUTPUT ;'
					     	
							 WHEN @bankcode = 'UBI' 
							THEN 'EXEC usp_DispenseValidationUBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+ ''''+@region+'''' + ','+ ''''+@recordstatus+'''' + ','+ ''''+@referenceid+''''+ ','+''''+@systemUser+''''+','+ cast (@debug  as varchar(1))+', @Result OUTPUT ;'
					     	
							END
			  		
			EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 

			IF(@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence from app_config_param where category = 'CBR_Validation' and sub_category = 'Error')
			END


			SET @successVal =
			              (
							SELECT sequence from  [dbo].[app_config_param]
							where category = 'File Operation' and sub_category = 'Data Validation Successful'
							)
                            
            SET @failedVal =
			              (
							SELECT sequence from  [dbo].[app_config_param]
							where  category = 'File Operation' and sub_category = 'Data Validation Failed'
							)

            
            IF(@Result =@successVal)
				BEGIN	
				
					SELECT @Result;
					
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
						(
							@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
						)
						SELECT @ErrorNumber;
				END
			 END CATCH
END
GO
