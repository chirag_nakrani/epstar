/****** Object:  StoredProcedure [dbo].[uspConsolidateCbrFiles]    Script Date: 27-11-2018 15:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 


ALTER PROCEDURE [dbo].[uspConsolidateCbrFiles] 
@datafor_date_time varchar(50) ,@bankcode nvarchar(10),@projectid varchar(50),@region varchar(50), @referenceid varchar(50),@systemUser varchar(50)
AS
BEGIN
	BEGIN TRY
		BEGIN TRAN
			DECLARE @Cmd NVARCHAR(MAX);
			DECLARE @Result varchar(100);
			DECLARE @out varchar(100);
			DECLARE @successVal varchar(30);
			DECLARE @failedVal varchar(30);
			DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE());	
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Started',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			--Execute the query according to the Bank code
			SET @Cmd = CASE 
							WHEN @bankcode = 'ALB' 
							THEN 'EXEC uspConsolidateCBR_ALB '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
						    WHEN @bankcode = 'BOMH' 
							THEN 'EXEC uspConsolidateCBR_BOMH ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'BOI' 
							THEN 'EXEC uspConsolidateCBR_BOI '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'BOB' 																														 
							THEN 'EXEC uspConsolidateCBR_BOB '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'CORP' 
							THEN 'EXEC uspConsolidateCBR_CORP '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'CAB' 
							THEN 'EXEC uspConsolidateCBR_CAB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'CBI' 																														  
							THEN 'EXEC uspConsolidateCBR_CBI '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'DEB' 																													 
							THEN 'EXEC uspConsolidateCBR_DENA ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'IDBI' 																													
							THEN 'EXEC uspConsolidateCBR_IDBI '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'IOB' 																														
							THEN 'EXEC uspConsolidateCBR_IOB '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'LVB' 																														 
							THEN 'EXEC uspConsolidateCBR_LVB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'PSB' 
							THEN 'EXEC uspConsolidateCBR_PSB ''' +  cast(@datafor_date_time as varchar(50))+ '''' +  ','+ ''''+@projectid+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'RSBL' 
							THEN 'EXEC uspConsolidateCBR_RSBL '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'SBI' 
							THEN 'EXEC uspConsolidateCBR_SBI ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'UBI' 
							THEN 'EXEC uspConsolidateCBR_UBI '''  +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'UCO' 																														
							THEN 'EXEC uspConsolidateCBR_UCO ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+ ''''+@projectid+'''' + ','+''''+@region+'''' + ','+''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
							WHEN @bankcode = 'VJB' 																														
							THEN 'EXEC uspConsolidateCBR_VJB ''' +  cast(@datafor_date_time as varchar(50))+ '''' + ','+''''+@projectid+'''' + ','+ ''''+@referenceid+''''+ ','+ ''''+@systemUser+''''+ ', @Result OUTPUT ;'
						END

			EXECUTE  sp_executesql  @Cmd ,N' @Result varchar(100) Output ', @Result output 


		IF(@Result is NULL)
			BEGIN
				SET @Result = (SELECT sequence from app_config_param where category = 'CBR_Validation' and sub_category = 'Error')
			END
------ Fetching sequence from table for success and error

			SET @successVal =(
							SELECT Sequence from  [dbo].[app_config_param] where 
							category = 'File Operation' AND sub_category = 'Consolidation Successful'
							)
                            
            SET @failedVal =(
							SELECT Sequence from  [dbo].[app_config_param] where 
							category = 'File Operation' AND sub_category = 'Consolidation Failed'
							)

---	If Consolidation is successfull then proceed with the status update change

            IF(@Result = @successVal)
				BEGIN	
					DECLARE @tableName VARCHAR(30) = 'dbo.data_update_log'
					DECLARE @ForStatus VARCHAR(15) = 'Approved'
					DECLARE @ToStatus VARCHAR(15) = 'Active'

		--- Calling Procedure to Update the status          

					DECLARE @SetWhereClause VARCHAR(MAX) = ' bank_code = ''' + 
													@bankcode + ''' and datafor_date_time = ''' + 
													cast(@datafor_date_time as varchar(50)) +  ''' and data_for_type = ''CBR'
													+ ''' and project_id = ''' + @projectid + ''' and region='''+@region+''''
				
					EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,
					@ToStatus,@timestamp_date,@systemUser,
					@referenceid,@SetWhereClause,@out OUTPUT
				--	select @out					
					SELECT @Result;
					
				END
				ELSE 
				BEGIN
					SELECT @Result;
				END
 		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of Stored Procedure Completed',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

		COMMIT TRAN;
	END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = ERROR_PROCEDURE();
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = GETDATE();
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						SELECT @ErrorNumber;
						
						--IF NOT EXISTS( Select 1 from app_config_param where sequence = @out)
						--BEGIN
						--	INSERT INTO app_config_param values ('Error','Failed',@out,'Failed ' + @ErrorMessage)
						--END
						--ELSE
						--BEGIN
						--	UPDATE  app_config_param
						--	SET value = @ErrorMessage
						--	WHERE sequence = @out
						--END
				END
			 END CATCH
END


GO
