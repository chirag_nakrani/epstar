/****** Object:  StoredProcedure [dbo].[uspConsolidateCDR_RSBL]    Script Date: 23-01-2019 13:24:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

-- consolidation of Dispense 
ALTER PROCEDURE [dbo].[uspConsolidateCDR_RSBL] 
   (@datafor_date_time varchar(100),
    @projectid varchar(50),
   @referenceid varchar(50),
   @systemUser varchar(50),
   @out varchar(200) OUTPUT)  -- added parameter @out

AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @errorNumber int;
DECLARE @current_datetime_stmp datetime = GETDATE()
DECLARE @rowcount int ;
--DECLARE @out varchar(200);

		IF EXISTS(
			Select 1 as ColumnName
			FROM data_update_log
			WHERE datafor_date_time = @datafor_date_time AND 
					bank_code = 'RSBL' AND 
					data_for_type = 'Dispense' and 
					record_status = 'Approved' and
					project_id = @projectid          --chg
			) 
		BEGIN			--- Check for approved status in DUL

			IF EXISTS   (						
						SELECT 1
						FROM data_update_log
						WHERE datafor_date_time = @datafor_date_time AND 
						 bank_code = 'RSBL' AND 
					data_for_type = 'Dispense' and 
					record_status = 'Active'
					and
					project_id = @projectid          --chg
				)	
				BEGIN				--- Check Active status in DUL

					DECLARE @ForStatus VARCHAR(15) = 'Active'
					DECLARE @ToStatus VARCHAR(15) = 'Deleted'
					DECLARE @data_for_type varchar(30) = 'Dispense'

					--IF ('RSBL'='RSBL')
					--	BEGIN	--- For RSBL bank (START)
							IF EXISTS
							(
								Select 1 as ColumnName
								FROM [cash_dispense_file_RSBL]
								WHERE datafor_date_time = @datafor_date_time AND  
										     record_status = 'Active'
											 and
					project_id = @projectid          --chg
							) 
							BEGIN						-----Check active in bankwise file
								IF EXISTS(
										Select 1 as ColumnName
										FROM [cash_dispense_register]
										WHERE
										 datafor_date_time =@datafor_date_time AND 
											bank_name = 'RSBL'  AND 
												record_status = 'Active'
																	 and
					                               project_id = @projectid          --chg
										)
										BEGIN			---- Check Cash Balance Register Table for Active Status (START)	

										-- Set status of bankwise dispense table  for that date
											DECLARE @SetWhere_CDR_bankWise VARCHAR(MAX) =    ' datafor_date_time = ''' +       --chgevery projectid
													@datafor_date_time  + ''' and project_id = ''' + @projectid + ''''
											EXEC dbo.[uspSetStatus] 'cash_dispense_file_RSBL',	@ForStatus,@ToStatus,@current_datetime_stmp,
											@systemUser,@referenceid,@SetWhere_CDR_bankWise,@out OUTPUT
											

											-- update status in  consolidated dispense table  cash_dispense_resgister
											DECLARE @SetWhere_CDR VARCHAR(MAX) = ' bank_name = ''' + 
														'RSBL' +
														 ''' and datafor_date_time = ''' + 
														@datafor_date_time  +
													    ''' and project_id = ''' + @projectid + ''''
							
											EXEC dbo.[uspSetStatus] 'cash_dispense_register',@ForStatus,@ToStatus,@current_datetime_stmp,
											@systemUser,@referenceid,
											@SetWhere_CDR,
											@out OUTPUT
										
										 -- update status of Data Update Log  for that bank /date 
											DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
														'RSBL' + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @data_for_type + ''' and project_id = ''' + @projectid + ''''


											EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@ForStatus,@ToStatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
											
											-- Set status in bank wise table as Approved
											DECLARE @SetWhere_CDR_bankWise1 VARCHAR(MAX) =' datafor_date_time = ''' +									------ Changing status from approved to active
													@datafor_date_time   + ''' and project_id = ''' + @projectid + ''' and  record_status = ''Approved'''
											
											EXEC dbo.[uspSetStatus] 'cash_dispense_file_RSBL','Approved','Active',
																					@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CDR_bankWise1,@out OUTPUT

											---------inserting data after status update for revision
												INSERT INTO 
													cash_dispense_register ( 
														datafor_date_time,
														[atm_id] , 
														[bank_name] ,
														[total_dispense_amount] ,
														record_status,
														project_id,        --chg
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id
													)
											
													Select 
													       datafor_date_time,
													        atm_id,
															'RSBL',	
															amount_dispensed  ,
															'Active',
																	project_id,        --chg
															created_on,
															created_by,
															created_reference_id,
															approved_on,
															approved_by,
															approved_reference_id,
															modified_on,
															modified_by,
															modified_reference_id   
														from  
														cash_dispense_file_RSBL
														where datafor_date_time= @datafor_date_time 
														and record_status = 'Active'
														and project_id = @projectid      

														/*******Check for successful execution of inserting records*******/
														IF EXISTS (
																	SELECT 1 from dbo.cash_dispense_register 
																			where	datafor_date_time = @datafor_date_time 
																					and bank_name = 'RSBL' 
																					and record_status = 'Active'
																					and project_id = @projectid      
																	)
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'  --chg
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END

													


										END				---- Check Cash Dispense Register Table for Active Status (END)

							END
							ELSE		------	BOMH ELSE
							BEGIN
								RAISERROR(50050,16,1)		
										--ELSE			---- Check Cash Dispense Register Table ELSE
										--BEGIN 
										--END			

									----- RSBL Active Check Start(END)
							--ELSE		------	RSBL ELSE
							--BEGIN
							--END
				
						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN
					DECLARE @SetWhere_CDR_bankWise2 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
							@datafor_date_time   + ''' and project_id = ''' + @projectid + ''' and  record_status = ''Approved'''
											
					EXEC dbo.[uspSetStatus] 'cash_dispense_file_RSBL','Approved','Active',
					@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CDR_bankWise2,@out OUTPUT
						INSERT INTO 
						cash_dispense_register ( 
														datafor_date_time,
														[atm_id] , 
														[bank_name] ,
														[total_dispense_amount] ,
														project_id,
														record_status,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id
						)
											
					                   	Select datafor_date_time, 						 
													        atm_id,
															'RSBL',	
															amount_dispensed  ,
															project_id,
															'Active',
															created_on,
															created_by,
															created_reference_id,
															approved_on,
															approved_by,
															approved_reference_id ,
															modified_on,
															modified_by,
															modified_reference_id 
							from  
							cash_dispense_file_RSBL
							where datafor_date_time= @datafor_date_time 
							and record_status = 'Active'
							and project_id = @projectid


						IF EXISTS (SELECT 1 from dbo.cash_dispense_register where
														 datafor_date_time = @datafor_date_time and  bank_name = 'RSBL' 
														 and record_status = 'Active' and project_id = @projectid
									)
						BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'      --chg
																		) 
														END
							ELSE
						BEGIN
															RAISERROR (50003,16,1)
														END
						 
				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
		RAISERROR(50010,16,1)
END
