				update MetaDataTable
				set IsRequired = 'False'
				where datafor = 'CBR' and bankcode = 'LVB'
				and columnHeaders in ('f_tray','f_count','g_tray','g_count','h_tray','h_count','total_amt')
			


				insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name)
				values ('CBR','LVB','atm_type','True','atm_type','ATM Type','cash_balance_file_lvb')

				
				insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name)
				values ('CBR','LVB','AVAIL_100','True','availability_100','Availability 100','cash_balance_file_lvb')

				
				insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name)
				values ('CBR','LVB','DENOMIN_100','True','denomination_100','Denomination 100','cash_balance_file_lvb')

				
				insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name)
				values ('CBR','LVB','AVAIL_500','True','availability_500','Availability 500','cash_balance_file_lvb')


				insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name)
				values ('CBR','LVB','DENOMIN_500','True','denomination_500','Denomination 500','cash_balance_file_lvb')

				
				insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name)
				values ('CBR','LVB','AVAIL_2000','True','availability_2000','Availability 2000','cash_balance_file_lvb')


				insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name)
				values ('CBR','LVB','DENOMIN_2000','True','denomination_2000','Denomination 2000','cash_balance_file_lvb')

				
				insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name)
				values ('CBR','LVB','TOTAL','True','total_amt','Total Amount','cash_balance_file_lvb')