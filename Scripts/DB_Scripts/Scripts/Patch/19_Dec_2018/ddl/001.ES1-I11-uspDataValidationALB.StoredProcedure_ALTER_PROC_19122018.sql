/****** Object:  StoredProcedure [dbo].[uspDataValidationALB]    Script Date: 04-12-2018 20:30:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[uspDataValidationALB]
( 
    @datafor_date_time varchar(50) ,@projectid varchar(50),@ForRecordstatus varchar(50),@referenceid varchar(50),@systemUser varchar(50),@Debug BIT = 0,@outputVal VARCHAR(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

    SET XACT_ABORT ON;
    SET NOCOUNT ON;
    DECLARE @CountNull int 
    DECLARE @CountTotal  int 
    DECLARE @countCalculated  int
    DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
    DECLARE @out varchar(50)
    DECLARE @errorcode nvarchar(max)
    

    IF (@Debug = 1)
    BEGIN
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
        PRINT '-- Execution of [dbo].[uspDataValidationALB] Started.';
        PRINT '-- -----------------------------------------------------------------------------------------------------------------';
    END;

    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
    values ('Execution of [dbo].[uspDataValidationALB] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


------------------ Check if file is present in Data Update Log---------------

    IF EXISTS( Select 1 as ColumnName
                FROM [dbo].[data_update_log] WITH (NOLOCK)
                WHERE [datafor_date_time] = @datafor_date_time AND 
                      bank_code = 'ALB' AND 
                      data_for_type = 'CBR' and 
                      record_status = @ForRecordstatus AND
                      project_id = @projectid
                )
                
        BEGIN

            IF (@Debug = 1)
            BEGIN
                PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                PRINT '-- Checking in Data Update Log Table ';
                PRINT '-- -----------------------------------------------------------------------------------------------------------------';
            END;
            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
             values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

            
------------------ Check if file is present in Bank Wise File Table ----------------------

            IF EXISTS( Select 1 as ColumnName
                        FROM [dbo].[cash_balance_file_alb] WITH (NOLOCK)
                        WHERE [datafor_date_time] = @datafor_date_time AND 
                              record_status = @ForRecordstatus AND
                              project_id = @projectid 
                )
                
                BEGIN
                    IF (@Debug = 1)
                    BEGIN
                        PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                        PRINT '-- Checking in [dbo].[cash_balance_file_alb]  ';
                        PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                    END;
                    INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
                    values ('Checking in [dbo].[cash_balance_file_alb]', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
---------------- Select total count of records present in ALB file for particular date and status------------
                
                SET @CountTotal = (    
                                    SELECT count(1) 
                                    FROM [dbo].[cash_balance_file_alb] WITH (NOLOCK)
                                    WHERE [datafor_date_time] = @datafor_date_time 
                                          and record_status = @ForRecordstatus 
                                          AND project_id = @projectid 
                    
                                   ) 

--------------------- Creating temporary table to process multiple update operations ---------------------------
    
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Creating Temporary Table and storing data ';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
                values ('Creating Temporary Table and storing data', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

                SELECT * 
                INTO #temp_cash_balance_file_alb 
                FROM [dbo].[cash_balance_file_alb] WITH (NOLOCK)
                WHERE datafor_date_time= @datafor_date_time
                      AND record_status = @ForRecordstatus  
                      AND project_id = @projectid

            
----------------------------------------Validation 1--------------------------        
--                            Check For ATM status in Master Data                --
------------------------------------------------------------------------------
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Validation for AMS master Started';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;

                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
                 values ('Validation for AMS master Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


                UPDATE #temp_cash_balance_file_alb 
                SET     error_code =  
                        CASE 
                            WHEN (    termid IN (
                                                    SELECT atm_id from 
                                                    atm_master where site_status in ('Live','Active')
                                                    and is_deleted is null or is_deleted = 'No'
                                                    and bank_code = 'ALB' and project_id = @projectid
                                                 )
                                 )
                            THEN NULL
                            ELSE
                                (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'atm_not_live')
                        END    
                    WHERE datafor_date_time= @datafor_date_time
                    AND record_status = @ForRecordstatus  
                    AND project_id = @projectid

        
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Validation for AMS master Completed ';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
                 values ('Validation for AMS master Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
                    
----------------------------------------Validation 3----------------------------    
------            1.switch balance = 0            
------            2.switch balance < 0
------            3.switch balance is in decimal values
------            4.denomination wise value does not match with the available data
--------------------------------------------------------------------------------
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Validation for Switch balance check started';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
                 values ('Validation for Switch balance check started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
    
                UPDATE #temp_cash_balance_file_alb
                SET     error_code =  
                        CASE 
                            WHEN    currbalcass1 + ccurrbalcass2 + currbalcass3 + currbalcass4 = 0
                                OR  currbalcass1 + ccurrbalcass2 + currbalcass3 + currbalcass4 < 0
                                OR  currbalcass1 + ccurrbalcass2 + currbalcass3 + currbalcass4 <> FLOOR(currbalcass1 + ccurrbalcass2 + currbalcass3 + currbalcass4)
                                OR    (currbalcass1 % 200 <> 0) OR (ccurrbalcass2 % 100 <> 0) OR (currbalcass3 % 500 <> 0) OR (currbalcass4 % 2000 <> 0) 
                            THEN    
                                CASE
                                    WHEN (error_code IS NULL)
                                    THEN    
                                        (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'numeric_validation')
                                     ELSE 
                                        CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'numeric_validation')) as varchar(max))
                                END
                            ELSE error_code
                   END        
                         WHERE datafor_date_time= @datafor_date_time
                        AND record_status = @ForRecordstatus  
                        AND project_id = @projectid
                                        
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Validation for Switch banalnce check Completed';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;    
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
                values ('Validation for Switch balance check Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
    
-------------------------------------------Validation 4 ---------------------------------        
------            1.switch balance > cassette capacity            
------            2.switch balance > Bank Limit (In case of UBI and CBI only)
------            3.switch balance > Insurance Limit
-----------------------------------------------------------------------------------------
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Validation for Cassette Capacity, Insurance Limit, Bank Limit check started';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
                 values ('Validation for Cassette Capacity, Insurance Limit, Bank Limit check started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
                    
                UPDATE #temp_cash_balance_file_alb                        
                SET     error_code =  
                        CASE 
                            WHEN    currbalcass1 + ccurrbalcass2 + currbalcass3 + currbalcass4 >
                                    (    
                                        select (COALESCE(cc.cassette_50_count, clm.cassette_50_count,0) * COALESCE(bc.capacity_50,0)  *50)  +
                                         (COALESCE(cc.cassette_100_count, clm.cassette_100_count,0)     * COALESCE(bc.capacity_100,0) *100) +
                                         (COALESCE(cc.cassette_200_count, clm.cassette_200_count,0)     * COALESCE(bc.capacity_200,0) *200) +    
                                         (COALESCE(cc.cassette_500_count, clm.cassette_500_count,0)     * COALESCE(bc.capacity_500,0) *500) +
                                         (COALESCE(cc.cassette_2000_count, clm.cassette_2000_count,0)   * COALESCE(bc.capacity_2000,0)*2000) 
                                         AS total_cassette_capacity
                                         FROM 
                                              atm_master AS  mast
                                              INNER JOIN     ATM_Config_limits clm
                                              ON            mast.atm_id = clm.atm_id  AND
                                                            mast.site_code = clm.site_code AND
                                                            clm.record_status = 'Active'                              
                                              LEFT JOIN      
                                                   (
                                                   SELECT 
                                                        project_id,
                                                        bank_code, 
                                                        site_code,
                                                        atm_id, 
                                                        cassette_50_count, 
                                                        cassette_100_count, 
                                                        cassette_200_count,
                                                        cassette_500_count,
                                                        cassette_2000_count 
                                                    FROM Modify_cassette_pre_config 
                                                    WHERE @timestamp_date between from_date AND to_date
                                                    AND record_status = 'Active'
                                                   )cc
                                              ON clm.site_code = cc.site_code AND 
                                                 clm.atm_id = cc.atm_id
                                                 LEFT JOIN Brand_Bill_Capacity bc ON 
                                                           bc.brand_code = mast.brand 
                                                           AND bc.record_status = 'Active'
                                                  WHERE mast.record_status = 'Active'
                                                  and mast.bank_code = 'ALB'
                                                  AND mast.project_id = @projectid 
                                                  AND mast.atm_id = #temp_cash_balance_file_alb.termid                                            
                                        )
                                        OR 
                                        currbalcass1 + ccurrbalcass2 + currbalcass3 + currbalcass4 >
                                        (
                                            CASE 
                                                WHEN ( SELECT insurance_limit from ATM_Config_limits atm where atm.atm_id = #temp_cash_balance_file_alb.termid and bank_code = 'CBI' and project_id = @projectid ) IS NOT NULL OR ( SELECT insurance_limit from ATM_Config_limits atm where atm.atm_id = #temp_cash_balance_file_alb.termid and bank_code = 'CBI' and project_id = @projectid ) <> 0
                                                THEN
                                                (SELECT insurance_limit from ATM_Config_limits atm where atm.atm_id = #temp_cash_balance_file_alb.termid and bank_code = 'CBI' and project_id = @projectid)
                                                END
                                        )
                            THEN    
                                CASE
                                    WHEN (error_code IS NULL)
                                    THEN    
                                        (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'exceeding_limits')
                                     ELSE 
                                        CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'exceeding_limits')) as varchar(max))
                                END
                            ELSE error_code
                  END        
                       WHERE datafor_date_time= @datafor_date_time
                        AND record_status = @ForRecordstatus  
                        AND project_id = @projectid

                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Validation for Cassette Capacity, Insurance Limit, Bank Limit check Completed';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
                 values ('Validation for Cassette Capacity, Insurance Limit, Bank Limit check Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)        
 -----------Updating is_valid_record column as Yes or No-------------

            UPDATE #temp_cash_balance_file_alb
            SET    is_valid_record = 
                    CASE 
                        WHEN error_code IS NULL
                        THEN 'Yes'
                        ELSE 'No'
                    END        
            
            IF (@Debug = 1)
            BEGIN
                PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                PRINT '-- Is Valid Record Field Updated';
                PRINT '-- -----------------------------------------------------------------------------------------------------------------';
            END;            
            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
             values ('Is Valid Record Field Updated', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
-----------------Checking count for valid records in updated temporary table -----------------------------

            SET @countCalculated = (
                                    SELECT count(1) 
                                    FROM #temp_cash_balance_file_alb  
                                    WHERE is_valid_record = 'Yes' 
                                    )

-----------------Comparing both the counts ---------------------------------------------------------

            IF(@countCalculated != @CountTotal AND @countCalculated > 0)
                BEGIN
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Count Mismatch (Partial Valid File)';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
            
                SET @outputVal = (
                                        SELECT                                                                                                                                                        
                                        [sequence] from 
                                        [dbo].[app_config_param]                                                
                                        where category = 'Exception' and sub_category = 'Partial Valid'
                                     )                
                END
            ELSE IF (@countCalculated = @CountTotal)
                BEGIN 
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Count Matched (Valid File)';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                SET @outputVal = (
                                SELECT sequence from  [dbo].[app_config_param]
                                where  category = 'File Operation' and sub_category = 'Data Validation Successful'
                            )                                                
                END

            ELSE
                BEGIN
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- No valid record found in table';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                
                SET @outputVal = (
                                        SELECT                                                                                                                                                        
                                        [sequence] from 
                                        [dbo].[app_config_param]                                                
                                        where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
                                     )                
            END

------------------------Inserting distinct error code in new temporary table. Also splitting with , ------------------------

            IF OBJECT_ID('tempdb..##tempbankfile') IS NOT NULL
            BEGIN
                DROP TABLE #tempbankfile
            END
            ELSE
            BEGIN
                SELECT DISTINCT VALUE 
                INTO #tempbankfile 
                FROM  #temp_cash_balance_file_alb
                CROSS APPLY STRING_SPLIT(error_code, ',') 
                
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Temporary Table Created for storing error code';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
                INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
                 values ('Temporary Table Created for storing error code', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

--------------------- Creating one single error code string with , seperated delimiter---------------------------------------                
                SET @errorcode = (
                                SELECT 
                                    Stuff((
                                        SELECT N', ' + VALUE FROM #tempbankfile FOR XML PATH(''),TYPE)
                                        .value('text()[1]','nvarchar(max)'),1,2,N''
                                        )
                                )
                IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Error code String Created';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;            
            END            
            
            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
             values ('Transaction Started.... Updating columns in bank wise file and data update log', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)        
            BEGIN TRAN

            IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Transaction Started.... Updating columns in bank wise file';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;    


------------------------- Updating columns in bank wise file using temporary table------------------------------------------

                UPDATE ALB 
                SET ALB.is_valid_record =  b.is_valid_record,
                    ALB.error_code = b.error_code,
                    record_status = 
                        CASE 
                            WHEN b.is_valid_record = 'Yes' 
                            THEN 'Approved'
                            WHEN b.is_valid_record = 'No' 
                            THEN 'Uploaded'
                        END,        
                    modified_on =@timestamp_date,
                    modified_by = @systemUser,
                    modified_reference_id = @referenceid
                FROM [cash_balance_file_alb] ALB
                    INNER JOIN #temp_cash_balance_file_alb b
                    on b.termid = ALB.termid
                    AND ALB.project_id = @projectid
                    AND ALB.[datafor_date_time] = @datafor_date_time 
                    AND ALB.record_status = @ForRecordstatus
            
            IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Columns has been updated in bank wise file';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;
        

            IF (@Debug = 1)
                BEGIN
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                    PRINT '-- Updating columns in data update log ';
                    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
                END;

------------------------- Updating columns in Data Update Log table------------------------------------------
                
                
                UPDATE dbo.data_update_log
                SET is_valid_file =
                    CASE    
                            WHEN @outputVal = 50009 
                            THEN  1
                            WHEN @outputVal = 50001
                            THEN 0
                            WHEN @outputVal = 10001
                            THEN 0
                    END,
                    validation_code = (SELECT @errorcode),
                    record_status = 
                    CASE    
                            WHEN @outputVal = 50009 
                            THEN 'Approved'
                            WHEN @outputVal = 50001
                            THEN 'Approved'
                            WHEN @outputVal = 10001
                            THEN 'Uploaded'
                    END,    
                    modified_on =@timestamp_date,
                    modified_by = @systemUser,
                    modified_reference_id = @referenceid
                WHERE [datafor_date_time] =  @datafor_date_time
                    AND record_status = @ForRecordstatus 
                    AND bank_code = 'ALB' 
                    AND    data_for_type = 'CBR'
                    AND project_id = @projectid

            COMMIT TRAN;

            IF (@Debug = 1)
            BEGIN
                PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                PRINT '-- Transaction Completed....Column has been updated in data update log ';
                PRINT '-- -----------------------------------------------------------------------------------------------------------------';
            END;

            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
             values ('Transaction Completed....Column has been updated in data update log and bank wise file', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


            IF (@Debug = 1)
            BEGIN
                PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
                PRINT '-- Execution of [dbo].[uspDataValidationALB] Completed.';
                PRINT '-- -----------------------------------------------------------------------------------------------------------------';
            END;

            INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by)
             values ('Execution of [dbo].[uspDataValidationALB] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


        END
            ELSE
            BEGIN
                  RAISERROR (50002, 16,1);
            END
        END
    ELSE
        BEGIN
            RAISERROR (    50004, 16,1);
        END
END
