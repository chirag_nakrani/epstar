Delete from metadatatable where datafor = 'CBR' and bankcode = 'UBI' and columnHeaders = 'atm_id'
Delete from metadatatable where datafor = 'CBR' and bankcode = 'UCO' and columnHeaders = 'atm_id'

INSERT [dbo].[MetaDataTable] ([datafor], [bankcode], [columnHeaders], [IsRequired], [table_column], [display_column_name], [table_name]) VALUES (N'CBR', N'UBI', N'term_id', N'True', N'atm_id', N'Term Id', N'cash_balance_file_ubi')
INSERT [dbo].[MetaDataTable] ([datafor], [bankcode], [columnHeaders], [IsRequired], [table_column], [display_column_name], [table_name]) VALUES (N'CBR', N'UCO', N'atmid', N'True', N'atm_id', N'Atm Id', N'cash_balance_file_uco')