/****** Object:  StoredProcedure [dbo].[uspConsolidateCDR_SBI]    Script Date: 27-11-2018 15:11:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

-- consolidation of Dispense 
ALTER PROCEDURE [dbo].[uspConsolidateCDR_SBI] 
   (@datafor_date_time varchar(100),
    @projectid varchar(50),
   @referenceid varchar(50),
   @systemUser varchar(50),
   @out varchar(200) OUTPUT)  -- added parameter @out

AS 
BEGIN
DECLARE @recordStatus varchar(50);
DECLARE @successmsg varchar(max);
DECLARE @errorsmsg varchar(max);
DECLARE @count1 int;
DECLARE @errorNumber int;
DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
DECLARE @rowcount int ;
DECLARE @bankcode nvarchar(10) = 'SBI'
DECLARE @datafor nvarchar(10) = 'Dispense'
DECLARE @activestatus nvarchar(20) = 'Active'
DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
DECLARE @approvedstatus nvarchar(20) = 'Approved'
--DECLARE @out varchar(200);

		IF EXISTS(
			Select 1 as ColumnName
			FROM data_update_log
			WHERE datafor_date_time = @datafor_date_time AND 
					bank_code = @bankcode AND 
					data_for_type = @datafor and 
					record_status = @approvedstatus and
					project_id = @projectid          --chg
					AND created_reference_id = @referenceid
			) 
		BEGIN			--- Check for approved status in DUL

			IF EXISTS   (						
						SELECT 1
						FROM data_update_log
						WHERE datafor_date_time = @datafor_date_time AND 
						 bank_code = @bankcode AND 
					data_for_type = @datafor and 
					record_status = @activestatus
					and
					project_id = @projectid          --chg
				)	
				BEGIN				--- Check Active status in DUL

					--IF ('SBI'='SBI')
					--	BEGIN	--- For SBI bank_name (START)
							IF EXISTS
							(
								Select 1 as ColumnName
								FROM [cash_dispense_file_SBI]
								WHERE datafor_date_time = @datafor_date_time AND  
										     record_status = @activestatus
											 and
											project_id = @projectid          --chg
							) 
							BEGIN						-----Check active in bankwise file
								IF EXISTS(
										Select 1 as ColumnName
										FROM [cash_dispense_register]
										WHERE
										 datafor_date_time =@datafor_date_time AND 
											bank_name = @bankcode  AND 
												record_status = @activestatus
																	 and
					                               project_id = @projectid          --chg
										)
										BEGIN			---- Check Cash Balance Register Table for Active Status (START)	

										-- Set status of bankwise dispense table  for that date
											DECLARE @SetWhere_CDR_bankWise VARCHAR(MAX) =    ' datafor_date_time = ''' +       --chgevery projectid
													@datafor_date_time  + ''' and project_id = ''' + @projectid + ''''
											EXEC dbo.[uspSetStatus] 'cash_dispense_file_SBI',	@activestatus,@deletedstatus,@current_datetime_stmp,
											@systemUser,@referenceid,@SetWhere_CDR_bankWise,@out OUTPUT
											

											-- update status in  consolidated dispense table  cash_dispense_resgister
											DECLARE @SetWhere_CDR VARCHAR(MAX) = ' bank_name = ''' + 
														@bankcode +
														 ''' and datafor_date_time = ''' + 
														@datafor_date_time  +
													    ''' and project_id = ''' + @projectid + ''''
							
											EXEC dbo.[uspSetStatus] 'dbo.cash_dispense_register',@activestatus,@deletedstatus,@current_datetime_stmp,
											@systemUser,@referenceid,
											@SetWhere_CDR,
											@out OUTPUT
										
										 -- update status of Data Update Log  for that bank_name /date 
											DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor + ''' and project_id = ''' + @projectid + ''''


											EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT
											
											-- Set status in bank_name wise table as Approved
											DECLARE @SetWhere_CDR_bankWise1 VARCHAR(MAX) =' datafor_date_time = ''' +									------ Changing status from approved to active
													@datafor_date_time   + ''' and project_id = ''' + @projectid + ''' and  created_reference_id = ''' + @referenceid + ''''
											
											EXEC dbo.[uspSetStatus] 'dbo.cash_dispense_file_SBI',@approvedstatus,@activestatus,
																					@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CDR_bankWise1,@out OUTPUT

											---------inserting data after status update for revision
												INSERT INTO 
													cash_dispense_register ( 
														datafor_date_time,
														[atm_id] , 
														[bank_name] ,
														[total_dispense_amount] ,
														record_status,
																project_id,        --chg
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id
													)
											
													Select 
													       datafor_date_time,
													        atm_id,
															@bankcode,	
															cash_withdrawal ,
															@activestatus,
																	project_id,        --chg
															created_on,
															created_by,
															created_reference_id,
															approved_on,
															approved_by,
															approved_reference_id,
															modified_on,
															modified_by,
															modified_reference_id
														from  
														cash_dispense_file_SBI
														where datafor_date_time= @datafor_date_time 
														and record_status = @activestatus
														and project_id = @projectid   
														and created_reference_id = @referenceid    

														/*******Check for successful execution of inserting records*******/
														IF EXISTS (
																	SELECT 1 from dbo.cash_dispense_register 
																			where	datafor_date_time = @datafor_date_time 
																					and bank_name = @bankcode
																					and record_status = @activestatus
																					and project_id = @projectid      
																	)
														BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'  --chg
																		) 
														END
														ELSE
														BEGIN
															RAISERROR (50003,16,1)
														END

													


										END				---- Check Cash Dispense Register Table for Active Status (END)

										
							END
							ELSE		------	BOMH ELSE
							BEGIN
								RAISERROR(50050,16,1)	
										--ELSE			---- Check Cash Dispense Register Table ELSE
										--BEGIN 
										--END			

									----- SBI Active Check Start(END)
							--ELSE		------	SBI ELSE
							--BEGIN
							--END
				
						END
			    END
			ELSE			--- if active status not found in DUL
				BEGIN
					DECLARE @SetWhere_CDR_bankWise2 VARCHAR(MAX) = ' datafor_date_time = ''' +									------ Changing status from approved to active
							@datafor_date_time   + ''' and project_id = ''' + @projectid + ''' and  created_reference_id = ''' + @referenceid + ''''
											
					EXEC dbo.[uspSetStatus] 'cash_dispense_file_SBI',@approvedstatus,@activestatus,
					@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_CDR_bankWise2,@out OUTPUT
						INSERT INTO 
						cash_dispense_register ( 
														datafor_date_time,
														[atm_id] , 
														[bank_name] ,
														[total_dispense_amount] ,
														project_id,
														record_status,
														[created_on],
														[created_by],
														[created_reference_id],
														[approved_on],
														[approved_by],
														[approved_reference_id],
														modified_on,
														modified_by,
														modified_reference_id
						)
											
					                   	Select datafor_date_time, 						 
													        atm_id,
															@bankcode,	
															cash_withdrawal  ,
															project_id,
															@activestatus,
															created_on,
															created_by,
															created_reference_id,
															approved_on,
															approved_by,
															approved_reference_id,
															modified_on,
															modified_by,
															modified_reference_id  
							from  
							cash_dispense_file_SBI
							where datafor_date_time= @datafor_date_time 
							and record_status = @activestatus
							and project_id = @projectid
							and created_reference_id = @referenceid


						IF EXISTS (SELECT 1 from dbo.cash_dispense_register where
														 datafor_date_time = @datafor_date_time and  bank_name = @bankcode
														 and record_status = @activestatus and project_id = @projectid
									)
						BEGIN
															SET @out = (
																		SELECT Sequence from  [dbo].[app_config_param] where 
																		category = 'File Operation' AND sub_category = 'Consolidation Successful'      --chg
																		) 
														END
							ELSE
						BEGIN
															RAISERROR (50003,16,1)
														END
						 
				END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
	RAISERROR(50010,16,1)
END
GO
