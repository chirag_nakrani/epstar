/****** Object:  StoredProcedure [dbo].[usp_validate_C3R_CMIS]    Script Date: 28-11-2018 14:19:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_validate_C3R_CMIS]
( 
	@datafor_date_time varchar(50) ,@ForRecordstatus varchar(50),@systemUser varchar(50),@referenceid varchar(50),@outputVal varchar(50) OUTPUT
)
AS
--- Declaring Local variables to store the temporary values like count
BEGIN

	SET XACT_ABORT ON;
	SET NOCOUNT ON;
	DECLARE @CountNull int 
	DECLARE @CountTotal  int 
	DECLARE @countCalculated  int
	DECLARE @timestamp_date datetime = DATEADD(MI,330,GETUTCDATE())
	--DECLARE @out varchar(50)
	DECLARE @errorcode nvarchar(max)
	--DECLARE @outputVal varchar(max) 

	--SELECT SESSION_ID from sys.dm_exec_sessions; 

		--IF (@Debug = 1)
		--BEGIN
		--	PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
		--	PRINT '-- Execution of [dbo].[[sp_validate_C3R_CMIS] Started.';
		--	PRINT '-- -----------------------------------------------------------------------------------------------------------------';
		--END;

	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[usp_validate_C3R_CMIS] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


------------------ Check if file is present in Data Update Log---------------

	IF EXISTS( Select 1 as ColumnName
				FROM [dbo].[data_update_log] WITH (NOLOCK)
				WHERE [datafor_date_time] = @datafor_date_time AND 
                      data_for_type = 'C3R' and 
                      record_status = @ForRecordstatus AND 
					created_reference_id = @referenceid
				)
				
		BEGIN

			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Checking in Data Update Log Table ';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in Data Update Log Table ', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			
------------------ Check if file is present in Bank Wise File Table ----------------------

			IF EXISTS( Select 1 as ColumnName
						FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
						WHERE [datafor_date_time] = @datafor_date_time AND 
							  record_status = @ForRecordstatus 
							  AND created_reference_id = @referenceid
				)
				
				BEGIN
				--	IF (@Debug = 1)
				--	BEGIN
				--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--	    PRINT '-- Checking in [dbo].[c3r_CMIS]  ';
				--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--	END;
					INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Checking in [dbo].[c3r_CMIS]', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
---------------- Select total count of records present inb file for particular date and status------------
				
				SET @CountTotal = (	
									SELECT count(1) 
									FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
						            WHERE [datafor_date_time] = @datafor_date_time 
										  and record_status = @ForRecordstatus 
										  AND created_reference_id = @referenceid					
								   ) 

--------------------- Creating temporary table to process multiple update operations ---------------------------
	
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Creating Temporary Table and storing data ';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Creating Temporary Table and storing data', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				SELECT * 
				INTO #temp_C3R 
				FROM [dbo].[c3r_CMIS] WITH (NOLOCK)
				WHERE datafor_date_time= @datafor_date_time
					  AND record_status = @ForRecordstatus  
					  AND created_reference_id = @referenceid

			
----------------------------------------Validation 1--------------------------		
--							Check For ATM status in Master Data				--
------------------------------------------------------------------------------
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Validation for AMS master Started';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for C3R ATM id Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

				--select * from #temp_C3R

				UPDATE #temp_C3R 
					SET     error_code =  
								CASE 
								WHEN (sw_closing_total = atmcount_remain_counter_total)
								and (atmcount_remain_counter_total = atmphysical_total_remainingcash_total)
									THEN NULL
										ELSE (SELECT cast(sequence as varchar(50)) FROM app_config_param where sub_category = 'counter_mismatch_c3r' and category = 'C3R validation')
								END		

				--select * from #temp_C3R
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Validation for C3R ATM id Completed ';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for C3R ATM id Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
										  
----------------------------------------Validation 2------------------------------------		
--		           Hopper wise balance should match with the ending balance			  --
----------------------------------------------------------------------------------------	
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Validation for Hopper wise balance should match with the ending balance Started';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				--INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Hopper wise balance should match with the ending balance Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
				
				--UPDATE #temp_C3R 
				--SET     error_code =  
				--	  CASE  
				--	          WHEN vault_balance_100+vault_balance_200+vault_balance_500+vault_balance_2000 <> total_vault_balance 
				--	          THEN 
				--	          CASE
				--	                          WHEN (error_code IS NULL)
				--	                          THEN    
				--	                              (SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Amount Mismatch')
				--	                           ELSE 
				--	                              CAST(CONCAT(error_code,',',(SELECT cast(sequence as varchar(50)) FROM app_config_param where category = 'CBR_Validation' and sub_category = 'Amount Mismatch')) as varchar(max))
				--	                      END
				--	                  ELSE error_code 
				--	END
				--	     WHERE datafor_date_time= @datafor_date_time 
				--	     AND record_status = @ForRecordstatus

				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Validation for Hopper wise balance should match with the ending balance Completed';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				--INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Validation for Hopper wise balance should match with the ending balance Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
		
 -----------Updating is_valid_record column as Yes or No-------------

			UPDATE #temp_C3R
			SET    is_valid_record = 
					CASE 
						WHEN error_code IS NULL
						THEN 'Yes'
						ELSE 'No'
					END		
			
			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Is Valid Record Field Updated';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;			
			--INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Is Valid Record Field Updated', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)
-----------------Checking count for valid records in updated temporary table -----------------------------

			SET @countCalculated = (
									SELECT count(1) 
									FROM #temp_C3R  
									WHERE is_valid_record = 'Yes' 
									)

-----------------Comparing both the counts ---------------------------------------------------------

			IF(@countCalculated != @CountTotal AND @countCalculated > 0)
				BEGIN
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Count Mismatch (Partial Valid File)';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
			
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'Exception' and sub_category = 'Partial Valid'
									)	
							
				END
			ELSE IF (@countCalculated = @CountTotal)
				BEGIN 
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Count Matched (Valid File)';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				SET @outputVal = (
								SELECT sequence from  [dbo].[app_config_param]
								where  category = 'File Operation' and sub_category = 'Data Validation Successful'
							)												
				END

			ELSE
				BEGIN
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- No valid record found in table';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				
				SET @outputVal = (
										SELECT																																						
										[sequence] from 
										[dbo].[app_config_param]												
										where category = 'CBR_operation' and sub_category = 'No_Valid_Record'
									)				
			END

------------------------Inserting distinct error code in new temporary table. Also splitting with , ------------------------

			IF OBJECT_ID('tempdb..#tempbankfile') IS NOT NULL
			BEGIN
				DROP TABLE #tempbankfile
			END
			ELSE
			BEGIN
				SELECT DISTINCT VALUE 
				INTO #tempbankfile 
				FROM  #temp_C3R
				CROSS APPLY STRING_SPLIT(error_code, ',') 
				
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Temporary Table Created for storing error code';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;
				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Temporary Table Created for storing error code', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

--------------------- Creating one single error code string with , seperated delimiter---------------------------------------				
				SET @errorcode = (
								SELECT 
									Stuff((
										SELECT N', ' + VALUE FROM #tempbankfile FOR XML PATH(''),TYPE)
										.value('text()[1]','nvarchar(max)'),1,2,N''
										)
								)
				--IF (@Debug = 1)
				--BEGIN
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
				--    PRINT '-- Error code String Created';
				--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
				--END;			
			END			
			
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Started.... Updating columns in C3R file and data update log', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)		
			BEGIN TRAN

			--IF (@Debug = 1)
			--	BEGIN
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--	    PRINT '-- Transaction Started.... Updating columns in bank wise file';
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--	END;	


------------------------- Updating columns in bank wise file using temporary table------------------------------------------

				UPDATE C3R 
				SET C3R.is_valid_record =  b.is_valid_record,
					C3R.error_code = b.error_code,
					record_status = 
						CASE 
							WHEN b.is_valid_record = 'Yes' 
							THEN 'Approval Pending'
							WHEN b.is_valid_record = 'No' 
							THEN 'Uploaded'
						END,	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				FROM [c3r_CMIS] C3R
					INNER JOIN #temp_C3R b
					on b.atm_id = C3R.atm_id
					AND C3R.[datafor_date_time] = @datafor_date_time 
					AND C3R.record_status = @ForRecordstatus
					AND C3R.created_reference_id = @referenceid


				UPDATE C3R 
				SET record_status = 'Approval Pending',
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				FROM [c3r_VMIS] C3R
					WHERE C3R.[datafor_date_time] = @datafor_date_time 
					AND C3R.record_status = @ForRecordstatus
					AND C3R.created_reference_id = @referenceid
			
			--IF (@Debug = 1)
			--	BEGIN
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--	    PRINT '-- Columns has been updated in bank wise file';
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--	END;
		

			--IF (@Debug = 1)
			--	BEGIN
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--	    PRINT '-- Updating columns in data update log ';
			--	    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--	END;

------------------------- Updating columns in Data Update Log table------------------------------------------
				
				
				UPDATE dbo.data_update_log
				SET is_valid_file =
					CASE	
							WHEN @outputVal = 50009 
							THEN  1
							WHEN @outputVal = 50001
							THEN 0
							WHEN @outputVal = 10001
							THEN 0
					END,
					validation_code = (SELECT @errorcode),
					record_status = 
					CASE	
							WHEN @outputVal = 50009 
							THEN 'Approval Pending'
							WHEN @outputVal = 50001
							THEN 'Approval Pending'
							WHEN @outputVal = 10001
							THEN 'Uploaded'
					END,	
					modified_on =@timestamp_date,
					modified_by = @systemUser,
					modified_reference_id = @referenceid
				WHERE [datafor_date_time] =  @datafor_date_time
					AND record_status = @ForRecordstatus 
					AND	data_for_type = 'C3R'
					and created_reference_id = @referenceid

			COMMIT TRAN;

			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Transaction Completed....Column has been updated in data update log ';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Transaction Completed....Column has been updated in data update log and bank wise file', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)


			--IF (@Debug = 1)
			--BEGIN
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';    
			--    PRINT '-- Execution of [dbo].[[sp_validate_C3R_CMIS] Completed.';
			--    PRINT '-- -----------------------------------------------------------------------------------------------------------------';
			--END;

			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Execution of [dbo].[usp_validate_C3R_CMIS] Completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

			    --DECLARE @tableName VARCHAR(30) = 'dbo.[c3r_CMIS]'
       --         DECLARE @ForStatus VARCHAR(30) = 'Uploaded'
       --         DECLARE @ToStatus VARCHAR(30) =  'Approval Pending'
       --         DECLARE @SetWhereClause VARCHAR(MAX) = '  datafor_date_time = ''' + 
       --                                         cast(@datafor_date_time as varchar(50)) + ''''


       --         EXEC  dbo.[uspSetStatus] @tableName,@ForStatus,@ToStatus,@timestamp_date,@systemUser,@referenceid,@SetWhereClause,@out OUTPUT


       --         DECLARE @tableName_DUL VARCHAR(30) = 'dbo.data_update_log'
       --         DECLARE @ForStatus_DUL VARCHAR(30) = 'Uploaded'
       --         DECLARE @ToStatus_DUL VARCHAR(30) = 'Approval Pending'
       --         DECLARE @SetWhereClause_DUL VARCHAR(MAX) ='datafor_date_time = ''' + 
       --                                             cast(@datafor_date_time as varchar(50)) +  ''' and  data_for_type = ''C3R'''

       --         EXEC  dbo.[uspSetStatus] @tableName_DUL,@ForStatus_DUL,@ToStatus_DUL,@timestamp_date,@systemUser,@referenceid,@SetWhereClause_DUL,@out OUTPUT
                     -- SELECT @outputVal

	
		END
			ELSE
			BEGIN
				  RAISERROR (50002, 16,1);
			END
		END
	ELSE
		BEGIN
		--SELECT SESSION_ID from sys.dm_exec_sessions;  
			RAISERROR (	50004, 16,1);
		END
END


select * from app_config_param

update app_config_param 
set sub_category = 'counter_mismatch_c3r'
where sequence = 70010
