/****** Object:  StoredProcedure [dbo].[usp_indent_revision_request_V2]    Script Date: 2/22/2019 11:08:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- EXEC [usp_indent_revision_request] '2018-11-22','CAB/GOREGAON/Securevalue India Ltd/20190103135022','MOF','CAB',NULL,'Malay','abc223'


--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: Calculation of Indent for the indentdate @dateT
 --- =========================================================================

 -- On failure: E109
 -- On Success: S105
 -- Pick up status for 'Approved'

ALTER PROCEDURE [dbo].[usp_indent_revision_request_V2]
 (		
        @dateT						DATETIME,		
		@active_indent_code			nVARCHAR(max),
		@project_id					nVARCHAR(50),
		@bank_code					nVARCHAR(50),
		@feeder_branch_code			nVARCHAR(50),
		@cur_user					nvarchar(50),
		@created_reference_id	    nvarchar(50)
 )

AS 
BEGIN
	BEGIN TRY
	-- Temp_variable to calculate indent
	--DECLARE @dateT DATETIME = '2019-02-11'

	--DECLARE @dateT DATETIME		= '2018-11-22'							
	--DECLARE @active_indent_code VARCHAR(max) = 'BOMH/MOF_MAH/SITABULDI/LOGICASH/20190209215831'
	--DECLARE @project_id  VARCHAR(50) = 'MOF_MAH'
	--DECLARE @bank_code VARCHAR(50) = 'BOMH'
	--DECLARE @feeder_branch_code VARCHAR(50) = 'SITABULDI'
	--DECLARE @dateT DATETIME = '2019-02-20'
	--DECLARE @cur_user nvarchar(50) = 'SA'
	--DECLARE @created_reference_id nvarchar(50) = 'C10001593654120'
	declare @timestamp_date datetime =  DATEADD(MI,330,GETUTCDATE())
	DECLARE @out varchar(50) 
	DECLARE @procedure_name varchar(100) = OBJECT_NAME(@@PROCID)
	declare @Execution_log_str nvarchar(max)
	DECLARE @outputVal varchar(100)
	declare @feederbranchcode varchar(max)
	
	
	DECLARE @forecast_amt BIGINT
	
	set @Execution_log_str = convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Procedure Started with parameters, Date = ' + CAST(@dateT as nvarchar(50)) 
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Execution of [dbo].[usp_indent_revision] Started', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
	
		--DECLARE @dateT DATETIME
		--SET     @dateT ='2018-11-22'

		DECLARE @MaxDestinationDate DATE
		DECLARE @dispenseformula NVARCHAR(50)
		DECLARE @confidence_factor NVARCHAR(50)
		DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		DECLARE @denomination_wise_round_off_500 int
		DECLARE @denomination_wise_round_off_2000 int
		DECLARE @denomination_wise_round_off_100 INT
		declare @default_avg_dispense int
		
		
		Select	@confidence_factor = confidence_factor
		, @dispenseformula = dispenseformula
		, @denomination_wise_round_off_100 = denomination_wise_round_off_100 
		, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		,@default_avg_dispense = default_average_dispense
		 from system_settings where record_status = 'Active'


		 DROP TABLE IF EXISTS #dist_with_cra
		
		 select * ,
		 concat(bank_code,'/',project_id,'/',feeder_branch_code,'/',cra,'/',format(DATEADD(MI,330,GETUTCDATE()),'yyyyMMddHHmmss')) AS indent_order_number
		 into #dist_with_cra 
		 from distribution_planning_detail_V2 
		 where indent_code = @active_indent_code AND record_status = 'Active'

		 DROP TABLE IF EXISTS #feeder_level_forecast


		 SELECT distinct dist.indent_order_number,master.* 
		 into #feeder_level_forecast
		 FROM  distribution_planning_master_V2 master
		 JOIN  #dist_with_cra dist
		 ON dist.indent_code = master.indent_code
		 AND master.record_status ='Active'
		 AND master.indent_code = @active_indent_code

		--select * from #dist_with_cra
		--select * from #feeder_level_forecast
		--select * from distribution_planning_master
		--return;

	

		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #dist_with_cra completed'
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Insert into #dist_with_cra completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


		 
		
		
		-- select * ,
		-- CONCAT(bank_code,'/',project_id,'/',feeder_branch_code,'/',cra,'/',format(DATEADD(MI,330,GETUTCDATE()),'yyyyMMddHHmmss')) AS indent_order_number
		-- into #feeder_level_forecast
		--from distribution_planning_master 
		-- where indent_code = @active_indent_code AND record_status = 'Active'
		



		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #feeder_level_forecast completed'
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Insert into #feeder_level_forecast completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


		 DROP TABLE IF EXISTS #cash_pre_availability
		 select	  project_id
				, bank_code
				, feeder_branch_code
				, total_amount_available
				, available_100_amount
				, available_200_amount
				, available_500_amount
				, available_2000_amount
				, case when (available_100_amount + available_200_amount + available_500_amount + available_2000_amount) = total_amount_available then
			1 else 0 end as is_deno_wise_cash_available  
		 into #cash_pre_availability 
		 from indent_revision_request where indent_order_number = @active_indent_code and record_status = 'Approved'
		
		
		-- select * from #cash_pre_availability


		 set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #cash_pre_availability completed'
		
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values ('Insert into #cash_pre_availability completed', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))


		 --select * from #cash_pre_availability
		 
		--DECLARE @MaxDestinationDate DATE
		--DECLARE @dispenseformula NVARCHAR(50)
		--DECLARE @confidence_factor NVARCHAR(50)
		--DECLARE @buffer_percentage nvarchar(50)                      -- for additional 20% in 100 and 200 denominations. before rounding code.
		--DECLARE @denomination_wise_round_off_200 int       -- for rounding, replace with 100000
		--DECLARE @denomination_wise_round_off_500 int
		--DECLARE @denomination_wise_round_off_2000 int
		--DECLARE @denomination_wise_round_off_100 INT
		--declare @default_avg_dispense int
		--DECLARE @feederbranchcode varchar(100)
		
		
		--Select
		--@denomination_wise_round_off_100 = denomination_wise_round_off_100 
		--, @denomination_wise_round_off_200 = denomination_wise_round_off_200 
		--, @denomination_wise_round_off_500 = denomination_wise_round_off_500 
		--, @denomination_wise_round_off_2000 = denomination_wise_round_off_2000 
		--,@default_avg_dispense = default_average_dispense
		-- from system_settings where record_status = 'Active'
	--	 	DECLARE @dateT DATETIME = '2019-02-20'
	--DECLARE @cur_user nvarchar(50) = 'SA'
	--DECLARE @created_reference_id nvarchar(50) = 'C10001593654120'

	--DECLARE @project_id  VARCHAR(50) = 'MOF_MAH'
	--DECLARE @bank_code VARCHAR(50) = 'BOMH'


			DROP TABLE IF EXISTS #temp_cash_pre_availability
			
			CREATE TABLE #temp_cash_pre_availability
			(
				project_id		nvarchar(50)
			,	bank_code		nvarchar(50)
			,	atmid		nvarchar(50)
			,	feeder_branch_code nvarchar(100)
			,	total_opening_remaining_available_amount		bigint
			,	opening_remaining_available_amount_100		bigint
			,	opening_remaining_available_amount_200		bigint
			,	opening_remaining_available_amount_500		bigint
			,	opening_remaining_available_amount_2000		bigint
			,	original_total_forecasted_amt		bigint
			,	original_forecasted_amt_100		bigint
			,	original_forecasted_amt_200		bigint
			,	original_forecasted_amt_500		bigint
			,	original_forecasted_amt_2000		bigint
			,	morning_balance_100		bigint
			,	morning_balance_200		bigint
			,	morning_balance_500		bigint
			,	morning_balance_2000		bigint
			,	total_morning_balance		bigint
			,	cassette_100_count_original		bigint
			,	cassette_200_count_original		bigint
			,	cassette_500_count_original		bigint
			,	cassette_2000_count_original		bigint
			,	cassette_100_brand_capacity		bigint
			,	cassette_200_brand_capacity		bigint
			,	cassette_500_brand_capacity		bigint
			,	cassette_2000_brand_capacity		bigint
			,	total_capacity_amount_100		bigint
			,	total_capacity_amount_200		bigint
			,	total_capacity_amount_500		bigint
			,	total_capacity_amount_2000		bigint
			,	denomination_100_max_capacity_percentage		bigint
			,	denomination_200_max_capacity_percentage		bigint
			,	denomination_500_max_capacity_percentage		bigint
			,	denomination_2000_max_capacity_percentage		bigint
			,	max_amt_allowed_100		bigint
			,	max_amt_allowed_200		bigint
			,	max_amt_allowed_500		bigint
			,	max_amt_allowed_2000		bigint
			,	denomination_wise_round_off_100		bigint
			,	denomination_wise_round_off_200		bigint
			,	denomination_wise_round_off_500		bigint
			,	denomination_wise_round_off_2000		bigint
			,	tentative_loading_100		bigint
			,	tentative_loading_200		bigint
			,	tentative_loading_500		bigint
			,	tentative_loading_2000		bigint
			,	rounded_tentative_loading_100		bigint
			,	rounded_tentative_loading_200		bigint
			,	rounded_tentative_loading_500		bigint
			,	rounded_tentative_loading_2000		bigint
			,	deno_100_priority		bigint
			,	deno_200_priority		bigint
			,	deno_500_priority		bigint
			,	deno_2000_priority		bigint
			,	is_deno_wise_cash_available		int
			,	priority_1_is_denomination_100		int
			,	priority_1_is_denomination_200		int
			,	priority_1_is_denomination_500		int
			,	priority_1_is_denomination_2000		int
			,	priority_1_is_remaining_amount_available_100		int
			,	priority_1_is_remaining_amount_available_200		int
			,	priority_1_is_remaining_amount_available_500		int
			,	priority_1_is_remaining_amount_available_2000		int
			,	priority_1_is_remaining_capacity_available_100		int
			,	priority_1_is_remaining_capacity_available_200		int
			,	priority_1_is_remaining_capacity_available_500		int
			,	priority_1_is_remaining_capacity_available_2000		int
			,	priority_1_loading_amount_100		bigint
			,	priority_1_loading_amount_200		bigint
			,	priority_1_loading_amount_500		bigint
			,	priority_1_loading_amount_2000		bigint
			,	priority_2_is_denomination_100		int
			,	priority_2_is_denomination_200		int
			,	priority_2_is_denomination_500		int
			,	priority_2_is_denomination_2000		int
			,	priority_2_is_remaining_amount_available_100		int
			,	priority_2_is_remaining_amount_available_200		int
			,	priority_2_is_remaining_amount_available_500		int
			,	priority_2_is_remaining_amount_available_2000		int
			,	priority_2_is_remaining_capacity_available_100		int
			,	priority_2_is_remaining_capacity_available_200		int
			,	priority_2_is_remaining_capacity_available_500		int
			,	priority_2_is_remaining_capacity_available_2000		int
			,	priority_2_loading_amount_100		bigint
			,	priority_2_loading_amount_200		bigint
			,	priority_2_loading_amount_500		bigint
			,	priority_2_loading_amount_2000		bigint
			,	priority_3_is_denomination_100		int
			,	priority_3_is_denomination_200		int
			,	priority_3_is_denomination_500		int
			,	priority_3_is_denomination_2000		int
			,	priority_3_is_remaining_amount_available_100		int
			,	priority_3_is_remaining_amount_available_200		int
			,	priority_3_is_remaining_amount_available_500		int
			,	priority_3_is_remaining_amount_available_2000		int
			,	priority_3_is_remaining_capacity_available_100		int
			,	priority_3_is_remaining_capacity_available_200		int
			,	priority_3_is_remaining_capacity_available_500		int
			,	priority_3_is_remaining_capacity_available_2000		int
			,	priority_3_loading_amount_100		bigint
			,	priority_3_loading_amount_200		bigint
			,	priority_3_loading_amount_500		bigint
			,	priority_3_loading_amount_2000		bigint
			,	priority_4_is_denomination_100		int
			,	priority_4_is_denomination_200		int
			,	priority_4_is_denomination_500		int
			,	priority_4_is_denomination_2000		int
			,	priority_4_is_remaining_amount_available_100		int
			,	priority_4_is_remaining_amount_available_200		int
			,	priority_4_is_remaining_amount_available_500		int
			,	priority_4_is_remaining_amount_available_2000		int
			,	priority_4_is_remaining_capacity_available_100		int
			,	priority_4_is_remaining_capacity_available_200		int
			,	priority_4_is_remaining_capacity_available_500		int
			,	priority_4_is_remaining_capacity_available_2000		int
			,	priority_4_loading_amount_100		bigint
			,	priority_4_loading_amount_200		bigint
			,	priority_4_loading_amount_500		bigint
			,	priority_4_loading_amount_2000		bigint
			,	loading_amount_100		bigint
			,	loading_amount_200		bigint
			,	loading_amount_500		bigint
			,	loading_amount_2000		bigint
			,	total_loading_amount		bigint
			,	remaining_capacity_amount_100		bigint
			,	remaining_capacity_amount_200		bigint
			,	remaining_capacity_amount_500		bigint
			,	remaining_capacity_amount_2000		bigint
			,   closing_remaining_available_amount_100      bigint
			,   closing_remaining_available_amount_200		bigint
			,   closing_remaining_available_amount_500		bigint
			,   closing_remaining_available_amount_2000		bigint
			,   total_closing_remaining_available_amount	bigint
			
			,	total_forecasted_remaining_amt		bigint
			)
			
			DROP TABLE IF EXISTS #temp_feeder_forecast
			CREATE TABLE #temp_feeder_forecast
			(
				project_id							nvarchar(50)
			,	bank_code							nvarchar(50)
			,	feeder_branch_code					nvarchar(50)
			,	is_deno_wise_cash_available			INT    NULL
			,	total_remaining_available_amount	BIGINT NULL
			,   remaining_avail_100					BIGINT NULL
			,   remaining_avail_200					BIGINT NULL
			,   remaining_avail_500					BIGINT NULL
			,   remaining_avail_2000				BIGINT NULL
			
			)
			
		 -- Run cursor for feeder level from #cash_pre_availability
		 DECLARE cursor1 CURSOR READ_ONLY
		 FOR
		 SELECT project_id, bank_code, feeder_branch_code,total_amount_available,available_100_amount,available_200_amount,available_500_amount,available_2000_amount,is_deno_wise_cash_available
		 FROM #cash_pre_availability 
		 ORDER BY feeder_branch_code

		-- SELECT * FROM #cash_pre_availability

	declare @total_remaining_available_amount BIGINT
	, @remaining_avail_100 BIGINT
	, @remaining_avail_200 BIGINT
	, @remaining_avail_500 BIGINT
	, @remaining_avail_2000 BIGINT
	, @total_original_available_amount BIGINT
	, @original_avail_100 BIGINT
	, @original_avail_200 BIGINT
	, @original_avail_500 BIGINT
	, @original_avail_2000 BIGINT
	, @loading_amount_100 BIGINT
	, @loading_amount_200 BIGINT
	, @loading_amount_500 BIGINT
	, @loading_amount_2000 BIGINT
	, @is_deno_wise_cash_available INT
	, @projectid nvarchar(50)
	, @bankcode nvarchar(50)
	OPEN cursor1

	FETCH NEXT FROM cursor1 INTO @projectid,@bankcode, @feederbranchcode, @total_original_available_amount, @original_avail_100, @original_avail_200, @original_avail_500, @original_avail_2000, @is_deno_wise_cash_available
    
    WHILE @@FETCH_STATUS = 0  
    BEGIN		
				SET @total_remaining_available_amount	= @total_original_available_amount
				SET @remaining_avail_100				= @original_avail_100
				SET @remaining_avail_200				= @original_avail_200
				SET @remaining_avail_500				= @original_avail_500
				SET @remaining_avail_2000				= @original_avail_2000
				
				
					DECLARE cursor2 cursor Read_Only
					For
					
					-- New
					SELECT	
							 dist.atm_id,
							 CASE 
								WHEN atm_band = 'Platinum'
								THEN 1
								WHEN atm_band = 'Gold'
								THEN 2
								WHEN CashOut = 1
								THEN 3
								WHEN atm_band = 'Silver'
								THEN 4
								ELSE 5
							END as atm_priority 						
							, COALESCE(denomination_100_max_capacity_percentage,deno_100_max_capacity_percentage)   as denomination_100_max_capacity_percentage
							, COALESCE(denomination_200_max_capacity_percentage ,deno_200_max_capacity_percentage)  as denomination_200_max_capacity_percentage
							, COALESCE(denomination_500_max_capacity_percentage	,deno_500_max_capacity_percentage)  as denomination_500_max_capacity_percentage
							, COALESCE(denomination_2000_max_capacity_percentage,deno_2000_max_capacity_percentage) as denomination_2000_max_capacity_percentage
							, morning_balance_100
							, morning_balance_200
							, morning_balance_500
							, morning_balance_2000
							, total_morning_balance
							, rounded_amount_100  as forecasted_amt_100		 
							, rounded_amount_200  as forecasted_amt_200	
							, rounded_amount_500  as forecasted_amt_500	
							, rounded_amount_2000 as forecasted_amt_2000	
							, (rounded_amount_100 + rounded_amount_200 + rounded_amount_500 + rounded_amount_2000) as total_forecasted_amt
							--, cassette_50_count_original					
							, cassette_100_count_original 
							, cassette_200_count_original 
							, cassette_500_count_original 
							, cassette_2000_count_original
							, COALESCE(dist.deno_100_priority ,sa.deno_100_priority)   as deno_100_priority
							, COALESCE(dist.deno_200_priority ,sa.deno_200_priority)   as deno_200_priority
							, COALESCE(dist.deno_500_priority ,sa.deno_500_priority)   as deno_500_priority
							, COALESCE(dist.deno_2000_priority,sa.deno_2000_priority)  as deno_2000_priority
							, cassette_100_brand_capacity
							, cassette_200_brand_capacity
							, cassette_500_brand_capacity
							, cassette_2000_brand_capacity
							
					 from #dist_with_cra dist
					LEFT JOIN atm_master atm
					on atm.atm_id =  dist.atm_id and atm.site_code = dist.site_code 
					and dist.project_id = @projectid and dist.bank_code = @bankcode and feeder_branch_code = @feederbranchcode
					and atm.record_status = 'Active' and atm.site_status = 'Active'
					CROSS JOIN system_settings sa 
					WHERE sa.record_status = 'Active'
					ORDER BY atm_priority

					--select * from system_settings 
					--select distinct atm.* from atm_master atm join distribution_planning_detail dist  
					--on atm.atm_id = dist.atm_id and atm.bank_code = dist.bank_code 
					--where atm.project_id = 'MOF_MAH' and atm.bank_code = 'BOMH' and feeder_branch_code = 'ACHALPUR'
					
					--select * from atm_master where atm_id = 'NA0541C1' and site_code ='BOMAMR10287'

					--select * from #dist_with_cra
					--SELECT * FROM #dist_with_cra where feeder_branch_code = 'KHADAKI'
					declare @atmid varchar(50)
							, @atm_priority INT												
							, @denomination_100_max_capacity_percentage bigint
							, @denomination_200_max_capacity_percentage bigint
							, @denomination_500_max_capacity_percentage bigint
							, @denomination_2000_max_capacity_percentage bigint
							, @morning_balance_100 bigint
							, @morning_balance_200 bigint
							, @morning_balance_500 bigint
							, @morning_balance_2000 bigint
							, @total_morning_balance BIGINT
							, @original_forecasted_amt_100  bigint
							, @original_forecasted_amt_200  bigint
							, @original_forecasted_amt_500  bigint
							, @original_forecasted_amt_2000 bigint
							, @original_total_forecasted_amt BIGINT
							--, @cassette_50_count_original   BIGINT							
							, @cassette_100_count_original 	bigint
							, @cassette_200_count_original 	bigint
							, @cassette_500_count_original 	bigint
							, @cassette_2000_count_original	bigint		
							, @deno_100_priority   BIGINT
							, @deno_200_priority	  BIGINT
							, @deno_500_priority	  BIGINT
							, @deno_2000_priority  BIGINT
							, @cassette_100_brand_capacity 	bigint		
							, @cassette_200_brand_capacity	bigint		
							, @cassette_500_brand_capacity	bigint		
							, @cassette_2000_brand_capacity	bigint		
				    OPEN cursor2
					FETCH NEXT FROM cursor2 
						  INTO  							
							 @atmid	
							 ,@atm_priority						
							, @denomination_100_max_capacity_percentage
							, @denomination_200_max_capacity_percentage
							, @denomination_500_max_capacity_percentage
							, @denomination_2000_max_capacity_percentage
							, @morning_balance_100
							, @morning_balance_200
							, @morning_balance_500
							, @morning_balance_2000
							, @total_morning_balance
							, @original_forecasted_amt_100
							, @original_forecasted_amt_200
							, @original_forecasted_amt_500
							, @original_forecasted_amt_2000
							, @original_total_forecasted_amt
							--, @cassette_50_count_original  
							, @cassette_100_count_original 
							, @cassette_200_count_original 
							, @cassette_500_count_original 
							, @cassette_2000_count_original
							, @deno_100_priority
							, @deno_200_priority
							, @deno_500_priority
							, @deno_2000_priority
							, @cassette_100_brand_capacity
							, @cassette_200_brand_capacity
							, @cassette_500_brand_capacity
							, @cassette_2000_brand_capacity
					WHILE @@FETCH_STATUS=0
					BEGIN			---- Cursor 2 Begin
							--SELECT 'Hi'
						DECLARE @forecasted_remaining_amt_100  bigint
							, @forecasted_remaining_amt_200  bigint
							, @forecasted_remaining_amt_500  bigint
							, @forecasted_remaining_amt_2000 bigint

							, @total_forecasted_remaining_amt BIGINT
							, @remaining_capacity_amount_100  bigint
							, @remaining_capacity_amount_200  bigint
							, @remaining_capacity_amount_500  bigint
							, @remaining_capacity_amount_2000 bigint
							, @total_remaining_capacity_amount BIGINT

							, @total_opening_remaining_available_amount BIGINT
							, @opening_remaining_available_amount_100 BIGINT
							, @opening_remaining_available_amount_200 BIGINT
							, @opening_remaining_available_amount_500 BIGINT
							, @opening_remaining_available_amount_2000 BIGINT


						SET  @total_opening_remaining_available_amount = @total_remaining_available_amount
						SET	 @opening_remaining_available_amount_100 =  @remaining_avail_100
						SET	 @opening_remaining_available_amount_200 =  @remaining_avail_200
						SET	 @opening_remaining_available_amount_500 = @remaining_avail_500
						SET	 @opening_remaining_available_amount_2000 = @remaining_avail_2000

						set @forecasted_remaining_amt_100 = @original_forecasted_amt_100
						SET	@forecasted_remaining_amt_200 = @original_forecasted_amt_200
						SET	@forecasted_remaining_amt_500 = @original_forecasted_amt_500
						SET	@forecasted_remaining_amt_2000 = @original_forecasted_amt_2000
						SET @total_forecasted_remaining_amt = @original_total_forecasted_amt
						SET @remaining_capacity_amount_100   = @remaining_capacity_amount_100
						SET @remaining_capacity_amount_200   = @remaining_capacity_amount_200
						SET @remaining_capacity_amount_500  = @remaining_capacity_amount_500
						SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000
						SET @total_remaining_capacity_amount = @total_remaining_capacity_amount
						
						--select @total_opening_remaining_available_amount
						--select @total_forecasted_remaining_amt
						-- GET TOTAL CAPACITY AMOUNT
						DECLARE @total_capacity_amount_100 bigint
							, @total_capacity_amount_200 bigint
							, @total_capacity_amount_500 bigint 
							, @total_capacity_amount_2000 bigint

						SET @total_capacity_amount_100 = @cassette_100_count_original * @cassette_100_brand_capacity * 100
						SET @total_capacity_amount_200 = @cassette_200_count_original * @cassette_200_brand_capacity * 200
						SET @total_capacity_amount_500 = @cassette_500_count_original * @cassette_500_brand_capacity * 500
						SET @total_capacity_amount_2000 = @cassette_2000_count_original * @cassette_2000_brand_capacity * 2000
						
						
						-- GET TOTAL CAPACITY AMOUNT LIMIT TO CASSETTE CAPACITY PERCENTAGE

						DECLARE @max_amt_allowed_100 BIGINT,
								@max_amt_allowed_200 BIGINT,
								@max_amt_allowed_500 BIGINT,
								@max_amt_allowed_2000 BIGINT

						SET @max_amt_allowed_100 =  @total_capacity_amount_100 * @denomination_100_max_capacity_percentage / 100
						SET @max_amt_allowed_200 =  @total_capacity_amount_200 * @denomination_200_max_capacity_percentage / 100
						SET @max_amt_allowed_500 =  @total_capacity_amount_500 * @denomination_500_max_capacity_percentage / 100
						SET @max_amt_allowed_2000 =  @total_capacity_amount_2000 * @denomination_2000_max_capacity_percentage / 100

						

						-- Calculate  Deduct Morning balance
						DECLARE @tentative_loading_100 BIGINT,
								@tentative_loading_200 BIGINT,
								@tentative_loading_500 BIGINT,
								@tentative_loading_2000 BIGINT

						SET @tentative_loading_100   =  ABS(@max_amt_allowed_100 -  CASE  WHEN  (@max_amt_allowed_100 = 0) 
																				THEN 0 
																				ELSE @morning_balance_100
																				END)
						SET @tentative_loading_200   = ABS( @max_amt_allowed_200 - CASE  WHEN  (@max_amt_allowed_200 = 0) 
																				THEN 0 
																				ELSE @morning_balance_200
																				END)
						SET @tentative_loading_500   =  ABS(@max_amt_allowed_500 - CASE  WHEN  (@max_amt_allowed_500 = 0) 
																				THEN 0 
																				ELSE @morning_balance_500
																				END)
						SET @tentative_loading_2000  =  ABS(@max_amt_allowed_2000 - CASE  WHEN  (@max_amt_allowed_2000 = 0) 
																				THEN 0 
																				ELSE @morning_balance_2000
																				END)
						--- Calculate rounded tentative loading

						DECLARE @rounded_tentative_loading_100 BIGINT,
								@rounded_tentative_loading_200 BIGINT,
								@rounded_tentative_loading_500 BIGINT,
								@rounded_tentative_loading_2000 BIGINT

						SET @rounded_tentative_loading_100   =   @tentative_loading_100 - (@tentative_loading_100 % @denomination_wise_round_off_100 )
						SET @rounded_tentative_loading_200   =   @tentative_loading_200 - (@tentative_loading_200 % @denomination_wise_round_off_200 )
						SET @rounded_tentative_loading_500   =   @tentative_loading_500 - (@tentative_loading_500 % @denomination_wise_round_off_500 )
						SET @rounded_tentative_loading_2000   =   @tentative_loading_2000 - (@tentative_loading_2000 % @denomination_wise_round_off_2000 )

						DECLARE     @priority_1_is_denomination_100  BIGINT = 0,
									@priority_1_is_denomination_200 BIGINT = 0,
									@priority_1_is_denomination_500 BIGINT = 0,
									@priority_1_is_denomination_2000 BIGINT = 0,
									@priority_1_is_remaining_amount_available_100 BIGINT = 0,
									@priority_1_is_remaining_amount_available_200 BIGINT = 0,
									@priority_1_is_remaining_amount_available_500 BIGINT = 0,
									@priority_1_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_1_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_1_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_1_loading_amount_100 BIGINT = 0,
									@priority_1_loading_amount_200 BIGINT = 0,
									@priority_1_loading_amount_500 BIGINT = 0,
									@priority_1_loading_amount_2000 BIGINT = 0,
									@priority_2_is_denomination_100  BIGINT = 0,
									@priority_2_is_denomination_200 BIGINT = 0,
									@priority_2_is_denomination_500 BIGINT = 0,
									@priority_2_is_denomination_2000 BIGINT = 0,
									@priority_2_is_remaining_amount_available_100 BIGINT = 0,
									@priority_2_is_remaining_amount_available_200 BIGINT = 0,
									@priority_2_is_remaining_amount_available_500 BIGINT = 0,
									@priority_2_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_2_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_2_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_2_loading_amount_100 BIGINT = 0,
									@priority_2_loading_amount_200 BIGINT = 0,
									@priority_2_loading_amount_500 BIGINT = 0,
									@priority_2_loading_amount_2000 BIGINT = 0,
									@priority_3_is_denomination_100  BIGINT = 0,
									@priority_3_is_denomination_200 BIGINT = 0,
									@priority_3_is_denomination_500 BIGINT = 0,
									@priority_3_is_denomination_2000 BIGINT = 0,
									@priority_3_is_remaining_amount_available_100 BIGINT = 0,
									@priority_3_is_remaining_amount_available_200 BIGINT = 0,
									@priority_3_is_remaining_amount_available_500 BIGINT = 0,
									@priority_3_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_3_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_3_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_3_loading_amount_100 BIGINT = 0,
									@priority_3_loading_amount_200 BIGINT = 0,
									@priority_3_loading_amount_500 BIGINT = 0,
									@priority_3_loading_amount_2000 BIGINT = 0,
									@priority_4_is_denomination_100  BIGINT = 0,
									@priority_4_is_denomination_200 BIGINT = 0,
									@priority_4_is_denomination_500 BIGINT = 0,
									@priority_4_is_denomination_2000 BIGINT = 0,
									@priority_4_is_remaining_amount_available_100 BIGINT = 0,
									@priority_4_is_remaining_amount_available_200 BIGINT = 0,
									@priority_4_is_remaining_amount_available_500 BIGINT = 0,
									@priority_4_is_remaining_amount_available_2000 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_100 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_200 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_500 BIGINT = 0,
									@priority_4_is_remaining_capacity_available_2000 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_100 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_200 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_500 BIGINT = 0,
									@priority_4_max_loading_capacity_amount_2000 BIGINT = 0,
									@priority_4_loading_amount_100 BIGINT = 0,
									@priority_4_loading_amount_200 BIGINT = 0,
									@priority_4_loading_amount_500 BIGINT = 0,
									@priority_4_loading_amount_2000 BIGINT = 0

						SET @loading_amount_100 = 0
						SET @loading_amount_200 = 0
						SET @loading_amount_2000 = 0
						SET @loading_amount_500 = 0
							

						IF(@is_deno_wise_cash_available = 1)
						BEGIN
							-- CONTINUE FOR ALLOCATION LOGIC
							--select 'Denomination Available'
							IF (@total_forecasted_remaining_amt > 0)
							BEGIN
								IF (@deno_100_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_100 = 1
											SET @priority_1_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_1_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_100 = @priority_1_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_100 = @priority_1_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_1_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_1_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 1)

								ELSE IF (@deno_200_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_200 = 1
											SET @priority_1_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_1_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_200 = @priority_1_max_loading_capacity_amount_200
												--SELECT @priority_1_loading_amount_200
											END	-- END of @remaining_avail_200 > @priority_1_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_200 = @remaining_avail_200
												--SELECT @priority_1_loading_amount_200
											END		-- END of else
											--SELECT @priority_1_loading_amount_200
											--SELECT @priority_2_loading_amount_200
											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_200 = @priority_1_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_1_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_1_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 1)
									ELSE IF (@deno_500_priority = 1)
									BEGIN
										SET @priority_1_is_denomination_500 = 1
										IF (@remaining_avail_500 > 0)
										BEGIN
											SET @priority_1_is_remaining_amount_available_500 = 1
											IF (@rounded_tentative_loading_500 > 0 )
											BEGIN
												SET @priority_1_is_remaining_capacity_available_500 = 1
												SET @priority_1_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
												
												IF @remaining_avail_500 > @priority_1_max_loading_capacity_amount_500
												BEGIN
													-- assigining amount as per available capacity
													SET @priority_1_loading_amount_500 = @priority_1_max_loading_capacity_amount_500
												END
												ELSE
												BEGIN
													--	When available amount is less than capacity then allocate only available amount 
													SET @priority_1_loading_amount_500 = @remaining_avail_500
												END

												--Loading for priority 1 done
												--deduct from available amount
												SET @loading_amount_500 = @priority_1_loading_amount_500
												SET @remaining_avail_500 = @remaining_avail_500 - @priority_1_loading_amount_500
												SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_500
												SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_500
												SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_500
												SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_1_loading_amount_500
											
											END	-- END OF  (@rounded_tentative_loading_500 > 0 )
										END		-- END of (@remaining_avail_500 > 0)
										END		-- END OF (@deno_500_priority = 1)
								ELSE IF (@deno_2000_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_1_is_remaining_capacity_available_2000 = 1
											SET @priority_1_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_1_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_1_loading_amount_2000 = @priority_1_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_1_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 1 done
											--deduct from available amount
											SET @loading_amount_2000 = @priority_1_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_1_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_1_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 1)
					
		  -------------------------------------- Checking for 2nd priority denomination --------------------------------- 
								IF (@deno_100_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_100 = 1
											SET @priority_2_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_2_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_100 = @priority_2_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_2_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_2_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_2_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 2)

								ELSE IF (@deno_200_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_200 = 1
											SET @priority_2_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_2_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_200 = @priority_2_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_2_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_2_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_2_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_2_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 2)
								ELSE IF (@deno_500_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_500 = 1
											SET @priority_2_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF @remaining_avail_500 > @priority_2_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_500 = @priority_2_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_2_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_2_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_2_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 2)
								ELSE IF (@deno_2000_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_2_is_remaining_capacity_available_2000 = 1
											SET @priority_2_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_2_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_2_loading_amount_2000 = @priority_2_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_2_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_2_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_2_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_2_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 2)
			
			
			--------------------------------- Checking for 3rd priority denomination --------------------------------- 
								IF (@deno_100_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_100 = 1
											SET @priority_3_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											
											
											IF @remaining_avail_100 > @priority_3_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_100 = @priority_3_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 3 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_3_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_3_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_3_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 3)

								ELSE IF (@deno_200_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_200 = 1
											SET @priority_3_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_3_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_200 = @priority_3_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_3_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_3_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_3_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_3_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 3)
								ELSE IF (@deno_500_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_500 = 1
											SET @priority_3_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											
											--SELECT @remaining_avail_500
											--SELECT @priority_3_max_loading_capacity_amount_500
											
											IF @remaining_avail_500 > @priority_3_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_500 = @priority_3_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 3 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_3_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_3_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_3_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 3)
								ELSE IF (@deno_2000_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_3_is_remaining_capacity_available_2000 = 1
											SET @priority_3_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_3_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_3_loading_amount_2000 = @priority_3_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_3_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_3_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_3_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_3_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 3)
							
							
				---------------------------------------- Checking for 4th priority denomination --------------------------------- 
								IF (@deno_100_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_100 = 1
									IF (@remaining_avail_100 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 1
										IF (@rounded_tentative_loading_100 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_100 = 1
											SET @priority_4_max_loading_capacity_amount_100 = IIF (@rounded_tentative_loading_100 < @total_forecasted_remaining_amt,@rounded_tentative_loading_100,@total_forecasted_remaining_amt)
											IF @remaining_avail_100 > @priority_4_max_loading_capacity_amount_100
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_100 = @priority_4_max_loading_capacity_amount_100
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_100 = @remaining_avail_100
											END

											--Loading for priority 4 done
											--deduct from available amount
											
											SET @loading_amount_100 = @priority_4_loading_amount_100
											SET @remaining_avail_100 = @remaining_avail_100 - @priority_4_loading_amount_100
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_100
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_100
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_100
											SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_4_loading_amount_100
										
										END		-- end of (@rounded_tentative_loading_100 > 0 )
									END			-- END OF (@remaining_avail_100 > 0)

								END				---- END OF (@deno_100_priority = 4)

								ELSE IF (@deno_200_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_200 = 1
									IF (@remaining_avail_200 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 1
										IF (@rounded_tentative_loading_200 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_200 = 1
											SET @priority_4_max_loading_capacity_amount_200 = IIF (@rounded_tentative_loading_200 < @total_forecasted_remaining_amt,@rounded_tentative_loading_200,@total_forecasted_remaining_amt)
											IF @remaining_avail_200 > @priority_4_max_loading_capacity_amount_200
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_200 = @priority_4_max_loading_capacity_amount_200
											END	-- END of @remaining_avail_200 > @priority_4_max_loading_capacity_amount_200
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_200 = @remaining_avail_200
											END		-- END of else

											--Loading for priority 4 done
											--deduct from available amount
											
											SET @loading_amount_200 = @priority_4_loading_amount_200
											SET @remaining_avail_200 = @remaining_avail_200 - @priority_4_loading_amount_200
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_200
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_200
											SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_4_loading_amount_200
										
										END		-- END OF (@rounded_tentative_loading_200 > 0 )
									END			-- END OF (@remaining_avail_200 > 0)
									END			-- END OF (@deno_200_priority = 4)
								ELSE IF (@deno_500_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_500 = 1
									IF (@remaining_avail_500 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 1
										IF (@rounded_tentative_loading_500 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_500 = 1
											SET @priority_4_max_loading_capacity_amount_500 = IIF (@rounded_tentative_loading_500 < @total_forecasted_remaining_amt,@rounded_tentative_loading_500,@total_forecasted_remaining_amt)
											IF @remaining_avail_500 > @priority_4_max_loading_capacity_amount_500
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_500 = @priority_4_max_loading_capacity_amount_500
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_500 = @remaining_avail_500
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_500 = @priority_4_loading_amount_500
											SET @remaining_avail_500 = @remaining_avail_500 - @priority_4_loading_amount_500
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_500
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_500
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_500
											SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_4_loading_amount_500
											
										END	-- END OF  (@rounded_tentative_loading_500 > 0 )
									END		-- END of (@remaining_avail_500 > 0)
									END		-- END OF (@deno_500_priority = 4)
								ELSE IF (@deno_2000_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_2000 = 1
									IF (@remaining_avail_2000 > 0)
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 1
										IF (@rounded_tentative_loading_2000 > 0 )
										BEGIN
											SET @priority_4_is_remaining_capacity_available_2000 = 1
											SET @priority_4_max_loading_capacity_amount_2000 = IIF (@rounded_tentative_loading_2000 < @total_forecasted_remaining_amt,@rounded_tentative_loading_2000,@total_forecasted_remaining_amt)
											IF @remaining_avail_2000 > @priority_4_max_loading_capacity_amount_2000
											BEGIN
												-- assigining amount as per available capacity
												SET @priority_4_loading_amount_2000 = @priority_4_max_loading_capacity_amount_2000
											END
											ELSE
											BEGIN
												--	When available amount is less than capacity then allocate only available amount 
												SET @priority_4_loading_amount_2000 = @remaining_avail_2000
											END

											--Loading for priority 2 done
											--deduct from available amount
											
											SET @loading_amount_2000 = @priority_4_loading_amount_2000
											SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_4_loading_amount_2000
											SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_2000
											SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_2000
											SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_2000
											SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_4_loading_amount_2000
										
										END		-- END OF (@rounded_tentative_loading_2000 > 0 )
									END		-- END OF 	(@remaining_avail_2000 > 0)				
							END				-- END OF (@deno_2000_priority = 4)
		
		
										--SELECT @loading_amount_100	
										--SELECT @loading_amount_200	
										--SELECT @loading_amount_500
										--SELECT @loading_amount_2000	
		 

		
						INSERT INTO #temp_cash_pre_availability
							(
										project_id
									,	bank_code
									,	atmid
									,	feeder_branch_code
									,	total_opening_remaining_available_amount
									,	opening_remaining_available_amount_100
									,	opening_remaining_available_amount_200
									,	opening_remaining_available_amount_500
									,	opening_remaining_available_amount_2000
									,	original_total_forecasted_amt
									,	original_forecasted_amt_100
									,	original_forecasted_amt_200
									,	original_forecasted_amt_500
									,	original_forecasted_amt_2000
									,	morning_balance_100
									,	morning_balance_200
									,	morning_balance_500
									,	morning_balance_2000
									,	total_morning_balance
									,	cassette_100_count_original
									,	cassette_200_count_original
									,	cassette_500_count_original
									,	cassette_2000_count_original
									,	cassette_100_brand_capacity
									,	cassette_200_brand_capacity
									,	cassette_500_brand_capacity
									,	cassette_2000_brand_capacity
									,	total_capacity_amount_100
									,	total_capacity_amount_200
									,	total_capacity_amount_500
									,	total_capacity_amount_2000
									,	denomination_100_max_capacity_percentage
									,	denomination_200_max_capacity_percentage
									,	denomination_500_max_capacity_percentage
									,	denomination_2000_max_capacity_percentage
									,	max_amt_allowed_100
									,	max_amt_allowed_200
									,	max_amt_allowed_500
									,	max_amt_allowed_2000
									,	denomination_wise_round_off_100
									,	denomination_wise_round_off_200
									,	denomination_wise_round_off_500
									,	denomination_wise_round_off_2000
									,	tentative_loading_100
									,	tentative_loading_200
									,	tentative_loading_500
									,	tentative_loading_2000
									,	rounded_tentative_loading_100
									,	rounded_tentative_loading_200
									,	rounded_tentative_loading_500
									,	rounded_tentative_loading_2000
									,	deno_100_priority
									,	deno_200_priority
									,	deno_500_priority
									,	deno_2000_priority
									,	is_deno_wise_cash_available
									,	priority_1_is_denomination_100
									,	priority_1_is_denomination_200
									,	priority_1_is_denomination_500
									,	priority_1_is_denomination_2000
									,	priority_1_is_remaining_amount_available_100
									,	priority_1_is_remaining_amount_available_200
									,	priority_1_is_remaining_amount_available_500
									,	priority_1_is_remaining_amount_available_2000
									,	priority_1_is_remaining_capacity_available_100
									,	priority_1_is_remaining_capacity_available_200
									,	priority_1_is_remaining_capacity_available_500
									,	priority_1_is_remaining_capacity_available_2000
									,	priority_1_loading_amount_100
									,	priority_1_loading_amount_200
									,	priority_1_loading_amount_500
									,	priority_1_loading_amount_2000
									,	priority_2_is_denomination_100
									,	priority_2_is_denomination_200
									,	priority_2_is_denomination_500
									,	priority_2_is_denomination_2000
									,	priority_2_is_remaining_amount_available_100
									,	priority_2_is_remaining_amount_available_200
									,	priority_2_is_remaining_amount_available_500
									,	priority_2_is_remaining_amount_available_2000
									,	priority_2_is_remaining_capacity_available_100
									,	priority_2_is_remaining_capacity_available_200
									,	priority_2_is_remaining_capacity_available_500
									,	priority_2_is_remaining_capacity_available_2000
									,	priority_2_loading_amount_100
									,	priority_2_loading_amount_200
									,	priority_2_loading_amount_500
									,	priority_2_loading_amount_2000
									,	priority_3_is_denomination_100
									,	priority_3_is_denomination_200
									,	priority_3_is_denomination_500
									,	priority_3_is_denomination_2000
									,	priority_3_is_remaining_amount_available_100
									,	priority_3_is_remaining_amount_available_200
									,	priority_3_is_remaining_amount_available_500
									,	priority_3_is_remaining_amount_available_2000
									,	priority_3_is_remaining_capacity_available_100
									,	priority_3_is_remaining_capacity_available_200
									,	priority_3_is_remaining_capacity_available_500
									,	priority_3_is_remaining_capacity_available_2000
									,	priority_3_loading_amount_100
									,	priority_3_loading_amount_200
									,	priority_3_loading_amount_500
									,	priority_3_loading_amount_2000
									,	priority_4_is_denomination_100
									,	priority_4_is_denomination_200
									,	priority_4_is_denomination_500
									,	priority_4_is_denomination_2000
									,	priority_4_is_remaining_amount_available_100
									,	priority_4_is_remaining_amount_available_200
									,	priority_4_is_remaining_amount_available_500
									,	priority_4_is_remaining_amount_available_2000
									,	priority_4_is_remaining_capacity_available_100
									,	priority_4_is_remaining_capacity_available_200
									,	priority_4_is_remaining_capacity_available_500
									,	priority_4_is_remaining_capacity_available_2000
									,	priority_4_loading_amount_100
									,	priority_4_loading_amount_200
									,	priority_4_loading_amount_500
									,	priority_4_loading_amount_2000
									,	loading_amount_100
									,	loading_amount_200
									,	loading_amount_500
									,	loading_amount_2000
									,	total_loading_amount
									,	remaining_capacity_amount_100
									,	remaining_capacity_amount_200
									,	remaining_capacity_amount_500
									,	remaining_capacity_amount_2000
									,   closing_remaining_available_amount_100
									,   closing_remaining_available_amount_200
									,   closing_remaining_available_amount_500
									,   closing_remaining_available_amount_2000
									,   total_closing_remaining_available_amount									
									,	total_forecasted_remaining_amt
							)
							SELECT  
									   @project_id								                                                            as project_id								
								   , @bank_code									                                                            as bank_code									
								   , @atmid										                                                            as atmid
								   , @feederbranchcode																						as feeder_branch_code										
								   , @total_opening_remaining_available_amount																as total_opening_remaining_available_amount
								   , COALESCE(@opening_remaining_available_amount_100	,0)													as opening_remaining_available_amount_100	
								   , COALESCE(@opening_remaining_available_amount_200	,0)                                                 as opening_remaining_available_amount_200	
								   , COALESCE(@opening_remaining_available_amount_500	,0)                                                 as opening_remaining_available_amount_500	
								   , COALESCE(@opening_remaining_available_amount_2000	,0)                                                 as opening_remaining_available_amount_2000	

								   , @original_total_forecasted_amt							                                                as original_total_forecasted_amt							
								   , @original_forecasted_amt_100				                                                            as original_forecasted_amt_100				
								   , @original_forecasted_amt_200                                                                           as original_forecasted_amt_200
								   , @original_forecasted_amt_500                                                                           as original_forecasted_amt_500
								   , @original_forecasted_amt_2000                                                                          as original_forecasted_amt_2000
								   , @morning_balance_100                                                                                   as morning_balance_100
								   , @morning_balance_200                                                                                   as morning_balance_200
								   , @morning_balance_500                                                                                   as morning_balance_500
								   , @morning_balance_2000                                                                                  as morning_balance_2000
								   , @total_morning_balance                                                                                 as total_morning_balance
								   , @cassette_100_count_original                                                                           as cassette_100_count_original
								   , @cassette_200_count_original                                                                           as cassette_200_count_original
								   , @cassette_500_count_original                                                                           as cassette_500_count_original
								   , @cassette_2000_count_original                                                                          as cassette_2000_count_original
								   , @cassette_100_brand_capacity                                                                           as cassette_100_brand_capacity
								   , @cassette_200_brand_capacity                                                                           as cassette_200_brand_capacity
								   , @cassette_500_brand_capacity                                                                           as cassette_500_brand_capacity
								   , @cassette_2000_brand_capacity                                                                          as cassette_2000_brand_capacity
								   , @total_capacity_amount_100                                                                             as total_capacity_amount_100
								   , @total_capacity_amount_200                                                                             as total_capacity_amount_200
								   , @total_capacity_amount_500                                                                             as total_capacity_amount_500
								   , @total_capacity_amount_2000                                                                            as total_capacity_amount_2000
								   , @denomination_100_max_capacity_percentage                                                              as denomination_100_max_capacity_percentage
								   , @denomination_200_max_capacity_percentage                                                              as denomination_200_max_capacity_percentage
								   , @denomination_500_max_capacity_percentage                                                              as denomination_500_max_capacity_percentage
								   , @denomination_2000_max_capacity_percentage                                                             as denomination_2000_max_capacity_percentage
								   , @max_amt_allowed_100                                                                                   as max_amt_allowed_100
								   , @max_amt_allowed_200                                                                                   as max_amt_allowed_200
								   , @max_amt_allowed_500                                                                                   as max_amt_allowed_500
								   , @max_amt_allowed_2000                                                                                  as max_amt_allowed_2000
								   , @denomination_wise_round_off_100                                                                       as denomination_wise_round_off_100
								   , @denomination_wise_round_off_200                                                                       as denomination_wise_round_off_200
								   , @denomination_wise_round_off_500                                                                       as denomination_wise_round_off_500
								   , @denomination_wise_round_off_2000                                                                      as denomination_wise_round_off_2000
								   , @tentative_loading_100                                                                                 as tentative_loading_100 
								   , @tentative_loading_200                                                                                 as tentative_loading_200 
								   , @tentative_loading_500                                                                                 as tentative_loading_500 
								   , @tentative_loading_2000                                                                                as tentative_loading_2000
								   -- Max Capacity                                                                                          
								   , @rounded_tentative_loading_100		                                                                    as rounded_tentative_loading_100		
								   , @rounded_tentative_loading_200	                                                                        as rounded_tentative_loading_200	    
								   , @rounded_tentative_loading_500	                                                                        as rounded_tentative_loading_500	    
								   , @rounded_tentative_loading_2000	                                                                    as rounded_tentative_loading_2000	
								   , @deno_100_priority                                                                                     as deno_100_priority
								   , @deno_200_priority                                                                                     as deno_200_priority
								   , @deno_500_priority                                                                                     as deno_500_priority
								   , @deno_2000_priority                                                                                    as deno_2000_priority
								   , @is_deno_wise_cash_available                                                                           as is_deno_wise_cash_available
									---- If denomination wise is availabble                                                                 
									-- Priority of denomination                                                                             
								   , @priority_1_is_denomination_100                                                                        as priority_1_is_denomination_100
								   , @priority_1_is_denomination_200                                                                        as priority_1_is_denomination_200
								   , @priority_1_is_denomination_500                                                                        as priority_1_is_denomination_500
								   , @priority_1_is_denomination_2000 								   	                                    as priority_1_is_denomination_2000 								   	
								   , @priority_1_is_remaining_amount_available_100                                                          as priority_1_is_remaining_amount_available_100
								   , @priority_1_is_remaining_amount_available_200                                                          as priority_1_is_remaining_amount_available_200
								   , @priority_1_is_remaining_amount_available_500                                                          as priority_1_is_remaining_amount_available_500
								   , @priority_1_is_remaining_amount_available_2000                                                         as priority_1_is_remaining_amount_available_2000
								   , @priority_1_is_remaining_capacity_available_100                                                        as priority_1_is_remaining_capacity_available_100
								   , @priority_1_is_remaining_capacity_available_200                                                        as priority_1_is_remaining_capacity_available_200
								   , @priority_1_is_remaining_capacity_available_500                                                        as priority_1_is_remaining_capacity_available_500
								   , @priority_1_is_remaining_capacity_available_2000								                        as priority_1_is_remaining_capacity_available_2000								
								   , @priority_1_loading_amount_100                                                                         as priority_1_loading_amount_100
								   , @priority_1_loading_amount_200                                                                         as priority_1_loading_amount_200
								   , @priority_1_loading_amount_500                                                                         as priority_1_loading_amount_500
								   , @priority_1_loading_amount_2000								                                        as priority_1_loading_amount_2000								   
								   , @priority_2_is_denomination_100                                                                        as priority_2_is_denomination_100
								   , @priority_2_is_denomination_200                                                                        as priority_2_is_denomination_200
								   , @priority_2_is_denomination_500                                                                        as priority_2_is_denomination_500
								   , @priority_2_is_denomination_2000 								                                        as priority_2_is_denomination_2000 								   
								   , @priority_2_is_remaining_amount_available_100                                                          as priority_2_is_remaining_amount_available_100
								   , @priority_2_is_remaining_amount_available_200                                                          as priority_2_is_remaining_amount_available_200
								   , @priority_2_is_remaining_amount_available_500                                                          as priority_2_is_remaining_amount_available_500
								   , @priority_2_is_remaining_amount_available_2000                                                         as priority_2_is_remaining_amount_available_2000
								   , @priority_2_is_remaining_capacity_available_100                                                        as priority_2_is_remaining_capacity_available_100
								   , @priority_2_is_remaining_capacity_available_200                                                        as priority_2_is_remaining_capacity_available_200
								   , @priority_2_is_remaining_capacity_available_500                                                        as priority_2_is_remaining_capacity_available_500
								   , @priority_2_is_remaining_capacity_available_2000  								                        as priority_2_is_remaining_capacity_available_2000  								
								   , @priority_2_loading_amount_100                                                                         as priority_2_loading_amount_100
								   , @priority_2_loading_amount_200                                                                         as priority_2_loading_amount_200
								   , @priority_2_loading_amount_500                                                                         as priority_2_loading_amount_500
								   , @priority_2_loading_amount_2000								                                        as priority_2_loading_amount_2000								   
								   , @priority_3_is_denomination_100                                                                        as priority_3_is_denomination_100
								   , @priority_3_is_denomination_200                                                                        as priority_3_is_denomination_200
								   , @priority_3_is_denomination_500                                                                        as priority_3_is_denomination_500
								   , @priority_3_is_denomination_2000 								                                        as priority_3_is_denomination_2000 								   
								   , @priority_3_is_remaining_amount_available_100                                                          as priority_3_is_remaining_amount_available_100
								   , @priority_3_is_remaining_amount_available_200                                                          as priority_3_is_remaining_amount_available_200
								   , @priority_3_is_remaining_amount_available_500                                                          as priority_3_is_remaining_amount_available_500
								   , @priority_3_is_remaining_amount_available_2000								                            as priority_3_is_remaining_amount_available_2000								   
								   , @priority_3_is_remaining_capacity_available_100                                                        as priority_3_is_remaining_capacity_available_100
								   , @priority_3_is_remaining_capacity_available_200                                                        as priority_3_is_remaining_capacity_available_200
								   , @priority_3_is_remaining_capacity_available_500                                                        as priority_3_is_remaining_capacity_available_500
								   , @priority_3_is_remaining_capacity_available_2000  						                                as priority_3_is_remaining_capacity_available_2000  						
								   , @priority_3_loading_amount_100                                                                         as priority_3_loading_amount_100
								   , @priority_3_loading_amount_200                                                                         as priority_3_loading_amount_200
								   , @priority_3_loading_amount_500                                                                         as priority_3_loading_amount_500
								   , @priority_3_loading_amount_2000								                                        as priority_3_loading_amount_2000								   
								   , @priority_4_is_denomination_100                                                                        as priority_4_is_denomination_100
								   , @priority_4_is_denomination_200                                                                        as priority_4_is_denomination_200
								   , @priority_4_is_denomination_500                                                                        as priority_4_is_denomination_500
								   , @priority_4_is_denomination_2000 								                                        as priority_4_is_denomination_2000 								   
								   , @priority_4_is_remaining_amount_available_100                                                          as priority_4_is_remaining_amount_available_100
								   , @priority_4_is_remaining_amount_available_200                                                          as priority_4_is_remaining_amount_available_200
								   , @priority_4_is_remaining_amount_available_500                                                          as priority_4_is_remaining_amount_available_500
								   , @priority_4_is_remaining_amount_available_2000								                            as priority_4_is_remaining_amount_available_2000								   
								   , @priority_4_is_remaining_capacity_available_100                                                        as priority_4_is_remaining_capacity_available_100
								   , @priority_4_is_remaining_capacity_available_200                                                        as priority_4_is_remaining_capacity_available_200
								   , @priority_4_is_remaining_capacity_available_500                                                        as priority_4_is_remaining_capacity_available_500
								   , @priority_4_is_remaining_capacity_available_2000		                                                as priority_4_is_remaining_capacity_available_2000		
								   , @priority_4_loading_amount_100                                                                         as priority_4_loading_amount_100
								   , @priority_4_loading_amount_200                                                                         as priority_4_loading_amount_200
								   , @priority_4_loading_amount_500                                                                         as priority_4_loading_amount_500
								   , @priority_4_loading_amount_2000								                                        as priority_4_loading_amount_2000								   
								   , @loading_amount_100                                                                                    as loading_amount_100
								   , @loading_amount_200                                                                                    as loading_amount_200
								   , @loading_amount_500                                                                                    as loading_amount_500
								   , @loading_amount_2000                                                                                   as loading_amount_2000
									-- Total loading amount								                                                    
								   , @loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000                 as total_loading_amount
								   , @remaining_capacity_amount_100                                                                         as remaining_capacity_amount_100
								   , @remaining_capacity_amount_200                                                                         as remaining_capacity_amount_200
								   , @remaining_capacity_amount_500                                                                         as remaining_capacity_amount_500
								   , @remaining_capacity_amount_2000								                                        as remaining_capacity_amount_2000								   
								   --closing available amount	                                                                           
								   , @remaining_avail_100			                                                                        as closing_avail_100			
								   , @remaining_avail_200			                                                                        as closing_avail_200			
								   , @remaining_avail_500			                                                                        as closing_avail_500			
								   , @remaining_avail_2000                                                                                  as closing_avail_2000
								   , @total_remaining_available_amount                                                                      as total_closing_available_amount
								   , @total_forecasted_remaining_amt                                                                        as total_forecasted_remaining_amt

							END -- END OF (@total_forecasted_remaining_amt > 0)						

						 
										
									
							  
						END
						
						
						ELSE --If denoination wise cash is not available
						BEGIN							-- Denomination wise not available BEGIN
							--select @total_remaining_available_amount
							--select @original_total_forecasted_amt
							--select 'Denomination not Available'
							IF (@total_remaining_available_amount > @original_total_forecasted_amt)
							BEGIN						--
								SET @loading_amount_100 = @original_forecasted_amt_100
								SET @loading_amount_200 = @original_forecasted_amt_200
								SET @loading_amount_500 = @original_forecasted_amt_500
								SET @loading_amount_2000 = @original_forecasted_amt_2000								

								SET @total_remaining_available_amount = @total_remaining_available_amount - (@loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000)
							END
							ELSE -- IF REMAINING AMOUNT IS LESS THAN FORCASTED AMOUNT
							BEGIN
								
								IF (@deno_100_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 1
										SET @priority_1_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_100 = 0
										SET @priority_1_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_100 = @priority_1_loading_amount_100
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_1_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_1_loading_amount_100
								END		-- end of (@deno_100_priority = 1 )

								ELSE IF (@deno_200_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 1
										SET @priority_1_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_200 = 0
										SET @priority_1_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_200 = @priority_1_loading_amount_200
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_1_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_1_loading_amount_200
								END		-- end of (@deno_200_priority = 1 )

								ELSE IF (@deno_500_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_500 = 1
										SET @priority_1_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_500 = 0
										SET @priority_1_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_500 = @priority_1_loading_amount_500
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_1_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_1_loading_amount_500
								END		-- end of (@deno_500_priority = 1 )

								ELSE IF (@deno_2000_priority = 1)
								BEGIN
									SET @priority_1_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 1
										SET @priority_1_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_1_is_remaining_amount_available_2000 = 0
										SET @priority_1_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_2000 = @priority_1_loading_amount_2000
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_1_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_1_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_1_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_1_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_1_loading_amount_2000
								END		-- end of (@deno_2000_priority = 1 )
								----------------------------------Priority 2 start -------------------------------------------
								IF (@deno_100_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 1
										SET @priority_2_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_100 = 0
										SET @priority_2_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_100 = @priority_2_loading_amount_100
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_2_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_2_loading_amount_100
								END		-- end of (@deno_100_priority = 2 )

								ELSE IF (@deno_200_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 1
										SET @priority_2_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_200 = 0
										SET @priority_2_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_200 = @priority_2_loading_amount_200
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_2_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_2_loading_amount_200
								END		-- end of (@deno_200_priority = 2 )

								ELSE IF (@deno_500_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 1
										SET @priority_2_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_500 = 0
										SET @priority_2_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_500 = @priority_2_loading_amount_500
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_2_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_2_loading_amount_500
								END		-- end of (@deno_500_priority = 2 )

								ELSE IF (@deno_2000_priority = 2)
								BEGIN
									SET @priority_2_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 1
										SET @priority_2_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_2_is_remaining_amount_available_2000 = 0
										SET @priority_2_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_2000 = @priority_2_loading_amount_2000
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_2_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_2_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_2_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_2_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_2_loading_amount_2000
								END		-- end of (@deno_2000_priority = 2 )

								---------------------------------Priority 3 start -------------------------------
								IF (@deno_100_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 1
										SET @priority_3_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_100 = 0
										SET @priority_3_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_100 = @priority_3_loading_amount_100
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_3_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_3_loading_amount_100
								END		-- end of (@deno_100_priority = 3 )

								ELSE IF (@deno_200_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 1
										SET @priority_3_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_200 = 0
										SET @priority_3_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_200 = @priority_3_loading_amount_200
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_3_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_3_loading_amount_200
								END		-- end of (@deno_200_priority = 1 )

								ELSE IF (@deno_500_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 1
										SET @priority_3_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_500 = 0
										SET @priority_3_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_500 = @priority_3_loading_amount_500
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_3_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_3_loading_amount_500
								END		-- end of (@deno_500_priority = 3 )

								ELSE IF (@deno_2000_priority = 3)
								BEGIN
									SET @priority_3_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 1
										SET @priority_3_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_3_is_remaining_amount_available_2000 = 0
										SET @priority_3_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_2000 = @priority_3_loading_amount_2000
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_3_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_3_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_3_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_3_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_3_loading_amount_2000
								END		-- end of (@deno_2000_priority = 3 )

								----------------------------------priority 4 start------------------------------
								IF (@deno_100_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_100 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_100 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 1
										SET @priority_4_loading_amount_100 = @original_forecasted_amt_100										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_100 = 0
										SET @priority_4_loading_amount_100 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_100 = @priority_4_loading_amount_100
									SET @remaining_avail_100 = @remaining_avail_100 - @priority_4_loading_amount_100
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_100
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_100
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_100
									SET @remaining_capacity_amount_100 = @remaining_capacity_amount_100 - @priority_4_loading_amount_100
								END		-- end of (@deno_100_priority = 4 )

								ELSE IF (@deno_200_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_200 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_200 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 1
										SET @priority_4_loading_amount_200 = @original_forecasted_amt_200
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_200 = 0
										SET @priority_4_loading_amount_200 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_200 = @priority_4_loading_amount_200
									SET @remaining_avail_200 = @remaining_avail_200 - @priority_4_loading_amount_200
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_200
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_200
									SET @remaining_capacity_amount_200 = @remaining_capacity_amount_200 - @priority_4_loading_amount_200
								END		-- end of (@deno_200_priority = 4 )

								ELSE IF (@deno_500_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_500 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_500 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 1
										SET @priority_4_loading_amount_500 = @original_forecasted_amt_500										
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_500 = 0
										SET @priority_4_loading_amount_500 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_500 = @priority_4_loading_amount_500
									SET @remaining_avail_500 = @remaining_avail_500 - @priority_4_loading_amount_500
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_500
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_500
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_500
									SET @remaining_capacity_amount_500 = @remaining_capacity_amount_500 - @priority_4_loading_amount_500
								END		-- end of (@deno_500_priority = 4 )

								ELSE IF (@deno_2000_priority = 4)
								BEGIN
									SET @priority_4_is_denomination_2000 = 1
									IF (@total_remaining_available_amount > @original_forecasted_amt_2000 )
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 1
										SET @priority_4_loading_amount_2000 = @original_forecasted_amt_2000
									END
									ELSE -- IF REMAINING AMOUNT IS LESS THAN PRIORITY LOADING AMOUNT
									BEGIN
										SET @priority_4_is_remaining_amount_available_2000 = 0
										SET @priority_4_loading_amount_2000 = @total_remaining_available_amount
									END
									--deduct from available amount
									SET @loading_amount_2000 = @priority_4_loading_amount_2000
									SET @remaining_avail_2000 = @remaining_avail_2000 - @priority_4_loading_amount_2000
									SET @total_remaining_available_amount = @total_remaining_available_amount - @priority_4_loading_amount_200
									SET @total_forecasted_remaining_amt = @total_forecasted_remaining_amt - @priority_4_loading_amount_2000
									SET @total_remaining_capacity_amount = @total_remaining_capacity_amount - @priority_4_loading_amount_2000
									SET @remaining_capacity_amount_2000 = @remaining_capacity_amount_2000 - @priority_4_loading_amount_2000
								END		-- end of (@deno_2000_priority = 4 )
							END	-- END OF ELSE -- IF REMAINING AMOUNT IS LESS THAN FORCASTED AMOUNT

							-- insert into temp table
								




							INSERT INTO #temp_cash_pre_availability 
							(
													project_id
												,	bank_code
												,	atmid
												,	feeder_branch_code
												,	total_opening_remaining_available_amount
												,	opening_remaining_available_amount_100
												,	opening_remaining_available_amount_200
												,	opening_remaining_available_amount_500
												,	opening_remaining_available_amount_2000
												,	original_total_forecasted_amt
												,	original_forecasted_amt_100
												,	original_forecasted_amt_200
												,	original_forecasted_amt_500
												,	original_forecasted_amt_2000
												,	morning_balance_100
												,	morning_balance_200
												,	morning_balance_500
												,	morning_balance_2000
												,	total_morning_balance
												,	cassette_100_count_original
												,	cassette_200_count_original
												,	cassette_500_count_original
												,	cassette_2000_count_original
												,	cassette_100_brand_capacity
												,	cassette_200_brand_capacity
												,	cassette_500_brand_capacity
												,	cassette_2000_brand_capacity
												,	total_capacity_amount_100
												,	total_capacity_amount_200
												,	total_capacity_amount_500
												,	total_capacity_amount_2000
												,	denomination_100_max_capacity_percentage
												,	denomination_200_max_capacity_percentage
												,	denomination_500_max_capacity_percentage
												,	denomination_2000_max_capacity_percentage
												,	max_amt_allowed_100
												,	max_amt_allowed_200
												,	max_amt_allowed_500
												,	max_amt_allowed_2000
												,	denomination_wise_round_off_100
												,	denomination_wise_round_off_200
												,	denomination_wise_round_off_500
												,	denomination_wise_round_off_2000
												,	tentative_loading_100
												,	tentative_loading_200
												,	tentative_loading_500
												,	tentative_loading_2000
												,	rounded_tentative_loading_100
												,	rounded_tentative_loading_200
												,	rounded_tentative_loading_500
												,	rounded_tentative_loading_2000
												,	deno_100_priority
												,	deno_200_priority
												,	deno_500_priority
												,	deno_2000_priority
												,	is_deno_wise_cash_available
												,	priority_1_is_denomination_100
												,	priority_1_is_denomination_200
												,	priority_1_is_denomination_500
												,	priority_1_is_denomination_2000
												,	priority_1_is_remaining_amount_available_100
												,	priority_1_is_remaining_amount_available_200
												,	priority_1_is_remaining_amount_available_500
												,	priority_1_is_remaining_amount_available_2000
												,	priority_1_loading_amount_100
												,	priority_1_loading_amount_200
												,	priority_1_loading_amount_500
												,	priority_1_loading_amount_2000
												,	priority_2_is_denomination_100
												,	priority_2_is_denomination_200
												,	priority_2_is_denomination_500
												,	priority_2_is_denomination_2000
												,	priority_2_is_remaining_amount_available_100
												,	priority_2_is_remaining_amount_available_200
												,	priority_2_is_remaining_amount_available_500
												,	priority_2_is_remaining_amount_available_2000
												,	priority_2_loading_amount_100
												,	priority_2_loading_amount_200
												,	priority_2_loading_amount_500
												,	priority_2_loading_amount_2000
												,	priority_3_is_denomination_100
												,	priority_3_is_denomination_200
												,	priority_3_is_denomination_500
												,	priority_3_is_denomination_2000
												,	priority_3_is_remaining_amount_available_100
												,	priority_3_is_remaining_amount_available_200
												,	priority_3_is_remaining_amount_available_500
												,	priority_3_is_remaining_amount_available_2000
												,	priority_3_loading_amount_100
												,	priority_3_loading_amount_200
												,	priority_3_loading_amount_500
												,	priority_3_loading_amount_2000
												,	priority_4_is_denomination_100
												,	priority_4_is_denomination_200
												,	priority_4_is_denomination_500
												,	priority_4_is_denomination_2000
												,	priority_4_is_remaining_amount_available_100
												,	priority_4_is_remaining_amount_available_200
												,	priority_4_is_remaining_amount_available_500
												,	priority_4_is_remaining_amount_available_2000
												,	priority_4_loading_amount_100
												,	priority_4_loading_amount_200
												,	priority_4_loading_amount_500
												,	priority_4_loading_amount_2000
												,	loading_amount_100
												,	loading_amount_200
												,	loading_amount_500
												,	loading_amount_2000
												,	total_loading_amount
												,	total_closing_remaining_available_amount
												,	total_forecasted_remaining_amt
			)
			SELECT
									 @project_id								                                                            as project_id								
								   , @bank_code									                                                            as bank_code									
								   , @atmid										                                                            as atmid
								   , @feederbranchcode																						as feeder_branch_code		
								   , @total_opening_remaining_available_amount																as total_opening_remaining_available_amount
								   , COALESCE(@opening_remaining_available_amount_100		,0)												as opening_remaining_available_amount_100	
								   , COALESCE(@opening_remaining_available_amount_200	    ,0)                                             as opening_remaining_available_amount_200	
								   , COALESCE(@opening_remaining_available_amount_500	    ,0)                                             as opening_remaining_available_amount_500	
								   , COALESCE(@opening_remaining_available_amount_2000	    ,0)                                             as opening_remaining_available_amount_2000											
								   , @original_total_forecasted_amt							                                                as original_total_forecasted_amt							
								   , @original_forecasted_amt_100				                                                            as original_forecasted_amt_100				
								   , @original_forecasted_amt_200                                                                           as original_forecasted_amt_200
								   , @original_forecasted_amt_500                                                                           as original_forecasted_amt_500
								   , @original_forecasted_amt_2000                                                                          as original_forecasted_amt_2000
								   , @morning_balance_100                                                                                   as morning_balance_100
								   , @morning_balance_200                                                                                   as morning_balance_200
								   , @morning_balance_500                                                                                   as morning_balance_500
								   , @morning_balance_2000                                                                                  as morning_balance_2000
								   , @total_morning_balance                                                                                 as total_morning_balance
								   , @cassette_100_count_original                                                                           as cassette_100_count_original
								   , @cassette_200_count_original                                                                           as cassette_200_count_original
								   , @cassette_500_count_original                                                                           as cassette_500_count_original
								   , @cassette_2000_count_original                                                                          as cassette_2000_count_original
								   , @cassette_100_brand_capacity                                                                           as cassette_100_brand_capacity
								   , @cassette_200_brand_capacity                                                                           as cassette_200_brand_capacity
								   , @cassette_500_brand_capacity                                                                           as cassette_500_brand_capacity
								   , @cassette_2000_brand_capacity                                                                          as cassette_2000_brand_capacity
								   , @total_capacity_amount_100                                                                             as total_capacity_amount_100
								   , @total_capacity_amount_200                                                                             as total_capacity_amount_200
								   , @total_capacity_amount_500                                                                             as total_capacity_amount_500
								   , @total_capacity_amount_2000                                                                            as total_capacity_amount_2000
								   , @denomination_100_max_capacity_percentage                                                              as denomination_100_max_capacity_percentage
								   , @denomination_200_max_capacity_percentage                                                              as denomination_200_max_capacity_percentage
								   , @denomination_500_max_capacity_percentage                                                              as denomination_500_max_capacity_percentage
								   , @denomination_2000_max_capacity_percentage                                                             as denomination_2000_max_capacity_percentage
								   , @max_amt_allowed_100                                                                                   as max_amt_allowed_100
								   , @max_amt_allowed_200                                                                                   as max_amt_allowed_200
								   , @max_amt_allowed_500                                                                                   as max_amt_allowed_500
								   , @max_amt_allowed_2000                                                                                  as max_amt_allowed_2000
								   , @denomination_wise_round_off_100                                                                       as denomination_wise_round_off_100
								   , @denomination_wise_round_off_200                                                                       as denomination_wise_round_off_200
								   , @denomination_wise_round_off_500                                                                       as denomination_wise_round_off_500
								   , @denomination_wise_round_off_2000                                                                      as denomination_wise_round_off_2000
								   , @tentative_loading_100                                                                                 as tentative_loading_100 
								   , @tentative_loading_200                                                                                 as tentative_loading_200 
								   , @tentative_loading_500                                                                                 as tentative_loading_500 
								   , @tentative_loading_2000                                                                                as tentative_loading_2000
								   -- Max Capacity                                                                                          
								   , @rounded_tentative_loading_100		                                                                    as rounded_tentative_loading_100		
								   , @rounded_tentative_loading_200	                                                                        as rounded_tentative_loading_200	    
								   , @rounded_tentative_loading_500	                                                                        as rounded_tentative_loading_500	    
								   , @rounded_tentative_loading_2000	                                                                    as rounded_tentative_loading_2000	
								   , @deno_100_priority                                                                                     as deno_100_priority
								   , @deno_200_priority                                                                                     as deno_200_priority
								   , @deno_500_priority                                                                                     as deno_500_priority
								   , @deno_2000_priority                                                                                    as deno_2000_priority
								   , @is_deno_wise_cash_available                                                                           as is_deno_wise_cash_available
									---- If denomination wise is availabble                                                                 
									-- Priority of denomination                                                                             
								   , @priority_1_is_denomination_100                                                                        as priority_1_is_denomination_100
								   , @priority_1_is_denomination_200                                                                        as priority_1_is_denomination_200
								   , @priority_1_is_denomination_500                                                                        as priority_1_is_denomination_500
								   , @priority_1_is_denomination_2000 								   	                                    as priority_1_is_denomination_2000 								   	
								   , @priority_1_is_remaining_amount_available_100                                                          as priority_1_is_remaining_amount_available_100
								   , @priority_1_is_remaining_amount_available_200                                                          as priority_1_is_remaining_amount_available_200
								   , @priority_1_is_remaining_amount_available_500                                                          as priority_1_is_remaining_amount_available_500
								   , @priority_1_is_remaining_amount_available_2000                                                         as priority_1_is_remaining_amount_available_2000
								   , @priority_1_loading_amount_100                                                                         as priority_1_loading_amount_100
								   , @priority_1_loading_amount_200                                                                         as priority_1_loading_amount_200
								   , @priority_1_loading_amount_500                                                                         as priority_1_loading_amount_500
								   , @priority_1_loading_amount_2000								                                        as priority_1_loading_amount_2000								   
								   , @priority_2_is_denomination_100                                                                        as priority_2_is_denomination_100
								   , @priority_2_is_denomination_200                                                                        as priority_2_is_denomination_200
								   , @priority_2_is_denomination_500                                                                        as priority_2_is_denomination_500
								   , @priority_2_is_denomination_2000 								                                        as priority_2_is_denomination_2000 								   
								   , @priority_2_is_remaining_amount_available_100                                                          as priority_2_is_remaining_amount_available_100
								   , @priority_2_is_remaining_amount_available_200                                                          as priority_2_is_remaining_amount_available_200
								   , @priority_2_is_remaining_amount_available_500                                                          as priority_2_is_remaining_amount_available_500
								   , @priority_2_is_remaining_amount_available_2000                                                         as priority_2_is_remaining_amount_available_2000
								   , @priority_2_loading_amount_100                                                                         as priority_2_loading_amount_100
								   , @priority_2_loading_amount_200                                                                         as priority_2_loading_amount_200
								   , @priority_2_loading_amount_500                                                                         as priority_2_loading_amount_500
								   , @priority_2_loading_amount_2000								                                        as priority_2_loading_amount_2000								   
								   , @priority_3_is_denomination_100                                                                        as priority_3_is_denomination_100
								   , @priority_3_is_denomination_200                                                                        as priority_3_is_denomination_200
								   , @priority_3_is_denomination_500                                                                        as priority_3_is_denomination_500
								   , @priority_3_is_denomination_2000 								                                        as priority_3_is_denomination_2000 								   
								   , @priority_3_is_remaining_amount_available_100                                                          as priority_3_is_remaining_amount_available_100
								   , @priority_3_is_remaining_amount_available_200                                                          as priority_3_is_remaining_amount_available_200
								   , @priority_3_is_remaining_amount_available_500                                                          as priority_3_is_remaining_amount_available_500
								   , @priority_3_is_remaining_amount_available_2000								                            as priority_3_is_remaining_amount_available_2000								   
								   , @priority_3_loading_amount_100                                                                         as priority_3_loading_amount_100
								   , @priority_3_loading_amount_200                                                                         as priority_3_loading_amount_200
								   , @priority_3_loading_amount_500                                                                         as priority_3_loading_amount_500
								   , @priority_3_loading_amount_2000								                                        as priority_3_loading_amount_2000								   
								   , @priority_4_is_denomination_100                                                                        as priority_4_is_denomination_100
								   , @priority_4_is_denomination_200                                                                        as priority_4_is_denomination_200
								   , @priority_4_is_denomination_500                                                                        as priority_4_is_denomination_500
								   , @priority_4_is_denomination_2000 								                                        as priority_4_is_denomination_2000 								   
								   , @priority_4_is_remaining_amount_available_100                                                          as priority_4_is_remaining_amount_available_100
								   , @priority_4_is_remaining_amount_available_200                                                          as priority_4_is_remaining_amount_available_200
								   , @priority_4_is_remaining_amount_available_500                                                          as priority_4_is_remaining_amount_available_500
								   , @priority_4_is_remaining_amount_available_2000								                            as priority_4_is_remaining_amount_available_2000								   
								   , @priority_4_loading_amount_100                                                                         as priority_4_loading_amount_100
								   , @priority_4_loading_amount_200                                                                         as priority_4_loading_amount_200
								   , @priority_4_loading_amount_500                                                                         as priority_4_loading_amount_500
								   , @priority_4_loading_amount_2000								                                        as priority_4_loading_amount_2000								   
								   , @loading_amount_100                                                                                    as loading_amount_100
								   , @loading_amount_200                                                                                    as loading_amount_200
								   , @loading_amount_500                                                                                    as loading_amount_500
								   , @loading_amount_2000                                                                                   as loading_amount_2000
									-- Total loading amount								                                                    
								   , @loading_amount_100 + @loading_amount_200 + @loading_amount_500 + @loading_amount_2000                 as total_loading_amount
								   --closing available amount	                                                                           
								   , @total_remaining_available_amount                                                                      as total_closing_remaining_available_amount
								   , @total_forecasted_remaining_amt                                                                        as total_forecasted_remaining_amt



				

						END	--ELSE --If denoination wise cash is not available.						
						
						FETCH NEXT FROM cursor2 
									 INTO  @atmid
									 , @atm_priority							
									, @denomination_100_max_capacity_percentage
									, @denomination_200_max_capacity_percentage
									, @denomination_500_max_capacity_percentage
									, @denomination_2000_max_capacity_percentage
									, @morning_balance_100
									, @morning_balance_200
									, @morning_balance_500
									, @morning_balance_2000
									, @total_morning_balance
									, @original_forecasted_amt_100
									, @original_forecasted_amt_200
									, @original_forecasted_amt_500
									, @original_forecasted_amt_2000
									, @original_total_forecasted_amt
									--, @cassette_50_count_original  
									, @cassette_100_count_original 
									, @cassette_200_count_original 
									, @cassette_500_count_original 
									, @cassette_2000_count_original
									, @deno_100_priority
									, @deno_200_priority
									, @deno_500_priority
									, @deno_2000_priority
									, @cassette_100_brand_capacity
									, @cassette_200_brand_capacity
									, @cassette_500_brand_capacity
									, @cassette_2000_brand_capacity
					END			-- While End (cusror2)
				CLOSE cursor2 
				DEALLOCATE cursor2
			END

		
			--- Insert in temp table for feeder 

				
				INSERT INTO #temp_feeder_forecast
				(
						project_id						
					,	bank_code						
					,	feeder_branch_code							
					,	is_deno_wise_cash_available		
					,	total_remaining_available_amount
					,   remaining_avail_100				
					,   remaining_avail_200				
					,   remaining_avail_500				
					,   remaining_avail_2000			
				)

				SELECT	  @project_id
						, @bank_code
						, @feederbranchcode
						, @is_deno_wise_cash_available
						, @total_remaining_available_amount
						, @remaining_avail_100			
						, @remaining_avail_200			
						, @remaining_avail_500			
						, @remaining_avail_2000			


			


	FETCH NEXT FROM cursor1 INTO @project_id,@bank_code,@feederbranchcode, @total_remaining_available_amount, @remaining_avail_100, @remaining_avail_200, @remaining_avail_500, @remaining_avail_2000,@is_deno_wise_cash_available

   CLOSE cursor1 
 DEALLOCATE cursor1

 

 set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Cursor completed for allocating amount'
 
	drop table if exists #forecasted_dataset

	-- Create final dataset for atm level here

	
 set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #forecasted_Dataset started'

	-- Add flag indicating if cash pre availability is there
	select a.*
			--, case when b.Total_Amount_Available > 0 then 1 else 0 end as is_cash_pre_available
			, b.available_100_amount as		feeder_available_100_amount
			, b.available_200_amount as		feeder_available_200_amount
			, b.available_500_amount as		feeder_available_500_amount
			, b.available_2000_amount as	feeder_available_2000_amount
			, b.Total_Amount_Available as	feeder_total_amount_available					
	into #forecasted_dataset
	from #dist_with_cra  a
	left join #feeder_level_forecast b
	on a.project_id = b.project_id
	and a.bank_code = b.bank_code 
	and a.feeder_branch_code = b.feeder_branch_code
	
	--select * from #forecasted_dataset
	--select  * from #temp_cash_pre_availability
	--select * from #dist_with_cra
	--select * from #feeder_level_forecast
 set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #forecasted_Dataset Completed'
	--Prepare final dataset to insert into database
	drop table if exists #final_atm_level_allocated_dataset

	
 set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #final_atm_level_allocated_dataset started'

	select	distinct a.project_id
			,a.bank_code
			,a.feeder_branch_code
			,a.atm_priority
			,COALESCE(b.total_opening_remaining_available_amount,0) as total_opening_remaining_available_amount
			,a.atm_id
			,COALESCE(b.is_deno_wise_cash_available,a.is_cash_pre_available) as is_cash_pre_available
			,(rounded_amount_100 + rounded_amount_200 + rounded_amount_500 + rounded_amount_2000) as total_forecasted_amt
			,coalesce(b.total_loading_amount,0) as total_loading_amount
			,coalesce(b.total_loading_amount,0) as final_total_loading_amount ---total
			,coalesce(b.loading_amount_100,0)   as final_total_loading_amount_100		
			,coalesce(b.loading_amount_200,0)   as final_total_loading_amount_200
			,coalesce(b.loading_amount_500,0)   as final_total_loading_amount_500
			,coalesce(b.loading_amount_2000,0)  as final_total_loading_amount_2000
			,COALESCE(a.total_rounded_vault_amt,0) as total_atm_vaulting_amount_after_pre_availability --total
			,a.rounded_vaultingamount_100  as atm_vaulting_amount_after_pre_availability_100
			,a.rounded_vaultingamount_200  as atm_vaulting_amount_after_pre_availability_200
			,a.rounded_vaultingamount_500  as atm_vaulting_amount_after_pre_availability_500
			,a.rounded_vaultingamount_2000 as atm_vaulting_amount_after_pre_availability_2000
			--Add dennomination wise
	into #final_atm_level_allocated_dataset
	from #forecasted_dataset a
	left join 
		#temp_cash_pre_availability b
			on 
				a.project_id COLLATE DATABASE_DEFAULT = b.project_id COLLATE DATABASE_DEFAULT
				and a.bank_code COLLATE DATABASE_DEFAULT = b.bank_code COLLATE DATABASE_DEFAULT
				and a.feeder_branch_code COLLATE DATABASE_DEFAULT = a.feeder_branch_code COLLATE DATABASE_DEFAULT
				and a.atm_id = b.atmid  COLLATE DATABASE_DEFAULT	
	
	-- Create Feeder level dataset by grouping atm level data on basis of project, bank and feeder branch 
	--select * from #final_atm_level_allocated_dataset
	--select distinct * from #temp_cash_pre_availability
 set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #final_atm_level_allocated_dataset Completed'
	
	drop table if exists #final_feeder_level_allocated_dataset

	
 set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #final_feeder_level_allocated_dataset started'
	select distinct a.*
			
			,case when a.Total_Amount_Available > 0 then isnull(b.total_remaining_available_amount,0) else isnull(a.total_rounded_vault_amt,0) end as revised_total_vaulting_after_pre_availabiliy_allocation
			,case when a.available_100_amount > 0 then isnull(b.remaining_avail_100,0) else isnull(a.vaultingamount_100,0) end as revised_vaulting_after_pre_availabiliy_allocation_100
			,case when a.available_200_amount > 0 then isnull(b.remaining_avail_200,0) else isnull(a.vaultingamount_200,0) end as revised_vaulting_after_pre_availabiliy_allocation_200
			,case when a.available_500_amount > 0 then isnull(b.remaining_avail_500,0) else isnull(a.vaultingamount_500,0) end as revised_vaulting_after_pre_availabiliy_allocation_500
			,case when a.available_2000_amount > 0 then isnull(b.remaining_avail_2000,0) else isnull(a.vaultingamount_2000,0) end as revised_vaulting_after_pre_availabiliy_allocation_2000
			--Add for all denomination
			, c.revised_final_total_loading_amount
			, case when a.Total_Amount_Available > 0 then (isnull(c.revised_final_total_loading_amount,0) + isnull(b.total_remaining_available_amount,0)) else (isnull(c.revised_final_total_loading_amount,0) + isnull(a.total_rounded_vault_amt,0)) end as revised_total_indent_after_pre_availabiliy_allocation
			, case when a.available_100_amount > 0 then (isnull(c.revised_final_total_loading_amount_100,0) + isnull(b.remaining_avail_100,0)) else (isnull(c.revised_final_total_loading_amount_100,0) + isnull(a.vaultingamount_100,0)) end as revised_indent_after_pre_availabiliy_allocation_100
			, case when a.available_200_amount > 0 then (isnull(c.revised_final_total_loading_amount_200,0) + isnull(b.remaining_avail_200,0)) else (isnull(c.revised_final_total_loading_amount_200,0) + isnull(a.vaultingamount_200,0)) end as revised_indent_after_pre_availabiliy_allocation_200
			, case when a.available_500_amount > 0 then (isnull(c.revised_final_total_loading_amount_500,0) + isnull(b.remaining_avail_500,0)) else (isnull(c.revised_final_total_loading_amount_500,0) + isnull(a.vaultingamount_500,0)) end as revised_indent_after_pre_availabiliy_allocation_500
			, case when a.available_2000_amount > 0 then (isnull(c.revised_final_total_loading_amount_2000,0) + isnull(b.remaining_avail_2000,0)) else (isnull(c.revised_final_total_loading_amount_2000,0) + isnull(a.vaultingamount_2000,0)) end as revised_indent_after_pre_availabiliy_allocation_2000
			-- add for all allocation
	into #final_feeder_level_allocated_dataset
	from
	#feeder_level_forecast a	
	left join #temp_feeder_forecast b
	on a.project_id = b.project_id COLLATE DATABASE_DEFAULT
	and a.bank_code = b.bank_code COLLATE DATABASE_DEFAULT
	and a.feeder_branch_code = b.feeder_branch_code COLLATE DATABASE_DEFAULT
	left join 
	(
	select project_id
		 , bank_code
		 , feeder_branch_code
		 , sum(final_total_loading_amount) as revised_final_total_loading_amount
		 , SUM(final_total_loading_amount_100) as revised_final_total_loading_amount_100
		 , SUM(final_total_loading_amount_200) as revised_final_total_loading_amount_200
		 , SUM(final_total_loading_amount_500) as revised_final_total_loading_amount_500
		 , SUM(final_total_loading_amount_2000) as revised_final_total_loading_amount_2000
			--Add for all denomination
	  from #final_atm_level_allocated_dataset
	  group by project_id, bank_code, feeder_branch_code
	  ) c
	  on c.project_id = a.project_id
	  and c.bank_code = a.bank_code
	  and c.feeder_branch_code = a.feeder_branch_code

	  --select * from #temp_feeder_forecast
	 --select * from #final_feeder_level_allocated_dataset
 set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into #final_feeder_level_allocated_dataset Completed'

BEGIN TRANSACTION

 		DECLARE @row INT
		DECLARE @errorcode int

		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_detail Started'

		 INSERT INTO  distribution_planning_detail_V2
				(
				[indent_code]
				,[atm_id]
				,[site_code]
				,[sol_id]
				,[bank_code]
				,[project_id]
				,[indentdate]
				,[bank_cash_limit]
				,[insurance_limit]
				,[feeder_branch_code]
				,[cra]
				,[base_limit]
				,[dispenseformula]
				,[decidelimitdays]
				,[loadinglimitdays]
				,[avgdispense]
				,[loading_amount]
				,[morning_balance_50]
				,[morning_balance_100]
				,[morning_balance_200]
				,[morning_balance_500]
				,[morning_balance_2000]
				,[total_morning_balance]
				,[cassette_50_brand_capacity]
				,[cassette_100_brand_capacity]
				,[cassette_200_brand_capacity]
				,[cassette_500_brand_capacity]
				,[cassette_2000_brand_capacity]
				,[cassette_50_count]
				,[cassette_100_count]
				,[cassette_200_count]
				,[cassette_500_count]
				,[cassette_2000_count]
				,[limit_amount]
				,[total_capacity_amount_50]
				,[total_capacity_amount_100]
				,[total_capacity_amount_200]
				,[total_capacity_amount_500]
				,[total_capacity_amount_2000]
				,[total_cassette_capacity]
				,[avgdecidelimit]
				,[DecideLimit]
				,[threshold_limit]
				,[loadinglimit]
				,[applied_vaulting_percentage]
				,[ignore_code]
				,[ignore_description]
				,[dist_Purpose]
				,[defaultamt]
				,[default_flag]
				,[curbal_div_avgDisp]
				,[loadingGap_cur_bal_avg_disp]
				,[CashOut]
				,[remaining_balance_50]
				,[remaining_balance_100]
				,[remaining_balance_200]
				,[remaining_balance_500]
				,[remaining_balance_2000]
				--,[cassette_50_capacity_amount]
				--,[cassette_100_capacity_amount]
				--,[cassette_200_capacity_amount]
				--,[cassette_500_capacity_amount]
				--,[cassette_2000_capacity_amount]
				--,[total_cassette_capacity_amount]
				,[cassette_capacity_percentage_50]
				,[cassette_capacity_percentage_100]
				,[cassette_capacity_percentage_200]
				,[cassette_capacity_percentage_500]
				,[cassette_capacity_percentage_2000]
				,max_loading_capacity_amount_100  
				,max_loading_capacity_amount_200	 
				,max_loading_capacity_amount_500	 
				,max_loading_capacity_amount_2000 
				--,[tentative_loading_amount_50]
				--,[tentative_loading_amount_100]
				--,[tentative_loading_amount_200]
				--,[tentative_loading_amount_500]
				--,[tentative_loading_amount_2000]
				--,[rounded_amount_50]
				,[rounded_amount_100]
				,[rounded_amount_200]
				,[rounded_amount_500]
				,[rounded_amount_2000]
				,[total_expected_balanceT1]
				,[expected_balanceT1_50]
				,[expected_balanceT1_100]
				,[expected_balanceT1_200]
				,[expected_balanceT1_500]
				,[expected_balanceT1_2000]
				,[total_vault_amount]
				,[vaultingamount_50]
				,[vaultingamount_100]
				,[vaultingamount_200]
				,[vaultingamount_500]
				,[vaultingamount_2000]
				,[rounded_vaultingamount_50]
				,[rounded_vaultingamount_100]
				,[rounded_vaultingamount_200]
				,[rounded_vaultingamount_500]
				,[rounded_vaultingamount_2000]
				,[total_rounded_vault_amt]
				, total_opening_remaining_available_amount	
				, opening_remaining_available_amount_100	
				, opening_remaining_available_amount_200	
				, opening_remaining_available_amount_500	
				, opening_remaining_available_amount_2000	
				, cassette_100_count_original				
				, cassette_200_count_original				
				, cassette_500_count_original				
				, cassette_2000_count_original				
				, denomination_100_max_capacity_percentage	
				, denomination_200_max_capacity_percentage	
				, denomination_500_max_capacity_percentage	
				, denomination_2000_max_capacity_percentage	
				, max_amt_allowed_100						
				, max_amt_allowed_200						
				, max_amt_allowed_500						
				, max_amt_allowed_2000						
				, denomination_wise_round_off_100			
				, denomination_wise_round_off_200			
				, denomination_wise_round_off_500			
				, denomination_wise_round_off_2000			
				--, tentative_loading_100						
				--, tentative_loading_200						
				--, tentative_loading_500						
				--, tentative_loading_2000					
				, rounded_tentative_loading_100				
				, rounded_tentative_loading_200				
				, rounded_tentative_loading_500				
				, rounded_tentative_loading_2000			
				, deno_100_priority							
				, deno_200_priority							
				, deno_500_priority							
				, deno_2000_priority							
				, is_deno_wise_cash_available						
				, priority_1_is_denomination_100					
				, priority_1_is_denomination_200					
				, priority_1_is_denomination_500					
				, priority_1_is_denomination_2000					
				, priority_1_is_remaining_amount_available_100		
				, priority_1_is_remaining_amount_available_200		
				, priority_1_is_remaining_amount_available_500		
				, priority_1_is_remaining_amount_available_2000		
				, priority_1_is_remaining_capacity_available_100	
				, priority_1_is_remaining_capacity_available_200	
				, priority_1_is_remaining_capacity_available_500	
				, priority_1_is_remaining_capacity_available_2000	
				, priority_1_loading_amount_100						
				, priority_1_loading_amount_200						
				, priority_1_loading_amount_500						
				, priority_1_loading_amount_2000					
				, priority_2_is_denomination_100					
				, priority_2_is_denomination_200					
				, priority_2_is_denomination_500					
				, priority_2_is_denomination_2000					
				, priority_2_is_remaining_amount_available_100		
				, priority_2_is_remaining_amount_available_200		
				, priority_2_is_remaining_amount_available_500		
				, priority_2_is_remaining_amount_available_2000		
				, priority_2_is_remaining_capacity_available_100	
				, priority_2_is_remaining_capacity_available_200	
				, priority_2_is_remaining_capacity_available_500	
				, priority_2_is_remaining_capacity_available_2000	
				, priority_2_loading_amount_100						
				, priority_2_loading_amount_200						
				, priority_2_loading_amount_500						
				, priority_2_loading_amount_2000					
				, priority_3_is_denomination_100					
				, priority_3_is_denomination_200					
				, priority_3_is_denomination_500					
				, priority_3_is_denomination_2000					
				, priority_3_is_remaining_amount_available_100		
				, priority_3_is_remaining_amount_available_200		
				, priority_3_is_remaining_amount_available_500		
				, priority_3_is_remaining_amount_available_2000		
				, priority_3_is_remaining_capacity_available_100	
				, priority_3_is_remaining_capacity_available_200	
				, priority_3_is_remaining_capacity_available_500	
				, priority_3_is_remaining_capacity_available_2000	
				, priority_3_loading_amount_100						
				, priority_3_loading_amount_200						
				, priority_3_loading_amount_500						
				, priority_3_loading_amount_2000					
				, priority_4_is_denomination_100					
				, priority_4_is_denomination_200					
				, priority_4_is_denomination_500					
				, priority_4_is_denomination_2000					
				, priority_4_is_remaining_amount_available_100		
				, priority_4_is_remaining_amount_available_200		
				, priority_4_is_remaining_amount_available_500		
				, priority_4_is_remaining_amount_available_2000		
				, priority_4_is_remaining_capacity_available_100	
				, priority_4_is_remaining_capacity_available_200	
				, priority_4_is_remaining_capacity_available_500	
				, priority_4_is_remaining_capacity_available_2000	
				, priority_4_loading_amount_100						
				, priority_4_loading_amount_200						
				, priority_4_loading_amount_500						
				, priority_4_loading_amount_2000					
				, pre_avail_loading_amount_100								
				, pre_avail_loading_amount_200								
				, pre_avail_loading_amount_500								
				, pre_avail_loading_amount_2000								
				, pre_avail_total_loading_amount								
				, remaining_capacity_amount_100						
				, remaining_capacity_amount_200						
				, remaining_capacity_amount_500						
				, remaining_capacity_amount_2000					
				, closing_remaining_available_amount_100			
				, closing_remaining_available_amount_200			
				, closing_remaining_available_amount_500			
				, closing_remaining_available_amount_2000			
				, total_closing_remaining_available_amount			
				, total_forecasted_remaining_amt					
				, indent_counter 									
				, record_status
				,created_on
				,created_by
				,created_reference_id
				,final_total_loading_amount                         
				,final_total_loading_amount_100						
				,final_total_loading_amount_200						
				,final_total_loading_amount_500						
				,final_total_loading_amount_2000					
				,total_atm_vaulting_amount_after_pre_availability	
				,atm_vaulting_amount_after_pre_availability_100		
				,atm_vaulting_amount_after_pre_availability_200		
				,atm_vaulting_amount_after_pre_availability_500		
				,atm_vaulting_amount_after_pre_availability_2000	
				,atm_priority									
				,is_cash_pre_available		
				,previous_indent_code						
				)
			  SELECT	indent_order_number
					,	dist.atm_id
					,   dist.site_code
					,	dist.sol_id
					,	dist.bank_code
					,	dist.project_id
					,	dist.indentdate
					,	dist.bank_cash_limit
					,	dist.insurance_limit
					,	dist.feeder_branch_code
					,	dist.cra
					,   dist.base_limit
					,	dist.dispenseformula
					,	dist.decidelimitdays
					,	dist.loadinglimitdays
					,	dist.avgdispense
					,	dist.loading_amount
					,	dist.morning_balance_50
					,	dist.morning_balance_100
					,	dist.morning_balance_200
					,	dist.morning_balance_500
					,	dist.morning_balance_2000
					,	dist.total_morning_balance
					,	cassette_50_brand_capacity
					,	COALESCE(temp.cassette_100_brand_capacity,dist.cassette_100_brand_capacity) as cassette_100_brand_capacity
					,	COALESCE(temp.cassette_200_brand_capacity,dist.cassette_200_brand_capacity) as cassette_200_brand_capacity
					,	COALESCE(temp.cassette_500_brand_capacity,dist.cassette_500_brand_capacity) as cassette_500_brand_capacity
					,	COALESCE(temp.cassette_2000_brand_capacity,dist.cassette_2000_brand_capacity) as cassette_2000_brand_capacity
					,	dist.cassette_50_count
					,	dist.cassette_100_count
					,	dist.cassette_200_count
					,	dist.cassette_500_count
					,	dist.cassette_2000_count
					,	limit_amount
					,	total_capacity_amount_50
					,	COALESCE(temp.total_capacity_amount_100,dist.total_capacity_amount_100) as total_capacity_amount_100
					,	COALESCE(temp.total_capacity_amount_200,dist.total_capacity_amount_200) as total_capacity_amount_200
					,	COALESCE(temp.total_capacity_amount_500,dist.total_capacity_amount_500) as total_capacity_amount_500
					,	COALESCE(temp.total_capacity_amount_2000,dist.total_capacity_amount_2000) as total_capacity_amount_2000
					,	total_cassette_capacity
					,	avgdecidelimit
					,	DecideLimit
					,	threshold_limit
					,	loadinglimit
					,   applied_vaulting_percentage
					,   ignore_code
			        ,   ignore_description 
					,   dist_Purpose
					,   defaultamt
					,   default_flag
					,   curbal_div_avgDisp
					,   loadingGap_cur_bal_avg_disp
					,   CashOut	
					,	remaining_balance_50
					,	remaining_balance_100
					,	remaining_balance_200
					,	remaining_balance_500
					,	remaining_balance_2000
					--,	cassette_50_capacity_amount
					--,	cassette_100_capacity_amount
					--,	cassette_200_capacity_amount
					--,	cassette_500_capacity_amount
					--,	cassette_2000_capacity_amount
					--,	total_cassette_capacity_amount
					,	cassette_capacity_percentage_50
					,	cassette_capacity_percentage_100
					,	cassette_capacity_percentage_200
					,	cassette_capacity_percentage_500
					,	cassette_capacity_percentage_2000
					,	max_loading_capacity_amount_100
					,	max_loading_capacity_amount_200
					,	max_loading_capacity_amount_500
					,	max_loading_capacity_amount_2000
					--,	tentative_loading_amount_50
					--,	tentative_loading_amount_100
					--,	tentative_loading_amount_200
					--,	tentative_loading_amount_500
					--,	tentative_loading_amount_2000
					--,   NULL
					,   rounded_amount_100
					,   rounded_amount_200
					,   rounded_amount_500
					,   rounded_amount_2000
					,	total_expected_balanceT1
					,	expected_balanceT1_50
					,	expected_balanceT1_100
					,	expected_balanceT1_200
					,	expected_balanceT1_500
					,	expected_balanceT1_2000
					,	total_vault_amount
					,	vaultingamount_50
					,	vaultingamount_100
					,	vaultingamount_200
					,	vaultingamount_500
					,	vaultingamount_2000
					,	rounded_vaultingamount_50
					,	rounded_vaultingamount_100
					,	rounded_vaultingamount_200
					,	rounded_vaultingamount_500
					,	rounded_vaultingamount_2000
					,	total_rounded_vault_amt
					, atm.total_opening_remaining_available_amount	
					, COALESCE(temp.opening_remaining_available_amount_100	,0) as opening_remaining_available_amount_100
					, COALESCE(temp.opening_remaining_available_amount_200	,0) as opening_remaining_available_amount_200
					, COALESCE(temp.opening_remaining_available_amount_500	,0) as opening_remaining_available_amount_500
					, COALESCE(temp.opening_remaining_available_amount_2000	,0) as opening_remaining_available_amount_2000
					, COALESCE(temp.cassette_100_count_original,dist.cassette_100_count)  as	 cassette_100_count_original
					, COALESCE(temp.cassette_200_count_original,dist.cassette_200_count)  as	 cassette_200_count_original			
					, COALESCE(temp.cassette_500_count_original,dist.cassette_500_count)  as	 cassette_500_count_original			
					, COALESCE(temp.cassette_2000_count_original,dist.cassette_2000_count)  as	 cassette_2000_count_original				
					, COALESCE(temp.denomination_100_max_capacity_percentage, dist.denomination_100_max_capacity_percentage)	 as denomination_100_max_capacity_percentage
					, COALESCE(temp.denomination_200_max_capacity_percentage, dist.denomination_200_max_capacity_percentage) as denomination_200_max_capacity_percentage
					, COALESCE(temp.denomination_500_max_capacity_percentage, dist.denomination_500_max_capacity_percentage) as denomination_500_max_capacity_percentage
					, COALESCE(temp.denomination_2000_max_capacity_percentage,dist.denomination_2000_max_capacity_percentage) as denomination_2000_max_capacity_percentage
					, COALESCE(temp.max_amt_allowed_100,0		)	 as max_amt_allowed_100			
					, COALESCE(temp.max_amt_allowed_200	,0		)	 as max_amt_allowed_200		
					, COALESCE(temp.max_amt_allowed_500	,0		)	 as max_amt_allowed_500	
					, COALESCE(temp.max_amt_allowed_2000	,0)		 as max_amt_allowed_2000			
					, COALESCE(temp.denomination_wise_round_off_100,sa.denomination_wise_round_off_100)			as denomination_wise_round_off_100
					, COALESCE(temp.denomination_wise_round_off_200	,sa.denomination_wise_round_off_200)		as denomination_wise_round_off_200
					, COALESCE(temp.denomination_wise_round_off_500	,sa.denomination_wise_round_off_500)		as denomination_wise_round_off_500
					, COALESCE(temp.denomination_wise_round_off_2000	,sa.denomination_wise_round_off_2000)	as denomination_wise_round_off_2000	
					--, COALESCE(temp.tentative_loading_100	, dist.	tentative_loading_amount_100)		 as tentative_loading_100		
					--, COALESCE(temp.tentative_loading_200	, dist.	tentative_loading_amount_200)		 as tentative_loading_200							
					--, COALESCE(temp.tentative_loading_500	, dist.	tentative_loading_amount_500)		 as tentative_loading_500							
					--, COALESCE(temp.tentative_loading_2000	, dist.	tentative_loading_amount_2000)		 as tentative_loading_2000						
					, temp.rounded_tentative_loading_100				
					, temp.rounded_tentative_loading_200				
					, temp.rounded_tentative_loading_500				
					, temp.rounded_tentative_loading_2000			
					, COALESCE(temp.deno_100_priority  , sa.deno_100_priority) as deno_100_priority								
					, COALESCE(temp.deno_200_priority  , sa.deno_200_priority) as deno_200_priority 										
					, COALESCE(temp.deno_500_priority  , sa.deno_500_priority) as deno_500_priority										
					, COALESCE(temp.deno_2000_priority , sa.deno_2000_priority) as deno_2000_priority									
					, temp.is_deno_wise_cash_available							
					, COALESCE(temp.priority_1_is_denomination_100						,0) as priority_1_is_denomination_100						
					, COALESCE(temp.priority_1_is_denomination_200						,0) as priority_1_is_denomination_200						
					, COALESCE(temp.priority_1_is_denomination_500						,0)	as priority_1_is_denomination_500						
					, COALESCE(temp.priority_1_is_denomination_2000						,0)	as priority_1_is_denomination_2000						
					, COALESCE(temp.priority_1_is_remaining_amount_available_100		,0)	as priority_1_is_remaining_amount_available_100		
					, COALESCE(temp.priority_1_is_remaining_amount_available_200		,0)	as priority_1_is_remaining_amount_available_200		
					, COALESCE(temp.priority_1_is_remaining_amount_available_500		,0)	as priority_1_is_remaining_amount_available_500		
					, COALESCE(temp.priority_1_is_remaining_amount_available_2000		,0)	as priority_1_is_remaining_amount_available_2000		
					, COALESCE(temp.priority_1_is_remaining_capacity_available_100		,0)	as priority_1_is_remaining_capacity_available_100		
					, COALESCE(temp.priority_1_is_remaining_capacity_available_200		,0)	as priority_1_is_remaining_capacity_available_200		
					, COALESCE(temp.priority_1_is_remaining_capacity_available_500		,0)	as priority_1_is_remaining_capacity_available_500		
					, COALESCE(temp.priority_1_is_remaining_capacity_available_2000		,0)	as priority_1_is_remaining_capacity_available_2000		
					, COALESCE(temp.priority_1_loading_amount_100						,0)	as priority_1_loading_amount_100						
					, COALESCE(temp.priority_1_loading_amount_200						,0)	as priority_1_loading_amount_200						
					, COALESCE(temp.priority_1_loading_amount_500						,0)	as priority_1_loading_amount_500						
					, COALESCE(temp.priority_1_loading_amount_2000						,0)	as priority_1_loading_amount_2000						
					, COALESCE(temp.priority_2_is_denomination_100						,0)	as priority_2_is_denomination_100						
					, COALESCE(temp.priority_2_is_denomination_200						,0)	as priority_2_is_denomination_200						
					, COALESCE(temp.priority_2_is_denomination_500						,0)	as priority_2_is_denomination_500						
					, COALESCE(temp.priority_2_is_denomination_2000						,0)	as priority_2_is_denomination_2000						
					, COALESCE(temp.priority_2_is_remaining_amount_available_100		,0)	as priority_2_is_remaining_amount_available_100		
					, COALESCE(temp.priority_2_is_remaining_amount_available_200		,0)	as priority_2_is_remaining_amount_available_200		
					, COALESCE(temp.priority_2_is_remaining_amount_available_500		,0)	as priority_2_is_remaining_amount_available_500		
					, COALESCE(temp.priority_2_is_remaining_amount_available_2000		,0)	as priority_2_is_remaining_amount_available_2000		
					, COALESCE(temp.priority_2_is_remaining_capacity_available_100		,0)	as priority_2_is_remaining_capacity_available_100		
					, COALESCE(temp.priority_2_is_remaining_capacity_available_200		,0)	as priority_2_is_remaining_capacity_available_200		
					, COALESCE(temp.priority_2_is_remaining_capacity_available_500		,0)	as priority_2_is_remaining_capacity_available_500		
					, COALESCE(temp.priority_2_is_remaining_capacity_available_2000		,0)	as priority_2_is_remaining_capacity_available_2000		
					, COALESCE(temp.priority_2_loading_amount_100						,0)	as priority_2_loading_amount_100						
					, COALESCE(temp.priority_2_loading_amount_200						,0)	as priority_2_loading_amount_200						
					, COALESCE(temp.priority_2_loading_amount_500						,0)	as priority_2_loading_amount_500						
					, COALESCE(temp.priority_2_loading_amount_2000						,0)	as priority_2_loading_amount_2000						
					, COALESCE(temp.priority_3_is_denomination_100						,0)	as priority_3_is_denomination_100						
					, COALESCE(temp.priority_3_is_denomination_200						,0)	as priority_3_is_denomination_200						
					, COALESCE(temp.priority_3_is_denomination_500						,0)	as priority_3_is_denomination_500						
					, COALESCE(temp.priority_3_is_denomination_2000						,0)	as priority_3_is_denomination_2000						
					, COALESCE(temp.priority_3_is_remaining_amount_available_100		,0)	as priority_3_is_remaining_amount_available_100		
					, COALESCE(temp.priority_3_is_remaining_amount_available_200		,0)	as priority_3_is_remaining_amount_available_200		
					, COALESCE(temp.priority_3_is_remaining_amount_available_500		,0)	as priority_3_is_remaining_amount_available_500		
					, COALESCE(temp.priority_3_is_remaining_amount_available_2000		,0)	as priority_3_is_remaining_amount_available_2000		
					, COALESCE(temp.priority_3_is_remaining_capacity_available_100		,0)	as priority_3_is_remaining_capacity_available_100		
					, COALESCE(temp.priority_3_is_remaining_capacity_available_200		,0)	as priority_3_is_remaining_capacity_available_200		
					, COALESCE(temp.priority_3_is_remaining_capacity_available_500		,0)	as priority_3_is_remaining_capacity_available_500		
					, COALESCE(temp.priority_3_is_remaining_capacity_available_2000		,0)	as priority_3_is_remaining_capacity_available_2000		
					, COALESCE(temp.priority_3_loading_amount_100						,0)	as priority_3_loading_amount_100						
					, COALESCE(temp.priority_3_loading_amount_200						,0)	as priority_3_loading_amount_200						
					, COALESCE(temp.priority_3_loading_amount_500						,0)	as priority_3_loading_amount_500						
					, COALESCE(temp.priority_3_loading_amount_2000						,0)	as priority_3_loading_amount_2000						
					, COALESCE(temp.priority_4_is_denomination_100						,0)	as priority_4_is_denomination_100						
					, COALESCE(temp.priority_4_is_denomination_200						,0)	as priority_4_is_denomination_200						
					, COALESCE(temp.priority_4_is_denomination_500						,0)	as priority_4_is_denomination_500						
					, COALESCE(temp.priority_4_is_denomination_2000						,0)	as priority_4_is_denomination_2000						
					, COALESCE(temp.priority_4_is_remaining_amount_available_100		,0)	as priority_4_is_remaining_amount_available_100		
					, COALESCE(temp.priority_4_is_remaining_amount_available_200		,0)	as priority_4_is_remaining_amount_available_200		
					, COALESCE(temp.priority_4_is_remaining_amount_available_500		,0)	as priority_4_is_remaining_amount_available_500		
					, COALESCE(temp.priority_4_is_remaining_amount_available_2000		,0)	as priority_4_is_remaining_amount_available_2000		
					, COALESCE(temp.priority_4_is_remaining_capacity_available_100		,0)	as priority_4_is_remaining_capacity_available_100		
					, COALESCE(temp.priority_4_is_remaining_capacity_available_200		,0)	as priority_4_is_remaining_capacity_available_200		
					, COALESCE(temp.priority_4_is_remaining_capacity_available_500		,0)	as priority_4_is_remaining_capacity_available_500		
					, COALESCE(temp.priority_4_is_remaining_capacity_available_2000		,0)	as priority_4_is_remaining_capacity_available_2000		
					, COALESCE(temp.priority_4_loading_amount_100						,0)	as priority_4_loading_amount_100						
					, COALESCE(temp.priority_4_loading_amount_200						,0)	as priority_4_loading_amount_200						
					, COALESCE(temp.priority_4_loading_amount_500						,0)	as priority_4_loading_amount_500						
					, COALESCE(temp.priority_4_loading_amount_2000						,0)	as priority_4_loading_amount_2000						
					, COALESCE(loading_amount_100,  rounded_amount_100) as pre_avail_loading_amount_100				
					, COALESCE(loading_amount_200	, rounded_amount_200) as pre_avail_loading_amount_200								
					, COALESCE(loading_amount_500	, rounded_amount_500) as pre_avail_loading_amount_500								
					, COALESCE(loading_amount_2000	, rounded_amount_2000) as pre_avail_loading_amount_2000								
					, COALESCE(atm.Total_loading_amount, rounded_amount_100+rounded_amount_200+rounded_amount_500+rounded_amount_2000) as pre_avail_total_loading_amount					
					, COALESCE(temp.remaining_capacity_amount_100					,0)		as remaining_capacity_amount_100			
					, COALESCE(temp.remaining_capacity_amount_200					,0)		as remaining_capacity_amount_200			
					, COALESCE(temp.remaining_capacity_amount_500					,0)		as remaining_capacity_amount_500			
					, COALESCE(temp.remaining_capacity_amount_2000					,0)		as remaining_capacity_amount_2000			
					, COALESCE(temp.closing_remaining_available_amount_100			,0)		as closing_remaining_available_amount_100	
					, COALESCE(temp.closing_remaining_available_amount_200			,0)		as closing_remaining_available_amount_200	
					, COALESCE(temp.closing_remaining_available_amount_500			,0)		as closing_remaining_available_amount_500	
					, COALESCE(temp.closing_remaining_available_amount_2000			,0)		as closing_remaining_available_amount_2000	
					, COALESCE(temp.total_closing_remaining_available_amount		,0)		as total_closing_remaining_available_amount
					, COALESCE(temp.total_forecasted_remaining_amt					,0)		as total_forecasted_remaining_amt			
					, dist.indent_counter + 1 										 
					,	'Review Pending'
					,	@timestamp_date
					,	@cur_user
					,	@created_reference_id
					,atm.final_total_loading_amount                         
					,atm.final_total_loading_amount_100						
					,atm.final_total_loading_amount_200						
					,atm.final_total_loading_amount_500						
					,atm.final_total_loading_amount_2000					
					,atm.total_atm_vaulting_amount_after_pre_availability	
					,atm.atm_vaulting_amount_after_pre_availability_100		
					,atm.atm_vaulting_amount_after_pre_availability_200		
					,atm.atm_vaulting_amount_after_pre_availability_500		
					,atm.atm_vaulting_amount_after_pre_availability_2000	
					,dist.atm_priority										
					,atm.is_cash_pre_available
					,indent_code								
					FROM #dist_with_cra dist
					CROSS JOIN system_settings sa
					LEFT JOIN #final_atm_level_allocated_dataset atm
					on atm.atm_id  = dist.atm_id
					AND atm.bank_code = dist.bank_code
					and atm.project_id = dist.project_id
					LEFT JOIN #temp_cash_pre_availability temp
					on		temp.atmid COLLATE DATABASE_DEFAULT = dist.atm_id COLLATE DATABASE_DEFAULT
					AND		temp.project_id  COLLATE DATABASE_DEFAULT= dist.project_id COLLATE DATABASE_DEFAULT
					AND		temp.bank_code COLLATE DATABASE_DEFAULT = dist.bank_code COLLATE DATABASE_DEFAULT
					WHERE sa.record_status = 'Active'
				

				--select * from distribution_planning_detail order by created_on desc
				SELECT @row	= @@ROWCOUNT,@ErrorCode = @@error

				set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_detail Completed'

				set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_master Started'



					INSERT INTO distribution_planning_master_V2
					( 
					[indent_code]
				   ,[indentdate]
				   ,[project_id]
				   ,[bank_code]
				   ,[feeder_branch_code]
				   ,[cra]
				   ,[max_loading_amount]
				  -- ,[max_loading_amount_50]
				   ,[max_loading_amount_100]
				   ,[max_loading_amount_200]
				   ,[max_loading_amount_500]
				   ,[max_loading_amount_2000]
				   ,[min_loading_amount]
				   ,[forecast_loading_amount]
				  -- ,[forecast_loading_amount_50]
				   ,[forecast_loading_amount_100]
				   ,[forecast_loading_amount_200]
				   ,[forecast_loading_amount_500]
				   ,[forecast_loading_amount_2000]
				   ,[total_cassette_capacity]
				   --,[cassette_50_capacity_amount]
				   --,[cassette_100_capacity_amount]
				   --,[cassette_200_capacity_amount]
				   --,[cassette_500_capacity_amount]
				   --,[cassette_2000_capacity_amount]
				   ,[vaultingamount_50]
				   ,[vaultingamount_100]
				   ,[vaultingamount_200]
				   ,[vaultingamount_500]
				   ,[vaultingamount_2000]
				   ,[total_rounded_vault_amt]
				   ,[vault_balance_100]
				   ,[vault_balance_200]
				   ,[vault_balance_500]
				   ,[vault_balance_2000]
				   ,[total_vault_balance]
				   ,is_deno_wise_cash_available		
				   ,Total_Amount_Available
				   ,available_100_amount				
				   ,available_200_amount				
				   ,available_500_amount				
				   ,available_2000_amount			
				   --,[indent_50]
				   ,[indent_100]
				   ,[indent_200]
				   ,[indent_500]
				   ,[indent_2000]
				   ,[record_status]
				   ,[created_on]
				   ,[created_by]
				   ,[created_reference_id]
				   ,[indent_counter]
				   ,[total_vaulting_after_pre_availabiliy_allocation]   
				   ,[vaulting_after_pre_availabiliy_allocation_100]	    
				   ,[vaulting_after_pre_availabiliy_allocation_200]	    
				   ,[vaulting_after_pre_availabiliy_allocation_500]	    
				   ,[vaulting_after_pre_availabiliy_allocation_2000]	
				   ,[final_total_loading_amount]						
				   ,[total_indent_after_pre_availabiliy_allocation]		
				   ,[indent_after_pre_availabiliy_allocation_100]		
				   ,[indent_after_pre_availabiliy_allocation_200]		
				   ,[indent_after_pre_availabiliy_allocation_500]		
				   ,[indent_after_pre_availabiliy_allocation_2000]
				   ,previous_indent_code					   
				   )
				SELECT 	
					 indent_order_number
					,indentdate
					,temp.project_id
					,temp.bank_code
					,temp.feeder_branch_code
					,cra
					,max_loading_amount
					--,max_loading_amount_50
					,max_loading_amount_100
					,max_loading_amount_200
					,max_loading_amount_500
					,max_loading_amount_2000
					,min_loading_amount
					,forecast_loading_amount
					--,forecast_loading_amount_50
					,forecast_loading_amount_100
					,forecast_loading_amount_200
					,forecast_loading_amount_500
					,forecast_loading_amount_2000
					,total_cassette_capacity
					--,cassette_50_capacity_amount
					--,cassette_100_capacity_amount
					--,cassette_200_capacity_amount
					--,cassette_500_capacity_amount
					--,cassette_2000_capacity_amount
					,vaultingamount_50
					,vaultingamount_100
					,vaultingamount_200
					,vaultingamount_500
					,vaultingamount_2000
					,total_rounded_vault_amt
					,vault_balance_100
					,vault_balance_200
					,vault_balance_500
					,vault_balance_2000
					,total_vault_balance
					,feeder.is_deno_wise_cash_available		
					,total_remaining_available_amount
					,remaining_avail_100				
					,remaining_avail_200				
					,remaining_avail_500				
					,remaining_avail_2000			
					--,indent_50
					,indent_100
					,indent_200
					,indent_500
					,indent_2000
					,	'Review Pending'
					,	@timestamp_date
					,	@cur_user
					,	@created_reference_id
					, indent_counter + 1
					,[revised_total_vaulting_after_pre_availabiliy_allocation]
					,[revised_vaulting_after_pre_availabiliy_allocation_100]	
					,[revised_vaulting_after_pre_availabiliy_allocation_200]	
					,[revised_vaulting_after_pre_availabiliy_allocation_500]	
					,[revised_vaulting_after_pre_availabiliy_allocation_2000]
					,[final_total_loading_amount]					
					,revised_total_indent_after_pre_availabiliy_allocation	
					,[revised_indent_after_pre_availabiliy_allocation_100]	
					,[revised_indent_after_pre_availabiliy_allocation_200]	
					,[revised_indent_after_pre_availabiliy_allocation_500]	
					,[revised_indent_after_pre_availabiliy_allocation_2000]	
					,indent_code
				FROM #final_feeder_level_allocated_dataset feeder
				LEFT JOIN #temp_feeder_forecast temp
				on feeder.bank_code COLLATE DATABASE_DEFAULT= temp.bank_code COLLATE DATABASE_DEFAULT
				AND feeder.feeder_branch_code COLLATE DATABASE_DEFAULT= temp.feeder_branch_code COLLATE DATABASE_DEFAULT
				AND feeder.project_id COLLATE DATABASE_DEFAULT = temp.project_id COLLATE DATABASE_DEFAULT
	
	--select * from #final_feeder_level_allocated_dataset
	--select * from #temp_feeder_forecast

	--select * from distribution_planning_master order by created_on desc
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into distribution_planning_detail Completed'

--- Insert into indent master table-------			
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into indent_master Started'
		
				INSERT INTO indent_master
					(  [indent_order_number]
					  ,[order_date]
					  ,[collection_date]
					  ,[replenishment_date]
					  ,[cypher_code]
					  ,[bank]
					  ,[feeder_branch]
					  ,[cra]
					  ,[total_atm_loading_amount_50]
					  ,[total_atm_loading_amount_100]
					  ,[total_atm_loading_amount_200]
					  ,[total_atm_loading_amount_500]
					  ,[total_atm_loading_amount_2000]
					  ,[total_atm_loading_amount]
					  ,[cra_opening_vault_balance_50]
					  ,[cra_opening_vault_balance_100]
					  ,[cra_opening_vault_balance_200]
					  ,[cra_opening_vault_balance_500]
					  ,[cra_opening_vault_balance_2000]
					  ,[cra_opening_vault_balance]
					  ,[total_bank_withdrawal_amount_50]
					  ,[total_bank_withdrawal_amount_100]
					  ,[total_bank_withdrawal_amount_200]
					  ,[total_bank_withdrawal_amount_500]
					  ,[total_bank_withdrawal_amount_2000]
					  ,[total_bank_withdrawal_amount]
					  ,[authorized_by_1]
					  ,[authorized_by_2]
					  ,[notes]
					  ,[amount_in_words]
					  ,[indent_type]
					  ,[project_id]
					  ,[record_status]
					  ,[created_on]
					  ,[indent_counter]
					  ,created_reference_id
					  ,created_by
					  ,previous_indent_code
					  ,email_indent_mail_master_id
					  ,signature_authorized_by_1_auth_signatories_signatures_id
					  ,signature_authorized_by_2_auth_signatories_signatures_id
					  )
					  SELECT  feeder.indent_order_number
							 ,indentdate AS [order_date]
							 ,indentdate AS [collection_date]
							 ,indentdate AS [replenishment_date]
							 ,'PENDING_INTEGRATION' as [cypher_code] 
							 ,feeder.[bank_code]
							 ,feeder.[feeder_branch_code]
							 ,feeder.[cra]
							 ,0
							 ,revised_indent_after_pre_availabiliy_allocation_100
							 ,revised_indent_after_pre_availabiliy_allocation_200
							 ,revised_indent_after_pre_availabiliy_allocation_500
							 ,revised_indent_after_pre_availabiliy_allocation_2000
							 --,revised_indent_after_pre_availabiliy_allocation_100+revised_indent_after_pre_availabiliy_allocation_200+revised_indent_after_pre_availabiliy_allocation_500+revised_indent_after_pre_availabiliy_allocation_2000 
							,revised_total_indent_after_pre_availabiliy_allocation
							 , 0 AS [cra_opening_vault_balance_50]
							 ,  revised_vaulting_after_pre_availabiliy_allocation_100 AS [cra_opening_vault_balance_100]
							 , revised_vaulting_after_pre_availabiliy_allocation_200   AS [cra_opening_vault_balance_200]
							 , revised_vaulting_after_pre_availabiliy_allocation_500   AS [cra_opening_vault_balance_500]
							 , revised_vaulting_after_pre_availabiliy_allocation_2000  AS [cra_opening_vault_balance_2000]
							 , revised_total_vaulting_after_pre_availabiliy_allocation AS [cra_opening_vault_balance]
							 , 0 AS [total_bank_withdrawal_amount_50]
							 , revised_indent_after_pre_availabiliy_allocation_100 AS [total_bank_withdrawal_amount_100]
							 , revised_indent_after_pre_availabiliy_allocation_200 AS [total_bank_withdrawal_amount_200]
							 , revised_indent_after_pre_availabiliy_allocation_500 [total_bank_withdrawal_amount_500]
							 , revised_indent_after_pre_availabiliy_allocation_2000 AS [total_bank_withdrawal_amount_2000]
							 , revised_total_indent_after_pre_availabiliy_allocation AS [total_bank_withdrawal_amount]
							 ,'' AS [authorized_by_1]
							 ,'' AS [authorized_by_2]
							 ,'' AS [notes]
							 , dbo.fnNumberToWords(revised_total_indent_after_pre_availabiliy_allocation) AS [amount_in_words]
							 ,'Revised Indent' AS [indent_type]
							 , feeder.[project_id]
							 ,'Review Pending' [record_status]
							 , @timestamp_date  AS [created_on]
							 , indent_counter+1	
							 ,@created_reference_id
							 ,@cur_user		
							 ,feeder.indent_code 
							 , m.id -- id for indent_mail_master
							 , sign_executive.Id -- id for Auth_Signatories_Signatures for designation = 'Cash Executive'
							 , sign_supervisor.Id -- id for Auth_Signatories_Signatures for designation = 'Cash Supervisor'
							from #final_feeder_level_allocated_dataset feeder
							LEFT JOIN Mail_Master m
							on m.project_id = feeder.project_id
							and m.bank_code = feeder.bank_code
							and m.feeder_branch = feeder.feeder_branch_code
							and m.record_status = 'Active'
							left join Auth_Signatories_Signatures sign_executive
								on sign_executive.project_id = feeder.project_id
								and sign_executive.bank_code = feeder.bank_code
								and sign_executive.designation = 'Cash Executive'
								and sign_executive.record_status = 'Active'
							left join Auth_Signatories_Signatures sign_supervisor
								 on sign_supervisor.project_id = feeder.project_id
								 and sign_supervisor.bank_code = feeder.bank_code
								 and sign_supervisor.designation = 'Cash Supervisor'
								 and sign_supervisor.record_status = 'Active'

					         DECLARE @rowcount INT	
							 --DECLARE @ErrorCode INT		 
					         SELECT @rowcount=@@ROWCOUNT,@ErrorCode = @@error

							 --select * from #final_feeder_level_allocated_dataset
							 
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into indent_master completed'

--- Insert into indent detail table-------
			
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into indent_detail Started'


				INSERT INTO indent_detail
					(  
					       indent_order_number,
					       [atm_id]
					      ,[location]
					      ,[purpose]
					      ,[loading_amount_50]
					      ,[loading_amount_100]
					      ,[loading_amount_200]
					      ,[loading_amount_500]
					      ,[loading_amount_2000]
					      ,[total]
					      ,[record_status]
					      ,[created_on]
						  ,created_by
						  ,created_reference_id
						  ,indent_counter
						  ,previous_indent_code
					)
					SELECT
						     A.indent_order_number  ,
							 A.atm_id,
							 M.location_name, 
							 A.dist_Purpose,
							 NULL,
							 temp.final_total_loading_amount_100,
							 temp.final_total_loading_amount_200,
							 temp.final_total_loading_amount_500,
							 temp.final_total_loading_amount_2000,
							 isnull(temp.final_total_loading_amount_100,0) +	 isnull(temp.final_total_loading_amount_200,0) + isnull(temp.final_total_loading_amount_500,0) +isnull( temp.final_total_loading_amount_2000,0) AS [total] ,
							 'Review Pending'  AS [record_status],
							 @timestamp_date  AS [created_on],
							 @cur_user,
							 @created_reference_id	
						     ,a.indent_counter + 1		
						     ,a.indent_code	
						 FROM #dist_with_cra   A
						 LEFT JOIN atm_master M
						 ON A.site_code  COLLATE DATABASE_DEFAULT= M.site_code COLLATE DATABASE_DEFAULT
						 AND A.atm_id   COLLATE DATABASE_DEFAULT= M.atm_id COLLATE DATABASE_DEFAULT
						 AND M.record_status ='Active' AND M.site_status = 'Active'
						 LEFT JOIN  #final_atm_level_allocated_dataset temp
						 on temp.atm_id COLLATE DATABASE_DEFAULT= A.atm_id COLLATE DATABASE_DEFAULT
						 AND temp.bank_code COLLATE DATABASE_DEFAULT= A.bank_code COLLATE DATABASE_DEFAULT
						 AND temp.project_id COLLATE DATABASE_DEFAULT= A.project_id COLLATE DATABASE_DEFAULT

					
					--select * from #final_atm_level_allocated_dataset
						 
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Insert into indent_detail Completed'


---------------- Update record status, indent order number in indent revision request table ---------------------------

	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Updating columns in indent revision request table started'

				 UPDATE irr
				 SET irr.indent_revision_order_number = ir.indent_order_number,
					 irr.record_status = 'Review Pending',
					 modified_by = @cur_user,
					 modified_on = @timestamp_date,
					 modified_reference_id = @created_reference_id
				 FROM indent_revision_request irr
				 JOIN indent_master ir
				 on irr.indent_order_number = ir.previous_indent_code
				 AND irr.record_status = 'Approved'
				-- AND ir.record_status = 'Active'

				 
	set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Updating columns in indent revision request table Completed'


		set @Execution_log_str = @Execution_log_str + convert(varchar(50),DATEADD(MI,330,GETUTCDATE()),120) + ' : Execution of usp_indent_revision_request Completed'


COMMIT TRANSACTION;
	
		INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
        values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
		  
		   IF (@ErrorCode = 0)
                        BEGIN
                            IF(@row > 0)
                            BEGIN
                                    SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'success') 
                                    SELECT @outputval
                            END
							ELSE
							BEGIN
									SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'no_records') 
                                    SELECT @outputval
							END
						END



END TRY
		BEGIN CATCH
			IF(@@TRANCOUNT > 0 )
				BEGIN
					ROLLBACK TRAN;
				END
						DECLARE @ErrorNumber INT = ERROR_NUMBER();
						DECLARE @ErrorSeverity INT = ERROR_SEVERITY();
						DECLARE @ErrorState INT = ERROR_STATE();
						DECLARE @ErrorProcedure varchar(50) = @procedure_name
						DECLARE @ErrorLine INT = ERROR_LINE();
						DECLARE @ErrorMessage varchar(max) = ERROR_MESSAGE();
						DECLARE @dateAdded datetime = DATEADD(MI,330,GETUTCDATE());
						
						INSERT INTO dbo.error_log values
							(
								@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine,@ErrorMessage,@dateAdded
							)
						INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
					    values (@Execution_log_str, DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

						SET @outputVal = (SELECT sequence FROM app_config_param WHERE category = 'calculated_avg_dispense' AND sub_category = 'Failed') 
                        SELECT @outputval	
										
						
						
			 END CATCH
END