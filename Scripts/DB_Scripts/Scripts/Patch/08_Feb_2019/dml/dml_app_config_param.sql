DELETE from app_config_param where category = 'C3R validation'

INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'atm_counter_mismatch', 70010, N'ATM Counter Balance is not matching with Switch Counter Balance')
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'amount_replenished', 70013, N'Denomination wise amount replenished does not match with the total amount replenished')
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'closing_balance', 70014, N'Denomination wise closing balance does not match with the total closing balance')
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'opening_balance', 70015, N'Denomination wise opening balance does not match with total opening balance')
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'remaining_cash', 70016, N'Denomination wise remaining cash does not match with total remaining cash')
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'atm_closing_balance', 70017, N'Denomination wise closing balance in atm does not match with total closing balance in atm')
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'opening_closing_amount', 70018, N'Current day opening balance does not match with previous day closing balance')
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'cmis_vmis_mismatch', 70019, N'Total loading amount in CMIS does not match with VMIS')
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES ( N'C3R validation', N'master_validation_failed', 70020, N'atm and bank combination not found in master data')

