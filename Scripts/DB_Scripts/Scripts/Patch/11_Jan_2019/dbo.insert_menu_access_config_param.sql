INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'configurable_menu', N'Cash_Admin', 1)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'configurable_menu', N'Cash_Executive', 4)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'eod_activity', N'Cash_Admin', 1)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'eod_activity', N'Cash_Executive', 4)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'qualify_atms', N'Cash_Admin', 1)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'qualify_atms', N'Cash_Executive', 4)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'system_settings', N'Cash_Admin', 1)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'system_settings', N'Cash_Executive', 4)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'create_settings', N'Cash_Admin', 1)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'create_settings', N'Cash_Executive', 4)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'view_settings', N'Cash_Admin', 1)
INSERT INTO [dbo].[menu_access] ( [menu_id], [group_name], [group_id]) VALUES (N'view_settings', N'Cash_Executive', 4)

