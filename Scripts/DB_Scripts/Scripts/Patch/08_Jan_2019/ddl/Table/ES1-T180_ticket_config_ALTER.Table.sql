DROP TABLE IF EXISTS [ticket_config]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ticket_config](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[category_id] [varchar](300) NULL,
	[sla] [varchar](200) NULL,
	[assigned_to_group] [int] NULL,
	[assigned_to_user] [int] NULL,
	[escalation_level1] [varchar](500) NULL,
	[escalation_level2] [varchar](500) NULL,
	[escalation_level3] [varchar](500) NULL,
	[status] [varchar](500) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [date] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [date] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL
)
GO