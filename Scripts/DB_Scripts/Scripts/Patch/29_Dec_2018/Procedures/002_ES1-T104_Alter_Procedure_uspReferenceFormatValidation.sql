/****** Object:  StoredProcedure [dbo].[uspReferenceFormatValidation]    Script Date: 29-12-2018 11:33:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[uspReferenceFormatValidation]
	@file_type varchar(50),@ColumnNames varchar(max)
AS
----------Declaring Variables------------------------
BEGIN
DECLARE @MetaDataHeaders varchar(max)
DECLARE @FileHeader varchar(max)
DECLARE @IsRequired varchar(max)
DECLARE @splitcolumncount int
DECLARE @requiredcolcount int = 0
DECLARE @Counter int 
DECLARE @output varchar(50)
----------------Declaring and initializing counter---------------------
SET @Counter = 0
DECLARE @MetaDataColumnCount int
--SET @ColumnNames = (SELECT CONCAT(@columnNames,'+'))

---------------Checking the no of columns in Meta Data table and storing it in variable----------
SET @MetaDataColumnCount = (SELECT Count(1) FROM MetaDataTableMaster WHERE file_type=@file_type and IsRequired = 'True')
SET @splitcolumncount = (SELECT count(1) FROM fn_split(@ColumnNames,'+'))
	IF(@MetaDataColumnCount > 0)
		BEGIN
		--------------Declaring Cursor1 that will loop through all the records of Meta data table--------------------
		DECLARE cursor1 CURSOR READ_ONLY
			FOR
				SELECT columnHeaders,IsRequired 
				FROM MetaDataTableMaster
				WHERE file_type=@file_type
		
			OPEN cursor1
		------------fetching first record------------------
				FETCH NEXT FROM cursor1 INTO @MetaDataHeaders,@IsRequired
					WHILE @@FETCH_STATUS = 0 
					BEGIN
							DECLARE cursor2 cursor Read_Only
							For
								SELECT ColumnHeaders FROM fn_split(@ColumnNames,'+') OPTION (MAXRECURSION 1000)
								OPEN cursor2
									FETCH NEXT FROM cursor2 INTO @FileHeader
									
										WHILE @@FETCH_STATUS=0
											BEGIN
											
											IF(charindex(',',@MetaDataHeaders)>1)
											BEGIN
											--SELECT @Counter
												IF(charindex(@FileHeader,@MetaDataHeaders)>=1 and @IsRequired = 'True')
												BEGIN
													SET @Counter = @Counter + 1
													
												END
											END
											
											IF(ltrim(rtrim(@FileHeader)) = ltrim(rtrim(@MetaDataHeaders)) and @IsRequired = 'True')
												BEGIN
												
												SET @Counter = @Counter + 1
												
												END
											FETCH NEXT FROM cursor2 INTO @FileHeader
								END
								CLOSE cursor2 
								DEALLOCATE cursor2
								FETCH NEXT FROM cursor1 INTO @MetaDataHeaders,@IsRequired	
					END
			CLOSE cursor1 
		DEALLOCATE cursor1
		
		------ Comparing output from both variables-----------------
		IF( @Counter = @MetaDataColumnCount)
			BEGIN
			DECLARE cursor1 CURSOR READ_ONLY
			FOR
				SELECT columnHeaders 
				FROM MetaDataTableMaster
				WHERE file_type=@file_type
		
				OPEN cursor1
		------------fetching first record------------------
				FETCH NEXT FROM cursor1 INTO @MetaDataHeaders
					WHILE @@FETCH_STATUS = 0 
					BEGIN
							DECLARE cursor2 cursor Read_Only
							For
								SELECT ColumnHeaders FROM fn_split(@ColumnNames,'+') OPTION (MAXRECURSION 1000)
								OPEN cursor2
									FETCH NEXT FROM cursor2 INTO @FileHeader
									
										WHILE @@FETCH_STATUS=0
											BEGIN
											
											IF(charindex(',',@MetaDataHeaders)>1)
											BEGIN
										
												IF(charindex(@FileHeader,@MetaDataHeaders)>=1 )
												BEGIN
													SET @requiredcolcount = @requiredcolcount + 1
													
												END
											END
											
											IF(ltrim(rtrim(@FileHeader)) = ltrim(rtrim(@MetaDataHeaders)))
												BEGIN
												
												SET @requiredcolcount = @requiredcolcount + 1
												
												END
											FETCH NEXT FROM cursor2 INTO @FileHeader
								END
								CLOSE cursor2 
								DEALLOCATE cursor2
								FETCH NEXT FROM cursor1 INTO @MetaDataHeaders	
					END
			CLOSE cursor1 
		DEALLOCATE cursor1--SET @output = ( SELECT sequence from app_config_param where category = 'Format_Validation' and sub_category = 'Success')

			IF(@requiredcolcount = @splitcolumncount)
			BEGIN
			SET @output = ('S100')
			SELECT @output
			END
			Else
			BEGIN
				SET @output = ('E100')
				SELECT @output
			END

			END
		Else
			BEGIN
				SET @output = ( 'E100')
				SELECT @output
			END
	END
ELSE
		BEGIN
		INSERT INTO error_log (errorMessage,errorProcedure,errorSeverity) values ('Column information not defined in Meta Data Table','uspMasterFormatValidation',16)

		SET @output = ('E100')
		SELECT @output
		END
END

