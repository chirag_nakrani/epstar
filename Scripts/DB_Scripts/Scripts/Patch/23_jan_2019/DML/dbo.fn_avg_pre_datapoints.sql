 
/****** Object:  UserDefinedFunction [dbo].[fn_avg_pre_datapoints]    Script Date: 14-01-2019 20:01:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 
 --- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: calculated maximum of the average dispense over three months
 --- =========================================================================
 -- Select dbo.[fn_avg_pre_datapoints] ('DA0068C1',2,'2018-12-26','BOMH','MOF')
 
 ALTER FUNCTION [dbo].[fn_avg_pre_datapoints]

(
 @atm_id    varchar(20),
 @factor int,
 @datetoday date       ,
 @bank      varchar(50),
 @project   varchar(30)
 )
RETURNS int
AS
BEGIN
  

/*

declare @datetoday as date
set @datetoday='2018-10-15'  --15 coctober
Select  CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@datetoday)  )  , 105)	

*/
--Select * from cash_dispense_dataset1

declare @avg_month_minus_3 int,  --@avg1
        @avg_month_minus_2 int, 
		@avg_month_minus_1 int,
		@avg_month_minus_0 int,
		@avg4 int,
		@avg_of_avg int,
		@max_of_avg int,
		@avg_5days int,
		@avg_2days int,
		@avg_of_three_months int,
		@default_avg int=50000,
		@max_of_four_months_avg int,
	    @nreturn int


  

 IF @factor=1
 begin

  Set @avg_5days=(Select	avg(total_dispense_amount)  
				from [cash_dispense_month_minus_0]
				where 
				cast(datafor_date_time as date) <= 
				CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-1,@datetoday)  )  , 105) ),105) 
				and  cast(datafor_date_time as date) >=
				CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-5,@datetoday)  )  , 105) ),105) 
				and bank=@bank and project_id=@project 
				and atm_id=@atm_id 
				and record_status='Active'  )

     Set  @nreturn =  @avg_5days

 end

 ELSE  IF @factor=2
 begin
     Set @avg_2days=  (Select avg(total_dispense_amount) as max_avg
						from [cash_dispense_month_minus_0]
						where 
						cast(datafor_date_time as date) <=
						CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-1,@datetoday)  )  , 105) ),105) 
						and  cast(datafor_date_time as date) >=
						CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-2,@datetoday)  )  , 105) ),105) 
						and bank=@bank and project_id=@project 
						and atm_id=@atm_id 
						and record_status='Active'  )

     Set  @nreturn =  @avg_2days
 end
 
 ELSE  IF @factor=3
		 begin
			  Set @avg_month_minus_3=  ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_3]  where   atm_id=@atm_id  and bank=@bank and project_id=@project)
			  Set  @nreturn =  @avg_month_minus_3
		 end

  ELSE  IF @factor=4
		 begin
			 Set @avg_month_minus_2= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_2]  where  atm_id=@atm_id  and bank=@bank and project_id=@project )
			 Set  @nreturn =  @avg_month_minus_2
		 end

  ELSE  IF @factor=5
		 begin
			  Set @avg_month_minus_1= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_1] where   atm_id=@atm_id  and bank=@bank and project_id=@project)
			  Set  @nreturn =  @avg_month_minus_1
		 end

  ELSE  IF @factor=6

		 begin
			Set @avg_month_minus_0= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_0] where   atm_id=@atm_id  and bank=@bank and project_id=@project)
			Set  @nreturn = @avg_month_minus_0
		 end
 

 ELSE IF @factor=7
			 begin

			 SET @max_of_four_months_avg =
							  (
							  SELECT MAX(n) as max_avg
								FROM (
								Select avg(total_dispense_amount) as n from [cash_dispense_month_minus_3]  where   atm_id=@atm_id  and bank=@bank and project_id=@project
								union
								Select avg(total_dispense_amount) as n from [cash_dispense_month_minus_2]  where  atm_id=@atm_id  and bank=@bank and project_id=@project 
								union
								Select avg(total_dispense_amount) as n from [cash_dispense_month_minus_1] where   atm_id=@atm_id  and bank=@bank and project_id=@project
								union
								Select avg(total_dispense_amount) as n from [cash_dispense_month_minus_0] where   atm_id=@atm_id  and bank=@bank and project_id=@project
									 )s
							  )

			 Set  @nreturn = @max_of_four_months_avg
			 end


 return @nreturn
  
 END

 
