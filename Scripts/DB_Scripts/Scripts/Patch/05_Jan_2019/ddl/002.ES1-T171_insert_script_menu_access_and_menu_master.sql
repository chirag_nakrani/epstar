INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'reference_data_menu', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'reference_data_menu', N'Cash_Admin', 1)

INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'brand_bill_capacity', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'cra_vault_master', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'cra_empaneled', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'cra_escalation_emails', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'bank_escalation_emails', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'eps_escalation_emails', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'escalation_emails', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'default_loading', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'cypher_code', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'data_update_reasons', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'data_rejection_reasons', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'data_overwrite_reasons', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'authorization_signatories_signatures', N'Cash_Executive', 4)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'sla', N'Cash_Executive', 4)

INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'brand_bill_capacity', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'cra_vault_master', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'cra_empaneled', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'cra_escalation_emails', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'bank_escalation_emails', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'eps_escalation_emails', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'escalation_emails', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'default_loading', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'cypher_code', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'data_update_reasons', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'data_rejection_reasons', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'data_overwrite_reasons', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'authorization_signatories_signatures', N'Cash_Admin', 1)
INSERT [dbo].[menu_access] ([menu_id], [group_name], [group_id]) VALUES (N'sla', N'Cash_Admin', 1)




INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'reference_data_menu', N'Reference Data', NULL, NULL, NULL, N'expand', NULL, 1, 1, 6, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'brand_bill_capacity', N'Brand Bill Capacity', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 1, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'default_loading', N'Default Loading', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 2, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'cypher_code', N'Cypher Code', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 3, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'cra_empaneled', N'CRA Empaneled', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 4, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'cra_vault_master', N'Vault Master', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 5, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'escalation_emails', N'Escalation Emails', NULL, NULL, NULL, N'expand', N'reference_data_menu', 1, 2, 6, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'cra_escalation_emails', N'CRA Escalation Emails', NULL, NULL, NULL, N'direct', N'escalation_emails', 1, 3, 1, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'bank_escalation_emails', N'Bank Escalation Emails', NULL, NULL, NULL, N'direct', N'escalation_emails', 1, 3, 2, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'eps_escalation_emails', N'EPS Escalation Emails', NULL, NULL, NULL, N'direct', N'escalation_emails', 1, 3, 3, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'data_update_reasons', N'Data Update Reasons', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 7, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'data_rejection_reasons', N'Data Rejection Reasons', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 8, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'data_overwrite_reasons', N'Data Overwrite Reasons', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 9, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'authorization_signatories_signatures', N'Authorization Signatories Signatures', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 10, NULL)
INSERT [dbo].[menu_master] ([menu_id], [display_name], [menu_description], [keyword], [mapped_url], [link_type], [parent_menu_id], [is_active], [menu_level], [seq_no], [icon_name]) VALUES (N'sla', N'SLA', NULL, NULL, NULL, N'direct', N'reference_data_menu', 1, 2, 11, NULL)
