
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES (N'calculated_avg_dispense', N'success', 11001, N'Average dispense calculated successfully')
GO
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES (N'calculated_avg_dispense', N'Failed', 11002, N'Unable to calculate average dispense')
GO
INSERT [dbo].[app_config_param] ([category], [sub_category], [sequence], [value]) VALUES (N'calculated_avg_dispense', N'no_records', 11003, N'No record found ')
GO

