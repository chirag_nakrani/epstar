update MetaDataTable set columnHeaders = 'Fault_Description' where datafor = 'IMS' and table_column = 'fault_description'
update MetaDataTable set columnHeaders = 'OpenDate' where datafor = 'IMS' and table_column = 'open_date'
update MetaDataTable set columnHeaders = 'OpenTime' where datafor = 'IMS' and table_column = 'opentime'
update MetaDataTable set columnHeaders = 'Remarks' where datafor = 'IMS' and table_column = 'remarks'
update MetaDataTable set columnHeaders = 'LatestRemarks' where datafor = 'IMS' and table_column = 'latestremarks'

insert into MetaDataTable (datafor,bankcode,columnHeaders,IsRequired,table_column,display_column_name,table_name) values ('IMS','','Follow_Up_Date','True','follow_up_date','Follow Up Date','ims_master')