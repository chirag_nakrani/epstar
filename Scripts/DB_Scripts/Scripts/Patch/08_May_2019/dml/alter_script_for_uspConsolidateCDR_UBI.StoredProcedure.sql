USE [epstar]
GO
/****** Object:  StoredProcedure [dbo].[uspConsolidateCDR_UBI]    Script Date: 5/8/2019 10:49:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

-- consolidation of Dispense 
ALTER PROCEDURE [dbo].[uspConsolidateCDR_UBI] 
   (@datafor_date_time varchar(100),
    @projectid varchar(50),
	@region varchar(50),
   @referenceid varchar(50),
   @systemUser varchar(50),
   @out varchar(200) OUTPUT)  -- added parameter @out

AS 
BEGIN
	INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
						values ('Execution of [dbo].[uspConsolidateCDR_UBI] Started. param @datafor_date_time = ' + @datafor_date_time  , 
						DATEADD(MI,330,GETUTCDATE()),
						@@SPID,NULL,
						'Stored Procedure',
						OBJECT_NAME(@@PROCID))

	declare @datafor_date  nvarchar(50) = cast(cast(@datafor_date_time as date) as nvarchar(50))
 

	DECLARE @recordStatus varchar(50);
	DECLARE @successmsg varchar(max);
	DECLARE @errorsmsg varchar(max);
	DECLARE @count1 int;
	DECLARE @errorNumber int;
	DECLARE @current_datetime_stmp datetime = DATEADD(MI,330,GETUTCDATE())
	DECLARE @rowcount int ;
	DECLARE @bankcode nvarchar(10) = 'UBI'
	DECLARE @datafor nvarchar(10) = 'Dispense'
	DECLARE @activestatus nvarchar(20) = 'Active'
	DECLARE @deletedstatus VARCHAR(15) = 'Deleted'
	DECLARE @approvedstatus nvarchar(20) = 'Approved'
	--DECLARE @out varchar(200);

		IF EXISTS(
			Select 1 as ColumnName
			FROM data_update_log
			WHERE cast(datafor_date_time as date) = @datafor_date_time AND 
					bank_code = @bankcode AND 
					data_for_type = @datafor and 
					record_status = @approvedstatus and
					project_id = @projectid  
					AND created_reference_id = @referenceid
			       ) 
		BEGIN			--- Check for approved status in DUL
			INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('Found Record Status = Approved in data_update_log', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
			IF EXISTS   (						
						SELECT 1
						FROM data_update_log
						WHERE cast(datafor_date_time as date) = @datafor_date_time AND 
						bank_code = @bankcode AND 
						data_for_type = @datafor and 
						record_status = @activestatus  
					                --chg
            )	
            BEGIN				--- Check Active status in DUL
                --Add
                -- update status of Data Update Log  for that bank /date 

				INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
				values ('Found Record Status = Active in data_update_log. Update from active to Deleted in data_update_log', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))

    		IF EXISTS
				(
					Select 1 as ColumnName
					FROM [cash_dispense_file_UBI]
					WHERE cast(datafor_date_time as date) = @datafor_date_time AND 
								 record_status = @activestatus
				 ) 
					BEGIN		
						INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
						values ('Found Record Status = Active in [cash_dispense_file_UBI]. Update from active to Deleted in [cash_dispense_file_UBI]', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))     

							IF EXISTS(
									Select 1 as ColumnName
									FROM [cash_dispense_register]
									WHERE
									 cast(datafor_date_time as date) =@datafor_date AND 
										bank_name = @bankcode  AND 
										record_status = @activestatus
							)
							BEGIN			
									UPDATE history
									SET history.record_status = 'Deleted',
										history.deleted_on = @current_datetime_stmp,
										history.deleted_by = @systemUser,
										history.deleted_reference_id = @referenceid
									FROM [cash_dispense_file_UBI] history
									JOIN [cash_dispense_file_UBI] new
									on new.atm_id = history.atm_id
									AND new.datafor_date_time = history.datafor_date_time
									AND new.record_status = 'Approved'
									AND history.record_status = 'Active'

									UPDATE [cash_dispense_file_UBI] set record_status='Active',
									modified_on=@current_datetime_stmp,
									modified_by=@systemUser , 
									modified_reference_id=@referenceid
									where  datafor_date_time=@datafor_date_time 
									and created_reference_id=@referenceid
									and record_status='Approved'

							INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method)
								values ('Now, insert into [cash_dispense_register].', DATEADD(MI,330,GETUTCDATE()),@@SPID,NULL,'Stored Procedure',OBJECT_NAME(@@PROCID))
							---------inserting data after status update for revision
															INSERT INTO 
																	cash_dispense_register ( 
																		datafor_date_time,
																		[atm_id] , 
																		bank_name,
																		[total_dispense_amount] ,
																		record_status,
																		project_id, 
																		region,       --chg
																		[created_on],
																		[created_by],
																		[created_reference_id],
																		[approved_on],
																		[approved_by],
																		[approved_reference_id],
																			modified_on,
																			modified_by,
																			modified_reference_id,
																			is_valid_record,
																			error_code
																	)
											
																	Select 
																			CBH.datafor_date_time,
													        					CBH.atm_id,
																			@bankcode,	
																			type_01+type_02+type_03+type_04  as [total_dispense_amount]   ,
																			@activestatus,
																			AM.project_id,  
																			CBH.region,      --chg
																			CBH.created_on,
																			CBH.created_by,
																			CBH.created_reference_id,
																			CBH.approved_on,
																			CBH.approved_by,
																			CBH.approved_reference_id,
																			CBH.modified_on,
																			CBH.modified_by,
																			CBH.modified_reference_id  
																			,CBH.is_valid_record,
																			CBH.error_code
																		from  
																cash_dispense_file_ubi CBH
																left join atm_master AM
																on CBH.atm_id=AM.atm_id
																and AM.bank_code=@bankcode
																and AM.site_status='Active'
																and AM.record_status='Active'

																where cast(datafor_date_time as date)= cast(@datafor_date_time as date) 
																and CBH.record_status = @activestatus
																and CBH.created_reference_id = @referenceid      

																	
															INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) values ('Data consolidated successfully',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)	
														
														
														UPDATE history
														SET history.record_status = 'Deleted',
															history.deleted_on = @current_datetime_stmp,
															history.deleted_by = @systemUser,
															history.deleted_reference_id = @referenceid
														FROM cash_dispense_register history
														JOIN cash_dispense_register new
														on new.atm_id = history.atm_id
														AND new.bank_name = @bankcode
														AND history.bank_name = @bankcode
														AND new.datafor_date_time  = history.datafor_date_time
														AND new.created_reference_id <> history.created_reference_id
														AND history.created_reference_id <> @referenceid
														and new.record_status = @activestatus
														AND history.record_status = @activestatus

														INSERT INTO dbo.execution_log (description,execution_date_time,process_spid,process_reference_id,execution_program,executor_method,executed_by) 
														values ('Start:updating dataupdate log to mark active to deleted',DATEADD(MI,330,GETUTCDATE()),@@SPID,@referenceid,'Stored Procedure',OBJECT_NAME(@@PROCID),@systemUser)

														DECLARE @SetWhere_DataUpdateLog VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor + ''' and project_id = ''' + @projectid + ''''


														EXEC dbo.[uspSetStatus] 'dbo.data_update_log',@activestatus,@deletedstatus,@current_datetime_stmp,@systemUser,@referenceid,@SetWhere_DataUpdateLog,@out OUTPUT


												DECLARE	@NumRowsChanged int
												SELECT @NumRowsChanged = @@ROWCOUNT
												/*******Check for successful execution of inserting records*******/
												IF(@NumRowsChanged > 0)
													BEGIN
														SET @out = (
														SELECT Sequence from  [dbo].[app_config_param] where 
														category = 'File Operation'
														AND sub_category = 'Consolidation Successful' 
														)
													END
													ELSE
													BEGIN
													  RAISERROR (50003,16,1)
													END


												END					--- Approve END
									END
							ELSE		------	UBI ELSE
							BEGIN
								RAISERROR(50050,16,1)	
										--ELSE			---- Check Cash Balance Register Table ELSE
										--BEGIN 
										--END			

									----- BOMH Active Check Start(END)
							--ELSE		------	BOMH ELSE
							--BEGIN
							--END
				
							END
				END
		ELSE				---- Approve ELSE
			BEGIN
				UPDATE [cash_dispense_file_UBI] set record_status='Active',
									modified_on=@current_datetime_stmp,
									modified_by=@systemUser , 
									modified_reference_id=@referenceid
									where  datafor_date_time=@datafor_date_time 
									and created_reference_id=@referenceid
									and record_status='Approved'


							INSERT INTO 
							cash_dispense_register ( 
								datafor_date_time,
								[atm_id] , 
								bank_name,
								[total_dispense_amount] ,
								record_status,
								project_id, 
								region,       --chg
								[created_on],
								[created_by],
								[created_reference_id],
								[approved_on],
								[approved_by],
								[approved_reference_id],
									modified_on,
									modified_by,
									modified_reference_id,
									is_valid_record,
									error_code
							)
											
							Select 
									CBH.datafor_date_time,
										CBH.atm_id,
									@bankcode,	
									type_01+type_02+type_03+type_04  as [total_dispense_amount]   ,
									@activestatus,
									AM.project_id,  
									CBH.region,      --chg
									CBH.created_on,
									CBH.created_by,
									CBH.created_reference_id,
									CBH.approved_on,
									CBH.approved_by,
									CBH.approved_reference_id,
									CBH.modified_on,
									CBH.modified_by,
									CBH.modified_reference_id  
									,CBH.is_valid_record,
									CBH.error_code
								from  
						cash_dispense_file_ubi CBH
						left join atm_master AM
						on CBH.atm_id=AM.atm_id
						and AM.bank_code=@bankcode
						and AM.site_status='Active'
						and AM.record_status='Active'

						where cast(datafor_date_time as date)= cast(@datafor_date_time as date) 
						and CBH.record_status = @activestatus
						and CBH.created_reference_id = @referenceid


						DECLARE @SetWhere_DataUpdateLog_dul VARCHAR(MAX) =  ' bank_code = ''' + 
														@bankcode + ''' and datafor_date_time = ''' + 
														@datafor_date_time  + ''' and data_for_type = ''' + @datafor 
																	 + ''' and project_id = ''' + @projectid +
																	  ''' and region= ''' +@region+''''


						EXEC dbo.[uspSetStatus] 'dbo.data_update_log',
										@approvedstatus,@activestatus,
										@current_datetime_stmp,@systemUser,
										@referenceid,@SetWhere_DataUpdateLog_dul,@out OUTPUT
							
						SELECT @NumRowsChanged = @@ROWCOUNT
						/*******Check for successful execution of inserting records*******/
						IF(@NumRowsChanged > 0)
							BEGIN
								SET @out = (
								SELECT Sequence from  [dbo].[app_config_param] where 
								category = 'File Operation'
								AND sub_category = 'Consolidation Successful' 
								)
							END
							ELSE
							BEGIN
								RAISERROR (50003,16,1)
							END

			END		------ Check active END
							
				--Insert SP 
 		END					--- Approve END
		ELSE				---- Approve ELSE
			BEGIN
				RAISERROR(50010,16,1)
			END
END

 