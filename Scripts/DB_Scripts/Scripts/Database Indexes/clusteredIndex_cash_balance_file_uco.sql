CREATE CLUSTERED INDEX [ClusteredIndex_cash_balance_file_uco] ON [dbo].[cash_balance_file_uco]
(
	[id] ASC,
	[atm_id] ASC,
	[record_status] ASC,
	[created_reference_id] ASC,
	[datafor_date_time] ASC
)
