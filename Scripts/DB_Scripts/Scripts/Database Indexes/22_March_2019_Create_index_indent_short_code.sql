CREATE INDEX idx_indent_master_indent_short_code
ON indent_master (indent_short_code);

CREATE INDEX idx_distribution_planning_master_V2_indent_short_code
ON distribution_planning_master_V2 (indent_short_code);
