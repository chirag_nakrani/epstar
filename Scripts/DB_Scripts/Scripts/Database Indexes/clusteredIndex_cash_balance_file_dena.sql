CREATE CLUSTERED INDEX [ClusteredIndex_cash_balance_file_dena] ON [dbo].[cash_balance_file_dena]
(
	[id] ASC,
	[atm_id] ASC,
	[record_status] ASC,
	[created_reference_id] ASC,
	[datafor_date_time] ASC
)
