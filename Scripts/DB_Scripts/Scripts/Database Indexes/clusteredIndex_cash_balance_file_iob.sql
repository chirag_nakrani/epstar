CREATE CLUSTERED INDEX [ClusteredIndex_cash_balance_file_iob] ON [dbo].[cash_balance_file_iob]
(
	[id] ASC,
	[atm_id] ASC,
	[record_status] ASC,
	[created_reference_id] ASC,
	[datafor_date_time] ASC
)

