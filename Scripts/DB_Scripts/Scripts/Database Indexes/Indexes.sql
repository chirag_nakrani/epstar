CREATE INDEX idx_bankcode_fld
ON feeder_limit_days (bank_code);

CREATE INDEX idx_feeder_fld
ON feeder_limit_days (feeder);


CREATE INDEX idx_bankcode_bld
ON bank_limit_days (bank_code);



CREATE INDEX idx_bankcode_ld
ON limitdays (bank_code);


CREATE INDEX idx_atm_id_ld
ON limitdays (atm_id);
