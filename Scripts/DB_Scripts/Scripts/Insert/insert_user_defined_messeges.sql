GO  
EXEC sp_addmessage 50001, 16,N'Sum of denomination wise value does not match with total value';  
GO  
EXEC sp_addmessage 50002, 16,N'Please check the record status in bank wise file';  
GO  
EXEC sp_addmessage 50003, 16,N'Unable to insert records in consolidated table ';  
GO  
EXEC sp_addmessage 50004, 16,N'Please check data update log with specified upload datetime, bankcode, data for type,record status and project';  
GO  
EXEC sp_addmessage 50005, 16,N'Status update failed!! Please reconsider checking uspsetstatus SP';  
GO  
EXEC sp_addmessage 50006, 16,N'Since data update log contains both active and approved file (Revision). While for the specified bank, there are no records as active status in bankwise file ';  
GO  
EXEC sp_addmessage 50008, 16,N'Active status not found in bankwise file';  
GO  
EXEC sp_addmessage 50010, 16,N'Please check record status in data update log. It should be in approved status before proceeding ';  
GO  
EXEC sp_addmessage 50050, 16,N'Since data update log contains both active and approved file (Revision). While for the specified bank, there are no records as active status in bankwise file ';  
GO  
EXEC sp_addmessage 70005, 16,N'No record found to update';  
GO  
EXEC sp_addmessage 80001, 16,N'Dispense amount is not in divisible of 100';  
GO  
EXEC sp_addmessage 90001, 16,N'ATM id not present in master data';  
GO
EXEC sp_addmessage 90002, 16,N'No record found for uploaded status';  
GO    
 