
------- Cash Batch Operator Role Permission Mapping start------
Insert into auth_group_permissions values(2,3);
Insert into auth_group_permissions values(2,6);
Insert into auth_group_permissions values(2,11);
Insert into auth_group_permissions values(2,14);
Insert into auth_group_permissions values(2,18);
Insert into auth_group_permissions values(2,19);
------- Cash Batch Operator Role Permission Mapping ends------

------- Cash Admin Role Permission Mapping start------
Insert into auth_group_permissions values(1,3);
Insert into auth_group_permissions values(1,6);
Insert into auth_group_permissions values(1,7);
Insert into auth_group_permissions values(1,8);
Insert into auth_group_permissions values(1,11);
Insert into auth_group_permissions values(1,13);
Insert into auth_group_permissions values(1,14);
Insert into auth_group_permissions values(1,18);
Insert into auth_group_permissions values(1,19);
------- Cash Admin Role Permission Mapping ends------

------- Cash Ops Head Role Permission Mapping start------
Insert into auth_group_permissions values(4,3);
Insert into auth_group_permissions values(4,6);
Insert into auth_group_permissions values(4,14);
Insert into auth_group_permissions values(4,19);
------- Cash Ops Head Role Permission Mapping end------


------- Cash Ops Manager Role Permission Mapping start------
Insert into auth_group_permissions values(5,3);
Insert into auth_group_permissions values(5,9);
Insert into auth_group_permissions values(5,12);
Insert into auth_group_permissions values(5,14);
Insert into auth_group_permissions values(5,19);
------- Cash Ops Manager Role Permission Mapping end------

------- Cash Executive Role Permission Mapping start------
Insert into auth_group_permissions values(3,10);
Insert into auth_group_permissions values(3,16);
Insert into auth_group_permissions values(3,17);
Insert into auth_group_permissions values(3,18);
Insert into auth_group_permissions values(3,19);
------- Cash Executive Role Permission Mapping end------

------- System Admin Role Permission Mapping start------
Insert into auth_group_permissions values(7,1);
Insert into auth_group_permissions values(7,2);
Insert into auth_group_permissions values(7,7);
Insert into auth_group_permissions values(7,9);
Insert into auth_group_permissions values(7,10);
Insert into auth_group_permissions values(7,12);
Insert into auth_group_permissions values(7,15);
Insert into auth_group_permissions values(7,19);
------- System Admin Role Permission Mapping ends------


------- Cash Team Lead Role Permission Mapping start------
Insert into auth_group_permissions values(6,10);
Insert into auth_group_permissions values(6,16);
Insert into auth_group_permissions values(6,17);
Insert into auth_group_permissions values(6,18);
Insert into auth_group_permissions values(6,19);
------- Cash Team Lead Role Permission Mapping end------
