
insert into auth_permission (name,content_type_id,codename) values('Add',2,'add');
insert into auth_permission (name,content_type_id,codename) values('Apply',2,'apply');
insert into auth_permission (name,content_type_id,codename) values('Approve',2,'approve');
insert into auth_permission (name,content_type_id,codename) values('Assign',2,'assign');
insert into auth_permission (name,content_type_id,codename) values('Close',2,'close');
insert into auth_permission (name,content_type_id,codename) values('Data Information',2,'data_information');
insert into auth_permission (name,content_type_id,codename) values('Delete',2,'delete');
insert into auth_permission (name,content_type_id,codename) values('Edit Data',2,'edit_data');
insert into auth_permission (name,content_type_id,codename) values('Force Send',2,'force_send');
insert into auth_permission (name,content_type_id,codename) values('Modify',2,'modify');
insert into auth_permission (name,content_type_id,codename) values('Overwrite',2,'overwrite');
insert into auth_permission (name,content_type_id,codename) values('Re Run Forecasting',2,'re_run_forecasting');
insert into auth_permission (name,content_type_id,codename) values('Refresh Data',2,'refresh_data');
insert into auth_permission (name,content_type_id,codename) values('Reject',2,'reject');
insert into auth_permission (name,content_type_id,codename) values('Select',2,'select');
insert into auth_permission (name,content_type_id,codename) values('Select one RadioButton',2,'select_one_radiobutton');
insert into auth_permission (name,content_type_id,codename) values('Submit',2,'submit');
insert into auth_permission (name,content_type_id,codename) values('Upload File',2,'upload_file');
insert into auth_permission (name,content_type_id,codename) values('View',2,'view');
INSERT [dbo].[auth_permission] ([name], [content_type_id], [codename]) VALUES (N'Auto Approval', 2, N'auto_approval');

