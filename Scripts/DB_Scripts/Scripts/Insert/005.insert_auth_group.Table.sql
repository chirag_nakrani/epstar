INSERT [dbo].[auth_group] ([name]) VALUES (N'Cash_Admin')
GO
INSERT [dbo].[auth_group] ([name]) VALUES (N'Cash_Batch_Operator')
GO
INSERT [dbo].[auth_group] ([name]) VALUES (N'Cash_Executive')
GO
INSERT [dbo].[auth_group] ([name]) VALUES (N'Cash_Ops_Head')
GO
INSERT [dbo].[auth_group] ([name]) VALUES (N'Cash_Ops_Manager')
GO
INSERT [dbo].[auth_group] ([name]) VALUES (N'Cash_Team_Lead')
GO
INSERT [dbo].[auth_group] ([name]) VALUES (N'System_Admin')
