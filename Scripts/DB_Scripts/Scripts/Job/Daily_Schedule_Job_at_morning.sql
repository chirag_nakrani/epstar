DECLARE @indent_date nvarchar(20) = cast( cast(DATEADD(MI,330,GETUTCDATE()) as date) as nvarchar(20))
DECLARE @referenceid varchar(50) = (next value for generate_ref_seq)
DECLARE @username varchar(20)= (SELECT SYSTEM_USER)

-- for all banks, calculate morning balance
EXEC [usp_morningBalance] @indent_date,'AllProjects','Allbanks',@referenceid,@username

-- For each bank, calculate average dispense
EXEC [usp_calculated_avg_dispense] @indent_date,'BOMH','MOF_MAH',@username,@referenceid

EXEC [usp_calculated_avg_dispense] @indent_date,'SBI','MOF_MAH',@username,@referenceid
EXEC [usp_calculated_avg_dispense] @indent_date,'SBI','MOF_BJK',@username,@referenceid
EXEC [usp_calculated_avg_dispense] @indent_date,'SBI','TOM',@username,@referenceid

EXEC [usp_calculated_avg_dispense] @indent_date,'DENA','MOF_MAH',@username,@referenceid
EXEC [usp_calculated_avg_dispense] @indent_date,'DENA','MOF_BJK',@username,@referenceid
EXEC [usp_calculated_avg_dispense] @indent_date,'DENA','TOM',@username,@referenceid

EXEC [usp_calculated_avg_dispense] @indent_date,'CAB','MOF_MAH',@username,@referenceid
EXEC [usp_calculated_avg_dispense] @indent_date,'CAB','MOF_BJK',@username,@referenceid
EXEC [usp_calculated_avg_dispense] @indent_date,'CAB','TOM',@username,@referenceid

EXEC [usp_calculated_avg_dispense] @indent_date,'UBI','MOF_MAH',@username,@referenceid
EXEC [usp_calculated_avg_dispense] @indent_date,'UBI','MOF_BJK',@username,@referenceid
EXEC [usp_calculated_avg_dispense] @indent_date,'UBI','TOM',@username,@referenceid

exec [usp_indent_calculation] @indent_date,@username,@referenceid