DECLARE @indent_date nvarchar(20) = cast( cast(DATEADD(MI,330,GETUTCDATE()) as date) as nvarchar(20))
DECLARE @referenceid varchar(50) = (next value for generate_ref_seq)
DECLARE @username varchar(20)= (SELECT SYSTEM_USER)

EXEC [usp_DecideLimitDays] @indent_date,@referenceid,@username
