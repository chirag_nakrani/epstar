/****** Object:  Table [dbo].[indent_pdf_status]    Script Date: 1/29/2019 3:24:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[indent_pdf_status](
      [id] [bigint] IDENTITY(1,1) PRIMARY KEY,
      [project_id] [nvarchar](50) NULL,
      [bank_code] [nvarchar](50) NULL,
      [feeder_branch] [nvarchar](50) NULL,
      [cra] [nvarchar](50) NULL,
      [indent_order_number] [nvarchar](100) NULL,
      [file_name] [nvarchar](250) NULL,
      [physical_file] [image] NULL,
      [pdf_status] [nvarchar](50) NULL,
      [indent_date] [datetime] NULL,
	  [indent_type] [nvarchar](50) NULL,
      [record_status] [nvarchar](50) NULL,
      [created_on] [datetime] NULL,
      [created_by] [nvarchar](50) NULL,
      [created_reference_id] [nvarchar](50) NULL,
      [approved_on] [datetime] NULL,
      [approved_by] [nvarchar](50) NULL,
      [approved_reference_id] [nvarchar](50) NULL,
      [deleted_on] [datetime] NULL,
      [deleted_by] [nvarchar](50) NULL,
      [deleted_reference_id] [nvarchar](50) NULL,
      [modified_on] [datetime] NULL,
      [modified_by] [nvarchar](50) NULL,
      [modified_reference_id] [nvarchar](50) NULL
 )
 GO

