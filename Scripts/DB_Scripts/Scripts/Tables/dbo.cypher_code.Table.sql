
/****** Object:  Table [dbo].[cypher_code]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cypher_code](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[category] [nvarchar](20) NULL,
	[value] [nvarchar](50) NULL,
	[cypher_code] [int] NULL,
	[is_applicable_to_all_projects] [nvarchar](5) NULL,
	[project_id] [nvarchar](50) NULL,
	[is_applicable_to_all_banks] [nvarchar](5) NULL,
	[bank_code] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)    
GO
