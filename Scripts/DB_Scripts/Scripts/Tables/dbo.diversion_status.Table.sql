SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create table [dbo].[diversion_status](
	[diversion_request_no] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[indent_order_number] [nvarchar](255) NULL,
	[bank] [nvarchar](50) NULL,
	[feeder_branch] [nvarchar](100) NULL,
	[region] [nvarchar](100) NULL,
	[cra] [nvarchar](100) NULL,
	[order_date] [datetime] NULL,
	[original_atm_id] [nvarchar](50) NULL,
	[diverted_atm_id] [nvarchar](50) NULL,
	[total_diversion_amount] [int] NULL,
	[denomination_100] [int] NULL,
	[denomination_200] [int] NULL,
	[denomination_500] [int] NULL,
	[denomination_2000] [int] NULL,
	[reasons] [nvarchar](max) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)