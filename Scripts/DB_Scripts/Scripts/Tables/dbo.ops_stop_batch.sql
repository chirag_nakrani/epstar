/****** Object:  Table [dbo].[indent_eod_pre_activity]    Script Date: 29-11-2018 15:34:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ops_stop_batch](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[project_id] [nvarchar](25) NULL,
	[bank_code] [nvarchar](10) NULL,
	[feeder_branch_code] [nvarchar](50) NULL,
	[region_code] [nvarchar](50) NULL,
	[state] nvarchar(100) NULL,
	from_date datetime NULL,
	to_date datetime NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)
