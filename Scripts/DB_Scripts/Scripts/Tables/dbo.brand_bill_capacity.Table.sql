GO
/****** Object:  Table [dbo].[brand_bill_capacity]    Script Date: 11-12-2018 18:37:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[brand_bill_capacity](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[brand_code] [nvarchar](50) NULL,
	[description] [nvarchar](50) NULL,
	[capacity_50] [int] NULL,
	[capacity_100] [int] NULL,
	[capacity_200] [int] NULL,
	[capacity_500] [int] NULL,
	[capacity_2000] [int] NULL,
	[record_status] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)
GO
