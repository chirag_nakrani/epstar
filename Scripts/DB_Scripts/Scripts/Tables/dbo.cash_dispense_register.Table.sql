/****** Object:  Table [dbo].[cash_dispense_register]    Script Date: 19-12-2018 22:08:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[cash_dispense_register](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[date] [datetime] NULL,
	[atm_id] [nvarchar](20) NOT NULL,
	[bank_name] [nvarchar](50) NOT NULL,
	[Location] [nvarchar](50) NULL,
	[total_dispense_amount] [int] NULL,
	[reference_id] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[record_status] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[dispense_type] [nvarchar](20) NULL
)
