/****** Object:  Table [dbo].[Mail_Master]    Script Date: 12/12/2018 4:04:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Mail_Master](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](10) NULL,
	[feeder_branch] [nvarchar](50) NULL,
	[sol_id] [nvarchar](10) NULL,
	[cra] [nvarchar](50) NULL,
	[from_email_id] [nvarchar](100) NULL,
	[to_bank_email_id] [nvarchar](max) NULL,
	[to_CRA_email_id] [nvarchar](max) NULL,
	[bcc_email_id] [nvarchar](max) NULL,
	[subject] [nvarchar](100) NULL,
	[body_text] [nvarchar](max) NULL,
	[state] [nvarchar](50) NULL,
	[importance] [nvarchar](50) NULL,
	[atm_count] [nvarchar](50) NULL,
	[status] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[rejected_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[isValidRecord] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL)
GO
