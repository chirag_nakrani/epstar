/****** Object:  Table [dbo].[data_update_log_master]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[data_update_log_master](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[project_id] [nvarchar](50) NULL,
	[operation_type] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[data_for_type] [nvarchar](50) NULL,
	[source_info] [nvarchar](2000) NULL,
	[destination_info] [nvarchar](2000) NULL,
	[status_info_json] [nvarchar](max) NULL,
	[start_loading_on] [datetime] NULL,
	[end_loading_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[validated_on] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approver_comment] [nvarchar](2000) NULL,
	[consolidated_on] [datetime] NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_comment] [nvarchar](2000) NULL,
	[record_status] [nvarchar](50) NULL,
	[is_valid_file] [nvarchar](20) NULL,
	[date_created] [datetime] NULL,
	[date_updated] [datetime] NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[validation_code] [nvarchar](100) NULL,
	[total_count] [int] NULL,
	[approved_count] [int] NULL,
	[pending_count] [int] NULL,
	[reject_count] [int] NULL
)
