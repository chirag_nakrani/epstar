/****** Object:  Table [dbo].[morningbalance]    Script Date: 20-12-2018 17:33:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[morningbalance](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[atm_id] [nvarchar](50) NULL,
	[bank_name] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](20) NULL,
	[created_on] [datetime] NULL,
	[datafor_date_time] [datetime] NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[remaining_balance_50] [bigint] NULL,
	[remaining_balance_100] [bigint] NULL,
	[remaining_balance_200] [bigint] NULL,
	[remaining_balance_500] [bigint] NULL,
	[remaining_balance_2000] [bigint] NULL,
	[Morning_balance_T_minus_1] [bigint] NULL,
	[created_by] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL
	)
