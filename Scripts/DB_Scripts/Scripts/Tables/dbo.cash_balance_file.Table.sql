/****** Object:  Table [dbo].[cash_balance_file]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cash_balance_file](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[date_time] [datetime] NULL,
	[bank_name] [nvarchar](50) NULL,
	[atm_id] [nvarchar](20) NULL,
	[remaining_balance_50] [int] NULL,
	[remaining_balance_100] [int] NULL,
	[remaining_balance_200] [int] NULL,
	[remaining_balance_500] [int] NULL,
	[remaining_balance_2000] [int] NULL,
	[total_remaining_balance] [bigint] NULL,
	[calculated_remaining_balance_amount] [int] NULL,
	[is_total_remaining_matched_with_calculated] [bit] NULL,
	[exception_trigger_id] [nvarchar](255) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL
)
GO
