USE [epstar]
GO
/****** Object:  Table [dbo].[cash_dispense_file_sbi_staging]    Script Date: 5/14/2019 12:03:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cash_dispense_file_sbi_staging](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[atm_id] [nvarchar](20) NOT NULL,
	[cash_withdrawal] [bigint] NULL,
	[opening_cash] [int] NULL,
	[total_cash_replenished] [bigint] NULL,
	[cer] [float] NULL,
	[msvendor] [nvarchar](20) NULL,
	[circle_name] [nvarchar](30) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[region] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
)
GO
