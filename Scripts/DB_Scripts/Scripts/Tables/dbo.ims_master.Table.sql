/****** Object:  Table [dbo].[ims_master]    Script Date: 04-12-2018 20:27:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ims_master](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[ims_sync_master_id] [bigint] NULL,
	[ticketno] [nvarchar](50) NOT NULL,
	[vendorticketno] [nvarchar](50) NULL,
	[site_code] [nvarchar](100) NULL,
	[atmid] [nvarchar](50) NOT NULL,
	[bankname] [nvarchar](100) NULL,
	[ticket_generation_type] [nvarchar](255) NULL,
	[site_details] [nvarchar](255) NULL,
	[fault] [nvarchar](255) NULL,
	[fault_description] [nvarchar](255) NULL,
	[call_status] [nvarchar](255) NULL,
	[ticket_status] [nvarchar](255) NULL,
	[message_date_time] [nvarchar](50) NULL,
	[open_date] [date] NULL,
	[opentime] [datetime] NULL,
	[duration] [nvarchar](50) NULL,
	[remarks] [nvarchar](max) NULL,
	[latestremarks] [nvarchar](max) NULL,
	[follow_up_date] [nvarchar](max) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [nvarchar](50) NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](100) NULL
	)


