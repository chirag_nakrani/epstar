CREATE TABLE [dbo].[ams_api_token_data](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[datafor_date_time] [datetime] NOT NULL,
	[max_response_id] [int] NOT NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL
) 
GO
