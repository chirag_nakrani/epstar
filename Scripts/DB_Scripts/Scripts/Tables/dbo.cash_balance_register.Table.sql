/****** Object:  Table [dbo].[cash_balance_register]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cash_balance_register](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY ,
	[datafor_date_time] [datetime] NULL,
	[bank_name] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[remaining_balance_50] [bigint] NULL,
	[remaining_balance_100] [bigint] NULL,
	[remaining_balance_200] [bigint] NULL,
	[remaining_balance_500] [bigint] NULL,
	[remaining_balance_2000] [bigint] NULL,
	[total_remaining_balance] [bigint] NULL,
	[calculated_remaining_balance_amount] [bigint] NULL,
	[is_total_remaining_matched_with_calculated] [bit] NULL,
	[exception_trigger_id] [nvarchar](255) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](20) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL
)    
GO
