
/****** Object:  Table [dbo].[MetaDataTableMaster]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MetaDataTableMaster](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[file_type] [nvarchar](100) NULL,
	[columnHeaders] [nvarchar](100) NULL,
	[IsRequired] [nvarchar](10) NULL,
	[table_column] [nvarchar](300) NULL,
	[display_column_name] [nvarchar](300) NULL,
	[table_name] [nvarchar](250) NULL
) 
GO
