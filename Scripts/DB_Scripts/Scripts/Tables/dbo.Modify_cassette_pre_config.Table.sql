/****** Object:  Table [dbo].[Modify_cassette_pre_config]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Modify_cassette_pre_config](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[from_date] [datetime] NULL,
	[to_date] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[bank_name] [nvarchar](50) NULL,
	[bank_code] [nvarchar](10) NULL,
	[site_code] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[cassette_50_count] [float] NULL,
	[cassette_100_count] [float] NULL,
	[cassette_200_count] [float] NULL,
	[cassette_500_count] [float] NULL,
	[cassette_2000_count] [float] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL
)
GO
