
/****** Object:  Table [dbo].[menu_master]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menu_master](
	[menu_id] [nvarchar](50) PRIMARY KEY,
	[display_name] [nvarchar](255) NOT NULL,
	[menu_description] [nvarchar](255) NULL,
	[keyword] [nvarchar](255) NULL,
	[mapped_url] [nvarchar](255) NULL,
	[link_type] [nvarchar](50) NULL,
	[parent_menu_id] [nvarchar](50) NULL,
	[is_active] [int] NULL,
	[menu_level] [int] NULL,
	[seq_no] [int] NULL,
	[icon_name] [nvarchar](255) NULL
)
GO
