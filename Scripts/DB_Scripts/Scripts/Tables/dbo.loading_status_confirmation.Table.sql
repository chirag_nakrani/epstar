/****** Object:  Table [dbo].[loading_status_confirmation]    Script Date: 11-04-2019 12:55:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[loading_status_confirmation](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[indent_order_number] [nvarchar](200) NULL,
	[site_code] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[feeder_branch_code] [nvarchar](100) NULL,
	[cra] [nvarchar](50) NULL,
	[indent_date] [date] NULL,
	[loading_status] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
	)
