/****** Object:  Table [dbo].[cash_balance_file_vjb]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cash_balance_file_vjb](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY ,
	[atm_id] [nvarchar](15) NULL,
	[cash_start1] [bigint] NULL,
	[cash_start2] [bigint] NULL,
	[cash_start3] [bigint] NULL,
	[cash_start4] [bigint] NULL,
	[cash_inc1] [bigint] NULL,
	[cash_inc2] [bigint] NULL,
	[cash_inc3] [bigint] NULL,
	[cash_inc4] [bigint] NULL,
	[cash_dec1] [bigint] NULL,
	[cash_dec2] [bigint] NULL,
	[cash_dec3] [bigint] NULL,
	[cash_dec4] [bigint] NULL,
	[cash_out1] [bigint] NULL,
	[cash_out2] [bigint] NULL,
	[cash_out3] [bigint] NULL,
	[cash_out4] [bigint] NULL,
	[cash_currbal1] [bigint] NULL,
	[cash_currbal2] [bigint] NULL,
	[cash_currbal3] [bigint] NULL,
	[cash_currbal4] [bigint] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
)
GO
