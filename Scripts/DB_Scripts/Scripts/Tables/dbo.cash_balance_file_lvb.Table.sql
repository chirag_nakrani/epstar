/****** Object:  Table [dbo].[cash_balance_file_lvb]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cash_balance_file_lvb](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY ,
	[brn] [nvarchar](50) NULL,
	[atm_id] [nvarchar](15) NULL,
	[atm_location] [nvarchar](100) NULL,
	[f_tray] [int] NULL,
	[f_count] [int] NULL,
	[g_tray] [int] NULL,
	[g_count] [int] NULL,
	[h_tray] [int] NULL,
	[h_count] [int] NULL,
	[total_amt] [int] NULL,
	[file_date_time] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
) 
GO
