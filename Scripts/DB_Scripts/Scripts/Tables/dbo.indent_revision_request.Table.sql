
/****** Object:  Table [dbo].[indent_revision_request]    Script Date: 04-01-2019 11:52:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[indent_revision_request](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[indent_revision_order_number] [nvarchar](255) NOT NULL,
	[indent_order_number] [nvarchar](255) NULL,
	[total_original_forecast_amount] [bigint] NULL,
	[original_forecast_amount_50] [bigint] NULL,
	[original_forecast_amount_100] [bigint] NULL,
	[original_forecast_amount_200] [bigint] NULL,
	[original_forecast_amount_500] [bigint] NULL,
	[original_forecast_amount_2000] [bigint] NULL,
	[total_amount_available] [bigint] NULL,
	[available_50_amount] [bigint] NULL,
	[available_100_amount] [bigint] NULL,
	[available_200_amount] [bigint] NULL,
	[available_500_amount] [bigint] NULL,
	[available_2000_amount] [bigint] NULL,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](50) NULL,
	[feeder_branch_code] [nvarchar](50) NULL,
	[is_withdrawal_done] [int] NULL,
	[reason] [nvarchar](500) NULL,
	[remarks] [nvarchar](max) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL
	)

