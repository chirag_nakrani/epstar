
/****** Object:  Table [dbo].[cash_live_master_sample]    Script Date: 27-11-2018 17:09:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[project_bank_feeder_cra_mapping](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[bank_id] [nvarchar](50) NOT NULL,
	[project_id] [nvarchar](50) NOT NULL,
	[region] [nvarchar](50) NULL,
	[Feeder] [nvarchar](50) NULL,
	[iscbr] [nvarchar](50) NULL,
	[isdispense] [nvarchar](50) NULL
)
GO


