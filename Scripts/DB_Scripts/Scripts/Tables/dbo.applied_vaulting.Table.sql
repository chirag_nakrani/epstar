/****** Object:  Table [dbo].[applied_vaulting]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[applied_vaulting](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[for_date] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](10) NULL,
	[site_code] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[applied_vaulting_percentage] [decimal](5, 2) NULL,
	[comment] [nvarchar](100) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_ref_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_ref_id] [nvarchar](50) NULL
) 
GO
