
/****** Object:  Table [dbo].[error_log]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_log](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[errorNumber] [int] NULL,
	[errorSeverity] [int] NULL,
	[errorState] [int] NULL,
	[errorProcedure] [nvarchar](50) NULL,
	[errorLine] [int] NULL,
	[errorMessage] [nvarchar](max) NULL,
	[dateAdded] [datetime] NULL
	)