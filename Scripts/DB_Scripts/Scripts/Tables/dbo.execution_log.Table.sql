
/****** Object:  Table [dbo].[execution_log]    Script Date: 04-12-2018 14:46:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[execution_log](
	[ID] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[description] [varchar](max) NULL,
	[execution_date_time] [datetime] NULL,
	[process_spid] [bigint] NULL,
	[process_reference_id] [nvarchar](50) NULL,
	[execution_program] [nvarchar](100) NULL,
	[executor_method] [nvarchar](100) NULL,
	[executed_by] [nvarchar](50) NULL,
)

