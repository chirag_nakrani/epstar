 

/****** Object:  Table [dbo].[calculated_dispense]    Script Date: 02-01-2019 19:35:01 ******/
DROP TABLE [dbo].[calculated_dispense]
GO

/****** Object:  Table [dbo].[calculated_dispense]    Script Date: 02-01-2019 19:35:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[calculated_dispense](
	[id] [bigint] IDENTITY(1,1)  PRIMARY KEY,
	[atm_id] [nvarchar](20) NOT NULL,
	[bank] [nvarchar](50) NOT NULL,
	[project_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[T_M3_minus_3] [int] NULL,
	[T_M3_minus_2] [int] NULL,
	[T_M3_minus_1] [int] NULL,
	[T_M3_minus_0] [int] NULL,
	[T_M3_plus_1] [int] NULL,
	[T_M3_plus_2] [int] NULL,
	[T_M3_plus_3] [int] NULL,
	[T_M2_minus_3] [int] NULL,
	[T_M2_minus_2] [int] NULL,
	[T_M2_minus_1] [int] NULL,
	[T_M2_minus_0] [int] NULL,
	[T_M2_plus_1] [int] NULL,
	[T_M2_plus_2] [int] NULL,
	[T_M2_plus_3] [int] NULL,
	[T_M1_minus_3] [int] NULL,
	[T_M1_minus_2] [int] NULL,
	[T_M1_minus_1] [int] NULL,
	[T_M1_minus_0] [int] NULL,
	[T_M1_plus_1] [int] NULL,
	[T_M1_plus_2] [int] NULL,
	[T_M1_plus_3] [int] NULL,	
	[T_minus_7] [int] NULL,
	[T_minus_6] [int] NULL,
	[T_minus_5] [int] NULL,
	[T_minus_4] [int] NULL,
	[T_minus_3] [int] NULL,
	[T_minus_2] [int] NULL,
    [T_minus_1] [int] NULL,
	[max_of_max] [int] NULL,
	[avg_of_max] [int] NULL,
	[max_of_avg] [int] NULL,
	[avg_of_avg] [int] NULL,
	[max_of_secondmax] [int] NULL,
	[avg_of_secondmax] [int] NULL,
	[max_of_thirdmax] [int] NULL,
	[avg_of_thirdmax] [int] NULL,
	[_28days_or_T2dispense] [int] NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[avg_of_5days_Tminus5] [int] NULL,
	[avg_of_2days_Tminus2] [int] NULL,
	[avg_month_minus_3] [int] NULL,
	[avg_month_minus_2] [int] NULL,
	[avg_month_minus_1] [int] NULL,
	[avg_month_minus_0] [int] NULL,
	[max_of_four_months_avg] [int] NULL,
	[default_dispense_amount] [int] NULL,
	[max_of_current] [int] NULL
 
GO


