GO
/****** Object:  Table [dbo].[cra_vault_master]    Script Date: 11-12-2018 18:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cra_vault_master](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[vault_code] [nvarchar](200) NULL,
	[cra_code] [nvarchar](4000) NULL,
	[state] [nvarchar](50) NULL,
	[region] [nvarchar](50) NULL,
	[location] [nvarchar](1000) NULL,
	[address] [nvarchar](4000) NULL,
	[vault_type] [nvarchar](100) NULL,
	[vault_status] [nvarchar](100) NULL,
	[vaulting_allowed] [nvarchar](10) NULL,
	[contact_details] [nvarchar](100) NULL,
	[record_status] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[rejected_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)
GO
