/****** Object:  Table [dbo].[eps_escalation_emails]    Script Date: 12/12/2018 4:04:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eps_escalation_emails](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[eps] [nvarchar](50) NULL,
	[bank] [nvarchar](50) NULL,
	[activity] [nvarchar](100) NULL,
	[primary_email_id] [nvarchar](100) NULL,
	[primary_contact_no] [nvarchar](100) NULL,
	[level_1_email_id] [nvarchar](100) NULL,
	[level_1_contact_no] [nvarchar](100) NULL,
	[level_2_email_id] [nvarchar](100) NULL,
	[level_2_contact_no] [nvarchar](100) NULL,
	[level_3_email_id] [nvarchar](100) NULL,
	[level_3_contact_no] [nvarchar](100) NULL,
	[level_4_email_id] [nvarchar](100) NULL,
	[level_4_contact_no] [nvarchar](100) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL)
GO
