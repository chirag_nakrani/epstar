/****** Object:  Table [dbo].[cash_balance_file_sbi]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cash_balance_file_sbi](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY ,
	[terminal_location] [nvarchar](50) NULL,
	[city] [nvarchar](50) NULL,
	[cash_dispensed_hopper_1] [int] NULL,
	[bills_hopr1] [int] NULL,
	[cash_dispensed_hopper_2] [int] NULL,
	[bills_hopr2] [int] NULL,
	[cash_dispensed_hopper_3] [int] NULL,
	[bills_hopr3] [int] NULL,
	[cash_dispensed_hopper_4] [int] NULL,
	[bills_hopr4] [int] NULL,
	[cash_dispensed_hopper_5] [int] NULL,
	[bills_hopr5] [int] NULL,
	[cash_dispensed_hopper_6] [int] NULL,
	[bills_hopr6] [int] NULL,
	[atm_id] [nvarchar](20) NULL,
	[total_remaining_cash_def_curr] [int] NULL,
	[remote_address] [nvarchar](255) NULL,
	[cash_increment_hopper_1] [int] NULL,
	[cash_increment_hopper_2] [int] NULL,
	[cash_increment_hopper_3] [int] NULL,
	[cash_increment_hopper_4] [int] NULL,
	[cash_increment_hopper_5] [int] NULL,
	[cash_increment_hopper_6] [int] NULL,
	[circle] [nvarchar](100) NULL,
	[sitetype] [nvarchar](15) NULL,
	[msvendor] [nvarchar](30) NULL,
	[lastwithdrawaltime] [datetime] NULL,
	[switch] [nvarchar](50) NULL,
	[district] [nvarchar](50) NULL,
	[module] [nvarchar](100) NULL,
	[currentstatus] [nvarchar](10) NULL,
	[branchmanagerphone] [nvarchar](max) NULL,
	[channelmanagercontact] [nvarchar](max) NULL,
	[jointcustodianphone] [nvarchar](max) NULL,
	[network] [nvarchar](10) NULL,
	[populationgroup] [nvarchar](20) NULL,
	[regionname] [int] NULL,
	[statename] [nvarchar](255) NULL,
	[jointcustodianname1] [nvarchar](50) NULL,
	[jointcustodianphone2] [nvarchar](15) NULL,
	[last_deposit_txn_time] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
) 
GO
