
/****** Object:  Table [dbo].[vault_cash_balance]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vault_cash_balance](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[bank_name] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[feeder_branch_name] [nvarchar](50) NULL,
	[cra_name] [nvarchar](20) NULL,
	[agent] [nvarchar](50) NULL,
	[vault_balance_100] [int] NULL,
	[vault_balance_200] [int] NULL,
	[vault_balance_500] [int] NULL,
	[vault_balance_2000] [int] NULL,
	[total_vault_balance] [int] NULL,
	[dependency] [nvarchar](50) NULL,
	[remark] [nvarchar](max) NULL,
	[Date] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
)
GO
