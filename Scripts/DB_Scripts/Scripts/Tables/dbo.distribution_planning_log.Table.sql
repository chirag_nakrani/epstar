
/****** Object:  Table [dbo].[distribution_planning_log]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[distribution_planning_log](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[distribution_planning_master_id] [bigint] NULL,
	[message] [nvarchar](200) NULL,
	[date_created] [datetime] NULL
)
