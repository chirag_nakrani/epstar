/****** object:  table [dbo].[bank_escalation_emails]    script date: 12/12/2018 4:04:03 pm ******/
set ansi_nulls on
go
set quoted_identifier on
go
create table [dbo].[bank_escalation_emails](
	[id] [bigint] identity(1,1) primary key,
	[bank] [nvarchar](100) null,
	[branch] [nvarchar](100) null,
	[activity] [nvarchar](100) null,
	[primary_email_id] [nvarchar](max) null,
	[primary_contact_no] [nvarchar](100) null,
	[level_1_email_id] [nvarchar](200) null,
	[level_1_contact_no] [nvarchar](50) null,
	[level_2_email_id] [nvarchar](200) null,
	[level_2_contact_no] [nvarchar](50) null,
	[level_3_email_id] [nvarchar](200) null,
	[level_3_contact_no] [nvarchar](50) null,
	[level_4_email_id] [nvarchar](200) null,
	[level_4_contact_no] [nvarchar](50) null,
	[project_id] [nvarchar](50) null,
	[record_status] [nvarchar](50) null,
	[created_on] [datetime] null,
	[created_by] [nvarchar](50) null,
	[created_reference_id] [nvarchar](50) null,
	[approved_on] [datetime] null,
	[approved_by] [nvarchar](50) null,
	[approved_reference_id] [nvarchar](50) null,
	[approve_reject_comment] [nvarchar](50) null,
	[rejected_on] [datetime] null,
	[rejected_by] [nvarchar](50) null,
	[rejected_reference_id] [nvarchar](50) null,
	[modified_on] [datetime] null,
	[modified_by] [nvarchar](50) null,
	[modified_reference_id] [nvarchar](50) null,
	[is_valid_record] [nvarchar](20) null,
	[error_code] [nvarchar](50) null,
	[sol_id] [nvarchar](10) null,
	[circle] [nvarchar](100) null
)
go
