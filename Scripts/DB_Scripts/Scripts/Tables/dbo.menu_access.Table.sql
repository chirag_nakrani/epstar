/****** Object:  Table [dbo].[menu_access]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[menu_access](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[menu_id] [nvarchar](50) NOT NULL,
	[group_name] [nvarchar](50) NOT NULL,
	[group_id] [int] NULL
)
GO
