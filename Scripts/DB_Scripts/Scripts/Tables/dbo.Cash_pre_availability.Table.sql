drop table if exists cash_pre_availability
/****** object:  table [dbo].[cash_pre_availability]    script date: 27-11-2018 14:53:08 ******/
set ansi_nulls on
go
set quoted_identifier on
go
create table [dbo].[cash_pre_availability](
	[id] [bigint] identity(1,1) primary key,
	[from_date] [date] null,
	[to_date] [date] null,
	[project_id] [nvarchar](50) null,
	[bank_code] [nvarchar](50) null,
	[applied_to_level] [nvarchar](50) null,
	[feeder_branch_code] [nvarchar](50) null,
	[atm_id] [nvarchar](50) null,
	[available_50_amount] [int] null,
	[available_100_amount] [int] null,
	[available_200_amount] [int] null,
	[available_500_amount] [int] null,
	[available_2000_amount] [int] null,
	[total_amount_available] [int] null,
	[record_status] [nvarchar](50) null,
	[created_on] [datetime] null,
	[created_by] [nvarchar](50) null,
	[created_reference_id] [nvarchar](50) null,
	[approved_on] [datetime] null,
	[approved_by] [nvarchar](50) null,
	[approved_reference_id] [nvarchar](50) null,
	[approve_reject_comment] [nvarchar](50) null,
	[rejected_on] [datetime] null,
	[rejected_by] [nvarchar](50) null,
	[reject_reference_id] [nvarchar](50) null,
	[modified_on] [datetime] null,
	[modified_by] [nvarchar](50) null,
	[modified_reference_id] [nvarchar](50) null,
	[deleted_on] [datetime] null,
	[deleted_by] [nvarchar](50) null,
	[deleted_reference_id] [nvarchar](50) null,
	[is_valid_record] [nvarchar](20) null,
	[error_code] [nvarchar](50) null
)
go
