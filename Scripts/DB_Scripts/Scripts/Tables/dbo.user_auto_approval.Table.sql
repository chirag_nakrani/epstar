/****** Object:  Table [dbo].[user_auto_approval]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_auto_approval](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[user_name] [nvarchar](50) NULL,
	[auto_approval_c3r] [int] NULL,
	[auto_approval_master_data] [int] NULL,
	[auto_approval_pre_config_param] [int] NULL,
	[auto_approval_reference_data] [int] NULL,
	[auto_approval_time_in_hour] [int] NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [date] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [date] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](500) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL
)
GO
