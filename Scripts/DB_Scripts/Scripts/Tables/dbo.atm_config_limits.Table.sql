/****** Object:  Table [dbo].[ATM_Config_limits]    Script Date: 27-11-2018 17:32:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[ATM_Config_limits](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[site_code] [nvarchar](50) NULL,
	[old_atm_id] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[bank_name] [nvarchar](50) NULL,
	[bank_code] [nvarchar](10) NULL,
	[insurance_limit] [nvarchar](50) NULL,
	[whether_critical_atm] [nvarchar](5) NULL,
	[cassette_50_count] [int] NULL,
	[cassette_100_count] [int] NULL,
	[cassette_200_count] [int] NULL,
	[cassette_500_count] [int] NULL,
	[cassette_2000_count] [int] NULL,
	[total_cassette_count] [int] NULL,
	[bank_cash_limit] [int] NULL,
	[base_limit] [int] NULL,
	[s_g_locker_no] [nvarchar](50) NULL,
	[type_of_switch] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)
GO


