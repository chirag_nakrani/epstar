/****** Object:  Table [dbo].[app_config_param]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[app_config_param](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[category] [nvarchar](50) NULL,
	[sub_category] [nvarchar](50) NULL,
	[sequence] [int] NULL,
	[value] [nvarchar](255) NULL
) 
GO
