/****** Object:  Table [dbo].[cash_balance_file_psb]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cash_balance_file_psb](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY ,
	[atm_id] [nvarchar](15) NULL,
	[cash_out] [int] NULL,
	[endcash] [int] NULL,
	[hop1cash] [int] NULL,
	[hop1bill] [int] NULL,
	[hop2cash] [int] NULL,
	[hop2bill] [int] NULL,
	[hop3cash] [int] NULL,
	[hop3bill] [int] NULL,
	[hop4cash] [int] NULL,
	[hop4bill] [int] NULL,
	[ownr] [nvarchar](50) NULL,
	[state] [nvarchar](10) NULL,
	[timestamp] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
)
GO
