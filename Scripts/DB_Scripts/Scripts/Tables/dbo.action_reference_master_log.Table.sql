/****** Object:  Table [dbo].[action_reference_master_log]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[action_reference_master_log](
	[reference_no] [nvarchar](50) PRIMARY KEY,
	[project_id] [nvarchar](50) NULL,
	[bank_id] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[action_level] [nvarchar](50) NULL,
	[pending_action_id] [bigint] NULL,
	[event_description] [nvarchar](max) NOT NULL,
	[log_time] [datetime] NULL,
	[action_type] [nvarchar](255) NULL,
	[action_datetime] [datetime] NULL,
	[action_by_user] [nvarchar](50) NULL,
	[user_role] [nvarchar](50) NULL,
	[user_comment] [nvarchar](500) NULL
) 
GO
