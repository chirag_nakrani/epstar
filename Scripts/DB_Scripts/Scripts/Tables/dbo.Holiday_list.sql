/****** Object:  Table [dbo].[holiday_list]    Script Date: 03-12-2018 13:17:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[holiday_list](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[holiday_code] [nvarchar](50) NULL,
	[holiday_name] [nvarchar](100) NULL,
	[description] [nvarchar](100) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)    
GO

