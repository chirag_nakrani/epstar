
/****** Object:  Table [dbo].[user_auth_info]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user_auth_info](
	[username] [nvarchar](max) NOT NULL,
	[userkey] [nvarchar](450) PRIMARY KEY,
	[timestamp_key] [datetime] NOT NULL,
	[machine_info] [nvarchar](max) NULL,
	[request_params] [nvarchar](max) NULL
	)