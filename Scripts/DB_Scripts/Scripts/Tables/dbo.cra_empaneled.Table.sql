GO
/****** Object:  Table [dbo].[cra_empaneled]    Script Date: 11-12-2018 18:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cra_empaneled](
	[Id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[cra_code] [nvarchar](50) NULL,
	[cra_name] [nvarchar](500) NULL,
	[registered_office] [nvarchar](max) NULL,
	[registration_number] [nvarchar](50) NULL,
	[contact_details] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[project_id] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[rejected_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
)
GO
