/****** Object:  Table [dbo].[cash_balance_file_rsbl]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[cash_balance_file_rsbl](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY ,
	[cfb_date_time] [nvarchar](50) NULL,
	[atm_id] [nvarchar](15) NULL,
	[i_denom] [int] NULL,
	[i_begin_cash] [int] NULL,
	[i_dispense] [int] NULL,
	[i_remining] [int] NULL,
	[ii_denom] [int] NULL,
	[ii_begin_cash] [int] NULL,
	[ii_dispense] [int] NULL,
	[ii_remining] [int] NULL,
	[iii_denom] [int] NULL,
	[iii_begin_cash] [int] NULL,
	[iii_dispense] [int] NULL,
	[iii_remining] [int] NULL,
	[iv_denom] [int] NULL,
	[iv_begin_cash] [int] NULL,
	[iv_dispense] [int] NULL,
	[iv_remining] [int] NULL,
	[project_id] [nvarchar](50) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[datafor_date_time] [datetime] NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[reject_trigger_reference_id] [nvarchar](50) NULL,
	[deleted_on] [datetime] NULL,
	[deleted_by] [nvarchar](50) NULL,
	[deleted_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
)
GO
