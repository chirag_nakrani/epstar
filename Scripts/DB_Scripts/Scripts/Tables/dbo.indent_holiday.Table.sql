/****** Object:  Table [dbo].[indent_holiday]    Script Date: 23-04-2019 13:22:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[indent_holiday](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[holiday_date] [datetime] NULL,
	[is_holiday] [nvarchar](10) NULL,
	[holiday_code] [nvarchar](50) NULL,
	[holiday_name] [nvarchar](100) NULL,
	[holiday_type] [nvarchar](50) NULL,
	[applied_to_level] [nvarchar](50) NULL,
	[state_code] [nvarchar](50) NULL,
	[district_code] [nvarchar](50) NULL,
	[city_code] [nvarchar](50) NULL,
	[area_code] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[withdrawal_type] [nvarchar](100) NULL,
	[Feeder_branch] [nvarchar](100) NULL,
	[is_indent_required] [nvarchar](100) NULL,
	[cra] [nvarchar](100) NULL,
	[record_status] [nvarchar](50) NULL,
	[Project_id] [varchar](50) NULL,
	[Bank_Code] [nvarchar](30) NULL,
	[is_valid_record] [nvarchar](10) NULL,
	[error_code] [nvarchar](50) NULL
)
GO