
/****** Object:  Table [dbo].[eod_pre_activity]    Script Date: 27-11-2018 14:53:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eod_pre_activity](
	[id] [int] IDENTITY(1,1) PRIMARY KEY,
	[for_date] [datetime] NULL,
	[project_id] [nvarchar](50) NULL,
	[bank_code] [nvarchar](10) NULL,
	[feeder] [nvarchar](50) NULL,
	[site_code] [nvarchar](50) NULL,
	[atm_id] [nvarchar](50) NULL,
	[is_performed_eod] [nvarchar](10) NULL,
	[record_status] [nvarchar](50) NULL,
	[created_on] [datetime] NULL,
	[created_by] [nvarchar](50) NULL,
	[created_reference_id] [nvarchar](50) NULL,
	[approved_on] [datetime] NULL,
	[approved_by] [nvarchar](50) NULL,
	[approved_reference_id] [nvarchar](50) NULL,
	[approve_reject_comment] [nvarchar](50) NULL,
	[rejected_on] [datetime] NULL,
	[rejected_by] [nvarchar](50) NULL,
	[reject_reference_id] [nvarchar](50) NULL,
	[modified_on] [datetime] NULL,
	[modified_by] [nvarchar](50) NULL,
	[modified_reference_id] [nvarchar](50) NULL,
	[is_valid_record] [nvarchar](20) NULL,
	[error_code] [nvarchar](50) NULL
	)
