GO
/****** Object:  Table [dbo].[metadatatablereference]    Script Date: 11-12-2018 18:37:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[metadatatablereference](
	[id] [bigint] IDENTITY(1,1) PRIMARY KEY,
	[file_type] [varchar](100) NULL,
	[columnHeaders] [varchar](100) NULL,
	[IsRequired] [varchar](10) NULL,
	[table_column] [varchar](300) NULL,
	[display_column_name] [varchar](300) NULL,
	[table_name] [varchar](250) NULL
) 
GO
