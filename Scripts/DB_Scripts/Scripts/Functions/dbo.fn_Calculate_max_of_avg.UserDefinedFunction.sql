
/****** Object:  UserDefinedFunction [dbo].[fn_Calculate_max_of_avg]    Script Date: 12-12-2018 17:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 --- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: calculated maximum of the average dispense over three months
 --- =========================================================================

 
 CREATE FUNCTION [dbo].[fn_Calculate_max_of_avg]
(@atm_id varchar(20),@datetoday date,@bank varchar(50), @project varchar(30))
RETURNS int
AS
BEGIN
  

/*

declare @datetoday as date
set @datetoday='2018-10-15'  --15 coctober
Select  CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@datetoday)  )  , 105)	

*/
--Select * from cash_dispense_dataset1

declare @avg1 int,@avg2 int, @avg3 int,@avg4 int,@avg_of_avg int,@max_of_avg int

-- Get average of total dispense amount from three months ago
Set @avg1=  ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_3]  where   atm_id=@atm_id  and bank=@bank and project_id=@project)

-- Get average of total dispense amount from two months ago
Set @avg2= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_2]  where  atm_id=@atm_id  and bank=@bank and project_id=@project )

-- Get average of total dispense amount from one month ago
Set @avg3= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_1] where   atm_id=@atm_id  and bank=@bank and project_id=@project)


-- Get average of total dispense amount from current month ago
Set @avg4= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_0]  where  atm_id=@atm_id  and bank=@bank and project_id=@project)


 
SET @max_of_avg= 
(
SELECT MAX(n)
FROM (VALUES (@avg1), (@avg2), (@avg3), (@avg4)) AS i(n)
)
 
 return @max_of_avg
  
 END

 
