 

/****** Object:  UserDefinedFunction [dbo].[fn_Calculate_avg_of_secondmax]    Script Date: 20-12-2018 19:37:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 

 --- ========================================================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: This procedure calculates average dispense over three months for second maximum dispense amount
 --- =========================================================================================================

 
 CREATE FUNCTION [dbo].[fn_Calculate_avg_of_secondmax]
(@atm_id varchar(20),@datetoday date,@bank varchar(50), @project varchar(30))
RETURNS int
AS
BEGIN
  

 
declare @max1 int,@max2 int, @max3 int,@max4 int,@max_of_secondmax int,@avg_of_secondmax int
 

 --  get second max amount from three month ago
Set @max1= (SELECT MAX( total_dispense_amount )
				  FROM [cash_dispense_month_minus_3]
				 WHERE total_dispense_amount < ( SELECT MAX( total_dispense_amount )
                 FROM [cash_dispense_month_minus_3]  where   atm_id=@atm_id and bank=@bank and project_id=@project )
				 and atm_id=@atm_id and bank=@bank and project_id=@project)


--  get second max amount from two month ago
Set @max2= (SELECT MAX( total_dispense_amount )
			  FROM [cash_dispense_month_minus_2]
			 WHERE total_dispense_amount < ( SELECT MAX( total_dispense_amount )
                 FROM [cash_dispense_month_minus_2]  where   atm_id=@atm_id  and bank=@bank and project_id=@project)
				 and atm_id=@atm_id  and bank=@bank and project_id=@project
				 )

--  get second max amount from one month ago

Set @max3= (SELECT MAX( total_dispense_amount )
				  FROM [cash_dispense_month_minus_1]
				 WHERE total_dispense_amount < ( SELECT MAX( total_dispense_amount )
                 FROM [cash_dispense_month_minus_1]  where   atm_id=@atm_id  and bank=@bank and project_id=@project )
				 and atm_id=@atm_id  and bank=@bank and project_id=@project
				 )


--  get second max amount from current month 

Set @max4= (SELECT MAX( total_dispense_amount )
				  FROM [cash_dispense_month_minus_0]
				 WHERE total_dispense_amount < ( SELECT MAX( total_dispense_amount )
                 FROM [cash_dispense_month_minus_0] where   atm_id=@atm_id  and bank=@bank and project_id=@project)
				 and atm_id=@atm_id  and bank=@bank and project_id=@project
				 )

 
--SET @avg_of_secondmax= 
--(
--SELECT  (ISNULL(@max1,0)+ISNULL(@max2,0)+ISNULL(@max3,0)+ISNULL(@max4,0))/4
--)
 
SET @avg_of_secondmax= 
(
SELECT avg(n)
FROM (VALUES (@max1), (@max2), (@max3), (@max4)) AS i(n)
)  
 
 
 return @avg_of_secondmax
  
 END

 
GO


