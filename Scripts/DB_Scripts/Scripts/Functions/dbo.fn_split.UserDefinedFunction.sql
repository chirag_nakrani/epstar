
/****** Object:  UserDefinedFunction [dbo].[fn_split]    Script Date: 27-11-2018 14:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fn_split] (@columnname varchar(max),@delim varchar(5))
RETURNS
TABLE
AS
RETURN
	WITH Split
    AS(
        SELECT CAST(0 as varchar(max)) AS stpos, cast(CHARINDEX(@delim,@columnname) as varchar(max)) AS endpos
        UNION ALL
        SELECT cast(endpos+1 as varchar(max)), CAST(CHARINDEX(@delim,@columnname,endpos+1) as varchar(max))
            FROM Split
            WHERE endpos > 0
		)
		select SubString(@columnname,cast(stpos as int),cast(endpos as int)-cast(stpos as int)) as ColumnHeaders from Split
		where endpos <> 0

GO
