
/****** Object:  UserDefinedFunction [dbo].[fn_max 28days_avg]    Script Date: 12-12-2018 17:24:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: calculated maximum of average dispense amount over each week from past three months and current month
 --- =========================================================================
  
 
 CREATE FUNCTION [dbo].[fn_max 28days_avg]
(@atm_id varchar(20),@datetoday date,@bank varchar(50), @project varchar(30))
RETURNS int


AS
BEGIN 

DECLARE @avg_from_28days int,
                @T2_dispense int,
				@nreturn int


-- A) get average of dispense amount from each month from indentdate till three months ago

Set @avg_from_28days=  ( Select avg(total_dispense_amount) from 
(
Select    total_dispense_amount from cash_dispense_month_minus_3 where atm_id=@atm_id and bank=@bank and project_id=@project
 union
Select  total_dispense_amount from cash_dispense_month_minus_2  where atm_id=@atm_id and bank=@bank and project_id=@project
union
Select  total_dispense_amount from cash_dispense_month_minus_1  where atm_id=@atm_id and bank=@bank and project_id=@project
union
Select  total_dispense_amount from cash_dispense_month_minus_0  where atm_id=@atm_id and bank=@bank and project_id=@project
								)		as a			
								)		
								
-- B)  Get dispense amount for T-2 from indentdate											 
SET @T2_dispense = (Select total_dispense_amount from cash_dispense_register 
							where  atm_id=@atm_id and  bank_name=@bank and project_id=@project
							and
							 datafor_date_time = 
							 CONVERT (date, ( CONVERT(varchar,    DATEADD(month,0, DATEADD(day,-2,@datetoday)  )  , 105) ),105)
							 and record_status= 'active'  -- and isValidRecord  is null-- add isvalid collumn in condition
                                  )
 
 --Compare which one is greater A or B and select the greater from the same
  

 IF @avg_from_28days> @T2_dispense
         BEGIN
			 
			 Select  @nreturn=@avg_from_28days 

			 END
 else
			 BEGIN
			 Select @nreturn= @T2_dispense
			END
			return @nreturn
 END

 
 


