
/****** Object:  UserDefinedFunction [dbo].[fn_Calculate_max_of_max]    Script Date: 12-12-2018 17:17:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
  --- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: calculated maximum of the maximum dispense over three months
 --- =========================================================================

 
 CREATE FUNCTION [dbo].[fn_Calculate_max_of_max]
(@atm_id varchar(20),@datetoday date,@bank varchar(50), @project varchar(30))
RETURNS int
AS
BEGIN
  

/*

declare @datetoday as date
set @datetoday='2018-10-15'  --15 coctober
Select  CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@datetoday)  )  , 105)	

*/
--Select * from cash_dispense_dataset1

declare @max1 int,@max2 int, @max3 int,@max4 int,@max_of_max int,@avg_of_max int
-- get maximum dispense amount from three months ago
Set @max1=  ( Select max(total_dispense_amount) from [cash_dispense_month_minus_3]  where   atm_id=@atm_id  and bank=@bank and project_id=@project)

-- get maximum dispense amount from two months ago
Set @max2= ( Select max(total_dispense_amount) from [cash_dispense_month_minus_2]  where   atm_id=@atm_id  and bank=@bank and project_id=@project)

-- get maximum dispense amount from one months ago
Set @max3= ( Select max(total_dispense_amount) from [cash_dispense_month_minus_1] where   atm_id=@atm_id  and bank=@bank and project_id=@project)

-- get maximum dispense amount from current  month 
Set @max4= ( Select max(total_dispense_amount) from [cash_dispense_month_minus_0]  where   atm_id=@atm_id  and bank=@bank and project_id=@project)


 
SET @max_of_max= 
(
SELECT MAX(n)
FROM (VALUES (@max1), (@max2), (@max3), (@max4)) AS i(n)
)
 
 return @max_of_max
  
 END

 
