/****** Object:  UserDefinedFunction [dbo].[fn_Calculate_avg_of_avg]    Script Date: 12-12-2018 17:01:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: calculated average of the average dispense over three months
 --- =========================================================================

 CREATE FUNCTION [dbo].[fn_Calculate_avg_of_avg]
(@atm_id varchar(20),@datetoday date,@bank varchar(50), @project varchar(30))
RETURNS int
AS
BEGIN
  

/*

declare @datetoday as date
set @datetoday='2018-10-15'  --15 coctober
Select  CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@datetoday)  )  , 105)	

*/
--Select * from cash_dispense_dataset1

declare @avg1 int,@avg2 int, @avg3 int,@avg4 int,@avg_of_avg int,@max_of_avg int

-- calculates average dispense three months ago

Set @avg1=  ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_3]  where   atm_id=@atm_id   and bank=@bank and project_id=@project)

-- calculates average dispense two months ago
Set @avg2= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_2]  where  atm_id=@atm_id   and bank=@bank and project_id=@project )

-- calculates average dispense one month ago
Set @avg3= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_1] where   atm_id=@atm_id  and bank=@bank and project_id=@project )

-- calculates average dispense current month 

Set @avg4= ( Select avg(total_dispense_amount) from [cash_dispense_month_minus_0]  where  atm_id=@atm_id  and bank=@bank and project_id=@project)


 
--SET @avg_of_avg= 
 --(Select (ifnull(@avg1,0)+ifnull(@avg2,0)+ifnull(@avg3,0)+ifnull(@avg4,0))/4)
 
SET @avg_of_avg= 
(
SELECT avg(n)
FROM (VALUES (@avg1), (@avg2), (@avg3), (@avg4)) AS i(n)
) 
 
 return @avg_of_avg
  
 END

 
