
/****** Object:  UserDefinedFunction [dbo].[fn_Calculate_max_of_secondmax]    Script Date: 12-12-2018 17:19:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
 --- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: calculated maximum of the second maximum dispense amount over three months
 --- =========================================================================

 
 CREATE FUNCTION [dbo].[fn_Calculate_max_of_secondmax]
(@atm_id varchar(20),@datetoday date,@bank varchar(50), @project varchar(30))
RETURNS int
AS
BEGIN
  

/*

declare @datetoday as date
set @datetoday='2018-10-15'  --15 coctober
Select  CONVERT(varchar,    DATEADD(month,-2, DATEADD(day,-3,@datetoday)  )  , 105)	

*/
--Select * from cash_dispense_dataset1

declare @max1 int,@max2 int, @max3 int,@max4 int,@max_of_secondmax int,@avg_of_secondmax int

--Set @max1=  ( Select max(total_dispense_amount) from cash_dispense_dataset1 where    atm_id=@atm_id )
--Set @max2= ( Select max(total_dispense_amount) from cash_dispense_dataset2  where   atm_id=@atm_id )
--Set @max3= ( Select max(total_dispense_amount) from cash_dispense_dataset3 where    atm_id=@atm_id )
--Set @max4= ( Select max(total_dispense_amount) from cash_dispense_dataset4  where   atm_id=@atm_id )

-- get second max dispense amount for three months ago
Set @max1= (SELECT MAX( total_dispense_amount )
			  FROM [cash_dispense_month_minus_3]
			 WHERE total_dispense_amount < ( SELECT MAX( total_dispense_amount )
										FROM [cash_dispense_month_minus_3]  
										where   atm_id=@atm_id and bank=@bank and project_id=@project )
										and     atm_id=@atm_id and bank=@bank and project_id=@project
							 )

-- get second max dispense amount for two months ago
 Set @max2= (SELECT MAX( total_dispense_amount )
			  FROM [cash_dispense_month_minus_2]
			  WHERE total_dispense_amount < ( SELECT MAX( total_dispense_amount )
				FROM [cash_dispense_month_minus_2]  where   atm_id=@atm_id  and bank=@bank and project_id=@project)
				and     atm_id=@atm_id and bank=@bank and project_id=@project
				)


-- get second max dispense amount for one months ago
Set @max3= (SELECT MAX( total_dispense_amount )
			  FROM [cash_dispense_month_minus_1]
			 WHERE total_dispense_amount < ( SELECT MAX( total_dispense_amount )
							 FROM [cash_dispense_month_minus_1]  where   atm_id=@atm_id and bank=@bank and project_id=@project) 
							  and     atm_id=@atm_id and bank=@bank and project_id=@project
							 )

-- get second max dispense amount for current month

Set @max4= (SELECT MAX( total_dispense_amount )
  FROM [cash_dispense_month_minus_0]
 WHERE total_dispense_amount < ( SELECT MAX( total_dispense_amount )
                 FROM [cash_dispense_month_minus_0] where   atm_id=@atm_id  and bank=@bank and project_id=@project )
					  and     atm_id=@atm_id and bank=@bank and project_id=@project			 
				 )

 
SET @max_of_secondmax= 
(
SELECT MAX(n)
FROM (VALUES (@max1), (@max2), (@max3), (@max4)) AS i(n)
)
 
 return @max_of_secondmax
  
 END

 
