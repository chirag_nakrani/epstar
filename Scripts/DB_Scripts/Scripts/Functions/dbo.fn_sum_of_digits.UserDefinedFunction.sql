 

/****** Object:  UserDefinedFunction [dbo].[fn_sum_of_digits]    Script Date: 09-01-2019 11:18:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

 -- Select [dbo].[fn_sum_of_digits](990)
 -- =========================================================================
 --- Created By :Rubina Q
 --- Created Date: 12-12-2018
 --- Description: Calculation of  sum of all digits in a number
 --- =========================================================================

CREATE function [dbo].[fn_sum_of_digits](@number as int)
returns int


as BEGIN

 DECLARE @out varchar(max)

;WITH i AS (
  SELECT @Number / 10 n, 
         @Number % 10 d

UNION ALL

  SELECT n / 10,
		 n % 10
		 FROM i
		 WHERE n > 0
   )

SELECT @out = SUM(d) FROM i

RETURN @out

end
GO


