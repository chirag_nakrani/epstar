import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndentSummaryReportComponent } from './indent-summary-report.component';

describe('IndentSummaryReportComponent', () => {
  let component: IndentSummaryReportComponent;
  let fixture: ComponentFixture<IndentSummaryReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndentSummaryReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndentSummaryReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
