import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import * as FileSaver from 'file-saver';



declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-indent-summary-report',
  templateUrl: './indent-summary-report.component.html',
  styleUrls: ['./indent-summary-report.component.scss']
})
export class IndentSummaryReportComponent implements OnInit {
  type="Indent Summary Report"
  public colList: any =[{}];
  viewColumnAll: any = {};
  rows: any = [];
  alert: any;
  page = new Page();
  previewData: any;
  filterData: any = {};
  datePipe ;
  public filterDate: Date = null;
  blob: Blob;
  fileName: string;
  public bankList: any;
  public projectBankList: any;
  public statusList = ['All' ,'Active' ,'Deleted' ,'History' ,'Manually deleted' ,'Rejected' ,'Review Pending'];
  public currentPageSize: number;
  public currentPage = 1;
  public isBankValid: boolean = true;
  constructor(private apiService: ApiService, private columnConfig: ColumnConfig) {
    this.datePipe = new DatePipe('en-US');
    this.page.size = 20;
    this.currentPageSize = 20;
    this.alert = {};
    this.alert.isvisible = false;
  }

  ngOnInit() {
    $('#modalfilter').modal('show');
    this.getViewCol();
    this.dropdownsForProjectBank();
  }
  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }
  InsertDatafn(offSet:number) {
    if('bank' in this.filterData) {
      if(this.filterData.bank == null || this.filterData.bank == ''){
        this.isBankValid = false;
        return;
      }else{
        this.isBankValid = true;
      }
    }else{
      this.isBankValid = false;
      return;
    }
    this.page.pageNumber = offSet;
    // this.page.pageNumber = this.currentPage + offSet;
    const params = this.filterData;
    params.type=this.type,
    console.log("paramsparams--------",params);
    if (this.filterDate != null) {
        params.indent_date = this.datePipe.transform(this.filterDate, 'yyyy-MM-dd');
      }
    for (let key in params) {
      if (params[key].toLowerCase() == "all") {
          delete params[key];
      }
    }

      this.apiService.post('indent_summary_report/?page=' + (offSet + 1) + '&page_size=' + this.page.size, params).subscribe((response: any) => {
        console.log('resonse------', response);

          this.rows = response.results;
          this.rows = [...this.rows];
          this.page.totalElements = response.count;
          this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
        $('#modalfilter').modal('hide');

    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#modalfilter').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
  }

  resetFilter() {
    this.filterDate = null;
    this.filterData = {};
  }

  dropdownsForProjectBank() {
    const params = {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    };
    this.apiService.post('bankinfo/', params).subscribe((data: any) => {
       this.bankList = [];
       this.projectBankList = data.data;
       this.projectBankList.forEach( x => {
         x.bank.forEach( y => {
           this.bankList.push(y);
         });
       });
      this.bankList = this.removeDuplicate(this.bankList, "bank_id");
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }

  removeDuplicate (arr: any, comp: string) {

    const finalArr = arr
                  .map(el => el[comp]) // store the keys of the unique objects
                  .map((el, index, a) => a.indexOf(el) === index && index) // eliminate the dead keys & store unique objects
                  .filter(el => arr[el])
                  .map(el => arr[el]);
     return finalArr;

  }

  exportReport(){
    const params = this.filterData;
    params.type=this.type,
    console.log("paramsparams--------",params);
    if (this.filterDate != null) {
      params.indent_date = this.datePipe.transform(this.filterDate, 'yyyy-MM-dd');
    }
    params.export = 1;
      this.apiService.getFile('indent_summary_report/', params).subscribe((response: Blob) => {
      console.log('resonse------', response);
      this.blob = new Blob([(response)], { type: 'application/pdf' });
      this.fileName = params.type+'_'+params.bank+'_'+params.indent_date;
      FileSaver.saveAs(this.blob, this.fileName + '.xlsx');

    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#modalfilter').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
  }

  getViewCol() {

    this.colList = [];

    this.columnConfig.columnConfigindentSummaryReport.forEach(x => {
      this.colList.push({
        'name': x.display_column_name,
        'prop': x.table_column,
      });
    });
    this.viewColumnAll = this.colList;
    console.log('viewColumn', this.viewColumnAll);
  }

}
