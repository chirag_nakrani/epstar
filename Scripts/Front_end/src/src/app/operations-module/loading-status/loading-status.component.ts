import { Component, OnInit , TemplateRef } from '@angular/core';
import {ViewChild, ElementRef} from '@angular/core';

import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Page} from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';

declare var jquery: any;   // not required
declare var $: any;   // not required


@Component({
  selector: 'app-loading-status',
  templateUrl: './loading-status.component.html',
  styleUrls: ['./loading-status.component.scss']
})
export class LoadingStatusComponent implements OnInit {


  @ViewChild('previewModal') previewModal: ElementRef;

  fileType = 'DISPENSE';
    modalRef: BsModalRef;
  alert: any;
  page = new Page();
  previewPage = new Page();

  public uploadData: any;
  uploadForm: FormGroup;
  submitted = false;
  temp = [];
  datePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: ColumnConfig;
  public permissionList: any = [];
  public bankDropDown: any = [];
  public feederRegionDropDown: any = [];
  fileName: string;
  public date_range: any = [];
  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];
  public statusList = ['All', 'Active', 'Deleted'];
  public filterData: any = {};

  viewColumnAll: any = [
    {
      name: 'ID',
      prop: 'id',
      datatype: 'string'
    },
    {
      name: 'Indent Order Number',
      prop: 'indent_order_number',
      datatype: 'string'
    },
    {
      name: 'Site Code',
      prop: 'site_code',
      datatype: 'string'
    },
    {
      name: 'Project',
      prop: 'project_id',
      datatype: 'string'
    },
    {
      name: 'Atm Id',
      prop: 'atm_id',
      datatype: 'string'
    },
    {
      name: 'Bank Code',
      prop: 'bank_code',
      datatype: 'string'
    },
    {
      name: 'Feeder Branch Code',
      prop: 'feeder_branch_code',
      datatype: 'string'
    }, {
      name: 'Bank Code',
      prop: 'bank_code',
      datatype: 'string'
    },
    {
      name: 'Cra',
      prop: 'cra',
      datatype: 'string'
    },
    {
      name: 'Indent Date',
      prop: 'indent_date',
      datatype: 'date'
    },
    {
      name: 'Loading Status',
      prop: 'loading_status',
      datatype: 'string'
    },
    {
      name: 'Record Status',
      prop: 'record_status',
      datatype: 'string'
    }
  ];
  //  --------------code for select column -------------
  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];
  public bankFilterDropDown: any = [];
    constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
      this.datePipe = new DatePipe('en-US');
      this.uploadData = {};
      this.alert = {};
      this.alert.isvisible = false;
      this.page.pageNumber = 0;
      this.page.size = 20;

      this.previewPage.pageNumber = 0;
      this.previewPage.size = 20;
      this.dropdownValue = [];
      this.filterDropDownValue = {};
     }


    ngOnInit() {
      if(this.sessionStorageService.get('permissionList') != null)
      this.permissionList = this.sessionStorageService.get('permissionList');
      this.uploadData.date = new Date();
      this.uploadData.time = '12:00';
      this.uploadForm = this.formBuilder.group({
        bank_code: ['', Validators.required],
        cra: ['', Validators.required],
        project_id: ['', Validators.required],
        date: [ this.uploadData.date],
        time: [  this.uploadData.time],
    });


      this.updateViewAll();
      this.populateSelectColumn();
      this.populateDropDown();
    }











    // ----------------------- upload file ------------------------------------

  // convenience getter for easy access to form fields
  get f() { return this.uploadForm.controls; }

  openTimeSelector() {
      console.log('time picker opened');
      const amazingTimePicker = this.atp.open();
      amazingTimePicker.afterClose().subscribe(time => {
        this.uploadData.time = time;
      console.log('time picker closed');

      });
  }


  onFileChanged(event) {

    //const file = event.target.files[0]

    this.uploadData.file = event.target.files[0];
  }

  getAllBankList() {
    this.bankFilterDropDown = [];
    this.dropdownValue.forEach( x => {
      x.bank.forEach( y => {
        this.bankFilterDropDown.push(y);
      });
    });
  }

  // uploadDatafn() {
  //   this.submitted = true;

  //   // stop here if form is invalid
  //   if (this.uploadData.invalid) {
  //       return;
  //   }
  //   console.log('upload clicked----------', this.uploadData);
  //   this.previewColList = this.columnConfig.columnConfig.filter(x => x.datafor == this.fileType && x.bankcode == this.uploadData.bank_id);



  //   // Selected date formatting
  //     // const hours_min = this.uploadForm.value.time.split(':');
  //     this.uploadData.upload_datetime = new Date(this.uploadForm.value.date);
  //     this.uploadData.upload_datetime.setHours('0','0', '00');
  //     const date = this.datePipe.transform(this.uploadData.upload_datetime, 'yyyy-MM-dd HH:mm:ss');
  //   //
  // const fileData = new FormData();
  // //uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
  // fileData.append('file', this.uploadData.file, this.uploadData.file.name);
  // fileData.append('bank_code', this.uploadData.bank_id);
  // fileData.append('project_id', this.uploadData.project_id);
  // fileData.append('region', this.uploadData.region);
  // // fileData.append('project_id', "MOF");
  // // fileData.append('cra', this.uploadData.cra);
  // fileData.append('file_type', this.fileType);
  // fileData.append('upload_datatime', date);
  // console.log('upload clicked formdata', fileData);
  // this.apiService.uploadFile('upload/', fileData).subscribe(event => {
  //     console.log('event', event); // handle event here
  //     const response: any = event;
  //    if (response && response.body) {
  //      console.log('resonse', response.body);
  //     //if(response.body.status_text=="Data Validation Successful" || response.body.status_text=="Partial Valid File"){
  //       this.alert.isvisible = true;
  //       this.alert.type = 'Upload';
  //       this.alert.class = 'success';
  //      this.alert.text = ' ' + response.body.status_text;

  //      $('#m_modal_6').modal('hide');
  //      this.updateViewAll();
  //     //this.updatePreview();
  //     //this.openPreviewModal();
  //     // }
  //     // else{
  //     //   this.alert.isvisible=true;
  //     //   this.alert.type="Upload";
  //     //   this.alert.class="danger"
  //     //   this.alert.text=" "+ response.body.status_text;
  //     // }
  //    }
  //   }, (error => {
  //     console.log('error', error);
  //     // alert("Error - "+ error.status+ " "+error.statusText);
  //     $('#m_modal_6').modal('hide');

  //     this.alert.isvisible = true;
  //     this.alert.type = 'Error';
  //     let error_str : string = '';
  //     if (typeof(error.error['status_text']) == 'object') {
  //         error.error['status_text'].forEach(x => {

  //           error_str = error_str + ' | ' + x;
  //         });
  //       } else {
  //         error_str = error.error['status_text'];
  //       }

  //     this.alert.text =  error_str;

  //     this.alert.class = 'danger';
  //   }));
  //   this.clearUploadFormData();

  // }

  // ------------------------------ upload end -----------------------------------

  clearUploadFormData() {
    this.fileName = '';
    this.uploadData.bank_id = [];
    this.uploadData.project_id = [];
    this.uploadData.region = [];
    this.uploadData.file = [];
  }

  getBankList(){
    // console.log("----",this.uploadData.project.bank)
       this.bankDropDown = this.dropdownValue.filter( x => x.project_id == this.uploadData.project_id )[0].bank;
     }

     getFeederRegionList(){
       this.feederRegionDropDown = this.bankDropDown.filter( x => x.bank_id == this.uploadData.bank_id )[0].region;
     }













  populateSelectColumn() {
    this.viewColumnAll.filter( (col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
  this.viewColumn = this.viewColumnAll.filter( (col) => {
    return this.selectedColumn.includes(col.prop);
  });
  // }
  console.log('col change', this.selectedColumn, this.viewColumn);
  }

// ----------------- end of code for select column --------
// ----------------- code for search   -----------------
  viewSearch(event) {
        console.log('updateing');
          const val = event.target.value.toLowerCase();

          const temp = this.tempRows.filter(function(d) {
              const search_atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
              const search_bank_name = d.bank_name.toLowerCase().indexOf(val) !== -1;
              const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
             return search_atm_id || search_bank_name || search_project_id || !val ;
             });

          this.rows = temp;
  }
// -----------------end of code for search   -----------------

  updateViewAll() {
    console.log('updarte called');
    console.log('filter parameter', this.filterData);
    this.setPage({ offset: 0 });
    $('#modalfilter').modal('hide');
  }

  resetFilter() {
    this.filterData = {};
    this.date_range = [];
  }
  setPage(pageInfo) {
    console.log("Inmside %%%%%%%%%%%%%%%%%%%%%%%%% set page funtion called",pageInfo)
    if (this.date_range) {
      this.date_range = this.date_range.map( x => {
        return this.datePipe.transform(new Date(x), 'yyyy-MM-dd');
      });
    }
    let params: any;
   
    params = {
      'file_type': "CONFIRMLOADING"
    };
    if (this.filterData != undefined || this.filterData != {}) {
      const filter = this.filterData;
     
      params.filters = filter;
    }
    console.log("111111111111111111111 final params",params)
    this.page.pageNumber = pageInfo.offset;
    this.apiService.post('view/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size, params).subscribe(data => {
        console.log('-------get all');
     console.log("***************************************"); // handle event here
    //  this.filterData={};
     const response: any = data;
      //   this.page = pagedData.page;
      // this.temp = [...data];

      // push our inital complete list
    this.rows = response.results;
     this.tempRows = response.results;
     this.page.totalElements = response.count;
    });
  }



    // ---------end of  view all data ----------------------






    // --------preview modal ----------
  previewRows: any = [];
  tempPreviewRow: any = [];
  previewFilter: any;

  previewSearch(event) {
  console.log('updateing');
    const val = event.target.value.toLowerCase();
    // filter our data
    const filterParam: any[] = [];

    this.previewColList.forEach(x => {
      filterParam.push([x.table_column, val]);
    });

    const temp = this.searchFilter(filterParam);

    // update the rows
    this.previewRows = temp;
  }

  searchFilter(filters): any[] {
    const returnVal: any[] = [];
    let columnName: string;
    let val: string;
      this.tempPreviewRow.forEach(x => {
          for (let i = 0; i < filters.length; i++) {
            if (filters != undefined) {
              columnName = filters[i][0];
              val = filters[i][1].toString();
              if (x[columnName].toString().toLowerCase().indexOf(val) !== -1) {
                if (returnVal.indexOf(x) == -1) {
                  returnVal.push(x);
                }
              }
            }
          }
      });
    return returnVal;
  }




  setPreviewPage(pageInfo) {
    console.log('page clicked');
    this.previewPage.pageNumber = pageInfo.offset;

    this.updatePreview();
  }


  updatePreview() {
    console.log("@@@@@@@@@@@@@@@@@@ inside update Preview funtion")
  const hours_min = this.uploadData.time.split(':');
    this.uploadData.upload_datetime = new Date(this.uploadData.date);
    this.uploadData.upload_datetime.setHours(hours_min[0], hours_min[1]);
    const date = this.datePipe.transform(this.uploadData.upload_datetime, 'yyyy-MM-dd HH:mm:ss');

      this.apiService.post('view/?page=' + (this.previewPage.pageNumber + 1) + '&' + 'page_size=' + this.previewPage.size, {
        'page': this.page,
        'file_type': this.fileType,
        'bank_code': this.uploadData.bank_code.bank_id,
        'filters' : {
          //  "datafor_date_time":"2018-09-26 18:16:23",
          'datafor_date_time': date,
          'project_id': this.uploadData.project.project_id,
  'record_status': 'Approval Pending',
        }}).subscribe(data => {
          console.log('-------get all preview');
       console.log(data); // handle event here

       const response: any = data;
    
        this.tempPreviewRow = response.results;

       this.previewRows = response.results;
       this.previewPage.totalElements = response.count;

      // this.tempPreviewRow.push(response.results);
      this.tempPreviewRow = [...response.results];
      // this.previewRows=[];
      // this.previewRows.push(response.results);
       this.previewRows = [...response.results];
      // this.previewRows = this.previewRows[0]

       console.log('prevew data', this.previewRows);
      });
    }


    approvePreviewFn(status) {
      const hours_min = this.uploadData.time.split(':');
      this.uploadData.upload_datetime = new Date(this.uploadData.date);
      this.uploadData.upload_datetime.setHours(hours_min[0], hours_min[1]);
      const date = this.datePipe.transform(this.uploadData.upload_datetime, 'yyyy-MM-dd HH:mm:ss');

      this.apiService.post('status/', {
        'file_type': this.fileType,
        'bank_code': this.uploadData.bank_code.bank_id,
        'project_id': this.uploadData.project.project_id,
        'datafor_date_time': date,
        'status': status,
        // "filters" : {
        //   //  "datafor_date_time":"2018-09-26 18:16:23",
        //   // "datafor_date_time": date,
        //   // "record_status":"Approval Pending"
        // }
      }).subscribe(data => {
          console.log('-------approved ');
       console.log(data); // handle event here
       const response: any = data;
       if (status == 'Approved') {
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
       this.alert.class = 'success';
       this.alert.text = ' Data Approved';
       }
       if (status == 'Rejected') {
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
       this.alert.class = 'success';
       this.alert.text = ' Data Rejected';
       }

    // $('#modalfilter').modal('hide');
    this.modalRef.hide();
      }, (error) => {
        console.log('error', error);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
       this.alert.class = 'danger';
       this.alert.text = ' Something Went Wrong';
      });
    }


    openPreviewModal() {
    $('#m_modal_6').modal('hide');
      console.log(this.previewModal);
      this.modalRef = this.modalService.show(this.previewModal, Object.assign({}, { class: 'preview-modal modal-lg' }));
    }


    // ------- end of preview modal ----------




    // --------------- drop down value --------------
    populateDropDown() {


  // --------- for upload dropdown
      this.apiService.post('bankinfo/', {
        'file_type': this.fileType,
      'routine_info': 'ROUTINE_DATA_INFO'
      }).subscribe(data => {
          console.log('------- dropdown value ');
         const response: any = data;
         console.log('-------dropdown', response.data);
       this.dropdownValue = response.data;
       console.log(this.dropdownValue); // handle event here
       this.getAllBankList();
      }, (error) => {
        console.log('error in dropdown value', error);

      });
      // --------------- end of upload dropdown

      // filter dropdown
      this.apiService.post('bankFeederCommonInfo/', {
        'file_type': this.fileType,
      'common_bankfeeder_info': 'common_bankfeeder_info'
      }).subscribe(data => {
          console.log('------- dropdown value ');
         const response: any = data;

         this.filterDropDownValue = response;
         console.log('------- filter dropdown ', this.filterDropDownValue);

      //  this.dropdownValue=[response.data];
      //  console.log(this.dropdownValue); // handle event here

      }, (error) => {
        console.log('error in filter dropdown value', error);

      });
      // end of filter dropdown
    }

    // --------------- end of drop down value --------------


    clicked(data) {
      console.log(data);
    }



    calculateTotalPage(rowcount, rowsize) {
      return Math.ceil(rowcount / rowsize);
        }

}
