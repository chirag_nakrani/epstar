import {
  Component,
  OnInit,
  TemplateRef,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';
import {
  DatePipe
} from '@angular/common';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl
} from '@angular/forms';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-indent-holiday',
  templateUrl: './indent-holiday.component.html',
  styleUrls: ['./indent-holiday.component.scss']
})
export class IndentHolidayComponent implements OnInit {

  @ViewChild('fileInput')
  myInputVariable: ElementRef;
  public uploadData: any;
  fileType = 'Indent_Holiday';
  uploadForm: FormGroup;
  modalRef: BsModalRef;
  datePipe;
  alert: any;
  page = new Page();
  selected = [];
  selectedids = [];
  submitted = false;
  temp = [];
  previewData: any;
  dropdownValue: any;
  public colList: any;
  viewColumnAll: any = [];
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  public editRow: any = {};
  public isMultiEdit: boolean;
  public permissionList: any = [];
  viewColumns: any = {};
  rows: any = [];
  tempRows: any = [];
  public filtersForAllView: any = {};
  viewColumn: any = {};
  selectedColumn: any = [];
  columnChange: any = [];
  public bankList: any = [];
  public projectbank: any;
  public bank_list: any;
  public feederList: any = [];
  public flag: boolean;
  public updateGroup: FormGroup;
  public now: Date = new Date();
  public indentData: any = {};

  constructor(private apiService: ApiService, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService, private formBuilder: FormBuilder) {
    this.datePipe = new DatePipe('en-US');
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.dropdownValue = [];
  }


  ngOnInit() {
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    this.uploadData.file;
    this.populateDropDown();
    this.getViewCol();
    this.flag = false;
    this.allDataActive(0);
    this.isMultiEdit = false;
    this.populateSelectColumn();
    this.uploadForm = this.formBuilder.group({
      file: ['',Validators.required],
    });
    this.updateGroup = this.formBuilder.group({
       is_indent_required: ['',Validators.required],
       withdrawal_type: ['',Validators.required],
    }); 
  }

  get f() { return this.updateGroup.controls; }
  // ------------------------------ upload end -----------------------------------
// ------------------start of code for all select column -----------

  populateSelectColumn() {
    this.viewColumnAll.filter( (col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
  this.viewColumn = this.viewColumnAll.filter((col) => {
    return this.selectedColumn.includes(col.prop);
  });
  // }
  console.log('col change', this.selectedColumn, this.viewColumn);
  }

// ---------------- end  of code for all select column -----------
// --------------------search function ---------------

  viewSearch(event) {
    console.log('updateing');
    const val = event.target.value.toLowerCase();
    const temp = this.tempRows.filter(function(d) {
      const site_code = d.site_code.toLowerCase().indexOf(val) !== -1;
      const atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
      const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
      return site_code || atm_id || search_project_id || !val;
    });

    this.rows = temp;
  }
//----------- end of serch function ---------

  updateViewAll() {
    console.log('updarte called');
    console.log('filter parameter', this.filtersForAllView);
    this.flag = true;
    this.allDataActive(0);
    $('#modalfilter').modal('hide');
  } 

  // --------------- drop down value ---------------------
  populateDropDown() {


    // --------- for upload dropdown----------------------
    this.apiService.post('bankinfo/', {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      console.log('-------dropdown', response.data);
      this.dropdownValue = response.data;
      console.log(this.dropdownValue); // handle event here
      //this.getBank(this.dropdownValue);
    }, (error) => {
      console.log('error in dropdown value', error);

    });
    // --------------- end of upload dropdown----------------
  }

  // --------------- end of drop down value -----------------

  resetFilter() {
    this.filtersForAllView = {};
    this.indentData = {};
  }

  verifyIndentCall(){
    $('#modalholiday').modal('show');
  }

  clicked(data) {
    console.log(data);
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  getBank(projectBankList: any){
    this.bankList.push({bank_name:'All'});
    console.log('project',projectBankList);
    console.log('project length',projectBankList.length);
    for (let  i = 0; i<= projectBankList.length-1;i++){
      this.projectbank = projectBankList[i].bank;
      for (let j = 0;j<=this.projectbank.length-1;j++){
        this.bankList.push({bank_name:this.projectbank[j].bank_id});

      }
    }
    this.bankList = this.bankList.reduce((acc, current) => {
      const x = acc.find(item => item.bank_name === current.bank_name);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
    console.log('bank',  this.bankList);
    this.bank_list = this.bankList;
  }

  generateIndentHoliday(){
    let params = {};

    if (this.indentData != undefined || this.indentData != {}) {
        params = {
          'indent_date' : this.datePipe.transform(this.indentData.indent_date, 'yyyy-MM-dd')
        }
      }
    this.apiService.post('indent/holiday/generate/', params).subscribe((response: any) => {
      this.allDataActive(0);
    this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_desc;
    }, (error: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_desc;
      } else {
        this.alert.text = error.status_desc;
        }
    });
  }

  //------------------------code for view file in modal for current and previous tab----------------------------------
  //common section
  viewMasterData(params: any, offSet: number) {
    this.page.pageNumber = offSet;
    this.apiService.post('indent/holiday/list/?page=' + (this.currentPage + offSet) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      this.tempRows=[...this.rows];
      this.rows.forEach( (x,index) => {
        this.isEdit[index] = false;
        this.colList.filter( x => x.datatype == 'date').forEach( y => {
          if(x[y.prop] != null){
            x[y.prop] = new Date(x[y.prop]);
          }
        })
      });
      console.log('-----------common', this.rows);
      this.page.totalElements = response.count;
      // // this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      // console.log('View Error', error.error);
    });
  }
  //end of common section

  sanitizeFilters(){
      for(let key in this.filtersForAllView){
        if(this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All'){
          delete this.filtersForAllView[key];
        }
      }
  }

  //to Display All data of active status
  allDataActive(offSet: number) {
  	this.sanitizeFilters();
    let params: any;
    if (this.flag != false){
    	params = {
	      'filters': this.filtersForAllView
	    };
    }else{
    	params = {
	      'filters': {
	      	"record_status":"Active"
	      }
	    };
    }
    
    this.viewMasterData(params, offSet);
  }
  onSelect({ selected }) {
    this.isMultiEdit = true;
    console.log('Select Event', this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

  }
  onFileChanged(event) {

    //const file = event.target.files[0]
    this.uploadData.file = event.target.files[0];
    console.log("******************",this.myInputVariable.nativeElement.files);
    this.myInputVariable.nativeElement.value = "";
    console.log(this.myInputVariable.nativeElement.files);
  }

  uploadDatafn() {
    // stop here if form is invalid
    if (this.uploadData.invalid) {
      return;
    }
    console.log('upload clicked----------', this.uploadData);
    this.submitted = true;

    const fileData = new FormData();
    //uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
    fileData.append('file', this.uploadData.file, this.uploadData.file.name);
    fileData.append('file_type', this.fileType);
    console.log('upload clicked formdata', fileData);
    this.apiService.uploadFile('referencedata/', fileData).subscribe(event => {
      console.log(event); // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);

        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        this.alert.text = response.body.status_text;
        this.alert.class = 'success';
        $('#m_modal_6').modal('hide');
        this.allDataActive(0);

      }
    }, (error => {
      $('#m_modal_6').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      this.alert.text = error.error.status_text;
      this.alert.class = 'danger';
    }));

    this.clearUploadFormData();

  }
  clearUploadFormData() {
    this.uploadForm.reset();
    this.uploadData.file =true;
    console.log("uploadData.fileuploadData.fileuploadData.file",this.uploadData.file)

  }
  // ------------------------------ upload end -----------------------------------

  // ------------------------- to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.columnIndentHoliday);
    this.columnConfig.columnIndentHoliday.forEach(x => {
        this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column, 'datatype': x.datatype, 'isEditable': x.isEditable});
    });
    this.viewColumnAll = this.colList;
    this.viewColumn = this.viewColumnAll;
    console.log('viewColumn', this.viewColumnAll);
  }
  // ------------------------- to display column data in modal and in All Data ----------------------
  // -------------code for edit all data section---------------------------
  // indivisual edit
  viewEdit(rowIndex: number, row: any) {
    this.rows = [];
    this.rows.push(row);
    this.isMultiEdit = true;
    this.isEdit[rowIndex] = true;
    this.selected.push(row);
  }

  // multiple Edit
  onMultipleEdit() {
    this.rows = [];
    this.rows = this.selected;
    this.rows = [...this.rows];

    for (let i = 0; i < this.rows.length; i++) {
      this.isEdit[i] = true;
    }
    console.log('edited row', this.rows);
  }

  //to submit edited data
  updateEdit(params: any) {
    this.apiService.post('indent/holiday/edit/', params).subscribe((response: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Edit';
      this.alert.class = 'success';
      this.alert.text = 'Records Updated successfully.';
      this.selected = [];
      this.allDataActive(0);
    }, (error) => {
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      const error_str: string = 'Unable to edit records.';
      this.alert.text = error_str;
      this.alert.class = 'danger';
      this.selected = [];
    });
  }

  editSubmit() {
  this.submitted = true;
  if (this.updateGroup.invalid) {
	  this.alert.isvisible = true;
	  this.alert.type = 'Error';
	  this.alert.class = 'Error';
	  if (this.f.is_indent_required.errors){
	  	this.alert.text = 'Please Select is_indent_required and then submit.';
	  }else{
	  	this.alert.text = 'Please Select withdrawal_type and then submit.';
	  }
	  
	}else{
		const params = {
      		'data': this.rows
	    };
	    this.updateEdit(params);
	}

  }

  getBankList() {
    this.bankList = [];
    this.bankList = this.dropdownValue.filter( x => x.project_id == this.filtersForAllView.project_id )[0].bank;
  }

  getFeeder() {
    this.feederList = [];
    this.feederList = this.bankList.filter( x => x.bank_id == this.filtersForAllView.bank_code)[0].feeder;
  }
  //getFeeder() {
//    this.feederList = [];
    //this.feederList = this.projectbank.filter( x => x.bank_id == this.filtersForAllView.bank_code)[0].feeder;
  //}
  // -------------End of code for edit all data section---------------------------

}
