import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndentHolidayComponent } from './indent-holiday.component';

describe('IndentHolidayComponent', () => {
  let component: IndentHolidayComponent;
  let fixture: ComponentFixture<IndentHolidayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndentHolidayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndentHolidayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
