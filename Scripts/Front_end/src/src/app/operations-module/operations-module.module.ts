import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { ActivityStatusComponent } from './activity-status/activity-status.component';
import { DailyloadingreportComponent } from './dailyloadingreport/dailyloadingreport.component';
import { IndentAdminComponent } from './indent-admin/indent-admin.component';
import { IndentreportsComponent } from './indentreports/indentreports.component';
import { IndentsComponent } from './indents/indents.component';
import { IndentsrevisionsComponent } from './indentsrevisions/indentsrevisions.component';
import { LoadingrecommendationComponent } from './loadingrecommendation/loadingrecommendation.component';
import { RecommendationsheetComponent } from './recommendationsheet/recommendationsheet.component';
import { SubmitRevisionComponent } from './indents/submit-revision/submit-revision.component';
import { IndentSummaryReportComponent } from './indent-summary-report/indent-summary-report.component';

import { CommonImportsModule } from '../common/common-imports.module';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';
import { VaultingBalanceComponent } from './vaulting-balance/vaulting-balance.component';

import { DiversionStatusComponent } from './diversion-status/diversion-status.component';
import { LoadingStatusComponent } from './loading-status/loading-status.component';
import { WithdrawalStatusComponent } from './withdrawal-status/withdrawal-status.component';
import { IndentHolidayComponent } from './indent-holiday/indent-holiday.component';

const routes: Routes = [
  { path: 'activitystatus', component: ActivityStatusComponent,
     canActivate: [AuthCheckService] },
     { path: 'vaultingofbalance', component: VaultingBalanceComponent,
     canActivate: [AuthCheckService] },
  { path: 'dailyloadingreport', component: DailyloadingreportComponent,
     canActivate: [AuthCheckService] },
  { path: 'indentadmin', component: IndentAdminComponent,
     canActivate: [AuthCheckService] },
  { path: 'indents', component: IndentsComponent,
     canActivate: [AuthCheckService] },
  { path: 'indentreports', component: IndentreportsComponent,
     canActivate: [AuthCheckService] },
  { path: 'indentsrevisions', component: IndentsrevisionsComponent,
     canActivate: [AuthCheckService] },
  { path: 'loadingrecommendation', component: LoadingrecommendationComponent,
     canActivate: [AuthCheckService] },
  { path: 'recommendationsheet', component: RecommendationsheetComponent,
     canActivate: [AuthCheckService] },
  { path: 'indentsummary', component: IndentSummaryReportComponent,
    canActivate: [AuthCheckService] },
  { path: 'diversionstatus', component: DiversionStatusComponent,
    canActivate: [AuthCheckService] },
  { path: 'loadingstatus', component: LoadingStatusComponent,
    canActivate: [AuthCheckService] },
  { path: 'withdrawalstatus', component: WithdrawalStatusComponent,
    canActivate: [AuthCheckService] },
  { path: 'indent_holiday', component: IndentHolidayComponent,
    canActivate: [AuthCheckService] },
  ];

@NgModule({
  declarations: [
    ActivityStatusComponent,
    DailyloadingreportComponent,
    IndentAdminComponent,
    IndentreportsComponent,
    IndentsComponent,
    IndentsrevisionsComponent,
    LoadingrecommendationComponent,
    RecommendationsheetComponent,
    SubmitRevisionComponent,
    IndentSummaryReportComponent,
    VaultingBalanceComponent,
    DiversionStatusComponent,
    LoadingStatusComponent,
    WithdrawalStatusComponent,
    IndentHolidayComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class OperationsModuleModule { }
