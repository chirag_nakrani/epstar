import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VaultingBalanceComponent } from './vaulting-balance.component';

describe('VaultingBalanceComponent', () => {
  let component: VaultingBalanceComponent;
  let fixture: ComponentFixture<VaultingBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VaultingBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaultingBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
