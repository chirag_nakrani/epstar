import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';



declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-vaulting-balance',
  templateUrl: './vaulting-balance.component.html',
  styleUrls: ['./vaulting-balance.component.scss']
})
export class VaultingBalanceComponent implements OnInit {
  @ViewChild('fileInput')
  myInputVariable: ElementRef;
  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  fileType = 'VB';
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  selected = [];
  selectedids = [];
  finalAtmArrey=[];
  errorcondition:boolean
  finalAmount=0;
  previewPage = new Page();
  public uploadData: any;
  uploadForm: FormGroup ;
  submitted = false;
  totalarrey=0;
  temp = [];
  objTmp:any;
  finalArrey=[];
  public atmId: any = [];
  previewData: any;
  datePipe;
  public atm_list: any;
  showerr=false;
  public addField: any = [{}];
  totalAmt=0;
  amt1=0;
  amt2=0;
  amt3=0;
  amt4=0;
  public bankList: any = [];
  public atmListPdf: any = [];
  public config: any;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  diRows: any;
  viewColumnAll: any = [];
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  submitbuttonval:boolean;
  public editRow: any = {};
  public isMultiEdit: boolean;
  public permissionList: any = [];
  public statusList: Array<string>;
  viewColumns: any = {};
  rows: any = [];
  tempRows: any = [];
  tmpshowerr:any;
  filtersForAllView: any = {};
  viewColumn: any = {};
  today:any;
  selectedColumn: any = [];
  columnChange: any = [];
  public date_range: Array<Date> = [];
  public bankFilterDropDown: any = [];
  

  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
    this.datePipe = new DatePipe('en-US');
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.dropdownValue = [];
    this.filterDropDownValue = {};
    this.filtersForAllView.record_status = 'Active';
    this.statusList = ['All', 'Active', 'Approval Pending', 'History', 'Rejected', 'Uploaded'];
  }


  ngOnInit() {
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    console.log("$$$$$$$$$$$$$$$$$$$$$",this.permissionList )
    this.populateDropDown();
    this.setDataInformationCurrent();
    this.uploadData.date = new Date();
    this.uploadData.time = '12:00';
    this.getViewCol();
    this.submitbuttonval=true;

    this.allDataActive(0);
    this.today=new Date();
    this.isMultiEdit = false;
    this.uploadForm = this.formBuilder.group({
      indent_order_number: ['', Validators.required],
      order_date: [ this.uploadData.order_date],
      date:[this.uploadData.date],
      comments:[ this.uploadData.comments],
      reason_for_vaulting:[ this.uploadData.reason_for_vaulting],
      cra: [ this.uploadData.cra],
      atm:[ this.uploadData.atm],
      bank_code: [  this.uploadData.bank_code],
      feeder_branch_code: [  this.uploadData.feeder_branch_code],
      original_atm_id: [  this.uploadData.original_atm_id],
      temporary_atm_id: [  this.uploadData.temporary_atm_id],
      total_diversion_amount: [  this.uploadData.total_diversion_amount],
      denomination_100: [  this.uploadData.denomination_100],
      denomination_200: [  this.uploadData.denomination_200],
      denomination_500: [  this.uploadData.denomination_500],
      denomination_2000: [  this.uploadData.denomination_2000],
      reasons: [  this.uploadData.reasons],
      region: [  this.uploadData.region],
    });

    this.populateSelectColumn();
    this.config = {
      displayKey: 'atm_name', //if objects array passed which key to be displayed defaults to description
      search: true, //true/false for the search functionlity defaults to false,
      //height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
      placeholder: 'Select', // text to be displayed when no item is selected defaults to Select,
      noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
      searchPlaceholder: 'Search', // label thats displayed in search input,
      default: 'All',
      searchOnKey: 'atm_name' // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
    };
  }
  // ----------------------- upload file ------------------------------------
  // convenience getter for easy access to form fields
  get f() {
    return this.uploadForm.controls;
  }
  openTimeSelector() {
    console.log('time picker opened');
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.uploadData.time = time;
      console.log('time picker closed');

    });
  }
  againselect(index,atmIdValueToRemove){
    this.addField[index].tmpshowerr=false
    for(var i=0;i<this.addField.length-1;i++){
   
      if(this.addField[i].orgAtm){
        if(this.addField[i].orgAtm==atmIdValueToRemove && this.addField.length>1){
          this.addField[index].tmpshowerr=true
          this.addField[index].showErr5=false
        }
      }
      }
  }
  AddFiled(index,atmIdValueToRemove) {

              if( this.addField[index].tmpshowerr!=true && this.addField.length!=this.atm_list.length && atmIdValueToRemove!=undefined){
                this.addField.push({});

              }
    // var index = this.atm_list.indexOf(atmIdValueToRemove);
    // if (index > -1) {
    //   this.atm_list.splice(index, 1);
    // }
    }
    RemoveField(index,atmIdValueToAdd) {
      this.addField[index].tmpshowerr=false
     if(atmIdValueToAdd!=undefined){
      for(var i=0;i<this.addField.length;i++){
        if(this.addField[i].orgAtm){
          this.addField[i].tmpshowerr=false;
        }
            }
     }
      this.finalAmount=0
    
      this.addField.splice(index,1);
      for(var i=0;i<this.addField.length;i++){
        console.log("this.addField[i].totalAmt",this.addField[i].totalAmt)
        if(this.addField[i].totalAmt){
          this.finalAmount=(+this.addField[i].totalAmt)+(+this.finalAmount)
        }
      }
      }
  populateDropdown(data1) {
    let params = 
    {
      'indent_order_number' : data1.indent_order_number
    }

    this.apiService.post('Vbdropdown/', params).subscribe((response: any) => {  
        if(response.data){
          this.uploadData.feeder_branch_code=response.data.feeder_branch
          this.uploadData.bank_code=response.data.bank
          this.uploadData.cra=response.data.cra
          this.atm_list=response.data.atm_id;
          for (let j = 0;j<=response.data.atm_id.length-1;j++){
            this.atmListPdf.push({atm_name:response.data.atm_id[j],showvalue:true});
    
          }
          this.atm_list = this.atmListPdf;
          this.uploadData.indent_order_number=data1.indent_order_number
        }
        
    }, (error) => {
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      const error_str: string = error.error['status_text'];
      this.alert.text =  error_str;
      this.alert.class = 'danger';
    });


    }
    addTotalAmount(i: number){
      
                if(this.rows[i].amount_100==undefined){
                  this.rows[i].amount_100=0;
                }else{
                  this.rows[i].amount_100=(+ this.rows[i].amount_100);
                }
                if(this.rows[i].amount_200==undefined){
                  this.rows[i].amount_200=0;
                }else{
                  this.rows[i].amount_200=(+ this.rows[i].amount_200);

                }
                if(this.rows[i].amount_500==undefined){
                  this.rows[i].amount_500=0;
                }else{
                  this.rows[i].amount_500=(+ this.rows[i].amount_500);

                }
                if(this.rows[i].amount_2000==undefined){
                  this.rows[i].amount_2000=0;
                }else{
                  this.rows[i].amount_2000=(+ this.rows[i].amount_2000);

                }
                this.rows[i].total_amount=((+ this.rows[i].amount_100)+(+ this.rows[i].amount_200)+(+ this.rows[i].amount_500)+(+ this.rows[i].amount_2000));
                
 
    }


    addTotalAmoutUpload(i) {
      console.log("Inside Total Amount",i,this.addField[i].amt1,this.addField[i].amt2,this.addField[i].amt3,this.addField[i].amt4)
              // if(this.addField[i].amt1==undefined){
              //   this.addField[i].amt1=0;
              // }
              // if(this.addField[i].amt2==undefined){
              //   this.addField[i].amt2=0;
              // }
              // if(this.addField[i].amt3==undefined){
              //   this.addField[i].amt3=0;
              // }
              // if(this.addField[i].amt4==undefined){
              //   this.addField[i].amt4=0;
              // }
    this.addField[i].totalAmt=((+ this.addField[i].amt1)+(+ this.addField[i].amt2)+(+ this.addField[i].amt3)+(+ this.addField[i].amt4));
    if(isNaN(this.addField[i].totalAmt)){
      this.addField[i].totalAmt=0
    }
              this.finalAmount=0;
                for(var j=0;j<this.addField.length;j++){
                  if(this.addField[j].totalAmt){
                      this.finalAmount=this.addField[j].totalAmt+this.finalAmount;
                  }

                }
 
     }
     uploadDatafn() {
      this.finalArrey=[];
      for(var b=0;b<this.addField.length;b++){
      
        if(this.addField[b].totalAmt!=undefined){
          this.addField[b].showErr1=false
          this.addField[b].showErr2=false
          this.addField[b].showErr3=false
          this.addField[b].showErr4=false
          this.addField[b].showErr5=false
          // this.addField[b].errorcondition=false;

        }
        if(this.addField[b].amt1==undefined || this.addField[b].amt1==''){
          this.addField[b].showErr1=true
          // this.addField[b].errorcondition=true;
        }
        if(this.addField[b].amt2==undefined || this.addField[b].amt2==''){
          this.addField[b].showErr2=true
          // this.addField[b].errorcondition=true;

        }
        if(this.addField[b].amt3==undefined || this.addField[b].amt3==''){
          this.addField[b].showErr3=true
          // this.addField[b].errorcondition=true;

        }
        if(this.addField[b].amt4==undefined || this.addField[b].amt4==''){
          this.addField[b].showErr4=true
          // this.addField[b].errorcondition=true;

        }
        if(this.addField[b].orgAtm==undefined || this.addField[b].orgAtm==''){
          this.addField[b].showErr5=true
          this.addField[b].tmpshowerr=false
          // this.addField[b].errorcondition=true;

        }
      
        if(this.addField[b].amt1==undefined ||this.addField[b].amt1==''||this.addField[b].amt2==undefined ||this.addField[b].amt2==''||this.addField[b].amt3==undefined||this.addField[b].amt3==''||this.addField[b].amt4==undefined||this.addField[b].amt4==''||this.addField[b].amt4==undefined||this.addField[b].amt4==''||this.addField[b].amt4==undefined||this.addField[b].amt4==''||this.addField[b].orgAtm==undefined||this.addField[b].orgAtm==''){
        
        }else{         
          if(!isNaN(this.addField[b].totalAmt) && this.addField[b].tmpshowerr==false && this.addField[b].showErr5==false){
            this.objTmp={}
            this.objTmp.amount_100=(+this.addField[b].amt1);
            this.objTmp.amount_200=(+this.addField[b].amt2);
            this.objTmp.amount_500=(+this.addField[b].amt3);
            this.objTmp.amount_2000=(+this.addField[b].amt4);
            this.objTmp.total_amount=(+this.addField[b].totalAmt);
            this.objTmp.atm_id=this.addField[b].orgAtm;
            this.objTmp.bank_code=this.uploadData.bank_code
            this.objTmp.comments=this.uploadData.comments
            this.objTmp.cra=this.uploadData.cra
            this.objTmp.date=this.uploadData.date
            this.objTmp.feeder_branch_code= this.uploadData.feeder_branch_code
              this.objTmp.indent_order_number= this.uploadData.indent_order_number
            this.objTmp.reason_for_vaulting=this.uploadData.reason_for_vaulting
            this.objTmp.vaulting_date=this.uploadData.vaulting_date
               this.finalArrey.push(this.objTmp)
          }
        }
      }
 

    if(this.finalArrey.length == this.addField.length){
      var objTosend={
        file_type:"VB",
        payload:this.finalArrey
      }
      this.apiService.post('operations/vaultingbalance/', objTosend).subscribe((response: any) => {  
        this.alert.isvisible = true;
        $('#m_modal_6').modal('hide');
  
        this.setDataInformationCurrent();
        this.alert.type = 'Upload';
        this.alert.text = ' ' + response.data;
        this.alert.class = 'success';
        // if(response.data){
        //   this.uploadData=response.data
        // }
        
    }, (error) => {
      $('#m_modal_6').modal('hide');
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      const error_str: string = error.error.data;
      this.alert.text = error_str;
      this.alert.class = 'danger';
    });
    this.clearUploadFormData();
    }

    }
  clearUploadFormData() {
    this.uploadData = {};

this.finalAmount=0;
this.atm_list=[];
this.atmId = [];
this.atmListPdf=[];
this.addField=[{}];
  }
  // ------------------------------ upload end -----------------------------------
// ------------------start of code for all select column -----------

  populateSelectColumn() {
    this.viewColumnAll.filter( (col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
  this.viewColumn = this.viewColumnAll.filter((col) => {
    return this.selectedColumn.includes(col.prop);
  });
  // }
  console.log('col change', this.selectedColumn, this.viewColumn);
  }
// ---------------- end  of code for all select column -----------
// --------------------search function ---------------

  viewSearch(event) {
    console.log('updateing');
    const val = event.target.value.toLowerCase();
    const temp = this.tempRows.filter(function(d) {
      const cra = d.cra.toLowerCase().indexOf(val) !== -1;
      const atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
      const search_indent_order_id = d.indent_order_id.toLowerCase().indexOf(val) !== -1;
      return cra || atm_id || search_indent_order_id || !val;
    });

    this.rows = temp;
  }
//----------- end of serch function ---------

  updateViewAll() {
 
    this.allDataActive(0);
    $('#modalfilter').modal('hide');
  }

  setPage(pageInfo) {
    // console.log("page clicked");
    this.page.pageNumber = pageInfo.offset;
    this.apiService.post('viewmaster/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size, {
      'menu': this.fileType,
      'filter_data': this.filtersForAllView
    }).subscribe(data => {
      const response: any = data;
      this.rows = response.results;
      this.tempRows = response.results;
      this.page.totalElements = response.count;
    });
  }
  // ---------end of  view all data ----------------------

  // --------------- drop down value ---------------------
  populateDropDown() {


    // --------- for upload dropdown----------------------
    this.apiService.post('bankinfo/', {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      console.log('-------dropdown', response.data);
      this.dropdownValue = response.data;
      console.log(this.dropdownValue); // handle event here
      this.getAllBankList();
    }, (error) => {
      console.log('error in dropdown value', error);

    });
    // --------------- end of upload dropdown----------------

    // filter dropdown
    this.apiService.post('bankFeederCommonInfo/', {
      'file_type': 'CBR',
      'common_bankfeeder_info': 'common_bankfeeder_info'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      this.filterDropDownValue = response;
      console.log('------- filter dropdown ', this.filterDropDownValue);

      //  this.dropdownValue=[response.data];
      //  console.log(this.dropdownValue); // handle event here

    }, (error) => {
      console.log('error in filter dropdown value', error);

    });
    // end of filter dropdown
  }

  // --------------- end of drop down value -----------------

  resetFilter() {
    this.filtersForAllView = {};
    this.filtersForAllView.record_status = 'Active';
    this.date_range = [];
  }


  clicked(data) {
    console.log(data);
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  getAllBankList() {
    this.bankFilterDropDown = [];
    this.dropdownValue.forEach( x => {
      x.bank.forEach( y => {
        this.bankFilterDropDown.push(y);
      });
    });
  }
  // ------------------------- code for data information ----------------------
  setDataInformationCurrent() {
    const params = {
      'menu': this.fileType,
      'record_status': ['Approval Pending']
    };

    this.getDataInfo(params);
  }
  setDataInformationPrevious() {
    const params = {
      'menu': this.fileType,
      'record_status': ['Active', 'Rejected']
    };

    this.getDataInfo(params);
  }
  getDataInfo(params: any) {
    this.apiService.post('getmasterdata/', params).subscribe((data: any) => {
      const tempRow = data.data;
      this.diRows = tempRow;
      this.diRows = [...this.diRows];
      console.log('Active data', this.diRows);
    }, (error) => {
      console.log('Data Info Error', error.error.status_text);
    });
  }

  // -------------------------End of code for data information ----------------------

  //------------------------code for view file in modal for current and previous tab----------------------------------
  //common section
  viewMasterData(params: any, offSet: number) {
    this.page.pageNumber = offSet;
    this.apiService.post('viewmaster/?page=' + (this.currentPage + offSet) + '&page_size=' + this.page.size, params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      this.tempRows=[...this.rows];
      this.rows.forEach( (x,index) => {
        this.isEdit[index] = false;
        this.colList.filter( x => x.datatype == 'date').forEach( y => {
          if(x[y.prop] != null){
            x[y.prop] = new Date(x[y.prop]);
          }
        })
      });
      console.log('-----------common', this.rows);
      this.page.totalElements = response.count;
      // // this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      // console.log('View Error', error.error);
    });
  }
  getFileData(offSet: number, status: string) {
    const params = {
      'menu': this.fileType,
      'filter_data': {
        'created_reference_id': this.previewData.created_reference_id,
        'record_status': status
      }
    };
    this.viewMasterData(params, offSet);
  }
  //end of common section
  //to open view file in current tab
  closethismodal(){
    this.selectedids=[];
    this.selected=[];
  }
  viewFile(rowData: any) {
    this.closethismodal();
    // this.selectedids=[];
    // this.selected=[];
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0, rowData.record_status);
    this.modalRef = this.modalService.show(this.previewModalcurrent, Object.assign({}, { class: 'preview-modal modal-lg'},{backdrop:"static"}));
  }
  //to open view file in previous tab
  viewFilePrevious(rowData: any) {
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0, rowData.record_status);
    this.modalRef = this.modalService.show(this.previewModalprevious, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }

  //to Display All data of active status
  allDataActive(offSet: number) {

    if (this.date_range) {
      this.date_range = this.date_range.map( x => {
        return this.datePipe.transform(new Date(x), 'yyyy-MM-dd');
      });
    }
    let params: any;
    for (let key in this.filtersForAllView) {
      if (this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All') {
        delete this.filtersForAllView[key];
      }
    }
    if ( this.date_range.length > 0 ) {
      params = {
        'menu': this.fileType,
        'date_range': this.date_range,
        'filter_data': this.filtersForAllView
      };
    } else {
      params = {
        'menu': this.fileType,
        'filter_data': this.filtersForAllView
      };
    }
    this.viewMasterData(params, offSet);
  }
  //---------------------------------------code for view file in modal for current and previous tab- --------------------------------------
  //------------------code for Modal which is approved or rejected indivisual file -----------------------------
  onSelect({ selected },reqfrom) {
    this.isMultiEdit = true;
    console.log('Select Event', selected, this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    
      if(this.selected.length<1 && reqfrom=='Active'){
        this.allDataActive(0);
      }
    
  }
  getApprovedData(params: any) {
    this.apiService.post('ApproveRejectAll/', params).subscribe((response: any) => {
      this.allDataActive(0);
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_desc;
      console.log('-----------', this.rows);
    }, (error:any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_desc;
      } else {
        this.alert.text = error.statusText;
      }
    });
    this.closethismodal();

  }
  //to approve indivisual row
  approveIndivisualFile() {
    for (let i = 0; i <= this.selected.length - 1; i++) {
      this.selectedids.push(this.selected[i].id);
    }
    const params = {
      'menu': this.fileType,
      'id': this.selectedids.toString(),
      'status': 'Active',
      'created_reference_id': this.previewData.created_reference_id
    };
    
    this.getApprovedData(params);
  }
  //to reject indivisual row
  rejectIndivisualFile() {
    for (let i = 0; i <= this.selected.length - 1; i++) {
      this.selectedids.push(this.selected[i].id);
    }
    const params = {
      'menu': this.fileType,
      'id': this.selectedids.toString(),
      'status': 'Rejected',
      'created_reference_id': this.previewData.created_reference_id
    };
    this.getApprovedData(params);
  }
  //to approve complete file
  approveFile(row: any) {
    const params = {
      'menu': this.fileType,
      'id': '0',
      'created_reference_id': row.created_reference_id,
      'status': 'Active'
    };
    this.getApprovedData(params);
  }
  //to reject complete file
  rejectFile(row: any) {
    const params = {
      'menu': this.fileType,
      'id': '0',
      'status': 'Rejected',
      'created_reference_id': row.created_reference_id
    };
    this.getApprovedData(params);
  }
  //----------------end of code for Modal which is approved or rejected for--------------
  // ------------------------- to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.columnVaultingStatus);
    this.columnConfig.columnVaultingStatus.forEach(x => {

	this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column,'datatype': x.datatype,'isEditable': x.isEditable});
    });
    this.viewColumnAll = this.colList;
  }
  // ------------------------- to display column data in modal and in All Data ----------------------
  // -------------code for edit all data section---------------------------
  // indivisual edit
  viewEdit(rowIndex: number, row: any) {
    this.editRow = row;
    this.isEdit[rowIndex] = 'true';
  }

  // multiple Edit
  onMultipleEdit() {

    if(this.selected.length>0){
      this.submitbuttonval=false;

      this.rows = this.selected;
      this.rows = [...this.rows];
  
      for (let i = 0; i < this.rows.length; i++) {
        this.isEdit[i] = true;
      }
      console.log('edited row', this.rows);
    }
  }

  //to submit edited data
  updateEdit(params: any) {
    for(var i=0;i<params.update.length;i++){
      params.update[i].file_type="VB"
    }
    this.apiService.post('updatemaster/', params).subscribe((response: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Edit';
      this.alert.class = 'success';
      this.alert.text = response.data;
      this.setDataInformationCurrent();
      this.selected = [];
      this.allDataActive(0);
    }, (error) => {
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      const error_str: string = error.error.data;
      this.alert.text = error_str;
      this.alert.class = 'danger';
      this.selected = [];
    });
  }

  editSubmit() {
    this.submitbuttonval=true;

    const params = {
      'file_type': this.fileType,
      'update': this.selected
    };
    this.updateEdit(params);

  }
  // -------------End of code for edit all data section---------------------------

}
