import { Component, OnInit , TemplateRef } from '@angular/core';
import {ViewChild, ElementRef} from '@angular/core';

import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Page} from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';
import { DomSanitizer } from '@angular/platform-browser';

declare var jquery: any;   // not required
declare var $: any;   // not required


@Component({
  selector: 'app-withdrawal-status',
  templateUrl: './withdrawal-status.component.html',
  styleUrls: ['./withdrawal-status.component.scss']
})
export class WithdrawalStatusComponent implements OnInit {
  @ViewChild('fileInput')
  myInputVariable: ElementRef;

  @ViewChild('previewModal') previewModal: ElementRef;

  fileType = 'DISPENSE';
    modalRef: BsModalRef;
  alert: any;
  blobUrl: string;
  blobUrl1:any;
  showerr=false
  page = new Page();
  previewPage = new Page();
  dataTosubmit: Object = {};
  public uploadData: any;
  public editData: any;
  fileuploadcalled=true;
  uploadForm: FormGroup;
  editForm:FormGroup;
  submitted = false;
  temp = [];
  datePipe;
  blob: Blob;
  abc:{};
  recordToDelete:any;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: ColumnConfig;
  public permissionList: any = [];
  public bankDropDown: any = [];
  public feederRegionDropDown: any = [];
  fileName: string;
  public date_range: any = [];
  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];
  imageTodisplay:any;
  public statusList = ['All', 'Active', 'Deleted'];
  public filterData: any = {};
  viewColumnAll: any = [
   
    {
      name: 'Bank Code',
      prop: 'bank_code',
      datatype: 'string'
    },
    {
      name: 'Cra',
      prop: 'cra',
      datatype: 'string'
    },
    
    {
      name: 'View Slip',
      prop: 'viewslip',
      datatype: 'string'
    },
    {
      name: 'Denomination 100',
      prop: 'denomination_100',
      datatype: 'string'
    },
    {
      name: 'Denomination 200',
      prop: 'denomination_200',
      datatype: 'string'
    },
    {
      name: 'Denomination 500',
      prop: 'denomination_500',
      datatype: 'string'
    },
    {
      name: 'Denomination 2000',
      prop: 'denomination_2000',
      datatype: 'string'
    },
    {
      name: 'Feeder Branch',
      prop: 'feeder_branch',
      datatype: 'string'
    },
    {
      name: 'Project Id',
      prop: 'project_id',
      datatype: 'string'
    },
    {
      name: 'Record Status',
      prop: 'record_status',
      datatype: 'string'
    },
    {
      name: 'Remarks',
      prop: 'remarks',
      datatype: 'string'
    },
    {
      name: 'Total Withdrawal Amount',
      prop: 'total_withdrawal_amount',
      datatype: 'string'
    },
    {
      name: 'Delete',
      prop: 'delete',
      datatype: 'string'
    },
    {
      name: 'Edit',
      prop: 'Edit',
      datatype: 'string'
    },
   
  ];
  //  --------------code for select column -------------
  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];
    constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService, private domSanitize: DomSanitizer) {
      this.datePipe = new DatePipe('en-US');
      this.uploadData = {};
      this.editData = {};
      this.alert = {};
      this.alert.isvisible = false;
      this.page.pageNumber = 0;
      this.page.size = 20;

      this.previewPage.pageNumber = 0;
      this.previewPage.size = 20;
      this.dropdownValue = [];
      this.filterDropDownValue = {};
     }


    ngOnInit() {
      if(this.sessionStorageService.get('permissionList') != null)
      this.permissionList = this.sessionStorageService.get('permissionList');
      this.uploadData.date = new Date();
      this.uploadData.time = '12:00';
      this.uploadForm = this.formBuilder.group({
        indent_order_number: ['', Validators.required]
      });
      this.editForm = this.formBuilder.group({
        indent_order_number: ['', Validators.required]
      });
      

      this.updateViewAll();
      this.populateSelectColumn();
    }

    deleteRecord(deleteRecord){
      this.recordToDelete=deleteRecord.indent_short_code
    }
    deleteData(){
      this.apiService.post('withdrawal/delete/', {indent_short_code:this.recordToDelete}).subscribe((response: any) => {
        this.uploadData=response
        this.alert.isvisible = true;
        this.alert.type = 'Success';
        this.alert.class = 'success';
        this.alert.text = 'Data Deleted Successfully';
        this.updateViewAll();
         }, (error: any) => {
            console.log('Error', error.error);
            this.alert.isvisible = true;
            this.alert.class = 'danger';
            this.alert.type = 'Error!';
            this.alert.text = 'No record found to delete';
         });
         
    }
    getImage(dataToShowImage,reqfrom){
      console.log("$$$$$$$$$$$$$$$$$",dataToShowImage)
      this.apiService.downloadFile('withdrawal/create/',{"indent_short_code":dataToShowImage.indent_short_code}).subscribe((response: any) => {
        this.blob = new Blob([(response)], { type: 'image/png' });
        this.showIndentPdf();
        if(reqfrom=='view'){
          $('#imagemodal').modal('show');
        }
        
       this.imageTodisplay=response;
         }, (error: any) => {
          console.log("errrrrrrrrrrrrrrrrrr^",error)
          this.blobUrl1='';
          this.alert.isvisible = true;
            this.alert.type = 'Error';
            const error_str: string = 'Withdrawal Slip Not Available. Try again!!!';
            this.alert.text =  error_str;
            this.alert.class = 'danger';
         })
    }
    showIndentPdf() {
      this.blobUrl = URL.createObjectURL(this.blob);
      console.log(" this.blobUrl this.blobUrl", this.blobUrl)
      this.blobUrl1 = this.domSanitize.bypassSecurityTrustUrl(this.blobUrl);
      console.log(" this.blobUrl this.blobUrl",this.blobUrl1)

    }








    // ----------------------- upload file ------------------------------------

  // convenience getter for easy access to form fields
  get f() { return this.uploadForm.controls; }


  clickMeToClear(){
    this.uploadData = {};
  }

  onFileChanged(event) {
this.fileuploadcalled=false
    //const file = event.target.files[0]
   console.log("@@@@@@@@@@@@@@",event.target.files[0])
    this.uploadData.file = event.target.files[0];
    console.log("@@@@@@@@@@@@@@",this.uploadData.file)
  //  this.blobUrl1='';
    console.log("******************",this.myInputVariable.nativeElement.files);
    this.myInputVariable.nativeElement.value = "";
    console.log(this.myInputVariable.nativeElement.files);
  }

  populateDropdown(data1) {
    const params = {
      'indent_order_no' : data1,
      'filters': {
        'record_status': 'Active'
      }
    };
    console.log("paramsparamsparamsparams",params)
    this.apiService.post('indent/detail/', params).subscribe((response: any) => {
    console.log("After APi cAlled^^^^^^^^^^^^",response)
    this.uploadData=response
   
     }, (error: any) => {
        console.log('Error', error.error);
        this.alert.isvisible = true;
        this.alert.class = 'danger';
        this.alert.type = 'Error!';
        this.alert.text = 'Indent details not available please try after sometime.';
     });
  }

  addTotalAmountupload() {
    this.uploadData.total_bank_withdrawal_amount=((+this.uploadData.total_bank_withdrawal_amount_100)+
    (+this.uploadData.total_bank_withdrawal_amount_200)+
    (+this.uploadData.total_bank_withdrawal_amount_500)+
    (+this.uploadData.total_bank_withdrawal_amount_2000))
   }
   addTotalAmountEdit() {
    this.editData.total_withdrawal_amount=((+this.editData.denomination_100)+
    (+this.editData.denomination_200)+
    (+this.editData.denomination_500)+
    (+this.editData.denomination_2000))
   }

   editRecord(editDatavalue){
     this.editData=editDatavalue;
    this.getImage(this.editData,'edit')
  }

 


   editDataFn(){
    var fileData = new FormData();
    if(this.uploadData.file!=undefined){
      fileData.append('file', this.uploadData.file);
    }
  fileData.append('bank_code', this.editData.bank_code);
  fileData.append('indent_order_number', this.editData.indent_order_number);
  fileData.append('indent_short_code', this.editData.indent_short_code);
  fileData.append('project_id', this.editData.project_id);
  fileData.append('feeder_branch', this.editData.feeder_branch);
  fileData.append('cra', this.editData.cra);
  fileData.append('total_withdrawal_amount', this.editData.total_withdrawal_amount);
  fileData.append('denomination_100',this.editData.denomination_100);
  fileData.append('denomination_200', this.editData.denomination_200);
  fileData.append('denomination_500',this.editData.denomination_500);
  fileData.append('denomination_2000',this.editData.denomination_2000);
  fileData.append('remarks',this.editData.remarks);
this.uploadDatafn(fileData,'edit')
   }

  uploadDatafn(data,reqfrom) {
 if(reqfrom=='add'){
  if (this.uploadForm.invalid) {
        this.showerr=true;
        return;
        }
      this.showerr=false;
      var fileData = new FormData();
        fileData.append('file', this.uploadData.file);
      fileData.append('bank_code', this.uploadData.bank);
      fileData.append('indent_order_number', this.uploadData.indent_order_number);
      fileData.append('indent_short_code', this.uploadData.indent_short_code);
      fileData.append('project_id', this.uploadData.project_id);
      fileData.append('feeder_branch', this.uploadData.feeder_branch);
      fileData.append('cra', this.uploadData.cra);
      fileData.append('indent_order_number', this.uploadData.indent_order_number);
      fileData.append('total_withdrawal_amount', this.uploadData.total_bank_withdrawal_amount);
      fileData.append('denomination_100',this.uploadData.total_bank_withdrawal_amount_100);
      fileData.append('denomination_200', this.uploadData.total_bank_withdrawal_amount_200);
      fileData.append('denomination_500',this.uploadData.total_bank_withdrawal_amount_500);
      fileData.append('denomination_2000',this.uploadData.total_bank_withdrawal_amount_2000);
      fileData.append('remarks',this.uploadData.remarks);
      this.submitted = true;
    }else{
      fileData=data;
    }
     this.apiService.uploadFile('withdrawal/create/', fileData).subscribe((response:any)=> {
       $('#m_modal_6').modal('hide');
       this.alert.isvisible = true;
       this.alert.type = 'Upload';
       this.alert.text = ' Data Uploaded Successfully'  ;
       this.alert.class = 'success';
       this.updateViewAll();
      }, (error: any) => {
         console.log('Error', error.error);
         this.alert.isvisible = true;
         this.alert.type = 'Failure';
         this.alert.class = 'danger';
         this.alert.text = "Failed To upload";

      });
      this.clearUploadFormData();
  }

  // ------------------------------ upload end -----------------------------------

  clearUploadFormData() {
    this.fileName = '';
    this.blobUrl1='';
    this.uploadData.bank_id = [];
    this.uploadData.project_id = [];
    this.uploadData.region = [];
    this.uploadData.file = [];
    this.uploadData={}
    this.fileuploadcalled=true;
  }

  populateSelectColumn() {
    this.viewColumnAll.filter( (col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
  this.viewColumn = this.viewColumnAll.filter( (col) => {
    return this.selectedColumn.includes(col.prop);
  });
  // }
  console.log('col change', this.selectedColumn, this.viewColumn);
  }

  viewSearch(event) {
          const val = event.target.value.toLowerCase();

          const temp = this.tempRows.filter(function(d) {
              const search_indent_order_number = d.indent_order_number.toLowerCase().indexOf(val) !== -1;
              const search_bank = d.bank_code.toLowerCase().indexOf(val) !== -1;
              const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
             return search_indent_order_number || search_bank || search_project_id || !val ;
             });

          this.rows = temp;
  }
// -----------------end of code for search   -----------------

  updateViewAll() {
    this.setPage({ offset: 0 });
    $('#modalfilter').modal('hide');
  }

  resetFilter() {
    this.filterData = {};
    this.date_range = [];
  }
  setPage(pageInfo) {
  
    let params: any;
   
    params = {
      'indent_type' : 'indent',
    };
    if (this.filterData != undefined || this.filterData != {}) {
      const filter = this.filterData;
     
      params.filters = filter;
    }
    this.page.pageNumber = pageInfo.offset;
    this.apiService.get('withdrawal/get_all/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size).subscribe(data => {
      const response: any = data;
       this.rows = response.results;
        this.tempRows = response.results;
        this.page.totalElements = response.count;
    });
  }

  previewRows: any = [];
  tempPreviewRow: any = [];
  previewFilter: any;

  previewSearch(event) {
  console.log('updateing');
    const val = event.target.value.toLowerCase();
    // filter our data
    const filterParam: any[] = [];
   this.previewColList.forEach(x => {
      filterParam.push([x.table_column, val]);
    });

    const temp = this.searchFilter(filterParam);
     this.previewRows = temp;
  }

  searchFilter(filters): any[] {
    const returnVal: any[] = [];
    let columnName: string;
    let val: string;
      this.tempPreviewRow.forEach(x => {
          for (let i = 0; i < filters.length; i++) {
            if (filters != undefined) {
              columnName = filters[i][0];
              val = filters[i][1].toString();
              if (x[columnName].toString().toLowerCase().indexOf(val) !== -1) {
                if (returnVal.indexOf(x) == -1) {
                  returnVal.push(x);
                }
              }
            }
          }
      });
    return returnVal;
  }




  setPreviewPage(pageInfo) {
    console.log('page clicked');
    this.previewPage.pageNumber = pageInfo.offset;
  }                 

    calculateTotalPage(rowcount, rowsize) {
      return Math.ceil(rowcount / rowsize);
        }

}
