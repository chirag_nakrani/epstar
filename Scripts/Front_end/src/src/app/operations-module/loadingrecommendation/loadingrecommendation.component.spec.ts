import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadingrecommendationComponent } from './loadingrecommendation.component';

describe('LoadingrecommendationComponent', () => {
  let component: LoadingrecommendationComponent;
  let fixture: ComponentFixture<LoadingrecommendationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingrecommendationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingrecommendationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
