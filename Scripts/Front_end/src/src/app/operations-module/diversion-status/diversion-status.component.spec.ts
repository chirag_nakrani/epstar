import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiversionStatusComponent } from './diversion-status.component';

describe('DiversionStatusComponent', () => {
  let component: DiversionStatusComponent;
  let fixture: ComponentFixture<DiversionStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiversionStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiversionStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
