import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';


declare var jquery: any;   // not required
declare var $: any;   // not required

@Component({
  selector: 'app-diversion-status',
  templateUrl: './diversion-status.component.html',
  styleUrls: ['./diversion-status.component.scss']
})
export class DiversionStatusComponent implements OnInit {

  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  modalRef: BsModalRef;
  alert_upload: any;
  alert: any;
  public isEdit = [];
  public isMultiEdit:boolean;
  page = new Page();
  selected = [];
  tmpArry=[];
  public addField: any = [{}];
  finalArrey=[];
  objTmp:any;
  atmArry:any;
  submitbuttonval:boolean;
  totalAmt:number=0;
  amt1:number;
  amt2:number;
  amt3:number;
  amt4:number;
  atmArryLength:number;
  selectedids = [];
  previewPage = new Page();
  submitted = false;
  temp = [];
  showErr1:boolean;
  showErr2:boolean;
  showErr3:boolean;
  showErr4:boolean;
  showErr5:boolean;
  showErr6:boolean;
  previewData: any;
  datePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  diRows: any;
  rowSingle: any;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;
  public uploadData: any;
  uploadForm: FormGroup;
  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];
  selectedColumn: any = [];
  columnChange: any = [];
  public filterData: any = {};
  public fileType = 'Diversion_status';

  constructor( private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig) {
    this.datePipe = new DatePipe('en-US');
    this.alert = {};
    this.alert.isvisible = false;
    this.alert_upload = {};
    this.alert_upload.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.dropdownValue = [];
    this.filterDropDownValue = {};
  }


  ngOnInit() {
    this.uploadData={};
    this.setDataInformationCurrent();
    this.getViewCol();
    this.submitbuttonval=true;
    this.allDataActive(0);
    this.populateSelectColumn();
    this.uploadForm = this.formBuilder.group({
      indent_order_number: ['', Validators.required],
      order_date: [ this.uploadData.order_date],
      cra: [ this.uploadData.cra],
      bank: [  this.uploadData.bank],
      feeder_branch: [  this.uploadData.feeder_branch],
      original_atm_id: [  this.uploadData.original_atm_id],
      temporary_atm_id: [  this.uploadData.temporary_atm_id],
      total_diversion_amount: [  this.uploadData.total_diversion_amount],
      denomination_100: [  this.uploadData.denomination_100],
      denomination_200: [  this.uploadData.denomination_200],
      denomination_500: [  this.uploadData.denomination_500],
      denomination_2000: [  this.uploadData.denomination_2000],
      reasons: [  this.uploadData.reasons],
      region: [  this.uploadData.region],
    });
  }

  // ------------------------------ upload end -----------------------------------
  // ----------code for select column ----------------


  filtersForAllView: any = {
    'record_status': 'Active'
  };

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));


  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  // ---------end of  code for select column ----------------------
//------------------ code for search ------------
AddFiled() {
  this.addField.push({});
  }
  RemoveField(index) {
    this.addField.splice(index,1);
    }

    clickMeToClear(){
      this.uploadData = {};
      this.addField= [{}];
      this.finalArrey=[];
      this.atmArry=[];
    }


viewSearch(event) {
  const val = event.target.value.toLowerCase();
  const temp = this.tempRows.filter(function (d) {
    var search_atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
    var search_bank_name = d.bank_name.toLowerCase().indexOf(val) !== -1;
    var search_category = d.category.toLowerCase().indexOf(val) !== -1;
    return search_atm_id || search_bank_name || search_category || !val;
  });

  this.rows = temp;
}
//------------------ code for search ------------
  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }
  // ------------------------- code for data information ----------------------
  setDataInformationCurrent() {
    const params = {
      'filters':{
    		'record_status': 'Approval Pending'
    	}
    };

    this.getDataInfo(0,params);
  }
  setDataInformationPrevious() {
    const params = {


    };

    this.getDataInfo(0,params);
  }
  getDataInfo(offSet:number,params: any) {
    console.log("all")
  	this.page.pageNumber = offSet;
    this.apiService.post('diversion_status/list/?page=' + (offSet + 1) + '&page_size=' + this.page.size, params).subscribe((data: any) => {
      if (data.results){
        const tempRow = data.results;
        this.diRows = tempRow;
        this.diRows = [...this.diRows];
        this.page.totalElements = data.count;
        this.page.totalPages = this.calculateTotalPage(data.count, this.page.size);
      }else{
        this.diRows = [];
        this.diRows = [...this.diRows];
      }

    }, (error) => {
      console.log('Data Info Error', error.error.status_text);
    });
    this.clearSelectArrayfn();
  }
  
  clearSelectArrayfn(){
    console.log("Inside select arrey clr called")
    this.selectedids=[];
    this.selected=[];
  }

  // -------------------------End of code for data information ----------------------
  onSelect({ selected },reqfrom) {
    this.isMultiEdit=true;
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if(this.selected.length>1){
      this.selected.splice(0,1);
    }
    if(this.selected.length<1 && reqfrom=='Active'){
      this.submitbuttonval=true;
      this.allDataActive(0);
    }

  }
  onMultipleEdit(){
    
    if(this.selected.length>0){
      this.submitbuttonval=false;
      this.rows = this.selected;
      this.rows = [...this.rows];
  
      for (let i = 0; i < this.rows.length; i++) {
        this.isEdit[i] = true;
      }
      console.log('edited row', this.rows);
    }

    }
    editSubmit(){
      this.submitbuttonval=true;
      var objTosend={
       total_diversion_amount:this.selected[0].total_diversion_amount,
       denomination_100:this.selected[0].denomination_100,
       denomination_200:this.selected[0].denomination_200,
       denomination_500:this.selected[0].denomination_500,
       denomination_2000:this.selected[0].denomination_2000
      }
      const params = {
        'id' : this.selected[0].diversion_request_no,
        'data': objTosend
      };
     this.apiService.post('diversion_status/edit/', params).subscribe((response: any) => {
      this.allDataActive(0);
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success  }';
      this.alert.text = response.status_text;
    }, (error:any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.status_text;
      }
    });
    }
  //------------------------code for view file in modal for current and previous tab----------------------------------
  //common section

  viewSystemData(offSet:number,params: any) {
  	this.page.pageNumber = offSet;
    this.apiService.post('diversion_status/list/?page=' + (offSet + 1) + '&page_size=' + this.page.size, params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      this.tempRows =[...this.rows];
      this.rows.forEach( (x,index) => {
       this.isEdit[index] = false;
       this.colList.filter( x => x.datatype == 'date').forEach( y => {
         if(x[y.prop] != null){
           x[y.prop] = new Date(x[y.prop]);
      }
       })
     });
      this.page.totalElements = response.count;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      // console.log('View Error', error.error);
    });
  }
  viewSystemDataSingle(offSet:number,params: any) {
    console.log('offset-----',offSet);
    this.page.pageNumber = offSet;
    this.apiService.post('diversion_status/list/?page=' + (offSet + 1) + '&page_size=' + this.page.size, params).subscribe((response: any) => {
      this.rowSingle = response.results;
      this.rowSingle = [...this.rowSingle];
      console.log('-----------single', this.rowSingle);
      this.page.totalElements = response.count;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      // console.log('View Error', error.error);
    });
  }
  getFileData(offSet:number, row:any) {
    const params = {
      'filters' :{
        'diversion_request_no':row.diversion_request_no
      }
    };
    console.log('params selected------',params);
    this.viewSystemDataSingle(offSet,params);
  }
  //end of common section
  //to open view file in current tab
  viewFile(rowData: any) {
  console.log("--------view file",rowData);
    this.getFileData(0,rowData);
    this.modalRef = this.modalService.show(this.previewModalcurrent, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }
  //to open view file in previous tab
  viewFilePrevious(rowData: any) {
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0,rowData);
    this.modalRef = this.modalService.show(this.previewModalprevious, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }

  //to Display All data of active status
  resetFilter() {
    this.filterData = {};
  }
  closeFilter(){
    this.filterData={};
    this.allDataActive(0);
  }
  allDataActive(offSet:number) {
    const params = {
      'filters':{
        'record_status': 'Approved'
      }

    };
    if (this.filterData != undefined || this.filterData != {}) {
      const filter = this.filterData;

      params.filters = filter;
    }
    this.viewSystemData(offSet,params);
  }
  //---------------------------------------code for view file in modal for current and previous tab- --------------------------------------
  //------------------code for Modal which is approved or rejected individual file -----------------------------
  getApprovedData(params: any) {
    this.apiService.post('diversion_status/approve/', params).subscribe((response: any) => {
      this.allDataActive(0);
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success  }';
      this.alert.text = response.status_text;
      console.log('-----------', this.rows);
    }, (error:any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.status_text;
      }
    });
  }
  getRejectedData(params: any) {
    this.apiService.post('diversion_status/reject/', params).subscribe((response: any) => {
      this.allDataActive(0);
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_text;
      console.log('-----------', this.rows);
    }, (error:any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.status_text;
      }
    });
  }
  //to approve complete diversion status
  approveData(row: any) {
    const params = {
      'id': row.diversion_request_no
    };
    this.getApprovedData(params);
  }
  //to reject complete Settings
  rejectData(row: any) {
    const params = {
      'id': row.diversion_request_no
    };
    this.getRejectedData(params);
  }
  //----------------end of code for Modal which is approved or rejected for--------------
  // ----------------d---Settings to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    this.columnConfig.columnDiversionStatus.forEach(x => {

        this.colList.push({'name': x.display_column_name, 'prop': x.table_column,"datatype":x.datatype,"isEditable":x.isEditable});
    });
    this.viewColumnAll = this.colList;
    this.viewColumn = this.viewColumnAll;
  }
  // ------------------------- to display column data in modal and in All Data ----------------------

  /////////////////// drop down for insert //////////////
  populateDropdown(data1) {
    let params = {
      'indent_order_number' : data1.indent_order_number
    }
      console.log('params-----',params);
      this.apiService.post('diversion_status/dropdown/', params).subscribe((response: any) => {
        console.log('response---',response);
        if (response.status_code == 'E100'){
          this.alert_upload.isvisible = true;
          this.alert_upload.type = 'Error';
          this.alert_upload.class = 'danger';
          this.alert_upload.text = ' ' + response.status_text;
          this.uploadData = {};
        }
        else{
          this.uploadData = response;
          this.uploadData.indent_order_number=data1.indent_order_number
          this.atmArry=response.atm;
          this.uploadForm.patchValue({
            bank: this.uploadData.bank,
             indent_order_number: this.uploadData.indent_order_number,
            order_date: this.uploadData.order_date,
            cra: this.uploadData.cra,
            feeder_branch: this.uploadData.feeder_branch,
            region: this.uploadData.region,
          });
        }

      }, (error) => {
        // console.log('View Error', error.error);
        this.alert_upload.isvisible = true;
        this.alert_upload.type = 'Error';
        // this.alert.text="error desc- " +error.status+ " "+error.statusText;
        this.alert_upload.text = 'Level - ' + error.error.level + '     Status -  ' + error.error.status_text;

        this.alert_upload.class = 'danger';
      });
    }
    addTotalAmount(i) {
    this.addField[i].totalAmt=((+ this.addField[i].amt1)+(+ this.addField[i].amt2)+(+ this.addField[i].amt3)+(+ this.addField[i].amt4));
    if(isNaN(this.addField[i].totalAmt)){
        this.addField[i].totalAmt=0
      }
     }
     addTotalAmountEdit(i) {
       console.log("Inside  edit row",i)
      if(this.rows[i].denomination_100==undefined){
        this.rows[i].denomination_100=0;
      }
      if(this.rows[i].denomination_200==undefined){
        this.rows[i].denomination_200=0;
      }
      if(this.rows[i].denomination_500==undefined){
        this.rows[i].denomination_500=0;
      }
      if(this.rows[i].denomination_2000==undefined){
        this.rows[i].denomination_2000=0;
      }
      this.rows[i].total_diversion_amount=((+ this.rows[i].denomination_100)+(+ this.rows[i].denomination_200)+(+ this.rows[i].denomination_500)+(+ this.rows[i].denomination_2000));

       }
    uploadDatafn(data) {
      this.finalArrey=[];
      for(var b=0;b<this.addField.length;b++){
        if(this.addField[b].totalAmt!=undefined){
          this.addField[b].showErr1=false
          this.addField[b].showErr2=false
          this.addField[b].showErr3=false
          this.addField[b].showErr4=false

        }
        if(this.addField[b].amt1==undefined || this.addField[b].amt1==''){
          this.addField[b].showErr1=true
        }else{
          this.addField[b].showErr1=false

        }
        if(this.addField[b].amt2==undefined || this.addField[b].amt2==''){
          this.addField[b].showErr2=true
        }else{
          this.addField[b].showErr2=false

        }
        if(this.addField[b].amt3==undefined || this.addField[b].amt3==''){
          this.addField[b].showErr3=true
        }else{
          this.addField[b].showErr3=false

        }
        if(this.addField[b].amt4==undefined || this.addField[b].amt4==''){
          this.addField[b].showErr4=true
        }else{
          this.addField[b].showErr4=false
        }
        if(this.addField[b].orgAtm==undefined || this.addField[b].orgAtm==''){
          this.addField[b].showErr5=true
        }else{
          this.addField[b].showErr5=false

        }
        if(this.addField[b].tmpAtm==undefined || this.addField[b].tmpAtm==''){
          this.addField[b].showErr6=true
        }else{
          this.addField[b].showErr6=false

        }
        if(this.addField[b].amt1==undefined ||this.addField[b].amt1==''||this.addField[b].amt2==undefined ||this.addField[b].amt2==''||this.addField[b].amt3==undefined||this.addField[b].amt3==''||this.addField[b].amt4==undefined||this.addField[b].amt4==''||this.addField[b].amt4==undefined||this.addField[b].amt4==''||this.addField[b].amt4==undefined||this.addField[b].amt4==''||this.addField[b].tmpAtm==undefined||this.addField[b].tmpAtm==''||this.addField[b].orgAtm==undefined||this.addField[b].orgAtm==''){
        }else{
          this.objTmp={}

          this.objTmp.bank=data.bank;
          this.objTmp.cra=data.cra;
          this.objTmp.denomination_100=this.addField[b].amt1;
          this.objTmp.denomination_200=this.addField[b].amt2;
          this.objTmp.denomination_500=this.addField[b].amt3;
          this.objTmp.denomination_2000=this.addField[b].amt4;
          this.objTmp.feeder_branch=data.feeder_branch;
          this.objTmp.indent_order_number=data.indent_order_number;
          this.objTmp.order_date=data.order_date;
          this.objTmp.region=data.region;

          this.objTmp.original_atm_id=this.addField[b].orgAtm;
          this.objTmp.total_diversion_amount=this.addField[b].totalAmt;
         this.objTmp.diverted_atm_id=this.addField[b].tmpAtm;


           this.finalArrey.push(this.objTmp)
          if(this.finalArrey.length == this.addField.length){
            if (this.uploadForm.invalid) {
              return;
              }

              this.apiService.post('diversion_status/upload/', this.finalArrey).subscribe((response:any) => {
                  this.alert.isvisible = true;
                  $('#m_modal_6').modal('hide');

                  this.setDataInformationCurrent();
                  this.alert.type = 'Upload';
                  this.alert.text = ' ' + response.status_text;
                  this.alert.class = 'success';
              }, (error => {
                // alert("Error - "+ error.status+ " "+error.statusText);


                this.alert.isvisible = true;
                this.alert.type = 'Error';
                // this.alert.text="error desc- " +error.status+ " "+error.statusText;
                this.alert.text = 'Level - ' + error.error.level + '     Status -  ' + error.error.status_text;

                this.alert.class = 'danger';
              }));

          }
        }
      }


    }
}
