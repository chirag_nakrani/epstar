import { Component, OnInit , TemplateRef, AfterViewInit } from '@angular/core';
import {ViewChild, ElementRef} from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Page} from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';

@Component({
  selector: 'app-indentsrevisions',
  templateUrl: './indentsrevisions.component.html',
  styleUrls: ['./indentsrevisions.component.scss']
})

export class IndentsrevisionsComponent implements OnInit {
  excelType = 'Indents Revisions';
  modalRef: BsModalRef;
  alert: any = {};
  page = new Page();
  selected = [];
  selectedids = [];
  previewPage = new Page();
  public uploadData: any;
  uploadForm: FormGroup;
  submitted = false;
  temp = [];
  previewData: any;
  datePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  public detailsColList: any;
  rows: any = [];
  prevRows: any;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  public editRow: any;
  public currentIndent: any;
  public viewIndent = true;
  public rowData: any;
  tempRows: any = [];
  public permissionList: any = [];
  public projectBankList: any;
  public bankList: any = [];
  public feederList: any = [];
  public filterData: any = {};
  public now: Date = new Date();
  // public isDataLoaded: boolean;
  public pagesizeList: any = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  public statusList = ['All', 'Active', 'Approval Pending', 'Review Pending', 'History'];
  public filterDate: Date = null;
  public config: any;
  public feederValue: any = [];
  public countData: any = {};

  @ViewChild('viewDetailsModal') viewDetailsModal: ElementRef;
  // @ViewChild('viewDataModal') viewDataModal: DatatableComponent;
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
    this.datePipe = new DatePipe('en-US');
  }

  ngOnInit() {
    this.dropdownsForProjectBank();
    if(this.sessionStorageService.get('permissionList') != null)
      this.permissionList = this.sessionStorageService.get('permissionList');
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.filterData.record_status = "Review Pending";
    // this.previewPage.pageNumber = 0;
    // this.previewPage.size = 20;
    this.getViewCol();
    this.allDataActive(0);
    //this.filterDate = new Date();

    this.config = {
      displayKey: 'feeder', //if objects array passed which key to be displayed defaults to description
      search: true, //true/false for the search functionlity defaults to false,
      height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
      placeholder: 'Select', // text to be displayed when no item is selected defaults to Select,
      noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
      searchPlaceholder: 'Search', // label thats displayed in search input,
      searchOnKey: 'feeder' // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
    };
  }


// --------------------search function ---------------

viewSearch(event) {
  console.log('updateing');
  const val = event.target.value.toLowerCase();
  const temp = this.tempRows.filter(function(d) {
    const search_indent_revision_order_number = d.indent_revision_order_number.toLowerCase().indexOf(val) !== -1;
    const search_indent_order_number = d.indent_order_number.toLowerCase().indexOf(val) !== -1;
    return  search_indent_revision_order_number || search_indent_order_number || !val;
  });

  this.rows = temp;
}
//----------- end of serch function ---------
  getViewCol() {
    this.colList = [];
    this.colList = this.columnConfig.indenRevisionColumnConfig.filter(x => x.isDisplayColumn ).map((x) => {
      return {'name': x.display_column_name, 'prop': x.table_column, 'dataType': x.dataType};
    });
    this.viewColumnAll = this.colList;

  }

  allDataActive(pageNumber: any) {
    this.sanitizeFilters();
    const params =  {
          'indent_type' : 'indent_revision',
          'filters': {}
      };
      if (this.filterData != undefined || this.filterData != {}) {
      const filter = this.filterData;
      if (this.filterDate != null) {
        filter.order_date = this.datePipe.transform(this.filterDate, 'yyyy-MM-dd');
      }
      params.filters = filter;
    }
    this.page.pageNumber = pageNumber;
    this.currentPage = pageNumber + 1;
    this.getStatusCount(JSON.stringify(params));
    this.apiService.post('indent/list/?page=' + (this.currentPage) + '&' + 'page_size=' + this.page.size, params).subscribe((response: any) => {
       this.rows = response.results;
       this.rows = [...this.rows];
       this.page.totalElements = response.count;
       this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
       this.tempRows= [...this.rows];
     }, (error: any) => {
        console.log('Error', error.error);
     });
  }

  getStatusCount(params: string) {
    const param = JSON.parse(params);
    param.filters.created_on = this.datePipe.transform(this.now, 'yyyy-MM-dd');
    if(param.filters.record_status != undefined){
      delete param.filters.record_status;
    }
    this.apiService.post('indent/count/', param).subscribe((response: any) => {
        this.countData = response;
        console.log('lsad',this.countData);
     }, (error: any) => {
        console.log('Error', error.error);
     });
  }

  statusFilter(status: string) {
    this.filterData.record_status = status;
    this.allDataActive(0);
  }

  sanitizeFilters(){
      for(let key in this.filterData){
        if(this.filterData[key] == null || this.filterData[key] == '' || this.filterData[key] == 'All'){
          delete this.filterData[key];
        }
      }
  }

  getIndentDetails(row: any) {
    this.currentIndent = row;
    const params = {'indent_order_no' : row.indent_order_number};
    this.apiService.post('indent/detail/', params).subscribe((response: any) => {
       this.prevRows = response.indent_detail;
       this.prevRows = [...this.prevRows];
       this.openDetailsModal();
     }, (error: any) => {
        console.log('Error', error.error);
        this.alert.isvisible = true;
        this.alert.class = 'danger';
        this.alert.type = 'Error!';
        this.alert.text = 'Indent details not available please try after sometime.';
     });
  }

  openDetailsModal() {
    this.modalRef = this.modalService.show(this.viewDetailsModal, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }

  dropdownsForProjectBank() {
    const params = {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    };
    this.apiService.post('bankinfo/', params).subscribe((data: any) => {
       this.bankList = [];
       this.projectBankList = data.data;
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }

  resetFilter() {
    this.filterData = {};
    this.feederValue = [];
    this.bankList = [];
    this.feederList = [];
    this.filterDate = null
  }

  getBankList() {
    this.bankList = [];
    this.bankList = this.projectBankList.filter( x => x.project_id == this.filterData.project_id )[0].bank;
    this.bankList = this.removeDuplicate(this.bankList, "bank_id");
  }

  getFeeder() {
    this.feederList = [];
    const tempList = [];
    this.bankList.filter( x => x.bank_id == this.filterData.bank_code)[0].feeder.forEach( z => {
      tempList.push(z);
    });
    this.removeDuplicate(tempList,"feeder").forEach( x => {
      this.feederList.push(x.feeder)
    });
  }

  removeDuplicate (arr: any, comp: string) {

    const finalArr = arr
                  .map(el => el[comp]) // store the keys of the unique objects
                  .map((el, index, a) => a.indexOf(el) === index && index) // eliminate the dead keys & store unique objects
                  .filter(el => arr[el])
                  .map(el => arr[el]);
     return finalArr;

  }


  approveIndent(row: any, status: string) {
    const params = {
      'existing_record': row,
      'updated_record_status': ''
    };
    console.log(status+"******");
    if( status == 'Approval Pending' ) {
    console.log("****valid******");
      params.updated_record_status = 'Approved';
      console.log(params.updated_record_status+"****valid******");
    } else if( status == 'Review Pending' ) {
      params.updated_record_status = 'Active';
    }

    this.apiService.post('indent/status/', params).subscribe((response: any) => {
        this.alert.isvisible = true;
        this.alert.class = 'success';
        this.alert.type = 'Success';
        this.alert.text = response.status_desc;
        this.allDataActive(0);
     }, (error: any) => {
        this.alert.isvisible = true;
        this.alert.class = 'danger';
        this.alert.type = 'Error!';
        this.alert.text = error.error;
     });
  }

  rejectIndent(row: any, status: string) {

    const params = {
      'updated_record_status': status,
      'indent_revision_date': '2018-11-22',
      'existing_record': row
    };

    this.apiService.post('indent/status/', params).subscribe((response: any) => {
        this.alert.isvisible = true;
        this.alert.class = 'success';
        this.alert.type = 'Success';
        this.alert.text = response.status_desc;
        this.allDataActive(0);
     }, (error: any) => {
        this.alert.isvisible = true;
        this.alert.class = 'danger';
        this.alert.type = 'Error!';
        this.alert.text = error.error;
     });
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  returnToView(event: any) {
    this.viewIndent = event.view;
    this.alert = event.alert;
    this.allDataActive(0);
  }

  editIndent(row: any) {
    this.rowData = row;
    this.viewIndent = false;
  }

}
