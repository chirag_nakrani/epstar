import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import * as FileSaver from 'file-saver';



declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-indentreports',
  templateUrl: './indentreports.component.html',
  styleUrls: ['./indentreports.component.scss']
})
export class IndentreportsComponent implements OnInit {
  type="Indent Report"
  public colList: any =[{}];
  viewColumnAll: any = {};
  rows: any = [];
  alert: any;
  page = new Page();
  previewData: any;
  filterData: any = {};
  datePipe ;
  public filterDate: Date = null;
  blob: Blob;
  fileName: string;

  bankList: any = [{
    bank: 'Lakshmi Vilas Bank',
    value: 'LVB'
  }, {
    bank: 'Bank of Maharashtra',
    value: 'BOMH'
  }, {
    bank: 'Dena Bank',
    value: 'DENA'
  }, {
    bank: 'Allahabad Bank',
    value: 'ALB'
  }]
  public currentPageSize: number;
  public currentPage = 1;
  constructor(private apiService: ApiService, private columnConfig: ColumnConfig) {
    this.datePipe = new DatePipe('en-US');
    this.page.size = 20;
    this.currentPageSize = 20;
    this.alert = {};
    this.alert.isvisible = false;
  }

  ngOnInit() {
    $('#modalfilter').modal('show');
  }
  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }
  InsertDatafn(offSet:number) {
    this.page.pageNumber = offSet;
    this.getViewCol(this.filterData.bank);
    // this.page.pageNumber = this.currentPage + offSet;
    const params = this.filterData;
    params.type=this.type,
    console.log("paramsparams--------",params);
    if (this.filterDate != null) {
        params.date = this.datePipe.transform(this.filterDate, 'yyyy-MM-dd');
      }
    
      this.apiService.post('cash_reports/?page=' + (offSet + 1) + '&page_size=' + this.page.size, params).subscribe((response: any) => {
        console.log('resonse------', response);
       
          this.rows = response.results;
          this.rows = [...this.rows];
          this.page.totalElements = response.count;
          this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
        $('#modalfilter').modal('hide');
      
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#modalfilter').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
  this.resetFormdata();
  }
  exportReport(){
    this.getViewCol(this.filterData.bank);
    const params = this.filterData;
    params.type=this.type,
    console.log("paramsparams--------",params);
    if (this.filterDate != null) {
      params.date = this.datePipe.transform(this.filterDate, 'yyyy-MM-dd');
    }
    params.export = 1;
      this.apiService.getFile('cash_reports/', params).subscribe((response: Blob) => {
      console.log('resonse------', response);
      this.blob = new Blob([(response)], { type: 'application/pdf' });
      this.fileName = params.bank+'_'+params.date+'_'+params.type;
      FileSaver.saveAs(this.blob, this.fileName + '.xlsx');
      
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#modalfilter').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
  }
  resetFormdata(){
  this.filterData.type=null;
  this.filterData.date=null;
  }
  getViewCol(bank: string) {
   
    this.colList = [];
    console.log(this.columnConfig.columnConfigindentReport);
    this.columnConfig.columnConfigindentReport.filter( x => x.bank_name == bank && x.type == this.type).forEach(x => {
      this.colList.push({
        'name': x.display_column_name,
        'prop': x.table_column,
      });
    });
    this.viewColumnAll = this.colList;
    console.log('viewColumn', this.viewColumnAll);
  }
  
}
