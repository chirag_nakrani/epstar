import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-activity-status',
  templateUrl: './activity-status.component.html',
  styleUrls: ['./activity-status.component.scss']
})
export class ActivityStatusComponent implements OnInit {
excelType="Activity Status"
  alert: any;
  page: Page = new Page();
  temp = [];
  public colList: any;
  diRows: any;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;
  public pagesizeList = [10,20,30,40,50,100];
  
  // Constructor to declare all the variables used
  constructor( private apiService: ApiService, private columnConfig: ColumnConfig) {
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 10;
    this.currentPageSize = 10;
  }


  ngOnInit() {
    this.getViewCol();
    this.viewActivityStatus(0);
    this.populateSelectColumn();
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  // ----------view all data ----------------

  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    });
    // }
    console.log('col change', this.selectedColumn, this.viewColumn);
  }


  // ---------end of  view all data ----------------------

  //------------------------code for view data in datatable----------------------------------
  //commonon
  viewActivityStatus(offSet:number) {
  const params = {
  }
  this.page.pageNumber = offSet;
    this.apiService.post('activity_status/?page=' + (offSet + 1) + '&page_size=' + this.page.size,params).subscribe((response: any) => {
      ;
      this.rows = response.results;
      this.rows = [...this.rows];
      //this.alert.isvisible = true;
      //this.alert.type = 'Success';
      //this.alert.class = 'success';
      //this.alert.text = response.status_text;
      console.log('-----------common', this.rows);
      this.page.totalElements = response.count;
      // // this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error: any) => {
      //this.alert.isvisible = true;
      //this.alert.type = 'Failure';
      //this.alert.class = 'danger';
      if (error.error != null){
        //this.alert.text = error.error.status_text;
      } else {
        //this.alert.text = error.statusText;
        }
    });
  }
  //end of common section
;
  
  
  // ------------------------- to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.activityStatusColumnConfig);
    this.columnConfig.activityStatusColumnConfig.forEach(x => {

      this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column, 'datatype': x.datatype });
      // console.log("xxxxxxxxxxx",x);
    });
    this.viewColumnAll = this.colList;
    console.log('viewColumn', this.viewColumnAll);
  }
  // ------------------------- to display column data in modal and in All Data ----------------------


}


