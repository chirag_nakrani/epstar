import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndentAdminComponent } from './indent-admin.component';

describe('IndentAdminComponent', () => {
  let component: IndentAdminComponent;
  let fixture: ComponentFixture<IndentAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndentAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndentAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
