import {
  Component,
  OnInit,
  OnDestroy,
  HostListener,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';


declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-indent-admin',
  templateUrl: './indent-admin.component.html',
  styleUrls: ['./indent-admin.component.scss']
})
export class IndentAdminComponent implements OnInit {

  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  @ViewChild('progressbar') progressbar: ElementRef;
  @ViewChild('flagCall') flagCall: ElementRef;
  @ViewChild('pdfGenerateModal') pdfGenerateModal: ElementRef;

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHander(event) {
    if(this.pdfProgressDataActive.state != undefined){
      if( this.pdfProgressDataActive.state != 'SUCCESS' ) {
        this.sessionStorageService.set('pdfTaskList',this.pdfProgressDataActive);
      }else{
        this.sessionStorageService.set('pdfTaskList',null);
      }
    }else{
      this.sessionStorageService.set('pdfTaskList',null);
    }
    this.timeOutIDs.forEach(id => clearTimeout(id));
  }
  modalRef: BsModalRef;
  alert: any;
  alert_dispense: any;
  pdf_response: any;
  page: Page = new Page();
  selected = [];
  selectedids = [];
  submitted = false;
  temp = [];
  previewColList: any;
  public colList: any;
  diRows: any;
  viewColumnAll: any = {};
  datePipe: DatePipe;
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  public editRow: any = {};
  public isMultiEdit: boolean;
  public pagesizeList = [10,20,30,40,50,100];
  public permissionList: any = [];
  public filterData: any = {};
  public pdfData: any = {};
  public projectBankList: any;
  public bankList: any = [];
  public bankListPdf: any = [];
  public feederList: any = [];
  public now: Date = new Date();
  public progressResponse: any = {};
  public task_id: string;
  public progressStatus: string ='';
  public progressMessage: string;
  public progressVisible: boolean;
  public projectbank: any;
  public config: any;
  public flag: boolean;
  public bank: any;
  public bank_list: any;
  public title: string;
  public calculateType: string;
  public pdf_email_status: any = [];
  public bankValue: any = [];
  public cbr_date_1: any = [];
  public cbr_date_2: any = [];
  public bank_list_single_record = [];
  public displayProgress = false;
  public isExpanded = true;
  public progressPercentage: number;
  public toggleProgress = false;
  public pdfProgressDataActive: any = {};
  timeOutIDs:number[] = [];
  // Constructor to declare all the variables used
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
    this.alert = {};
    this.alert.isvisible = false;
    this.alert_dispense = {};
    this.alert_dispense.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 10;
    this.currentPageSize = 10;
    this.datePipe = new DatePipe('en-US');
    this.pdf_email_status = [
      "Success",
      "Failure"
    ];
    this.pdfData.cbr_date_1 = [];
    this.pdfData.cbr_date_2 = [];
  }


  ngOnInit() {

    if(this.sessionStorageService.get('permissionList') != null)
      this.permissionList = this.sessionStorageService.get('permissionList');
    this.progressVisible = false;
    this.getViewCol();
    this.dropdownsForProjectBank();
    this.allDataActive(0);
    this.isMultiEdit = false;
    this.populateSelectColumn();
    this.flag= false;
    this.config = {
      displayKey: 'bank_name', //if objects array passed which key to be displayed defaults to description
      search: true, //true/false for the search functionlity defaults to false,
      //height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
      placeholder: 'Select', // text to be displayed when no item is selected defaults to Select,
      noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
      searchPlaceholder: 'Search', // label thats displayed in search input,
      default: 'All',
      searchOnKey: 'bank_name' // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
    };
    if(this.sessionStorageService.get('pdfTaskList') != null){
      this.pdfProgressDataActive = this.sessionStorageService.get('pdfTaskList');
      this.openProgressModal();
      this.getPdfResponse(this.pdfProgressDataActive.task_id);
    }
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  // ----------code to select column ----------------

  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    });
    // }
    console.log('col change', this.selectedColumn, this.viewColumn);
  }


  // ---------end of code to select column  ----------------------
// --------------------search function ---------------
viewSearch(event) {
  console.log('updateing');
  const val = event.target.value.toLowerCase();
  const temp = this.tempRows.filter(function(d) {
    const bank_code = d.bank_code.toLowerCase().indexOf(val) !== -1;
    const atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
    const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
    return bank_code || atm_id || search_project_id || !val;
  });
  this.rows = temp;
}
//----------- end of serch function ---------

  //------------------------code for view data in datatable----------------------------------
  //commonon
  viewIndentData(offSet: number) {
    this.sanitizeFilters();
    this.page.pageNumber = offSet;
    let params = {

    }
    if (this.filterData != undefined || this.filterData != {}) {
      const filter = this.filterData;
      if (this.filterData.indent_order_date != null) {
        filter.indent_order_date = this.datePipe.transform(this.filterData.indent_order_date, 'yyyy-MM-dd');
      }
      params = filter;
    }
    this.apiService.post('indent_admin/?page=' + (offSet + 1) + '&page_size=' + this.page.size,params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      this.page.totalElements = response.count;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.statusText;
        }
    });
  };
  //end of common section

  sanitizeFilters(){
      for(let key in this.filterData){
        if(this.filterData[key] == null || this.filterData[key] == '' || this.filterData[key] == 'All'){
          delete this.filterData[key];
        }
      }

      for(let key in this.pdfData){
        if(this.pdfData[key] == null || this.pdfData[key] == '' || this.pdfData[key] == 'All'){
          delete this.pdfData[key];
        }
      }
  }

  //to Display All data of active status
  allDataActive(offSet: number) {
    console.log('inside all data active');
    this.viewIndentData(offSet);
  }
  //---------------------------------------end of code for view data in datatable---------------------------------------
  //------------------code for select particular record -----------------------------
  onSelect({ selected }) {
    this.isMultiEdit = true;
    console.log('Select Event', this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

  }

  //----------------end of code for select particular record--------------

    // ------------------------- to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.indentAdminColumnConfig);
    this.columnConfig.indentAdminColumnConfig.forEach(x => {

      this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column, 'datatype': x.datatype });
      // console.log("xxxxxxxxxxx",x);
    });
    this.viewColumnAll = this.colList;
    console.log('viewColumn', this.viewColumnAll);
  }
  // ------------------------- to display column data in modal and in All Data ----------------------

  //---------------generate indent function------------------
  generateIndent() {
    let params = {};
    if (this.flag == false){
      this.getSelectedBank(this.pdfData.bank_code);
      if (this.pdfData != undefined || this.pdfData != {}) {
        if (this.pdfData.bank_code.indexOf('All') != -1){
          params = {
            'feeder_branch': 'all',
            'project_id': 'all',
            'bank_code': 'all',
            'indent_date': this.datePipe.transform(this.now, 'yyyy-MM-dd')
          };
        }else{
          params = {
            'feeder_branch':  'all',
            'project_id': 'all',
            'bank_code': this.pdfData.bank_code,
            'indent_date': this.datePipe.transform(this.now, 'yyyy-MM-dd')
          };
        }

      }
    }else{
      if (this.selected.length > 0 ){
      this.bank_list_single_record.push(this.selected[0].bank_code)
        params = {
          'feeder_branch':  this.selected[0].feeder_branch,
          'project_id': this.selected[0].project_id,
          'bank_code': this.bank_list_single_record
        };
        if (this.selected[0].indent_date != ''){
          params['indent_date'] = this.datePipe.transform(this.selected[0].indent_date, 'yyyy-MM-dd');
        }else{
          params['indent_date'] = this.datePipe.transform(this.now, 'yyyy-MM-dd');
        }
      }else{
        params = {
          'feeder_branch': 'all',
          'project_id': 'all',
          'bank_code': 'all',
          'indent_date': this.datePipe.transform(this.now, 'yyyy-MM-dd')
        };
      }
    }

    this.apiService.post('indent/generate/', params).subscribe((response: any) => {
      this.allDataActive(0);
    this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_desc;
    }, (error: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_desc;
      } else {
        this.alert.text = error.status_desc;
        }
    });
    this.resetFilter();

  }
  //---------------end of generate indent function------------------

  //---------------generate indent pdf function------------------


  verifyPdfCall(){
    if (this.selected.length > 0){
      this.flag= true;
      this.generatePdf();
    }else{
      $('#flagCall').val(2);
      this.title = 'Generate PDF';
      $('#modalpdf').modal('show');
    }
  }

  verifyCalculateCall(type: string){
    this.calculateType = type;
    $('#flagCall').val(3);
    $('#modalpdf').modal('show');
  }

  verifyCalculateDispenseCall(type: string){
    this.calculateType = type;
    $('#flagCall').val(4);
    //this.bank_list.splice(0,1);
    $('#modaldispense').modal('show');
  }

  get_dispense_date(){
    this.sanitizeFilters();
    let params = {

    }
    if (this.pdfData != undefined || this.pdfData != {}) {
      this.pdfData.file_type = 'Dispense';
      const filter = this.pdfData;
      if (this.pdfData.datafor_date_time != null) {
        filter.datafor_date_time = this.datePipe.transform(this.pdfData.datafor_date_time, 'yyyy-MM-dd');
      }else{
        filter.datafor_date_time = this.datePipe.transform(this.now, 'yyyy-MM-dd');
      }
      params = filter;
    }
    this.apiService.post('indent_calculation/dropdown/', params).subscribe((response: any) => {
      console.log('response',response);
      if ('status_c3r_text' in response){
          this.alert_dispense.isvisible = true;
          this.alert_dispense.type = 'Error';
          this.alert_dispense.class = 'danger';
          this.alert_dispense.text = response.status_c3r_text;
      }else if ('status_cbr_1_text' in response){
        this.alert_dispense.isvisible = true;
        this.alert_dispense.type = 'Error';
        this.alert_dispense.class = 'danger';
        this.alert_dispense.text = response.status_cbr_1_text;
      }else if ('status_cbr_2_text' in response){
        this.alert_dispense.isvisible = true;
        this.alert_dispense.type = 'Error';
        this.alert_dispense.class = 'danger';
        this.alert_dispense.text = response.status_cbr_2_text;
      }
      //else{
       // this.alert_dispense.type = 'Success';
        //this.alert_dispense.class = 'success';
        //this.alert_dispense.text = 'Data Loaded for bank ' + this.pdfData.bank_code;
      //}
      this.pdfData.c3r_date = new Date(Date.parse(response.data.c3r_date));
      this.cbr_date_1 = response.data.cbr_1;
      this.cbr_date_2 = response.data.cbr_2;
      console.log("this.pdfDatathis.pdfData",this.pdfData);
    }, (error: any) => {
      this.alert_dispense.isvisible = true;
      this.alert_dispense.type = 'Failure';
      this.alert_dispense.class = 'danger';
      if (error.error != null){
        this.alert_dispense.text = error.error.status_text;
      } else {
        this.alert_dispense.text = error.status_text;
        }
    });
    this.pdfData.datafor_date_time = new Date(Date.parse(this.pdfData.datafor_date_time));
  }

  verifyIndentCall(){
    if (this.selected.length > 0){
      this.flag= true;
      this.generateIndent();
    }else{
      $('#flagCall').val(1);
      this.title = 'Generate Indent';
      $('#modalpdf').modal('show');
    }
  }

  functionCall(){
    if ($('#flagCall').val() == 1){
      this.generateIndent();
    }else if($('#flagCall').val() == 2){
      this.generatePdf();
    }else if($('#flagCall').val() == 3){
      this.calculateIndent();
    }else if($('#flagCall').val() == 4){
      this.calculateIndent();
    }
    this.resetFilter();
  }

  generatePdf() {
    this.sanitizeFilters();
    this.progressResponse = null;
    let params : any;
    console.log('pdfdata:',this.pdfData);
    if (this.flag == false){
      this.getSelectedBank(this.pdfData.bank_code);
      if (this.pdfData != undefined || this.pdfData != {}) {
        if (this.pdfData.bank_code.indexOf('All') != -1){
          params = {
            'record_status':'Active'
          };
        }else{
          params = {
            'bank_code': this.pdfData.bank_code,
            'indent_date': this.datePipe.transform(this.now, 'yyyy-MM-dd')
          }
        }

      }
    }else{
      if (this.selected.length > 0 ){
        params = {
          'indent_order_number': this.selected[0].indent_order_number
        };
      }else{
        params = {
          'record_status':'Active'
        };
      }
    }

    this.apiService.post('create_pdf/', params).subscribe((response: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = 'In Progress. Please check your email for progress report.';
      this.task_id = response.task_id;
      //document.getElementById('progress').innerHTML = response;
      this.progressMessage = 'PDFs are being generated Plaese wait...';
      this.progressPercentage = 0;
      this.openProgressModal();
      this.getPdfResponse(this.task_id);
    }, (error: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.statusText;
        }
    }, () => {
        this.allDataActive(0);
    });
    this.selected = [];
    this.pdfData = {};
    this.resetFilter();

  }

  openProgressModal() {
    this.displayProgress = true;
  }

  calculateIndent() {
    this.sanitizeFilters();
    let params : any;
    console.log('pdfdata:',this.pdfData);
    this.getSelectedBank(this.pdfData.bank_code);
    if (this.flag == false){
      if (this.pdfData != undefined || this.pdfData != {}) {
        if (this.pdfData.bank_code.indexOf('All') != -1){
          params =
            {
              "file_type":this.calculateType,
              "indent_date":this.datePipe.transform(this.now, 'yyyy-MM-dd'),
              "bank_code":"",
              "project_id":""
            }
        }else{
          params = {
            "file_type":this.calculateType,
            "indent_date":this.datePipe.transform(this.now, 'yyyy-MM-dd'),
            "bank_code":this.pdfData.bank_code,
            "project_id":""
          }
        }

      }
    }

    this.apiService.post('indent_calculation/', params).subscribe((response: any) => {
      this.allDataActive(0);
    this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_text;
    }, (error: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.status_text;
        }
    });
    this.resetFilter();
    this.pdfData = {};

  }

  calculateDispense() {
    this.sanitizeFilters();
    let params : any;
    console.log('pdfdata:',this.pdfData);
    if (this.pdfData != undefined || this.pdfData != {}) {
        if (this.pdfData.datafor_date_time == null || this.pdfData.cbr_date_1 == null || this.pdfData.cbr_date_2 == null || this.pdfData.c3r_date == null  || this.pdfData.bank_code == null ){
          this.alert_dispense.isvisible = true;
          this.alert_dispense.type = 'Error';
          this.alert_dispense.class = 'danger';
          this.alert_dispense.text = 'Please select all the values and then submit.';
        }else{
          params = {
            "file_type":this.calculateType,
            "datafor_date_time":this.datePipe.transform(this.pdfData.datafor_date_time, 'yyyy-MM-dd'),
            "cbr_t_minus_1" : this.datePipe.transform(this.pdfData.cbr_date_1, 'yyyy-MM-dd HH:mm:SS'),
            "cbr_t_minus_2" : this.datePipe.transform(this.pdfData.cbr_date_2, 'yyyy-MM-dd HH:mm:SS'),
            "c3r_date":this.datePipe.transform(this.pdfData.c3r_date, 'yyyy-MM-dd'),
            "bank_code":this.pdfData.bank_code,
            "project_id":"All"
          }
          this.apiService.post('indent_calculation/', params).subscribe((response: any) => {
            this.allDataActive(0);
            this.alert.isvisible = true;
            this.alert.type = 'Success';
            this.alert.class = 'success';
            this.alert.text = response.status_text;
          }, (error: any) => {
            this.alert.isvisible = true;
            this.alert.type = 'Failure';
            this.alert.class = 'danger';
            if (error.error != null){
              this.alert.text = error.error.status_text;
            } else {
              this.alert.text = error.status_text;
              }
          });
          $('#modaldispense').modal('hide');
          this.resetFilter();
          this.pdfData = {};
        }
    }
  }

  getSelectedBank(bank_code: any){
    this.pdfData.bank_code = [];
    bank_code.forEach(x => {
      this.pdfData.bank_code.push(x.bank_name);
    });
    //this.pdfData.bank_code = this.pdfData.bank_code.toString();

  }

  async getPdfResponse(task_id: any) {
   this.apiService.post('get-task-info/',{"task_id": task_id}).subscribe((response: any) => {
        console.log('response ---- ',response);
        this.progressResponse = response;
        this.progressStatus = this.progressResponse.state;

        if (response.state != 'SUCCESS') {
          if(this.progressResponse.result){
            this.progressVisible = true;
            this.progressMessage = this.progressResponse.result.current + ' out of ' + this.progressResponse.result.total + ' PDFs Generated';
            this.progressPercentage = this.progressResponse.result.percent;
          }

          if(this.pdfProgressDataActive.task_id == task_id){
              this.pdfProgressDataActive.results = this.progressResponse.result;
              this.pdfProgressDataActive.state = this.progressResponse.state;
          }else{
            this.pdfProgressDataActive = {'task_id': task_id, 'results': this.progressResponse.result,'state': this.progressResponse.state};
          }
          this.timeOutIDs.push(
            setTimeout(() => this.getPdfResponse(task_id), 30000)
          );

        } else {
          this.alert.isvisible = true;
          this.alert.type = 'Success';
          this.alert.class = 'success';
          this.alert.text = 'PDFs Generated Successfully';
          this.progressMessage = this.progressResponse.result.current + ' out of ' + this.progressResponse.result.total + ' PDFs Generated';
          this.progressPercentage = this.progressResponse.result.percent

          if(this.pdfProgressDataActive.task_id == task_id){
              this.pdfProgressDataActive.results = this.progressResponse.result;
              this.pdfProgressDataActive.state = this.progressResponse.state;
          }else{
            this.pdfProgressDataActive = {'task_id': task_id, 'results': this.progressResponse.result,'state': this.progressResponse.state};
          }
          this.progressVisible = true;
          this.allDataActive(0);
        }
      // if(!response.complete){
      //   this.getPdfResponse();
      // }
      //document.getElementById('atmsdataportlet').innerHTML = response;
    }, (error: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;

      } else {
        this.alert.text = error.status_text;
        }
    });

  }

  //---------------end of generate indent pdf function------------------

  //---------------send indent pdf email function------------------
  sendEmail() {

    const params = {
      'id':this.selected[0].id,
      'indent_order_number':this.selected[0].indent_order_number,
    };

    this.apiService.post('generate_indent_pdf/', params).subscribe((response: any) => {
    this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.email_status.status_text;
    }, (error: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.email_status.status_text;
      } else {
        this.alert.text = error.status_text;
        }
    }, ()=>{
         this.allDataActive(0);
         this.resetFilter();
    });

  }
  dropdownsForProjectBank() {
    const params = {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    };
    this.apiService.post('bankinfo/', params).subscribe((data: any) => {
       this.bankList = [];
       this.projectBankList = data.data;
       this.getBank(this.projectBankList);

    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }

  getBank(projectBankList: any){
    this.bankListPdf.push({bank_name:'All'});
    console.log('project',projectBankList);
    console.log('project length',projectBankList.length);
    for (let  i = 0; i<= projectBankList.length-1;i++){
      this.projectbank = projectBankList[i].bank;
      for (let j = 0;j<=this.projectbank.length-1;j++){
        this.bankListPdf.push({bank_name:this.projectbank[j].bank_id});

      }
    }
    this.bankListPdf = this.bankListPdf.reduce((acc, current) => {
      const x = acc.find(item => item.bank_name === current.bank_name);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);
    console.log('bank',  this.bankListPdf);
    this.bank_list = this.bankListPdf;
  }

  resetFilter() {
    this.filterData = {};
    this.bankList = [];
    this.feederList = [];
    this.pdfData = {};
    this.bankValue = [];
    this.getBank(this.projectBankList);
    this.alert_dispense.isvisible = false;
  }

  resetModal(){
    this.bank = [];
    this.bank_list = [...this.bank_list];
  }

  getBankList() {
    this.bankList = [];
    this.bankList = this.projectBankList.filter( x => x.project_id == this.filterData.project_id )[0].bank;
  }

  getFeeder() {
    this.feederList = [];
    this.feederList = this.bankList.filter( x => x.bank_id == this.filterData.bank_code)[0].feeder;
  }

  toggelApiCall(flag:boolean){
    if(flag == true){
      this.timeOutIDs.forEach(id => clearTimeout(id));
    }else{
      this.getPdfResponse(this.pdfProgressDataActive.task_id);
    }
  }

  //---------------end of send indent pdf email function------------------
  ngOnDestroy(){
    if(this.pdfProgressDataActive.state != undefined){
      if( this.pdfProgressDataActive.state != 'SUCCESS' ) {
        this.sessionStorageService.set('pdfTaskList',this.pdfProgressDataActive);
      }else{
        this.sessionStorageService.set('pdfTaskList',null);
      }
    }else{
      this.sessionStorageService.set('pdfTaskList',null);
    }
    this.timeOutIDs.forEach(id => clearTimeout(id));
  }
}
