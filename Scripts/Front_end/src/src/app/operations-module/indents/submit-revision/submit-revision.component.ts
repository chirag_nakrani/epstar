import { ApiService } from '../../../common/commonServices/apiservice.service';
import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-submit-revision',
  templateUrl: './submit-revision.component.html',
  styleUrls: ['./submit-revision.component.scss']
})

export class SubmitRevisionComponent implements OnInit, OnChanges {

  @Input() rowData: any = {};
  @Output() returnToView = new EventEmitter();
  public indentEditForm: any = {};
  alert: any = {};
  cumulative: number = 0;
  amountError: string = '';
  reasonsList: any = [];
  public autofillCheck: boolean = false;
  public autoLoadError : boolean = false;
  public autoloadMessage : string;
  datePipe: DatePipe;
  public autoloadClass: string = '';

  constructor(private apiService: ApiService) {
      this.datePipe = new DatePipe('en-US');
  }

  ngOnInit() {
    this.getReasonList();
    this.indentEditForm.total_amount_available = 0;
    this.indentEditForm.available_50_amount = 0;
    this.indentEditForm.available_100_amount = 0;
    this.indentEditForm.available_200_amount = 0;
    this.indentEditForm.available_500_amount = 0;
    this.indentEditForm.available_2000_amount = 0;
  }

  ngOnChanges() {
  }

  getReasonList() {
    this.apiService.post('update_reason_crud/', {
      "operation": "R",
      "payload": {}
      }).subscribe((response: any) => {
      this.reasonsList = response;
    });
  }

  submitIndentEdit() {
    if ( this.cumulative > this.indentEditForm.total_amount_available ) {
      this.alert.text = 'Cumulative available amount can\'t be more than total available amount';
      this.alert.type = 'Error';
      this.alert.class = 'danger';
      this.alert.isvisible = true;
      return;
    } else if ( this.cumulative > this.indentEditForm.total_amount_available ) {
      this.alert.text = 'Cumulative available amount can\'t be less than total available amount';
      this.alert.type = 'Error';
      this.alert.class = 'danger';
      this.alert.isvisible = true;
      return;
    }
    this.indentEditForm.available_50_amount = 0;
    this.indentEditForm.is_withdrawal_done = 0;
    this.indentEditForm.available_100_amount = this.sanitizeCurrency(this.indentEditForm.available_100_amount);
    this.indentEditForm.available_200_amount = this.sanitizeCurrency(this.indentEditForm.available_200_amount);
    this.indentEditForm.available_500_amount = this.sanitizeCurrency(this.indentEditForm.available_500_amount);
    this.indentEditForm.available_2000_amount = this.sanitizeCurrency(this.indentEditForm.available_2000_amount);
    this.indentEditForm.total_amount_available = this.sanitizeCurrency(this.indentEditForm.total_amount_available);
    this.indentEditForm.indent_order_number = this.rowData.indent_order_number;
    this.indentEditForm.indent_short_code = this.rowData.indent_short_code;
    this.indentEditForm.project_id = this.rowData.project_id;
    this.indentEditForm.bank = this.rowData.bank;
    this.indentEditForm.feeder_branch = this.rowData.feeder_branch;
    this.indentEditForm.order_date = this.rowData.order_date;
    this.apiService.post('indent/revision/', this.indentEditForm).subscribe((response: any) => {
      this.alert.text = 'Indent Revision Submitted Succesfully For Indent Order No: ' + this.indentEditForm.indent_order_number + '.';
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.isvisible = true;
      this.backToIndent();
    }, (error) => {
      if (error) {
        this.alert.text = 'Indent Revision Failed.';
        this.alert.type = 'Failure';
        this.alert.class = 'danger';
        this.alert.isvisible = true;
        this.backToIndent();
      }
    });
  }

  checkTotal() {
    this.cumulative = this.sanitizeCurrency(this.indentEditForm.available_100_amount) +
                      this.sanitizeCurrency(this.indentEditForm.available_200_amount) +
                      this.sanitizeCurrency(this.indentEditForm.available_500_amount) +
                      this.sanitizeCurrency(this.indentEditForm.available_2000_amount);
    if ( this.cumulative == this.sanitizeCurrency(this.indentEditForm.total_amount_available) ) {
      this.amountError = 'Available amount to allocate ₹0';
    } else if ( this.cumulative > this.sanitizeCurrency(this.indentEditForm.total_amount_available) ) {
      this.amountError = 'Denomination wise cumulative amount can not be more than total available amount';
    } else {
      this.amountError = 'Available amount to allocate ₹' + (this.indentEditForm.total_amount_available - this.cumulative);
    }
  }

  sanitizeCurrency(value: any):number {
    return +value;
  }

  backToIndent() {
    this.returnToView.emit({'view': true, 'alert': this.alert});
  }

  autofillInputs() {
    if(this.autofillCheck){
      const params = {
       'menu' : 'Atm_Revision_Cash_Availability',
          'filter_data': {
            'record_status':'Active',
            'project_id':this.rowData.project_id,
            'bank_code':this.rowData.bank,
            'feeder_branch_code':this.rowData.feeder_branch,
            'for_date':  this.datePipe.transform(this.rowData.order_date, 'yyyy-MM-dd')
          }
      };
      this.apiService.post("viewmaster/?page=1&page_size=100", params).subscribe((response: any) => {
          if(response.results.length > 0) {
            this.autoLoadError = false;
            this.autoloadMessage = '';
            response.results.forEach( x => this.indentEditForm.available_100_amount = (+this.indentEditForm.available_100_amount +  +x.denomination_100) ) ;
            response.results.forEach( x => this.indentEditForm.available_200_amount = (+this.indentEditForm.available_200_amount +  +x.denomination_200) ) ;
            response.results.forEach( x => this.indentEditForm.available_500_amount = (+this.indentEditForm.available_500_amount +  +x.denomination_500) ) ;
            response.results.forEach( x => this.indentEditForm.available_2000_amount  = (+this.indentEditForm.available_2000_amount +  +x.denomination_2000) ) ;
            response.results.forEach( x => this.indentEditForm.total_amount_available = (+this.indentEditForm.total_amount_available +  +x.total_amount  ) ) ;
          }else{
            this.autoloadClass = 'text-warning';
            this.autoloadMessage = 'Warning!!! No data available for the currenct indent. Please upload the data and try again.';
            this.autoLoadError = true;
            this.indentEditForm.available_100_amount = 0;
            this.indentEditForm.available_200_amount = 0;
            this.indentEditForm.available_500_amount = 0;
            this.indentEditForm.available_2000_amount = 0;
            this.indentEditForm.total_amount_available = 0;
          }
      },(error) => {
          this.autoloadClass = 'text-danger'
          this.autoloadMessage = 'Sorry!!! We are not able to fetch data right now. Please contact system admin.'
          this.autoLoadError = true;
      });

    }else{
        this.indentEditForm.available_100_amount = 0;
        this.indentEditForm.available_200_amount = 0;
        this.indentEditForm.available_500_amount = 0;
        this.indentEditForm.available_2000_amount = 0;
        this.indentEditForm.total_amount_available = 0;
        this.autoLoadError = false;
        this.autoloadMessage = '';
    }

  }

}
