import { Component, OnInit , TemplateRef, AfterViewInit } from '@angular/core';
import {ViewChild, ElementRef } from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Page} from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { DomSanitizer } from '@angular/platform-browser';
import * as FileSaver from 'file-saver';
import { SessionStorageService } from 'ngx-store';
// import { DatatableComponent, ColumnMode } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-indents',
  templateUrl: './indents.component.html',
  styleUrls: ['./indents.component.scss']
})
export class IndentsComponent implements OnInit {

  modalRef: BsModalRef;
  alert: any = {};
  page = new Page();
  selected = [];
  selectedids = [];
  previewPage = new Page();
  public uploadData: any;
  uploadForm: FormGroup;
  submitted = false;
  temp = [];
  previewData: any;
  fileName: string;
  datePipe: DatePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  public detailsColList: any;
  rows: any;
  prevRows: any;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  public editRow: any;
  public currentIndent: any;
  public viewIndent = true;
  public rowData: any;
  blob: Blob;
  iframeSrc: any;
  blobUrl: string;
  public permissionList: any = [];
  public projectBankList: any;
  public bankList: any = [];
  public feederList: any = [];
  public filterData: any = {};
  public fileType = 'indent';
  public message: any = [{}];
  // public isDataLoaded: boolean;
  private pagesizeList: any = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  private statusList = ['All', 'Active', 'History'];
  public isError: boolean = false;
  public config: any;
  public feederValue: any = [];
  public pageSizeSelect: any = 20;
  public prevClassList: any = '{"0":"modal-dialog","1":"preview-modal","2":"modal-lg"}';
  public isFirst: boolean = true;
  public isExpanded = false;
  public purposeList = ['','Add Cash','EOD'];

  @ViewChild('viewPdfDetailsModal') viewPdfDetailsModal: ElementRef;
  @ViewChild('viewDetailsModal') viewDetailsModal: ElementRef;
  public filterDate: Date = null;
  // @ViewChild('viewDataModal') viewDataModal: DatatableComponent;
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sanitizer: DomSanitizer, private sessionStorageService: SessionStorageService) {
    this.datePipe = new DatePipe('en-US');
  }

  ngOnInit() {

    if(this.sessionStorageService.get('permissionList') != null)
      this.permissionList = this.sessionStorageService.get('permissionList');
    this.filterData.record_status = 'Active';
    this.filterDate = new Date();
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.getViewCol();
    this.populateSelectColumn()
    this.allDataActive(0);
    this.dropdownsForProjectBank();
    this.config = {
      displayKey: 'feeder', //if objects array passed which key to be displayed defaults to description
      search: true, //true/false for the search functionlity defaults to false,
      height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
      placeholder: 'Select', // text to be displayed when no item is selected defaults to Select,
      noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
      searchPlaceholder: 'Search', // label thats displayed in search input,
      searchOnKey: 'feeder' // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
    };
  }

  // ngAfterViewInit() {
  //   this.isDataLoaded = true;
  //   this.viewDataModal.columnMode = ColumnMode.force;
  // }
// ------------------start of code for all select column -----------

viewColumns: Object = {};
tempRows: any = [];
filtersForAllView: any = {
  'record_status': 'Active'
};
viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
selectedColumn: any = [];
columnChange: any = [];
populateSelectColumn() {
  this.viewColumnAll.filter( (col) => {
    this.selectedColumn.push(col.prop);
  });
}
columnChangefn() {
this.viewColumn = this.viewColumnAll.filter((col) => {
  return this.selectedColumn.includes(col.prop);
});
// }
console.log('col change', this.selectedColumn, this.viewColumn);
}
// ---------------- end  of code for all select column -----------
// --------------------search function ---------------
viewSearch(event) {
  console.log('updateing');
  const val = event.target.value.toLowerCase();
  const temp = this.tempRows.filter(function(d) {
    const search_indent_order_number = d.indent_order_number.toLowerCase().indexOf(val) !== -1;
    const search_feeder_branch = d.feeder_branch.toLowerCase().indexOf(val) !== -1;
    const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
    return search_indent_order_number || search_feeder_branch || search_project_id || !val;
  });
  this.rows = temp;
}
//----------- end of serch function ---------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.columnConfigMasterData);
    this.colList = this.columnConfig.indentListColumnList.filter(x => x.isDisplayColumn ).map((x) => {
      return {'name': x.display_column_name, 'prop': x.table_column, 'dataType': x.dataType};
    });
    this.viewColumnAll = this.colList;

    this.detailsColList = [];
    this.detailsColList = this.columnConfig.indentDetailsColumnList.filter(x => x.isDisplayColumn ).map((x) => {
      return {'name': x.display_column_name, 'prop': x.table_column, 'dataType': x.dataType, 'isEditable': x.isEditable, 'width': x.width, 'bg': x.background};
    });

  }

  sanitizeCurrency(value: any){
    return +value;
  }

  expandModal(e) {
    if(JSON.stringify(e.currentTarget.parentElement.parentElement.parentElement.parentElement.classList) == this.prevClassList){
      e.currentTarget.parentElement.parentElement.parentElement.parentElement.classList.remove("modal-dialog","preview-modal","modal-lg");
      e.currentTarget.parentElement.parentElement.parentElement.parentElement.classList.add("expanded-modal");
      e.currentTarget.parentElement.parentElement.parentElement.classList.add("fullVh");
      window.dispatchEvent(new Event('resize'));
    }else if(JSON.stringify(e.currentTarget.parentElement.parentElement.parentElement.parentElement.classList) == '{"0":"expanded-modal"}'){
      e.currentTarget.parentElement.parentElement.parentElement.parentElement.classList.add("modal-dialog","preview-modal","modal-lg");
      e.currentTarget.parentElement.parentElement.parentElement.parentElement.classList.remove("expanded-modal");
      e.currentTarget.parentElement.parentElement.parentElement.classList.remove("fullVh");
      window.dispatchEvent(new Event('resize'));
    }

  }

  changeTotal(e: Event, rowIndex: number, prop: string) {
      if(e != null) {
          this.prevRows[rowIndex].total = this.sanitizeCurrency(this.prevRows[rowIndex].loading_amount_100)+
                                          this.sanitizeCurrency(this.prevRows[rowIndex].loading_amount_200)+
                                          this.sanitizeCurrency(this.prevRows[rowIndex].loading_amount_500)+
                                          this.sanitizeCurrency(this.prevRows[rowIndex].loading_amount_2000);

          let sum_total = 0;
          this.prevRows.forEach( x => {
            sum_total += this.sanitizeCurrency(x.total);
          });
          this.currentIndent.total_atm_loading_amount = sum_total;
          this.message[rowIndex] = {};

          // if ( prop.indexOf('50') != -1 ) {
          //   if (this.prevRows[rowIndex].loading_amount_50 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_50) {
          //     if(this.message[rowIndex] == undefined)
          //     this.message[rowIndex]={};
          //     this.message[rowIndex][prop] = 'Loading amount Can not be more than total capacity. Total capacity : '+this.prevRows[rowIndex].distribution_details.total_capacity_amount_50;
          //     this.isError = true;
          //   } else {
          //     if(this.message[rowIndex] == undefined)
          //     this.message[rowIndex] = {};
          //     this.message[rowIndex][prop] = '';
          //     this.isError = false;
          //   }
          //   let sum_50Deno = 0;
          //   this.prevRows.forEach( x => {
          //     sum_50Deno += x.loading_amount_50;
          //   });
          //   this.currentIndent.total_atm_loading_amount_50 = sum_50Deno;
          // } else

          if (prop.indexOf('100') != -1) {
            if ((+this.prevRows[rowIndex].loading_amount_100 < 100 || +this.prevRows[rowIndex].loading_amount_100 % 100 != 0 || +this.prevRows[rowIndex].loading_amount_100 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_100) && (this.prevRows[rowIndex].loading_amount_100 != 0 && this.prevRows[rowIndex].loading_amount_100 != '')) {
              this.message[rowIndex][prop] = '';
              if (+this.prevRows[rowIndex].loading_amount_100 < 100 || +this.prevRows[rowIndex].loading_amount_100 % 100 != 0 ) {
                this.message[rowIndex][prop] = 'Amount needs to be multiple of 100. '
              }
              if (+this.prevRows[rowIndex].loading_amount_100 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_100) {
                this.message[rowIndex][prop] = this.message[rowIndex][prop]+'Loading amount Can not be more than total capacity. Total capacity : '+this.prevRows[rowIndex].distribution_details.total_capacity_amount_100;
              }
              this.isError = true;

            } else {
              if(this.message[rowIndex] == undefined)
              this.message[rowIndex] = {};
              this.message[rowIndex][prop] = '';
              this.isError = false;
            }
            let sum_100Deno = 0;
            this.prevRows.forEach( x => {
              sum_100Deno += this.sanitizeCurrency(x.loading_amount_100);
            });
            this.currentIndent.total_atm_loading_amount_100 = sum_100Deno;
          } else if (prop.indexOf('200') != -1 && prop.indexOf('2000') == -1) {
            if ((+this.prevRows[rowIndex].loading_amount_200 < 200 || +this.prevRows[rowIndex].loading_amount_200 % 200 != 0 || +this.prevRows[rowIndex].loading_amount_200 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_200) && (+this.prevRows[rowIndex].loading_amount_200 != 0 && this.prevRows[rowIndex].loading_amount_200 != null)) {
              this.message[rowIndex][prop] = '';
              if (+this.prevRows[rowIndex].loading_amount_200 < 200 || +this.prevRows[rowIndex].loading_amount_200 % 200 != 0 ) {
                this.message[rowIndex][prop] = 'Amount needs to be multiple of 200. '
              }
              if (+this.prevRows[rowIndex].loading_amount_200 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_200) {
                this.message[rowIndex][prop] = this.message[rowIndex][prop]+'Loading amount Can not be more than total capacity. Total capacity : '+this.prevRows[rowIndex].distribution_details.total_capacity_amount_200;
              }
              this.isError = true;
            } else {
              if(this.message[rowIndex] == undefined)
              this.message[rowIndex] = {};
              this.message[rowIndex][prop] = '';
              this.isError = false;
            }
            let sum_200Deno = 0;
            this.prevRows.forEach( x => {
              sum_200Deno += this.sanitizeCurrency(x.loading_amount_200);
            });
            this.currentIndent.total_atm_loading_amount_200 = sum_200Deno;
          } else if (prop.indexOf('500') != -1) {
            if ((+this.prevRows[rowIndex].loading_amount_500 < 500 || +this.prevRows[rowIndex].loading_amount_500 % 500 != 0 || +this.prevRows[rowIndex].loading_amount_500 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_500) && (this.prevRows[rowIndex].loading_amount_500 != 0 && this.prevRows[rowIndex].loading_amount_500 != null)) {
              this.message[rowIndex][prop] = '';
              if (+this.prevRows[rowIndex].loading_amount_500 < 500 || +this.prevRows[rowIndex].loading_amount_500 % 500 != 0 ) {
                this.message[rowIndex][prop] = 'Amount needs to be multiple of 500. '
              }
              if (+this.prevRows[rowIndex].loading_amount_500 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_500) {
                this.message[rowIndex][prop] = this.message[rowIndex][prop]+'Loading amount Can not be more than total capacity. Total capacity : '+this.prevRows[rowIndex].distribution_details.total_capacity_amount_500;
              }
              this.isError = true;
            } else {
              if(this.message[rowIndex] == undefined)
              this.message[rowIndex] = {};
              this.message[rowIndex][prop] = '';
              this.isError = false;
            }
            let sum_500Deno = 0;
            this.prevRows.forEach( x => {
              sum_500Deno += this.sanitizeCurrency(x.loading_amount_500);
            });
            this.currentIndent.total_atm_loading_amount_500 = sum_500Deno;
          } else if (prop.indexOf('2000') != -1) {
            if ((+this.prevRows[rowIndex].loading_amount_2000 < 2000 || +this.prevRows[rowIndex].loading_amount_2000 % 2000 != 0 || +this.prevRows[rowIndex].loading_amount_2000 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_2000) && (this.prevRows[rowIndex].loading_amount_2000 != 0 && this.prevRows[rowIndex].loading_amount_2000 != null)) {
                this.message[rowIndex][prop] = '';
              if (+this.prevRows[rowIndex].loading_amount_2000 < 2000 || +this.prevRows[rowIndex].loading_amount_2000 % 2000 != 0 ) {
                this.message[rowIndex][prop] = 'Amount needs to be multiple of 2000. '
              }
              if (+this.prevRows[rowIndex].loading_amount_2000 > this.prevRows[rowIndex].distribution_details.total_capacity_amount_2000) {
                this.message[rowIndex][prop] = this.message[rowIndex][prop]+'Loading amount Can not be more than total capacity. Total capacity : '+this.prevRows[rowIndex].distribution_details.total_capacity_amount_2000;
              }
              this.isError = true;
            } else {
              if(this.message[rowIndex] == undefined)
              this.message[rowIndex] = {};
              this.message[rowIndex][prop] = '';
              this.isError = false;
            }
            let sum_2000Deno = 0;
            this.prevRows.forEach( x => {
              sum_2000Deno += this.sanitizeCurrency(x.loading_amount_2000);
            });
            this.currentIndent.total_atm_loading_amount_2000 = sum_2000Deno;
          }
        } else {
          this.message = [];
          this.isError = false;
        }
  }

  dropdownsForProjectBank() {
    const params = {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    };
    this.apiService.post('bankinfo/', params).subscribe((data: any) => {
       this.bankList = [];
       this.projectBankList = data.data;
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }

  resetFilter() {
    this.filterData = {};
    this.feederValue = [];
    this.bankList = [];
    this.feederList = [];
    this.filterDate = null
  }


  getBankList() {
    this.bankList = [];
    this.bankList = this.projectBankList.filter( x => x.project_id == this.filterData.project_id )[0].bank;
    this.bankList = this.removeDuplicate(this.bankList, "bank_id");
  }

  getFeeder() {
    this.feederList = [];
    const tempList = [];
    this.bankList.filter( x => x.bank_id == this.filterData.bank)[0].feeder.forEach( z => {
      tempList.push(z);
    });
    this.removeDuplicate(tempList,"feeder").forEach(x => {
      this.feederList.push(x.feeder)
    });
  }

  removeDuplicate (arr: any, comp: string) {

    const finalArr = arr
                  .map(el => el[comp]) // store the keys of the unique objects
                  .map((el, index, a) => a.indexOf(el) === index && index) // eliminate the dead keys & store unique objects
                  .filter(el => arr[el])
                  .map(el => arr[el]);
     return finalArr;

  }


  allDataActive(pageNumber: any) {
    this.sanitizeFilters();
    const params =  {
          'indent_type' : 'indent',
          'filters': {}
      };
    if (this.filterData != undefined || this.filterData != {}) {
      const filter = this.filterData;
      if (this.filterDate != null) {
        filter.order_date = this.datePipe.transform(this.filterDate, 'yyyy-MM-dd');
      }
      params.filters = filter;
    }
    this.page.pageNumber = pageNumber;
    this.currentPage = pageNumber + 1;

    this.apiService.post('indent/list/?page=' + (this.currentPage) + '&' + 'page_size=' + this.page.size, params).subscribe((response: any) => {
       this.rows = response.results;
       this.rows = [...this.rows];
       this.page.totalElements = response.count;
       this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
       this.tempRows = [...this.rows];
     }, (error: any) => {
        console.log('Error', error.error);
     });
  }

  sanitizeFilters(){

      for(let key in this.filterData){
        if(this.filterData[key] == null || this.filterData[key] == '' || this.filterData[key] == 'All'){
          delete this.filterData[key];
        }
      }

  }

  getIndentDetails(row: any) {
    this.currentIndent = row;
    const params = {
      'indent_order_no' : row.indent_order_number,
      'filters': {
        'record_status': 'Active'
      }
    };
    this.apiService.post('indent/detail/', params).subscribe((response: any) => {
       this.currentIndent = response;
       this.prevRows = JSON.parse(JSON.stringify(this.currentIndent.indent_detail));
       this.prevRows.forEach( x => {
         x.total_morning_balance = x.distribution_details.total_morning_balance;
         x.morning_balance_100 = x.distribution_details.morning_balance_100;
         x.morning_balance_200 = x.distribution_details.morning_balance_200;
         x.morning_balance_500 = x.distribution_details.morning_balance_500;
         x.morning_balance_2000 = x.distribution_details.morning_balance_2000;
         x.avgdispense = x.distribution_details.avgdispense;
       });
       this.prevRows = [...this.prevRows];
       this.openDetailsModal();
     }, (error: any) => {
        console.log('Error', error.error);
        this.alert.isvisible = true;
        this.alert.class = 'danger';
        this.alert.type = 'Error!';
        this.alert.text = 'Indent details not available please try after sometime.';
     });
  }

  getIndentPdf(row: any) {

      const params = {'indent_order_number' : row.indent_order_number};
      this.apiService.post('view_pdf/', params).subscribe((response: any) => {

         const byteNumbers = this._base64ToArrayBuffer(response);
         this.blob = new Blob(byteNumbers, { type: 'application/pdf' });
         this.fileName = row.indent_order_number.replace(/(\s|\\)/g, '_');
         this.showIndentPdf();

       }, (error: any) => {
         this.alert.isvisible = true;
         this.alert.class = 'danger';
         this.alert.type = 'Error';
         this.alert.text = "Pdf not available !";
       });

  }

  _base64ToArrayBuffer(base64):Array<any> {
      let binary_string =  window.atob(base64);
      const len=binary_string.length;
      let bytes = new Uint8Array( len );
      for (var i = 0; i < len; i++){
          bytes[i] = binary_string.charCodeAt(i);
      }
      return [bytes.buffer];
  }

  submitIndentDetailsEdit(is_save_or_submit) {
    if (is_save_or_submit == 'submit'){
      this.currentIndent.is_generate_pdf = 0
    }
    else{
     this.currentIndent.is_generate_pdf = 1
    }

    this.currentIndent.total_bank_withdrawal_amount = this.currentIndent.total_atm_loading_amount;
    this.currentIndent.total_bank_withdrawal_amount_100 = this.currentIndent.total_atm_loading_amount_100;
    this.currentIndent.total_bank_withdrawal_amount_200 = this.currentIndent.total_atm_loading_amount_200;
    this.currentIndent.total_bank_withdrawal_amount_500 = this.currentIndent.total_atm_loading_amount_500;
    this.currentIndent.total_bank_withdrawal_amount_2000 = this.currentIndent.total_atm_loading_amount_2000;
    this.prevRows.forEach( x => {
      x.total = this.sanitizeCurrency(x.total);
      x.loading_amount_100 = this.sanitizeCurrency(x.loading_amount_100);
      x.loading_amount_200 = this.sanitizeCurrency(x.loading_amount_200);
      x.loading_amount_500 = this.sanitizeCurrency(x.loading_amount_500);
      x.loading_amount_2000 = this.sanitizeCurrency(x.loading_amount_2000);
      delete x.total_morning_balance;
      delete x.morning_balance_100;
      delete x.morning_balance_200;
      delete x.morning_balance_500;
      delete x.morning_balance_2000;
      delete x.avgdispense;
      console.log("indent details",x);
    });
    this.currentIndent.indent_detail = this.prevRows;
    this.apiService.post('indent/edit/', this.currentIndent).subscribe((response: any) => {
      this.alert.text = response.status_desc;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.isvisible = true;
      this.isError = false;
    }, (error) => {
      if (error) {
        this.alert.text = error.error.status_desc;
        this.alert.type = 'Failure';
        this.alert.class = 'danger';
        this.alert.isvisible = true;
        this.isError = false;
      }
    });
  }

  viewDetailSearch(event) {

          const val = event.target.value.toLowerCase();

          const temp = this.currentIndent.indent_detail.filter( x => {
              const search_atm_id = x.atm_id.toLowerCase().indexOf(val) !== -1;
             return search_atm_id || !val ;
             });

          this.prevRows = temp;
  }


  showIndentPdf() {

    this.blobUrl = URL.createObjectURL(this.blob);
    this.modalRef = this.modalService.show(this.viewPdfDetailsModal, Object.assign({}, { class: 'modal-lg' }));
    this.iframeSrc = this.sanitizer.bypassSecurityTrustResourceUrl(this.blobUrl);
  }

  downloadPdf() {
    FileSaver.saveAs(this.blob, this.fileName + '.pdf');
  }

  openDetailsModal() {
    this.modalRef = this.modalService.show(this.viewDetailsModal, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }
  returnToView(event: any) {
    this.viewIndent = event.view;
    this.alert = event.alert;
    this.allDataActive(0);
  }
  editIndent(row: any) {
    this.rowData = row;
    this.viewIndent = false;
  }

  resetIndentDetailsEdit() {
    this.currentIndent.total_atm_loading_amount = 0 ;
    this.currentIndent.total_atm_loading_amount_100 = 0 ;
    this.currentIndent.total_atm_loading_amount_200 = 0 ;
    this.currentIndent.total_atm_loading_amount_500 = 0 ;
    this.currentIndent.total_atm_loading_amount_2000 = 0 ;
    this.prevRows.forEach( x => {
      x.loading_amount_100 = 0;
      x.loading_amount_200 = 0;
      x.loading_amount_500 = 0;
      x.loading_amount_2000 = 0;
      x.total = 0;
      x.purpose = '';
    });
    this.changeTotal(null,null,null);
    this.prevRows = [...this.prevRows];
  }
}
