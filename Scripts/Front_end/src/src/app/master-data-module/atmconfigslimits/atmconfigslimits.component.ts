import { Component, OnInit, TemplateRef } from '@angular/core';
import { ViewChild, ElementRef } from '@angular/core';

import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Page } from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';


declare var jquery: any;   // not required
declare var $: any;   // not required

@Component({
  selector: 'app-atmconfigslimits',
  templateUrl: './atmconfigslimits.component.html',
  styleUrls: ['./atmconfigslimits.component.scss']
})
export class AtmconfigslimitsComponent implements OnInit {
  @ViewChild('fileInput')
  myInputVariable: ElementRef;
  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  fileType = 'ATMCNFGLIMIT';
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  previewPage = new Page();

  public uploadData: any;
  uploadForm: FormGroup;
  submitted = false;
  temp = [];
  selected = [];
  selectedids = [];
  datePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  diRows: any;
  viewColumnAll: any = {};
  previewData: any;
  public currentPage = 1;
  public isEdit = [];
  public editRow: any;
  public currentPageSize: number;
  public isMultiEdit: boolean;
  public permissionList: any=[];
  viewColumns: any = {};
  submitbuttonval:boolean;
  rows: any = [];
  tempRows: any = [];
  filtersForAllView: any = {};
  viewColumn: any = {};
  selectedColumn: any = [];
  columnChange: any = [];
  public date_range: Array<Date> = [];
  public statusList: Array<string>;
  public bankFilterDropDown: any = [];
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig,private sessionStorageService: SessionStorageService) {
    this.datePipe = new DatePipe('en-US');
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.dropdownValue = [];


    this.filterDropDownValue = {};
    this.filtersForAllView.record_status = 'Active';
    this.statusList = ['All', 'Active', 'Approval Pending', 'History', 'Rejected', 'Uploaded'];
  }


  ngOnInit() {
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    this.uploadData.date = new Date();
    this.uploadData.time = '12:00';
    this.uploadData.file;
    this.populateDropDown();
    this.setDataInformationCurrent();
    this.getViewCol();
    this.isMultiEdit = false;
    this.submitbuttonval=true;
    this.allDataActive(0);
    this.uploadForm = this.formBuilder.group({
      bank_code: ['', Validators.required],
      cra: ['', Validators.required],
      project: ['', Validators.required],
      date: [this.uploadData.date],
      time: [this.uploadData.time],
    });

    this.populateSelectColumn();

  }


  // ----------------------- upload file ------------------------------------

  // convenience getter for easy access to form fields
  get f() { return this.uploadForm.controls; }

  openTimeSelector() {
    console.log('time picker opened');
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.uploadData.time = time;
      console.log('time picker closed');

    });
  }


  onFileChanged(event) {

    //const file = event.target.files[0]
    this.uploadData.file = event.target.files[0];
    console.log("******************",this.myInputVariable.nativeElement.files);
    this.myInputVariable.nativeElement.value = "";
    console.log(this.myInputVariable.nativeElement.files);
  }


  uploadDatafn() {
    // stop here if form is invalid
    if (this.uploadData.invalid) {
      return;
    }
    console.log('upload clicked----------', this.uploadData);
    // this.previewColList = this.columnConfig.columnConfig.filter(x => x.datafor == this.fileType && x.bankcode == this.uploadData.bank_code.bank_id);
    this.submitted = true;



    // console.log("upload clicked------------",this.uploadData);
    const hours_min = this.uploadData.time.split(':');
    this.uploadData.upload_datetime = new Date(this.uploadData.date);
    this.uploadData.upload_datetime.setHours(hours_min[0], hours_min[1]);
    const date = this.datePipe.transform(this.uploadData.upload_datetime, 'yyyy-MM-dd HH:mm:ss');
    const fileData = new FormData();
    //uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
    fileData.append('file', this.uploadData.file, this.uploadData.file.name);
    // fileData.append('bank_code', this.uploadData.bank_code.bank_id);
    // fileData.append('project_id', this.uploadData.project.project_id);
    fileData.append('project_id', this.uploadData.project);

    // fileData.append('project_id', "MOF");
    // fileData.append('cra', this.uploadData.cra);
    fileData.append('file_type', this.fileType);
    // fileData.append('upload_datatime', date);
    console.log('upload clicked formdata', fileData);
    this.apiService.uploadFile('uploadmaster/', fileData).subscribe(event => {
      console.log(event); // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);

        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        // this.alert.class="danger"
        this.alert.text = ' ' + response.body.status_desc;
        this.alert.class = 'success';
        $('#m_modal_6').modal('hide');
        this.setDataInformationCurrent();

      }
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#m_modal_6').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));

    this.clearUploadFormData();

  }
  clearUploadFormData() {
    this.uploadForm.reset();
    this.uploadData.file =true;
console.log("uploadData.fileuploadData.fileuploadData.file",this.uploadData.file)

  }
  // ------------------------------ upload end -----------------------------------

// start of code for all select column -----------



populateSelectColumn() {
  this.viewColumnAll.filter( (col) => {
    this.selectedColumn.push(col.prop);
  });
}
resetFilter() {
  this.filtersForAllView = {};
  this.date_range = [];
}

columnChangefn() {
this.viewColumn = this.viewColumnAll.filter((col) => {
  return this.selectedColumn.includes(col.prop);
});
// }
console.log('col change', this.selectedColumn, this.viewColumn);
}
// end  of code for all select column -----------
// --------------------search function ---------------

  viewSearch(event) {
    console.log('updateing');
    const val = event.target.value.toLowerCase();

    const temp = this.tempRows.filter(function(d) {
      const search_atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
      const search_site_code = d.site_code.toLowerCase().indexOf(val) !== -1;
      const search_bank_code = d.bank_code.toLowerCase().indexOf(val) !== -1;
      return search_atm_id || search_site_code || search_bank_code || !val;
    });

    this.rows = temp;
  }

// --------------------end of search function ---------------
  updateViewAll() {
    console.log('updarte called');
    console.log('filter parameter', this.filtersForAllView);
    this.allDataActive(0);
    $('#modalfilter').modal('hide');
  }

  setPage(pageInfo) {
    // console.log("page clicked");
    console.log('page clicked', pageInfo);

    this.page.pageNumber = pageInfo.offset;
    this.apiService.post('view/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size, {
      'menu': this.fileType,
      'filter_data': this.filtersForAllView
      // {
      //   // "datafor_date_time":"",
      //   // "datafor_date_time": "2018-10-12T08:00:00Z",
      // //  "atm_id":"APN0712A"
      // }
    }
    ).subscribe(data => {
      console.log('-------get all');
      console.log(data); // handle event here
      const response: any = data;
      //   this.page = pagedData.page;
      // this.temp = [...data];

      // push our inital complete list
      this.rows = response.results;
      this.tempRows = response.results;
      this.page.totalElements = response.count;
    });
  }



  // ---------end of  view all data ----------------------



  // --------------- drop down value --------------
  populateDropDown() {


    // --------- for upload dropdown
    this.apiService.post('bankinfo/', {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      console.log('-------dropdown', response.data);
      this.dropdownValue = response.data;
      console.log(this.dropdownValue); // handle event here
      this.getAllBankList();
    }, (error) => {
      console.log('error in dropdown value', error);

    });
    // --------------- end of upload dropdown

    // filter dropdown
    this.apiService.post('bankFeederCommonInfo/', {
      'file_type': 'CBR',
      'common_bankfeeder_info': 'common_bankfeeder_info'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      this.filterDropDownValue = response;
      console.log('------- filter dropdown ', this.filterDropDownValue);

      //  this.dropdownValue=[response.data];
      //  console.log(this.dropdownValue); // handle event here

    }, (error) => {
      console.log('error in filter dropdown value', error);

    });
    // end of filter dropdown
  }

  getAllBankList() {
    this.bankFilterDropDown = [];
    this.dropdownValue.forEach( x => {
      x.bank.forEach( y => {
        this.bankFilterDropDown.push(y);
      });
    });
    this.bankFilterDropDown = this.removeDuplicate(this.bankFilterDropDown, "bank_id");
  }

  removeDuplicate (arr: any, comp: string) {

    const finalArr = arr
                  .map(el => el[comp]) // store the keys of the unique objects
                  .map((el, index, a) => a.indexOf(el) === index && index) // eliminate the dead keys & store unique objects
                  .filter(el => arr[el])
                  .map(el => arr[el]);
     return finalArr;

  }
  // --------------- end of drop down value --------------


  clicked(data) {
    console.log(data);
  }



  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }
  // ------------------------- code for data information ----------------------
  setDataInformationCurrent() {
    const params = {
      'menu': this.fileType,
      'record_status': ['Approval Pending']
    };

    this.getDataInfo(params);
  }
  setDataInformationPrevious() {
    const params = {
      'menu': this.fileType,
      'record_status': ['Active', 'Rejected']
    };

    this.getDataInfo(params);
  }
  getDataInfo(params: any) {
    this.apiService.post('getmasterdata/', params).subscribe((data: any) => {
      const tempRow = data.data;
      this.diRows = tempRow;
      this.diRows = [...this.diRows];

    }, (error) => {
      console.log('Data Info Error', error.error.status_text);
    });
  }

  // -------------------------End of code for data information ----------------------

  //------------------------code for view file in modal for current and previous tab ----------------------------------
  //common section
  viewMasterData(params: any, offSet: number) {
    this.page.pageNumber = offSet;
    this.apiService.post('viewmaster/?page=' + (this.currentPage + offSet) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      this.tempRows = [...this.rows];
      this.rows.forEach( (x,index) => {
        this.isEdit[index] = false;
        this.colList.filter( x => x.datatype == 'date').forEach( y => {
          if(x[y.prop] != null){
            x[y.prop] = new Date(x[y.prop]);
          }
        })
      });
      console.log('-----------common', this.rows);
      this.page.totalElements = response.count;
      // // this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      // console.log('View Error', error.error);
    });
    this.clearSelectArrayfn();
  }
  
  clearSelectArrayfn(){
    console.log("Inside select arrey clr called")
    this.selectedids=[];
    this.selected=[];
  }
  getFileData(offSet: number, status: string) {
    const params = {
      'menu': this.fileType,
      'filter_data': {
        'created_reference_id': this.previewData.created_reference_id,
        'record_status': status
      }
    };
    this.viewMasterData(params, offSet);
  }
  //end of common section
  //to open view file in current tab
  viewFile(rowData: any) {
    ;
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0, rowData.record_status);
    this.modalRef = this.modalService.show(this.previewModalcurrent, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }
  //to open view file in previous tab
  viewFilePrevious(rowData: any) {
    for (let i = 0; i < this.rows.length; i++) {
      this.isEdit[i] = false;
    }
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0, rowData.record_status);
    this.modalRef = this.modalService.show(this.previewModalprevious, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }
  //to Display All data of active status
  allDataActive(offSet: number) {
    if (this.date_range) {
      this.date_range = this.date_range.map( x => {
        return this.datePipe.transform(new Date(x), 'yyyy-MM-dd');
      });
    }
    let params: any;
    for (let key in this.filtersForAllView) {
      if (this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All') {
        delete this.filtersForAllView[key];
      }
    }
    if ( this.date_range.length > 0 ) {
      params = {
      'menu': this.fileType,
        'date_range': this.date_range,
        'filter_data': this.filtersForAllView
      };
    } else {
      params = {
        'menu': this.fileType,
        'filter_data': this.filtersForAllView
    };
    }
    this.viewMasterData(params, offSet);
  }
  //---------------------------------------code for view file in modal for current and previous tab- -------------------------------------------------

  //------------------code for Modal which is approved or rejected for indivisual file -----------------------------
  onSelect({ selected },reqfrom) {
    this.isMultiEdit = true;
    console.log('Select Event', selected, this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    
    if(this.selected.length<1 && reqfrom=='Active'){
      this.submitbuttonval=true;
      this.allDataActive(0);
    }
    
  }
  getApprovedData(params: any) {
    this.apiService.post('ApproveRejectAll/', params).subscribe((response: any) => {
      this.allDataActive(0);
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_desc;
    }, (error) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_desc;
      } else {
        this.alert.text = error.statusText;
      }
    });
  }

  //to approve indivisual row
  approveIndivisualFile() {
    for (let i = 0; i <= this.selected.length - 1; i++) {
      this.selectedids.push(this.selected[i].id);
    }
    const params = {
      'menu': this.fileType,
      'id': this.selectedids.toString(),
      'status': 'Active',
      'created_reference_id': this.previewData.created_reference_id
    };
    this.getApprovedData(params);
  }
  //to reject indivisual row
  rejectIndivisualFile() {
    for (let i = 0; i <= this.selected.length - 1; i++) {
      this.selectedids.push(this.selected[i].id);
    }
    const params = {
      'menu': this.fileType,
      'id': this.selectedids.toString(),
      'status': 'Rejected',
      'created_reference_id': this.previewData.created_reference_id
    };
    this.getApprovedData(params);
  }
  //to approve complete file
  approveFile(row: any) {
    const params = {
      'menu': this.fileType,
      'id': '0',
      'created_reference_id': row.created_reference_id,
      'status': 'Active'
    };
    this.getApprovedData(params);
  }
  //to reject complete file
  rejectFile(row: any) {
    const params = {
      'menu': this.fileType,
      'id': '0',
      'status': 'Rejected',
      'created_reference_id': row.created_reference_id
    };
    this.getApprovedData(params);
  }
  //----------------end of code for Modal which is approved or rejected for--------------
  // ------------------------- to display column data in modal and in All Data----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.columnConfigatmconfigLimit);
    this.columnConfig.columnConfigatmconfigLimit.forEach(x => {
      if (x.file_type === 'ATMCNFGLIMIT') {
        this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column, 'datatype': x.datatype, 'isEditable': x.isEditable });
      }
      console.log('xxxxxxxxxxx', x);
    });

    this.viewColumnAll = this.colList;
    this.viewColumn = this.viewColumnAll;
    console.log('viewColumn', this.viewColumnAll);
  }
  // ------------------------- to display column data in modal and in All Data----------------------
  //-------------code for edit all data section---------------------------
  //indivisual edit
  viewEdit(rowIndex: number, row: any) {
    ;
    this.editRow = row;
    this.isEdit[rowIndex] = 'true';
  }

  //multiple Edit
  onMultipleEdit() {
    if(this.selected.length>0){
      this.submitbuttonval=false;
      this.rows = this.selected;
      this.rows = [...this.rows];
  
      for (let i = 0; i < this.rows.length; i++) {
        this.isEdit[i] = true;
      }
      console.log('edited row', this.rows);
    }
  }
  //to submit edited data
  updateEdit(params: any) {
    this.apiService.post('updatemaster/', params).subscribe((response: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Edit';
      this.alert.class = 'success';
      this.alert.text = response.data;
      this.setDataInformationCurrent();
      this.selected=[];
      this.allDataActive(0);

    }, (error) => {
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      const error_str: string = error.error.data;
      this.alert.text = error_str;
      this.alert.class = 'danger';
      this.selected=[];
    });
  }

  editSubmit() {
    this.submitbuttonval=true;
    const params = {
      'file_type': this.fileType,
      'update': this.rows
    };
    this.updateEdit(params);

  }
  //-------------End of code for edit all data section---------------------------
}
