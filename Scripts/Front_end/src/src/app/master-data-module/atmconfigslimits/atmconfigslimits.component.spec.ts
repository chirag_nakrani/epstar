import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmconfigslimitsComponent } from './atmconfigslimits.component';

describe('AtmconfigslimitsComponent', () => {
  let component: AtmconfigslimitsComponent;
  let fixture: ComponentFixture<AtmconfigslimitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtmconfigslimitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmconfigslimitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
