import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeederbranchComponent } from './feederbranch.component';

describe('FeederbranchComponent', () => {
  let component: FeederbranchComponent;
  let fixture: ComponentFixture<FeederbranchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeederbranchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeederbranchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
