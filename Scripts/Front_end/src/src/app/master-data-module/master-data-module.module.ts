import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { AtmconfigslimitsComponent } from './atmconfigslimits/atmconfigslimits.component';
import { AtmsComponent } from './atms/atms.component';
import { ConsolidatedmasterdataComponent } from './consolidatedmasterdata/consolidatedmasterdata.component';
import { CraFeasibilityComponent } from './cra-feasibility/cra-feasibility.component';
import { FeederbranchComponent } from './feederbranch/feederbranch.component';

import { CommonImportsModule } from '../common/common-imports.module';

const routes: Routes = [
  { path: 'atmconfigslimits', component: AtmconfigslimitsComponent,
     canActivate: [AuthCheckService] },
  { path: 'atms', component: AtmsComponent,
     canActivate: [AuthCheckService] },
  { path: 'consolidatedmasterdata', component: ConsolidatedmasterdataComponent,
     canActivate: [AuthCheckService] },
  { path: 'cra-feasibility', component: CraFeasibilityComponent,
     canActivate: [AuthCheckService] },
  { path: 'feederbranch', component: FeederbranchComponent,
     canActivate: [AuthCheckService] },

];

@NgModule({
  declarations: [
    AtmconfigslimitsComponent,
    AtmsComponent,
    ConsolidatedmasterdataComponent,
    CraFeasibilityComponent,
    FeederbranchComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class MasterDataModuleModule { }
