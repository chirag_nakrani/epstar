import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraFeasibilityComponent } from './cra-feasibility.component';

describe('CraFeasibilityComponent', () => {
  let component: CraFeasibilityComponent;
  let fixture: ComponentFixture<CraFeasibilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraFeasibilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraFeasibilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
