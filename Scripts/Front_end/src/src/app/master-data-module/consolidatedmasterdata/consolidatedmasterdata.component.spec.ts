import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsolidatedmasterdataComponent } from './consolidatedmasterdata.component';

describe('ConsolidatedmasterdataComponent', () => {
  let component: ConsolidatedmasterdataComponent;
  let fixture: ComponentFixture<ConsolidatedmasterdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsolidatedmasterdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsolidatedmasterdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
