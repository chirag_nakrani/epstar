import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';



declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-consolidatedmasterdata',
  templateUrl: './consolidatedmasterdata.component.html',
  styleUrls: ['./consolidatedmasterdata.component.scss']
})
export class ConsolidatedmasterdataComponent implements OnInit {

  fileType = 'consolidate';
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  previewPage = new Page();
  public uploadData: any;
  uploadForm: FormGroup;
  submitted = false;
  previewData: any;
  datePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  diRows: any;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;

  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig) {
    this.datePipe = new DatePipe('en-US');
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.dropdownValue = [];
    this.filterDropDownValue = {};
  }


  ngOnInit() {

    this.getViewCol();
    this.allDataActive(0);
    this.populateSelectColumn();
  }
  // ----------------------- upload file ------------------------------------
  // convenience getter for easy access to form fields
  get f() {
    return this.uploadForm.controls;
  }
  openTimeSelector() {
    console.log('time picker opened');
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.uploadData.time = time;
      console.log('time picker closed');

    });
  }

  // ------------------------------ upload end -----------------------------------
  // ----------view all data ----------------

  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];
  filtersForAllView: any = {
    'record_status': 'Active'
  };

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    });
    // }
    console.log('col change', this.selectedColumn, this.viewColumn);
  }



  viewSearch(event) {
    console.log('updateing');
    const val = event.target.value.toLowerCase();
    const temp = this.tempRows.filter(function(d) {
      const search_atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
      const search_bank_name = d.bank_name.toLowerCase().indexOf(val) !== -1;
      const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
      return search_atm_id || search_bank_name || search_project_id || !val;
    });

    this.rows = temp;
  }


  updateViewAll() {
    console.log('updarte called');
    console.log('filter parameter', this.filtersForAllView);
    this.setPage({
      offset: 0
    });
    $('#modalfilter').modal('hide');
  }

  setPage(pageInfo) {
    // console.log("page clicked");
    console.log('page clicked', pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.apiService.post('viewmaster/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size, {
      'file_type': this.fileType,
      'filters': this.filtersForAllView
    }).subscribe(data => {
      console.log('-------get all');
      console.log(data); // handle event here
      const response: any = data;
      this.rows = response.results;
      this.tempRows = response.results;
      this.page.totalElements = response.count;
    });
  }
  // ---------end of  view all data ----------------------

  // --------------- drop down value ---------------------
  populateDropDown() {


    // --------- for upload dropdown----------------------
    this.apiService.post('bankinfo/', {
      'file_type': this.fileType,
      'routine_info': 'ROUTINE_DATA_INFO'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      console.log('-------dropdown', response.data);
      this.dropdownValue = [response.data];
      console.log(this.dropdownValue); // handle event here

    }, (error) => {
      console.log('error in dropdown value', error);

    });
    // --------------- end of upload dropdown----------------

    // filter dropdown
    this.apiService.post('bankFeederCommonInfo/', {
      'file_type': this.fileType,
      'common_bankfeeder_info': 'common_bankfeeder_info'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      this.filterDropDownValue = response;
      console.log('------- filter dropdown ', this.filterDropDownValue);
      this.filterDropDownValue.bank = this.removeDuplicate(this.filterDropDownValue.bank, "bank");
      this.filterDropDownValue.feederlist = this.removeDuplicate(this.filterDropDownValue.feederlist, "bank");

      //  this.dropdownValue=[response.data];
      //  console.log(this.dropdownValue); // handle event here

    }, (error) => {
      console.log('error in filter dropdown value', error);

    });
    // end of filter dropdown
  }

  removeDuplicate (arr: any, comp: string) {

    const finalArr = arr
                  .map(el => el[comp]) // store the keys of the unique objects
                  .map((el, index, a) => a.indexOf(el) === index && index) // eliminate the dead keys & store unique objects
                  .filter(el => arr[el])
                  .map(el => arr[el]);
     return finalArr;

  }

  // --------------- end of drop down value -----------------


  clicked(data) {
    console.log(data);
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }


  //------------------------code for view file in modal for current and previous tab----------------------------------
  //common section
  viewMasterData(params: any, offSet: number) {
    this.apiService.post('viewmaster/?page=' + (this.currentPage + offSet) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      for (let i = 0; i < this.rows.length; i++) {
      }
      console.log('-----------common', this.rows);
      this.page.totalElements = response.count;
      // // this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      // console.log('View Error', error.error);
    });
  }
  getFileData(offSet: number, status: string) {
    const params = {
      'menu': this.fileType,
      'filter_data': {
        'created_reference_id': this.previewData.created_reference_id,
        'record_status': status
      }
    };
    this.viewMasterData(params, offSet);
  }
  //end of common sectio

  //to Display All data of active status
  allDataActive(offSet: number) {
    console.log('inside all data active');
    const params = {
      'menu': this.fileType,
      'filter_data': {
        'record_status': 'Active'
      }
    };
    this.viewMasterData(params, offSet);
  }
  //---------------------------------------code for view file in modal for current and previous tab- --------------------------------------
  // ------------------------- to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.ColumnConfigConsolidation);
    this.columnConfig.ColumnConfigConsolidation.forEach(x => {


        this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column, 'datatype': x.datatype });

      // console.log("xxxxxxxxxxx",x);
    });
    this.viewColumnAll = this.colList;
    console.log('viewColumn', this.viewColumnAll);
  }
  // ------------------------- to display column data in modal and in All Data ----------------------
}
