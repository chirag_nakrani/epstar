import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import {DatatableComponent} from '@swimlane/ngx-datatable/src/components/datatable.component';
import { ColumnConfig } from '../../common/shared/columnConfig';
import {ApiService} from '../../common/commonServices/apiservice.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-exceptions',
  templateUrl: './exceptions.component.html',
  styleUrls: ['./exceptions.component.scss']
})
export class ExceptionsComponent implements OnInit {
  title: string;
  columnList: any;
  allException: object[];
  page: any = {};
  selected = [];
  rows = [];
  temp = [];
  alert: any;
  public isViewTicket = false;
  public currentTicketId: number;
  private pagesizeList: any = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  public pageLimit = 20;
    public userList: any;
  @ViewChild('exceptionTable') table;

  constructor (private apiService: ApiService, private columnConfig: ColumnConfig, route: ActivatedRoute) {
    this.title = 'Exception details';
    route.params.subscribe(val => {
      this.isViewTicket = false;
    });
  }

  ngOnInit() {
    this.alert = {};
    this.alert.isvisible = false;
    this.getUser();
    this.getColumnList();
    this.getall();
  }

  getUser() {
    this.apiService.post('users/list/', {}).subscribe(
      (data: any) => {
        this.userList = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }


  getColumnList() {
    this.columnList = this.columnConfig.exceptionsColumnConfig.map((x) => {
        return {'prop': x.table_column, 'name': x.display_column_name};
    });
  }

  getall() {
      const params = {
        'filters': {
          'record_status' : 'Active' ,
        }
      };

      this.apiService.post('ticket/all/', params).subscribe(
        (data: any) => {
           this.rows = data;
           this.temp = data;
        },
        error => {
            console.log('Error', error);
        }
    );
   }

  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }

  add() {
    this.selected.push(this.rows[1], this.rows[3]);
  }

  update() {
    this.selected = [ this.rows[1], this.rows[3] ];
  }

  displayCheck(event) {
    // Business Losic for selected rows
  }

  returnToView(event: any) {
    this.getall();
    this.isViewTicket = event.isView;
    this.alert = event.alert;
  }

  getUserName(userId: any) {
    return this.userList.filter(x => x.id == userId)[0].user_name;
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    // filter our data
    const temp = this.temp.filter((d) => {
      return d.subject.toLowerCase().indexOf(val) !== -1 || !val;
    });
    // update the rows
    this.rows = temp;
  }




}
