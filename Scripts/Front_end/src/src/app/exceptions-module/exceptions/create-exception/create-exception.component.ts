import { Component, OnInit, TemplateRef } from '@angular/core';
import { ApiService } from '../../../common/commonServices/apiservice.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';




@Component({
  selector: 'app-create-exception',
  templateUrl: './create-exception.component.html',
  styleUrls: ['./create-exception.component.scss']
})
export class CreateExceptionComponent implements OnInit {
  creatExcepForm: FormGroup;
  public alert: any = {};

  // modal
  createdModal: BsModalRef;
  catDDList: any;


  constructor(private apiservice: ApiService, private modalService: BsModalService, private router: Router) {
    this.alert.isvisible = false;
  }



  ngOnInit() {
    this.getCategoy();
    this.creatExcepForm = new FormGroup({
      'subject': new FormControl(null, Validators.required),
      'remarks': new FormControl(null, Validators.required),
      'category': new FormControl(null, Validators.required),
      'sub_category': new FormControl(null, Validators.required)
    });
  }



  save() {
    console.log(this.creatExcepForm.value);
    const param = this.creatExcepForm.value;
   this.apiservice.post('ticket/create/', param).subscribe(
     (data) => {
       this.router.navigate(['./exceptions/exceptions']);
       console.log('sucess', data);
     },
     (error) => {
       this.alert.isvisible = true;
       this.alert.class = 'danger';
       this.alert.type = 'Ticket Creaion';
       this.alert.text = error.error.status;
       console.log('error', error);
     }
   );


  }

  getCategoy() {
    this.apiservice.post('ticket/categorymenus/', {}).subscribe((data: any) => {
      this.catDDList = data;
    });
  }

  trackOpt(index: number, opt: any) {
    return opt ? opt.category : undefined;
  }

  // OpenCreateModal(template: TemplateRef<any>) {
  //   this.createdModal = this.modalService.show(template);
  // }


}
