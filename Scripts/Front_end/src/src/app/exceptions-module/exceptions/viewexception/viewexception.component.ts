import { Component, OnInit, TemplateRef, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../common/commonServices/apiservice.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-viewexception',
  templateUrl: './viewexception.component.html',
  styleUrls: ['./viewexception.component.scss']
})
export class ViewexceptionComponent implements OnInit {
  public exceptionDetails: any = {};
  reply: string;
  status: boolean;
  catDDList: any = {};
  alert: any = {};
  public userList: any;
  @Input() id: number;
  @Output() returnToView = new EventEmitter();
  constructor(private route: ActivatedRoute, private apiService: ApiService) {
    this.exceptionDetails.ticket_reply_data = [];
  }

  ngOnInit() {
    this.alert.isvisible = false;
    this.status = true;
    this.getUser();
    this.getdata(this.id);
    this.getCategoy();

  }


  replyfn(reply: string, id: number) {
    console.log(reply, id);
    this.reply = '';
    const param = {
      'ticket_id': id,
      'replymessage': reply
    };
    this.apiService.post('ticket/create_reply/', param).subscribe(
      data => {
        console.log('sucess in reply', data);
        this.getdata(id);
      },
      error => {
        console.log('error in reply');
      }
    );

  }

  getdata(id: any) {

    const param = {
      'ticket_id': id
    };

    this.apiService.post('ticket/detail/', param).subscribe(
      (data: any) => {
        console.log('POST Request is successful ', data);
        this.exceptionDetails = data;
        this.status = this.exceptionDetails.status == 'open' ? true : false ;
      },
      error => {
        console.log('Error', error);
      }
    );
  }

  getCategoy() {
    this.apiService.post('ticket/categorymenus/', {}).subscribe((data: any) => {
      this.catDDList = data;
    });
  }

  assignTicket() {

    const params = {
      'ticket_id': this.exceptionDetails.ticket_id,
      'additional_info': this.exceptionDetails.additional_info,
      'category': this.catDDList.filter(x => this.exceptionDetails.category_id == x.category_id)[0].category,
      'sub_category': this.catDDList.filter(x => this.exceptionDetails.category_id == x.category_id)[0].sub_category,
      'status': this.status ? 'open' : 'closed',
      'assign_to_user': this.userList.filter(x => this.exceptionDetails.assign_to_user == x.id )[0].user_name
    };

    this.apiService.post('ticket/update/', params).subscribe(
      (data: any) => {
        this.alert.isvisible = true;
        this.alert.text = data.status_desc;
        this.alert.class = 'success';
        this.alert.type = 'Ticket Update:';
        this.backToException();
      },
      (error: any) => {
        this.alert.isvisible = true;
        this.alert.text = error.error.status_desc;
        this.alert.class = 'danger';
        this.alert.type = 'Ticket Update Failure:';
        this.backToException();
      }
    );

  }

  getUser() {
    this.apiService.post('users/list/', {}).subscribe(
      (data: any) => {
        this.userList = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  backToException() {
    this.returnToView.emit({'isView': false, 'alert': this.alert});
  }

}
