import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';

import { CreateExceptionComponent } from './exceptions/create-exception/create-exception.component';
import { ExceptionsComponent } from './exceptions/exceptions.component';
import { ViewexceptionComponent } from './exceptions/viewexception/viewexception.component';
import { CommonImportsModule } from '../common/common-imports.module';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';

const routes: Routes = [
{ path: 'exceptions', component: ExceptionsComponent,
  canActivate: [AuthCheckService] },
{ path: 'addexception', component: CreateExceptionComponent,
  canActivate: [AuthCheckService] },  // addexception
{ path: 'viewexception', component: ViewexceptionComponent,
  canActivate: [AuthCheckService] }
];


@NgModule({
  declarations: [
    CreateExceptionComponent,
    ExceptionsComponent,
    ViewexceptionComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class ExceptionsModuleModule { }
