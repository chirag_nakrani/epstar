import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { FileUploadModule } from 'ng2-file-upload';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule, ProgressbarModule } from 'ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AmountMaskDirective } from './shared/directives/amount-mask.directive';
import { AmountMaskPipe } from './shared/pipes/amount-mask.pipe';

@NgModule({
  declarations: [
      AmountMaskPipe,
      AmountMaskDirective
  ],
  imports: [
    CommonModule,
    SelectDropDownModule,
    NgxDatatableModule,
    FileUploadModule,
    AmazingTimePickerModule,
    PdfViewerModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    TooltipModule,
    ProgressbarModule.forRoot()
  ],
  exports: [
    SelectDropDownModule,
    NgxDatatableModule,
    FileUploadModule,
    AmazingTimePickerModule,
    PdfViewerModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule,
    TooltipModule,
    AmountMaskPipe,
    AmountMaskDirective,
    ProgressbarModule
  ],
  providers: [
    AmountMaskPipe
  ]
})
export class CommonImportsModule { }
