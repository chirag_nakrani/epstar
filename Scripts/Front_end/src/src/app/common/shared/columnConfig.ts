import {
  Injectable
} from '@angular/core';

@Injectable()
export class ColumnConfig {
  columnConfigMasterData = [{
      'file_type': 'AMS_ATM',
      'table_column': 'site_code',
      'display_column_name': 'Site Code',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'old_atm_id',
      'display_column_name': 'Old Atm Iid',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'atm_id',
      'display_column_name': 'Current Atm Id',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'atm_band',
      'display_column_name': 'Atm Band',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'ej_docket_no',
      'display_column_name': 'Ej Docket No',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'grouting_status',
      'display_column_name': 'Grouting Status',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'brand',
      'display_column_name': 'Brand',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'vsat_id',
      'display_column_name': 'Vsat Id',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'vendor_name',
      'display_column_name': 'Vendor Name',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'serial_no',
      'display_column_name': 'Serial No',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'current_deployment_status',
      'display_column_name': 'Current Deployment Status',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'tech_live_date',
      'display_column_name': 'Tech Live Date',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'cash_live_date',
      'display_column_name': 'Cash Live Date',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'insurance_limits',
      'display_column_name': 'Insurance Limits',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string',
      'isEditable': 'false'
    },

    {
      'file_type': 'AMS_ATM',
      'table_column': 'circle_zone_region',
      'display_column_name': 'Circle Zone Region',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'site_code_2015_16',
      'display_column_name': 'Site Code 2015-16',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'site_code_2016_17',
      'display_column_name': 'Site Code 2016-17',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'state',
      'display_column_name': 'State',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'district',
      'display_column_name': 'District',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'city',
      'display_column_name': 'City',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'site_address_line_1',
      'display_column_name': 'Site Address Line 1',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'site_address_line_2',
      'display_column_name': 'Site Address Line 2',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'pincode',
      'display_column_name': 'Pincode',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'site_category',
      'display_column_name': 'Site Category',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'site_type',
      'display_column_name': 'Site Type',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'installation_type',
      'display_column_name': 'Installation Type',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'site_status',
      'display_column_name': 'Site Status',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'channel_manager_name',
      'display_column_name': 'Channel Manager Name',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'channel_manager_contact_no',
      'display_column_name': 'Channel Manager Contact No',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'location_name',
      'display_column_name': 'Location Name',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'atm_cash_removal_date',
      'display_column_name': 'Atm Cash Removal Date',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'atm_ip',
      'display_column_name': 'Atm Ip',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'switch_ip',
      'display_column_name': 'Switch Ip',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'external_camera_installation_status',
      'display_column_name': 'External Camera Installation Status',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'atm_owner',
      'display_column_name': 'Atm Owner',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'stabilizer_status',
      'display_column_name': 'Stabilizer Status',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'ups_capacity',
      'display_column_name': 'Ups Capacity',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'no_of_batteries',
      'display_column_name': 'No Of Batteries',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'ups_battery_backup',
      'display_column_name': 'Ups Battery Backup',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'load_shedding_status',
      'display_column_name': 'Load Shedding Status',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'solar_dg',
      'display_column_name': 'Solar Dg',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'date_created',
      'display_column_name': 'Date Created',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'date_updated',
      'display_column_name': 'Date Updated',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'date_deleted',
      'display_column_name': 'Date Deleted',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'AMS_ATM',
      'table_column': 'is_deleted',
      'display_column_name': 'Is Deleted',
      'datatype': 'string',
      'isEditable': 'true'
    }
  ];
  columnConfigmasterFeeder = [{
      'file_type': 'FEEDER',
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'region_code',
      'display_column_name': 'REGION',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'sol_id',
      'display_column_name': 'SOL ID',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'feeder_branch',
      'display_column_name': 'CASH FEEDER BRANCH',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'district',
      'display_column_name': 'DISTRICT',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'circle',
      'display_column_name': 'CIRCLE',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'feeder_linked_count',
      'display_column_name': 'No. OF ATMs LINKED TO THIS FEEDER',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'is_vaulting_enabled',
      'display_column_name': 'VAULTING (YES/NO)',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'contact_details',
      'display_column_name': 'CONTACT NO.',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'email_id',
      'display_column_name': 'EMAIL ID',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'alternate_cash_balance',
      'display_column_name': 'ALTERNATE CASH BRANCH',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'FEEDER',
      'table_column': 'is_currency_chest',
      'display_column_name': 'WHETHER CURRENCY CHEST - YES/NO',
      'datatype': 'string',
      'isEditable': 'true'
    }
  ];
  columnConfigcraFeasibility = [{
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'site_code',
      'display_column_name': 'Site Code',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'atm_id',
      'display_column_name': 'ATM ID',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'bank_name',
      'display_column_name': 'Bank Name',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'new_cra',
      'display_column_name': 'New CRA',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'feasibility_or_loading_frequency',
      'display_column_name': 'Feasibility/Loading Frequency',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'distance_fom_hub_to_site',
      'display_column_name': 'Distance from Hub to Site',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'distance_from_nodal_branch',
      'display_column_name': 'Distance from Nodal Branch',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'distance_from_atm_site_to_nodal_branch',
      'display_column_name': 'Distance from ATM Site to Nodal Branch',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'flm_tat',
      'display_column_name': 'FLM TAT',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'cra_spoc',
      'display_column_name': 'CRA Spoc',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'cra_spoc_contact_no',
      'display_column_name': 'CRA Spoc Contact No.',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'br_document_status',
      'display_column_name': 'BR Document Status',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'cash_van',
      'display_column_name': 'Cash van',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'gunman',
      'display_column_name': 'Gunman',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'lc_nearest_hub_or_spoke_or_branch_from_site',
      'display_column_name': 'LC Nearest Hub/Spoke/Branch from Site',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'accessibility',
      'display_column_name': 'Accessibility',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'first_call_dispatch_time',
      'display_column_name': 'First Call Dispatch Time',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'last_call_dispatch_time',
      'display_column_name': 'Last Call Dispatch Time',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'reason_for_limited_access',
      'display_column_name': 'Reason for Limited Access',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'vaulting',
      'display_column_name': 'Vaulting',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'feasibility_received_date',
      'display_column_name': 'Feasibility Received Date',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'feasibility_send_date',
      'display_column_name': 'Feasibility Send Date',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'br_request_date',
      'display_column_name': 'BR Request Date',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'br_send_date',
      'display_column_name': 'BR Send Date',
      'datatype': 'date',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'is_feasible_mon',
      'display_column_name': 'Is Feasible Monday',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'is_feasible_tue',
      'display_column_name': 'Is Feasible Tuesday',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'is_feasible_wed',
      'display_column_name': 'Is Feasible Wednesday',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'is_feasible_thu',
      'display_column_name': 'Is Feasible Thursday',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'is_feasible_fri',
      'display_column_name': 'Is Feasible Friday',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'is_feasible_sat',
      'display_column_name': 'Is Feasible Saturday',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'CRAFEASIBILITY',
      'table_column': 'feeder_branch_code',
      'display_column_name': 'Feeder Branch Code',
      'datatype': 'string',
      'isEditable': 'true'
    }
  ];
  columnConfigatmconfigLimit = [{
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'site_code',
      'display_column_name': 'Site Code',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'old_atm_id',
      'display_column_name': 'Old ATM ID',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'atm_id',
      'display_column_name': 'Current ATM ID',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'whether_critical_atm',
      'display_column_name': 'Whether Critical ATM',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'cassette_50_count',
      'display_column_name': 'Cassette 50',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'cassette_100_count',
      'display_column_name': 'Cassette 100',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'cassette_200_count',
      'display_column_name': 'Cassette 200',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'cassette_500_count',
      'display_column_name': 'Cassette 500',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'cassette_2000_count',
      'display_column_name': 'Cassette 2000',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'bank_cash_limit',
      'display_column_name': 'Bank Cash Limit',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'base_limit',
      'display_column_name': 'Base Limit',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 's_g_locker_no',
      'display_column_name': 'S&G Locker',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'type_of_switch',
      'display_column_name': 'Type of Switch',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'insurance_limit',
      'display_column_name': 'Insurance limit',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'total_cassette_count',
      'display_column_name': 'Total Cassette Count',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'file_type': 'ATMCNFGLIMIT',
      'table_column': 'project_id',
      'display_column_name': 'Project ID',
      'datatype': 'string',
      'isEditable': 'false'
    }
  ];

  columnConfig = [{
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'datafor_date_time',
      'display_column_name': 'File Upload Date & Time',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'date',
    }, {
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'term_id',
      'display_column_name': 'ATM ID',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'acceptorname',
      'display_column_name': 'Acceptor Name',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'location',
      'display_column_name': 'Location',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'tot_cash',
      'display_column_name': 'Total Cash',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'cassette1',
      'display_column_name': 'Cassette Balance 1',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'cassette2',
      'display_column_name': 'Cassette Balance 2',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'cassette3',
      'display_column_name': 'Cassette Balance 3',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOMH',
      'table_column': 'cassette4',
      'display_column_name': 'Cassette Balance 4',
      'table_name': 'cash_balance_file_bomh',
      'datatype': 'string',
    }, {
      'datafor': 'DISPENSE',
      'bankcode': 'BOMH',
      'table_column': 'datafor_date_time',
      'display_column_name': 'File Upload Date & Time',
      'table_name': 'cash_dispense_file_bomh',
      'datatype': 'date',
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'BOMH',
      'table_column': 'atm_id',
      'display_column_name': 'ATM ID',
      'table_name': 'cash_dispense_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'BOMH',
      'table_column': 'location',
      'display_column_name': 'Location',
      'table_name': 'cash_dispense_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'BOMH',
      'table_column': 'amount_dispensed',
      'display_column_name': 'Amount Dispensed',
      'table_name': 'cash_dispense_file_bomh',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'ALB',
      'table_column': 'group_name',
      'display_column_name': 'Group Name',
      'table_name': 'cash_balance_file_alb',
      'datatype': 'string',
    },
    {
      'datafor': 'CBR',
      'bankcode': 'ALB',
      'table_column': 'termid',
      'display_column_name': 'ATM ID',
      'table_name': 'cash_balance_file_alb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'ALB',
      'table_column': 'location',
      'display_column_name': 'Location',
      'table_name': 'cash_balance_file_alb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'ALB',
      'table_column': 'currbalcass1',
      'display_column_name': 'Current Balance 1',
      'table_name': 'cash_balance_file_alb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'ALB',
      'table_column': 'ccurrbalcass2',
      'display_column_name': 'Current Balance 2',
      'table_name': 'cash_balance_file_alb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'ALB',
      'table_column': 'currbalcass3',
      'display_column_name': 'Current Balance 3',
      'table_name': 'cash_balance_file_alb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'ALB',
      'table_column': 'currbalcass4',
      'display_column_name': 'Current Balance 4',
      'table_name': 'cash_balance_file_alb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'atmid',
      'display_column_name': 'ATM ID',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'location',
      'display_column_name': 'Location',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'state',
      'display_column_name': 'State',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'atmstatus',
      'display_column_name': 'ATM Status',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'msvendor',
      'display_column_name': 'MSVendor',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'currentstatus',
      'display_column_name': 'Current Status',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'regionname',
      'display_column_name': 'Region Name',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'zone',
      'display_column_name': 'Zone',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'address',
      'display_column_name': 'Address',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop1bill',
      'display_column_name': 'Hop 1 Bill',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop2bill',
      'display_column_name': 'Hop 2 Bill',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop3bill',
      'display_column_name': 'Hop 3 Bill',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop4bill',
      'display_column_name': 'Hop 4 Bill',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop1begcash',
      'display_column_name': 'Hop1 Beg Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop2begcash',
      'display_column_name': 'Hop2 Beg Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop3begcash',
      'display_column_name': 'Hop3 Beg Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop4begcash',
      'display_column_name': 'Hop4 Beg Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop1endcash',
      'display_column_name': 'Hop1 End Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop2endcash',
      'display_column_name': 'Hop2 End Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop3endcash',
      'display_column_name': 'Hop3 End Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop4endcash',
      'display_column_name': 'Hop4 End Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop1cashdeposit',
      'display_column_name': 'Hop1 Cash Deposit',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop2cashdeposit',
      'display_column_name': 'Hop2 Cash Deposit',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop3cashdeposit',
      'display_column_name': 'Hop3 Cash Deposit',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'hop4cashdeposit',
      'display_column_name': 'Hop4 Cash Deposit',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'begcash',
      'display_column_name': 'Beg Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'endcash',
      'display_column_name': 'End Cash',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'cashout',
      'display_column_name': 'Cash Out',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'cashincr',
      'display_column_name': 'Cash in cr',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'timestamp',
      'display_column_name': 'Time Stamp',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'lastwithdrawaltime',
      'display_column_name': 'Last Withdrawal Time',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'lastsupervisorytime',
      'display_column_name': 'Last Supervisory Time',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'lastdeposittranstime',
      'display_column_name': 'Last Dep Tran time',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'lastadmintxntime',
      'display_column_name': 'Last Admintxn Time',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOB',
      'table_column': 'lastreversaltime',
      'display_column_name': 'Last Reversal time',
      'table_name': 'cash_balance_file_bob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'institutionid',
      'display_column_name': 'institution id',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'termid',
      'display_column_name': 'ATM ID',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'location',
      'display_column_name': 'Location',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_start1',
      'display_column_name': 'Cash Start1',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_start2',
      'display_column_name': 'Cash Start2',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_start3',
      'display_column_name': 'Cash Start3',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_start4',
      'display_column_name': 'Cash Start4',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_inc1',
      'display_column_name': 'Cash Inc1',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_inc2',
      'display_column_name': 'Cash Inc2',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_inc3',
      'display_column_name': 'Cash Inc3',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_inc4',
      'display_column_name': 'Cash Inc4',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_dec1',
      'display_column_name': 'Cash Dec1',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_dec2',
      'display_column_name': 'Cash Dec2',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_dec3',
      'display_column_name': 'Cash Dec3',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_dec4',
      'display_column_name': 'Cash Dec4',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_out1',
      'display_column_name': 'Cash Out1',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_out2',
      'display_column_name': 'Cash Out2',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_out3',
      'display_column_name': 'Cash Out3',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_out4',
      'display_column_name': 'Cash Out4',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_currbal1',
      'display_column_name': 'Cash Currbal1',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_currbal2',
      'display_column_name': 'Cash Currbal2',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_currbal3',
      'display_column_name': 'Cash Currbal3',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'cash_currbal4',
      'display_column_name': 'Cash Currbal4',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'termid1',
      'display_column_name': 'Term id1',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'txn_date',
      'display_column_name': 'Txn Date',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'date'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'txn_time',
      'display_column_name': 'Txn Time',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'respcode',
      'display_column_name': 'Respcode',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'BOI',
      'table_column': 'pcode',
      'display_column_name': 'Pcode',
      'table_name': 'cash_balance_file_boi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'termloc',
      'display_column_name': 'Term Loc',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'termcity',
      'display_column_name': 'Term City',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'regn_id',
      'display_column_name': 'Regn Id',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'network_link_status',
      'display_column_name': 'Network Link Status',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'atm_status',
      'display_column_name': 'Atm Status',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'out_time',
      'display_column_name': 'Out Time',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'err_1',
      'display_column_name': 'Err 1',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'err_2',
      'display_column_name': 'Err 2',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'err_3',
      'display_column_name': 'Err 3',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'err_4',
      'display_column_name': 'Err 4',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'err_5',
      'display_column_name': 'Err 5',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'err_6',
      'display_column_name': 'Err 6',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'err_7',
      'display_column_name': 'Err 7',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'err_8',
      'display_column_name': 'Err 8',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'hop1cash',
      'display_column_name': 'Hop1cash',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'hop1bill',
      'display_column_name': 'Hop1bill',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'hop2cash',
      'display_column_name': 'Hop2cash',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'hop2bill',
      'display_column_name': 'Hop2bill',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'hop3cash',
      'display_column_name': 'Hop3cash',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'hop3bill',
      'display_column_name': 'Hop3bill',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'hop4cash',
      'display_column_name': 'Hop4cash',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'hop4bill',
      'display_column_name': 'Hop4bill',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'end_cash',
      'display_column_name': 'End Cash',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CAB',
      'table_column': 'timestamp',
      'display_column_name': 'Timestamp',
      'table_name': 'cash_balance_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CBI',
      'table_column': 'sst',
      'display_column_name': 'SST',
      'table_name': 'cash_balance_file_cbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CBI',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_cbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CBI',
      'table_column': 'cas_200',
      'display_column_name': 'Cas 200',
      'table_name': 'cash_balance_file_cbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CBI',
      'table_column': 'cas_100',
      'display_column_name': 'Cas 100',
      'table_name': 'cash_balance_file_cbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CBI',
      'table_column': 'cas_500',
      'display_column_name': 'Cas 500',
      'table_name': 'cash_balance_file_cbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CBI',
      'table_column': 'cas_2000',
      'display_column_name': 'Cas 2000',
      'table_name': 'cash_balance_file_cbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CBI',
      'table_column': 'remaining',
      'display_column_name': 'Remaining',
      'table_name': 'cash_balance_file_cbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CBI',
      'table_column': 'location',
      'display_column_name': 'Location',
      'table_name': 'cash_balance_file_cbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CORP',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_corp',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CORP',
      'table_column': 'cash_100s',
      'display_column_name': 'Cash 100s',
      'table_name': 'cash_balance_file_corp',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CORP',
      'table_column': 'cash_200s',
      'display_column_name': 'Cash 200s',
      'table_name': 'cash_balance_file_corp',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CORP',
      'table_column': 'cash_500s',
      'display_column_name': 'Cash 500s',
      'table_name': 'cash_balance_file_corp',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'CORP',
      'table_column': 'cash_2000s',
      'display_column_name': 'Cash 2000s',
      'table_name': 'cash_balance_file_corp',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'stat',
      'display_column_name': 'Stat',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'atm_stat',
      'display_column_name': 'Atm Stat',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'fiid',
      'display_column_name': 'Fiid',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'err_2',
      'display_column_name': 'Err 2',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'err_3',
      'display_column_name': 'Err 3',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'err_4',
      'display_column_name': 'Err 4',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'err_5',
      'display_column_name': 'Err 5',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'err_6',
      'display_column_name': 'Err 6',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'err_7',
      'display_column_name': 'Err 7',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'err_8',
      'display_column_name': 'Err 8',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'cashchng',
      'display_column_name': 'Cashchng',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'cashout',
      'display_column_name': 'Cashout',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'cashrepl',
      'display_column_name': 'Cashrepl',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'cashsply',
      'display_column_name': 'Cashsply',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'end_cash',
      'display_column_name': 'End Cash',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop4cash',
      'display_column_name': 'Hop4cash',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop4bill',
      'display_column_name': 'Hop4bill',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop5cash',
      'display_column_name': 'Hop5cash',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop5bill',
      'display_column_name': 'Hop5bill',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'amt_chk',
      'display_column_name': 'Amt Chk',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'num_chk',
      'display_column_name': 'Num Chk',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'amt_dep',
      'display_column_name': 'Amt Dep',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'num_dep',
      'display_column_name': 'Num Dep',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'intvdown',
      'display_column_name': 'Intvdown',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'flt_cnt',
      'display_column_name': 'Flt Cnt',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop1cash',
      'display_column_name': 'Hop1cash',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop1bill',
      'display_column_name': 'Hop1bill',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop2cash',
      'display_column_name': 'Hop2cash',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop2bill',
      'display_column_name': 'Hop2bill',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop3cash',
      'display_column_name': 'Hop3cash',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'hop3bill',
      'display_column_name': 'Hop3bill',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'linestat',
      'display_column_name': 'Linestat',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'termloc',
      'display_column_name': 'Termloc',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'termcity',
      'display_column_name': 'Termcity',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'term_st',
      'display_column_name': 'Term st',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'termst_x',
      'display_column_name': 'Termst x',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'ownr',
      'display_column_name': 'Ownr',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'row_cnt',
      'display_column_name': 'Row Cnt',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'src_name',
      'display_column_name': 'Src Name',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'spvr_on',
      'display_column_name': 'Spvr On',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'brch_id',
      'display_column_name': 'Brch Id',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'regn_id',
      'display_column_name': 'Regn Id',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'timeofst',
      'display_column_name': 'Time of st',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'atm_type',
      'display_column_name': 'Atm Type',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'vendor_name',
      'display_column_name': 'Vendor Name',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'DENA',
      'table_column': 'timestamp_data',
      'display_column_name': 'Timestamp Data',
      'table_name': 'cash_balance_file_dena',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'TERMID',
      'display_column_name': 'TERMID',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'TiaDATETIME',
      'display_column_name': 'TiaDATETIME',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'Bill1',
      'display_column_name': 'Bill1',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'Bill2',
      'display_column_name': 'Bill2',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'Bill3',
      'display_column_name': 'Bill3',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'Bill4',
      'display_column_name': 'Bill4',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'STARTBALCASS1',
      'display_column_name': 'STARTBALCASS1',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'STARTBALCASS2',
      'display_column_name': 'STARTBALCASS2',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'STARTBALCASS3',
      'display_column_name': 'STARTBALCASS3',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'STARTBALCASS4',
      'display_column_name': 'STARTBALCASS4',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CASHINCCASS1',
      'display_column_name': 'CASHINCCASS1',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CASHINCCASS2',
      'display_column_name': 'CASHINCCASS2',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CASHINCCASS3',
      'display_column_name': 'CASHINCCASS3',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CASHINCCASS4',
      'display_column_name': 'CASHINCCASS4',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CASHOUTCASS1',
      'display_column_name': 'CASHOUTCASS1',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CASHOUTCASS2',
      'display_column_name': 'CASHOUTCASS2',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CASHOUTCASS3',
      'display_column_name': 'CASHOUTCASS3',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CASHOUTCASS4',
      'display_column_name': 'CASHOUTCASS4',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CURRBALCASS1',
      'display_column_name': 'CURRBALCASS1',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CURRBALCASS2',
      'display_column_name': 'CURRBALCASS2',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CURRBALCASS3',
      'display_column_name': 'CURRBALCASS3',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'CURRBALCASS4',
      'display_column_name': 'CURRBALCASS4',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'LASTRESETDATE',
      'display_column_name': 'LASTRESETDATE',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'LASTRESETTIME',
      'display_column_name': 'LASTRESETTIME',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'LASTTXNDATE',
      'display_column_name': 'LASTTXNDATE',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'LASTTXNTIME',
      'display_column_name': 'LASTTXNTIME',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'LASTTXNSERIAL',
      'display_column_name': 'LASTTXNSERIAL',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IDBI',
      'table_column': 'LASTTXNSTATUS',
      'display_column_name': 'LASTTXNSTATUS',
      'table_name': 'cash_balance_file_idbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'remaining_1',
      'display_column_name': 'Remaining 1',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'remaining_2',
      'display_column_name': 'Remaining 2',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'remaining_3',
      'display_column_name': 'Remaining 3',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'remaining_4',
      'display_column_name': 'Remaining 4',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'total_of_remaining',
      'display_column_name': 'Total Of Remaining',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'opening_1',
      'display_column_name': 'Opening 1',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'opening_2',
      'display_column_name': 'Opening 2',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'opening_3',
      'display_column_name': 'Opening 3',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'opening_4',
      'display_column_name': 'Opening 4',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'total_of_opening',
      'display_column_name': 'Total Of Opening',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'dispense_1',
      'display_column_name': 'Dispense 1',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'dispense_2',
      'display_column_name': 'Dispense 2',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'dispense_3',
      'display_column_name': 'Dispense 3',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'dispense_4',
      'display_column_name': 'Dispense 4',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'IOB',
      'table_column': 'total_of_dispense',
      'display_column_name': 'Total Of Dispense',
      'table_name': 'cash_balance_file_iob',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'brn',
      'display_column_name': 'Brn',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'atm_location',
      'display_column_name': 'Atm Location',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'f_tray',
      'display_column_name': 'F tray',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'f_count',
      'display_column_name': 'F count',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'g_tray',
      'display_column_name': 'G tray',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'g_count',
      'display_column_name': 'G count',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'h_tray',
      'display_column_name': 'H tray',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'h_count',
      'display_column_name': 'H count',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'LVB',
      'table_column': 'total_amt',
      'display_column_name': 'Total Amt',
      'table_name': 'cash_balance_file_lvb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'cash_out',
      'display_column_name': 'Cash Out',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'endcash',
      'display_column_name': 'Endcash',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'hop1cash',
      'display_column_name': 'Hop1cash',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'hop1bill',
      'display_column_name': 'Hop1bill',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'hop2cash',
      'display_column_name': 'Hop2cash',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'hop2bill',
      'display_column_name': 'Hop2bill',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'hop3cash',
      'display_column_name': 'Hop3cash',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'hop3bill',
      'display_column_name': 'Hop3bill',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'hop4cash',
      'display_column_name': 'Hop4cash',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'hop4bill',
      'display_column_name': 'Hop4bill',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'ownr',
      'display_column_name': 'Ownr',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'state',
      'display_column_name': 'State',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'PSB',
      'table_column': 'timestamp',
      'display_column_name': 'Timestamp',
      'table_name': 'cash_balance_file_psb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'cfb_date_time',
      'display_column_name': 'Cfb Date Time',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'i_denom',
      'display_column_name': 'I Denom',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'i_begin_cash',
      'display_column_name': 'I Begin Cash',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'i_dispense',
      'display_column_name': 'I Dispense',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'i_remining',
      'display_column_name': 'I Remaining',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'ii_denom',
      'display_column_name': 'II Denom',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'ii_begin_cash',
      'display_column_name': 'II Begin Cash',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'ii_dispense',
      'display_column_name': 'II Dispense',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'ii_remining',
      'display_column_name': 'II Remaining',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'iii_denom',
      'display_column_name': 'III Denom',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'iii_begin_cash',
      'display_column_name': 'III Begin Cash',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'iii_dispense',
      'display_column_name': 'III Dispense',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'iii_remining',
      'display_column_name': 'III Remaining',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'iv_denom',
      'display_column_name': 'IV Denom',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'iv_begin_cash',
      'display_column_name': 'IV Begin Cash',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'iv_dispense',
      'display_column_name': 'IV Dispense',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'RSBL',
      'table_column': 'iv_remining',
      'display_column_name': 'IV Remaining',
      'table_name': 'cash_balance_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'terminal_location',
      'display_column_name': 'Terminal Location',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'city',
      'display_column_name': 'City',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_dispensed_hopper_1',
      'display_column_name': 'Cash Dispensed Hopper 1',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'bills_hopr1',
      'display_column_name': 'Bills Hopr1',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_dispensed_hopper_2',
      'display_column_name': 'Cash Dispensed Hopper 2',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'bills_hopr2',
      'display_column_name': 'Bills Hopr2',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_dispensed_hopper_3',
      'display_column_name': 'Cash Dispensed Hopper 3',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'bills_hopr3',
      'display_column_name': 'Bills Hopr3',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_dispensed_hopper_4',
      'display_column_name': 'Cash Dispensed Hopper 4',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'bills_hopr4',
      'display_column_name': 'Bills Hopr4',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_dispensed_hopper_5',
      'display_column_name': 'Cash Dispensed Hopper 5',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'bills_hopr5',
      'display_column_name': 'Bills Hopr5',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_dispensed_hopper_6',
      'display_column_name': 'Cash Dispensed Hopper 6',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'bills_hopr6',
      'display_column_name': 'Bills Hopr6',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'total_remaining_cash_def_curr',
      'display_column_name': 'Total Remaining Cash Def Curr',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'remote_address',
      'display_column_name': 'Remote Address',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_increment_hopper_1',
      'display_column_name': 'Cash Increment Hopper 1',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_increment_hopper_2',
      'display_column_name': 'Cash Increment Hopper 2',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_increment_hopper_3',
      'display_column_name': 'Cash Increment Hopper 3',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_increment_hopper_4',
      'display_column_name': 'Cash Increment Hopper 4',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_increment_hopper_5',
      'display_column_name': 'Cash Increment Hopper 5',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'cash_increment_hopper_6',
      'display_column_name': 'Cash Increment Hopper 6',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'circle',
      'display_column_name': 'Circle',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'sitetype',
      'display_column_name': 'Sitetype',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'msvendor',
      'display_column_name': 'Msvendor',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'lastwithdrawaltime',
      'display_column_name': 'Last withdrawal time',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'switch',
      'display_column_name': 'Switch',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'district',
      'display_column_name': 'District',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'module',
      'display_column_name': 'Module',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'currentstatus',
      'display_column_name': 'Current Status',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'branchmanagerphone',
      'display_column_name': 'Branch Manager Phone',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'channelmanagercontact',
      'display_column_name': 'Channel Manager Contact',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'jointcustodianphone',
      'display_column_name': 'Joint Custodian Phone',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'network',
      'display_column_name': 'Network',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'populationgroup',
      'display_column_name': 'Population Group',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'regionname',
      'display_column_name': 'Region Name',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'statename',
      'display_column_name': 'State Name',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'jointcustodianname1',
      'display_column_name': 'Joint Custodian Name1',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'jointcustodianphone2',
      'display_column_name': 'Joint CustodianPhone2',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'SBI',
      'table_column': 'last_deposit_txn_time',
      'display_column_name': 'Last Deposit Txn Time',
      'table_name': 'cash_balance_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UBI',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UBI',
      'table_column': 'type_01',
      'display_column_name': 'Type 01',
      'table_name': 'cash_balance_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UBI',
      'table_column': 'type_02',
      'display_column_name': 'Type 02',
      'table_name': 'cash_balance_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UBI',
      'table_column': 'type_03',
      'display_column_name': 'Type 03',
      'table_name': 'cash_balance_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UBI',
      'table_column': 'type_04',
      'display_column_name': 'Type 04',
      'table_name': 'cash_balance_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UBI',
      'table_column': 'file_date_time',
      'display_column_name': 'File Date Time',
      'table_name': 'cash_balance_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete1_start',
      'display_column_name': 'Cassete1 Start',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete1_dispensed',
      'display_column_name': 'Cassete1 Dispensed',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete1_remaining',
      'display_column_name': 'Cassete1 Remaining',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete2_start',
      'display_column_name': 'Cassete2 Start',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete2_dispensed',
      'display_column_name': 'Cassete2 Dispensed',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete2_remaining',
      'display_column_name': 'Cassete2 Remaining',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete3_start',
      'display_column_name': 'Cassete3 Start',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete3_dispensed',
      'display_column_name': 'Cassete3 Dispensed',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete3_remaining',
      'display_column_name': 'Cassete3 Remaining',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete4_start',
      'display_column_name': 'Cassete4 Start',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete4_dispensed',
      'display_column_name': 'Cassete4 Dispensed',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'cassete4_remaining',
      'display_column_name': 'Cassete4 Remaining',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'total_start',
      'display_column_name': 'Total Start',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'toatal_dispensed',
      'display_column_name': 'Total Dispensed',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'total_remaining',
      'display_column_name': 'Total Remaining',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'vendor_name',
      'display_column_name': 'Vendor Name',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'region',
      'display_column_name': 'Region',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'mtloc',
      'display_column_name': 'Mtloc',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'UCO',
      'table_column': 'mtadr',
      'display_column_name': 'Mtadr',
      'table_name': 'cash_balance_file_uco',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_start1',
      'display_column_name': 'Cash Start1',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_start2',
      'display_column_name': 'Cash Start2',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_start3',
      'display_column_name': 'Cash Start3',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_start4',
      'display_column_name': 'Cash Start4',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_inc1',
      'display_column_name': 'Cash Inc1',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_inc2',
      'display_column_name': 'Cash Inc2',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_inc3',
      'display_column_name': 'Cash Inc3',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_inc4',
      'display_column_name': 'Cash Inc4',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_dec1',
      'display_column_name': 'Cash Dec1',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_dec2',
      'display_column_name': 'Cash Dec2',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_dec3',
      'display_column_name': 'Cash Dec3',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_dec4',
      'display_column_name': 'Cash Dec4',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_out1',
      'display_column_name': 'Cash Out1',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_out2',
      'display_column_name': 'Cash Out2',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_out3',
      'display_column_name': 'Cash Out3',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_out4',
      'display_column_name': 'Cash Out4',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_currbal1',
      'display_column_name': 'Cash Currbal1',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_currbal2',
      'display_column_name': 'Cash Currbal2',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_currbal3',
      'display_column_name': 'Cash Currbal3',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'CBR',
      'bankcode': 'VJB',
      'table_column': 'cash_currbal4',
      'display_column_name': 'Cash Currbal4',
      'table_name': 'cash_balance_file_vjb',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'fiid',
      'display_column_name': 'Fiid',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'term_city',
      'display_column_name': 'Term City',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'term_location',
      'display_column_name': 'Term Location',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'unapproved_nonfin',
      'display_column_name': 'Unapproved Nonfin',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'unapproved_fin',
      'display_column_name': 'Unapproved Fin',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'approved_nonfin',
      'display_column_name': 'Approved Nonfin',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'approved_fin',
      'display_column_name': 'Approved Fin',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'approved_declined_tottran',
      'display_column_name': 'Approved Declined Tottran',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'amount_dispensed',
      'display_column_name': 'Amount Dispensed',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'CAB',
      'table_column': 'file_date',
      'display_column_name': 'File Date',
      'table_name': 'cash_dispense_file_cab',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'RSBL',
      'table_column': 'TRL_DATE_LOCAL',
      'display_column_name': 'Trl Date Local',
      'table_name': 'cash_dispense_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'RSBL',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_dispense_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'RSBL',
      'table_column': 'amount_dispensed',
      'display_column_name': 'Amount Dispensed',
      'table_name': 'cash_dispense_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'RSBL',
      'table_column': 'count_TRL_CARD_ACPT_TERMINAL_IDENT',
      'display_column_name': 'Count Trl Card Acpt Terminal Ident',
      'table_name': 'cash_dispense_file_rsbl',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'SBI',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_dispense_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'SBI',
      'table_column': 'cash_withdrawal',
      'display_column_name': 'Cash Withdrawal',
      'table_name': 'cash_dispense_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'SBI',
      'table_column': 'opening_cash',
      'display_column_name': 'Opening Cash',
      'table_name': 'cash_dispense_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'SBI',
      'table_column': 'total_cash_replenished',
      'display_column_name': 'Total Cash Replenished',
      'table_name': 'cash_dispense_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'SBI',
      'table_column': 'cer',
      'display_column_name': 'Cer',
      'table_name': 'cash_dispense_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'SBI',
      'table_column': 'msvendor',
      'display_column_name': 'Msvendor',
      'table_name': 'cash_dispense_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'SBI',
      'table_column': 'circle_name',
      'display_column_name': 'Circle Name',
      'table_name': 'cash_dispense_file_sbi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'UBI',
      'table_column': 'type_01',
      'display_column_name': 'Type 01',
      'table_name': 'cash_dispense_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'UBI',
      'table_column': 'type_02',
      'display_column_name': 'Type 02',
      'table_name': 'cash_dispense_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'UBI',
      'table_column': 'type_03',
      'display_column_name': 'Type_03',
      'table_name': 'cash_dispense_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'UBI',
      'table_column': 'type_04',
      'display_column_name': 'Type 04',
      'table_name': 'cash_dispense_file_ubi',
      'datatype': 'string'
    },
    {
      'datafor': 'DISPENSE',
      'bankcode': 'UBI',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'cash_dispense_file_ubi',
      'datatype': 'string'
    }, {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'datafor_date_time',
      'display_column_name': 'File Upload Date & Time',
      'table_name': 'vault_cash_balance',
      'datatype': 'date',
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'bank_name',
      'display_column_name': 'Bank',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'feeder_branch_name',
      'display_column_name': 'Feeder Branch Name',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'cra_name',
      'display_column_name': 'Cra Name',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'agent',
      'display_column_name': 'Agent',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'vault_balance_100',
      'display_column_name': 'Vault Balance 100',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'vault_balance_200',
      'display_column_name': 'Vault Balance 200',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'vault_balance_500',
      'display_column_name': 'Vault Balance 500',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'vault_balance_2000',
      'display_column_name': 'Vault Balance 2000',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'total_vault_balance',
      'display_column_name': 'Total Vault Balance',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'dependency',
      'display_column_name': 'Dependency',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'remark',
      'display_column_name': 'Remark',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    },
    {
      'datafor': 'VCB',
      'bankcode': 'SIS',
      'table_column': 'datafor_date_time',
      'display_column_name': 'Date',
      'table_name': 'vault_cash_balance',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '',
      'table_column': 'datafor_date_time',
      'display_column_name': 'File Upload Date & Time',
      'table_name': 'ims_master',
      'datatype': 'date',
    },
    {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'ticketno',
      'display_column_name': 'Ticket No',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'atmid',
      'display_column_name': 'ATM ID',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'site_code',
      'display_column_name': 'Site Code',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'bankname',
      'display_column_name': 'Bank Name',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'ticket_generation_type',
      'display_column_name': 'Ticket Generation Type',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'site_details',
      'display_column_name': 'Site Details',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'fault',
      'display_column_name': 'Fault',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'fault_description',
      'display_column_name': 'Fault Description',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'call_status',
      'display_column_name': 'Call Status',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'ticket_status',
      'display_column_name': 'Ticket Status',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'message_date_time',
      'display_column_name': 'Message Date Time',
      'table_name': 'ims_master',
      'datatype': 'date'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'open_date',
      'display_column_name': 'Open Date',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'opentime',
      'display_column_name': 'Open Time',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'duration',
      'display_column_name': 'Duration',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'remarks',
      'display_column_name': 'Remarks',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'latestremarks',
      'display_column_name': 'Latest Remarks',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'datafor': 'IMS',
      'bankcode': '  ',
      'table_column': 'follow_up_date',
      'display_column_name': 'Follow Up Date',
      'table_name': 'ims_master',
      'datatype': 'string'
    }, {
      'table_column': 'date',
      'display_column_name': 'Date',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'date',
      'display_column_name': 'Sr No',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'location',
      'display_column_name': 'Location',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'feeder_branch',
      'display_column_name': 'Feeder Branch',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'cra',
      'display_column_name': 'Cra',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'district',
      'display_column_name': 'District',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'state',
      'display_column_name': 'State',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'bank_name',
      'display_column_name': 'Bank Name',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'planning_100',
      'display_column_name': 'Planning 100',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'planning_200',
      'display_column_name': 'Planning 200',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'planning_500',
      'display_column_name': 'Planning 500',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'planning_2000',
      'display_column_name': 'Planning 2000',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'planning_total',
      'display_column_name': 'Planning Total',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'revised_planning_100',
      'display_column_name': 'Revised Planning 100',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'revised_planning_200',
      'display_column_name': 'Revised Planning 200',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'revised_planning_500',
      'display_column_name': 'Revised Planning 500',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'revised_planning_2000',
      'display_column_name': 'Revised Planning 2000',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'revised_planning_total',
      'display_column_name': 'Revised Planning Total',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'actual_loading_100',
      'display_column_name': 'Actual Loading 100',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'actual_loading_200',
      'display_column_name': 'Actual Loading 200',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'actual_loading_500',
      'display_column_name': 'Actual Loading 500',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'actual_loading_2000',
      'display_column_name': 'Actual Loading 2000',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'actual_loading_total',
      'display_column_name': 'Actual Loading Total',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'remarks',
      'display_column_name': 'Remarks',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    },
    {
      'table_column': 'diverted_atm_id',
      'display_column_name': 'Diverted Atm Id',
      'table_name': 'Daily_Loading_Report',
      'datatype': 'string'
    }
  ];
  indentListColumnList = [{
      'table_column': 'indent_short_code',
      'display_column_name': 'EPS Ref No.',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    {
      'table_column': 'indent_order_number',
      'display_column_name': 'Indent Order Number',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    {
      'table_column': 'order_date',
      'display_column_name': 'Order Date',
      'isDisplayColumn': 'true',
      'dataType': 'date'
    },
    {
      'table_column': 'collection_date',
      'display_column_name': 'Collection Date',
      'isDisplayColumn': 'true',
      'dataType': 'date'
    },
    {
      'table_column': 'replenishment_date',
      'display_column_name': 'Repl. Date',
      'isDisplayColumn': 'true',
      'dataType': 'date'
    },
    {
      'table_column': 'cypher_code',
      'display_column_name': 'Cypher Code',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    {
      'table_column': 'bank',
      'display_column_name': 'Bank Name',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    {
      'table_column': 'feeder_branch',
      'display_column_name': 'Feeder Br.',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    {
      'table_column': 'cra',
      'display_column_name': 'CRA',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    // {
    //   'table_column': 'total_atm_loading_amount_50',
    //   'display_column_name': 'Total ATM Loading Amount Deno (50)',
    //   'isDisplayColumn': 'true',
    //   'dataType': 'string'
    // },
    {
      'table_column': 'total_atm_loading_amount_100',
      'display_column_name': 'Original Indent 100',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_atm_loading_amount_200',
      'display_column_name': 'Original Indent 200',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_atm_loading_amount_500',
      'display_column_name': 'Original Indent 500',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_atm_loading_amount_2000',
      'display_column_name': 'Original Indent 2000',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_atm_loading_amount',
      'display_column_name': 'Original Indent Tot.',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    // {
    //   'table_column': 'cra_opening_vault_balance_50',
    //   'display_column_name': 'CRA Opening Vault Balance Deno (50)',
    //   'isDisplayColumn': 'false',
    //   'dataType': 'string'
    // },
    {
      'table_column': 'cra_opening_vault_balance_100',
      'display_column_name': 'VCB_100',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'cra_opening_vault_balance_200',
      'display_column_name': 'VCB_200',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'cra_opening_vault_balance_500',
      'display_column_name': 'VCB_500',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'cra_opening_vault_balance_2000',
      'display_column_name': 'VCB_2000',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'cra_opening_vault_balance',
      'display_column_name': 'VCB_Total',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    // {
    //   'table_column': 'total_bank_withdrawal_amount_50',
    //   'display_column_name': 'Total Bank Withdrawal Amount Deno (50)',
    //   'isDisplayColumn': 'false',
    //   'dataType': 'string'
    // },
    {
      'table_column': 'total_bank_withdrawal_amount_100',
      'display_column_name': 'Revised_Indent_100',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_bank_withdrawal_amount_200',
      'display_column_name': 'Revised_Indent_200',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_bank_withdrawal_amount_500',
      'display_column_name': 'Revised_Indent_500',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_bank_withdrawal_amount_2000',
      'display_column_name': 'Revised_Indent_2000',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_bank_withdrawal_amount',
      'display_column_name': 'Revised_Indent_Total',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'authorized_by_1',
      'display_column_name': '1st Auth. By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'authorized_by_2',
      'display_column_name': '2nd Auth. By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'notes',
      'display_column_name': 'Notes',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'amount_in_words',
      'display_column_name': 'Amt. In Words',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'indent_type',
      'display_column_name': 'Indent Type',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'project_id',
      'display_column_name': 'Project',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'indent_status',
      'display_column_name': 'Indent Status',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'created_on',
      'display_column_name': 'Created Date',
      'isDisplayColumn': 'false',
      'dataType': 'date'
    },
    {
      'table_column': 'created_by',
      'display_column_name': 'Created By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    // {
    //   'table_column': 'created_reference_id',
    //   'display_column_name': 'Created Ref. Id',
    //   'isDisplayColumn': 'false',
    //   'dataType': 'string'
    // },
    {
      'table_column': 'approved_on',
      'display_column_name': 'Approved Date',
      'isDisplayColumn': 'false',
      'dataType': 'date'
    },
    {
      'table_column': 'approved_by',
      'display_column_name': 'Approved By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    // {
    //   'table_column': 'approved_reference_id',
    //   'display_column_name': 'Approved Reference Id',
    //   'isDisplayColumn': 'false',
    //   'dataType': 'string'
    // },
    {
      'table_column': 'rejected_on',
      'display_column_name': 'Rejected On',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'rejected_by',
      'display_column_name': 'Rejected By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    // {
    //   'table_column': 'reject_reference_id',
    //   'display_column_name': 'Rejected Ref. Id',
    //   'isDisplayColumn': 'false',
    //   'dataType': 'string'
    // },
    // {
    //   'table_column': 'reject_trigger_reference_id',
    //   'display_column_name': 'Reject Trigger Reference Id',
    //   'isDisplayColumn': 'false',
    //   'dataType': 'string'
    // },
    // {
    //   'table_column': 'approved_trigger_reference_id',
    //   'display_column_name': 'Approve Trigger Reference Id',
    //   'isDisplayColumn': 'false',
    //   'dataType': 'string'
    // }
  ];
  indentDetailsColumnList = [
    {
      'table_column': 'atm_id',
      'display_column_name': 'ATM Id',
      'isDisplayColumn': 'true',
      'dataType': 'string',
      'isEditable': false,
      'width': 70,
      'background': 'no'
    },
    {
      'table_column': 'location',
      'display_column_name': 'Location',
      'isDisplayColumn': 'true',
      'dataType': 'string',
      'isEditable': false,
      'width': 80,
      'background': 'no'
    },
    {
      'table_column': 'morning_balance_100',
      'display_column_name': 'Avl. Bal. 100',
      'isDisplayColumn': 'true',
      'dataType': 'currency',
      'isEditable': false,
      'width': 75,
      'background': 'lgreen'
    },
    {
        'table_column': 'morning_balance_200',
        'display_column_name': 'Avl. Bal. 200',
        'isDisplayColumn': 'true',
        'dataType': 'currency',
        'isEditable': false,
        'width': 75,
        'background': 'lgreen'
      },
      {
          'table_column': 'morning_balance_500',
          'display_column_name': 'Avl. Bal. 500',
          'isDisplayColumn': 'true',
          'dataType': 'currency',
          'isEditable': false,
          'width': 75,
          'background': 'lgreen'
        },
        {
          'table_column': 'morning_balance_2000',
          'display_column_name': 'Avl. Bal. 2000',
          'isDisplayColumn': 'true',
          'dataType': 'currency',
          'isEditable': false,
          'width': 75,
          'background': 'lgreen'
        },
        {
            'table_column': 'total_morning_balance',
            'display_column_name': 'Tot. Bal',
            'isDisplayColumn': 'true',
            'dataType': 'currency',
            'isEditable': false,
            'width': 75,
            'background': 'lgreen'
          },
          {
              'table_column': 'avgdispense',
              'display_column_name': 'Avg. Dispense',
              'isDisplayColumn': 'true',
              'dataType': 'currency',
              'isEditable': false,
              'width': 80,
              'background': 'no'
            },
            {
              'table_column': 'purpose',
              'display_column_name': 'Purpose',
              'isDisplayColumn': 'true',
              'dataType': 'select',
              'isEditable': true,
              'width': 85,
              'background': 'no'
            },
    // {
    //   'table_column': 'loading_amount_50',
    //   'display_column_name': 'Loading Amount Deno (50)',
    //   'isDisplayColumn': 'true',
    //   'dataType': 'string',
    //   'isEditable': true
    // },

    {
      'table_column': 'loading_amount_100',
      'display_column_name': '100',
      'isDisplayColumn': 'true',
      'dataType': 'currency',
      'isEditable': true,
      'width': 85,
      'background': 'lyellow'
    },
    {
      'table_column': 'loading_amount_200',
      'display_column_name': '200',
      'isDisplayColumn': 'true',
      'dataType': 'currency',
      'isEditable': true,
      'width': 85,
      'background': 'lyellow'
    },
    {
      'table_column': 'loading_amount_500',
      'display_column_name': '500',
      'isDisplayColumn': 'true',
      'dataType': 'currency',
      'isEditable': true,
      'width': 85,
      'background': 'lyellow'
    },
    {
      'table_column': 'loading_amount_2000',
      'display_column_name': '2000',
      'isDisplayColumn': 'true',
      'dataType': 'currency',
      'isEditable': true,
      'width': 85,
      'background': 'lyellow'
    },
    {
      'table_column': 'total',
      'display_column_name': 'Total Amt.',
      'isDisplayColumn': 'true',
      'dataType': 'string',
      'isEditable': true,
      'width': 85,
      'background': 'lyellow'
    }
    // ,
    // {
    //   'table_column': 'record_status',
    //   'display_column_name': 'Record Status',
    //   'isDisplayColumn': 'true',
    //   'dataType': 'string',
    //   'isEditable': false
    // }
  ];

  columnConfigBrandbillRef = [{
      'file_type': 'BRANDBILLCAPACITY',
      'table_column': 'brand_code',
      'display_column_name': 'ATM Brand',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BRANDBILLCAPACITY',
      'table_column': 'description',
      'display_column_name': 'Description',
      'dataType': 'string',
      'isEditable': 'false',
    },
    {
      'file_type': 'BRANDBILLCAPACITY',
      'table_column': 'capacity_50',
      'display_column_name': 'Capacity 50',
      'dataType': 'string',
      'isEditable': 'false',
    },
    {
      'file_type': 'BRANDBILLCAPACITY',
      'table_column': 'capacity_100',
      'display_column_name': 'Capacity 100',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BRANDBILLCAPACITY',
      'table_column': 'capacity_200',
      'display_column_name': 'Capacity 200',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BRANDBILLCAPACITY',
      'table_column': 'capacity_500',
      'display_column_name': 'Capacity 500',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BRANDBILLCAPACITY',
      'table_column': 'capacity_2000',
      'display_column_name': 'Capacity 2000',
      'dataType': 'string',
      'isEditable': 'false'
    }
  ];

  columnAtmList = [{
    'file_type': 'ATM_LIST',
    'table_column': 'atm_id',
    'display_column_name': 'Atm Id',
    'dataType': 'string',
    'isEditable': 'true'
  },
  {
    'file_type': 'ATM_LIST',
    'table_column': 'bank_code',
    'display_column_name': 'Bank Code',
    'dataType': 'string',
    'isEditable': 'false',
  },
  {
    'file_type': 'ATM_LIST',
    'table_column': 'created_reference_id',
    'display_column_name': 'Created Reference Id',
    'dataType': 'string',
    'isEditable': 'false',
  },
  {
    'file_type': 'ATM_LIST',
    'table_column': 'feeder_branch_code',
    'display_column_name': 'Feeder Branch Code',
    'dataType': 'string',
    'isEditable': 'false'
  },
  {
    'file_type': 'ATM_LIST',
    'table_column': 'project_id',
    'display_column_name': 'Project Id',
    'dataType': 'string',
    'isEditable': 'false'
  },
  {
    'file_type': 'ATM_LIST',
    'table_column': 'record_status',
    'display_column_name': 'Record Status',
    'dataType': 'string',
    'isEditable': 'false'
  },
  {
    'file_type': 'ATM_LIST',
    'table_column': 'site_code',
    'display_column_name': 'Site Code',
    'dataType': 'string',
    'isEditable': 'false'
  }
];
  columnConfigCraVaultRef = [{
      'file_type': 'CRAVAULTMASTER',
      'table_column': 'vault_code',
      'display_column_name': 'Vault Code',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAVAULTMASTER',
      'table_column': 'cra_code',
      'display_column_name': 'CRA Name',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAVAULTMASTER',
      'table_column': 'state',
      'display_column_name': 'State',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAVAULTMASTER',
      'table_column': 'location',
      'display_column_name': 'Location',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAVAULTMASTER',
      'table_column': 'address',
      'display_column_name': 'Address',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAVAULTMASTER',
      'table_column': 'vault_type',
      'display_column_name': 'Vault_Type',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAVAULTMASTER',
      'table_column': 'vault_status',
      'display_column_name': 'Vault_Status',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAVAULTMASTER',
      'table_column': 'contact_details',
      'display_column_name': 'Contact_Details',
      'dataType': 'string',
      'isEditable': 'false'
    }
  ];

  ColumnConfigCraEmpaneledRef = [{
      'file_type': 'CRAEMPANELED',
      'table_column': 'cra_name',
      'display_column_name': 'CRA Name',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAEMPANELED',
      'table_column': 'registered_office',
      'display_column_name': 'Registered Office',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAEMPANELED',
      'table_column': 'registration_number',
      'display_column_name': 'Registration No',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAEMPANELED',
      'table_column': 'contact_details',
      'display_column_name': 'Contact Details',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAEMPANELED',
      'table_column': 'cra_code',
      'display_column_name': 'CRA_Code',
      'dataType': 'string',
      'isEditable': 'false'
    }
  ];
  ColumnConfigCraEscalationRef = [{
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'cra',
      'display_column_name': 'CRA',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'location',
      'display_column_name': 'Location',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'location_circle',
      'display_column_name': 'Circle',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'activity',
      'display_column_name': 'Activity',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'primary_email_id',
      'display_column_name': 'Primary Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'primary_contact_no',
      'display_column_name': 'Primary Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'level_1_email_id',
      'display_column_name': 'Level 1 Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'level_2_email_id',
      'display_column_name': 'Level 2 Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'level_3_email_id',
      'display_column_name': 'Level 3 Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'level_4_email_id',
      'display_column_name': 'Level 4 Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'level_1_contact_no',
      'display_column_name': 'Level 1 Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'level_2_contact_no',
      'display_column_name': 'Level 2 Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'level_3_contact_no',
      'display_column_name': 'Level 3 Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'CRAESCALATIONMATRIX',
      'table_column': 'level_4_contact_no',
      'display_column_name': 'Level 4 Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    }
  ];
  ColumnConfigBankEscalationRef = [{
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'project_id',
      'display_column_name': 'Project ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'bank',
      'display_column_name': 'Bank',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'branch',
      'display_column_name': 'Branch',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'sol_id',
      'display_column_name': 'SOL ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'circle',
      'display_column_name': 'Circle',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'activity',
      'display_column_name': 'Activity',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'primary_email_id',
      'display_column_name': 'Primary Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'primary_contact_no',
      'display_column_name': 'Primary Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'level_1_contact_no',
      'display_column_name': 'Level 1 Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'level_2_contact_no',
      'display_column_name': 'Level 2 Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'level_3_contact_no',
      'display_column_name': 'Level 3 Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'level_1_email_id',
      'display_column_name': 'Level 1 Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'level_2_email_id',
      'display_column_name': 'Level 2 Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'level_3_email_id',
      'display_column_name': 'Level 3 Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'level_4_email_id',
      'display_column_name': 'Level 4 Email ID',
      'dataType': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'BANKESCALATIONMATRIX',
      'table_column': 'level_4_contact_no',
      'display_column_name': 'Level 4 Contact NO',
      'dataType': 'string',
      'isEditable': 'false'
    }
  ];

  mailmasterColumnConfig = [
  	{
  		"table_column": "project_id",
  		"display_column_name": "Project Id",
  		"dataType": "string",
  		"isEditable": "false"
  	},
  	{
  		"table_column": "bank_code",
  		"display_column_name": "Bank",
  		"dataType": "string",
  		"isEditable": "false"
  	},
  	{
  		"table_column": "feeder_branch",
  		"display_column_name": "Feeder Branch",
  		"dataType": "string",
  		"isEditable": "false"
  	},
  	{
  		"table_column": "sol_id",
  		"display_column_name": "SOL Id",
  		"dataType": "string",
  		"isEditable": "true"
  	},
  	{
  		"table_column": "cra",
  		"display_column_name": "CRA",
  		"dataType": "string",
  		"isEditable": "false"
  	},
    {
      "table_column": "from_email_id",
      "display_column_name": "From Email Id",
      "dataType": "string",
      "isEditable": "true"
    },
  	{
  		"table_column": "to_bank_email_id",
  		"display_column_name": "To Bank Email Id",
  		"dataType": "string",
  		"isEditable": "true"
  	},
  	{
  		"table_column": "to_cra_email_id",
  		"display_column_name": "To CRA Email Id",
  		"dataType": "string",
  		"isEditable": "true"
  	},
  	{
  		"table_column": "bcc_email_id",
  		"display_column_name": "BCC Email Id",
  		"dataType": "string",
  		"isEditable": "true"
  	},
  	{
  		"table_column": "subject",
  		"display_column_name": "Subject",
  		"dataType": "string",
  		"isEditable": "true"
  	},
  	{
  		"table_column": "body_text",
  		"display_column_name": "Body Text",
  		"dataType": "string",
  		"isEditable": "true"
  	},
  	{
  		"table_column": "state",
  		"display_column_name": "State",
  		"dataType": "string",
  		"isEditable": "false"
  	},
  	{
  		"table_column": "importance",
  		"display_column_name": "Importance",
  		"dataType": "string",
  		"isEditable": "true"
  	},
  	{
  		"table_column": "atm_count",
  		"display_column_name": "Atm Count",
  		"dataType": "string",
  		"isEditable": "true"
  	},
  	{
  		"table_column": "status",
  		"display_column_name": "Status",
  		"dataType": "string",
  		"isEditable": "true"
  	}
  ];

  ColumnConfigEpsEscalationRef = [{
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'activity',
      'display_column_name': 'Activity',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'primary_email_id',
      'display_column_name': 'Primary Email ID',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'primary_contact_no',
      'display_column_name': 'Primary Contact NO',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'level_1_email_id',
      'display_column_name': 'Level 1 Email ID',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'level_2_email_id',
      'display_column_name': 'Level 2 Email ID',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'level_3_email_id',
      'display_column_name': 'Level 3 Email ID',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'level_4_email_id',
      'display_column_name': 'Level 4 Email ID',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'level_1_contact_no',
      'display_column_name': 'Level 1 Contact NO',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'eps',
      'display_column_name': 'EPS',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'location',
      'display_column_name': 'Location',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'level_2_contact_no',
      'display_column_name': 'Level 2 Contact NO',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'level_3_contact_no',
      'display_column_name': 'Level 3 Contact NO',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'file_type': 'EPSESCALATIONMATRIX',
      'table_column': 'level_4_contact_no',
      'display_column_name': 'Level 4 Contact NO',
      'datatype': 'string',
      'isEditable': 'false'
    }
  ];
  exceptionsColumnConfig = [{
      'table_column': 'ticket_id',
      'display_column_name': 'Ticket ID'
    },
    {
      'table_column': 'subject',
      'display_column_name': 'Subject'
    },
    {
      'table_column': 'remarks',
      'display_column_name': 'Remarks'
    },
    {
      'table_column': 'additional_info',
      'display_column_name': 'Additional Information'
    },
    {
      'table_column': 'category_id',
      'display_column_name': 'Category ID'
    },
    {
      'table_column': 'assign_to_user',
      'display_column_name': 'Assigned To User'
    },
    {
      'table_column': 'assign_to_group',
      'display_column_name': 'Assigned To Group'
    },
    {
      'table_column': 'status',
      'display_column_name': 'Status'
    }
  ];
  indenRevisionColumnConfig = [{
      'table_column': 'indent_short_code',
      'display_column_name': 'EPS Ref. No.',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    {
      'table_column': 'indent_revision_order_number',
      'display_column_name': 'Indent Revision Order Number',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    {
      'table_column': 'indent_order_number',
      'display_column_name': 'Indent Order Number',
      'isDisplayColumn': 'true',
      'dataType': 'string'
    },
    {
      'table_column': 'total_original_forecast_amount',
      'display_column_name': 'Total Original Forecast Amt.',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    // {
    //   'table_column': 'original_forecast_amount_50',
    //   'display_column_name': 'Original Forecast Ammount 50',
    //   'isDisplayColumn': 'true',
    //   'dataType': 'currency'
    // },
    {
      'table_column': 'original_forecast_amount_100',
      'display_column_name': 'Original Forecast Amt. 100',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'original_forecast_amount_200',
      'display_column_name': 'Original Forecast Amt. 200',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'original_forecast_amount_500',
      'display_column_name': 'Original Forecast Amt. 500',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'original_forecast_amount_2000',
      'display_column_name': 'Original Forecast Amt. 2000',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'total_amount_available',
      'display_column_name': 'Total Amt. Availbl.',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    // {
    //   'table_column': 'available_50_amount',
    //   'display_column_name': 'Available Amount 50',
    //   'isDisplayColumn': 'true',
    //   'dataType': 'string'
    // },
    {
      'table_column': 'available_100_amount',
      'display_column_name': 'Availbl. Amt. 100',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'available_200_amount',
      'display_column_name': 'Availbl. Amt. 200',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'available_500_amount',
      'display_column_name': 'Availbl. Amt. 500',
      'isDisplayColumn': 'true',
      'dataType': 'currency'
    },
    {
      'table_column': 'available_2000_amount',
      'display_column_name': 'Availbl. Amt. 2000',
      'isDisplayColumn': 'false',
      'dataType': 'currency'
    },
    {
      'table_column': 'project_id',
      'display_column_name': 'Project',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'feeder_branch_code',
      'display_column_name': 'Feeder Br.',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'is_withdrawal_done',
      'display_column_name': 'Is Withdrawal Done',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'reason',
      'display_column_name': 'Reason',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'remarks',
      'display_column_name': 'Remarks',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'record_status',
      'display_column_name': 'Status',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'created_on',
      'display_column_name': 'Created On',
      'isDisplayColumn': 'false',
      'dataType': 'date'
    },
    {
      'table_column': 'created_by',
      'display_column_name': 'Created By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'approved_on',
      'display_column_name': 'Approved On',
      'isDisplayColumn': 'false',
      'dataType': 'date'
    },
    {
      'table_column': 'approved_by',
      'display_column_name': 'Approved By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'rejected_on',
      'display_column_name': 'Rejected On',
      'isDisplayColumn': 'false',
      'dataType': 'date'
    },
    {
      'table_column': 'rejected_by',
      'display_column_name': 'Rejected By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'deleted_on',
      'display_column_name': 'Deleted On',
      'isDisplayColumn': 'false',
      'dataType': 'date'
    },
    {
      'table_column': 'deleted_by',
      'display_column_name': 'Deleted By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'modified_on',
      'display_column_name': 'Modified On',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'modified_by',
      'display_column_name': 'Modified By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'approved_by',
      'display_column_name': 'Approved By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'rejected_on',
      'display_column_name': 'Rejected On',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    },
    {
      'table_column': 'rejected_by',
      'display_column_name': 'Rejected By',
      'isDisplayColumn': 'false',
      'dataType': 'string'
    }
  ];


  ColumnConfigSignature = [{
      'table_column': 'project_id',
      'display_column_name': 'Project ID'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code'
    },
    {
      'table_column': 'signatory_name',
      'display_column_name': 'Signatory Name'
    },
    {
      'table_column': 'designation',
      'display_column_name': 'Designation'
    },
    {
      'table_column': 'physical_signature',
      'display_column_name': 'Physical Signature'
    },
    {
      'table_column': 'record_status',
      'display_column_name': 'Record Status'
    }
  ];
  ColumnConfigEodActivity = [{
      'table_column': 'project_id',
      'display_column_name': 'Project ID',
      'datatype': 'string'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string'
    },
    {
      'table_column': 'region_code',
      'display_column_name': 'Region Code',
      'datatype': 'string'
    },
    {
      'table_column': 'branch_code',
      'display_column_name': 'Branch Code',
      'datatype': 'string'
    },
    {
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'datatype': 'string'
    },
    {
      'table_column': 'site_code',
      'display_column_name': 'Site Code',
      'datatype': 'string'
    },
    {
      'table_column': 'datafor_date_time',
      'display_column_name': 'DataFor Date Time',
      'datatype': 'date'
    },
    {
      'table_column': 'record_status',
      'display_column_name': 'Record Status',
      'datatype': 'string'
    }
  ];
  ColumnConfigQualifyatms = [
	{
		"table_column": "project_id",
		"display_column_name": "Project Id",
		"datatype": "string",
		"modifyApproval": "false"
	},
	{
		"table_column": "bank_code",
		"display_column_name": "Bank Code",
		"datatype": "string",
		"modifyApproval": "false"
	},
	{
		"table_column": "feeder_branch_code",
		"display_column_name": "Feeder Branch Code",
		"datatype": "string",
		"modifyApproval": "false"
	},
	{
		"table_column": "site_code",
		"display_column_name": "Site Code",
		"datatype": "string",
		"modifyApproval": "false"
	},
	{
		"table_column": "atm_id",
		"display_column_name": "Atm Id",
		"datatype": "string",
		"modifyApproval": "false"
	},
	{
		"table_column": "category",
		"display_column_name": "Category",
		"datatype": "string",
		"modifyApproval": "false"
	},
	{
		"table_column": "is_qualified",
		"display_column_name": "Is Atm Qualified",
		"datatype": "select",
		"modifyApproval": "true"
	},
	{
		"table_column": "from_date",
		"display_column_name": "From Date",
		"datatype": "date",
		"modifyApproval": "false"
	},
	{
		"table_column": "to_date",
		"display_column_name": "To Date",
		"datatype": "date",
		"modifyApproval": "true"
	},
	{
		"table_column": "comment",
		"display_column_name": "Comment",
		"datatype": "string",
		"modifyApproval": "true"
	},
	{
		"table_column": "record_status",
		"display_column_name": "Record Status",
		"datatype": "string",
		"modifyApproval": "false"
	}
];

ColumnConfigDefaultAmount = [
	{
		'table_column': 'project_id',
    'display_column_name': 'Project ID',
    'dataType': 'string'

	},
	{
		'table_column': 'bank_code',
		'display_column_name': 'Bank Code',
    'dataType': 'string'
	},
	{
		'table_column': 'amount',
		'display_column_name': 'Amount',
    'dataType': 'string'
	}
];
  columnSystemSettings = [
  {
    'table_column': 'id',
      'display_column_name': 'ID',
      'isDisplayColumn': 'false',
      'dataType': 'number'
  },
  {
    'table_column': 'confidence_factor',
      'display_column_name': 'Confidence Factor',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'buffer_percentage',
      'display_column_name': 'Buffer Percentage',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'denomination_wise_round_off_100',
      'display_column_name': 'Denomination Wise Round Off 100',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'denomination_wise_round_off_200',
      'display_column_name': 'Denomination Wise Round Off 200',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'denomination_wise_round_off_500',
      'display_column_name': 'Denomination Wise Round Off 500',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'denomination_wise_round_off_2000',
      'display_column_name': 'Denomination Wise Round Off 2000',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'fixed_loading_amount',
      'display_column_name': 'Fixed Loading Amount',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'default_average_dispense',
      'display_column_name': 'Default Average Dispense',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'vaulting_for_normal_weekday_percentage',
      'display_column_name': 'Vaulting Normal Weekday Percentage',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'vaulting_for_normal_weekend_percentage',
      'display_column_name': 'Vaulting Normal Weekend Percentage',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'vaulting_for_extended_weekend_percentage',
      'display_column_name': 'Vaulting Extended Weekend Percentage',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'dispenseformula',
      'display_column_name': 'Dispense Formula',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_100_max_capacity_percentage',
      'display_column_name': 'Denomination Max Capacity Percentage 100',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_200_max_capacity_percentage',
      'display_column_name': 'Denomination Max Capacity Percentage 200',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_500_max_capacity_percentage',
      'display_column_name': 'Denomination Max Capacity Percentage 500',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_2000_max_capacity_percentage',
      'display_column_name': 'Denomination Max Capacity Percentage 2000',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_100_priority',
      'display_column_name': 'Denomination Priority 100',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_200_priority',
      'display_column_name': 'Denomination Priority 200',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_500_priority',
      'display_column_name': 'Denomination Priority 500',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_2000_priority',
      'display_column_name': 'Denomination Priority 2000',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_100_bill_capacity',
      'display_column_name': 'Denomination Bill Capacity 100',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_200_bill_capacity',
      'display_column_name': 'Denomination Bill Capacity 200',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_500_bill_capacity',
      'display_column_name': 'Denomination Bill Capacity 500',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'deno_2000_bill_capacity',
      'display_column_name': 'Denomination Bill Capacity 2000',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'cash_out_logic',
      'display_column_name': 'Cash Out Logic',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  },
  {
    'table_column': 'forecasting_algorithm',
      'display_column_name': 'Forecasting Algorithms',
      'isDisplayColumn': 'true',
      'dataType': 'string'
  }];
  indentAdminColumnConfig = [{
    'table_column': 'bank_code',
    'display_column_name': 'Bank Code',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'project_id',
    'display_column_name': 'Project ID',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'feeder_branch',
    'display_column_name': 'Feeder Branch',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'indent_short_code',
    'display_column_name': 'Indent Short Code',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'indent_order_number',
    'display_column_name': 'Indent Order Number',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'indent_date',
    'display_column_name': 'Indent No',
    'isDisplayColumn': 'true',
    'datatype': 'date'
  },
  {
    'table_column': 'pdf_status',
    'display_column_name': 'PDF Status',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'mail_status',
    'display_column_name': 'Mail Status',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  }];
  CashavailabilityColumnConfig = [{
    'table_column': 'from_date',
    'display_column_name': 'From Date',
    'isDisplayColumn': 'true',
    'datatype': 'date'
  },
  {
    'table_column': 'to_date',
    'display_column_name': 'To Date',
    'isDisplayColumn': 'true',
    'datatype': 'date'
  },
  {
    'table_column': 'project_id',
    'display_column_name': 'Project',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'bank_code',
    'display_column_name': 'Bank',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'feeder_branch_code',
    'display_column_name': 'Feeder Branch',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'available_100_amount',
    'display_column_name': 'Available 100 Amount',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'available_200_amount',
    'display_column_name': 'Available 200 Amount',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'available_500_amount',
    'display_column_name': 'Available 500 Amount',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'available_2000_amount',
    'display_column_name': 'Available 2000 Amount',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'total_amount_available',
    'display_column_name': 'Total Amount Available',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  }
  ];
    activityStatusColumnConfig = [{
    'table_column': 'project_id',
    'display_column_name': 'Project ID',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'region',
    'display_column_name': 'Region',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'bank_name',
    'display_column_name': 'Bank',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'data_for_type',
    'display_column_name': 'Type',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'data_status',
    'display_column_name': 'Status',
    'isDisplayColumn': 'true',
    'datatype': 'string'
  },
  {
    'table_column': 'latest_date_time',
    'display_column_name': 'Time',
    'isDisplayColumn': 'true',
    'datatype': 'date'
  }
  ];
  menuPersissionMappingColumn = [{
    'table_column': 'group_name',
    'display_column_name': 'Group Name'
  },
  {
    'table_column': 'parent_display_name',
    'display_column_name': 'Parent Menu'
  },
  {
    'table_column': 'child_display_name',
    'display_column_name': 'Sub Menu'
  },
  {
    'table_column': 'permission_name',
    'display_column_name': 'Pemission Level'
  },
  {
    'table_column': 'created_on',
    'display_column_name': 'Created On'
  }
  ];


  columnConfigLimits = [
    {
      'table_column': 'eps_site_code',
      'display_column_name': 'Eps Site Code',
      'dataType': 'string'
    },
    {
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'dataType': 'string'
    },
    {
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'dataType': 'string'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'dataType': 'string'
    },
    {
      'table_column': 'indent_date',
      'display_column_name': 'Indent Date',
      'dataType': 'date'
    },
    {
      'table_column': 'next_feasible_date',
      'display_column_name': 'Next Feasible Date',
      'dataType': 'date'
    },
    {
      'table_column': 'next_working_date',
      'display_column_name': 'Next Working Date',
      'dataType': 'date'
    }, {
      'table_column': 'decidelimitdays',
      'display_column_name': 'Decide Limit Days',
      'dataType': 'string'
    }, {
      'table_column': 'loadinglimitdays',
      'display_column_name': 'Loading Limit Days',
      'dataType': 'string'
    } ,  {
      'table_column': 'default_decide_limit_days',
      'display_column_name': 'Default Decide Limit Days',
      'dataType': 'string'
    },
    {
      'table_column': 'default_loading_limit_days',
      'display_column_name': 'Default Loading Limit Days',
      'dataType': 'string'
    }

  ];
  ColumnConfigAddUpdateVaulting = [
    {
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'dataType': 'string'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'dataType': 'string'
    },
    {
      'table_column': 'feeder_branch_code',
      'display_column_name': 'Feeder Branch Code',
      'dataType': 'string'
    },
    {
      'table_column': 'is_vaulting_allowed',
      'display_column_name': 'Is Vaulting Allowed',
      'dataType': 'string'
    },
    {
      'table_column': 'vaulting_percentage',
      'display_column_name': 'Vaulting Percentage',
      'dataType': 'string'
    },
    {
      'table_column': 'from_date',
      'display_column_name': 'From Date',
      'dataType': 'date'
    },
    {
      'table_column': 'to_date',
      'display_column_name': 'To Date',
      'dataType': 'date'
    },
    {
      'table_column': 'record_status',
      'display_column_name': 'record Status',
      'dataType': 'string'
    },
    {
      'table_column': 'created_on',
      'display_column_name': 'Created On',
      'dataType': 'date'
    },
    {
      'table_column': 'created_by',
      'display_column_name': 'Created By',
      'dataType': 'string'
    },
    {
      'table_column': 'created_reference_id',
      'display_column_name': 'Created Reference Id',
      'dataType': 'string'
    },
    {
      'table_column': 'approved_on',
      'display_column_name': 'Approved On',
      'dataType': 'date'
    },
    {
      'table_column': 'approved_by',
      'display_column_name': 'Approved By',
      'dataType': 'string'
    },
    {
      'table_column': 'approved_reference_id',
      'display_column_name': 'Approved Reference Id',
      'dataType': 'string'
    },
    {
      'table_column': 'approve_reject_comment',
      'display_column_name': 'Approve Reject Comment',
      'dataType': 'string'
    },
    {
      'table_column': 'rejected_on',
      'display_column_name': 'Rejected On',
      'dataType': 'date'
    },
    {
      'table_column': 'rejected_by',
      'display_column_name': 'Rejected By',
      'dataType': 'string'
    },
    {
      'table_column': 'reject_reference_id',
      'display_column_name': 'Reject Reference Id',
      'dataType': 'string'
    },
    {
      'table_column': 'modified_on',
      'display_column_name': 'Modified On',
      'dataType': 'date'
    },
    {
      'table_column': 'modified_by',
      'display_column_name': 'Modified By',
      'dataType': 'string'
    }
  ];
  ColumnConfigHolidays = [
    {
      'table_column': 'holiday_code',
      'display_column_name': 'Holiday Code',
      'datatype': 'string'
    },
    {
      'table_column': 'holiday_name',
      'display_column_name': 'Holiday Name',
      'datatype': 'string'
    },
    {
      'table_column': 'state',
      'display_column_name': 'State',
      'datatype': 'string'
    },
    {
      'table_column': 'start_date',
      'display_column_name': 'Start Date',
      'datatype': 'date'
    },
    {
      'table_column': 'end_date',
      'display_column_name': 'End Date',
      'datatype': 'date'
    }
  ];
  columnconfigCyphercode = [
    {
      'table_column': 'category',
      'display_column_name': 'Category',
      'datatype': 'string'
    },
    {
      'table_column': 'value',
      'display_column_name': 'Value',
      'datatype': 'string'
    },
    {
      'table_column': 'cypher_code',
      'display_column_name': 'Cypher Code',
      'datatype': 'string'
    },
    {
      'table_column': 'is_applicable_to_all_projects',
      'display_column_name': 'Is Applicable to All Project',
      'datatype': 'string'
    },
    {
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'datatype': 'string'
    },
    {
      'table_column': 'is_applicable_to_all_banks',
      'display_column_name': 'Is Applicable to All Bank',
      'datatype': 'string'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string'
    },
    {
      'table_column': 'record_status',
      'display_column_name': 'Record Status',
      'datatype': 'string'
    },
    {
      'table_column': 'created_on',
      'display_column_name': 'Created On',
      'datatype': 'date'
    },
    {
      'table_column': 'created_by',
      'display_column_name': 'Created By',
      'datatype': 'string'
    },
    {
      'table_column': 'created_reference_id',
      'display_column_name': 'Created Reference Id',
      'datatype': 'string'
    },
    {
      'table_column': 'approved_on',
      'display_column_name': 'Approved On',
      'datatype': 'string'
    },
    {
      'table_column': 'approved_by',
      'display_column_name': 'Approved By',
      'datatype': 'string'
    },
    {
      'table_column': 'approved_reference_id',
      'display_column_name': 'Approved Reference Id',
      'datatype': 'string'
    },
    {
      'table_column': 'approve_reject_comment',
      'display_column_name': 'Approve Reject Comment',
      'datatype': 'string'
    },
    {
      'table_column': 'rejected_on',
      'display_column_name': 'Rejected On',
      'datatype': 'date'
    },
    {
      'table_column': 'rejected_by',
      'display_column_name': 'Rejected By',
      'datatype': 'string'
    },
    {
      'table_column': 'reject_reference_id',
      'display_column_name': 'Reject Reference Id',
      'datatype': 'string'
    },
    {
      'table_column': 'modified_on',
      'display_column_name': 'Modified On',
      'datatype': 'date'
    },
    {
      'table_column': 'modified_by',
      'display_column_name': 'Modified By',
      'datatype': 'string'
    },
    {
      'table_column': 'modified _reference_id',
      'display_column_name': 'Modified Reference Id',
      'datatype': 'string'
    }
  ];
  columnConfigDenominationpriority = [
    {
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'datatype': 'string'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string'
    },
    {
      'table_column': 'feeder_branch_code',
      'display_column_name': 'Feeder Barnch Code',
      'datatype': 'string'
    },
    {
      'table_column': 'denomination_100',
      'display_column_name': 'Denomination 100',
      'datatype': 'string'
    },
    {
      'table_column': 'denomination_200',
      'display_column_name': 'Denomination 200',
      'datatype': 'string'
    },
    {
      'table_column': 'denomination_500',
      'display_column_name': 'Denomination 500',
      'datatype': 'string'
    },
    {
      'table_column': 'denomination_2000',
      'display_column_name': 'Denomination 2000',
      'datatype': 'string'
    }
  ];
  coloumnConfigcapacityPercentage = [
    {
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'dataType': 'string'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'dataType': 'string'
    },
    {
      'table_column': 'feeder_branch_code',
      'display_column_name': 'Feeder Barnch Code',
      'dataType': 'string'
    },
    {
      'table_column': 'denomination_100',
      'display_column_name': 'Denomination 100',
      'dataType': 'string'
    },
    {
      'table_column': 'denomination_200',
      'display_column_name': 'Denomination 200',
      'dataType': 'string'
    },
    {
      'table_column': 'denomination_500',
      'display_column_name': 'Denomination 500',
      'dataType': 'string'
    },
    {
      'table_column': 'denomination_2000',
      'display_column_name': 'Denomination 2000',
      'dataType': 'string'
    },
    {
      'table_column': 'record_status',
      'display_column_name': 'Record Status',
      'dataType': 'string'
    }
  ];
  ColumnConfigConsolidation = [
    {
      'table_column': 'site_code',
      'display_column_name': 'Site Code',
      'datatype': 'string'
    },
    {
      'table_column': 'atm_id',
      'display_column_name': 'Atm Id',
      'datatype': 'string'
    },
    {
      'table_column': 'atm_band',
      'display_column_name': 'Atm Band',
      'datatype': 'string'
    },
    {
      'table_column': 'brand',
      'display_column_name': 'Brand',
      'datatype': 'string'
    },
    {
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'datatype': 'string'
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string'
    },
    {
      'table_column': 'state',
      'display_column_name': 'State',
      'datatype': 'string'
    }, {
      'table_column': 'district',
      'display_column_name': 'District',
      'datatype': 'string'
    }, {
      'table_column': 'city',
      'display_column_name': 'City',
      'datatype': 'string'
    }, {
      'table_column': 'site_status',
      'display_column_name': 'Site Status',
      'datatype': 'string'
    }, {
      'table_column': 'insurance_limit',
      'display_column_name': 'Insurance Limit',
      'datatype': 'string'
    }, {
      'table_column': 'whether_critical_atm',
      'display_column_name': 'Whether Critical Atm',
      'datatype': 'string'
    }, {
      'table_column': 'cassette_50_count',
      'display_column_name': 'Cassette 50 Count',
      'datatype': 'string'
    }, {
      'table_column': 'cassette_200_count',
      'display_column_name': 'Cassette 200 Count',
      'datatype': 'string'
    }, {
      'table_column': 'cassette_500_count',
      'display_column_name': 'Cassette 500 Count',
      'datatype': 'string'
    }, {
      'table_column': 'cassette_2000_count',
      'display_column_name': 'Cassette 2000 Count',
      'datatype': 'string'
    }, {
      'table_column': 'total_cassette_count',
      'display_column_name': 'Total Cassette Count',
      'datatype': 'string'
    }, {
      'table_column': 'bank_cash_limit',
      'display_column_name': 'Bank Cash Limit',
      'datatype': 'string'
    }, {
      'table_column': 'base_limit',
      'display_column_name': 'Base Limit',
      'datatype': 'string'
    }, {
      'table_column': 'feeder_branch_code',
      'display_column_name': 'Feeder Branch Code',
      'datatype': 'string'
    }, {
      'table_column': 'new_cra',
      'display_column_name': 'New CRA',
      'datatype': 'string'
    }, {
      'table_column': 'feasibility_or_loading_frequency',
      'display_column_name': 'Feasibility or Loading Frequency',
      'datatype': 'string'
    }, {
      'table_column': 'br_document_status',
      'display_column_name': 'Branch Document Status',
      'datatype': 'string'
    }, {
      'table_column': 'vaulting',
      'display_column_name': 'Vaulting',
      'datatype': 'string'
    }, {
      'table_column': 'is_feasible_mon',
      'display_column_name': 'Is Feasible Mon',
      'datatype': 'string'
    }, {
      'table_column': 'is_feasible_tue',
      'display_column_name': 'Is Feasible Tue',
      'datatype': 'string'
    }, {
      'table_column': 'is_feasible_wed',
      'display_column_name': 'Is Feasible Wed',
      'datatype': 'string'
    }, {
      'table_column': 'is_feasible_thu',
      'display_column_name': 'Is Feasible Thus',
      'datatype': 'string'
    }, {
      'table_column': 'is_feasible_fri',
      'display_column_name': 'Is Feasible Fri',
      'datatype': 'string'
    }, {
      'table_column': 'is_feasible_sat',
      'display_column_name': 'Is Feasible Sat',
      'datatype': 'string'
    }, {
      'table_column': 'region_code',
      'display_column_name': 'Region Code',
      'datatype': 'string'
    }, {
      'table_column': 'sol_id',
      'display_column_name': 'Sol Id',
      'datatype': 'string'
    }, {
      'table_column': 'feeder_branch',
      'display_column_name': 'Feeder Branch',
      'datatype': 'string'
    }, {
      'table_column': 'feeder_linked_count',
      'display_column_name': 'Feeder Linked Count',
      'datatype': 'string'
    }, {
      'table_column': 'is_vaulting_enabled',
      'display_column_name': 'Is Vaulting Enabled',
      'datatype': 'string'
    }, {
      'table_column': 'alternate_cash_balance',
      'display_column_name': 'Alternate Cash Balance',
      'datatype': 'string'
    }, {
      'table_column': 'is_currency_chest',
      'display_column_name': 'Is Currency Chest',
      'datatype': 'string'
    }];
    columnConfigindentReport = [
      {
        'table_column': 'ATMID',
        'display_column_name': 'Atm Id',
        'bank_name': 'BOMH',
        'type': 'Indent Report'
      },
      {
        'table_column': 'INDENT DATE',
        'display_column_name': 'Indentdate',
        'bank_name': 'BOMH',
        'type': 'Indent Report'
      },
      {
        'table_column': 'VENDOR',
        'display_column_name': 'Vendor',
        'bank_name': 'BOMH',
        'type': 'Indent Report'
      },
      {
        'table_column': 'INDENT NO',
        'display_column_name': 'Indent No',
        'bank_name': 'BOMH',
        'type': 'Indent Report'
      },
      {
        'table_column': 'CODE',
        'display_column_name': 'Code',
        'bank_name': 'BOMH',
        'type': 'Indent Report'
      },
      {
        'table_column': 'Feeder Branch',
        'display_column_name': 'Feeder Branch',
        'bank_name': 'BOMH',
        'type': 'Indent Report'
      },
      {
        'table_column': 'Location',
        'display_column_name': 'Location Name',
        'bank_name': 'BOMH',
        'type': 'Indent Report'
      },
         {
        'table_column': 'CRA',
        'display_column_name': 'Cra',
        'bank_name': 'BOMH',
        'type': 'Indent Report'
      },
    {
      'table_column': '100',
      'display_column_name': 'Final Total Loading Amount 100',
      'bank_name': 'BOMH',
      'type': 'Indent Report'
    },
    {
      'table_column': '200',
      'display_column_name': 'Final Total Loading Amount 200',
      'bank_name': 'BOMH',
      'type': 'Indent Report'
    },  {
      'table_column': '500',
      'display_column_name': 'Final Total Loading Amount 500',
      'bank_name': 'BOMH',
      'type': 'Indent Report'
    },
    {
      'table_column': '2000',
      'display_column_name': 'Final Total Loading Amount 2000',
      'bank_name': 'BOMH',
      'type': 'Indent Report'
    },
    {
      'table_column': 'Total',
      'display_column_name': 'Final Total Loading Amount',
      'bank_name': 'BOMH',
      'type': 'Indent Report'
    },

  {
    'table_column': 'SR. NO.',
    'display_column_name': 'SR. NO.',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'ATM ID',
    'display_column_name': 'Atm Id',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'BANK',
    'display_column_name': 'Bank',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
     {
    'table_column': 'CRA',
    'display_column_name': 'Cra',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'DATE',
    'display_column_name': 'Indentdate',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'DISTRICT',
    'display_column_name': 'District',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'FEEDER BRANCH',
    'display_column_name': 'Feeder Branch',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'LOCATION',
    'display_column_name': 'Location Name',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Planning-100',
    'display_column_name': 'Planning 100',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Planning-200',
    'display_column_name': 'Planning 200',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Planning-500',
    'display_column_name': 'Planning 500',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Planning-2000',
    'display_column_name': 'Planning 2000',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Planning-Total',
    'display_column_name': 'Planning Total',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Revised Planning-100',
    'display_column_name': 'Revised Planning 100',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Revised Planning-200',
    'display_column_name': 'Revised Planning 200',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Revised Planning-500',
    'display_column_name': 'Revised Planning 500',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Revised Planning-2000',
    'display_column_name': 'Revised Planning 2000',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Revised Planning-Total',
    'display_column_name': 'Revised Planning Total',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'STATE / REGIONS',
    'display_column_name': 'STATE / REGIONS',
    'bank_name': 'BOMH',
    'type': 'Loading Recommendation'
  },

  {
    'table_column': 'SrNo',
    'display_column_name': 'Sr No',
    'bank_name': 'DENA',
    'type': 'Indent Report'
  },
  {
    'table_column': 'ATMID',
    'display_column_name': 'Atm Id',
    'bank_name': 'DENA',
    'type': 'Indent Report'
  },
  {
    'table_column': 'INDENT DATE',
    'display_column_name': 'Indentdate',
    'bank_name': 'DENA',
    'type': 'Indent Report'
  },


  {
    'table_column': 'Feeder Branch',
    'display_column_name': 'Feeder Branch',
    'bank_name': 'DENA',
    'type': 'Indent Report'
  },
  {
    'table_column': 'Location',
    'display_column_name': 'Location Name',
    'bank_name': 'DENA',
    'type': 'Indent Report'
  },
     {
    'table_column': 'CRA',
    'display_column_name': 'Cra',
    'bank_name': 'DENA',
    'type': 'Indent Report'
  },
  {
  'table_column': '100',
  'display_column_name': 'Final Total Loading Amount 100',
  'bank_name': 'DENA',
  'type': 'Indent Report'
  },
  {
  'table_column': '200',
  'display_column_name': 'Final Total Loading Amount 200',
  'bank_name': 'DENA',
  'type': 'Indent Report'
  },  {
  'table_column': '500',
  'display_column_name': 'Final Total Loading Amount 500',
  'bank_name': 'DENA',
  'type': 'Indent Report'
  },
  {
  'table_column': '2000',
  'display_column_name': 'Final Total Loading Amount 2000',
  'bank_name': 'DENA',
  'type': 'Indent Report'
  },
  {
  'table_column': 'Total',
  'display_column_name': 'Final Total Loading Amount',
  'bank_name': 'DENA',
  'type': 'Indent Report'
  },

  {
    'table_column': 'SrNo',
    'display_column_name': 'Sr No',
    'bank_name': 'DENA',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'ATM ID',
    'display_column_name': 'Atm Id',
    'bank_name': 'DENA',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'INDENT DATE',
    'display_column_name': 'Indentdate',
    'bank_name': 'DENA',
    'type': 'Loading Recommendation'
  },


  {
    'table_column': 'Feeder Branch',
    'display_column_name': 'Feeder Branch',
    'bank_name': 'DENA',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Location',
    'display_column_name': 'Location Name',
    'bank_name': 'DENA',
    'type': 'Loading Recommendation'
  },
     {
    'table_column': 'CRA',
    'display_column_name': 'Cra',
    'bank_name': 'DENA',
    'type': 'Loading Recommendation'
  },
  {
  'table_column': '100',
  'display_column_name': 'Final Total Loading Amount 100',
  'bank_name': 'DENA',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': '200',
  'display_column_name': 'Final Total Loading Amount 200',
  'bank_name': 'DENA',
  'type': 'Loading Recommendation'
  },  {
  'table_column': '500',
  'display_column_name': 'Final Total Loading Amount 500',
  'bank_name': 'DENA',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': '2000',
  'display_column_name': 'Final Total Loading Amount 2000',
  'bank_name': 'DENA',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': 'Total',
  'display_column_name': 'Final Total Loading Amount',
  'bank_name': 'DENA',
  'type': 'Loading Recommendation'
  },


  {
    'table_column': 'ATMID',
    'display_column_name': 'Atm Id',
    'bank_name': 'ALB',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'INDENT DATE',
    'display_column_name': 'Indentdate',
    'bank_name': 'ALB',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'BANK',
    'display_column_name': 'Bank',
    'bank_name': 'ALB',
    'type': 'Loading Recommendation'
  },

  {
    'table_column': 'Feeder Branch',
    'display_column_name': 'Feeder Branch',
    'bank_name': 'ALB',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Location',
    'display_column_name': 'Location Name',
    'bank_name': 'ALB',
    'type': 'Loading Recommendation'
  },
     {
    'table_column': 'CRA',
    'display_column_name': 'Cra',
    'bank_name': 'ALB',
    'type': 'Loading Recommendation'
  },
  {
  'table_column': '100',
  'display_column_name': 'Final Total Loading Amount 100',
  'bank_name': 'ALB',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': '200',
  'display_column_name': 'Final Total Loading Amount 200',
  'bank_name': 'ALB',
  'type': 'Loading Recommendation'
  },  {
  'table_column': '500',
  'display_column_name': 'Final Total Loading Amount 500',
  'bank_name': 'ALB',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': '2000',
  'display_column_name': 'Final Total Loading Amount 2000',
  'bank_name': 'ALB',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': 'Total',
  'display_column_name': 'Final Total Loading Amount',
  'bank_name': 'ALB',
  'type': 'Loading Recommendation'
  },

  {
    'table_column': 'ATMID',
    'display_column_name': 'Atm Id',
    'bank_name': 'ALB',
    'type': 'Indent Report'
  },
  {
    'table_column': 'INDENT DATE',
    'display_column_name': 'Indentdate',
    'bank_name': 'ALB',
    'type': 'Indent Report'
  },
  {
    'table_column': 'BANK',
    'display_column_name': 'Bank',
    'bank_name': 'ALB',
    'type': 'Indent Report'
  },

  {
    'table_column': 'Feeder Branch',
    'display_column_name': 'Feeder Branch',
    'bank_name': 'ALB',
    'type': 'Indent Report'
  },
  {
    'table_column': 'Location',
    'display_column_name': 'Location Name',
    'bank_name': 'ALB',
    'type': 'Indent Report'
  },
     {
    'table_column': 'CRA',
    'display_column_name': 'Cra',
    'bank_name': 'ALB',
    'type': 'Indent Report'
  },
  {
  'table_column': '100',
  'display_column_name': 'Final Total Loading Amount 100',
  'bank_name': 'ALB',
  'type': 'Indent Report'
  },
  {
  'table_column': '200',
  'display_column_name': 'Final Total Loading Amount 200',
  'bank_name': 'ALB',
  'type': 'Indent Report'
  },  {
  'table_column': '500',
  'display_column_name': 'Final Total Loading Amount 500',
  'bank_name': 'ALB',
  'type': 'Indent Report'
  },
  {
  'table_column': '2000',
  'display_column_name': 'Final Total Loading Amount 2000',
  'bank_name': 'ALB',
  'type': 'Indent Report'
  },
  {
  'table_column': 'Total',
  'display_column_name': 'Final Total Loading Amount',
  'bank_name': 'ALB',
  'type': 'Indent Report'
  },




  {
    'table_column': 'ATMID',
    'display_column_name': 'Atm Id',
    'bank_name': 'LVB',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'INDENT DATE',
    'display_column_name': 'Indentdate',
    'bank_name': 'LVB',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'BANK',
    'display_column_name': 'Bank',
    'bank_name': 'LVB',
    'type': 'Loading Recommendation'
  },

  {
    'table_column': 'Feeder Branch',
    'display_column_name': 'Feeder Branch',
    'bank_name': 'LVB',
    'type': 'Loading Recommendation'
  },
  {
    'table_column': 'Location',
    'display_column_name': 'Location Name',
    'bank_name': 'LVB',
    'type': 'Loading Recommendation'
  },
     {
    'table_column': 'CRA',
    'display_column_name': 'Cra',
    'bank_name': 'LVB',
    'type': 'Loading Recommendation'
  },
  {
  'table_column': '100',
  'display_column_name': 'Final Total Loading Amount 100',
  'bank_name': 'LVB',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': '200',
  'display_column_name': 'Final Total Loading Amount 200',
  'bank_name': 'LVB',
  'type': 'Loading Recommendation'
  },  {
  'table_column': '500',
  'display_column_name': 'Final Total Loading Amount 500',
  'bank_name': 'LVB',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': '2000',
  'display_column_name': 'Final Total Loading Amount 2000',
  'bank_name': 'LVB',
  'type': 'Loading Recommendation'
  },
  {
  'table_column': 'Total',
  'display_column_name': 'Final Total Loading Amount',
  'bank_name': 'LVB',
  'type': 'Loading Recommendation'
  },


  {
    'table_column': 'ATMID',
    'display_column_name': 'Atm Id',
    'bank_name': 'LVB',
    'type': 'Indent Report'
  },
  {
    'table_column': 'INDENT DATE',
    'display_column_name': 'Indentdate',
    'bank_name': 'LVB',
    'type': 'Indent Report'
  },
  {
    'table_column': 'BANK',
    'display_column_name': 'Bank',
    'bank_name': 'LVB',
    'type': 'Indent Report'
  },

  {
    'table_column': 'Feeder Branch',
    'display_column_name': 'Feeder Branch',
    'bank_name': 'LVB',
    'type': 'Indent Report'
  },
  {
    'table_column': 'Location',
    'display_column_name': 'Location Name',
    'bank_name': 'LVB',
    'type': 'Indent Report'
  },
     {
    'table_column': 'CRA',
    'display_column_name': 'Cra',
    'bank_name': 'LVB',
    'type': 'Indent Report'
  },
  {
  'table_column': '100',
  'display_column_name': 'Final Total Loading Amount 100',
  'bank_name': 'LVB',
  'type': 'Indent Report'
  },
  {
  'table_column': '200',
  'display_column_name': 'Final Total Loading Amount 200',
  'bank_name': 'LVB',
  'type': 'Indent Report'
  },  {
  'table_column': '500',
  'display_column_name': 'Final Total Loading Amount 500',
  'bank_name': 'LVB',
  'type': 'Indent Report'
  },
  {
  'table_column': '2000',
  'display_column_name': 'Final Total Loading Amount 2000',
  'bank_name': 'LVB',
  'type': 'Indent Report'
  },
  {
  'table_column': 'Total',
  'display_column_name': 'Final Total Loading Amount',
  'bank_name': 'LVB',
  'type': 'Indent Report'
  }
  ];
  ColumnConfigAddUpdateDenomination = [{
    'table_column': 'user',
    'display_column_name': 'User',

  },
{
  'table_column': 'user',
  'display_column_name': 'User',

}];
ColumnConfigUserroleMapping = [{
  'table_column': 'group_id',
  'display_column_name': 'Group',

},
{
'table_column': 'user_id',
'display_column_name': 'User',

}, {
  'table_column': 'rejected_on',
  'display_column_name': 'Rejected On',

  }, {
    'table_column': 'rejected_by',
    'display_column_name': 'Rejected By',

    }, {
      'table_column': 'reject_reference_id',
      'display_column_name': 'Reject Reference Id',

      }, {
        'table_column': 'record_status',
        'display_column_name': 'Record Status',

        }
        , {
          'table_column': 'modified_reference_id',
          'display_column_name': 'Modified Reference Id',

          }
          , {
            'table_column': 'modified_on',
            'display_column_name': 'Modified On',

            }
            , {
              'table_column': 'modified_by',
              'display_column_name': 'Modified By',

              }
              , {
                'table_column': 'deleted_reference_id',
                'display_column_name': 'Deleted Reference Id',

                }, {
                  'table_column': 'deleted_on',
                  'display_column_name': 'Deleted On',

                  }, {
                    'table_column': 'deleted_by',
                    'display_column_name': 'Deleted By',

                    }
                    , {
                      'table_column': 'created_reference_id',
                      'display_column_name': 'Created Reference Id',

                      }, {
                        'table_column': 'created_on',
                        'display_column_name': 'Created On',

                        }, {
                          'table_column': 'created_by',
                          'display_column_name': 'Created By',

                          }, {
                            'table_column': 'approved_reference_id',
                            'display_column_name': 'Approved Reference Id',

                            }, {
                              'table_column': 'approved_on',
                              'display_column_name': 'Approved On',

                              }, {
                                'table_column': 'approved_by',
                                'display_column_name': 'Approved By',

                                }

];
ColumnConfigUserProjectMapping = [
{
'table_column': 'user_id',
'display_column_name': 'User',

}, {
  'table_column': 'project_id',
  'display_column_name': 'Project',

}, {
  'table_column': 'bank_code',
  'display_column_name': 'Bank',

}];
ColumnConfigProjectList = [
  {
  'table_column': 'project_id',
  'display_column_name': 'Project Id',

  }];
  ColumnConfigbankList = [
    {
    'table_column': 'bank_code',
    'display_column_name': 'Bank Code',
    'isEditable': 'true'

    }, {
      'table_column': 'bank_name',
      'display_column_name': 'Bank Name',
      'isEditable': 'false'
      }];
      ColumnConfigcraList = [
        {
        'table_column': 'cra_id',
        'display_column_name': 'CRA Id',
        'isEditable': 'true'

        },  {
          'table_column': 'cra_name',
          'display_column_name': 'CRA Name',
          'isEditable': 'true'

          }];
        ColumnConfigregionList = [
          {
          'table_column': 'region_name',
          'display_column_name': 'Region Name ',
          'isEditable': 'true'

          }];
ColumnConfigRejectionReasons = [
{
'table_column': 'category',
'display_column_name': 'Category',
'dataType': 'string',
'isEditable': 'false'

}, {
  'table_column': 'rejection_reason',
  'display_column_name': 'Reason',
  'dataType': 'string',
  'isEditable': 'true'

}, {
  'table_column': 'record_status',
  'display_column_name': 'Status',
  'dataType': 'string',
  'isEditable': 'false'

}];
ColumnConfigOverwriteReasons = [
{
'table_column': 'category',
'display_column_name': 'Category',
'dataType': 'string',
'isEditable': 'false'

}, {
  'table_column': 'overwrite_reason',
  'display_column_name': 'Reason',
  'dataType': 'string',
  'isEditable': 'true'

}, {
  'table_column': 'record_status',
  'display_column_name': 'Status',

  'dataType': 'string',
  'isEditable': 'false'

}];
ColumnConfigUpdateReasons = [
{
'table_column': 'category',
'display_column_name': 'Category',
'dataType': 'string',
'isEditable': 'false'

}, {
  'table_column': 'update_reason',
  'display_column_name': 'Reason',
  'dataType': 'string',
  'isEditable': 'true'

}, {
  'table_column': 'record_status',
  'display_column_name': 'Status',
  'dataType': 'string',
  'isEditable': 'false'

}];
ColumnConfigZeroCashDispense = [
{
'table_column': 'project_id',
'display_column_name': 'Project',
'dataType': 'string',
'isEditable': 'false'

}, {
  'table_column': 'bank_name',
  'display_column_name': 'Bank',
  'dataType': 'string',
  'isEditable': 'true'

}, {
  'table_column': 'feeder_branch',
  'display_column_name': 'Feeder Branch',
  'dataType': 'string',
  'isEditable': 'false'

}, {
  'table_column': 'atm_id',
  'display_column_name': 'ATM ID',
  'dataType': 'string',
  'isEditable': 'false'

}, {
  'table_column': 'site_code',
  'display_column_name': 'SITE CODE',
  'dataType': 'string',
  'isEditable': 'false'

}, {
  'table_column': 'cra',
  'display_column_name': 'CRA',
  'dataType': 'string',
  'isEditable': 'false'

}, {
  'table_column': 'datafor_date_time',
  'display_column_name': 'Date',
  'dataType': 'date',
  'isEditable': 'false'

}, {
  'table_column': 'total_dispense_amount',
  'display_column_name': 'Total Dispense Amount',
  'dataType': 'string',
  'isEditable': 'false'

}];
bankLimitColumns = [
	{
		'tabe_column': 'project_id',
		'display_column_name': 'Project Code',
		'datatype': 'string',
    'isEditable': 'true'
	},
	{
		'tabe_column': 'bank_code',
		'display_column_name': 'Bank Code',
		'datatype': 'string',
    'isEditable': 'false'
	},
	{
		'tabe_column': 'decide_limit_days',
		'display_column_name': 'Decide Limit Days',
		'datatype': 'string',
    'isEditable': 'true'
	},
	{
		'tabe_column': 'loading_limit_days',
		'display_column_name': 'Loading Limit Days',
		'datatype': 'string',
    'isEditable': 'true'
	},
	{
		'tabe_column': 'fordate',
		'display_column_name': 'For Date',
		'datatype': 'date',
    'isEditable': 'true'
	},
	{
		'tabe_column': 'record_status',
		'display_column_name': 'Record Status',
		'datatype': 'string',
    'isEditable': 'false'
	}
];
feederColList=[
	{
		"tabe_column": "project_id",
		"display_column_name": "Project Code",
		"datatype": "string",
    'isEditable': true
	},
	{
		"tabe_column": "bank_code",
		"display_column_name": "Bank Code",
		"datatype": "string",
    'isEditable': false
	},
	{
		"tabe_column": "decide_limit_days",
		"display_column_name": "Decide Limit Days",
		"datatype": "string",
    'isEditable': true
	},
	{
		"tabe_column": "loading_limit_days",
		"display_column_name": "Loading Limit Days",
		"datatype": "string",
    'isEditable': true
	},
	{
		"tabe_column": "fordate",
		"display_column_name": "For Date",
		"datatype": "date",
    'isEditable': true
	},
	{
		"tabe_column": "feeder",
		"display_column_name": "Feeder Name",
		"datatype": "string",
    'isEditable': false
	},
	{
		"tabe_column": "cra",
		"display_column_name": "CRA",
		"datatype": "string",
    'isEditable': true
	},
	{
		"tabe_column": "record_status",
		"display_column_name": "Record Status",
		"datatype": "string",
    'isEditable': false
	}

];
  configurableDenoColumnConfig =
    [
  {
    "table_column": "project_id",
    "display_column_name": "Project ID",
    "datatype": "string"
  },
  {
    "table_column": "feeder_branch_code",
    "display_column_name": "Feeder Branch Code",
    "datatype": "string"
  },
  {
    "table_column": "bank_code",
    "display_column_name": "Bank Code",
    "datatype": "string"
  },
  {
    "table_column": "is_50_available",
    "display_column_name": "50 Available",
    "datatype": "string"
  },
  {
    "table_column": "is_100_available",
    "display_column_name": "100 Available",
    "datatype": "string"
  },
  {
    "table_column": "is_200_available",
    "display_column_name": "200 Available",
    "datatype": "string"
  },
  {
    "table_column": "is_500_available",
    "display_column_name": "500 Available",
    "datatype": "string"
  },
  {
    "table_column": "is_2000_available",
    "display_column_name": "2000 Available",
    "datatype": "string"
  },
  {
    "table_column": "from_date",
    "display_column_name": "From Date",
    "datatype": "date"
  },
  {
    "table_column": "to_date",
    "display_column_name": "To Date",
    "datatype": "date"
  }
];

    columnConfigindentSummaryReport = [
	{
		"table_column": "atm_id",
		"display_column_name": "Atm Id"
	},
	{
		"table_column": "atm_band",
		"display_column_name": "Atm Band"
	},
	{
		"table_column": "site_code",
		"display_column_name": "Site Code"
	},
	{
		"table_column": "bank_code",
		"display_column_name": "Bank Code"
	},
	{
		"table_column": "feeder_branch_code",
		"display_column_name": "Feeder Branch Code"
	},
	{
		"table_column": "indent_code",
		"display_column_name": "Indent Code"
	},
	{
		"table_column": "atm_priority",
		"display_column_name": "Atm Priority"
	},
	{
		"table_column": "ignore_code",
		"display_column_name": "Ignore Code"
	},
	{
		"table_column": "ignore_description",
		"display_column_name": "Ignore Description"
	},
	{
		"table_column": "reason_for_priority",
		"display_column_name": "Reason For Priority"
	},
	{
		"table_column": "project_id",
		"display_column_name": "Project Id"
	},
	{
		"table_column": "indentdate",
		"display_column_name": "Indent Date"
	},
	{
		"table_column": "CashOut",
		"display_column_name": "Cash Out"
	},
	{
		"table_column": "bank_decide_limit_days",
		"display_column_name": "Bank Decide Limit Days"
	},
	{
		"table_column": "bank_loading_limit_days",
		"display_column_name": "Bank Loading Limit Days"
	},
	{
		"table_column": "bank_cash_limit",
		"display_column_name": "Bank Cash Limit"
	},
	{
		"table_column": "insurance_limit",
		"display_column_name": "Isurance Limit"
	},
	{
		"table_column": "applied_vaulting_percentage",
		"display_column_name": "Applied Vaulting Percentage"
	},
	{
		"table_column": "cra",
		"display_column_name": "CRA"
	},
	{
		"table_column": "base_limit",
		"display_column_name": "Base Limit"
	},
	{
		"table_column": "dispenseformula",
		"display_column_name": "Dispense Formula"
	},
	{
		"table_column": "decidelimitdays",
		"display_column_name": "Decide Limit Days"
	},
	{
		"table_column": "loadinglimitdays",
		"display_column_name": "Loading Limit Days"
	},
	{
		"table_column": "deno_100_priority",
		"display_column_name": "Priority Deno. 100"
	},
	{
		"table_column": "deno_200_priority",
		"display_column_name": "Priority Deno. 200"
	},
	{
		"table_column": "deno_500_priority",
		"display_column_name": "Priority Deno. 500"
	},
	{
		"table_column": "deno_2000_priority",
		"display_column_name": "Priority Deno. 2000"
	},
	{
		"table_column": "avgdispense",
		"display_column_name": "Average Dispense"
	},
	{
		"table_column": "morning_balance_100",
		"display_column_name": "Morning Balance 100"
	},
	{
		"table_column": "morning_balance_200",
		"display_column_name": "Morning Balance 200"
	},
	{
		"table_column": "morning_balance_500",
		"display_column_name": "Morning Balance 500"
	},
	{
		"table_column": "morning_balance_2000",
		"display_column_name": "Morning Balance 2000"
	},
	{
		"table_column": "total_morning_balance",
		"display_column_name": "Total Morning Balance"
	},
	{
		"table_column": "avgdecidelimit",
		"display_column_name": "Average Decide Limit"
	},
	{
		"table_column": "DecideLimit",
		"display_column_name": "Decide Limit"
	},
	{
		"table_column": "threshold_limit",
		"display_column_name": "Threshold Limit"
	},
	{
		"table_column": "loadinglimit",
		"display_column_name": "Loading Limit"
	},
	{
		"table_column": "loadingGap_cur_bal_avg_disp",
		"display_column_name": "Loading Gap"
	},
	{
		"table_column": "loading_amount",
		"display_column_name": "Loading Amount"
	},
	{
		"table_column": "tentative_loading_amount_100",
		"display_column_name": "Tentative Loading Amount 100"
	},
	{
		"table_column": "tentative_loading_amount_200",
		"display_column_name": "Tentative Loading Amount 200"
	},
	{
		"table_column": "tentative_loading_amount_500",
		"display_column_name": "Tentative Loading Amount 500"
	},
	{
		"table_column": "tentative_loading_amount_2000",
		"display_column_name": "Tentative Loading Amount 2000"
	},
	{
		"table_column": "rounded_amount_100",
		"display_column_name": "Rounded Amount 100"
	},
	{
		"table_column": "rounded_amount_200",
		"display_column_name": "Rounded Amount 200"
	},
	{
		"table_column": "rounded_amount_500",
		"display_column_name": "Rounded Amount 500"
	},
	{
		"table_column": "rounded_amount_2000",
		"display_column_name": "Rounded Amount 2000"
	},
	{
		"table_column": "expected_balanceT1_100",
		"display_column_name": "Expected Balance Deno. 100"
	},
	{
		"table_column": "expected_balanceT1_200",
		"display_column_name": "Expected Balance Deno. 200"
	},
	{
		"table_column": "expected_balanceT1_500",
		"display_column_name": "Expected Balance Deno. 500"
	},
	{
		"table_column": "expected_balanceT1_2000",
		"display_column_name": "Expected Balance Deno. 2000"
	},
	{
		"table_column": "total_expected_balanceT1",
		"display_column_name": "Total Expected Balance"
	},
	{
		"table_column": "final_total_loading_amount_100",
		"display_column_name": "Final Total Loading Amount 100"
	},
	{
		"table_column": "final_total_loading_amount_200",
		"display_column_name": "Final Total Loading Amount 200"
	},
	{
		"table_column": "final_total_loading_amount_500",
		"display_column_name": "Final Total Loading Amount 500"
	},
	{
		"table_column": "final_total_loading_amount_2000",
		"display_column_name": "Final Total Loading Amount 2000"
	},
	{
		"table_column": "final_total_loading_amount",
		"display_column_name": "Final Total Loading Amount"
	},
	{
		"table_column": "pre_avail_loading_amount_100",
		"display_column_name": "Pre-Available Loading Amount 100"
	},
	{
		"table_column": "pre_avail_loading_amount_200",
		"display_column_name": "Pre-Available Loading Amount 200"
	},
	{
		"table_column": "pre_avail_loading_amount_500",
		"display_column_name": "Pre-Available Loading Amount 500"
	},
	{
		"table_column": "pre_avail_loading_amount_2000",
		"display_column_name": "Pre-Available Loading Amount 2000"
	},
	{
		"table_column": "previous_indent_code",
		"display_column_name": "Previous Indent Code"
	},
	{
		"table_column": "is_zero_dispense",
		"display_column_name": "Is Zero Dispense"
	},
	{
		"table_column": "indent_type",
		"display_column_name": "Indent Type"
	},
	{
		"table_column": "indent_counter",
		"display_column_name": "Indent Counter"
	},
	{
		"table_column": "curbal_div_avgDisp",
		"display_column_name": "Current Average Disp. Bal."
	},
	{
		"table_column": "default_flag",
		"display_column_name": "Default Falg"
	},
	{
		"table_column": "cassette_100_count",
		"display_column_name": "Cassette Count Deno. 100"
	},
	{
		"table_column": "cassette_200_count",
		"display_column_name": "Cassette Count Deno. 200"
	},
	{
		"table_column": "cassette_500_count",
		"display_column_name": "Cassette Count Deno. 500"
	},
	{
		"table_column": "cassette_2000_count",
		"display_column_name": "Cassette Count Deno. 2000"
	},
	{
		"table_column": "total_cassette_capacity",
		"display_column_name": "Total Cassette Capacity"
	},
	{
		"table_column": "created_on",
		"display_column_name": "Created On"
	},
	{
		"table_column": "record_status",
		"display_column_name": "Status"
	}
];
    columnDiversionStatus = [{
      'table_column': 'indent_order_number',
      'display_column_name': 'Indent Order Number',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'table_column': 'bank',
      'display_column_name': 'Bank',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'table_column': 'feeder_branch',
      'display_column_name': 'Feeder Branch',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'table_column': 'region',
      'display_column_name': 'Region',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'table_column': 'cra',
      'display_column_name': 'CRA',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'table_column': 'original_atm_id',
      'display_column_name': 'Original Atm Id',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'table_column': 'diverted_atm_id',
      'display_column_name': 'Diverted Atm Id',
      'datatype': 'string',
      'isEditable': 'true'
    },
    {
      'table_column': 'denomination_100',
      'display_column_name': 'Denomination 100',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'table_column': 'denomination_200',
      'display_column_name': 'Denomination 200',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'table_column': 'denomination_500',
      'display_column_name': 'Denomination 500',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'table_column': 'denomination_2000',
      'display_column_name': 'Denomination 2000',
      'datatype': 'string',
      'isEditable': 'false'
    },
    {
      'table_column': 'total_diversion_amount',
      'display_column_name': 'Total Diversion Amount',
      'datatype': 'string',
      'isEditable': 'false'
    }
  ];

  columnVaultingStatus = [{
    'table_column': 'indent_order_id',
    'display_column_name': 'Indent Order Number',
    'datatype': 'string',
    'isEditable': 'true'
  },
  {
    'table_column': 'bank_code',
    'display_column_name': 'Bank',
    'datatype': 'string',
    'isEditable': 'true'
  },
  {
    'table_column': 'feeder_branch_code',
    'display_column_name': 'Feeder Branch',
    'datatype': 'string',
    'isEditable': 'true'
  },
  {
    'table_column': 'record_status',
    'display_column_name': 'Record Status',
    'datatype': 'string',
    'isEditable': 'true'
  },

  {
    'table_column': 'atm_id',
    'display_column_name': 'Atm Id',
    'datatype': 'string',
    'isEditable': 'true'
  },
  {
    'table_column': 'vaulting_date',
    'display_column_name': 'Vaulting Date',
    'datatype': 'date',
    'isEditable': 'true'
  },
  {
    'table_column': 'cra',
    'display_column_name': 'Cra',
    'datatype': 'string',
    'isEditable': 'true'
  },
  {
    'table_column': 'reason_for_vaulting',
    'display_column_name': 'Reason For Vaulting',
    'datatype': 'string',
    'isEditable': 'false'
  },
  {
    'table_column': 'comments',
    'display_column_name': 'Comments',
    'datatype': 'string',
    'isEditable': 'false'
  },
  {
    'table_column': 'amount_100',
    'display_column_name': 'Denomination 100',
    'datatype': 'string',
    'isEditable': 'false'
  },
  {
    'table_column': 'amount_200',
    'display_column_name': 'Denomination 200',
    'datatype': 'string',
    'isEditable': 'false'
  },
  {
    'table_column': 'amount_500',
    'display_column_name': 'Denomination 500',
    'datatype': 'string',
    'isEditable': 'false'
  },
  {
    'table_column': 'amount_2000',
    'display_column_name': 'Denomination 2000',
    'datatype': 'string',
    'isEditable': 'false'
  },
  {
    'table_column': 'total_amount',
    'display_column_name': 'Total Diversion Amount',
    'datatype': 'string',
    'isEditable': 'true'
  }
];
columnIndentHoliday = [
    {
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'datatype': 'string',
      'isEditable': false
    },
    {
      'table_column': 'bank_code',
      'display_column_name': 'Bank',
      'datatype': 'string',
      'isEditable': false
    },
    {
      'table_column': 'feeder_branch',
      'display_column_name': 'Feeder Branch',
      'datatype': 'string',
      'isEditable': false
    },
    {
      'table_column': 'cra',
      'display_column_name': 'Cra',
      'datatype': 'string',
      'isEditable': false
    },
    {
      'table_column': 'holiday_date',
      'display_column_name': 'Holiday Date',
      'datatype': 'date',
      'isEditable': false
    },{
      'table_column': 'holiday_name',
      'display_column_name': 'Holiday Name',
      'datatype': 'string',
      'isEditable': false
    },
    {
      'table_column': 'withdrawal_type',
      'display_column_name': 'Withdrawal Type',
      'datatype': 'select',
      'isEditable': true
    },
    {
      'table_column': 'is_indent_required',
      'display_column_name': 'Is Indent Required',
      'datatype': 'select',
      'isEditable': true
    },{
      'table_column': 'state_code',
      'display_column_name': 'State',
      'datatype': 'string',
      'isEditable': false
    },{
      'table_column': 'district_code',
      'display_column_name': 'District',
      'datatype': 'string',
      'isEditable': false
    },{
      'table_column': 'city_code',
      'display_column_name': 'City',
      'datatype': 'string',
      'isEditable': false
    },{
      'table_column': 'area_code',
      'display_column_name': 'Area',
      'datatype': 'string',
      'isEditable': false
    }
    ];
    columnRevisionAtmCashAvailability = [
    {
      'table_column': 'project_id',
      'display_column_name': 'Project Id',
      'datatype': 'string',
      'isEditable': false
    },{
      'table_column': 'bank_code',
      'display_column_name': 'Bank Code',
      'datatype': 'string',
      'isEditable': false
    },{
      'table_column': 'feeder_branch_code',
      'display_column_name': 'Feeder Branch Code',
      'datatype': 'string',
      'isEditable': false
    },{
      'table_column': 'atm_id',
      'display_column_name': 'Atm ID',
      'datatype': 'string',
      'isEditable': true
    },{
      'table_column': 'site_code',
      'display_column_name': 'Site Code',
      'datatype': 'string',
      'isEditable': true
    },{
      'table_column': 'for_date',
      'display_column_name': 'For Date',
      'datatype': 'date',
      'isEditable': false
    },{
      'table_column': 'denomination_100',
      'display_column_name': 'Denomination 100',
      'datatype': 'string',
      'isEditable': true
    },{
      'table_column': 'denomination_200',
      'display_column_name': 'Denomination 200',
      'datatype': 'string',
      'isEditable': true
    },{
      'table_column': 'denomination_500',
      'display_column_name': 'Denomination 500',
      'datatype': 'string',
      'isEditable': true
    },{
      'table_column': 'denomination_2000',
      'display_column_name': 'Denomination 2000',
      'datatype': 'string',
      'isEditable': true
    },{
      'table_column': 'total_amount',
      'display_column_name': 'Total Amount',
      'datatype': 'string',
      'isEditable': false
    }
    ];

    columnGLAccount = [
      {
        'table_column': 'atm_id',
        'display_column_name': 'Atm ID',
        'datatype': 'string',
        'isEditable': true
      },{
        'table_column': 'site_code',
        'display_column_name': 'Site Code',
        'datatype': 'string',
        'isEditable': true
      },{
        'table_column': 'bank_code',
        'display_column_name': 'Bank Code',
        'datatype': 'string',
        'isEditable': true
      },{
        'table_column': 'gl_acc_number',
        'display_column_name': 'GL Account Number',
        'datatype': 'string',
        'isEditable': true
      }
    ];

    userColumnList = [
      {
        'table_column': 'username',
        'display_column_name': 'Username',
        'datatype': 'string',
        'isEditable': false
      },{
        'table_column': 'first_name',
        'display_column_name': 'First Name',
        'datatype': 'string',
        'isEditable': false
      },{
        'table_column': 'last_name',
        'display_column_name': 'Last Name',
        'datatype': 'string',
        'isEditable': false
      },{
        'table_column': 'email',
        'display_column_name': 'E-mail',
        'datatype': 'string',
        'isEditable': false
      },{
        'table_column': 'group_id',
        'display_column_name': 'Role',
        'datatype': 'string',
        'isEditable': true
      },{
        'table_column': 'record_status',
        'display_column_name': 'Status',
        'datatype': 'status',
        'isEditable': true
      }
    ]
}
