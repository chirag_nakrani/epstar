import { Pipe, PipeTransform } from '@angular/core';
const padding = "000000";

@Pipe({
  name: 'amountMask'
})
export class AmountMaskPipe implements PipeTransform {
  private prefix: string;
  private decimal_separator:string;
  private thousands_separator:string;
  private suffix:string;

  constructor(){
    this.prefix='';
    this.suffix='';
    this.decimal_separator='.';
    this.thousands_separator = ',';
  }
  transform(value: string, fractionSize:number = 0 ): string {
    let [ integer, fraction = ""] = (parseFloat(value).toString() || "").toString().split(".");
    if(value.length >= 4){
      if(parseFloat(value) % 1 != 0)
      {
        fractionSize = 2;
      }

      fraction = fractionSize > 0
        ? this.decimal_separator + (fraction+padding).substring(0, fractionSize) : "";
      integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.thousands_separator);
      const intArr = integer.split(',');
      let firstInt = '';
      for(let i = 0; i < intArr.length-1; i++){
        firstInt += intArr[i];
      }
      firstInt = firstInt.replace(/\B(?=(\d{2})+(?!\d))/g, this.thousands_separator);
      integer = firstInt+ this.thousands_separator+ intArr[intArr.length-1];
      if(isNaN(parseFloat(integer)))
      {
            integer = "0";
      }
        return this.prefix + integer + fraction + this.suffix;
    }else{
      integer = parseFloat(value).toString();
      return this.prefix + integer + this.suffix;
    }
  }

  parse(value: string, fractionSize: number = 2): string {
    let [ integer, fraction = "" ] = (value || "").replace(this.prefix, "")
                                                  .replace(this.suffix, "")
                                                  .split(this.decimal_separator);

    integer = integer.replace(new RegExp(this.thousands_separator, "g"), "");

    fraction = parseInt(fraction, 10) > 0 && fractionSize > 0
      ? this.decimal_separator + (fraction + padding).substring(0, fractionSize)
      : "";

    return integer + fraction;
  }

}
