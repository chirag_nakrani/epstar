import { Injectable } from '@angular/core';
import { Observable} from 'rxjs/Observable';
import { interval } from 'rxjs';
import { map } from 'rxjs/operators';
import {  Router } from '@angular/router';
import { CookieStorage, LocalStorage, SessionStorage } from 'ngx-store';
import { CookiesStorageService, LocalStorageService, SessionStorageService, SharedStorageService } from 'ngx-store';


@Injectable({
  providedIn: 'root'
})


export class LogoutService {
  lastAction: Date;

  constructor(private sessionStorageService: SessionStorageService, private router: Router) {
    this.check();
    this.initListener();
    this.initInterval();
  }

  now: Date = new Date();
  CHECK_INTERVAL = 1000; // in ms
  STORE_KEY =  'lastAction';

  getLastAction() {
    return +this.sessionStorageService.get(this.STORE_KEY);
  }
  setLastAction(value) {
    this.sessionStorageService.set(this.STORE_KEY, value);
  }

  initListener() {
    document.body.addEventListener('click', () => this.reset());
  }

  reset() {
    this.setLastAction(new Date());
  }

  initInterval() {//start session validation check in speified interval
    const inteval = setInterval(() => {
      this.check();
    }, this.CHECK_INTERVAL);
  }

  check() {//Check for session expiration
    const now = new Date();
    const timeleft: Date = this.sessionStorageService.get('sessionAliveTime');
    const diff = new Date(timeleft).valueOf() - now.valueOf();
    const isTimeout = diff < 0;
    this.sessionStorageService.set('logoutMessage', '');
    if (isTimeout && this.sessionStorageService.get('accessToken') && this.sessionStorageService.get('accessToken') != '') {
      this.sessionStorageService.set('accessToken', '');
      this.sessionStorageService.set('logoutMessage', 'Please login again.');
      this.sessionStorageService.set('logoutType', 'Session Timeout');
       // rout to logout component
	    this.router.navigate(['./login']);
    }
  }
}
