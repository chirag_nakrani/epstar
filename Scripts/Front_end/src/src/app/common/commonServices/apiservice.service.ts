import { Injectable } from '@angular/core';
import {HttpHeaders} from "@angular/common/http";
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { SessionStorageService } from 'ngx-store';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  domain = environment.url;
  httpOptionsForm = {};
  httpOptions: any;
  httpOptionsImage: any;
  httpOptionsPdf = {};

  constructor(private sessionStorageService: SessionStorageService, private http: HttpClient) {
console.log(this.sessionStorageService.get('accessToken'), '---------');

this.headerOptionInit();
   }

headerOptionInit() {
  this.httpOptionsForm = {
    headers: new HttpHeaders({'Authorization': '' + this.sessionStorageService.get('accessToken')}),
    reportProgress: true,
    observe: 'events'
  };

  this.httpOptionsPdf = {
      headers: new HttpHeaders({'Authorization': '' + this.sessionStorageService.get('accessToken') }),
     'responseType': 'blob',
     'Content-Type': 'application/pdf'
  };

  this.httpOptionsImage = {
    headers: new HttpHeaders({'Authorization': '' + this.sessionStorageService.get('accessToken') }),
   'responseType': 'blob',
   'Content-Type': 'image/png'
  };

  this.httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': '' + this.sessionStorageService.get('accessToken') })
  };
}

post(url, param) {

  this.headerOptionInit();

     return this.http.post(this.domain + url, param, this.httpOptions);
}

getFile(url, param) {
  this.headerOptionInit();
  return this.http.post(this.domain + url, param, this.httpOptionsPdf);
}

downloadFile(url: string, params: any) {
  this.headerOptionInit();
  
  this.httpOptionsImage.params = params;
  return this.http.get(this.domain + url,this.httpOptionsImage);
}

get(url) {

  // this.httpOptions.params = params;
    return this.http.get(this.domain + url,this.httpOptions);
}

async getProgress(url: string, param: any) {

  const token = await this.sessionStorageService.get('accessToken');
  const httpOptionsProgress = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
    })
  }
  return this.http.post(this.domain+url, param, httpOptionsProgress).toPromise();
}

uploadFile(url, data) {
  // data["access_token"]= "978DBDSGSWNWHU";
  return this.http.post(this.domain + url, data, this.httpOptionsForm);

  //   {
  //   reportProgress: true,
  //   observe: 'events'
  // }




}

}
