import { TestBed } from '@angular/core/testing';

import { LoderService } from './loder.service';

describe('LoderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoderService = TestBed.get(LoderService);
    expect(service).toBeTruthy();
  });
});
