import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class LoderService {

  constructor(private spinner: NgxSpinnerService) { }
  setLoading(status: boolean) {
    if ( status ) {
      this.spinner.show();
    } else {
      this.spinner.hide();
    }
  }
}
