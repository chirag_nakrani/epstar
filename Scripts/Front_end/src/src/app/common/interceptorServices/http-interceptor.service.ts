import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpResponse } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { LoderService } from '../commonServices/loder.service';

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
  private totalRequests = 0;
  public spinner: NgxSpinnerService;
  constructor(private loadingService: LoderService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler) {
    if(request.url.indexOf('get-task-info') == -1) {
      this.totalRequests++;
      this.loadingService.setLoading(true);
      return next.handle(request).pipe(
        tap(res => {
          if (res instanceof HttpResponse) {
            this.decreaseRequests();
            return res;
          }
        }),
        catchError(err => {
          this.decreaseRequests();
          throw err;
        })
      );
    }else {
      return next.handle(request).pipe(
        tap(res => {
         return res;
        }),
        catchError(err => {
          throw err;
        })
      );
    }    
  }

  private decreaseRequests() {
    this.totalRequests--;
    if (this.totalRequests === 0) {
      this.loadingService.setLoading(false);
    }
  }
}
