import { TestBed } from '@angular/core/testing';

import { LoadingInterceptor } from './http-interceptor.service';

describe('HttpInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LoadingInterceptor = TestBed.get(LoadingInterceptor);
    expect(service).toBeTruthy();
  });
});
