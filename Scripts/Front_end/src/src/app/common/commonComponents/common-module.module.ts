import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ExcelExportComponentComponent } from './excel-export-component/excel-export-component.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { MultiselectComponent } from './multiselect/multiselect.component';
import { QuickSidebarComponent } from './quick-sidebar/quick-sidebar.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { BsDatepickerModule, ProgressbarModule } from 'ngx-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    DashboardComponent,
    ExcelExportComponentComponent,
    FooterComponent,
    LoginComponent,
    MultiselectComponent,
    QuickSidebarComponent,
    SideMenuComponent,
    TopNavbarComponent
  ],
  exports: [
    DashboardComponent,
    ExcelExportComponentComponent,
    FooterComponent,
    LoginComponent,
    MultiselectComponent,
    QuickSidebarComponent,
    SideMenuComponent,
    TopNavbarComponent
  ],
  imports: [
    CommonModule,
    BsDatepickerModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ProgressbarModule.forRoot()
  ]
})
export class CommonModuleModule { }
