import { Component,ViewEncapsulation, OnInit } from '@angular/core';
import { SessionStorageService } from 'ngx-store';
import {  Router } from '@angular/router';
declare var $: any;

@Component({
  selector: 'app-top-navbar',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.scss']
})
export class TopNavbarComponent implements OnInit {

  public userName: string = this.sessionStorageService.get('userName');
  public userMail: string = this.sessionStorageService.get('userEmail');

  constructor(private sessionStorageService: SessionStorageService, private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.sessionStorageService.set('accessToken', '');
    this.sessionStorageService.set('logoutMessage', 'Please login again.');
    this.sessionStorageService.set('logoutType', 'Logged Out Succesfully');
    this.router.navigate(['./login']);
  }

  minimizeToggle(){
    console.log("minimize clicked");
    var minimized=$('body').hasClass('m-brand--minimize m-aside-left--minimize');

    if(!minimized){
      $('body').addClass('m-brand--minimize m-aside-left--minimize')
    }
    else{
      $('body').removeClass('m-brand--minimize m-aside-left--minimize')
    }
  }
  minimizeTogglemobile(){
    console.log("minimize clicked");
    // var minimized=$('body').hasClass('m-brand--minimize m-aside-left--minimize');
    var minimized=$('#m_aside_left').hasClass('sidemenu-inside');
      $('#m_aside_left').addClass('sidemenu-inside');
    if(!minimized){
      $('#m_aside_left').addClass('sidemenu-inside')
    }
    else{
      $('#m_aside_left').removeClass('sidemenu-inside')
    }
  }
  quickSideBarToggle(){
    console.log('quick side bar');

    var minimized=$('body').hasClass('m-quick-sidebar--on');

    if(!minimized){
      $('body').addClass('m-quick-sidebar--on')
      $('#m_quick_sidebar').addClass('m-quick-sidebar--on');
    }
    else{
      $('body').removeClass('m-quick-sidebar--on');
      $('#m_quick_sidebar').removeClass('m-quick-sidebar--on');

    }

  }

}
