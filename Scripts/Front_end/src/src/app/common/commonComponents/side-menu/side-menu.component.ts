
import {MenulistService} from '../../commonServices/menulist.service';
import {
  Component,
  OnInit
} from '@angular/core';
import { CookiesStorageService, LocalStorageService, SessionStorageService, SharedStorageService } from 'ngx-store';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  sideMenu: object[];
  constructor(public menulist:MenulistService, private sessionStorageService: SessionStorageService) {


  this.menulist.updateMenu();
  console.log('side menu list',this.menulist.Menu);
  }


  setPageLevelPermission(menuId: string, permissionList: any) {
    const permList = [];
    permissionList.forEach( x => {
      permList.push(x.permission_name);
    });

    this.sessionStorageService.set('permissionList', permList);
    this.sessionStorageService.set('menuId', menuId);
  }


  ngOnInit() {}

}
