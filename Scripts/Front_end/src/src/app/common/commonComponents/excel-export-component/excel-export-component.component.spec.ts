import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcelExportComponentComponent } from './excel-export-component.component';

describe('ExcelExportComponentComponent', () => {
  let component: ExcelExportComponentComponent;
  let fixture: ComponentFixture<ExcelExportComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcelExportComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcelExportComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
