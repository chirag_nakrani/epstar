import { Component, OnInit, Input } from '@angular/core';
import { ExcelExportService } from './excel-export.service';

@Component({
  selector: 'app-excel-export-component',
  templateUrl: './excel-export-component.component.html',
  styleUrls: ['./excel-export-component.component.scss']
})
export class ExcelExportComponentComponent implements OnInit {

  @Input() data: any;
  @Input() colConfig: any;
  @Input() fileName: string;

  constructor(private excelExportService?: ExcelExportService) { }

  ngOnInit() {
    this.mutateJsonData();
  }

// change header for excel file
  mutateJsonData() {
    if ( this.data != undefined && this.data.length != 0 ) {
    let temp = JSON.stringify(this.data);
    this.colConfig.forEach( x => {
      if ( temp.indexOf(x.prop) > -1 ) {
        const re = new RegExp(x.prop, 'g');
        temp = temp.replace(re, x.name);
      }

    });
    this.data = JSON.parse(temp);
    } else {
      this.data = [];
      this.data[0] = {};
      this.colConfig.forEach( x => {
        this.data[0][x.name] = null;
      });
    }
  }


// export excel file
  exportAsXLSX(): void {
    this.mutateJsonData();
    this.excelExportService.exportAsExcelFile(this.data, this.fileName);
  }

}
