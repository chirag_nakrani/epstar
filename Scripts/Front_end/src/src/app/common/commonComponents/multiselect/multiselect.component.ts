import {
  Component,
  OnInit,
  Input,
  HostListener,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'app-multiselect',
  templateUrl: './multiselect.component.html',
  styleUrls: ['./multiselect.component.scss']

})
export class MultiselectComponent implements OnInit {
  @Input('col') viewColumnAll: any;
  openselect = false;

  @HostListener('document:click', ['$event'])
   clickout(event) {
     if (event.target.parentElement != null && event.target.parentElement.getAttribute('class') != 'no-action') {
     if (this.elementRef.nativeElement.contains(event.target)) {
       this.openselect = !this.openselect;
     } else {
       this.openselect = false;
       }
     }
   }
  constructor(private elementRef: ElementRef) {}

  ngOnInit() {
    this.selectAll(this.viewColumnAll);
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  selectAll(col) {
    col.forEach((x) => x.selected = true)
  }

  isAllSelected(col) {
    let flagAllSelected = true;
    col.forEach((x) => {
      if (!x.selected) {
        flagAllSelected = false
      }
    })

    return flagAllSelected;

  }

}
