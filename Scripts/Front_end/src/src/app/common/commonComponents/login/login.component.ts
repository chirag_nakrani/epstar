import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { LogoutService } from '../../commonServices/logout.service';
import { ApiService } from '../../commonServices/apiservice.service';
import { CookieStorage, LocalStorage, SessionStorage } from 'ngx-store';
import { CookiesStorageService, LocalStorageService, SessionStorageService, SharedStorageService } from 'ngx-store';
import {MenulistService} from '../../commonServices/menulist.service';
import { JwtHelperService } from '@auth0/angular-jwt';


declare var $: any;


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})




export class LoginComponent implements OnInit {
  loginData: any = {};
  is_login_success = false;
  alert: any = {};
  haveValidationKey = false;
  v: string;
  private jwtHelperService = new JwtHelperService();


  constructor(  private sessionStorageService: SessionStorageService,
                private apiService: ApiService, private router: Router,
                private activatedRoute: ActivatedRoute,
                private menulistservice: MenulistService,
                private logoutService: LogoutService
              ) {

                  this.alert.isvisible =  false;

                  if(this.sessionStorageService.get('logoutMessage') != null && this.sessionStorageService.get('logoutMessage') != ''){
                    this.alert.isvisible = true;
                    this.alert.text = this.sessionStorageService.get('logoutMessage');
                    this.alert.class = 'warning';
                    this.alert.type = this.sessionStorageService.get('logoutType');
                  }

                  this.sessionStorageService.set('logoutMessage','');

                  this.activatedRoute.queryParams.subscribe(paramsId => { // for server
                    if ( paramsId != undefined && paramsId.v != null) {
                        this.haveValidationKey = true;
                        this.v = paramsId.v;

                        apiService.post('login/', {v: this.v}).subscribe(data1 => {
                                           console.log('------login data', JSON.parse(data1['menus_data']));
                                           if (data1['is_login_success']) {
                                               const accesstoken = 'Bearer ' + data1['jwt_token'];
                                               this.sessionStorageService.set('accessToken', accesstoken);
                                               const sessionAliveTime = this.jwtHelperService.getTokenExpirationDate(data1['jwt_token']);
                                               this.sessionStorageService.set('sessionAliveTime', sessionAliveTime);
                                               const decodedToken = this.jwtHelperService.decodeToken(data1['jwt_token']);
                                               this.sessionStorageService.set('userName', decodedToken.username);
                                               this.sessionStorageService.set('userEmail', decodedToken.email);
                                               console.log('menu list---', data1['menus_data']);
                                               this.sessionStorageService.set('menu', data1['menus_data']);
                                               this.menulistservice.setMenu( JSON.parse(data1['menus_data']) );
                                               console.log('new accesstoken', this.sessionStorageService.get('accessToken'));
                                               this.router.navigate(['./dashboard']);
                                               this.is_login_success = true;
                                           }
                                         },
                                         (error) => {
                                           this.is_login_success = false;
                                           console.log(error);
                                         }); // uncomment for server

                      } else {
                        this.haveValidationKey = false;
                      }
                  });



    }

  ngOnInit() {

  }

  login() {
    const params = {
      'username': this.loginData.username,
      'password': this.loginData.password
    };
    this.apiService.post('login/', params).subscribe((data: any) => {
      console.log('------login data', JSON.parse(data.menus_data));
      if (data.is_login_success) {
          const accesstoken = 'Bearer ' + data.jwt_token;
          this.sessionStorageService.set('accessToken', accesstoken);
          console.log('menu list---', data.menus_data);
          const sessionAliveTime = this.jwtHelperService.getTokenExpirationDate(data['jwt_token']);
          this.sessionStorageService.set('sessionAliveTime', sessionAliveTime);
          const decodedToken = this.jwtHelperService.decodeToken(data['jwt_token']);
          this.sessionStorageService.set('userName', decodedToken.username);
          this.sessionStorageService.set('userEmail', decodedToken.email);
          this.sessionStorageService.set('menu', data.menus_data);
          this.menulistservice.setMenu( JSON.parse(data.menus_data) );
          console.log('new accesstoken', this.sessionStorageService.get('accessToken'));
          this.router.navigate(['./dashboard']); // Route to dashboard component on login success
      }
      this.is_login_success = data.is_login_success;
    },
    (error) => {
      // this.redirectUrl = error.error.redirect_url;
      this.alert.isvisible = true;
      this.alert.text = error.error.status_desc;
      this.alert.class = 'danger';
      this.alert.type = 'Login Unsuccesful';
      console.log(error);
    });
  }

}
