import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable} from 'rxjs/Observable';
import { CookieStorage, LocalStorage, SessionStorage } from 'ngx-store';
import { CookiesStorageService, LocalStorageService, SessionStorageService, SharedStorageService } from 'ngx-store';


declare var isProduction: any;   // check for productions


@Injectable({
  providedIn: 'root'
})
export class AuthCheckService  implements CanActivate {



// this will check for login status



  constructor(private router: Router, private sessionStorageService: SessionStorageService) { }

  canActivate(

  ): Observable<boolean>|Promise<boolean>|boolean {

//Session validation time fetch from session storage
const sessionAliveTime = this.sessionStorageService.get('sessionAliveTime');
const now: Date = new Date(); //Current time

    const accesstoken = this.sessionStorageService.get('accessToken');
//Check for login 
    if (accesstoken && accesstoken != '' ) {
      return true;
    } else {
      this.router.navigate(['./login']);
      return false;
    }

 // return true;


  }


}
