import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OverwritereasonsComponent } from './overwritereasons.component';

describe('OverwritereasonsComponent', () => {
  let component: OverwritereasonsComponent;
  let fixture: ComponentFixture<OverwritereasonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OverwritereasonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OverwritereasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
