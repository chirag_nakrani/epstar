import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DefaultloadingamountComponent } from './defaultloadingamount.component';

describe('DefaultloadingamountComponent', () => {
  let component: DefaultloadingamountComponent;
  let fixture: ComponentFixture<DefaultloadingamountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DefaultloadingamountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DefaultloadingamountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
