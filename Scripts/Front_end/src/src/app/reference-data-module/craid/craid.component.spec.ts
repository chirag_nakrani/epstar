import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraidComponent } from './craid.component';

describe('CraidComponent', () => {
  let component: CraidComponent;
  let fixture: ComponentFixture<CraidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
