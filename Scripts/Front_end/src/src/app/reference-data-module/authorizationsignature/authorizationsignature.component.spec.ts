import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorizationsignatureComponent } from './authorizationsignature.component';

describe('AuthorizationsignatureComponent', () => {
  let component: AuthorizationsignatureComponent;
  let fixture: ComponentFixture<AuthorizationsignatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthorizationsignatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthorizationsignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
