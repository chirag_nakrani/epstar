import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { AuthorizationsignatureComponent } from './authorizationsignature/authorizationsignature.component';
import { BankcodeComponent } from './bankcode/bankcode.component';
import { BankEscalationmatrixComponent } from './bank-escalationmatrix/bank-escalationmatrix.component';
import { BrandbillcapacityComponent } from './brandbillcapacity/brandbillcapacity.component';
import { CraEscalationmatrixComponent } from './cra-escalationmatrix/cra-escalationmatrix.component';
import { CraidComponent } from './craid/craid.component';
import { CravendormasterComponent } from './cravendormaster/cravendormaster.component';
import { CyphercodeComponent } from './cyphercode/cyphercode.component';
import { DefaultloadingamountComponent } from './defaultloadingamount/defaultloadingamount.component';
import { EpsEscalationmatrixComponent } from './eps-escalationmatrix/eps-escalationmatrix.component';
import { OverwritereasonsComponent } from './overwritereasons/overwritereasons.component';
import { ProjectlistComponent } from './projectlist/projectlist.component';
import { RegionnameComponent } from './regionname/regionname.component';
import { RejectionreasonsComponent } from './rejectionreasons/rejectionreasons.component';
import { SlaComponent } from './sla/sla.component';
import { DataupdatereasonComponent } from './dataupdatereason/dataupdatereason.component';
import { CravaultmasterComponent } from './cravaultmaster/cravaultmaster.component';

import { CommonImportsModule } from '../common/common-imports.module';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';
import { MailMasterComponent } from './mail-master/mail-master.component';
import { HistoricalDataComponent } from './historical-data/historical-data.component';
import { AtmListComponent } from './atm-list/atm-list.component';
import { GlAccountComponent } from './gl-account/gl-account.component';

const routes: Routes = [
  { path: 'authorizationsignature', component: AuthorizationsignatureComponent,
     canActivate: [AuthCheckService] },
  { path: 'bankcode', component: BankcodeComponent,
     canActivate: [AuthCheckService] },
  { path: 'bank-escalationmatrix', component: BankEscalationmatrixComponent,
     canActivate: [AuthCheckService] },
  { path: 'brandbillcapacity', component: BrandbillcapacityComponent,
     canActivate: [AuthCheckService] },
       { path: 'atmlist', component: AtmListComponent,
     canActivate: [AuthCheckService] },
  { path: 'cra-escalationmatrix', component: CraEscalationmatrixComponent,
     canActivate: [AuthCheckService] },
  { path: 'cra-id', component: CraidComponent,
     canActivate: [AuthCheckService] },
  { path: 'cravaultmaster', component: CravaultmasterComponent,
     canActivate: [AuthCheckService] },
  { path: 'cravendormaster', component: CravendormasterComponent,
     canActivate: [AuthCheckService] },
  { path: 'cyphercode', component: CyphercodeComponent,
     canActivate: [AuthCheckService] },
  { path: 'defaultloading', component: DefaultloadingamountComponent,
     canActivate: [AuthCheckService] },
  { path: 'dataupdatereason', component: DataupdatereasonComponent,
     canActivate: [AuthCheckService] },
  { path: 'eps-escalationmatrix', component: EpsEscalationmatrixComponent,
     canActivate: [AuthCheckService] },
  { path: 'overwritereasons', component: OverwritereasonsComponent,
     canActivate: [AuthCheckService] },
  { path: 'rejectionreasons', component: RejectionreasonsComponent,
     canActivate: [AuthCheckService] },
  { path: 'projectlist', component: ProjectlistComponent,
     canActivate: [AuthCheckService] },
  { path: 'sla', component: SlaComponent,
     canActivate: [AuthCheckService] },
  { path: 'regionname', component: RegionnameComponent,
     canActivate: [AuthCheckService] },
  { path: 'mailmaster', component: MailMasterComponent,
    canActivate: [AuthCheckService] },
  { path: 'historicaldata', component: HistoricalDataComponent,
    canActivate: [AuthCheckService] },
  { path: 'gl-account', component: GlAccountComponent,
    canActivate: [AuthCheckService] }
];

@NgModule({
  declarations: [
  AuthorizationsignatureComponent,
  BankcodeComponent,
  BankEscalationmatrixComponent,
  BrandbillcapacityComponent,
  CraEscalationmatrixComponent,
  CraidComponent,
  CravendormasterComponent,
  CyphercodeComponent,
  DefaultloadingamountComponent,
  EpsEscalationmatrixComponent,
  OverwritereasonsComponent,
  ProjectlistComponent,
  RegionnameComponent,
  RejectionreasonsComponent,
  SlaComponent,
  DataupdatereasonComponent,
  CravaultmasterComponent,
  MailMasterComponent,
  HistoricalDataComponent,
  AtmListComponent,
  GlAccountComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class ReferenceDataModuleModule { }
