import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataupdatereasonComponent } from './dataupdatereason.component';

describe('DataupdatereasonComponent', () => {
  let component: DataupdatereasonComponent;
  let fixture: ComponentFixture<DataupdatereasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataupdatereasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataupdatereasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
