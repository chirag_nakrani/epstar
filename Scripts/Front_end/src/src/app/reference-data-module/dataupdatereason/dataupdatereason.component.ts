import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';



declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-dataupdatereason',
  templateUrl: './dataupdatereason.component.html',
  styleUrls: ['./dataupdatereason.component.scss']
})
export class DataupdatereasonComponent implements OnInit {
  excelType="Data Update Reason"
  public uploadData: any = {};
  catDDList: any;
  public currentPageSize: number;
  public isMultiEdit:boolean=false;
  public currentPage = 1;
  alert: any;
  uploadForm: FormGroup;
  submitted = false;
  selected = [];
  selectedids = [];
  temp = [];
  public isEdit = [];
  dropdownValue: any;
  filterDropDownValue: any;
  public colList: any;
  viewColumnAll: any={};
  private pagesizeList: any = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  modalRef: BsModalRef;
  page: Page = new Page();
  diRows: any;
  diColumn: any;
  filterData: any = [];
  rows: any = [];
  prevRows: any = [];
  public editRow:any={};
  tempRows: any = [];
  public permissionList: any = [];
  constructor( private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig,private sessionStorageService: SessionStorageService) {
    //this.diColumn = this.columnNameProviderService.dataInformationColumns;
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;

   }
  ngOnInit() { //Called on page initialization
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    this.getCategory();
    this.uploadForm = new FormGroup({
      'reason': new FormControl(null, Validators.required),
      'category': new FormControl(null, Validators.required),
    });
    this.getViewCol();
    this.allDataActive(0);
    this.updateViewAll();
  }

  onSelect({ selected }) {
    this.isMultiEdit = true;
    console.log('Select Event', this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
//  start of code for upload file
addRejection(params:any){
  this.apiService.post('update_reason_crud/',params).subscribe((event: any) => {
     if (event) {
          console.log('resonse', event);
          this.alert.isvisible = true;
          this.alert.type = 'Upload';
          this.alert.class = 'success';
          this.alert.text = event.status_desc;
          $('#m_modal_6').modal('hide');
          this.allDataActive(0);
          //this.updateViewAll();
        }
      }, error => {

          console.log('error', error);
          $('#m_modal_6').modal('hide');
          this.alert.isvisible = true;
          this.alert.type = 'Error';
          const error_str: string = error.error['status_desc'];
          this.alert.text =  error_str;
          this.alert.class = 'danger';

      });
      $('#m_modal_6').modal('hide');
      this.uploadForm.reset();
}
getData() {
  const params = {
    'operation' : 'A',
    'payload' : this.uploadForm.value
  };

  params.payload.status = 'Active';
  this.addRejection(params);
}

// end of code for upload file

  viewColumns: Object = {};
  filtersForAllView: any = {
    "record_status": "Active"
  };

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    })
  }
  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    })
    // }
    console.log("col change", this.selectedColumn, this.viewColumn);
  }

  viewSearch(event) {
    console.log("updateing");
    const val = event.target.value.toLowerCase();
    const temp = this.tempRows.filter(function (d) {
      var search_category = d.category.toLowerCase().indexOf(val) !== -1;
      var search_reason = d.update_reason.toLowerCase().indexOf(val) !== -1;
      return search_category || search_reason || !val;
    });

    this.rows = temp;
  }
  updateViewAll() {
    console.log("updarte called");
    console.log('filter parameter', this.filtersForAllView);
    $('#modalfilter').modal('hide');
  }

  // --------------- end of drop down value -----------------
  clicked(data) {
    console.log(data)
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
}
//common section
viewMasterData(params:any,offSet:number){
  this.apiService.post('update_reason_crud/', params).subscribe((response: any) => {
     this.rows = response;
     this.rows = [...this.rows];
     this.tempRows = [...this.rows];
     for(let i=0; i<this.rows.length; i++){
       this.isEdit[i] = false;
     }
     console.log("-----------common",this.rows)
     // // this.tempPageCMIS = this.page;
   }, (error) => {
     // console.log('View Error', error.error);
   });
}
getFileData(offSet:number) {
  const params = {
    "operation":"R",
    "payload":{
    }
  };
  this.viewMasterData(params,offSet);
}
//end of common section

 //to Display All data of active status
 allDataActive(offSet:number){
   console.log("inside all data active");
  const params = {
    "operation":"R",
    "payload":{
    }
  };
  this.viewMasterData(params,offSet);
}
// ------------------------- to display column data in modal and in All Data ----------------------
getViewCol() {
  this.colList = [];
  console.log(this.columnConfig.ColumnConfigUpdateReasons);
  this.columnConfig.ColumnConfigUpdateReasons.forEach(x => {
    
      this.colList.push({'name': x.display_column_name, 'prop': x.table_column,"datatype":x.dataType,"isEditable":x.isEditable});
    // console.log("xxxxxxxxxxx",x);
  });
  this.viewColumnAll = this.colList;
console.log("viewColumn",this.viewColumnAll)
}
// ------------------------- to display column data in modal and in All Data ----------------------
//-------------code for edit all data section---------------------------
//indivisual edit 
viewEdit(rowIndex:number,row:any){
  
  this.editRow=row;
  this.isEdit[rowIndex]="true"
}

//to submit edited data 
updateEdit(params:any){
  this.apiService.post('update_reason_crud/' , params).subscribe((response: any) => {
    this.alert.isvisible = true;
    this.alert.type = 'Edit';
    this.alert.class = 'success';
    this.alert.text = response.status_desc;
    this.allDataActive(0);
  }, (error) => {
       this.alert.isvisible = true;
          this.alert.type = 'Error';
          const error_str: string = error.error.data;
          this.alert.text =  error_str;
          this.alert.class = 'danger';
  });
}

editSubmit(rowIndex: number,row: any){
  const params = {
    "operation":"M",
    "payload":{
      "id":row.id,
      "reason":row.update_reason
    }
  };
  this.updateEdit(params);

}
deleteRejection(params:any){
  this.apiService.post('update_reason_crud/' , params).subscribe((response: any) => {
    this.alert.isvisible = true;
    this.alert.type = 'Delete';
    this.alert.class = 'success';
    this.alert.text = response.status_desc;
    this.allDataActive(0);
  }, (error) => {
       this.alert.isvisible = true;
          this.alert.type = 'Error';
          const error_str: string = error.error.data;
          this.alert.text =  error_str;
          this.alert.class = 'danger';
  });
}

deleteSubmit(rowIndex: any,row: any){
  const params = {
    "operation":"D",
    "payload":{
      "id":row.id
    }
  };
  this.deleteRejection(params);

}
//-------------End of code for edit all data section---------------------------

 getCategory() {
    this.apiService.post('ticket/categorymenus/', {}).subscribe((data: any) => {
      this.catDDList = data;
    });
  }

  trackOpt(index: number, opt: any) {
    return opt ? opt.category : undefined;
  }

}
