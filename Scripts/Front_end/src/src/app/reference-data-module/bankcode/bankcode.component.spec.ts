import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankcodeComponent } from './bankcode.component';

describe('BankcodeComponent', () => {
  let component: BankcodeComponent;
  let fixture: ComponentFixture<BankcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
