import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-cyphercode',
  templateUrl: './cyphercode.component.html',
  styleUrls: ['./cyphercode.component.scss']
})
export class CyphercodeComponent implements OnInit {
  @ViewChild('fileInput')
  myInputVariable: ElementRef;
  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  // file_type = 'cyphercode';
  file_type = 'CYPHERCODE';
  operation = 'Add';
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  selected = [];
  selectedids = [];
  previewPage = new Page();
  public uploadData: any;
  uploadForm: FormGroup;
  submitted = false;
  temp = [];
  previewData: any;
  datePipe;
  submitbuttonval:boolean;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public bankFilterDropDown: any;
  public colList: any;
  diRows: any;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  public editRow: any = {};
  public isMultiEdit: boolean;
  fileData: any = {};
  insertData: any = [{}];
  ProjectId: any = [];
  BankCode:any=[];
  public permissionList: any = [];
  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];
  filtersForAllView: any = {};
  viewColumn: any;
  selectedColumn: any = [];
  columnChange: any = [];
  record_status = 'Active';
  public statusList: Array<string>;
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig,private sessionStorageService: SessionStorageService) {
    this.datePipe = new DatePipe('en-US');
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.dropdownValue = [];
    this.filterDropDownValue = {};
  }


  ngOnInit() {
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    this.filtersForAllView.record_status = this.record_status;
    this.statusList = ['All', 'Active', 'Approval Pending', 'History', 'Rejected', 'Uploaded'];
    this.populateDropDownBank();
    this.setDataInformationCurrent();
    this.dropdownsForProjectId();
    this.dropdownsForBankcode();
    this.uploadData.date = new Date();
    this.uploadData.time = '12:00';
    this.uploadData.file;
    this.getViewCol();
    this.submitbuttonval=true;
    this.allDataActive(0);
    this.isMultiEdit = false;
    this.uploadForm = this.formBuilder.group({
      file_type: [this.file_type]
    });

    this.populateSelectColumn();
  }
  // ----------------------- upload file ------------------------------------
  // convenience getter for easy access to form fields
  get f() {
    return this.uploadForm.controls;
  }
  openTimeSelector() {
    console.log('time picker opened');
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.uploadData.time = time;
      console.log('time picker closed');

    });
  }
  onFileChanged(event) {

    //const file = event.target.files[0]
    this.uploadData.file = event.target.files[0];
    console.log("******************",this.myInputVariable.nativeElement.files);
    this.myInputVariable.nativeElement.value = "";
    console.log(this.myInputVariable.nativeElement.files);
  }
  dropdownsForProjectId() {
    const params = {
          'file_type': 'project',
      };
    this.apiService.post('displaydata/', params).subscribe((data: any) => {
       console.log('-------dropdown', data.data);
       this.ProjectId = data.data ;
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }
  dropdownsForBankcode() {
    const params = {
          'file_type': 'bank'
      };
    this.apiService.post('displaydata/', params).subscribe((data: any) => {
       this.BankCode = data.data ;
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }
  InsertDatafn() {
    const params = {
    'operation': this.operation,
    'file_type': this.file_type,
    'payload':this.insertData
    
  };
    this.apiService.uploadFile('cyphercode/', params).subscribe(event => { // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        // this.alert.class="danger"
        this.alert.text = ' ' + response.body.status_desc;
        this.alert.class = 'success';
        $('#m_modal_7').modal('hide');
        this.setDataInformationCurrent();
      }
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#m_modal_7').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
  this.resetFormdata();
  }
  resetFormdata(){
  this.insertData.project_id=null;
  this.insertData.cypher_code=null;
  this.insertData.bank_code=null;
  this.insertData.category=null;
  this.insertData.value=null;
  }
  uploadDatafn() {
    // stop here if form is invalid
    if (this.uploadData.invalid) {
      return;
    }
    // console.log("upload clicked----------", this.uploadData);
    // this.previewColList = this.columnConfig.columnConfig.filter(x => x.datafor == this.file_type && x.bankcode == this.uploadData.bank_code.bank_id);
    this.submitted = true;
    const fileData = new FormData();
    //uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
    fileData.append('file', this.uploadData.file, this.uploadData.file.name);
    fileData.append('file_type', this.file_type);
    // fileData.append('upload_datatime', date);
    // console.log("upload clicked formdata", fileData);
    this.apiService.uploadFile('referencedata/', fileData).subscribe(event => {
      console.log(event); // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        // this.alert.class="danger"
        this.alert.text = ' ' + response.body.status_desc;
        this.alert.class = 'success';
        $('#m_modal_6').modal('hide');
        this.setDataInformationCurrent();

      }
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#m_modal_6').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
    this.clearUploadFormData();

  }
  clearUploadFormData() {
    // this.uploadForm.reset();
    this.uploadData.file = [];
    this.uploadData={};

  }
  // ------------------------------ upload end -----------------------------------
  // ----------code for select column  ----------------



  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    });
    // }
    console.log('col change', this.selectedColumn, this.viewColumn);
  }


// ------------code for search ----------------
  viewSearch(event) {
    console.log('updateing');
    const val = event.target.value.toLowerCase();
    const temp = this.tempRows.filter(function(d) {
      const search_category = d.category.toLowerCase().indexOf(val) !== -1;
      const search_cypher_code = d.cypher_code.toLowerCase().indexOf(val) !== -1;
      const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
      return search_category || search_cypher_code || search_project_id || !val;
    });

    this.rows = temp;
  }
// ------------------ end of code for search  ----------------

  updateViewAll() {
    console.log('updarte called');
    console.log('filter parameter', this.filtersForAllView);
    this.allDataActive(0);
    $('#modalfilter').modal('hide');
  }

  setPage(pageInfo) {
    // console.log("page clicked");
    console.log('page clicked', pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.apiService.post('viewmaster/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size, {
      'file_type': this.file_type,
      'filters': this.filtersForAllView
    }).subscribe(data => {
      console.log('-------get all');
      console.log(data); // handle event here
      const response: any = data;
      this.rows = response.results;
      this.tempRows = response.results;
      this.page.totalElements = response.count;
    });
  }
  // ---------end of  view all data ----------------------

  populateDropDownBank() {


  // --------- for upload dropdown
    this.apiService.post('bankinfo/', {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      console.log('-------dropdown', response.data);
     this.dropdownValue = response.data;
      console.log(this.dropdownValue); // handle event here
     this.getAllBankList();
    }, (error) => {
      console.log('error in dropdown value', error);

    });
  }

  getAllBankList() {
    this.bankFilterDropDown = [];

    this.dropdownValue.forEach( x => {
      x.bank.forEach( y => {
        this.bankFilterDropDown.push(y);
      });
    });
  }
  // --------------- drop down value ---------------------

  // --------------- end of drop down value -----------------


  clicked(data) {
    console.log(data);
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }
  // ------------------------- code for data information ----------------------
  setDataInformationCurrent() {
    const params = {
      'menu': this.file_type,
      'record_status': ['Approval Pending']
    };

    this.getDataInfo(params);
  }
  setDataInformationPrevious() {
    const params = {
      'menu': this.file_type,
      'record_status': ['Active', 'Rejected']
    };

    this.getDataInfo(params);
  }
  getDataInfo(params: any) {
    this.apiService.post('getmasterdata/', params).subscribe((data: any) => {
      const tempRow = data.data;
      this.diRows = tempRow;
      this.diRows = [...this.diRows];
      console.log('Active data', this.diRows);
    }, (error) => {
      console.log('Data Info Error', error.error.status_text);
    });
  }

  // -------------------------End of code for data information ----------------------

  //------------------------code for view file in modal for current and previous tab----------------------------------
  //common section
  viewMasterData(params: any, offSet: number) {
    this.apiService.post('viewmaster/?page=' + (this.currentPage + offSet) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
      ;
      this.rows = response.results;
      this.rows = [...this.rows];
      this.tempRows = [...this.rows]
      this.rows.forEach( (x,index) => {
        this.isEdit[index] = false;
        this.colList.filter( x => x.datatype == 'date').forEach( y => {
          if(x[y.prop] != null){
            x[y.prop] = new Date(x[y.prop]);
      }
        })
      });
      console.log('-----------common', this.rows);
      this.page.totalElements = response.count;
      // // this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      // console.log('View Error', error.error);
    });
    this.clearSelectArrayfn();
  }
   clearSelectArrayfn(){
    console.log("Inside select arrey clr called")
    this.selectedids=[];
    this.selected=[];
  }
  getFileData(offSet: number, status: string) {
    const params = {
      'menu': this.file_type,
      'filter_data': {
        'created_reference_id': this.previewData.created_reference_id,
        'record_status': status
      }
    };
    this.viewMasterData(params, offSet);
  }
  //end of common section
  //to open view file in current tab
  viewFile(rowData: any) {
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0, rowData.record_status);
    this.modalRef = this.modalService.show(this.previewModalcurrent, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }
  //to open view file in previous tab
  viewFilePrevious(rowData: any) {
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0, rowData.record_status);
    this.modalRef = this.modalService.show(this.previewModalprevious, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }

  //to Display All data of active status
  allDataActive(offSet: number) {
    let params: any;
    for (let key in this.filtersForAllView) {
      if (this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All') {
        delete this.filtersForAllView[key];
      }
    }
    params = {
      'menu': this.file_type,
        'filter_data': this.filtersForAllView
    };
    this.viewMasterData(params, offSet);
  }
  //---------------------------------------code for view file in modal for current and previous tab- --------------------------------------
  //------------------code for Modal which is approved or rejected indivisual file -----------------------------
  onSelect({ selected },reqfrom) {
    this.isMultiEdit = true;
    console.log('Select Event', selected, this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    
      if(this.selected.length<1 && reqfrom=='Active'){
        this.submitbuttonval=true;
        this.allDataActive(0);
      }
  }
  getApprovedData(params: any) {
    this.apiService.post('ApproveRejectAll/', params).subscribe((response: any) => {
      this.allDataActive(0);
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_desc;
      console.log('-----------', this.rows);
    }, (error:any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_desc;
      } else {
        this.alert.text = error.statusText;
      }
    });
  }
  //to approve indivisual row
  approveIndivisualFile() {
    for (let i = 0; i <= this.selected.length - 1; i++) {
      this.selectedids.push(this.selected[i].id);
    }
    const params = {
      'menu': this.file_type,
      'id': this.selectedids.toString(),
      'status': 'Active',
      'created_reference_id': this.previewData.created_reference_id
    };
    this.getApprovedData(params);
  }
  //to reject indivisual row
  rejectIndivisualFile() {
    for (let i = 0; i <= this.selected.length - 1; i++) {
      this.selectedids.push(this.selected[i].id);
    }
    const params = {
      'menu': this.file_type,
      'id': this.selectedids.toString(),
      'status': 'Rejected',
      'created_reference_id': this.previewData.created_reference_id
    };
    this.getApprovedData(params);
  }
  //to approve complete file
  approveFile(row: any) {
    const params = {
      'menu': this.file_type,
      'id': '0',
      'created_reference_id': row.created_reference_id,
      'status': 'Active'
    };
    this.getApprovedData(params);
  }
  //to reject complete file
  rejectFile(row: any) {
    const params = {
      'menu': this.file_type,
      'id': '0',
      'status': 'Rejected',
      'created_reference_id': row.created_reference_id
    };
    this.getApprovedData(params);
  }
  //----------------end of code for Modal which is approved or rejected for--------------
  // ------------------------- to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.columnconfigCyphercode);
    this.columnConfig.columnconfigCyphercode.forEach(x => {
     

        this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column , 'datatype': x.datatype});
   
      // console.log("xxxxxxxxxxx",x);
    });
    this.viewColumnAll = this.colList;
    this.viewColumn = this.viewColumnAll;
    console.log('viewColumn', this.viewColumnAll);
  }
  resetFilter() {
    this.filtersForAllView = {};
  }
  // ------------------------- to display column data in modal and in All Data ----------------------
  //-------------code for edit all data section---------------------------
  //indivisual edit
  viewEdit(rowIndex: number, row: any) {
    ;
    this.editRow = row;
    this.isEdit[rowIndex] = 'true';
  }

  //multiple Edit
  onMultipleEdit() {

    if(this.selected.length>0){
      this.submitbuttonval=false;
      this.rows = this.selected;
      this.rows = [...this.rows];
  
      for (let i = 0; i < this.rows.length; i++) {
        this.isEdit[i] = true;
      }
      console.log('edited row', this.rows);
    }
  }

  //to submit edited data
  updateEdit(params: any) {
    this.apiService.post('updatemaster/', params).subscribe((response: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Edit';
      this.alert.class = 'success';
      this.alert.text = response.data;
      this.setDataInformationCurrent();
      this.selected=[];
      this.allDataActive(0);
    }, (error) => {
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      const error_str: string = error.error.data;
      this.alert.text = error_str;
      this.alert.class = 'danger';
      this.selected=[];
    });
  }

  editSubmit() {
    this.submitbuttonval=true;
    const params = {
      'file_type': this.file_type,
      'update': this.rows
    };
    this.updateEdit(params);

  }
  
AddFiled() {
  console.log("insode addddddddddd");
  this.insertData.push({});
  }
  RemoveFiled(index) {
  this.insertData.splice(index,1);
  }
  //-------------End of code for edit all data section---------------------------
}
