import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CravendormasterComponent } from './cravendormaster.component';

describe('CravendormasterComponent', () => {
  let component: CravendormasterComponent;
  let fixture: ComponentFixture<CravendormasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CravendormasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CravendormasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
