import { Component, OnInit , TemplateRef } from '@angular/core';
import {ViewChild, ElementRef} from '@angular/core';

import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Page} from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';

declare var jquery: any;   // not required
declare var $: any;   // not required



@Component({
  selector: 'app-historical-data',
  templateUrl: './historical-data.component.html',
  styleUrls: ['./historical-data.component.scss']
})
export class HistoricalDataComponent implements OnInit {

  excelType="User Role Mapping";
  operation = 'A';
  operationdelete="D";
  operationRead="R";
  @ViewChild('previewModal') previewModal: ElementRef;

  fileType = 'DISPENSE';
    modalRef: BsModalRef;
  alert: any;
  public regionList: any = [];
  page = new Page();
  previewPage = new Page();
  public projectBankList: any = [];
  craDDList: any = [];
  public bankList: any = [];
  public uploadData: any;
  uploadForm: FormGroup;
  submitted = false;
  temp = [];
  buttonEnable=true;
  datePipe;
  showonlybankList:boolean;
  folderPath:string;
    dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: ColumnConfig;
  public permissionList: any = [];
  public bankDropDown: any = [];
  public feederRegionDropDown: any = [];
  fileName: string;
  public date_range: any = [];
  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];
  public statusList = ['All', 'Active', 'Deleted'];
  public filterData: any = {}; 
     public menuList: any = [];
  public monthList = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"];
  public addData: any = {};
  public typeList = ['CBR','DISPENSE','C3R_VMIS'];

  createPathEnable=true;
  viewColumnAll: any = [
    {
      name: 'data For Type',
      prop: 'data_for_type',
      datatype: 'string'
    },
    {
      name: 'Data For Datetime',
      prop: 'data_for_datetime',
      datatype: 'string'
    },
    {
      name: 'File Name',
      prop: 'file_name',
      datatype: 'string'
    },
    {
      name: 'Status',
      prop: 'status',
      datatype: 'string'
    },
    {
      name: 'Level',
      prop: 'level',
      datatype: 'string'
    },
    {
      name: 'File Status',
      prop: 'file_status',
      datatype: 'string'
    },
    {
      name: 'Bank Code',
      prop: 'bank_code',
      datatype: 'string'
    },
    {
      name: 'Project Id',
      prop: 'project_id',
      datatype: 'string'
    },
    {
      name: 'Region',
      prop: 'region',
      datatype: 'string'
    },
    {
      name: 'Is Valid File',
      prop: 'is_valid_file',
      datatype: 'string'
    },
    {
      name: 'Validation Code',
      prop: 'validation_code',
      datatype: 'string'
    },

  ];
  //  --------------code for select column -------------
  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];
  public bankFilterDropDown: any = [];
    constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
      this.datePipe = new DatePipe('en-US');
      this.uploadData = {};
      this.alert = {};
      this.alert.isvisible = false;
      this.page.pageNumber = 0;
      this.page.size = 20;

      this.previewPage.pageNumber = 0;
      this.previewPage.size = 20;
      this.dropdownValue = [];
      this.filterDropDownValue = {};
     }


    ngOnInit() {
      if(this.sessionStorageService.get('permissionList') != null)
      this.permissionList = this.sessionStorageService.get('permissionList');
      this.uploadData.date = new Date();
      this.uploadData.time = '12:00';
      this.uploadForm = this.formBuilder.group({
        // bank_code: ['', Validators.required],
        // cra: ['', Validators.required],
        // project_id: ['', Validators.required],
        // date: [ this.uploadData.date],
        // time: [  this.uploadData.time],

        file_type: ['', Validators.required],
        // month: ['', Validators.required],
        // region: ['', Validators.required],
        // bank_id: ['', Validators.required],
    });

    this.getParentMenu();
    // this.dropdownsForProjectBank();
      this.updateViewAll();
      this.populateSelectColumn();
    }











    // ----------------------- upload file ------------------------------------

  // convenience getter for easy access to form fields
  get f() { return this.uploadForm.controls; }

  openTimeSelector() {
      console.log('time picker opened');
      const amazingTimePicker = this.atp.open();
      amazingTimePicker.afterClose().subscribe(time => {
        this.uploadData.time = time;
      console.log('time picker closed');

      });
  }

  getParentMenu() {

    const param = {
                  	'parent_menu_id': ''
                  };

    this.apiService.post('menu_list/', param).subscribe(
      (data: any) => {

        this.menuList = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }
  onFileChanged(event) {

    //const file = event.target.files[0]

    this.uploadData.file = event.target.files[0];
  }

  getAllBankList() {
    this.bankFilterDropDown = [];
    this.dropdownValue.forEach( x => {
      x.bank.forEach( y => {
        this.bankFilterDropDown.push(y);
      });
    });
  }


  clearUploadFormData() {
 this.addData={};
 this.addData.C3R_VMIS='';
  }
  // getBankList() {
  //   this.bankList = [];
  //   this.bankList = this.projectBankList.filter( x => x.project_id == this.addData.project_id )[0].bank;
  // }
  
  // getRegion() {
  //   this.regionList = [];
  //   this.regionList = this.bankList.filter( x => x.bank_id == this.addData.bank_code)[0].region;
  // }












  populateSelectColumn() {
    this.viewColumnAll.filter( (col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
  this.viewColumn = this.viewColumnAll.filter( (col) => {
    return this.selectedColumn.includes(col.prop);
  });
  // }
  console.log('col change', this.selectedColumn, this.viewColumn);
  }

// ----------------- end of code for select column --------
// ----------------- code for search   -----------------
  viewSearch(event) {
        console.log('updateing');
          const val = event.target.value.toLowerCase();

          const temp = this.tempRows.filter(function(d) {
              const search_data_for_type = d.data_for_type.toLowerCase().indexOf(val) !== -1;
              const search_data_for_datetime = d.data_for_datetime.toLowerCase().indexOf(val) !== -1;
              const search_bank_code = d.bank_code.toLowerCase().indexOf(val) !== -1;
             return search_data_for_type || search_data_for_datetime || search_bank_code || !val ;
             });

          this.rows = temp;
  }
// -----------------end of code for search   -----------------

  updateViewAll() {
    console.log('updarte called');
    console.log('filter parameter', this.filterData);
    this.setPage({ offset: 0 });
    $('#modalfilter').modal('hide');
  }

  resetFilter() {
    this.filterData = {};
    this.date_range = [];
  }
  setPage(pageInfo) {
    console.log("Inmside %%%%%%%%%%%%%%%%%%%%%%%%% set page funtion called",pageInfo)
    if (this.date_range) {
      this.date_range = this.date_range.map( x => {
        return this.datePipe.transform(new Date(x), 'yyyy-MM-dd');
      });
    }
    let params: any;
   
    // params = {
    //   'file_type': "historical"
    // };


    if ( this.date_range.length > 0 ) {
      params = {
        'file_type': "historical",
        'date_range': this.date_range,
      };
    } else {
      params = {
        'file_type': "historical",
      };
    }






    
    if (this.filterData != undefined || this.filterData != {}) {
      const filter = this.filterData;
     
      params.filters = filter;
    }
    console.log("111111111111111111111 final params",params)
    this.page.pageNumber = pageInfo.offset;
    this.apiService.post('ViewHistoryUpload/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size, params).subscribe(data => {
        console.log('-------get all');
     console.log("***************************************"); // handle event here
    //  this.filterData={};
     const response: any = data;
      //   this.page = pagedData.page;
      // this.temp = [...data];

      // push our inital complete list
    this.rows = response.results;
     this.tempRows = response.results;
     this.page.totalElements = response.count;
    });
  }



    // ---------end of  view all data ----------------------






    // --------preview modal ----------
  previewRows: any = [];
  tempPreviewRow: any = [];
  previewFilter: any;

  previewSearch(event) {
  console.log('updateing');
    const val = event.target.value.toLowerCase();
    // filter our data
    const filterParam: any[] = [];

    this.previewColList.forEach(x => {
      filterParam.push([x.table_column, val]);
    });

    const temp = this.searchFilter(filterParam);

    // update the rows
    this.previewRows = temp;
  }

  searchFilter(filters): any[] {
    const returnVal: any[] = [];
    let columnName: string;
    let val: string;
      this.tempPreviewRow.forEach(x => {
          for (let i = 0; i < filters.length; i++) {
            if (filters != undefined) {
              columnName = filters[i][0];
              val = filters[i][1].toString();
              if (x[columnName].toString().toLowerCase().indexOf(val) !== -1) {
                if (returnVal.indexOf(x) == -1) {
                  returnVal.push(x);
                }
              }
            }
          }
      });
    return returnVal;
  }




  setPreviewPage(pageInfo) {
    console.log('page clicked');
    this.previewPage.pageNumber = pageInfo.offset;

    this.updatePreview();
  }


  updatePreview() {
    console.log("@@@@@@@@@@@@@@@@@@ inside update Preview funtion")
  const hours_min = this.uploadData.time.split(':');
    this.uploadData.upload_datetime = new Date(this.uploadData.date);
    this.uploadData.upload_datetime.setHours(hours_min[0], hours_min[1]);
    const date = this.datePipe.transform(this.uploadData.upload_datetime, 'yyyy-MM-dd HH:mm:ss');

      this.apiService.post('view/?page=' + (this.previewPage.pageNumber + 1) + '&' + 'page_size=' + this.previewPage.size, {
        'page': this.page,
        'file_type': this.fileType,
        'bank_code': this.uploadData.bank_code.bank_id,
        'filters' : {
          //  "datafor_date_time":"2018-09-26 18:16:23",
          'datafor_date_time': date,
          'project_id': this.uploadData.project.project_id,
  'record_status': 'Approval Pending',
        }}).subscribe(data => {
          console.log('-------get all preview');
       console.log(data); // handle event here

       const response: any = data;
    
        this.tempPreviewRow = response.results;

       this.previewRows = response.results;
       this.previewPage.totalElements = response.count;

      // this.tempPreviewRow.push(response.results);
      this.tempPreviewRow = [...response.results];
      // this.previewRows=[];
      // this.previewRows.push(response.results);
       this.previewRows = [...response.results];
      // this.previewRows = this.previewRows[0]

       console.log('prevew data', this.previewRows);
      });
    }


    approvePreviewFn(status) {
      const hours_min = this.uploadData.time.split(':');
      this.uploadData.upload_datetime = new Date(this.uploadData.date);
      this.uploadData.upload_datetime.setHours(hours_min[0], hours_min[1]);
      const date = this.datePipe.transform(this.uploadData.upload_datetime, 'yyyy-MM-dd HH:mm:ss');

      this.apiService.post('status/', {
        'file_type': this.fileType,
        'bank_code': this.uploadData.bank_code.bank_id,
        'project_id': this.uploadData.project.project_id,
        'datafor_date_time': date,
        'status': status,
        // "filters" : {
        //   //  "datafor_date_time":"2018-09-26 18:16:23",
        //   // "datafor_date_time": date,
        //   // "record_status":"Approval Pending"
        // }
      }).subscribe(data => {
          console.log('-------approved ');
       console.log(data); // handle event here
       const response: any = data;
       if (status == 'Approved') {
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
       this.alert.class = 'success';
       this.alert.text = ' Data Approved';
       }
       if (status == 'Rejected') {
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
       this.alert.class = 'success';
       this.alert.text = ' Data Rejected';
       }

    // $('#modalfilter').modal('hide');
    this.modalRef.hide();
      }, (error) => {
        console.log('error', error);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
       this.alert.class = 'danger';
       this.alert.text = ' Something Went Wrong';
      });
    }


    openPreviewModal() {
    $('#m_modal_6').modal('hide');
      console.log(this.previewModal);
      this.modalRef = this.modalService.show(this.previewModal, Object.assign({}, { class: 'preview-modal modal-lg' }));
    }


    // ------- end of preview modal ----------




    // --------------- drop down value --------------
    

    // --------------- end of drop down value --------------


    clicked(data) {
      console.log(data);
    }
    chkForValidation(type){
      console.log("Inside check validation is called",type)
      if(this.addData.file_type!=undefined && this.addData.month!=undefined && this.addData.month!=''
      && this.addData.project_id!=undefined &&  this.addData.project_id!='' 
      && this.addData.region!=undefined && this.addData.region!=''
       && this.addData.bank_code!=undefined &&this.addData.bank_code!='' || this.addData.C3R_VMIS!=undefined &&this.addData.C3R_VMIS!=''){
     this.createPathEnable=false
      }
      if(type=='file_type'){
        console.log("$$$$$",this.addData.file_type)
        if(this.addData.file_type=="C3R_VMIS"){
          var selectedOtption="C3R"
          this.showonlybankList=true;
        }else{
           selectedOtption=this.addData.file_type
          this.showonlybankList=false;
        }
        this.addData.month='';
        this.addData.C3R_VMIS='';
        this.addData.project_id='';
        this.addData.region='';
        this.addData.bank_code='';
        this.projectBankList=[];
        this.regionList=[];
        this.bankList=[];
        this.apiService.post('bankinfo/', {
          'file_type': selectedOtption,
          'routine_info': 'ROUTINE_DATA_INFO'
        }).subscribe(data => {
          console.log('------- dropdown value ');
          const response: any = data;
                      if(this.addData.file_type=="C3R_VMIS"){
                      this.craDDList=response.data
                      }else{
                        for(var i=0;i<response.data.length;i++){
                          this.projectBankList.push(response.data[i].project_id)
                          for(var j=0;j<response.data[i].bank.length;j++){
                            var bank=response.data[i].bank[j].region;
                            this.bankList.push(response.data[i].bank[j].bank_id)
                            for(var k=0;k<bank.length;k++){
                              this.regionList.push(bank[k].region)
                              }
                            }
                        }
                      }

       
          function onlyUnique(value, index, self) { 
            return self.indexOf(value) === index;
        }
          this.regionList = this.regionList.filter( onlyUnique );
          this.projectBankList = this.projectBankList.filter( onlyUnique );
          this.bankList = this.bankList.filter( onlyUnique );


        }, (error) => {
          console.log('error in dropdown value', error);
    
        });
      }
    }
createPath(){
  // if (this.uploadForm.invalid) {
  //   console.log("Indie if ffffffffffffffffffff condtion",this.uploadForm.invalid)
  //   return;
  //   }
  this.createPathEnable=true;
    this.buttonEnable=false;
    this.addData.directory="folderpath"
    // data.region="abc"
    console.log("****************",this.addData)

    this.apiService.uploadFile('pathcreation/', this.addData).subscribe((response: any) => {
      console.log("$$$$$$$$$$$",response)
      if (response && response.body) {
        this.folderPath=response.body.folder_path
         this.alert.isvisible = true;
         this.alert.type = 'Upload';
         this.alert.class = 'success';
         this.alert.text = ' ' + response.body.status_desc+' '+response.body.folder_path;
      }
     }, error => {
       this.alert.isvisible = true;
       this.alert.type = 'Error';
       let error_str : string = '';
       if (typeof(error.error['status_text']) == 'object') {
           error.error['status_text'].forEach(x => {

             error_str = error_str + ' | ' + x;
           });
         } else {
           error_str = error.error['status_text'];
         }

       this.alert.text =  error_str;

       this.alert.class = 'danger';
     });

  }
  addDataFn() {
    this.buttonEnable=true;

    this.addData.folder_path=this.folderPath;
    this.apiService.uploadFile('historicaldata/upload/', this.addData).subscribe((response: any) => {
       if (response && response.body) {
          this.alert.isvisible = true;
          this.alert.type = 'Upload';
          this.alert.class = 'success';
          this.alert.text = "Upload is in progress please check your mailbox";
       }
      }, error => {
        this.alert.isvisible = true;
        this.alert.type = 'Error';
        // let error_str : string = '';
        // if (typeof(error.error['status_text']) == 'object') {
        //     error.error['status_text'].forEach(x => {

        //       error_str = error_str + ' | ' + x;
        //     });
        //   } else {
        //     error_str = error.error['status_text'];
        //   }

        this.alert.text =  'Directory is empty.Kindly store history files in the respective directory';

        this.alert.class = 'danger';
      });
      this.clearUploadFormData();
  }
    // dropdownsForProjectBank() {
    //   const params = {
    //     'file_type': 'CBR',
    //     'routine_info': 'ROUTINE_DATA_INFO'
    //   };
    //   this.apiService.post('bankinfo/', params).subscribe((data: any) => {
    //      this.bankList = [];
    //      this.projectBankList = data.data;
    //   }, (error) => {
    //      console.log('error in dropdown value', error);
    //   });
    // }

    calculateTotalPage(rowcount, rowsize) {
      return Math.ceil(rowcount / rowsize);
        }

}
