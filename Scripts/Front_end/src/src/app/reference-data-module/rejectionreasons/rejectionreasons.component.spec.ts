import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RejectionreasonsComponent } from './rejectionreasons.component';

describe('RejectionreasonsComponent', () => {
  let component: RejectionreasonsComponent;
  let fixture: ComponentFixture<RejectionreasonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectionreasonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RejectionreasonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
