import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankEscalationmatrixComponent } from './bank-escalationmatrix.component';

describe('BankEscalationmatrixComponent', () => {
  let component: BankEscalationmatrixComponent;
  let fixture: ComponentFixture<BankEscalationmatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankEscalationmatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankEscalationmatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
