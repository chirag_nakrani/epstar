import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandbillcapacityComponent } from './brandbillcapacity.component';

describe('BrandbillcapacityComponent', () => {
  let component: BrandbillcapacityComponent;
  let fixture: ComponentFixture<BrandbillcapacityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandbillcapacityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandbillcapacityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
