import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CraEscalationmatrixComponent } from './cra-escalationmatrix.component';

describe('CraEscalationmatrixComponent', () => {
  let component: CraEscalationmatrixComponent;
  let fixture: ComponentFixture<CraEscalationmatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CraEscalationmatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CraEscalationmatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
