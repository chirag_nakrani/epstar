import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpsEscalationmatrixComponent } from './eps-escalationmatrix.component';

describe('EpsEscalationmatrixComponent', () => {
  let component: EpsEscalationmatrixComponent;
  let fixture: ComponentFixture<EpsEscalationmatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpsEscalationmatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpsEscalationmatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
