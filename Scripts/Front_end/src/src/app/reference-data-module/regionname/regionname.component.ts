import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-regionname',
  templateUrl: './regionname.component.html',
  styleUrls: ['./regionname.component.scss']
})
export class RegionnameComponent implements OnInit {
  file_type="region"
  operation = 'a';
  operationM='m';
  operationdelete='d';
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  previewPage = new Page();
  submitted = false;
  temp = [];
  datePipe;
  public colList: any;
  viewColumnAll: any = [];
  public currentPageSize: number;
  public currentPage = 1;
  public addField: any = [{}];
  public selectedState: string;
  rows: any = [];
  newrows:any=[];
  public isEdit = [];
  public editRow: any =[ {}];
  selected = [];
  selectedids = [];
  public isMultiEdit: boolean;
  newselect:any=[];
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig) {
    this.datePipe = new DatePipe('en-US');
    this.alert = {};
    this.alert.isvisible = false;
  }
  ngOnInit() {
    this.isMultiEdit = false;
    this.getViewCol();
    this.AllDatafn();
    this.populateSelectColumn();
  }
  // ----------------------- upload file ------------------------------------

  viewColumns: Object = {};
  tempRows: any = [];
  filtersForAllView: any = {
    'record_status': 'Active'
  };

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    });
    // }
    console.log('col change', this.selectedColumn, this.viewColumn);
  }
//start of code for insert project id
  InsertDatafn() {
    const params = {
      "file_type" : this.file_type,
      'operation': this.operation,
      "payload": this.addField
  };

    this.apiService.uploadFile('insertrecord/', params).subscribe(event => { // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log("region ------",this.addField);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        // this.alert.class="danger"
        this.alert.text = ' ' + response.body.status_desc;
        this.alert.class = 'success';
        this.AllDatafn()
      }
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
  this.resetFormdata();
  }
  resetFormdata(){
  this.addField.region_name=null;
  }
// end of code for insert function

// to add new row and remove new row function
AddFiled() {
  console.log("insode addddddddddd");
  this.addField.push({});
  }

  RemoveField(index) {
  this.addField.splice(index,1);
  }
// end of code to add and remove row

// start of code for to show column in  all data section
getViewCol() {
  this.colList = [];
  console.log(this.columnConfig.ColumnConfigregionList);
  this.columnConfig.ColumnConfigregionList.forEach(x => {
      this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column ,'isEditable': x.isEditable });
    console.log("xxxxxxxxxxx",x);
  });
  this.viewColumnAll = this.colList;
  console.log('colListcolList------', this.viewColumnAll);
}
// end of code for to show column in  all data section

// start of code for view all data section
AllDatafn() {
  const params = {
    "file_type" : this.file_type

};
    this.apiService.post('displaydata/', params).subscribe((response: any) => {
      console.log('resonse------', response);
        this.rows = response.data;
        this.rows = [...this.rows];
        console.log("row-------------",   this.rows);
  }, (error => {
    this.alert.isvisible = true;
    this.alert.type = 'Error';
    this.alert.class = 'danger';
  }));
}
// end of code for view all data section
// code for selct and edit functionality in all data section
onSelect({ selected }) {
  this.isMultiEdit = true;
  console.log('Select Event', this.selected);
  this.selected.splice(0, this.selected.length);
  this.selected.push(...selected);
  const temp = JSON.stringify(this.selected);
  this.newselect = JSON.parse(temp);
console.log("new seleted", this.newselect );
}
//single edit
viewEdit(rowIndex: number, row: any){
  this.editRow = row;
  this.isEdit[rowIndex] = 'true';
}
  //multiple Edit
  onMultipleEdit() {

    this.rows = this.selected;
    this.rows = [...this.rows];

    for (let i = 0; i < this.rows.length; i++) {
      this.isEdit[i] = true;
    }
    console.log('edited row', this.rows);
  }
editSubmit() {
  this.rows.forEach((x, index) => {
    x.to_be_modified = this.newselect[index].project_id
  });
  const params = {
    'file_type': this.file_type,
    'operation': this.operationM,
    "payload": this.rows
};
  this.apiService.uploadFile('insertrecord/', params).subscribe(event => { // handle event here
    const response: any = event;
    if (response && response.body) {
      console.log('resonse', response.body);
      this.alert.isvisible = true;
      this.alert.type = 'Upload';
      // this.alert.class="danger"
      this.alert.text = ' ' + response.body.status_desc;
      this.alert.class = 'success';
      this.AllDatafn();
    }
  }, (error => {
    // alert("Error - "+ error.status+ " "+error.statusText);
    this.alert.isvisible = true;
    this.alert.type = 'Error';
    // this.alert.text="error desc- " +error.status+ " "+error.statusText;
    // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

    this.alert.class = 'danger';
  }));

}



// Deleterow(rowIndex: number, row: any) {
//   this.editRow=row;
//   console.log("this.newrowsthis.newrows",this.editRow);
//     const params = {
//       'file_type': this.file_type,
//       'operation': this.operationdelete,
//       "payload":[this.editRow[0].project_id]

// };
//   this.apiService.uploadFile('insertrecord/', params).subscribe(event => { // handle event here
//     const response: any = event;
//     if (response && response.body) {
//       console.log('resonse', response.body);
//       this.alert.isvisible = true;
//       this.alert.type = 'Upload';
//       // this.alert.class="danger"
//       this.alert.text = ' ' + response.body.status_desc;
//       this.alert.class = 'success';
//       this.AllDatafn();
//     }
//   }, (error => {
//     // alert("Error - "+ error.status+ " "+error.statusText);
//     this.alert.isvisible = true;
//     this.alert.type = 'Error';
//     // this.alert.text="error desc- " +error.status+ " "+error.statusText;
//     // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

//     this.alert.class = 'danger';
//   }));

// }

  viewSearch(event: any) {
    
  }
}
