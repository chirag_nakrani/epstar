import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegionnameComponent } from './regionname.component';

describe('RegionnameComponent', () => {
  let component: RegionnameComponent;
  let fixture: ComponentFixture<RegionnameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionnameComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionnameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
