import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { SystemsettingsComponent } from './systemsettings/systemsettings.component';
import { SystemSettingsListComponent } from './system-settings-list/system-settings-list.component';

import { CommonImportsModule } from '../common/common-imports.module';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';

const routes: Routes = [
  { path: 'systemsettings', component: SystemsettingsComponent,
     canActivate: [AuthCheckService] },
  { path: 'systemsettingslist', component: SystemSettingsListComponent,
     canActivate: [AuthCheckService]},
];

@NgModule({
  declarations: [
    SystemsettingsComponent,
    SystemSettingsListComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class SystemSettingsModuleModule { }
