import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-systemsettings',
  templateUrl: './systemsettings.component.html',
  styleUrls: ['./systemsettings.component.scss']
})


export class SystemsettingsComponent implements OnInit {
 data:any={};
  alert: any;
  constructor(private apiService: ApiService,) {
  this.data = {};
  this.alert = {};
  this.alert.isvisible = false;
  }

  ngOnInit() {
  }


onSubmit() {
    // console.log("upload clicked formdata", this.data);
    this.apiService.uploadFile('system_settings/', this.data).subscribe(event => {
      console.log(event); // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        // this.alert.class="danger"
        this.alert.text = ' ' + response.body.status_text;
        this.alert.class = 'success';

      }
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));


  }

} 
