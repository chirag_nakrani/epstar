import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';



declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-system-settings-list',
  templateUrl: './system-settings-list.component.html',
  styleUrls: ['./system-settings-list.component.scss']
})
export class SystemSettingsListComponent implements OnInit {

  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  selected = [];
  selectedids = [];
  previewPage = new Page();
  submitted = false;
  temp = [];
  previewData: any;
  datePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  diRows: any;
  rowSingle: any;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;

  constructor( private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig) {
    this.datePipe = new DatePipe('en-US');
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.dropdownValue = [];
    this.filterDropDownValue = {};
  }


  ngOnInit() {
    this.setDataInformationCurrent();
    this.getViewCol();
    this.allDataActive();
    this.populateSelectColumn();
  }

  // ------------------------------ upload end -----------------------------------
  // ----------code for select column ----------------

  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];
  filtersForAllView: any = {
    'record_status': 'Active'
  };

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  // ---------end of  code for select column ----------------------
//------------------ code for search ------------

viewSearch(event) {
  console.log("updateing");
  const val = event.target.value.toLowerCase();
  const temp = this.tempRows.filter(function (d) {
    var search_atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
    var search_bank_name = d.bank_name.toLowerCase().indexOf(val) !== -1;
    var search_category = d.category.toLowerCase().indexOf(val) !== -1;
    return search_atm_id || search_bank_name || search_category || !val;
  });

  this.rows = temp;
}
//------------------ code for search ------------
  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }
  // ------------------------- code for data information ----------------------
  setDataInformationCurrent() {
    const params = {
      'record_status': 'Approval Pending'
    };

    this.getDataInfo(params);
  }
  setDataInformationPrevious() {
    const params = {
      'record_status': 'History'
    };

    this.getDataInfo(params);
  }
  getDataInfo(params: any) {
    this.apiService.post('system_settings/list/', params).subscribe((data: any) => {
      const tempRow = data.data;
      this.diRows = tempRow;
      this.diRows = [...this.diRows];
      console.log('Active data', this.diRows);
    }, (error) => {
      console.log('Data Info Error', error.error.status_text);
    });
  }

  // -------------------------End of code for data information ----------------------

  //------------------------code for view file in modal for current and previous tab----------------------------------
  //common section
  viewSystemData(params: any) {
    this.apiService.post('system_settings/list/', params).subscribe((response: any) => {
      this.rows = response.data;
      this.rows = [...this.rows];
      this.tempRows=[...this.rows]
      console.log('-----------common', this.rows);
    }, (error) => {
      // console.log('View Error', error.error);
    });
  }
  viewSystemDataSingle(params: any) {
    this.apiService.post('system_settings/list/', params).subscribe((response: any) => {
      this.rowSingle = response.data;
      this.rowSingle = [...this.rowSingle];
      console.log('-----------single', this.rowSingle);
    }, (error) => {
      // console.log('View Error', error.error);
    });
  }
  getFileData( row:any) {
    const params = {
       	'id':row.id
    };
    this.viewSystemDataSingle(params);
  }
  //end of common section
  //to open view file in current tab
  viewFile(rowData: any) {
  console.log("--------view file",rowData);
    this.getFileData( rowData);
    this.modalRef = this.modalService.show(this.previewModalcurrent, Object.assign({}, { class: 'preview-modal modal-lg' }));
  }
  //to open view file in previous tab
  viewFilePrevious(rowData: any) {
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData( rowData.id);
    this.modalRef = this.modalService.show(this.previewModalprevious, Object.assign({}, { class: 'preview-modal modal-lg' }));
  }

  //to Display All data of active status
  allDataActive() {
    console.log('inside all data active');
    const params = {
        'record_status': 'Active'
    };
    this.viewSystemData(params);
  }
  //---------------------------------------code for view file in modal for current and previous tab- --------------------------------------
  //------------------code for Modal which is approved or rejected individual file -----------------------------
  getApprovedData(params: any) {
    this.apiService.post('system_settings/approve/', params).subscribe((response: any) => {
      this.allDataActive();
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success  }';
      this.alert.text = response.status_text;
      console.log('-----------', this.rows);
    }, (error:any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.status_text;
      }
    });
  }
  getRejectedData(params: any) {
    this.apiService.post('system_settings/reject/', params).subscribe((response: any) => {
      this.allDataActive();
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_text;
      console.log('-----------', this.rows);
    }, (error:any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.status_text;
      }
    });
  }
  //to approve complete Settings
  approveSettings(row: any) {
    const params = {
      'id': row.id
    };
    this.getApprovedData(params);
  }
  //to reject complete Settings
  rejectSettings(row: any) {
    const params = {
      'id': row.id
    };
    this.getRejectedData(params);
  }
  //----------------end of code for Modal which is approved or rejected for--------------
  // ----------------d---Settings to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.columnSystemSettings);
    this.columnConfig.columnSystemSettings.forEach(x => {

	this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column,'display': x.isDisplayColumn});
       console.log("xxxxxxxxxxx",x);
    });
    this.viewColumnAll = this.colList;
    console.log('viewColumn', this.viewColumnAll);
  }
  // ------------------------- to display column data in modal and in All Data ----------------------
}
