import { Injectable } from '@angular/core';

@Injectable()
export class ColumnNameProviderService {

   dataInformationColumns =  [
    {
        'display_column_name': 'Status',
        'table_column': 'record_status'
    },
    {
        'display_column_name': 'Uploaded Date & Time',
        'table_column': 'datafor_date_time'
    },
    {
        'display_column_name': 'Uploaded By',
        'table_column': 'created_by'
    }
  ];

  columnInfoVmis = [
  	{
  		'display_column_name': 'SR NO',
  		'table_column': 'sr_no',
		  'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Date',
  		'table_column': 'date',
  		'data_for': 'C3R_VMIS',
		  'datatype':'date'
  	},
  	{
  		'display_column_name': 'Bank',
  		'table_column': 'bank',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'CRA',
  		'table_column': 'cra_name',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Feeder Branch: Name',
  		'table_column': 'feeder_branch_name',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Vault Op. Balance 100',
  		'table_column': 'vault_balance_100',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Vault Op. Balance 200',
  		'table_column': 'vault_balance_200',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Vault Op. Balance 500',
  		'table_column': 'vault_balance_500',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Vault Op. Balance 2000',
  		'table_column': 'vault_balance_2000',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Total',
  		'table_column': 'total_vault_balance',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Withdrawal from Bank 100',
  		'table_column': 'withdrawalfrom_bank_100',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Withdrawal from Bank 200',
  		'table_column': 'withdrawalfrom_bank_200',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Withdrawal from Bank 500',
  		'table_column': 'withdrawalfrom_bank_500',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Withdrawal from Bank 2000',
  		'table_column': 'withdrawalfrom_bank_2000',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Total',
  		'table_column': 'total_withdrawalfrom_bank',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Replenish to ATM: 100',
  		'table_column': 'replenishto_atm_100',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Replenish to ATM: 200',
  		'table_column': 'replenishto_atm_200',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Replenish to ATM: 500',
  		'table_column': 'replenishto_atm_500',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Replenish to ATM: 2000',
  		'table_column': 'replenishto_atm_2000',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Total',
  		'table_column': 'total_replenishto_atm',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Cash Return from ATM: 100',
  		'table_column': 'cashreturn_atm_100',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Cash Return from ATM: 200',
  		'table_column': 'cashreturn_atm_200',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Cash Return from ATM: 500',
  		'table_column': 'cashreturn_atm_500',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Cash Return from ATM: 2000',
  		'table_column': 'cashreturn_atm_2000',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Total',
  		'table_column': 'total_cashreturn_atm',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Unfit Currency: 100',
  		'table_column': 'unfit_currency_100',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Unfit Currency: 200',
  		'table_column': 'unfit_currency_200',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Unfit Currency: 500',
  		'table_column': 'unfit_currency_500',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Unfit Currency: 2000',
  		'table_column': 'unfit_currency_2000',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Total',
  		'table_column': 'total_unfit_currency',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Unfit Currency Return to Bank: 100',
  		'table_column': 'atm_unfit_currency_return_100',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Unfit Currency Return to Bank: 200',
  		'table_column': 'atm_unfit_currency_return_200',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Unfit Currency Return to Bank: 500',
  		'table_column': 'atm_unfit_currency_return_500',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Unfit Currency Return to Bank: 2000',
  		'table_column': 'atm_unfit_currency_return_2000',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Total',
  		'table_column': 'total_atm_unfit_currency_return',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Closing Balance: 100',
  		'table_column': 'closing_balance_100',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Closing Balance: 200',
  		'table_column': 'closing_balance_200',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Closing Balance: 500',
  		'table_column': 'closing_balance_500',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Closing Balance: 2000',
  		'table_column': 'closing_balance_2000',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Total',
  		'table_column': 'total_closing_balance',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Fit Currency: 100',
  		'table_column': 'fit_currency_100',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Fit Currency: 200',
  		'table_column': 'fit_currency_200',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Fit Currency: 500',
  		'table_column': 'fit_currency_500',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'ATM Fit Currency: 2000',
  		'table_column': 'fit_currency_2000',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	},
  	{
  		'display_column_name': 'Total',
  		'table_column': 'total_fit_currency',
  		'data_for': 'C3R_VMIS',
		  'datatype':'string'
  	}
  ];

  columnInfoCmis = [
            	{
            		'table_column': 'id',
            		'display_column_name': 'Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sr_no',
            		'display_column_name': 'Sr No',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'bank',
            		'display_column_name': 'Bank',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'feeder_branch',
            		'display_column_name': 'Feeder Branch',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_id',
            		'display_column_name': 'Atm Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'location',
            		'display_column_name': 'Location',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'date',
            		'display_column_name': 'Date',
					'datafor': 'C3R_CMIS',
					'datatype':'date'
            	},
            	{
            		'table_column': 'eod_loading_time',
            		'display_column_name': 'Eod Loading Time',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'status_of_loading',
            		'display_column_name': 'Status Of Loading',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'last_transaction_no',
            		'display_column_name': 'Last Transaction No',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'cra',
            		'display_column_name': 'Cra',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'indentno',
            		'display_column_name': 'Indentno',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'bankrefno',
            		'display_column_name': 'Bankrefno',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_op__bal_100',
            		'display_column_name': 'ATM Counter Details: Op  Bal 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_op__bal_200',
            		'display_column_name': 'ATM Counter Details: Op  Bal 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_op__bal_500',
            		'display_column_name': 'ATM Counter Details: Op  Bal 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_op__bal_1000',
            		'display_column_name': 'ATM Counter Details: Op  Bal 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_op__bal_2000',
            		'display_column_name': 'ATM Counter Details: Op  Bal 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_op_bal_total',
            		'display_column_name': 'ATM Counter Details: Op Bal Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_disp_100',
            		'display_column_name': 'ATM Counter Details: Disp 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_disp_200',
            		'display_column_name': 'ATM Counter Details: Disp 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_disp_500',
            		'display_column_name': 'ATM Counter Details: Disp 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_disp_1000',
            		'display_column_name': 'ATM Counter Details: Disp 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_disp_2000',
            		'display_column_name': 'ATM Counter Details: Disp 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_disp_total',
            		'display_column_name': 'ATM Counter Details: Disp Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_divert_count_100',
            		'display_column_name': 'ATM Counter Details: Divert Count 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_divert_count_200',
            		'display_column_name': 'ATM Counter Details: Divert Count 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_divert_count_500',
            		'display_column_name': 'ATM Counter Details: Divert Count 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_divert_count_1000',
            		'display_column_name': 'ATM Counter Details: Divert Count 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_divert_count_2000',
            		'display_column_name': 'ATM Counter Details: Divert Count 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_divert_count_total',
            		'display_column_name': 'ATM Counter Details: Divert Count Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_remain_counter_100',
            		'display_column_name': 'ATM Counter Details: Remain Counter 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_remain_counter_200',
            		'display_column_name': 'ATM Counter Details: Remain Counter 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_remain_counter_500',
            		'display_column_name': 'ATM Counter Details: Remain Counter 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_remain_counter_1000',
            		'display_column_name': 'ATM Counter Details: Remain Counter 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_remain_counter_2000',
            		'display_column_name': 'ATM Counter Details: Remain Counter 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount_remain_counter_total',
            		'display_column_name': 'ATM Counter Details: Remain Counter Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_cash__from_cassettes_100',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_cash__from_cassettes_200',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_cash__from_cassettes_500',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_cash__from_cassettes_1000',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_cash__from_cassettes_2000',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_cash__from_cassettes_total',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_purge_bin_100',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_purge_bin_200',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_purge_bin_500',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_purge_bin_1000',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_purge_bin_2000',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_purge_bin_total',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_total_remainingcash_100',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_total_remainingcash_200',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_total_remainingcash_500',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_total_remainingcash_1000',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_total_remainingcash_2000',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical_total_remainingcash_total',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_100',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_200',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_500',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_1000',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_2000',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_total',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_sealno_100',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_sealno_200',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_sealno_500',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_sealno_1000',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_cashreturns_sealno_2000',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_return_cashfrompurge_100',
            		'display_column_name': 'Atm Return Cash from purge : 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_return_cashfrompurge_200',
            		'display_column_name': 'Atm Return Cash from purge : 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_return_cashfrompurge_500',
            		'display_column_name': 'Atm Return Cash from purge : 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_return_cashfrompurge_1000',
            		'display_column_name': 'Atm Return Cash from purge : 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_return_cashfrompurge_2000',
            		'display_column_name': 'Atm Return Cash from purge : 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_return_cashfrompurge_total',
            		'display_column_name': 'Atm Return Cash from purge : Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_100',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_200',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_500',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_1000',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_2000',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_total',
            		'display_column_name': 'ATM Repl. Details: Amount replenished Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_seal_100',
            		'display_column_name': 'ATM Repl. Details:Seal No 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_seal_200',
            		'display_column_name': 'ATM Repl. Details:Seal No 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_seal_500',
            		'display_column_name': 'ATM Repl. Details:Seal No 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_seal_1000',
            		'display_column_name': 'ATM Repl. Details:Seal No 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_repl_seal_2000',
            		'display_column_name': 'ATM Repl. Details:Seal No 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingcashreturn_100',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingcashreturn_200',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingcashreturn_500',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingcashreturn_1000',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingcashreturn_2000',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingcashreturn_total',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingbal_100',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingbal_200',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingbal_500',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingbal_1000',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingbal_2000',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm_closingbal_total',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_count_openbal_100',
            		'display_column_name': 'Switch Counter: Opening Balance 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_count_openbal_200',
            		'display_column_name': 'Switch Counter: Opening Balance 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_count_openbal_500',
            		'display_column_name': 'Switch Counter: Opening Balance 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_count_openbal_1000',
            		'display_column_name': 'Switch Counter: Opening Balance 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_count_openbal_2000',
            		'display_column_name': 'Switch Counter: Opening Balance 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_count_openbal_total',
            		'display_column_name': 'Switch Counter: Opening Balance Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_disp_100',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_disp_200',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_disp_500',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_disp_1000',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_disp_2000',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_disp_total',
            		'display_column_name': 'Switch Counter: Dispense As per Switch Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_loading_100',
            		'display_column_name': 'Switch Counter :  Loading 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_loading_200',
            		'display_column_name': 'Switch Counter :  Loading 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_loading_500',
            		'display_column_name': 'Switch Counter :  Loading 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_loading_1000',
            		'display_column_name': 'Switch Counter :  Loading 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_loading_2000',
            		'display_column_name': 'Switch Counter :  Loading 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_loading_total',
            		'display_column_name': 'Switch Counter :  Loading Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_adminincrease_100',
            		'display_column_name': 'Switch Counter :  Admin increase 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_adminincrease_200',
            		'display_column_name': 'Switch Counter :  Admin increase 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_adminincrease_500',
            		'display_column_name': 'Switch Counter :  Admin increase 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_adminincrease_1000',
            		'display_column_name': 'Switch Counter :  Admin increase 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_adminincrease_2000',
            		'display_column_name': 'Switch Counter :  Admin increase 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_adminincrease_total',
            		'display_column_name': 'Switch Counter :  Admin increase Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_admindecrease_100',
            		'display_column_name': 'Switch Counter :  Admin decrease 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_admindecrease_200',
            		'display_column_name': 'Switch Counter :  Admin decrease 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_admindecrease_500',
            		'display_column_name': 'Switch Counter :  Admin decrease 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_admindecrease_1000',
            		'display_column_name': 'Switch Counter :  Admin decrease 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_admindecrease_2000',
            		'display_column_name': 'Switch Counter :  Admin decrease 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_admindecrease_total',
            		'display_column_name': 'Switch Counter :  Admin decrease Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_closing_100',
            		'display_column_name': 'Switch Counter :  Closing 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_closing_200',
            		'display_column_name': 'Switch Counter :  Closing 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_closing_500',
            		'display_column_name': 'Switch Counter :  Closing 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_closing_1000',
            		'display_column_name': 'Switch Counter :  Closing 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_closing_2000',
            		'display_column_name': 'Switch Counter :  Closing 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw_closing_total',
            		'display_column_name': 'Switch Counter :  Closing Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_overage_100',
            		'display_column_name': 'Physical Difference: Overage 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_overage_200',
            		'display_column_name': 'Physical Difference: Overage 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_overage_500',
            		'display_column_name': 'Physical Difference: Overage 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_overage_1000',
            		'display_column_name': 'Physical Difference: Overage 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_overage_2000',
            		'display_column_name': 'Physical Difference: Overage 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_overage_total',
            		'display_column_name': 'Physical Difference: Overage Total',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_shortage_100',
            		'display_column_name': 'Physical Difference: Shortage 100',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_shortage_200',
            		'display_column_name': 'Physical Difference: Shortage 200',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_shortage_500',
            		'display_column_name': 'Physical Difference: Shortage 500',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_shortage_1000',
            		'display_column_name': 'Physical Difference: Shortage 1000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_shortage_2000',
            		'display_column_name': 'Physical Difference: Shortage 2000',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys_diff_shortage_total',
            		'display_column_name': 'Physical Difference: Shortage Tota',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'remarks',
            		'display_column_name': 'Remarks',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'project_id',
            		'display_column_name': 'Project Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'record_status',
            		'display_column_name': 'Record Status',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'created_on',
            		'display_column_name': 'Created On',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'created_by',
            		'display_column_name': 'Created By',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'created_reference_id',
            		'display_column_name': 'Created Reference Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'datafor_date_time',
            		'display_column_name': 'Datafor Date Time',
					'datafor': 'C3R_CMIS',
					'datatype':'date'
            	},
            	{
            		'table_column': 'approved_on',
            		'display_column_name': 'Approved On',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'approved_by',
            		'display_column_name': 'Approved By',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'approved_reference_id',
            		'display_column_name': 'Approved Reference Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'rejected_on',
            		'display_column_name': 'Rejected On',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'rejected_by',
            		'display_column_name': 'Rejected By',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'reject_reference_id',
            		'display_column_name': 'Reject Reference Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'reject_trigger_reference_id',
            		'display_column_name': 'Reject Trigger Reference Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'deleted_on',
            		'display_column_name': 'Deleted On',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'deleted_by',
            		'display_column_name': 'Deleted By',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'deleted_reference_id',
            		'display_column_name': 'Deleted Reference Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'modified_on',
            		'display_column_name': 'Modified On',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'modified_by',
            		'display_column_name': 'Modified By',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'modified_reference_id',
            		'display_column_name': 'Modified Reference Id',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'is_valid_record',
            		'display_column_name': 'Is Valid Record',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'error_code',
            		'display_column_name': 'Error Code',
					'datafor': 'C3R_CMIS',
					'datatype':'string'
            	},
            	{
            		'table_column': 'id',
            		'display_column_name': 'Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sr no',
            		'display_column_name': 'Sr No',
					'datatype':'string'
            	},
            	{
            		'table_column': 'bank',
            		'display_column_name': 'Bank',
					'datatype':'string'
            	},
            	{
            		'table_column': 'feeder branch',
            		'display_column_name': 'Feeder Branch',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm id',
            		'display_column_name': 'Atm Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'location',
            		'display_column_name': 'Location',
					'datatype':'string'
            	},
            	{
            		'table_column': 'date',
            		'display_column_name': 'Date',
					'datatype':'date'
            	},
            	{
            		'table_column': 'eod loading time',
            		'display_column_name': 'Eod Loading Time',
					'datatype':'string'
            	},
            	{
            		'table_column': 'status of loading',
            		'display_column_name': 'Status Of Loading',
					'datatype':'string'
            	},
            	{
            		'table_column': 'last transaction no',
            		'display_column_name': 'Last Transaction No',
					'datatype':'string'
            	},
            	{
            		'table_column': 'cra',
            		'display_column_name': 'Cra',
					'datatype':'string'
            	},
            	{
            		'table_column': 'indentno',
            		'display_column_name': 'Indentno',
					'datatype':'string'
            	},
            	{
            		'table_column': 'bankrefno',
            		'display_column_name': 'Bankrefno',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount op  bal 100',
            		'display_column_name': 'ATM Counter Details: Op  Bal 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount op  bal 200',
            		'display_column_name': 'ATM Counter Details: Op  Bal 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount op  bal 500',
            		'display_column_name': 'ATM Counter Details: Op  Bal 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount op  bal 1000',
            		'display_column_name': 'ATM Counter Details: Op  Bal 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount op  bal 2000',
            		'display_column_name': 'ATM Counter Details: Op  Bal 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount op bal total',
            		'display_column_name': 'ATM Counter Details: Op Bal Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount disp 100',
            		'display_column_name': 'ATM Counter Details: Disp 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount disp 200',
            		'display_column_name': 'ATM Counter Details: Disp 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount disp 500',
            		'display_column_name': 'ATM Counter Details: Disp 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount disp 1000',
            		'display_column_name': 'ATM Counter Details: Disp 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount disp 2000',
            		'display_column_name': 'ATM Counter Details: Disp 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount disp total',
            		'display_column_name': 'ATM Counter Details: Disp Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount divert count 100',
            		'display_column_name': 'ATM Counter Details: Divert Count 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount divert count 200',
            		'display_column_name': 'ATM Counter Details: Divert Count 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount divert count 500',
            		'display_column_name': 'ATM Counter Details: Divert Count 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount divert count 1000',
            		'display_column_name': 'ATM Counter Details: Divert Count 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount divert count 2000',
            		'display_column_name': 'ATM Counter Details: Divert Count 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount divert count total',
            		'display_column_name': 'ATM Counter Details: Divert Count Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount remain counter 100',
            		'display_column_name': 'ATM Counter Details: Remain Counter 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount remain counter 200',
            		'display_column_name': 'ATM Counter Details: Remain Counter 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount remain counter 500',
            		'display_column_name': 'ATM Counter Details: Remain Counter 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount remain counter 1000',
            		'display_column_name': 'ATM Counter Details: Remain Counter 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount remain counter 2000',
            		'display_column_name': 'ATM Counter Details: Remain Counter 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmcount remain counter total',
            		'display_column_name': 'ATM Counter Details: Remain Counter Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical cash  from cassettes 100',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical cash  from cassettes 200',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical cash  from cassettes 500',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical cash  from cassettes 1000',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 1000'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atmphysical cash  from cassettes 2000',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical cash  from cassettes total',
            		'display_column_name': 'ATM  Details (Physical):  Cash  From Cassettes Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical purge bin 100',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical purge bin 200',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical purge bin 500',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical purge bin 1000',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical purge bin 2000',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical purge bin total',
            		'display_column_name': 'ATM  Details (Physical):  Purge Bin Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical total remainingcash 100',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical total remainingcash 200',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical total remainingcash 500',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical total remainingcash 1000',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical total remainingcash 2000',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atmphysical total remainingcash total',
            		'display_column_name': 'ATM  Details (Physical):  Total Remainingcash Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns 100',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns 200',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns 500',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns 1000',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns 2000',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns total',
            		'display_column_name': 'ATM Return Cash to Vault/Bank: Cash From Cassettes Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns sealno 100',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns sealno 200',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns sealno 500',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns sealno 1000',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm cashreturns sealno 2000',
            		'display_column_name': 'ATM Return Cash to Vault/Bank Sealno : 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm return cashfrompurge 100',
            		'display_column_name': 'Atm Return Cash from purge : 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm return cashfrompurge 200',
            		'display_column_name': 'Atm Return Cash from purge : 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm return cashfrompurge 500',
            		'display_column_name': 'Atm Return Cash from purge : 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm return cashfrompurge 1000',
            		'display_column_name': 'Atm Return Cash from purge : 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm return cashfrompurge 2000',
            		'display_column_name': 'Atm Return Cash from purge : 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm return cashfrompurge total',
            		'display_column_name': 'Atm Return Cash from purge : Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl 100',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl 200',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl 500',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl 1000',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl 2000',
            		'display_column_name': 'ATM Repl. Details: Amount replenished 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl total',
            		'display_column_name': 'ATM Repl. Details: Amount replenished Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl seal 100',
            		'display_column_name': 'ATM Repl. Details:Seal No 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl seal 200',
            		'display_column_name': 'ATM Repl. Details:Seal No 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl seal 500',
            		'display_column_name': 'ATM Repl. Details:Seal No 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl seal 1000',
            		'display_column_name': 'ATM Repl. Details:Seal No 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm repl seal 2000',
            		'display_column_name': 'ATM Repl. Details:Seal No 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'atm closingcashreturn 100',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 100'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingcashreturn 200',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 200'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingcashreturn 500',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 500'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingcashreturn 1000',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 1000'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingcashreturn 2000',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 2000'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingcashreturn total',
            		'display_column_name': 'ATM Returns / Closing Balance: Total ATM Cash Returns 100 Total'
					,
					'datatype':'string'
				}
				,
            	{
            		'table_column': 'atm closingbal 100',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 100'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingbal 200',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 200'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingbal 500',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 500'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingbal 1000',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 1000'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingbal 2000',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM 2000'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'atm closingbal total',
            		'display_column_name': 'ATM Returns / Closing Balance: Closing Balance in ATM Total'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw count openbal 100',
            		'display_column_name': 'Switch Counter: Opening Balance 100'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw count openbal 200',
            		'display_column_name': 'Switch Counter: Opening Balance 200'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw count openbal 500',
            		'display_column_name': 'Switch Counter: Opening Balance 500'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw count openbal 1000',
            		'display_column_name': 'Switch Counter: Opening Balance 1000'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw count openbal 2000',
            		'display_column_name': 'Switch Counter: Opening Balance 2000'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw count openbal total',
            		'display_column_name': 'Switch Counter: Opening Balance Total'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw disp 100',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 100'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw disp 200',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 200'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw disp 500',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 500'
					,
					'datatype':'string'
				},
            	{
            		'table_column': 'sw disp 1000',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw disp 2000',
            		'display_column_name': 'Switch Counter: Dispense As per Switch 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw disp total',
            		'display_column_name': 'Switch Counter: Dispense As per Switch Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw loading 100',
            		'display_column_name': 'Switch Counter :  Loading 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw loading 200',
            		'display_column_name': 'Switch Counter :  Loading 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw loading 500',
            		'display_column_name': 'Switch Counter :  Loading 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw loading 1000',
            		'display_column_name': 'Switch Counter :  Loading 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw loading 2000',
            		'display_column_name': 'Switch Counter :  Loading 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw loading total',
            		'display_column_name': 'Switch Counter :  Loading Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw adminincrease 100',
            		'display_column_name': 'Switch Counter :  Admin increase 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw adminincrease 200',
            		'display_column_name': 'Switch Counter :  Admin increase 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw adminincrease 500',
            		'display_column_name': 'Switch Counter :  Admin increase 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw adminincrease 1000',
            		'display_column_name': 'Switch Counter :  Admin increase 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw adminincrease 2000',
            		'display_column_name': 'Switch Counter :  Admin increase 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw adminincrease total',
            		'display_column_name': 'Switch Counter :  Admin increase Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw admindecrease 100',
            		'display_column_name': 'Switch Counter :  Admin decrease 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw admindecrease 200',
            		'display_column_name': 'Switch Counter :  Admin decrease 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw admindecrease 500',
            		'display_column_name': 'Switch Counter :  Admin decrease 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw admindecrease 1000',
            		'display_column_name': 'Switch Counter :  Admin decrease 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw admindecrease 2000',
            		'display_column_name': 'Switch Counter :  Admin decrease 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw admindecrease total',
            		'display_column_name': 'Switch Counter :  Admin decrease Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw closing 100',
            		'display_column_name': 'Switch Counter :  Closing 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw closing 200',
            		'display_column_name': 'Switch Counter :  Closing 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw closing 500',
            		'display_column_name': 'Switch Counter :  Closing 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw closing 1000',
            		'display_column_name': 'Switch Counter :  Closing 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw closing 2000',
            		'display_column_name': 'Switch Counter :  Closing 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'sw closing total',
            		'display_column_name': 'Switch Counter :  Closing Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff overage 100',
            		'display_column_name': 'Physical Difference: Overage 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff overage 200',
            		'display_column_name': 'Physical Difference: Overage 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff overage 500',
            		'display_column_name': 'Physical Difference: Overage 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff overage 1000',
            		'display_column_name': 'Physical Difference: Overage 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff overage 2000',
            		'display_column_name': 'Physical Difference: Overage 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff overage total',
            		'display_column_name': 'Physical Difference: Overage Total',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff shortage 100',
            		'display_column_name': 'Physical Difference: Shortage 100',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff shortage 200',
            		'display_column_name': 'Physical Difference: Shortage 200',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff shortage 500',
            		'display_column_name': 'Physical Difference: Shortage 500',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff shortage 1000',
            		'display_column_name': 'Physical Difference: Shortage 1000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff shortage 2000',
            		'display_column_name': 'Physical Difference: Shortage 2000',
					'datatype':'string'
            	},
            	{
            		'table_column': 'phys diff shortage total',
            		'display_column_name': 'Physical Difference: Shortage Tota',
					'datatype':'string'
            	},
            	{
            		'table_column': 'remarks',
            		'display_column_name': 'Remarks',
					'datatype':'string'
            	},
            	{
            		'table_column': 'project id',
            		'display_column_name': 'Project Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'record status',
            		'display_column_name': 'Record Status',
					'datatype':'string'
            	},
            	{
            		'table_column': 'created on',
            		'display_column_name': 'Created On',
					'datatype':'string'
            	},
            	{
            		'table_column': 'created by',
            		'display_column_name': 'Created By',
					'datatype':'string'
            	},
            	{
            		'table_column': 'created reference id',
            		'display_column_name': 'Created Reference Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'datafor date time',
            		'display_column_name': 'Datafor Date Time',
					'datatype':'date'
            	},
            	{
            		'table_column': 'approved on',
            		'display_column_name': 'Approved On'
            	},
            	{
            		'table_column': 'approved by',
            		'display_column_name': 'Approved By',
					'datatype':'string'
            	},
            	{
            		'table_column': 'approved reference id',
            		'display_column_name': 'Approved Reference Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'rejected on',
            		'display_column_name': 'Rejected On',
					'datatype':'string'
            	},
            	{
            		'table_column': 'rejected by',
            		'display_column_name': 'Rejected By',
					'datatype':'string'
            	},
            	{
            		'table_column': 'reject reference id',
            		'display_column_name': 'Reject Reference Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'reject trigger reference id',
            		'display_column_name': 'Reject Trigger Reference Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'deleted on',
            		'display_column_name': 'Deleted On',
					'datatype':'string'
            	},
            	{
            		'table_column': 'deleted by',
            		'display_column_name': 'Deleted By',
					'datatype':'string'
            	},
            	{
            		'table_column': 'deleted reference id',
            		'display_column_name': 'Deleted Reference Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'modified on',
            		'display_column_name': 'Modified On',
					'datatype':'string'
            	},
            	{
            		'table_column': 'modified by',
            		'display_column_name': 'Modified By',
					'datatype':'string'
            	},
            	{
            		'table_column': 'modified reference id',
            		'display_column_name': 'Modified Reference Id',
					'datatype':'string'
            	},
            	{
            		'table_column': 'is valid record',
            		'display_column_name': 'Is Valid Record',
					'datatype':'string'
            	},
            	{
            		'table_column': 'error code',
            		'display_column_name': 'Error Code',
					'datatype':'string'
            	}
            ];




}
