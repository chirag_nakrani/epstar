import { Component, OnInit , TemplateRef } from '@angular/core';
import {ViewChild, ElementRef} from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Page} from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { ColumnNameProviderService } from './column-name-provider.service';
import { SessionStorageService } from 'ngx-store';

declare var $: any;

@Component({
  selector: 'app-crac3rreport',
  templateUrl: './crac3rreport.component.html',
  styleUrls: ['./crac3rreport.component.scss']
})
export class Crac3rreportComponent implements OnInit {
  @ViewChild('fileInput')myInputVariable: ElementRef;
  @ViewChild('previewModal') previewModal: ElementRef;
  public uploadData: any = {};
  fileType = 'C3R';
  fileType_VMIS = 'C3R_VMIS';
  public cra_name = '';
  public record_status = 'Active';
  public currentPageSize: number;
  public currentPage = 1;
  alert: any;
  datePipe: DatePipe;
  uploadForm: FormGroup;
  activeTab: string;
  submitted = false;
  temp = [];
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: ColumnConfig;
  fileName: string;
  private pagesizeList: any = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  modalRef: BsModalRef;
  page: Page = new Page();
  previewPage: Page = new Page();
  diRows: any;
  diColumn: any;
  filterData: any = [];
  currentFileIndex: number;
  tempRowsCMIS: any = [];
  tempRowsVMIS: any = [];
  tempPageCMIS: Page = new Page();
  tempPageVMIS: Page = new Page();
  public viewColCMIS: any = [];
  public viewColVMIS: any = [];
  craDDList: any = [];
  rows: any = [];
  prevRows: any = [];
  currentFileData: any = {};
  public permissionList: any = [];
  public filtersForAllView: any = {};
  public date_range: Array<any> = [];
  public statusList: Array<string>;
  public bankFilterDropDown: any = [];
  public feederDropDown: any = [];
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private columnNameProviderService: ColumnNameProviderService, private modalService: BsModalService, private sessionStorageService: SessionStorageService) {
    this.diColumn = this.columnNameProviderService.dataInformationColumns;
    this.datePipe = new DatePipe('en-US');
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.currentPageSize = 20;
    this.dropdownValue = [];
   }
  ngOnInit() { //Called on page initialization
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    this.getDropdowns();
    this.setPageDataCmis(0);
    this.setDataInformationCurrent();
    this.getViewCol();
    this.uploadData.date = new Date();
    this.uploadData.time = '00:00';
    this.alert.isvisible = false;
    this.uploadForm = this.formBuilder.group({
      cra: ['', Validators.required],
      date: [ this.uploadData.date],
      time: [  this.uploadData.time]
  });
    this.filtersForAllView.record_status = "Active";
    this.statusList = ['All', 'Active', 'Deleted', 'Rejected', 'Uploaded'];

  }


  getDropdowns() {
    const params = {
          'file_type': this.fileType,
          'routine_info': 'ROUTINE_DATA_INFO'
      };
    this.apiService.post('bankinfo/', params).subscribe((data: any) => {
       console.log('-------dropdown', data.data);
       this.craDDList = data.data ;
    }, (error) => {
       console.log('error in dropdown value', error);
    });
    this.apiService.post('bankinfo/', {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;
      console.log('-------dropdown', response.data);
      this.dropdownValue = response.data;
      console.log(this.dropdownValue); // handle event here
      this.getAllBankList();
    }, (error) => {
      console.log('error in dropdown value', error);
    });
  }

  filterPageData(pageNumber: any) {
    this.activeTab = 'C3R_CMIS';
    const params = {
      'file_type' : 'C3R_CMIS',
      'filters': {
        'record_status' : this.record_status ,
      //  "cra_name" : this.cra_name
      }
    };
    this.page.pageNumber = pageNumber;
    this.currentPage = pageNumber + 1;
    this.apiService.post('viewc3r/?page=' + (this.currentPage) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      this.page.totalElements = response.count;
      this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      console.log('View Error', error.error);
    });
  }

  setPreviewCmis(pageNumber: number) {
    this.activeTab = 'C3R_CMIS';
    const params = {
        'file_type' : 'C3R_CMIS',
        'filters': {
          'created_reference_id' : this.currentFileData.fileReferenceId
        //  "cra_name" : this.cra_name
        }
      };
    this.page.pageNumber = pageNumber;
    this.currentPage = pageNumber + 1;
    this.apiService.post('viewc3r/?page=' + (this.currentPage) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
      this.prevRows = response.results;
      this.prevRows = [...this.prevRows];
      this.page.totalElements = response.count;
      this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      console.log('View Error', error.error);
    });
  }

  setPreviewVmis(pageNumber: number) {
      this.activeTab = 'C3R_VMIS';
      const params = {
        'file_type' : this.activeTab,
        'filters': {
          'created_reference_id' : this.currentFileData.fileReferenceId
        //  "cra_name" : this.cra_name
        }
      };
      this.page.pageNumber = pageNumber;
      this.currentPage = pageNumber + 1;
      this.apiService.post('viewc3r/?page=' + (this.currentPage + pageNumber) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
        this.prevRows = response.results;
        this.prevRows = [...this.prevRows];
        this.page.totalElements = response.count;
        this.tempPageCMIS = this.page;
        this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
      }, (error) => {
        console.log('View Error', error.error);
      });
    }

  setPageDataCmis(pageNumber: number) {
    this.activeTab = 'C3R_CMIS';
    this.filtersForAllView.record_status = "Active";
    if (this.date_range) {
      this.date_range = this.date_range.map( x => {
        return this.datePipe.transform(new Date(x), 'yyyy-MM-dd');
      });
    }
    let params: any;
    for (let key in this.filtersForAllView) {
      if (this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All') {
        delete this.filtersForAllView[key];
        }
    }
    if ( this.date_range.length > 0 ) {

      params = {
        'date_range': this.date_range,
        'file_type' : this.activeTab,
        'filters': this.filtersForAllView
      };
    } else {

      params = {
        'file_type' : this.activeTab,
        'filters': this.filtersForAllView
      };
    }
    this.page.pageNumber = pageNumber;
    this.currentPage = pageNumber + 1;
    this.apiService.post('viewc3r/?page=' + (this.currentPage) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      this.tempRowsCMIS = response.results;
      this.page.totalElements = response.count;
      this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      console.log('View Error', error.error);
    });
  }



  setPageDataVmis(pageNumber: number) {
      this.activeTab = 'C3R_VMIS';
      if (this.date_range) {
        this.date_range = this.date_range.map( x => {
          return this.datePipe.transform(new Date(x), 'yyyy-MM-dd');
        });
      }
      let params: any;
      for (let key in this.filtersForAllView) {
        if (this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All') {
          delete this.filtersForAllView[key];
        }
      }
      if ( this.date_range.length > 0 ) {
        params = {
          'date_range': this.date_range,
        'file_type' : this.activeTab,
          'filters': this.filtersForAllView
        };
      } else {
        params = {
          'file_type' : this.activeTab,
          'filters': this.filtersForAllView
      };
      }
      this.page.pageNumber = pageNumber;
      this.currentPage = pageNumber + 1;
      this.apiService.post('viewc3r/?page=' + (this.currentPage + pageNumber) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
        this.rows = response.results;
        this.rows = [...this.rows];
        this.tempRowsVMIS = response.results;
        this.page.totalElements = response.count;
        this.tempPageVMIS = this.page;
        this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
      }, (error) => {
        console.log('View Error', error.error);
      });
    }

    resetFilter() {
      this.filtersForAllView = {};
      this.date_range = [];
    }

    getAllBankList() {
      this.bankFilterDropDown = [];
      this.dropdownValue.forEach( x => {
        x.bank.forEach( y => {
          this.bankFilterDropDown.push(y);
        });
      });
      this.getFeederList();
    }

    trackOpt(index: number, opt: any) {
      
      return opt ? opt.feeder : undefined;
    }

    getFeederList() {
      this.feederDropDown = [];
      if ( this.filtersForAllView.bank_name == undefined ) {
        this.bankFilterDropDown.forEach( x => {
          x.feeder.forEach( y => {
            if (this.feederDropDown.indexOf(y) === -1) {
              console.log('dd', this.feederDropDown.indexOf(y) , y , this.feederDropDown, this.feederDropDown.indexOf(y) === -1);
              this.feederDropDown.push(y);
            }
          });
        });
      } else {
        this.feederDropDown = this.bankFilterDropDown.filter( x => x.bank_id == this.filtersForAllView.bank_name )[0].feeder;
      }
    }

    setFilteredView() {
      if (this.activeTab == 'C3R_CMIS') {
        this.setPageDataCmis(0);
      } else if (this.activeTab == 'C3R_VMIS') {
        this.setPageDataVmis(0);
      }
      $('#modalfilter').modal('hide');
    }

  getViewCol() {

    this.viewColCMIS = [];
    this.viewColVMIS = [];

    this.columnNameProviderService.columnInfoCmis.forEach(x => {
      this.viewColCMIS.push({'name': x.display_column_name, 'prop': x.table_column,"datatype":x.datatype});
    });

    this.columnNameProviderService.columnInfoVmis.forEach(x => {
      this.viewColVMIS.push({'name': x.display_column_name, 'prop': x.table_column,"datatype":x.datatype});
    });

  }

  approveFile(rowData: any) {
    const params = {
      'status' : 'Approved' ,
      'file_type' : 'C3R',
      'datafor_date_time' : rowData.datafor_date_time ,
      'project_id': 'MOF',
      'bank_code' : 'ALL',
      'created_reference_id' : rowData.created_reference_id
    };
    this.setFileStatus(params);
    
  }
  rejectFile(rowData: any) {
    const params = {
      'status' : 'Rejected' ,
      'file_type' : 'C3R',
      'datafor_date_time' : rowData.datafor_date_time ,
      'project_id': 'MOF',
      'bank_code' : 'ALL',
      'created_reference_id' : rowData.created_reference_id
    };
    this.setFileStatus(params);
  }

  setDataInformationCurrent() {
    const params = {
      'data_for_type' : this.fileType,
      'record_status' : ['Approval Pending']
    };

    this.getDataInfo(params);
  }


  setDataInformationPrevious() {
    const params = {
      'data_for_type' : this.fileType,
      'record_status' : ['Active', 'Rejected']
    };

    this.getDataInfo(params);
  }

  calculateTotalPage(rowcount, rowsize) {
      return Math.ceil(rowcount / rowsize);
  }

  getDataInfo(params: any) {
    this.apiService.post('c3rapprovalpending/', params).subscribe((data: any) => {
      const tempRow = data.data;
      this.diRows = tempRow;
      this.diRows = [...this.diRows];
    }, (error) => {
      console.log('Data Info Error', error.error.status_text);
    });
  }

  setFileStatus(params: any) {
    this.apiService.post('status/', params).subscribe((data: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Status';
      this.alert.class = 'success';
      this.alert.text = ' ' + data.result;
      this.setDataInformationCurrent();
      this.setPageDataCmis(0);
    }, (error) => {
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      const error_str: string = error.error['result'];
      this.alert.text =  error_str;
      this.alert.class = 'danger';
    });
  }

  openTimeSelector() {
    console.log('time picker opened');
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.uploadData.time = time;
      console.log('time picker closed');

    });
}


onFileChanged(event) {

  //const file = event.target.files[0]
  this.uploadData.file = event.target.files[0];
  this.fileName = this.uploadData.file.name;
  console.log("******************",this.myInputVariable.nativeElement.files);
  this.myInputVariable.nativeElement.value = "";
  console.log(this.myInputVariable.nativeElement.files);
}


uploadDatafn() {
  // Selected date formatting
  const hours_min = this.uploadForm.value.time.split(':');
  this.uploadData.upload_datetime = new Date(this.uploadForm.value.date);
  this.uploadData.upload_datetime.setHours(+hours_min[0], +hours_min[1], '00');
  const date = this.datePipe.transform(this.uploadData.upload_datetime, 'yyyy-MM-dd HH:mm:ss');
//
  console.log('upload clicked', this.uploadData);
  const fileData = new FormData();
  fileData.append('file', this.uploadData.file, this.uploadData.file.name);
  fileData.append('cra', this.uploadData.cra);
  fileData.append('file_type', this.fileType_VMIS);
  fileData.append('upload_datatime', date);

  console.log('upload clicked formdata', fileData);


  this.apiService.uploadFile('upload/', fileData).subscribe((event: any) => {
   
      if (event && event.body) {
        console.log('resonse', event.body);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        this.alert.class = 'success';
        this.alert.text = ' ' + event.body.status_text;
        $('#m_modal_6').modal('hide');
        this.setDataInformationCurrent();
        this.filtersForAllView={
          record_status: "Active"
        };
        this.setPageDataCmis(0);

        //this.updateViewAll();
      }
    }, error => {

        console.log('error', error);
        $('#m_modal_6').modal('hide');
        this.alert.isvisible = true;
        this.alert.type = 'Error';
        const error_str: string = error.error['status_text'];
        this.alert.text =  error_str;
        this.alert.class = 'danger';

    });

    this.clearUploadFormData();
  }
  clearUploadFormData() {
    this.uploadData.file = [];
    this.uploadData.cra=[];


  }
  // updateViewAll(){
  //   console.log("updarte called");
  //   console.log('filter parameter',this.filtersForAllView);
  //   this.setPage({ offset: 0 });
  //   $('#modalfilter').modal('hide');
  // }

  openPreviewModal(row: any, isEdit: boolean) {

    const currFileData = {
      "fileReferenceId" : row.created_reference_id,
      "isEdit" : isEdit
    }

    this.currentFileData = currFileData;

    this.modalRef = this.modalService.show(this.previewModal, Object.assign({}, { class: 'preview-modal modal-lg' },{backdrop:"static"}));
  }

  setFilterCmis(){

  }

  setFilterVmis(){
  }
}
