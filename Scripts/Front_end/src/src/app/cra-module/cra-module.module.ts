import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { CommonImportsModule } from '../common/common-imports.module';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';
import { Crac3rreportComponent } from './crac3rreport/crac3rreport.component';

const routes: Routes = [
  { path: 'crac3rreport', component: Crac3rreportComponent,
     canActivate: [AuthCheckService] }
];

@NgModule({
  declarations: [
    Crac3rreportComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class CraModuleModule { }
