import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Imports Common Components

import { DashboardComponent } from './common/commonComponents/dashboard/dashboard.component';
import { LoginComponent } from './common/commonComponents/login/login.component';
import { AuthCheckService } from './common/guards/auth/auth-check.service';

const routes: Routes = [
  // Path For Common Components
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthCheckService] },

  // Redirection Login Component
  { path: '', redirectTo: 'login', pathMatch: 'full', canActivate: [AuthCheckService] },
  { path: 'login', component: LoginComponent },
  // Path For Config Params module
  { path: 'configParams',
    loadChildren: './config-params-module/config-params-module.module#ConfigParamsModuleModule', canActivate: [AuthCheckService] },
  // Path for cra module
  { path: 'cra',
    loadChildren: './cra-module/cra-module.module#CraModuleModule', canActivate: [AuthCheckService] },
  // Path for Exceptions module
  { path: 'exceptions',
    loadChildren: './exceptions-module/exceptions-module.module#ExceptionsModuleModule', canActivate: [AuthCheckService] },
  // Path for Master Data module
  { path: 'masterData',
    loadChildren: './master-data-module/master-data-module.module#MasterDataModuleModule', canActivate: [AuthCheckService] },
  // Path for Operations module
  { path: 'operations',
    loadChildren: './operations-module/operations-module.module#OperationsModuleModule', canActivate: [AuthCheckService] },
  // Path for Reference Data module
  { path: 'reference',
    loadChildren: './reference-data-module/reference-data-module.module#ReferenceDataModuleModule', canActivate: [AuthCheckService] },
  // Path for Routine Data module
  { path: 'routine',
    loadChildren: './routine-data-module/routine-data-module.module#RoutineDataModuleModule', canActivate: [AuthCheckService] },
  // Path for System Setting module
  { path: 'systemsetting',
    loadChildren: './system-settings-module/system-settings-module.module#SystemSettingsModuleModule', canActivate: [AuthCheckService]},
  // Path for User Module
  { path: 'user',
    loadChildren: './user-module/user-module.module#UserModuleModule', canActivate: [AuthCheckService] },
  // Path for workflow module
  { path: 'workflow',
    loadChildren: './workflow-module/workflow-module.module#WorkflowModuleModule', canActivate: [AuthCheckService] },
  { path: 'c3rreports',
    loadChildren: './c3rreports/c3rreports.module#C3rreportsModule', canActivate: [AuthCheckService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {

}
