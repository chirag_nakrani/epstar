import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DispenseformulaComponent } from './dispenseformula.component';

describe('DispenseformulaComponent', () => {
  let component: DispenseformulaComponent;
  let fixture: ComponentFixture<DispenseformulaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DispenseformulaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DispenseformulaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
