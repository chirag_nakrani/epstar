import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtmRevisionCashAvailabilityComponent } from './atm-revision-cash-availability.component';

describe('AtmRevisionCashAvailabilityComponent', () => {
  let component: AtmRevisionCashAvailabilityComponent;
  let fixture: ComponentFixture<AtmRevisionCashAvailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtmRevisionCashAvailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtmRevisionCashAvailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
