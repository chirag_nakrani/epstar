import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenominationwisepriorityComponent } from './denominationwisepriority.component';

describe('DenominationwisepriorityComponent', () => {
  let component: DenominationwisepriorityComponent;
  let fixture: ComponentFixture<DenominationwisepriorityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenominationwisepriorityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenominationwisepriorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
