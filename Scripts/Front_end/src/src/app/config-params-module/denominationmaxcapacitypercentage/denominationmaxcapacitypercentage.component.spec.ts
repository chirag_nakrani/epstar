import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DenominationmaxcapacitypercentageComponent } from './denominationmaxcapacitypercentage.component';

describe('DenominationmaxcapacitypercentageComponent', () => {
  let component: DenominationmaxcapacitypercentageComponent;
  let fixture: ComponentFixture<DenominationmaxcapacitypercentageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DenominationmaxcapacitypercentageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DenominationmaxcapacitypercentageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
