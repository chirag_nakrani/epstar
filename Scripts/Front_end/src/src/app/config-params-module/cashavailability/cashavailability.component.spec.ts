import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CashavailabilityComponent } from './cashavailability.component';

describe('CashavailabilityComponent', () => {
  let component: CashavailabilityComponent;
  let fixture: ComponentFixture<CashavailabilityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CashavailabilityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CashavailabilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
