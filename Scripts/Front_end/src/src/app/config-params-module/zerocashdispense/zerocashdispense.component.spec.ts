import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZeroCashDispenseComponent } from './zerocashdispense.component';

describe('ZeroCashDispenseComponent', () => {
  let component: ZeroCashDispenseComponent;
  let fixture: ComponentFixture<ZeroCashDispenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZeroCashDispenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZeroCashDispenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
