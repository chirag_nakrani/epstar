import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';


declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-zerocashdispense',
  templateUrl: './zerocashdispense.component.html',
  styleUrls: ['./zerocashdispense.component.scss']
})
export class ZeroCashDispenseComponent implements OnInit {

  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  modalRef: BsModalRef;
  alert: any;
  page: Page = new Page();
  selected = [];
  selectedids = [];
  submitted = false;
  temp = [];
  previewColList: any;
  public colList: any;
  diRows: any;
  viewColumnAll: any = {};
  datePipe: DatePipe;
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  public editRow: any = {};
  public isMultiEdit: boolean;
  public pagesizeList = [10,20,30,40,50,100];
  public permissionList: any = [];
  public filterData: any = {};
  public projectBankList: any;
  public bankList: any = [];
  public feederList: any = [];
  public now: Date = new Date();
  // Constructor to declare all the variables used
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 10;
    this.currentPageSize = 10;
    this.datePipe = new DatePipe('en-US');
  }


  ngOnInit() {

    if(this.sessionStorageService.get('permissionList') != null)
      this.permissionList = this.sessionStorageService.get('permissionList');
    this.getViewCol();
    this.dropdownsForProjectBank();
    this.allDataActive(0);
    this.isMultiEdit = false;
    this.populateSelectColumn();
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  // ----------code to select column ----------------

  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    });
    // }
    console.log('col change', this.selectedColumn, this.viewColumn);
  }


  // ---------end of code to select column  ----------------------
// --------------------search function ---------------
viewSearch(event) {
  console.log('updateing');
  const val = event.target.value.toLowerCase();
  const temp = this.tempRows.filter(function(d) {
    const bank_code = d.bank_code.toLowerCase().indexOf(val) !== -1;
    const atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
    const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
    return bank_code || atm_id || search_project_id || !val;
  });
  this.rows = temp;
}
//----------- end of serch function ---------

  //------------------------code for view data in datatable----------------------------------
  //commonon
  viewZeroCashDispenseData(offSet: number) {
    this.page.pageNumber = offSet;
    let params = {

    }
    this.filterData.record_status = 'Active';
    if (this.filterData != undefined || this.filterData != {}) {
      const filter = this.filterData;
      if (this.filterData.dispense_date != null) {
        filter.dispense_date = this.datePipe.transform(this.filterData.dispense_date, 'yyyy-MM-dd');
      }
      params = {
        "filters": filter
      }
    }
    console.log('filters.......',params);
    this.apiService.post('zero_cash_dispense/?page=' + (offSet + 1) + '&page_size=' + this.page.size,params).subscribe((response: any) => {
      console.log('resposne......',response.results);
      this.rows = response.results;
      this.rows = [...this.rows];
      this.page.totalElements = response.count;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      this.rows = [];
      if (error.error != null){
        this.alert.text = error.error.status_text;
      } else {
        this.alert.text = error.statusText;
        }
    });
  };
  //end of common section

  //to Display All data of active status
  allDataActive(offSet: number) {
    console.log('inside all data active');
    this.viewZeroCashDispenseData(offSet);
  }
  //---------------------------------------end of code for view data in datatable---------------------------------------
  //------------------code for select particular record -----------------------------
  onSelect({ selected }) {
    this.isMultiEdit = true;
    console.log('Select Event', this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

  }

  //----------------end of code for select particular record--------------

    // ------------------------- to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.ColumnConfigZeroCashDispense);
    this.columnConfig.ColumnConfigZeroCashDispense.forEach(x => {

      this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column, 'datatype': x.dataType });
      // console.log("xxxxxxxxxxx",x);
    });
    this.viewColumnAll = this.colList;
    console.log('viewColumn', this.viewColumnAll);
  }
  // ------------------------- to display column data in modal and in All Data ----------------------

  //---------------generate indent function------------------
  refreshDispense() {

  }
  //---------------end of generate indent function------------------

  dropdownsForProjectBank() {
    const params = {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    };
    this.apiService.post('bankinfo/', params).subscribe((data: any) => {
       this.bankList = [];
       this.projectBankList = data.data;
       this.projectBankList.forEach( x => {
         x.bank.forEach( y => {
           this.bankList.push(y);
         });
       });
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }

  resetFilter() {
    this.filterData = {};
  }

  getBankList() {
    this.bankList = this.projectBankList.filter( x => x.project_id == this.filterData.project_id)[0].bank;
  }

  getFeeder() {
    this.feederList = [];
    this.projectBankList.forEach( x => {
      x.bank.filter( y => y.bank_id == this.filterData.bank_name )[0].feeder.forEach( z => {
        this.feederList.push(z);
      });
    });
  }

  //---------------end of send indent pdf email function------------------

}
