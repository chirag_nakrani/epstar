import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifycasetteconfigsComponent } from './modifycasetteconfigs.component';

describe('ModifycasetteconfigsComponent', () => {
  let component: ModifycasetteconfigsComponent;
  let fixture: ComponentFixture<ModifycasetteconfigsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModifycasetteconfigsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifycasetteconfigsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
