import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VaultingconfigComponent } from './vaultingconfig.component';

describe('VaultingconfigComponent', () => {
  let component: VaultingconfigComponent;
  let fixture: ComponentFixture<VaultingconfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VaultingconfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VaultingconfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
