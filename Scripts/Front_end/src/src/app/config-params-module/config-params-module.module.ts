import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { CashavailabilityComponent } from './cashavailability/cashavailability.component';

import { ConfigurabledenominationsComponent } from './configurabledenominations/configurabledenominations.component';
import { ConfigurablelimitsComponent } from './configurablelimits/configurablelimits.component';
import {
  DenominationmaxcapacitypercentageComponent
} from './denominationmaxcapacitypercentage/denominationmaxcapacitypercentage.component';
import { DenominationwisepriorityComponent } from './denominationwisepriority/denominationwisepriority.component';
import { DispenseformulaComponent } from './dispenseformula/dispenseformula.component';
import { EodactivityComponent } from './eodactivity/eodactivity.component';
import { HolidaymasterComponent } from './holidaymaster/holidaymaster.component';
import { ModifycasetteconfigsComponent } from './modifycasetteconfigs/modifycasetteconfigs.component';
import { QualifyatmsComponent } from './qualifyatms/qualifyatms.component';
import { VaultingconfigComponent } from './vaultingconfig/vaultingconfig.component';
import { ZeroCashDispenseComponent } from './zerocashdispense/zerocashdispense.component';

import { CommonModuleModule } from '../common/commonComponents/common-module.module';
import { CommonImportsModule } from '../common/common-imports.module';
import { BankLimitDaysComponent } from './bank-limit-days/bank-limit-days.component';
import { FeederLimitDaysComponent } from './feeder-limit-days/feeder-limit-days.component';
import { AtmRevisionCashAvailabilityComponent } from './atm-revision-cash-availability/atm-revision-cash-availability.component';
// import { WithdrawalStatusConfirmationComponent } from './withdrawal-status-confirmation/withdrawal-status-confirmation.component';

const routes: Routes = [
  { path: 'cashavailability', component: CashavailabilityComponent,
      canActivate: [AuthCheckService] },
  { path: 'configurabledenominations', component: ConfigurabledenominationsComponent,
      canActivate: [AuthCheckService] },
  { path: 'configurablelimits', component: ConfigurablelimitsComponent,
      canActivate: [AuthCheckService] },
  { path: 'denominationmaxcapacitypercentage', component: DenominationmaxcapacitypercentageComponent,
      canActivate: [AuthCheckService] },
  { path: 'dispenseformula', component: DispenseformulaComponent,
      canActivate: [AuthCheckService] },
  { path: 'denominationwisepriority', component: DenominationwisepriorityComponent,
      canActivate: [AuthCheckService] },
  { path: 'eodactivity', component: EodactivityComponent,
      canActivate: [AuthCheckService] },
  { path: 'holidaymaster', component: HolidaymasterComponent,
      canActivate: [AuthCheckService] },
  { path: 'modifycasetteconfigs', component: ModifycasetteconfigsComponent,
      canActivate: [AuthCheckService] },
  { path: 'qualifyatms', component: QualifyatmsComponent,
      canActivate: [AuthCheckService] },
  { path: 'vaultingconfig', component: VaultingconfigComponent,
      canActivate: [AuthCheckService] },
  { path: 'zerocashdispense', component: ZeroCashDispenseComponent,
      canActivate: [AuthCheckService] },
  { path: 'banklimitdays', component: BankLimitDaysComponent,
      canActivate: [AuthCheckService] },
  { path: 'feederlimitdays', component: FeederLimitDaysComponent,
      canActivate: [AuthCheckService] },
  { path: 'atm_revision_cash_availability', component: AtmRevisionCashAvailabilityComponent,
      canActivate: [AuthCheckService] }
];

@NgModule({
  declarations: [
  CashavailabilityComponent,
  ConfigurabledenominationsComponent,
  ConfigurablelimitsComponent,
  DenominationmaxcapacitypercentageComponent,
  DenominationwisepriorityComponent,
  DispenseformulaComponent,
  EodactivityComponent,
  HolidaymasterComponent,
  ModifycasetteconfigsComponent,
  QualifyatmsComponent,
  VaultingconfigComponent,
  ZeroCashDispenseComponent,
  BankLimitDaysComponent,
  FeederLimitDaysComponent,
  AtmRevisionCashAvailabilityComponent
],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class ConfigParamsModuleModule { }
