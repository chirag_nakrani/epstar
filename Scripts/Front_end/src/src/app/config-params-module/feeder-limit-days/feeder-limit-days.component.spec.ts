import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FeederLimitDaysComponent } from './feeder-limit-days.component';

describe('FeederLimitDaysComponent', () => {
  let component: FeederLimitDaysComponent;
  let fixture: ComponentFixture<FeederLimitDaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeederLimitDaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FeederLimitDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
