import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';



declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-feeder-limit-days',
  templateUrl: './feeder-limit-days.component.html',
  styleUrls: ['./feeder-limit-days.component.scss']
})
export class FeederLimitDaysComponent implements OnInit {

  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  fileType = 'FLD';
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  selected = [];
  selectedids = [];
  previewPage = new Page();
  public uploadData: any;
  uploadForm: FormGroup ;
  submitted = false;
  temp = [];
  previewData: any;
  datePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  diRows: any;
  viewColumnAll: any = [];
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  public editRow: any = {};
  public isMultiEdit: boolean;
  public permissionList: any = [];
  public statusList: Array<string>;
  viewColumns: any = {};
  rows: any = [];
  tempRows: any = [];
  filtersForAllView: any = {};
  viewColumn: any = {};
  selectedColumn: any = [];
  columnChange: any = [];
  public date_range: Array<Date> = [];
  public bankFilterDropDown: any = [];
  public today: Date = new Date();
  public addForm: any = [];
  public bankDropDown: any = [[]];
  public feederList: any = [[]];
  public craDDList: any = [];

  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
    this.datePipe = new DatePipe('en-US');
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.dropdownValue = [];
    this.filterDropDownValue = {};
    this.filtersForAllView.record_status = 'Active';
    this.statusList = ['All', 'Active', 'Approval Pending', 'History', 'Rejected', 'Uploaded'];
    this.addForm.push({'fordate': new Date()});
  }


  ngOnInit() {
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    this.populateDropDown();
    this.setDataInformationCurrent();
    this.uploadData.date = new Date();
    this.uploadData.time = '12:00';
    this.getViewCol();
    this.allDataActive(0);
    this.isMultiEdit = false;
    this.uploadForm = this.formBuilder.group({
      file_type : this.fileType,
    });

    this.populateSelectColumn();
  }
  // ----------------------- upload file ------------------------------------
  // convenience getter for easy access to form fields
  get f() {
    return this.uploadForm.controls;
  }
  openTimeSelector() {
    console.log('time picker opened');
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.uploadData.time = time;
      console.log('time picker closed');

    });
  }
  onFileChanged(event) {

    //const file = event.target.files[0]
    this.uploadData.file = event.target.files[0];
  }

  getBankList(i: number) {
    // console.log("----",this.uploadData.project.bank)
    this.bankDropDown[i] = this.dropdownValue.filter( x => x.project_id == this.addForm[i].project_id )[0].bank;
  }

  getFeederList(i: number) {
    this.feederList[i] = this.bankDropDown[i].filter( x => x.bank_id == this.addForm[i].bank_code)[0].feeder;
  }

  // ------------------------------ upload end -----------------------------------
// ------------------start of code for all select column -----------

  addLimit() {
	this.setLimitType();
    this.addForm.forEach(x => {
      x.fordate = this.datePipe.transform(x.fordate, 'yyyy-MM-dd HH:mm:ss');
    });
    const params = {
      'file_type': this.fileType,
      'records': this.addForm
    };

    this.apiService.uploadFile('configparam/feeder_limitdays/', params).subscribe(event => {
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        this.alert.text = ' ' + response.body.status_desc;
        this.alert.class = 'success';
        $('#m_modal_6').modal('hide');
        this.setDataInformationCurrent();
      }
    }, (error => {
      $('#m_modal_6').modal('hide');
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      this.alert.text = 'STATUS :  ' + error.error.data;
      this.alert.class = 'danger';
    }));
    this.resetLimit();
  }

  populateSelectColumn() {
    this.viewColumnAll.filter( (col) => {
      this.selectedColumn.push(col.prop);
    });
  }

   limitRange(i:number ,e: any, flag: string) {
	if( Number.isNaN(+e.target.value) || this.precision(+e.target.value) > 1 ) {
		if(flag == 'load'){
			this.addForm[i].loading_limit_days = this.addForm[i].loading_limit_days.slice(0, -1);  
		}else if(flag == 'decide'){
			this.addForm[i].decide_limit_days = this.addForm[i].decide_limit_days.slice(0, -1);;  
		}
		e.preventDefault();
		return;		
	}
    if (e.target.value > 7 || e.target.value < 1 || e.key == '-') {
	  if(flag == 'load'){
		this.addForm[i].loading_limit_days = this.addForm[i].loading_limit_days.slice(0, -1);  
	  }else if(flag == 'decide'){
		this.addForm[i].decide_limit_days = this.addForm[i].decide_limit_days.slice(0, -1);;  
	  }		
	  e.preventDefault();
	  return;
    }
	
	
  }
  
  setLimitType(){
	  this.addForm.forEach( x => {
		 if(!Number.isNaN(+x.loading_limit_days))
			x.loading_limit_days = +x.loading_limit_days;
		 if(!Number.isNaN(+x.decide_limit_days))
			x.decide_limit_days = +x.decide_limit_days; 
	  });
  }
  
  precision(a) {
	  let e = 1;
	  while (Math.round(a * e) / e !== a) e *= 10;
	  return Math.log(e) / Math.LN10;
}

  columnChangefn() {
  this.viewColumn = this.viewColumnAll.filter((col) => {
    return this.selectedColumn.includes(col.prop);
  });
  // }
  console.log('col change', this.selectedColumn, this.viewColumn);
  }
// ---------------- end  of code for all select column -----------
// --------------------search function ---------------

  viewSearch(event) {
    console.log('updateing');
    const val = event.target.value.toLowerCase();
    const temp = this.tempRows.filter(function(d) {
      const site_code = d.site_code.toLowerCase().indexOf(val) !== -1;
      const atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
      const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
      return site_code || atm_id || search_project_id || !val;
    });

    this.rows = temp;
  }
//----------- end of serch function ---------

  updateViewAll() {
    console.log('updarte called');
    console.log('filter parameter', this.filtersForAllView);
    this.allDataActive(0);
    $('#modalfilter').modal('hide');
  }

  setPage(pageInfo) {
    // console.log("page clicked");
    console.log('page clicked', pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.apiService.post('viewmaster/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size, {
      'menu': this.fileType,
      'filter_data': this.filtersForAllView
    }).subscribe(data => {
      console.log('-------get all');
      console.log(data); // handle event here
      const response: any = data;
      this.rows = response.results;
      this.tempRows = response.results;
      this.page.totalElements = response.count;
    });
  }
  // ---------end of  view all data ----------------------

  // --------------- drop down value ---------------------
  populateDropDown() {

    this.apiService.post('bankinfo/', {
          'file_type': 'C3R',
          'routine_info': 'ROUTINE_DATA_INFO'
      }).subscribe((data: any) => {
       console.log('-------dropdown', data.data);
       this.craDDList = data.data ;
    }, (error) => {
       console.log('error in dropdown value', error);
    });

    // --------- for upload dropdown----------------------
    this.apiService.post('bankinfo/', {
      'file_type': 'CBR',
      'routine_info': 'ROUTINE_DATA_INFO'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      console.log('-------dropdown', response.data);
      this.dropdownValue = response.data;
      console.log(this.dropdownValue); // handle event here
      this.getAllBankList();
    }, (error) => {
      console.log('error in dropdown value', error);

    });
    // --------------- end of upload dropdown----------------

    // filter dropdown
    this.apiService.post('bankFeederCommonInfo/', {
      'file_type': 'CBR',
      'common_bankfeeder_info': 'common_bankfeeder_info'
    }).subscribe(data => {
      console.log('------- dropdown value ');
      const response: any = data;

      this.filterDropDownValue = response;
      console.log('------- filter dropdown ', this.filterDropDownValue);

      //  this.dropdownValue=[response.data];
      //  console.log(this.dropdownValue); // handle event here

    }, (error) => {
      console.log('error in filter dropdown value', error);

    });
    // end of filter dropdown
  }

  // --------------- end of drop down value -----------------

  resetFilter() {
    this.filtersForAllView = {};
    this.date_range = [];
  }


  clicked(data) {
    console.log(data);
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  getAllBankList() {
    this.bankFilterDropDown = [];
    this.dropdownValue.forEach( x => {
      x.bank.forEach( y => {
        this.bankFilterDropDown.push(y);
      });
    });
  }
  // ------------------------- code for data information ----------------------
  setDataInformationCurrent() {
    const params = {
      'menu': this.fileType,
      'record_status': ['Approval Pending']
    };

    this.getDataInfo(params);
  }
  setDataInformationPrevious() {
    const params = {
      'menu': this.fileType,
      'record_status': ['Active', 'Rejected']
    };

    this.getDataInfo(params);
  }
  getDataInfo(params: any) {
    this.apiService.post('getmasterdata/', params).subscribe((data: any) => {
      const tempRow = data.data;
      this.diRows = tempRow;
      this.diRows = [...this.diRows];
      console.log('Active data', this.diRows);
    }, (error) => {
      console.log('Data Info Error', error.error.status_text);
    });
  }

  // -------------------------End of code for data information ----------------------

  //------------------------code for view file in modal for current and previous tab----------------------------------
  //common section
  viewMasterData(params: any, offSet: number) {
    this.page.pageNumber = offSet;
    this.apiService.post('viewmaster/?page=' + (this.currentPage + offSet) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
      this.rows = response.results;
      this.rows = [...this.rows];
      this.tempRows=[...this.rows];
      this.rows.forEach( (x,index) => {
        this.isEdit[index] = false;
        this.colList.filter( x => x.datatype == 'date').forEach( y => {
          if(x[y.prop] != null){
            x[y.prop] = new Date(x[y.prop]);
          }
        })
      });
      console.log('-----------common', this.rows);
      this.page.totalElements = response.count;
      // // this.tempPageCMIS = this.page;
      this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
    }, (error) => {
      // console.log('View Error', error.error);
    });
  }
  getFileData(offSet: number, status: string) {
    const params = {
      'menu': this.fileType,
      'filter_data': {
        'created_reference_id': this.previewData.created_reference_id,
        'record_status': status
      }
    };
    this.viewMasterData(params, offSet);
  }
  //end of common section
  //to open view file in current tab
  viewFile(rowData: any) {
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0, rowData.record_status);
    this.modalRef = this.modalService.show(this.previewModalcurrent, Object.assign({}, { class: 'preview-modal modal-lg', backdrop : true,
      keyboard : false, ignoreBackdropClick: true }));
  }
  //to open view file in previous tab
  viewFilePrevious(rowData: any) {
    if (rowData != null) {
      this.previewData = rowData;
    }
    this.getFileData(0, rowData.record_status);
    this.modalRef = this.modalService.show(this.previewModalprevious, Object.assign({}, { class: 'preview-modal modal-lg', backdrop : true,
      keyboard : false, ignoreBackdropClick: true }));
  }

  //to Display All data of active status
  allDataActive(offSet: number) {

    if (this.date_range) {
      this.date_range = this.date_range.map( x => {
        return this.datePipe.transform(new Date(x), 'yyyy-MM-dd');
      });
    }
    let params: any;
    for (let key in this.filtersForAllView) {
      if (this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All') {
        delete this.filtersForAllView[key];
      }
    }
    if ( this.date_range.length > 0 ) {
      params = {
        'menu': this.fileType,
        'date_range': this.date_range,
        'filter_data': this.filtersForAllView
      };
    } else {
      params = {
        'menu': this.fileType,
        'filter_data': this.filtersForAllView
      };
    }
    this.viewMasterData(params, offSet);
  }
  //---------------------------------------code for view file in modal for current and previous tab- --------------------------------------
  //------------------code for Modal which is approved or rejected indivisual file -----------------------------
  onSelect({ selected }) {
    this.isMultiEdit = true;
    console.log('Select Event', this.selected);
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);

  }

  resetLimit() {
    this.addForm = [{'fordate': new Date()}];
  }

  addField() {
    this.addForm.push({'fordate': new Date()});
  }

  removeField(i: number) {
    this.addForm.splice(i, 1);
  }

  getApprovedData(params: any) {
    this.apiService.post('ApproveRejectAll/', params).subscribe((response: any) => {
      this.allDataActive(0);
      this.setDataInformationCurrent();
      this.alert.isvisible = true;
      this.alert.type = 'Success';
      this.alert.class = 'success';
      this.alert.text = response.status_desc;
      console.log('-----------', this.rows);
    }, (error:any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Failure';
      this.alert.class = 'danger';
      if (error.error != null){
        this.alert.text = error.error.status_desc;
      } else {
        this.alert.text = error.statusText;
      }
    });
  }
  //to approve indivisual row
  approveIndivisualFile() {
    for (let i = 0; i <= this.selected.length - 1; i++) {
      this.selectedids.push(this.selected[i].id);
    }
    const params = {
      'menu': this.fileType,
      'id': this.selectedids.toString(),
      'status': 'Active',
      'created_reference_id': this.previewData.created_reference_id
    };
    this.getApprovedData(params);
  }
  //to reject indivisual row
  rejectIndivisualFile() {
    for (let i = 0; i <= this.selected.length - 1; i++) {
      this.selectedids.push(this.selected[i].id);
    }
    const params = {
      'menu': this.fileType,
      'id': this.selectedids.toString(),
      'status': 'Rejected',
      'created_reference_id': this.previewData.created_reference_id
    };
    this.getApprovedData(params);
  }
  //to approve complete file
  approveFile(row: any) {
    const params = {
      'menu': this.fileType,
      'id': '0',
      'created_reference_id': row.created_reference_id,
      'status': 'Active'
    };
    this.getApprovedData(params);
  }
  // to reject complete file
  rejectFile(row: any) {
    const params = {
      'menu': this.fileType,
      'id': '0',
      'status': 'Rejected',
      'created_reference_id': row.created_reference_id
    };
    this.getApprovedData(params);
  }
  // ----------------end of code for Modal which is approved or rejected for--------------
  // ------------------------- to display column data in modal and in All Data ----------------------
  getViewCol() {
    this.colList = [];
    console.log(this.columnConfig.columnConfigMasterData);
    this.columnConfig.feederColList.forEach(x => {
            this.colList.push({ 'name': x.display_column_name, 'prop': x.tabe_column, 'datatype': x.datatype, 'isEditable': x.isEditable});
    });
    this.viewColumnAll = this.colList;
    this.viewColumn = this.viewColumnAll;
    console.log('viewColumn', this.viewColumnAll);
  }
  // ------------------------- to display column data in modal and in All Data ----------------------
  // -------------code for edit all data section---------------------------
  // indivisual edit
  viewEdit(rowIndex: number, row: any) {
    this.editRow = row;
    this.isEdit[rowIndex] = 'true';
  }

  // multiple Edit
  onMultipleEdit() {

    this.rows = [];
    this.rows = this.selected;
    this.rows = [...this.rows];

    for (let i = 0; i < this.rows.length; i++) {
      this.isEdit[i] = true;
    }
    console.log('edited row', this.rows);
  }

  //to submit edited data
  updateEdit(params: any) {
    this.apiService.post('updatemaster/', params).subscribe((response: any) => {
      this.alert.isvisible = true;
      this.alert.type = 'Edit';
      this.alert.class = 'success';
      this.alert.text = response.data;
      this.setDataInformationCurrent();
      this.selected = [];
      this.allDataActive(0);
    }, (error) => {
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      const error_str: string = error.error.data;
      this.alert.text = error_str;
      this.alert.class = 'danger';
      this.selected = [];
    });
  }

  editSubmit() {
    const params = {
      'file_type': this.fileType,
      'update': this.rows
    };
    this.updateEdit(params);

  }
  // -------------End of code for edit all data section---------------------------

}
