import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-holidaymaster',
  templateUrl: './holidaymaster.component.html',
  styleUrls: ['./holidaymaster.component.scss']
})
export class HolidaymasterComponent implements OnInit {
  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  file_type = 'HOLIDAYMASTER';
  addOperation = 'Add';
  updateOperation = 'update';
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  selected = [];
  selectedids = [];
  previewPage = new Page();
  public uploadData: any;
  uploadForm: FormGroup;
  submitted = false;
  temp = [];
  previewData: any;
  datePipe;
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  diRows: any;
  submitbuttonval:boolean;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;
  public isEdit = [];
  public addField: any = [{}];
  public editRow: any = {};
  public isMultiEdit: boolean;
  public selectedState: string;
  fileData: any = [];
  State:any=[];
  HolidayList:any=[];
  public stateList:any=[];
  public config: any;
  public permissionList: any = [];
  public statusList: Array<string>;
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
    this.statusList = ['All', 'Active', 'Approval Pending', 'History', 'Rejected', 'Uploaded'];
    this.filtersForAllView.record_status = 'Active';
    this.datePipe = new DatePipe('en-US');
    this.config = {
      displayKey:"state", //if objects array passed which key to be displayed defaults to description
      search:true, //true/false for the search functionlity defaults to false,
      height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
      placeholder:'Select', // text to be displayed when no item is selected defaults to Select,
      noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
      searchPlaceholder:'Search', // label thats displayed in search input,
      searchOnKey: 'state' // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
    }
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.currentPageSize = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.dropdownValue = [];
    this.filterDropDownValue = {};
  }


  ngOnInit() {
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    this.setDataInformationCurrent();
    this.dropdownsForState();
    this.getViewCol();
    this.submitbuttonval=true;
    this.allDataActive(0);
    this.isMultiEdit = false;
    this.uploadForm = this.formBuilder.group({
      file_type: [this.file_type]

    });

    this.populateSelectColumn();
  }
  // ----------------------- upload file ------------------------------------
  // convenience getter for easy access to form fields
  get f() {
    return this.uploadForm.controls;
  }
  openTimeSelector() {
    console.log('time picker opened');
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      this.uploadData.time = time;
      console.log('time picker closed');

    });
  }


  dropdownsForState() {
    const params = {
          'file_type': 'HOLIDAYMASTER'
      };
    this.apiService.post('configparam/holiday_master/state_dropdown/', params).subscribe((data: any) => {
      this.State = data ;
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }
  dropdownsForHolidayList(rowIndex:number) {
    const params = {
          'state':this.rows[rowIndex].state
      };
    this.apiService.post('configparam/holiday_master/holiday_name/', params).subscribe((data: any) => {
     
      this.HolidayList[rowIndex] = data ;
   console.log("holiday list");
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }

 InsertDatafn() {
    this.addField.forEach( x => {
      let stateArr: any = [];
      x.state.forEach(y => {
        const str = y.state;
        stateArr.push(str);
      })
      x.state = stateArr;
      x.start_date = this.datePipe.transform(x.start_date, 'yyyy-MM-dd');
      x.end_date = this.datePipe.transform(x.end_date, 'yyyy-MM-dd');
    });
    console.log("state  listy",  this.stateList);
    const params = {
      'operation': this.addOperation,
      "payload": this.addField
  };

  console.log("---------- State List",this.addField.stateList);
    this.apiService.uploadFile('configparam/holiday_master/', params).subscribe(event => { // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        // this.alert.class="danger"
        this.alert.text = ' ' + response.body.status_desc;
        this.alert.class = 'success';
        $('#m_modal_6').modal('hide');
        this.setDataInformationCurrent();
      }
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#m_modal_6').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
  this.resetFormdata();
  }
  resetFormdata(){
  this.addField=[{}];


  }
  // ------------------------------ upload end -----------------------------------
  // ----------view all data ----------------

  viewColumns: Object = {};
  rows: any = [];
  tempRows: any = [];
  filtersForAllView: any = {
    'record_status': 'Active'
  };

  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    });
  }

  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    });
    // }
    console.log('col change', this.selectedColumn, this.viewColumn);
  }



  viewSearch(event) {
    console.log('updateing');
    const val = event.target.value.toLowerCase();
    const temp = this.tempRows.filter(function(d) {
      const search_atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
      const search_bank_name = d.bank_name.toLowerCase().indexOf(val) !== -1;
      const search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
      return search_atm_id || search_bank_name || search_project_id || !val;
    });

    this.rows = temp;
  }


  updateViewAll() {
    console.log('updarte called');
    console.log('filter parameter', this.filtersForAllView);
    this.setPage({
      offset: 0
    });
    $('#modalfilter').modal('hide');
  }

  setPage(pageInfo) {
    // console.log("page clicked");
    console.log('page clicked', pageInfo);
    this.page.pageNumber = pageInfo.offset;
    this.apiService.post('viewmaster/?page=' + (this.page.pageNumber + 1) + '&' + 'page_size=' + this.page.size, {
      'file_type': this.file_type,
      'filters': this.filtersForAllView
    }).subscribe(data => {
      console.log('-------get all');
      console.log(data); // handle event here
      const response: any = data;
      this.rows = response.results;
      this.tempRows = response.results;
      this.page.totalElements = response.count;
    });
  }
  // ---------end of  view all data ----------------------

  // --------------- drop down value ---------------------


  // --------------- end of drop down value -----------------


  clicked(data) {
    console.log(data);
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }
 // ------------------------- code for data information ----------------------
 setDataInformationCurrent() {
  const params = {
    'menu': this.file_type,
    'record_status': ['Approval Pending']
  };

  this.getDataInfo(params);
}
setDataInformationPrevious() {
  const params = {
    'menu': this.file_type,
    'record_status': ['Active', 'Rejected']
  };

  this.getDataInfo(params);
}
getDataInfo(params: any) {
  this.apiService.post('getmasterdata/', params).subscribe((data: any) => {
    const tempRow = data.data;
    this.diRows = tempRow;
    this.diRows = [...this.diRows];
    console.log('Active data', this.diRows);
  }, (error) => {
    console.log('Data Info Error', error.error.status_text);
  });
}

// -------------------------End of code for data information ----------------------

//------------------------code for view file in modal for current and previous tab----------------------------------
//common section
viewMasterData(params: any, offSet: number) {
  this.apiService.post('viewmaster/?page=' + (this.currentPage + offSet) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
    this.rows = response.results;
    this.rows = [...this.rows];
    this.rows.forEach( (x,index) => {
      this.isEdit[index] = false;
      this.colList.filter( x => x.datatype == 'date').forEach( y => {
        if(x[y.prop] != null){
          x[y.prop] = new Date(x[y.prop]);
    }
      })
    });
    console.log('-----------common', this.rows);
    this.page.totalElements = response.count;
    // // this.tempPageCMIS = this.page;
    this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
  }, (error) => {
    // console.log('View Error', error.error);
  });
  this.clearSelectArrayfn();
}
 clearSelectArrayfn(){
  console.log("Inside select arrey clr called")
  this.selectedids=[];
  this.selected=[];
}
getFileData(offSet: number, status: string) {
  const params = {
    'menu': this.file_type,
    'filter_data': {
      'created_reference_id': this.previewData.created_reference_id,
      'record_status': status
    }
  };
  this.viewMasterData(params, offSet);
}
//end of common section
//to open view file in current tab
viewFile(rowData: any) {
  if (rowData != null) {
    this.previewData = rowData;
  }
  this.getFileData(0, rowData.record_status);
  this.modalRef = this.modalService.show(this.previewModalcurrent, Object.assign({}, { class: 'preview-modal modal-lg', backdrop : true,
      keyboard : false, ignoreBackdropClick: true }));
}
//to open view file in previous tab
viewFilePrevious(rowData: any) {
  if (rowData != null) {
    this.previewData = rowData;
  }
  this.getFileData(0, rowData.record_status);
  this.modalRef = this.modalService.show(this.previewModalprevious, Object.assign({}, { class: 'preview-modal modal-lg', backdrop : true,
      keyboard : false, ignoreBackdropClick: true }));
}

//to Display All data of active status
resetFilter() {
  this.filtersForAllView = {};
}
allDataActive(offSet: number) {
  let params: any;
  for (let key in this.filtersForAllView) {
    if (this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All') {
      delete this.filtersForAllView[key];
    }
  }
  params = {
    'menu': this.file_type,
      'filter_data': this.filtersForAllView
  };
  this.viewMasterData(params, offSet);
}
//---------------------------------------code for view file in modal for current and previous tab- --------------------------------------
//------------------code for Modal which is approved or rejected indivisual file -----------------------------
onSelect({ selected },reqfrom) {
  this.isMultiEdit = true;
  console.log('Select Event', this.selected);
  this.selected.splice(0, this.selected.length);
  this.selected.push(...selected);
  this.dropdownsForState();
  if(this.selected.length<1 && reqfrom=='Active'){
    this.submitbuttonval=true;
    this.allDataActive(0);
  }

}
getApprovedData(params: any) {
  this.apiService.post('ApproveRejectAll/', params).subscribe((response: any) => {
    this.allDataActive(0);
    this.setDataInformationCurrent();
    this.alert.isvisible = true;
    this.alert.type = 'Success';
    this.alert.class = 'success';
    this.alert.text = response.status_desc;
    console.log('-----------', this.rows);
  }, (error:any) => {
    this.alert.isvisible = true;
    this.alert.type = 'Failure';
    this.alert.class = 'danger';
    if (error.error != null){
      this.alert.text = error.error.status_desc;
    } else {
      this.alert.text = error.statusText;
    }
  });
}
//to approve indivisual row
approveIndivisualFile() {
  for (let i = 0; i <= this.selected.length - 1; i++) {
    this.selectedids.push(this.selected[i].id);
  }
  const params = {
    'menu': this.file_type,
    'id': this.selectedids.toString(),
    'updated_record_status': 'Active',
    'created_reference_id': this.previewData.created_reference_id,
    "payload_list":this.rows
  };
  this.getApprovedData(params);
}
//to reject indivisual row
rejectIndivisualFile() {
  for (let i = 0; i <= this.selected.length - 1; i++) {
    this.selectedids.push(this.selected[i].id);
  }
  const params = {
    'menu': this.file_type,
    'id': this.selectedids.toString(),
    'updated_record_status': 'Rejected',
    'created_reference_id': this.previewData.created_reference_id,
    "payload_list":this.rows
  };
  this.getApprovedData(params);
}
//to approve complete file
approveFile(row: any) {
  const params = {
    'menu': this.file_type,
    'created_reference_id': row.created_reference_id,
    'updated_record_status': 'Active'

  };
  this.getApprovedData(params);
}
//to reject complete file
rejectFile(row: any) {
  const params = {
    'menu': this.file_type,
    'updated_record_status': 'Rejected',
    'created_reference_id': row.created_reference_id

  };
  this.getApprovedData(params);
}
//----------------end of code for Modal which is approved or rejected for--------------
// ------------------------- to display column data in modal and in All Data ----------------------
getViewCol() {
  this.colList = [];
  console.log(this.columnConfig.ColumnConfigHolidays);
  this.columnConfig.ColumnConfigHolidays.forEach(x => {
      this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column ,"datatype":x.datatype });
    console.log("xxxxxxxxxxx",x);
  });
  this.viewColumnAll = this.colList;
  console.log('viewColumn', this.viewColumnAll);
}
// ------------------------- to display column data in modal and in All Data ----------------------
//-------------code for edit all data section---------------------------
//indivisual edit
viewEdit(rowIndex: number, row: any) {
  this.editRow = row;
  this.isEdit[rowIndex] = 'true';
}

//multiple Edit
onMultipleEdit() {

  if(this.selected.length>0){
    this.submitbuttonval=false;
    this.rows = this.selected;
    this.rows = [...this.rows];

    for (let i = 0; i < this.rows.length; i++) {
      this.isEdit[i] = true;
      this.dropdownsForHolidayList(i);
        }
    console.log('edited row', this.rows);
  }
}

//to submit edited data
updateEdit(params: any) {
  this.apiService.post('configparam/holiday_master/', params).subscribe((response: any) => {
    this.alert.isvisible = true;
    this.alert.type = 'Edit';
    this.alert.class = 'success';
    this.alert.text = response.data;
    this.setDataInformationCurrent();
    this.selected=[];
    this.allDataActive(0);
  }, (error) => {
    this.alert.isvisible = true;
    this.alert.type = 'Error';
    const error_str: string = error.error.data;
    this.alert.text = error_str;
    this.alert.class = 'danger';
    this.selected=[];
  });
}

editSubmit() {
  this.submitbuttonval=true;
  this.rows.forEach( x => {
    x.start_date = this.datePipe.transform(x.start_date, 'yyyy-MM-dd');
    x.end_date = this.datePipe.transform(x.end_date, 'yyyy-MM-dd');
  });
  const params = {
    "operation" : this.updateOperation,
    "payload" : this.rows
  };
  this.updateEdit(params);

}
AddFiled() {
  console.log("insode addddddddddd");
  this.addField.push({});
  }
  RemoveFiled(index) {
  this.addField.splice(index,1);
  }

//-------------End of code for edit all data section---------------------------
}
