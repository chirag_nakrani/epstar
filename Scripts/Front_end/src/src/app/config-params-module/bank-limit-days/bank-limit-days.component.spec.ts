import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankLimitDaysComponent } from './bank-limit-days.component';

describe('BankLimitDaysComponent', () => {
  let component: BankLimitDaysComponent;
  let fixture: ComponentFixture<BankLimitDaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankLimitDaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankLimitDaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
