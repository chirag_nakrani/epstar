import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EodactivityComponent } from './eodactivity.component';

describe('EodactivityComponent', () => {
  let component: EodactivityComponent;
  let fixture: ComponentFixture<EodactivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EodactivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EodactivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
