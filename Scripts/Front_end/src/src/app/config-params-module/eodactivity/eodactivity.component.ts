import { Component, OnInit , TemplateRef } from '@angular/core';
import {ViewChild, ElementRef} from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Page} from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';

declare var $: any;

@Component({
  selector: 'app-eodactivity',
  templateUrl: './eodactivity.component.html',
  styleUrls: ['./eodactivity.component.scss']
})
export class EodactivityComponent implements OnInit {

  @ViewChild('currentfileViewModal') previewModalcurrent: ElementRef;
  @ViewChild('previousfileViewModal') previewModalprevious: ElementRef;
  public uploadData: any = {};
  fileType = 'EOD_ACTIVITY';
  public cra_name = '';
  public record_status = 'Active';
  public currentPageSize: number;
  public currentPage = 1;
  alert: any;
  datePipe: DatePipe;
  uploadForm: FormGroup;
  activeTab: string;
  submitted = false;
  selected = [];
  selectedids = [];
  temp = [];
  public isEdit = [];
  dropdownValue: any;
  filterDropDownValue: any;
  previewColList: any;
  public colList: any;
  viewColumnAll: any={};
  fileName: string;
  private pagesizeList: any = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  modalRef: BsModalRef;
  page: Page = new Page();
  previewPage: Page = new Page();
  diRows: any;
  previewData:any;
  diColumn: any;
  filterData: any = [];
  currentFileIndex: number;
  tempRowsCMIS: any = [];
  tempRowsVMIS: any = [];
  tempPageCMIS: Page = new Page();
  tempPageVMIS: Page = new Page();
  public viewColCMIS: any = [];
  public viewColVMIS: any = [];
  projectDDList: any = [];
  rows: any = [];
  prevRows: any = [];
  currentFileData: any = {};
  public editRow:any={};
  public isMultiEdit:boolean;
  tempRows: any = [];
  public permissionList: any = [];
  public pageLimit = 20;
  public filtersForAllView: any ={};
  public statusList = ['All', 'Active', 'History', 'Approval Pending'];
  public bankFilterDropDown: any;

  public filterDate: Date = new Date();
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig, private sessionStorageService: SessionStorageService) {
    //this.diColumn = this.columnNameProviderService.dataInformationColumns;
    this.datePipe = new DatePipe('en-US');
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.currentPageSize = 20;

   }
  ngOnInit() { //Called on page initialization
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    // this.getDropdowns();
    // this.setPageData(0);
    this.filtersForAllView.record_status = 'Active';
    this.filtersForAllView.datafor_date_time = this.filterDate;
    this.populateDropDown();
    this.setDataInformationCurrent();
    this.getViewCol();
    this.allDataActive(0);
    this.isMultiEdit=false;
    this.uploadForm = this.formBuilder.group({
      project_id: ['', Validators.required]
  });
  this.updateViewAll();
  }

  // ----------start of other code  ----------------
    // convenience getter for easy access to form fields
    get f() {
        return this.uploadForm.controls;
    }

    resetFilter() {
      this.filtersForAllView = {};
      this.filtersForAllView.record_status = 'Active';
      this.filtersForAllView.datafor_date_time = new Date();
      this.filterDate = null;
    }

    updateViewAll() {
      console.log('updarte called');
      console.log('filter parameter', this.filtersForAllView);
      this.allDataActive(0);
      $('#modalfilter').modal('hide');
    }

    openTimeSelector() {
      console.log("time picker opened")
      const amazingTimePicker = this.atp.open();
      amazingTimePicker.afterClose().subscribe(time => {
        this.uploadData.time = time;
        console.log("time picker closed")

      });
    }
    onFileChanged(event) {

      //const file = event.target.files[0]
      this.uploadData.file = event.target.files[0];
    }
// ------------ code for select column --------------
  viewColumns: Object = {};
  viewColumn: any = (JSON.parse(JSON.stringify(this.viewColumnAll)));
  selectedColumn: any = [];
  columnChange: any = [];

  populateSelectColumn() {
    this.viewColumnAll.filter((col) => {
      this.selectedColumn.push(col.prop);
    })
  }
  columnChangefn() {
    this.viewColumn = this.viewColumnAll.filter((col) => {
      return this.selectedColumn.includes(col.prop);
    })
    // }
    console.log("col change", this.selectedColumn, this.viewColumn);
  }

  uploadDatafn() {
    // stop here if form is invalid
    if (this.uploadData.invalid) {
      return;
    }
    // console.log("upload clicked----------", this.uploadData);
    // this.previewColList = this.columnConfig.columnConfig.filter(x => x.datafor == this.fileType && x.bankcode == this.uploadData.bank_code.bank_id);
    this.submitted = true;
    const fileData = new FormData();
    //uploadData.append('myFile', this.selectedFile, this.selectedFile.name);
    fileData.append('file', this.uploadData.file, this.uploadData.file.name);
    fileData.append('file_type', this.fileType);
    // fileData.append('upload_datatime', date);
    // console.log("upload clicked formdata", fileData);
    this.apiService.uploadFile('configfileupload/', fileData).subscribe(event => {
      console.log(event); // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        // this.alert.class="danger"
        this.alert.text = ' ' + response.body.status_desc;
        this.alert.class = 'success';
        $('#m_modal_6').modal('hide');
        this.setDataInformationCurrent();

      }
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      $('#m_modal_6').modal('hide');

      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));

    this.clearUploadFormData();

  }
  clearUploadFormData() {
    // this.uploadForm.reset();
    this.uploadData.file = [];
    this.uploadData={};

  }
//------------------- end of code for select column ----------
// -----------start of code for search --------------
  viewSearch(event) {
    console.log("updateing");
    const val = event.target.value.toLowerCase();
    const temp = this.tempRows.filter(function (d) {
      var search_atm_id = d.atm_id.toLowerCase().indexOf(val) !== -1;
      var search_record_status = d.record_status.toLowerCase().indexOf(val) !== -1;
      var search_project_id = d.project_id.toLowerCase().indexOf(val) !== -1;
      return search_atm_id || search_record_status || search_project_id || !val;
    });

    this.rows = temp;
  }
  // ----------------end of code for search ---------------------
  // --------------- end of drop down value -----------------
  clicked(data) {
    console.log(data)
  }

  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  //----------------- end of other code -------------
// ------------------------- code for data information ----------------------
 setDataInformationCurrent() {
  const params = {
    'menu': this.fileType,
    'record_status': ['Approval Pending']
  };

  this.getDataInfo(params);
}
setDataInformationPrevious() {
  const params = {
    'menu': this.fileType,
    'record_status': ['Active', 'Rejected']
  };

  this.getDataInfo(params);
}
getDataInfo(params: any) {
  this.apiService.post('getmasterdata/', params).subscribe((data: any) => {
    const tempRow = data.data;
    this.diRows = tempRow;
    this.diRows = [...this.diRows];
     console.log("Active data", this.diRows);
  }, (error) => {
    console.log('Data Info Error', error.error.status_text);
  });
}

// -------------------------End of code for data information ----------------------
//------------------------code for view file in modal for current and previous tab----------------------------------
//common section
viewMasterData(params:any,offSet:number){
  this.apiService.post('configparam/eod_list/', params).subscribe((response: any) => {

     this.rows = response;
     this.rows = [...this.rows];
     for(let i=0; i<this.rows.length; i++){
       this.isEdit[i] = false;
     }
     this.tempRows=[...this.rows]
     console.log("-----------common",this.rows)
     this.page.totalElements = response.count;
     // // this.tempPageCMIS = this.page;
     this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
   }, (error) => {
     // console.log('View Error', error.error);
   });
   this.clearSelectArrayfn();
  }
   clearSelectArrayfn(){
    console.log("Inside select arrey clr called")
    this.selectedids=[];
    this.selected=[];
  }
getFileData(offSet:number) {
  const params = {
    'menu' :this.fileType,
    "filter_data" : {
     "created_reference_id" : this.previewData.created_reference_id,
    }
  };
  this.viewMasterData(params,offSet);
}
//end of common section
//to open view file in current tab
viewFile(rowData: any){
  if(rowData !=null){
    this.previewData=rowData;
  }
  this.modalRef = this.modalService.show(this.previewModalcurrent, Object.assign({}, { class: 'preview-modal modal-lg', backdrop : true,
      keyboard : false, ignoreBackdropClick: true}));
  this.getFileData(0);
}
//to open view file in previous tab
viewFilePrevious(rowData: any){
  if(rowData !=null){
    this.previewData=rowData;
  }
  this.modalRef = this.modalService.show(this.previewModalprevious, Object.assign({}, { class: 'preview-modal modal-lg', backdrop : true,
      keyboard : false, ignoreBackdropClick: true}));
  this.getFileData(0);
}


populateDropDown() {
  // --------- for upload dropdown----------------------
  this.apiService.post('bankinfo/', {
    'file_type': 'CBR',
    'routine_info': 'ROUTINE_DATA_INFO'
  }).subscribe(data => {
    console.log('------- dropdown value ');
    const response: any = data;

    console.log('-------dropdown', response.data);
    this.dropdownValue = response.data;
    console.log(this.dropdownValue); // handle event here
    this.getAllBankList();
  }, (error) => {
    console.log('error in dropdown value', error);

  });
  // --------------- end of upload dropdown----------------
}

getAllBankList() {
  this.bankFilterDropDown = [];
  this.dropdownValue.forEach( x => {
    x.bank.forEach( y => {
      this.bankFilterDropDown.push(y);
    });
  });
}
 //to Display All data of active status

 allDataActive(offSet:number){
  for (let key in this.filtersForAllView) {
    if (this.filtersForAllView[key] == null || this.filtersForAllView[key] == '' || this.filtersForAllView[key] == 'All') {
      delete this.filtersForAllView[key];
    }
    if (key == 'datafor_date_time') {
      this.filtersForAllView[key] = this.datePipe.transform(this.filterDate, 'yyyy-MM-dd');
    }
  }

  const params = {
      'menu': this.fileType,
      'filter_data': this.filtersForAllView
    };

  this.viewMasterData(params,offSet);
}
//---------------------------------------code for view file in modal for current and previous tab- --------------------------------------
//------------------code for Modal which is approved or rejected indivisual file -----------------------------
onSelect({ selected }) {
  this.isMultiEdit = true;
  console.log('Select Event', this.selected);
  this.selected.splice(0, this.selected.length);
  this.selected.push(...selected);

}
getApprovedData(params: any) {
  this.apiService.post('ApproveRejectAll/', params).subscribe((response: any) => {
    this.allDataActive(0);
    this.setDataInformationCurrent();
    this.alert.isvisible = true;
    this.alert.type = 'Success';
    this.alert.class = 'success';
    this.alert.text = response.status_desc;
    console.log('-----------', this.rows);
    this.selected = [];
    this.selectedids = [];
  }, (error:any) => {
    this.selected = [];
    this.selectedids = [];
    this.alert.isvisible = true;
    this.alert.type = 'Failure';
    this.alert.class = 'danger';
    if (error.error != null){
      this.alert.text = error.error.status_desc;
    } else {
      this.alert.text = error.statusText;
    }
  });
}
//to approve indivisual row
approveIndivisualFile() {
  for (let i = 0; i <= this.selected.length - 1; i++) {
    this.selectedids.push(this.selected[i].id);
  }
  const params = {
    'menu': this.fileType,
    'id': this.selectedids.toString(),
    'status': 'Active',
    'created_reference_id': this.previewData.created_reference_id
  };
  this.getApprovedData(params);
}
//to reject indivisual row
rejectIndivisualFile() {
  for (let i = 0; i <= this.selected.length - 1; i++) {
    this.selectedids.push(this.selected[i].id);
  }
  const params = {
    'menu': this.fileType,
    'id': this.selectedids.toString(),
    'status': 'Rejected',
    'created_reference_id': this.previewData.created_reference_id
  };
  this.getApprovedData(params);
}          //to approve complete file
approveFile(row: any) {
  const params = {
    'menu': this.fileType,
    'id': '0',
    'created_reference_id': row.created_reference_id,
    'status': 'Active'
  };
  this.getApprovedData(params);
  this.setDataInformationCurrent();
  this.allDataActive(0);

}
//to reject complete file
rejectFile(row: any) {
  const params = {
    'menu': this.fileType,
    'id': '0',
    'status': 'Rejected',
    'created_reference_id': row.created_reference_id
  };
  this.getApprovedData(params);
  this.setDataInformationCurrent();
  this.allDataActive(0);

}
//----------------end of code for Modal which is approved or rejected for--------------
// ------------------------- to display column data in modal and in All Data ----------------------
getViewCol() {
  this.colList = [];
  console.log(this.columnConfig.ColumnConfigEodActivity);
  this.columnConfig.ColumnConfigEodActivity.forEach(x => {
      this.colList.push({'name': x.display_column_name, 'prop': x.table_column, 'datatype': x.datatype});

  });
  this.viewColumnAll = this.colList;
console.log("viewColumn",this.viewColumnAll)
}
// ------------------------- to display column data in modal and in All Data ----------------------
//-------------code for edit all data section---------------------------
//indivisual edit
viewEdit(rowIndex:number,row:any){

  this.editRow=row;
  this.isEdit[rowIndex]="true"
}

//multiple Edit
onMultipleEdit(){

this.rows=this.selected;
this.rows = [...this.rows];

for(let i=0; i<this.rows.length; i++){
  this.isEdit[i] = true;
}
console.log("edited row",this.rows);
}

//to submit edited data
updateEdit(params:any){
  this.apiService.post('updatemaster/' , params).subscribe((response: any) => {
    this.alert.isvisible = true;
    this.alert.type = 'Edit';
    this.alert.class = 'success';
    this.alert.text = response.data;
    this.setDataInformationCurrent();
    this.allDataActive(0);
  }, (error) => {
       this.alert.isvisible = true;
          this.alert.type = 'Error';
          const error_str: string = error.error.data;
          this.alert.text =  error_str;
          this.alert.class = 'danger';
  });
}

editSubmit(){
  const params = {
    'file_type' : this.fileType,
    'update': this.rows
  };
  this.updateEdit(params);

}
//-------------End of code for edit all data section---------------------------
}
