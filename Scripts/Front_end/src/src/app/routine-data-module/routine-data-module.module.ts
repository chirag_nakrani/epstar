import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { BankcashbalanceComponent } from './bankcashbalance/bankcashbalance.component';
import { CashdispenseComponent } from './cashdispense/cashdispense.component';
import { ImsticketsComponent } from './imstickets/imstickets.component';
import { VaultcashComponent } from './vaultcash/vaultcash.component';

import { CommonImportsModule } from '../common/common-imports.module';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';

const routes: Routes = [
  { path: 'bankcashbalance', component: BankcashbalanceComponent,
     canActivate: [AuthCheckService] },
  { path: 'cashdispense', component: CashdispenseComponent,
     canActivate: [AuthCheckService] },
  { path: 'imstickets', component: ImsticketsComponent,
     canActivate: [AuthCheckService] },
  { path: 'vaultcash', component: VaultcashComponent,
     canActivate: [AuthCheckService] }
];

@NgModule({
  declarations: [
    BankcashbalanceComponent,
    CashdispenseComponent,
    ImsticketsComponent,
    VaultcashComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class RoutineDataModuleModule { }
