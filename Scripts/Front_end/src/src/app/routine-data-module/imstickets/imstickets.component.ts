import { Component, OnInit , TemplateRef } from '@angular/core';
import {ViewChild, ElementRef} from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {DatePipe} from '@angular/common';
import {Page} from '../../common/models/page';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
import { SessionStorageService } from 'ngx-store';
declare var $: any;

@Component({
  selector: 'app-imstickets',
  templateUrl: './imstickets.component.html',
  styleUrls: ['./imstickets.component.scss']
})

export class ImsticketsComponent implements OnInit {
  @ViewChild('fileInput')
  myInputVariable: ElementRef;
  public uploadData: any = {};
  fileType = 'ims';
  public record_status = 'Active';
  public currentPageSize: number;
  public currentPage = 1;
  alert: any;
  datePipe: DatePipe;
  selectedColumn: any = [];
  columnChange: any = [];
  uploadForm: FormGroup;
  submitted = false;
  temp = [];
  viewColumnAll: any;
  public colList: any = [];
  fileName: string;
  private pagesizeList: any = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
  modalRef: BsModalRef;
  page: Page = new Page();
  previewPage: Page = new Page();
  filterData: FormGroup;
  currentFileIndex: number;
  tempPage: Page = new Page();
  rows: any = [];
  tempRow: any = [];
  viewColumn: any = [];
  permissionList: any = [];
  constructor(private atp: AmazingTimePickerService,
              private apiService: ApiService,
              private formBuilder: FormBuilder,
              private columnConfig: ColumnConfig,
              private sessionStorageService: SessionStorageService
              ) {
    this.datePipe = new DatePipe('en-US');
    this.uploadData = {};
    this.alert = {};
    this.alert.isvisible = false;
    this.page.pageNumber = 0;
    this.page.size = 20;
    this.previewPage.pageNumber = 0;
    this.previewPage.size = 20;
    this.currentPageSize = 20;
  }

  ngOnInit() {
    if(this.sessionStorageService.get('permissionList') != null)
    this.permissionList = this.sessionStorageService.get('permissionList');
    this.getViewCol();
    this.setPageData(0);
    this.uploadData.date = new Date();
    this.uploadData.time = '12:00';
    this.alert.isvisible = false;
    this.uploadForm = this.formBuilder.group({
      'file_type' : [ this.fileType],
      'file' : '',
      'date' : ''
    });
    this.filterData = this.formBuilder.group({
      'atmid' : [''],
      'bankname' : [''],
      'fault' : [''],
      'ticket_status' : [''],
      'open_date' : [null]
    });
  }

  getViewCol() {

    this.colList = [];

    this.columnConfig.columnConfig.forEach(x => {
      if (x.datafor === 'IMS') {
        this.colList.push({'name': x.display_column_name, 'prop': x.table_column,'datatype': x.datatype});
      }

    });

    this.viewColumnAll = this.colList;

  }

  uploadDatafn() {

    const fileData = new FormData();
    fileData.append('file', this.uploadData.file, this.uploadData.file.name);
    fileData.append('date', this.datePipe.transform(this.uploadData.date,'yyyy-MM-dd'));
    fileData.append('file_type', this.fileType);
    this.apiService.uploadFile('upload/', fileData).subscribe((event: any) => {
      this.uploadData={};
        if (event && event.body) {
          console.log('resonse', event.body);
          this.alert.isvisible = true;
          this.alert.type = 'Upload';
          this.alert.class = 'success';
          this.alert.text = ' ' + event.body.status_text;
          this.setPageData(0);
          $('#m_modal_6').modal('hide');
        }
      }, error => {

          console.log('error', error);
          $('#m_modal_6').modal('hide');
          this.alert.isvisible = true;
          this.alert.type = 'Error';
          const error_str: string = error.error['status_text'];
          this.alert.text =  error_str;
          this.alert.class = 'danger';

      });

    }

    onFileChanged(event) {
      this.uploadData.file = event.target.files[0];
      this.fileName = this.uploadData.file.name;
      console.log("******************",this.myInputVariable.nativeElement.files);
      this.myInputVariable.nativeElement.value = "";
      console.log(this.myInputVariable.nativeElement.files);
    }

    openTimeSelector() {
      console.log('time picker opened');
      const amazingTimePicker = this.atp.open();
      amazingTimePicker.afterClose().subscribe(time => {
        this.uploadData.time = time;
        console.log('time picker closed');

      });
    }

    calculateTotalPage(rowcount, rowsize) {
        return Math.ceil(rowcount / rowsize);
    }

    setPageData(pageNumber: number) {
        const params = {
          'file_type' : this.fileType,
          'filters': {
            'record_status' : this.record_status ,
          }
        };
        this.page.pageNumber = pageNumber;
        this.apiService.post('view/?page=' + (this.currentPage + pageNumber) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
          this.rows = response.results;
          this.rows = [...this.rows];
          this.tempRow = response.results;
          this.page.totalElements = response.count;
          this.tempPage = this.page;
          this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
        }, (error) => {
          console.log('View Error', error.error);
        });
      }

      filterPageData(pageNumber: any) {

        this.filterData.value.open_date = this.datePipe.transform(this.filterData.value.open_date, 'yyyy-MM-dd');
        let params = {
          'file_type' : this.fileType,
          'filters': this.filterData.value
        };

        params.filters.record_status = 'active';

        this.page.pageNumber = pageNumber;
        this.currentPage = pageNumber + 1;
        this.apiService.post('view/?page=' + (this.currentPage) + '&page_size=' + this.currentPageSize, params).subscribe((response: any) => {
          this.rows = response.results;
          this.rows = [...this.rows];
          this.page.totalElements = response.count;
          this.tempPage = this.page;
          this.page.totalPages = this.calculateTotalPage(response.count, this.page.size);
        }, (error) => {
          console.log('View Error', error.error);
        });
      }

      viewSearch(event) {
            console.log('updateing');
              const val = event.target.value.toLowerCase();

              const temp = this.tempRow.filter(function(d) {
          const bankname = d.bankname.toLowerCase().indexOf(val) !== -1;
          const atmid = d.atmid.toLowerCase().indexOf(val) !== -1;
          const ticketno = d.ticketno.toLowerCase().indexOf(val) !== -1;
          return bankname || atmid || ticketno || !val;
                 });

              this.rows = temp;
      }
    //----------- end of serch function ---------

      populateSelectColumn() {
        this.viewColumnAll.filter( (col) => {
          this.selectedColumn.push(col.prop);
        });
      }

      columnChangefn() {
      this.viewColumn = this.viewColumnAll.filter( (col) => {
        return this.selectedColumn.includes(col.prop);
      });
      // }
      console.log('col change', this.selectedColumn, this.viewColumn);
      }

}
