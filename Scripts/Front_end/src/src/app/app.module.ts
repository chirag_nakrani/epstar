// Imports Common Packages
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { WebStorageModule } from 'ngx-store';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule, ProgressbarModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
// Imports Common Components

import { AppComponent } from './app.component';
import { ExcelExportService } from './common/commonComponents/excel-export-component/excel-export.service';
import { CommonModuleModule } from './common/commonComponents/common-module.module';

// Import Common Providers

import { ColumnConfig } from './common/shared/columnConfig';
import { ColumnNameProviderService } from './cra-module/crac3rreport/column-name-provider.service';

// Import Services

import { LoderService } from './common/commonServices/loder.service';
import { LoadingInterceptor } from './common/interceptorServices/http-interceptor.service';





@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModuleModule,
    BrowserModule,
    WebStorageModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    NgxSpinnerModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    ProgressbarModule.forRoot()
       ],
  providers: [
    ColumnConfig,
    ColumnNameProviderService,
    LoderService,
    ExcelExportService,
    { provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
