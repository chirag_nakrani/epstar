import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../common/commonServices/apiservice.service';

@Component({
  selector: 'app-workflow',
  templateUrl: './workflow.component.html',
  styleUrls: ['./workflow.component.scss']
})
export class WorkflowComponent implements OnInit {
  private catDDList: any = [];
  private readOperation = 'R';
  private addOperation = 'A';
  private deleteOperation = 'D';
  public UserList: any = [];
  public userList: any = [];
  public groupList: any = [];
  public addForm: any = [];
  public alert: any = {};
  public rows: any = [];
  public catList: any = [];
  public subCatList: any = [[]];
  private userRoleMapping: any;
  public userDisabled: any = [];

  constructor( private apiService: ApiService) { }

  ngOnInit() {
    this.getUserRoleMapping();
    this.getCategoy();
    this.getUsers();
    this.getGroup();
    this.readWorkflow();
    this.addField();
  }

  getCategoy() {
    this.apiService.post('ticket/categorymenus/', {}).subscribe((data: any) => {
      this.catDDList = data;
      this.catDDList.forEach( x => {
        if (this.catList.indexOf(x.category) == -1) {
          this.catList.push(x.category);
        }
      });
    });
  }

  getSubcat(index: number, category: string) {
    this.subCatList[index] = [];
    this.catDDList.filter( x => x.category == category).forEach( x=> {
      this.subCatList[index].push(x.sub_category);
    })
  }

  getUsers() {
    this.apiService.post('users/list/', {}).subscribe((data: any) => {
      this.UserList = data ;
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }

  getGroup() {
    this.apiService.post('groups/list/', {}).subscribe(
      (data: any) => {
        this.groupList = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  // getCategory( category_id: any ) {
  //   return this.catDDList.filter( x => x.category_id == category_id)[0].category;
  // }
  //
  // getSubCategory( category_id: any ) {
  //   return this.catDDList.filter( x => x.category_id == category_id)[0].sub_category;
  // }

  addWorkflow() {

    this.addForm.forEach( x => {
        x.status = 'Open';
    });

    this.workFlow(this.addOperation, this.addForm);

  }

  removeWorkflow(row: any) {
    this.workFlow(this.deleteOperation, [row]);
  }

  readWorkflow() {
    const params = {
      'operation_type': this.readOperation,
      'payload': {
        'filters': {

        }
      }
    };
    this.apiService.post('workflow/assigncategory/', params).subscribe(
      (data: any) => {
        this.rows = data;
        this.rows.forEach( x => {
          x.category = this.catDDList.filter( y => y.category_id == x.category_id)[0].category;
          x.sub_category = this.catDDList.filter( y => y.category_id == x.category_id)[0].sub_category;
          x.user_name = this.UserList.filter( y => y.id == x.assigned_to_user)[0].user_name;
          x.escalationLevel1 = this.UserList.filter( y => y.id == x.escalation_level1)[0].user_name;
          x.escalationLevel2 = this.UserList.filter( y => y.id == x.escalation_level2)[0].user_name;
          x.escalationLevel3 = this.UserList.filter( y => y.id == x.escalation_level3)[0].user_name;
          x.group_name = this.groupList.filter( y => y.id == x.assigned_to_group)[0].group_name;
        });
        this.rows = [...this.rows];
      },
      (error: any) => {
        console.log('Error', error);
      }
    );
  }

  workFlow(operation: any, payload: any) {
    const params = {
      'operation_type': operation,
      'payload': payload
    };

    this.apiService.post('workflow/assigncategory/', params).subscribe(
      (data: any) => {
        this.readWorkflow();
        this.resetWorkflow();
      },
      (error: any) => {

      }
    );
  }

  getUserRoleMapping() {
    const params = {
        'operation_type': 'R',
        "payload": {
          "users_id":[]
        }
    };
      this.apiService.post('user_group_mapping/' , params).subscribe((response: any) => {
        this.userRoleMapping = response;
    }, error => {
      console.log("Error",error.error);
    });
  }

  enableUser(i: number) {
    this.userList[i] = [];
    const userGroup = this.userRoleMapping.filter( x => x.group_id == this.addForm[i].assigned_to_group );
    userGroup.forEach( x => {
      this.UserList.forEach( y => {
        if ( y.id == x.user_id)  {
          this.userList[i].push(y);
        }
      })
    });
    this.userDisabled[i] = false;
  }

  addField() {
    this.userList.push([]);
    this.userDisabled.push(true);
    this.addForm.push({});
    this.addForm = [...this.addForm];
  }

  removeField(i: number) {
    this.addForm.splice(i, 1);
  }

  resetWorkflow() {
    this.userDisabled[0] = true;
    this.userList[0] = [];
    this.addForm = [{}];
  }

}
