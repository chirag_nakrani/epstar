import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { CommonImportsModule } from '../common/common-imports.module';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';
import { WorkflowComponent } from './workflow/workflow.component';

const routes: Routes = [
  { path: 'workflow', component: WorkflowComponent,
     canActivate: [AuthCheckService] }
];

@NgModule({
  declarations: [
	   WorkflowComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class WorkflowModuleModule { }
