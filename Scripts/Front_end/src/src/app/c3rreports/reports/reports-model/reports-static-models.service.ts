import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReportsStaticModelsService {
	columnbankList = [
	{
		"reportId": "Main_BOI",
		"bankName": "BOI"
	},
	{
		"reportId": "Main_CanMS",
		"bankName": "CanaraMS"
	},
	{
		"reportId": "Main_CBI",
		"bankName": "CBI"
	},
	{
		"reportId": "Main_Corp_BOB_BOBMS_Can",
		"bankName": "Corp"
	},
	{
		"reportId": "Main_Corp_BOB_BOBMS_Can",
		"bankName": "BOB"
	},
	{
		"reportId": "Main_Corp_BOB_BOBMS_Can",
		"bankName": "BOBMS"
	},
	{
		"reportId": "Main_DN_DNMS",
		"bankName": "Dena"
	},
	{
		"reportId": "Main_DN_DNMS",
		"bankName": "NNMS"
	},
	{
		"reportId": "Main_IDBI",
		"bankName": "IDBI"
	},
	{
		"reportId": "Main_LVB",
		"bankName": "LVB"
	},
	{
		"reportId": "Main1_SB_SBT_UB",
		"bankName": "SBI"
	},
	{
		"reportId": "Main1_SB_SBT_UB",
		"bankName": "SBI-TOM"
	},
	{
		"reportId": "Main1_SB_SBT_UB",
		"bankName": "UBI"
	}
];
}
