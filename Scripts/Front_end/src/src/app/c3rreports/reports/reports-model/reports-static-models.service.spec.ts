import { TestBed } from '@angular/core/testing';

import { ReportsStaticModelsService } from './reports-static-models.service';

describe('ReportsStaticModelsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReportsStaticModelsService = TestBed.get(ReportsStaticModelsService);
    expect(service).toBeTruthy();
  });
});
