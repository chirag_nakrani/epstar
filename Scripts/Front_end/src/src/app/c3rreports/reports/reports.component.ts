import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { ReportsStaticModelsService } from './reports-model/reports-static-models.service';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {

	public report_server_url: any;
	public src: SafeResourceUrl;
	public sourceUrl: string;
	public alert: any = {};
	public bankdropdownList: any = [];
	public report_id: any;
	public bank: any;
	public reportname: any;

	constructor(private apiService: ApiService, private sanitizer: DomSanitizer, private bankList: ReportsStaticModelsService) {
		this.report_server_url = environment.report_server_url; 
	}

	ngOnInit() {
		this.getBank();
	}

	getBank() {
		debugger;
	    this.apiService.post('report/list/', {}).subscribe(
	      (data: any) => {

	        this.bankdropdownList = data;
	      },
	      (error: any) => {
	        console.log(error);
	      }
	    );
	}

   getReportID(report_id: any){
   		debugger;
    	this.report_id = report_id;
    	this.sourceUrl = this.report_server_url + 'Pages/ReportViewer.aspx?%2fEpstar_Report%2f'+ this.report_id +'&rs:Command=Render&rs:embed=True';
		this.src = this.sanitizer.bypassSecurityTrustResourceUrl(this.sourceUrl);
    }
}
