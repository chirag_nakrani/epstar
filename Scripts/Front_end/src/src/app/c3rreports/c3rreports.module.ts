import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';
import { CommonImportsModule } from '../common/common-imports.module';
import { ReportsComponent } from './reports/reports.component';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
//import { ReportsStaticModelsService } from './reports/reports-model/reports-static-models.service';


const routes: Routes = [
	{ path: 'reports', component: ReportsComponent,
	  canActivate: [AuthCheckService] },
	];
@NgModule({
  declarations: [ReportsComponent],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
  //  ReportsStaticModelsService,
    RouterModule.forChild(routes)
  ]
})
export class C3rreportsModule { }
