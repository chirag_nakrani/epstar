import {
  Component,
  OnInit,
  TemplateRef,

} from '@angular/core';
import {
  ViewChild,
  ElementRef
} from '@angular/core';

import {
  AmazingTimePickerService
} from 'amazing-time-picker';
import {
  HttpClient
} from '@angular/common/http';
import {
  ApiService
} from '../../common/commonServices/apiservice.service';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  DatePipe
} from '@angular/common';
import {
  Page
} from '../../common/models/page';
import {
  BsModalService
} from 'ngx-bootstrap/modal';
import {
  BsModalRef
} from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {
  ColumnConfig
} from '../../common/shared/columnConfig';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-userrolemapping',
  templateUrl: './userrolemapping.component.html',
  styleUrls: ['./userrolemapping.component.scss']
})
export class UserrolemappingComponent implements OnInit {
  excelType="User Role Mapping";
  operation = 'A';
  operationdelete="D";
  operationRead="R";
  modalRef: BsModalRef;
  alert: any;
  page = new Page();
  previewPage = new Page();
  submitted = false;
  temp = [];
  datePipe;
  dropdownValue: any;
  public colList: any;
  viewColumnAll: any = {};
  public currentPageSize: number;
  public currentPage = 1;
  public addField: any = [{}];
  public selectedState: string;
  UserList:any=[];
  GroupList:any=[];
  public config: any;
  users_id:any = [];
  rows: any = [];
  newrows:any=[];
  filterData:any=[];
  constructor(private atp: AmazingTimePickerService, private apiService: ApiService, private formBuilder: FormBuilder, private modalService: BsModalService, private columnConfig: ColumnConfig) {
    this.datePipe = new DatePipe('en-US');
    this.config = {
      displayKey:"user_name", //if objects array passed which key to be displayed defaults to description
      search:true, //true/false for the search functionlity defaults to false,
      height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
      placeholder:'Select', // text to be displayed when no item is selected defaults to Select,
      noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
      searchPlaceholder:'Search', // label thats displayed in search input,
      searchOnKey: 'user_name' // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
    }
    this.alert = {};
    this.alert.isvisible = false;
  }
  ngOnInit() {
    this.dropdownsForUser();
    this.dropdownsForGroup();
    this.getViewCol();
    this.AllDatafn();
  }
  // ----------------------- upload file ------------------------------------
  dropdownsForUser() {
    this.apiService.post('users/list/', {}).subscribe((data: any) => {
      this.UserList = data ;
      console.log("user list",this.UserList );
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }
  dropdownsForGroup() {
    this.apiService.post('groups/list/', {}).subscribe((data: any) => {
      this.GroupList = data ;
      console.log("user list",this.GroupList );
    }, (error) => {
       console.log('error in dropdown value', error);
    });
  }
  InsertDatafn() {
    const params = {
      'operation_type': this.operation,
      "payload": this.addField
  };
  console.log("---------- State List",this.addField);
    this.apiService.uploadFile('user_group_mapping/', params).subscribe(event => { // handle event here
      const response: any = event;
      if (response && response.body) {
        console.log('resonse', response.body);
        this.alert.isvisible = true;
        this.alert.type = 'Upload';
        // this.alert.class="danger"
        this.alert.text = ' ' + response.body.status_desc;
        this.alert.class = 'success';
        this.AllDatafn();
        this.resetFormdata();
      }
    }, (error => {
      // alert("Error - "+ error.status+ " "+error.statusText);
      this.alert.isvisible = true;
      this.alert.type = 'Error';
      // this.alert.text="error desc- " +error.status+ " "+error.statusText;
      // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

      this.alert.class = 'danger';
    }));
    this.resetFormdata();
  }
  resetFormdata(){
    this.addField = [{}];
  }


// ------------------------- to display column data in modal and in All Data ----------------------

getViewCol() {
  this.colList = [];
  console.log(this.columnConfig.ColumnConfigUserroleMapping);
  this.columnConfig.ColumnConfigUserroleMapping.forEach(x => {
      this.colList.push({ 'name': x.display_column_name, 'prop': x.table_column});
    console.log("xxxxxxxxxxx",x);
  });
  this.viewColumnAll = this.colList;
  console.log('colListcolList------', this.viewColumnAll);
}
// ------------------------- to display column data in modal and in All Data ----------------------



AddFiled() {
  console.log("insode addddddddddd");

  this.addField.push({});
  }
  RemoveField(index) {
  this.addField.splice(index,1);
  }

//-------------End of code for edit all data section---------------------------


Deleterow(rowIndex: number, row: any) {
  this.newrows=row;
  console.log("this.newrowsthis.newrows",this.newrows);

  const params = {
    'operation_type': this.operationdelete,
    "payload": [{
      'user_id':this.newrows.user_id,
      'group_ids':[this.newrows.group_id]
    }]
};
  this.apiService.uploadFile('user_group_mapping/', params).subscribe(event => { // handle event here
    const response: any = event;
    if (response && response.body) {
      console.log('resonse', response.body);
      this.alert.isvisible = true;
      this.alert.type = 'Upload';
      // this.alert.class="danger"
      this.alert.text = ' ' + response.body.status_desc;
      this.alert.class = 'success';
      this.AllDatafn();
    }
  }, (error => {
    // alert("Error - "+ error.status+ " "+error.statusText);
    this.alert.isvisible = true;
    this.alert.type = 'Error';
    // this.alert.text="error desc- " +error.status+ " "+error.statusText;
    // this.alert.text = 'LEVEL : ' + error.error.level + ' STATUS :  ' + error.error.status_desc;

    this.alert.class = 'danger';
  }));

}


 
AllDatafn() {
  const params = {
    'operation_type': this.operationRead,
    "payload": {
      "users_id":[]
    }
};
if(this.filterData != [] || this.filterData != undefined) {
  this.filterData.forEach( x => {
    params.payload.users_id.push(x.id);
  })
}
    this.apiService.post('user_group_mapping/' , params).subscribe((response: any) => {
      console.log('resonse------', response);
        this.rows = response;
        this.rows = [...this.rows];
        $('#modalfilter').modal('hide');
        console.log("row-------------",   this.rows);
  }, (error => {
    this.alert.isvisible = true;
    this.alert.type = 'Error';
    this.alert.class = 'danger';
    $('#modalfilter').modal('hide');
  }));
}
resetFilter(){

  this.filterData=[];

}
}

