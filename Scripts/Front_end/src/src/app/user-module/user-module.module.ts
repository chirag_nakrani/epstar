import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppModule } from '../app.module';
import { RouterModule, Routes } from '@angular/router';
import { AuthCheckService } from '../common/guards/auth/auth-check.service';
import { UserMenuPermissionMappingComponent } from './user-menu-permission-mapping/user-menu-permission-mapping.component';
import { UserprojectmappingComponent } from './userprojectmapping/userprojectmapping.component';
import { UserrolemappingComponent } from './userrolemapping/userrolemapping.component';

import { CommonImportsModule } from '../common/common-imports.module';
import { CommonModuleModule } from '../common/commonComponents/common-module.module';

const routes: Routes = [
{ path: 'userMenu', component: UserMenuPermissionMappingComponent,
  canActivate: [AuthCheckService] },
{ path: 'userprojectmapping', component: UserprojectmappingComponent,
  canActivate: [AuthCheckService] },
{ path: 'userrolemapping', component: UserrolemappingComponent,
  canActivate: [AuthCheckService] }
];

@NgModule({
  declarations: [
    UserMenuPermissionMappingComponent,
    UserprojectmappingComponent,
    UserrolemappingComponent
  ],
  imports: [
    CommonModule,
    CommonModuleModule,
    CommonImportsModule,
    RouterModule.forChild(routes)
  ]
})
export class UserModuleModule { }
