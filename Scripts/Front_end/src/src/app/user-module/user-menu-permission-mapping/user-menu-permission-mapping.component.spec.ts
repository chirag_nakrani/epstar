import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMenuPermissionMappingComponent } from './user-menu-permission-mapping.component';

describe('UserMenuPermissionMappingComponent', () => {
  let component: UserMenuPermissionMappingComponent;
  let fixture: ComponentFixture<UserMenuPermissionMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserMenuPermissionMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMenuPermissionMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
