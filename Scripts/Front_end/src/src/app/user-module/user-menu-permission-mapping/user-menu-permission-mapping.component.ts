import { Component, OnInit } from '@angular/core';
import { Page } from '../../common/models/page';
import { ApiService } from '../../common/commonServices/apiservice.service';
import { ColumnConfig } from '../../common/shared/columnConfig';
declare var $: any;
declare var jQuery: any;
@Component({
  selector: 'app-user-menu-permission-mapping',
  templateUrl: './user-menu-permission-mapping.component.html',
  styleUrls: ['./user-menu-permission-mapping.component.scss']
})
export class UserMenuPermissionMappingComponent implements OnInit {

  public alert: any = {};
  public addField: any = [];
  public groupList: any = [];
  public menuList: any = [];
  public childMenuList: any = [[]];
  public childMenuListTable: any = [[]];
  public page: Page = new Page();
  public config: any;
  public permissionList: any;
  public dataForm: any;
  public colList: any = [];
  public rows: any = [];
  public pageLimit = 20;
  public pagesizeList = [10,20,30,40,50,100];
  public filterData:any={};
  public childMenuListFilter: any = [];

  constructor( private apiService: ApiService, private colunmConfig: ColumnConfig ) { }

  ngOnInit() {
    this.getCol();
    this.getUser();
    this.getParentMenu();
    this.getPermissonLevel();
    this.readPermissionMapping();

    this.config = {
      displayKey: 'perm_name', //if objects array passed which key to be displayed defaults to description
      search: true, //true/false for the search functionlity defaults to false,
      height: 'auto', //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
      placeholder: 'Select', // text to be displayed when no item is selected defaults to Select,
      noResultsFound: 'No results found!', // text to be displayed when no items are found while searching
      searchPlaceholder: 'Search', // label thats displayed in search input,
      searchOnKey: 'perm_name' // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
    };
    this.addField.push({
      'group_id': '',
      'menu_list': [
        {
          'menu': '',
          'submenu_permission_list': [
            {
              'submenu': '',
              'permission_list': []
            }
          ]
        }
      ]
    });
  }

  AddFiled(index: number) {

    this.addField[index] = {
      'group_id': '',
      'menu_list': [
        {
          'menu': '',
          'submenu_permission_list': [
            {
              'submenu': '',
              'permission_list': []
            }
          ]
        }
      ]
    };
  }
  RemoveField(index) {
    this.addField.splice(index,1);
    }
  calculateTotalPage(rowcount, rowsize) {
    return Math.ceil(rowcount / rowsize);
  }

  getCol() {
    this.colunmConfig.menuPersissionMappingColumn.forEach( x => {
      this.colList.push({'name': x.display_column_name, 'prop': x.table_column});
    });
  }

  getUser() {
    this.apiService.post('groups/list/', {}).subscribe(
      (data: any) => {

        this.groupList = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getParentMenu() {

    const param = {
                  	'parent_menu_id': ''
                  };

    this.apiService.post('menu_list/', param).subscribe(
      (data: any) => {

        this.menuList = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getChildMenu(index: number) {

    const param = {
                    	'parent_menu_id': this.addField[index].menu_list[0].menu,
                    };



    this.apiService.post('menu_list/', param).subscribe(
      (data: any) => {

        this.childMenuList[index] = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getChildMenuFilter(event: any) {

    const param = {
                    	'parent_menu_id': event.target.value.split(':')[1].trim(' ')
                    };



    this.apiService.post('menu_list/', param).subscribe(
      (data: any) => {

        this.childMenuListFilter = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getChildMenuTable(index: number, value?: any) {

    const param = {
                    	'parent_menu_id': value,
                    };

    this.apiService.post('menu_list/', param).subscribe(
      (data: any) => {
        this.childMenuListTable[index] = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  getPermissonLevel() {
    this.apiService.post('permissions/list/', {}).subscribe(
      (data: any) => {

        this.permissionList = data;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  addMenuPermission() {

    this.dataForm = this.transformForm(this.addField);
    const params = {
      'operation_type': 'A',
	    'payload': this.addField
    };

    this.apiService.post('user/menu_role_permission/', params).subscribe(
      (data: any) => {
        this.alert.isvisible = true;
        this.alert.type = 'Success';
        this.alert.text = data.status_desc;
        this.alert.class = 'success';
      },
      (error: any) => {
        console.log(error);
        this.alert.isvisible = true;
        this.alert.type = 'Failure';
        this.alert.text = error.status_desc;
        this.alert.class = 'danger';

      },
      () => {
        this.addField = [];
        this.addField.push({
          'group_id': '',
          'menu_list': [
            {
              'menu': '',
              'submenu_permission_list': [
                {
                  'submenu': '',
                  'permission_list': []
                }
              ]
            }
          ]
        });
        this.readPermissionMapping();
      }
    );

  }

  deletePermission(row) {
    const params = {
      'operation_type': 'D',
	    'payload': row
    };
    this.apiService.post('user/menu_role_permission/', params).subscribe(
      (data: any) => {
          this.readPermissionMapping();
          this.alert.isvisible = true;
          this.alert.type = 'Success';
          this.alert.text = data.status_desc;
          this.alert.class = 'success';
      },
      (error: any) => {
        console.log(error);
        this.alert.isvisible = true;
        this.alert.type = 'Failure';
        this.alert.text = error.status_desc;
        this.alert.class = 'danger';
      }
    );
  }

  isEmpty(obj) {
      for(var key in obj) {
          if (obj.hasOwnProperty(key)) {
              return false;
          }
      }
      return true;
  }

  readPermissionMapping() {

    const params = {
      'operation_type': 'R',
	    'payload': {
      }
    };
    if(this.filterData != undefined && !(this.isEmpty(this.filterData))) {

      params.payload = {"filters": this.filterData};

  }
    this.apiService.post('user/menu_role_permission/', params).subscribe(
      (data: any) => {
          this.rows = data;
          this.rows = [...this.rows];
          $('#modalfilter').modal('hide');
      },
      (error: any) => {
        console.log(error);
        $('#modalfilter').modal('hide');
      }
    );

  }
  resetFilter(){

    this.filterData={}

  }
  // getDisplayName(value: any, prop: string, index: number) {
  //
  //   let returnVal: any;
  //   if ( prop == 'group_id' && this.groupList != undefined && value != '' ) {
  //     if(this.groupList.filter( x => x.id == value )[0] != undefined)
  //     returnVal = this.groupList.filter( x => x.id == value )[0].group_name;
  //   } else if ( prop == 'menu_id' && this.menuList != undefined && value != '' ) {
  //     if(this.menuList.filter( x => x.menu_id == value )[0] != undefined)
  //     returnVal = this.menuList.filter( x => x.menu_id == value )[0].display_name;
  //   } else if ( prop == 'sub_menu_id' && this.childMenuListTable[index] != undefined && value != '' ) {
  //     if(this.childMenuListTable[index].filter( x => x.menu_id == value )[0] != undefined)
  //     returnVal = this.childMenuListTable[index].filter( x => x.menu_id == value )[0].display_name;
  //   } else if ( prop == 'permission_id' && this.permissionList != undefined && value != '' ) {
  //     if(this.permissionList.filter( x => x.id == value )[0] != undefined)
  //     returnVal = this.permissionList.filter( x => x.id == value )[0].perm_name;
  //   }
  //   return returnVal;
  // }

  transformForm( data: any) {
    data.forEach( x => {
      const tempList: any = [];
      x.menu_list[0].submenu_permission_list[0].permission_list.forEach( y => {
        tempList.push(y.id);
      });
      x.menu_list[0].submenu_permission_list[0].permission_list = tempList;
    });
    return data;
  }

}
