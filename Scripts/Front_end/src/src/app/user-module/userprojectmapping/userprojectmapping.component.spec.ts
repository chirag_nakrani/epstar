import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserprojectmappingComponent } from './userprojectmapping.component';

describe('UserprojectmappingComponent', () => {
  let component: UserprojectmappingComponent;
  let fixture: ComponentFixture<UserprojectmappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserprojectmappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserprojectmappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
